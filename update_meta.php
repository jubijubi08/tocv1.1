<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];

$sql_sd="SELECT DISTINCT study_type,therapeutic_area,primary_endpoint,secondary_endpoint,study_medication,phase FROM toc_$study_name WHERE title !='' LIMIT 1 ";
$result_sd=$conn->query($sql_sd);
while($row = $result_sd->fetch_assoc()) {
  $study_type=$row["study_type"];
  $therapeutic_area=$row["therapeutic_area"];
  $primary_endpoint=$row["primary_endpoint"];
  $secondary_endpoint=$row["secondary_endpoint"];
  $study_medication=$row["study_medication"];
  $phase=$row["phase"];
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Manage Programs</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">

        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?>

            </h3>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-12 col-sm-8">
                <div class="pad">

                  <div class="row">
                    <div class="col-md-12">
                      <p style="font-size:18px;">Update Meta data</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                    <form action="#" method="post" id="edit_meta_fd">

                        <div class="form-group has-feedback">
                          <p><b>Study Type:</b></p>
                          <input type="text" name="study_type" id="study_type" class="form-control" value="<?php echo $study_type; ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                          <p><b>Therapeutic Area:</b></p>
                          <input type="text" name="therapeutic_area" id="therapeutic_area" class="form-control" value="<?php echo $therapeutic_area; ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                          <p><b>Primary Endpoint:</b></p>
                          <input type="text" name="primary_endpoint" id="primary_endpoint" class="form-control" value="<?php echo $primary_endpoint; ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                          <p><b>Secondary Endpoint:</b></p>
                          <input type="text" name="secondary_endpoint" id="secondary_endpoint" class="form-control" value="<?php echo $secondary_endpoint; ?>"/>
                        </div>

                      <div class="form-group has-feedback">
                        <p><b>Study Medication:</b></p>
                        <input type="text" name="study_medication" id="study_medication" class="form-control" value="<?php echo $study_medication; ?>"/>
                      </div>

                      <div class="form-group has-feedback">
                        <p><b>Phase:</b></p>
                        <input type="text" name="phase" id="phase" class="form-control" value="<?php echo $phase; ?>"/>
                      </div>

                      <div class="row">
                        <div class="col-xs-8">
                          <div class="checkbox icheck">

                          </div>
                        </div><!-- /.col -->
                        <div class="col-xs-2">

                        </div>
                        <div class="col-xs-4">
                          <button type="button" class="btn btn-primary btn-block btn-flat" id="edit_meta_btn">Update</button>
                        </div><!-- /.col -->
                      </div>
                    </form>
                    </div>
                  </div>


                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!-- /.col -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php
include("include/footer.php");
$conn->close();
?>
      