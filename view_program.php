<?php    
  if(isset($_GET['file'])){
		$file_server = $_GET['file'];
 		if (file_exists($file_server)) {
 			if (is_readable($file_server)) {
 				
 				$fileName = basename($file_server);
      	$fileSize = filesize($file_server);
 				
 				header("Cache-Control: private");
        header("Content-Type: application/stream");
        header("Content-Length: ".$fileSize);
        header("Content-Disposition: attachment; filename=".$fileName);
        readfile ($file_server);                   
      	exit();
 			}
 		}
 		else {
        die('The provided file path is not valid.');
    }
 	}
?>