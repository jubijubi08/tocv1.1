<?php

include("include/connect.php");
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST"){
  // username and password sent from form 
  $myusername=addslashes($_POST['username']); 
  $mypassword=addslashes(md5($_POST['password'])); 


  $sql="SELECT * FROM user_info WHERE username='$myusername' and password='$mypassword'";
  $result=$conn->query($sql);
    while($row = $result->fetch_assoc()) {
			$usertype = $row['user_type'];
			$firstlog=$row['firstlog'];
      $prev_date=$row['creation_date'];	
      $status=$row['status'];					
    }

    // If result matched $myusername and $mypassword, table row must be 1 row
    if($result->num_rows==1 AND $status=='ON'){

      if(isset($_POST['remember']))
      {
        setcookie('username',$myusername, time() + (60*60*24*1));
        setcookie('password',$mypassword, time() + (60*60*24*1));
      }
        //session_register("myusername");
      $_SESSION['login_user']=$myusername;

      if ($usertype==0){
        header("location:administrator/admin.php");
      }
      else{
        date_default_timezone_set('Asia/Dhaka');
        $today = date("Y-m-d H:i:s");
        //$prev_date="2015-06-10 12:44:34";
        $diff = abs(strtotime($today) - strtotime($prev_date));
        $interval = floor($diff / (60*60*24));
        $_SESSION['login_interval']=$interval;

        if($interval>=90 OR trim($firstlog)=='Yes'){
          header("location:change_pass.php");
        }
        else{
          header("location:home.php");
        }
      }

    }
    else 
    {
      if(isset($status)=='OFF'){
        echo '<div class="alert alert-warning">
              <strong>Warning!</strong> Sorry your account is now disabled.
              </div>';
      }
      else {
        echo '<div class="alert alert-danger">
              <strong>SORRY!</strong> Your Login Name or Password is invalid.
              </div>';
      }
    }
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>MeD-OMS | Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="dist/img/scbd.ico">
    <style>
  .scbd {
    height:65px;
    background:#ffffff url('dist/img/logo.png') no-repeat;
    text-align: center;
    position: relative;
    background-position:center;
    z-index:1000;
    margin-top: 50px;
  }

</style>
  </head>
  <body class="login-page" style="background: #ffffff none repeat scroll 0% 0%;">
    <div class="scbd"></div>
    <div class="login-box" >
      <div class="login-logo">
        <a href="index.php"><b>MeD-OMS</b></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body" style="background: #D2D6DE none repeat scroll 0% 0%;">
        <p class="login-box-msg">Sign in to start your session</p>


        <form action="" method="post">
          <input type="hidden" name="login_form" value="true">
          <div class="form-group has-feedback">
            <input type="text" name="username" id="username" class="form-control" placeholder="User Name"/>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
           <div class="col-xs-8">    
               <label>
                 <input type="checkbox" >Remember Me</input>
               </label>
           </div><!-- /.col -->
           <div class="col-xs-4">
             <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
           </div><!-- /.col -->
         </div>
        </form>
        <a href="forgotpass.php">I forgot my password</a><br>
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.3 -->
    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    
  </body>
</html>

