<?php
include("include/connect.php");
include("lib/gen_func.php");
require 'vendor/autoload.php';

session_start();

$user_check=$_SESSION['login_user'];

$ses_sql=$conn->query("select username,firstlog from user_info where username='$user_check' ");
//print_r($ses_sql);

while($row = $ses_sql->fetch_assoc()) {
    $login_session = $row['username'];
    $firstlog = $row['firstlog'];
}
if(!isset($login_session))
{
    header("Location: index.php");
}

$username = $_SESSION['login_user'];
$sql="SELECT count(y.allocation_id) as snum FROM user_info as x , study_allocation as y WHERE x.username = '$username' AND x.user_id = y.user_id AND y.status='ON'";
$countstudy = $conn->query($sql);

while($row = $countstudy->fetch_assoc()) {
    $snum = $row['snum'];
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MeD-OMS</title>
<link rel="stylesheet" href="stree/demo.css" type="text/css">

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- Bootstrap 3.3.2 -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- FontAwesome 4.3.0 -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons 2.0.0 -->
<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
<!-- css top menu -->
<link rel="stylesheet" href="cssmenu/menustyles.css">
<!-- bootstrap wysihtml5 - text editor -->
<link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="dist/img/scbd.ico">

<link rel="stylesheet" href="stree/metroStyle/metroStyle.css" type="text/css">


<style>
  .scbd {
    height:65px;
    background:#ffffff url('http://tsdev.shaficonsultancy.com/resources/img/logo.png') no-repeat;
    text-align: center;
    position: relative;
    background-position:center;
    z-index:1000;
  }

  li, span{
    font-size:17px;
  }
</style>

</head>


<body class="skin-blue layout-wide">
<div class="scbd"></div>
<div class="wrapper">
<header class="main-header">
<!-- Logo -->
<a href="home.php" class="logo" ><b>MeD-OMS</b></a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation" >
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>


    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <span class="hidden-xs"><?php  echo "$username";  ?> </span>
                </a>
                <ul class="dropdown-menu">

                    <li class="user-footer">
                        <div class="pull-left">
                            <p> <small>New Study:<?php  echo " $snum";  ?> Total Study:<?php  echo " $snum";  ?></small></p>
                        </div>
                        <div class="pull-left">
                            <a href="change_pass.php" class="btn btn-default btn-flat">Change Password</a>
                        </div>
                        <div class="pull-right">
                            <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- drop down menu for user-->
        </ul>
    </div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar" style="padding-top: 80px;">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>

<?php
           if ($firstlog == 'No'){

            echo "

        <li>
            <a href='home.php'>
                <i class='fa fa-th'></i> <span>Home</span> <small class='label pull-right bg-green'></small>
            </a>
        </li>

                 ";
           }
?>
        <li>
          <a href="change_pass.php">
            <i class="fa fa-th"></i> <span>Change Password</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

    </ul>
</section>
<!-- /.sidebar -->
</aside>

<script type='text/javascript'>

function update_confirm(){
	var x = confirm('Are you sure want to change password?');

	if(x==true)
		document.getElementById("update_pass_f").submit();
	else
			document.getElementById("update_pass_f").reset();
}

</script>
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>

          </ol>
<!--           <h1>
            Dashboard
            <small>Control panel</small>
          </h1> -->

        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              <!-- MAP & BOX PANE -->
              <div class="box box-success">
                <div class="box-header with-border">
                    <?php if ($_SESSION["login_interval"] >=90) {
             								echo "<div class='alert alert-info' role='alert'>
    												Your password has been expired!
    												</div>";
    											}
            						$sql="SELECT * FROM user_info WHERE username='$login_session'";
            						$result=$conn->query($sql);
            						  while($row = $result->fetch_assoc()) {
            							$usertype = $row['user_type'];
            							$firstlog=$row['firstlog'];
            							$prev_date=$row['creation_date'];
            							$status=$row['status'];
            						  }

            						if ($firstlog =='Yes') {
            						echo "<div class='alert alert-info' role='alert'>
            						Please change your initial password to access to the system!
            						</div>";
            					  }
          				?>
                  <h3 class="box-title">Change your password</h3>
                  <div class="box-tools pull-right">

                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-9 col-sm-8">
                      <div class="pad">
                      <?php
                              if($_SERVER["REQUEST_METHOD"] == "POST")  {
                                  $username = $username;
                                  //echo $username;
                                  $password = md5($_POST['old_pass']);
                                  $newpassword = md5($_POST['new_pass']);
                                  $newpassword2 = $_POST['new_pass'];
                                  $confirmnewpassword = md5($_POST['confirm_pass']);

                                  $sql="SELECT password,email FROM user_info WHERE username = '$username'";
                                  $result=$conn->query($sql);
                                  while($row = $result->fetch_assoc()) {
                                          $userPass = $row['password'];
                                          $to=$row['email'];
                                  }

                                  date_default_timezone_set('Asia/Dhaka');
                                  $today = date("Y-m-d H:i:s");

                                  if($password == $userPass){
                                    if($newpassword != $password){
                                      if($newpassword == $confirmnewpassword){
                                        $sql_update="UPDATE user_info SET password = '$newpassword', creation_date='$today', firstlog='No' where username = '$username'";
                                        $result_update=$conn->query($sql_update);
                                        if($result_update){
                                          $sucess="Password updated successfully";
                                          echo "<div class='alert alert-success'>
                                                  <strong>Success!</strong> $sucess.
                                                </div>";

                                          //Details for sending E-mail

											$body = "Hi,\n\n Your password has been updated successfully.\n\n Your new password is:  $newpassword2";




										smtpmailer($to, 'admin_medoms@shaficonsultancy.com', 'MeD-OMS Admin', 'MeD-OMS Password Changed', $body);

                                        }
                                      }
                                      else{
                                        $error="Confirmed password does not matched!!";
                                          echo "<div class='alert alert-warning'>
                                                  <strong>Warning!</strong> $error.
                                                </div>";
                                      }
                                    }
                                    else{
                                      $error="You can not use your previous password as new password!!";
                                      echo "<div class='alert alert-warning'>
                                                  <strong>Warning!</strong> $error.
                                                </div>";
                                    }
                                  }
                                  else{
                                    $error="You entered current password!!";
                                    echo "<div class='alert alert-warning'>
                                                <strong>Warning!</strong> $error.
                                              </div>";
                                  }

                                }
                        ?>
              						<form action="change_pass.php" method= "post">
              							<p>User Name :</p>
              							<div class="form-group has-feedback">
              						   <input type="text" name="user_name"  disabled id="" class="form-control" value="<?php echo $username; ?>"/>
              						   <span class="glyphicon  form-control-feedback"></span>
              						   </div>
              							<div class="form-group has-feedback">
              							<p>Current Password :</p>
              							<input type="password" name="old_pass" id="old_pass" class="form-control" required placeholder="Current Password" />
              							<span class="glyphicon  form-control-feedback"></span>
                            <span id="curr_pass_alrt" class="curr_pass_alrt"></span>
              							</div>

              							<div class="form-group has-feedback">
              	 						<p>New Password :</p>
              							<input type="password" name="new_pass" id="new_pass" class="form-control" onkeyup="checkPass1(); return false;" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Password Should be contain(UpperCase, LowerCase, Number/SpecialChar and min 8 characters)" required  placeholder="New Password"/>
              							<span class="glyphicon  form-control-feedback"></span>
                            <span id="new_pass_alrt" class="new_pass_alrt"></span>
              							</div>

              							<div class="form-group has-feedback">
              	 					    <p>Confirm Password :</p>
              							<input type="password" name="confirm_pass" id="confirm_pass" class="form-control" onkeyup="checkPass(); return false;" required placeholder="Confirm Password"/>
              							<span class="glyphicon  form-control-feedback"></span>
              							<span id="confirmMessage" class="confirmMessage"></span>
              							</div>

              						    <div class="row">
              								<div class="col-xs-4">
              								  <button type="submit" class="btn btn-primary btn-block btn-flat">Update password</button>
              								</div>
              							</div>

              						</form>

                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row (main row) -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
<script type="text/javascript">
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('new_pass');
    var pass2 = document.getElementById('confirm_pass');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#ffffff";
    var goodColorf = "#66cc66";
    var badColor = "#ff6666";
    if(pass1.value == pass2.value && pass2.value != ""){
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColorf;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        if(pass2.value == "")
            message.innerHTML = "Empty field!";
        else
            message.innerHTML = "Passwords Do Not Match!"
    }
}

function checkPass1()
{
    //Store the password field objects into variables ...
    var pass0 = document.getElementById('old_pass');
    //alert(pass0);
    var pass1 = document.getElementById('new_pass');
    //Store the Confimation Message Object ...
    var new_pass_message = document.getElementById('new_pass_alrt');
    //Set the colors we will be using ...
    var goodColor = "#ffffff";
    var goodColorf = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if(pass0.value == pass1.value){
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass1.style.backgroundColor = badColor;
        new_pass_message.style.color = badColor;
        new_pass_message.innerHTML = "You can not use previous password!"
        pass1.innerHTML = " ";
    }
    else{
        pass1.style.backgroundColor = goodColor;
        new_pass_message.innerHTML = " ";
    }
}
</script>
<?php
include("include/footer.php");
?>
