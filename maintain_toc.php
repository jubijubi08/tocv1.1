<?php
  include("include/header.php");
  error_reporting(E_ALL ^ E_NOTICE);

  require 'include/Classes/PHPExcel.php';
  include("include/Classes/PHPExcel/IOFactory.php");
  include("include/connect.php");
  $study_name=$_SESSION["study"];

$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

  date_default_timezone_set('Asia/Dhaka');
  $today = date("Y-m-d H:i:s");

if(!isset($_SESSION["dc_selector"])){
    $dc_id = 0;
    $_SESSION["dc_selector"] =$dc_id;
}
else {
    $dc_id = $_SESSION["dc_selector"];
}
//$_SESSION["dc_selector"] = $dc_id;
$dc_db_val='SP'.$dc_id;


if(isset($_SESSION["filter_dc_selector"])){
    $dc2_id = $_SESSION["filter_dc_selector"];
}
else {
    $dc2_id = $dc_id;
    $_SESSION["filter_dc_selector"] = $dc2_id;
}


?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Maintain Toc</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-6" >
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name: <?php echo $study_name ?></h3>
              <div class="box-tools pull-right">

                  <div class="col-md-12">
                      <label><h3 class="box-title"> Select Data Currency: </h3> </label>

                      <select id="dc_selector" name="dc_selector" onchange="reload_tree_dc(this)">
                          <?php
                          while($row = $snap_list->fetch_assoc()) {
                              $dc_id_c = $row['id'];
                              $sp_name = $row['snap_name'];

                              if($dc_id_c==$dc_id){
                                  echo "<option value='".$dc_id_c."' selected='selected'>".$sp_name."</option>";
                                  $sp_name_active=$sp_name;
                              }else{
                                  echo "<option value=".$dc_id_c.">".$sp_name."</option>";
                              }

                          }

                          ?>


                      </select>
                  </div>
              </div>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-4 col-sm-8">
                <div class="pad">
                  <div class="col-xs-12">

                          <p> Download toc file 
                        <button type="submit" name="download"  onclick="export_toc()" class="btn btn-primary btn-block btn-flat">Download</button></p>

                  </div>
                  <br><br><br><br>

                  <?php




                    if(isset($_POST["submit"])) {
                        $dtvar=date("Y_m_d_H_i_s");


                      $uploadOk = 0; 
                      $target_dir = $file_server.$study_name."/";

                      if ($_FILES["fileToUpload"]["name"] != "") {

                        $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);

                        $uploadOk = 1;

                        $FileType = pathinfo($target_file, PATHINFO_EXTENSION);

                        // Check if file already exists
                        if (file_exists($target_file)) {
                          echo '<div class="alert alert-info" role="alert">
                                  <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                  <span class="sr-only">Error:</span>
                                  New Metafile replaced with old one!
                                </div>';
                          //$uploadOk = 1;
                        }
                        // Check file size
                        if ($_FILES["fileToUpload"]["size"] > 500000) {
                          echo "Sorry, your file is too large.";
                          $uploadOk = 0;
                        }
                        // Allow certain file formats
                        if ($FileType != "xls" ) {
                          echo '<div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                Sorry, Only xls files are allowed !
                                </div>';
                          $uploadOk = 0;
                        }
                        // Check if $uploadOk is set to 0 by an error
                        if ($uploadOk == 0) {
                          echo '<div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                <span class="sr-only">Error:</span>
                                Sorry, your file was not uploaded !
                                </div>';
                          // if everything is ok, try to upload file
                        } else {
                          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                            echo "<div class='alert alert-success' role='alert'>The file ". basename($_FILES["fileToUpload"]["name"]) ." has been imported.</div>";
                            $uploadOk = 1;
                          } else {
                            echo '<div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    Sorry, there was an error uploading your file !
                                  </div>';
                            $uploadOk = 0;
                          }
                        }
                      }
                      else {
                        echo '<div class="alert alert-danger" role="alert">
                                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                  <span class="sr-only">Error:</span>
                                No file selected !
                              </div>';
                              
                      }

                      //Create tables: toc content and toc status in database!!
                      if ($uploadOk == 1) {

                          $sql="CREATE TABLE tmptable_$dtvar AS 
                          SELECT x.pgmname as pgmname,x.outno as outno,y.*  
                          FROM toc_$study_name AS x ,toc_status_$study_name AS y 
                          WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency='SP0' ORDER BY x.sortorder ASC ";

                          $result = $conn->query($sql);
//
//                          if($result){echo "step 1: temoprary table created!!<BR>";}
//                          else{echo "FAIled to create tmptable create<BR> ";}

                          //create backup all old toc table
                          $bkday = $study_name."_".$dtvar;
                          //echo "$bkday";

                          $sql="CREATE TABLE bk_toc_$bkday AS SELECT *  FROM toc_$study_name";
                          $result = $conn->query($sql);
//                          if($result){
//                          echo "step 2: toc table backup created!!<BR>";
//                          }else{
//                          echo "failed to create backup";
//                          }

                          $delsql1 ="DELETE FROM toc_$study_name WHERE sortorder IN(SELECT sortorder from tmptable_$dtvar) AND data_currency='SP0' ";
                          $del = $conn->query($delsql1);
//
//                          if($del){echo "step:3 toc  table row  deleted!<BR>";}
//                          else{ echo "failed to create backup";}

                          $delsql2 ="DELETE FROM toc_status_$study_name WHERE sortorder IN(SELECT sortorder from tmptable_$dtvar) AND data_currency='SP0' ";
                          $del = $conn->query($delsql2);

//                          if($del){echo "step:3  toc status table row deleted!<BR>";}
//                          else{ echo "failed to create backup";}


                      }

                      //insert into database!!
                      if ($uploadOk == 1) {

                          $inputFileType = 'Excel5';
                          $inputFileName = $target_file;
                          $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                          $objPHPExcel = $objReader->load($inputFileName);

                          $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);

                          $total_row = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                          $total_col = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
                          $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($total_col);
                         // echo "row: ".$total_row." col: ".$highestColumnIndex."</br>";


                          for ($row = 1; $row < 2; $row++){
                              $db_variable_list_all2 = "";
                              $count1=0;
                              for($col = $highestColumnIndex-2; $col > -1; $col--){
                                  $cell = $objPHPExcel->setActiveSheetIndex(0)->getCellByColumnAndRow($col, $row);
                                  $variable_list= strtolower($val = $cell->getValue());

                                  $db_variable_list_all2 = $variable_list.",".$db_variable_list_all2;
                                  $count1++;
                              }
                              $db_variable_list_all = substr(trim($db_variable_list_all2),0,-1);

                          }

                          for ($row = 2; $row < $total_row + 1; $row++) {
                              $sql_values = " ";
                              $count2=0;
                              for ($col = $highestColumnIndex-2; $col > -1; $col--) {
                                  $cell = $objPHPExcel->setActiveSheetIndex(0)->getCellByColumnAndRow($col, $row);
                                  $filter_value=$cell->getValue();
                                  $filter_value1 = str_replace("'","''",$filter_value);

                                  $sql_values = "'" . $filter_value1 . "'" . "," . $sql_values;
                                  $count2++;
                              }
                              $sql_values2 = substr(trim($sql_values),0,-1);

                              /////insert values from metafile to database in toc table
                              $result = $conn->query("INSERT INTO toc_$study_name($db_variable_list_all ) VALUES ($sql_values2)");

//                              if ($result) {
//                                  echo "<div class='alert alert-success' role='alert'>TOC Table: Data has been imported successfully.</div>";
//                              }
//                              else {
//                                  echo '<div class="alert alert-danger" role="alert">
//                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
//                               <span class="sr-only">Error:</span>
//                               TOC Table: Sorry, failed to import !
//                               </div>';
//                              }
                          }

//                          echo "count1: ".$count1."   count2: ".$count2;

                          //insert into toc status table
                          $sql1="SELECT * FROM toc_$study_name WHERE data_currency='SP0' ";
                          $result1 = $conn->query($sql1);
                          // print_r($result);
                          while($row = $result1->fetch_assoc()) {

                              $sortorder=$row['sortorder'];
                              $study=$row['study'];
                              //echo $sortorder."<BR>";

                              date_default_timezone_set('Asia/Dhaka');
                              $today = date("Y-m-d H:i:s");
                              $var2 = "'SP0','$study','$sortorder','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today'";
                              // echo "<h3>".$var2."</h3>";

                              $result5 = $conn->query("INSERT INTO toc_status_$study_name
                                   (data_currency,study,sortorder,pgmstat,pgmstatdc,pdate_0,pdate_1,pdate_2,pdate_3,outstat
                                    ,outstatdc,odate_0,odate_1,odate_2) VALUES ($var2)");

//                              if ($result5) {
//                                  echo "<div class='alert alert-success' role='alert'>TOC Status Table: Data has been imported successfully.</div>";
//                              }
//                              else {
//                                  echo '<div class="alert alert-danger" role="alert">
//                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
//                               <span class="sr-only">Error:</span>
//                               TOC Status Table: Sorry, failed to insert !
//                               </div>';
//                              }

                          }

                          
                      
                        //update old data table with backup data
                        $selsql="SELECT x.sortorder as sortorder FROM toc_$study_name AS x , tmptable_$dtvar AS y WHERE x.pgmname=y.pgmname AND  x.data_currency=y.data_currency AND x.outno=y.outno AND x.data_currency='SP0'";
                        $sel = $conn->query($selsql);
                        while($row = $sel->fetch_assoc()) {
                            $sortorder = $row['sortorder'];
                            //echo $sortorder."<BR>";
                        }

                        $sql="SELECT * FROM tmptable_$dtvar";
                        $result = $conn->query($sql);
                        $pgmlist="";
                        while($row1 = $result->fetch_assoc()) {
                          $pgmname = $row1['pgmname'];
                          $outno = $row1['outno'];
                          $pgmstat=$row1['pgmstat'];
                          $pgmstatdc=$row1['pgmstatdc'];
                          $pdate_0=$row1['pdate_0'];
                          $pdate_1=$row1['pdate_1'];
                          $pdate_2=$row1['pdate_2'];
                          $pdate_3=$row1['pdate_3'];
                          $outstat=$row1['outstat'];
                          $outstatdc=$row1['outstatdc'];
                          $odate_0=$row1['odate_0'];
                          $odate_1=$row1['odate_1'];
                          $odate_2=$row1['odate_2'];

                          $selsql="SELECT sortorder FROM toc_$study_name  WHERE pgmname='$pgmname' AND outno='$outno' AND data_currency='SP0'";
                          $sel = $conn->query($selsql);
                          if($result->num_rows > 0){
                            while ($row2 = $sel->fetch_assoc()) {
                              $sortid=$row2['sortorder'];
                              //echo "<br><B>for update id : $sortid";

                              $updsql="UPDATE toc_status_$study_name
                                        SET data_currency='SP0'
                                            ,pgmstat=$pgmstat
                                            ,pgmstatdc='$pgmstatdc'
                                            ,pdate_0='$pdate_0'
                                            ,pdate_1='$pdate_1'
                                            ,pdate_2='$pdate_2'
                                            ,pdate_3='$pdate_3'
                                            ,outstat=$outstat
                                            ,outstatdc='$outstatdc'
                                            ,odate_0='$odate_0'
                                            ,odate_1='$odate_1'
                                            ,odate_2='$odate_2'
                                        WHERE sortorder='$sortid' AND data_currency ='SP0'";
                              $upreslt = $conn->query($updsql);
//                              if($upreslt) {echo "<br>update done nicely wow!!<br>";}
//                              else{echo "<br>update failed!!<br>";};
                            }
                          }
                          else{

                          }

                        }

                        $delsql="DROP TABLE tmptable_$dtvar";
                        $del = $conn->query($delsql);
//                        if ($del){echo " backup table tmptable droped<br>";}
//                        else{echo " tmptable droped failed<br>";};

                          if ($del) {
                                  echo "<div class='alert alert-success' role='alert'>Data has been imported successfully.</div>";
                              }
                              else {
                                  echo '<div class="alert alert-danger" role="alert">
                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                               <span class="sr-only">Error:</span>
                               Import Status : Sorry, failed to Import !
                               </div>';
                              }
                      }
                    }
                  ?>


                  <div class="col-xs-12">
                    <p> Import toc file <br>
                    <button  class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target=".bs-example-import-toc-modal-lg">Import</button></p>
                  </div>
                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->          
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      