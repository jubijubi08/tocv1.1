<?php
	include("include/header.php");
	include("include/connect.php");
	$study_name = $_SESSION["study"];
?>
<style>
	span.fa-calendar{
		display: inline-block;
		position:absolute;
		z-index:3;
		text-indent: -15px;
		margin-top: 8px;
	}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Set Run Time</li>
    </ol>
    
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name : <?php echo $_SESSION["study"] ?></h3>
            <div class="box-tools pull-right"></div>
          </div><!-- /.box-header -->
          
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-12">
                <div class="pad">
                    <div class="row">
                      <div class="col-md-12">
                        <p style="font-size:18px;"> Set Run Time for Existing Programs</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                      	<p>List of all available programs</p>
                      	<form onsubmit="runpgm(this,2); return false;" >
                      		<div class="col-xs-12">
                        		<div class="checkbox icheck">
                        			<table>
                        				<thead>
                        					<tr style="line-height: 1.5em;"><th style="padding-left: 10px;">Run Type</th><th style="padding-left: 10px;">Start Date Time</th><th style="padding-left: 10px;">Programe Title</th></tr>
                        				</thead>
                        				
                        				<tbody>
				                          <?php                                      
				                        		$sql="SELECT * FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.pgmstat!=0 AND x.data_currency='SP0' ORDER BY x.sortorder  ";
				                        		$result = $conn->query($sql);
				                            if ($result->num_rows > 0) {
				                            	$ind = 0;
				                              while($row = $result->fetch_assoc()) {
				                              	$val = $row['sortorder'];
				                              	$check = $conn->query("select * from program_run_time where study_name = '$study_name' AND  program_id = '$val' AND  status = 'ON' ");
				                              	$status = $check->fetch_assoc();
				                                if($row['pgmstat']==1){
				                                	echo "<tr  style='line-height: 1.5em;'><td style='padding-left: 10px;'><select id='run_type_$ind' name='run_type' class='run_type'  key='$ind' style='width:150px; margin-left: 5px; height:35px; margin-top:2px;'>
																						  <option value=''>select</option>";?>
																						  <option value='yearly' <?php if(isset($status) && $status['run_type'] == 'yearly') echo "selected";?>>Yearly</option>
																						  <option value='monthly' <?php if(isset($status) && $status['run_type'] == 'monthly') echo "selected";?>>Monthly</option>
																						  <option value='weekly' <?php if(isset($status) && $status['run_type'] == 'weekly') echo "selected";?>>Weekly</option>
																						  <option value='daily' <?php if(isset($status) && $status['run_type'] == 'daily') echo "selected";?>>Daily</option>
																						  <option value='once' <?php if(isset($status) && $status['run_type'] == 'once') echo "selected";?>>Once</option>
																					<?php echo "</select></td>";
				                                  echo "<td style='padding-left: 10px;'><input id='start_date_$ind' key='$ind' value='$status[start_date]' style='width:200px; height:35px; margin-top:2px;'/><span class='fa fa-calendar open-datetimepicker start_date' style='cursor: pointer;' key='$ind'></span></td>";
				                                  echo "<td  style='padding-left: 10px;'><label style='color:#F6AA12;'><input type='checkbox'";
				                                  if(isset($status)) echo "checked ";
				                                  echo "id='ind_check_$ind' class='pgmlst' name='pgnam[]' key='$ind' table-id='$status[id]' study='$study_name' value='".$row['sortorder']."'>".$row['title']."</label></td>";
				                                  if(isset($status)) echo "<td><a class='btn  btn-primary' href='#' id='delete_$status[id]' data='$status[id]'  key='$ind' onclick='myFunction($status[id])'><span class='text-center glyphicon glyphicon glyphicon-remove'></span></a></td>";
				                                  echo "</tr>";	
				                                }
				                                if($row['pgmstat']==3){
				                                  echo "<tr><td style='padding-left: 10px;'><select id='run_type_$ind' name='run_type' class='run_type'  key='$ind' style='width:150px; margin-left: 5px; height:35px; margin-top:2px;'>
																						  <option value=''>select</option>";?>
																						  <option value='yearly' <?php if(isset($status) && $status['run_type'] == 'yearly') echo "selected";?>>Yearly</option>
																						  <option value='monthly' <?php if(isset($status) && $status['run_type'] == 'monthly') echo "selected";?>>Monthly</option>
																						  <option value='weekly' <?php if(isset($status) && $status['run_type'] == 'weekly') echo "selected";?>>Weekly</option>
																						  <option value='daily' <?php if(isset($status) && $status['run_type'] == 'daily') echo "selected";?>>Daily</option>
																						  <option value='once' <?php if(isset($status) && $status['run_type'] == 'once') echo "selected";?>>Once</option>
																					<?php echo "</select></td>";
				                                  echo "<td style='padding-left: 10px;'><input id='start_date_$ind' class='start_date'  key='$ind' value='$status[start_date]' style='width:200px; height:35px; margin-top:2px;'/><span class='fa fa-calendar open-datetimepicker start_date' style='cursor: pointer; ' key='$ind' ></span></td>";
				                                  echo "<td  style='padding-left: 10px;'><label style='color:#0FAA0F;'><input type='checkbox'";
				                                  if(isset($status)) echo "checked ";
				                                  echo "id='ind_check_$ind' class='pgmlst' name='pgnam[]' key='$ind' table-id='$status[id]' study='$study_name' value='".$row['sortorder']."'>".$row['title']."</label></td>";
				                                  if(isset($status)) echo "<td><a class='btn btn-primary' href='#' id='delete_$status[id]' data='$status[id]'  key='$ind' onclick='myFunction($status[id])'><span class='text-center glyphicon glyphicon glyphicon-remove'></span></a></td>";
				                                  echo "</tr>";	
				                                }
				                                $ind++;
				                              }
				                            }
				                          ?>
                          			</tbody>
                          		</table>
                        		</div>
                      		</div>
                      		<div class="col-md-12">
                      			<div class="col-md-5"></div>
			                      <div class="col-md-1">
			                        <input type="button" value="Save" disabled class="btn btn-primary btn-block btn-flat sendRun"/>
			                      </div>
			                      <div class="col-md-6"></div>
                      		</div>
		                      <span class="alert alert-success select1 " style="font-size: 14px;  position: fixed; bottom: 20px;right: 200px; display:none;"> Changes save successfully.</span>
                  			</form>
                   		</div>
                		</div>
                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->          
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>


<script>
	$(document).ready(function() {
		$("input.pgmlst").on("change", function(){
			var showBtn = "no";
			if ($(this).prop('checked')) {
				var id = $(this).attr('key'); 
				var start_date = $("#start_date_"+id).val();
				var run_type = $("#run_type_"+id+" option:selected").val();
				if(start_date == "") {
					$("#start_date_"+id).focus();
					$(this).prop('checked', false);
					return false;
				}
				if(run_type == "") {
					$("#run_type_"+id).focus();
					$(this).prop('checked', false);
					return false;
				}
			}
			$('input.pgmlst').each(function() {
	      if($(this).is(':checked')) {
					showBtn = "yes";
				}
			});
			if(showBtn == "yes")$("input.sendRun").removeAttr("disabled");
			else $("input.sendRun").attr("disabled", true);
		});
		
		$("input.start_date").on("blur", function(){
			var start_date = $(this).val();
			var key = $(this).attr('key');
			var run_type = $("#run_type_"+key+" option:selected").val();
			if(start_date == ""){
				$("#start_date_"+key).focus();
				$("#ind_check_"+key).prop('checked', false);
				$("input.sendRun").attr("disabled", true);
			}else{
				if($("#ind_check_"+key).is(':checked') && run_type != ""){
					$("input.sendRun").removeAttr("disabled");
				}
				else{
					$("#ind_check_"+key).prop('checked', false);
					$("input.sendRun").attr("disabled", true);
				}
			}
		});
		
		$("select.run_type").on("change", function(){
			var run_type = $(this).val();
			var key = $(this).attr('key');
			var start_date = $("#start_date_"+key).val();
			if(run_type == ""){
				$("#run_type_"+key).focus();
				$("#ind_check_"+key).prop('checked', false);
				$("input.sendRun").attr("disabled", true);
			}else{
				if($("#ind_check_"+key).is(':checked') && start_date != ""){
					$("input.sendRun").removeAttr("disabled");
				}
				else{
					$("#ind_check_"+key).prop('checked', false);
					$("input.sendRun").attr("disabled", true);
				}
			}
		});
		
		$("input.sendRun").on("click", function(){
			var study = new Array();
			var value = new Array();
			var start_date = new Array();
			var run_type = new Array();
			var ids = new Array();
			var ind = 0;
			$('input.pgmlst').each(function() {
	      if($(this).is(':checked')) {
					var id = $(this).attr('key');
					ids[ind] = $(this).attr('table-id');
					study[ind] = $(this).attr('study');
					value[ind] = $(this).val();
					start_date[ind] = $("#start_date_"+id).val();
					run_type[ind] = $("#run_type_"+id+" option:selected").val();
					ind++;
				}
			});
			if(ind!=0){
				$.ajax({
					type: "POST", 
					async: false, 
					url: "save_time.php",
					data: {
					 	study:study,value:value,start_date:start_date,run_type:run_type,ids:ids,action:'save'
					},
					beforeSend: function(){
						
					},
				 	success: function(response){
				 		$("input.sendRun").attr("disabled", true);
				 		$(".select1").slideDown(function() {
				    	setTimeout(function() {
				        $(".select1").slideUp();
				    	}, 2000);
						});
				 	}
			 	});	
			}
		});	
	});	
</script>

	<script>
		function myFunction(ind){
			var id = $('#delete_'+ind).attr("data");
			var key = $('#delete_'+ind).attr('key'); 

			if(id !=0 ){
				$.ajax({
					type: "POST", 
					async: false, 
					url: "save_time.php",
					data: {
					 	id:id,action:'delete'
					},
					beforeSend: function(){
						
					},
				 	success: function(response){
				 		$("input#ind_check_"+key).prop('checked', false);
						$("input#start_date_"+key).val("");
						$("#run_type_"+key+" option:selected").removeAttr('selected');
						$('#delete_'+ind).remove();
				 	}
			 	});	
			}
		}
	</script>
<script src="js/jquery.simple-dtpicker.js"></script>
<script>
	$(document).ready(function() {
		$(".start_date").on("click", function(){
			var id = $(this).attr('key');
			$('#start_date_'+id).appendDtpicker({
				"futureOnly": true,
				"minuteInterval": 15,
				"onInit": function(handler){
					handler.show();
				},
				"onHide": function(handler){
					handler.destroy();
				},
			});
			$("div.datepicker").css("width", "200px"); 
			$("div.datepicker_timelist").css("height", "139px;"); 
	 	});
	});
</script>
      