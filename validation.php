<?php
include("include/header.php");
error_reporting(E_ALL ^ E_NOTICE);
require_once 'include/excel_reader2.php';
include("include/connect.php");
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Validation Program</li>
          </ol>
<!--           <h1>
            Dashboard
            <small>Control panel</small>
          </h1> -->
          
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12 ">
              <!-- MAP & BOX PANE -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Validation Program </h3>
                  <div class="box-tools pull-right">
                      <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-9 col-sm-8">
                      <div class="pad">
                        <h1>Programs validation here .........</h1>

                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->          
          </div><!-- /.row (main row) -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      