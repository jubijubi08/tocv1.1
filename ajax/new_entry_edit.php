<?php 
include("../include/connect.php");
session_start();
$study_name=$_SESSION["study"];
$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");

$data = array();
//var_dump($_POST);

//code insert new entry
if(isset($_POST['ne_row_id'])){

  $ne_title=$_POST["ne_title"];
  $ne_pgmnam=$_POST["ne_pgmnam"].".sas";
  $ne_pgtyp=$_POST["ne_pgtyp"];
  $ne_outnum=$_POST["ne_outnum"];
  $ne_outnam=$_POST["ne_outnam"].".lst";
  $ne_lognam=$_POST["ne_lognam"].".log";
  $ne_pgloc=$_POST["ne_pgloc"];
  $ne_outloc=$_POST["ne_outloc"];
  $pgmid =$_POST["ne_row_id"];
  $tlfnum =$_POST["ne_tlfnum"];
  $population =$_POST["population"];
  $shell_name =$_POST["shell_name"];
//  $Parameter_1_name =$_POST["Parameter_1_name"];
//  $Parameter_1_value =$_POST["Parameter_1_value"];

  //print_r($_POST);

  $param_count = 0;
  foreach($_POST as $key => $value)
  {
      if(trim(substr($key, 0, 10)) == trim('parameter_')) {
        $param_count++;
      }
  }

  $param_list_count = $param_count/2;

    $param_list_val_name='';
    $param_list_val_value='';
  for($i=1;$i<=$param_list_count;$i++){
    $paramname="parameter_".$i."_name";
    $$paramname = $_POST['parameter_'.$i.'_name'];

    $paramvalue="parameter_".$i."_value";
    $$paramvalue = $_POST['parameter_'.$i.'_value'];

//        echo "parameter_".$i."_value = '$$paramvalue',";
      $param_list_val_name =  $param_list_val_name.",parameter_".$i."_name,parameter_".$i."_value";
      $param_list_val_value =  $param_list_val_value.",'".$$paramname."','".str_replace("'","''",$$paramvalue)."'";

  }
 //echo $param_list_val_value;

  //echo $ne_title." ".$ne_pgmnam." ".$ne_pgtyp." ".$ne_outnum." ".$ne_outnam." ".$ne_lognam." ".$ne_pgloc." ".$ne_outloc." ".$pgmid." ";


  $sql_sd="SELECT DISTINCT study_type,therapeutic_area,primary_endpoint,secondary_endpoint,study_medication,phase FROM toc_$study_name WHERE title !='' LIMIT 1 ";
  $result_sd=$conn->query($sql_sd);
  while($row = $result_sd->fetch_assoc()) {
    $study_type=$row["study_type"];
    $therapeutic_area=$row["therapeutic_area"];
    $primary_endpoint=$row["primary_endpoint"];
    $secondary_endpoint=$row["secondary_endpoint"];
    $study_medication=$row["study_medication"];
    $phase=$row["phase"];
  }

  $pgmid_len=iconv_strlen (trim($pgmid));
  //echo  "<BR>pgmid_len: ".$pgmid_len."<BR>";
  $tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);

  $ses_sql=$conn->query("SELECT section FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0' ");
  while($row = $ses_sql->fetch_assoc()) {
      $section = $row['section'];
      //echo  "<BR>Section name: ".$section."<BR>";
  }

  if($section!=" "){
    //echo"<h1>this is a section</h1><br>";

    $pgmid_len1=($pgmid_len/2)+1;
    //echo "selected sortorder for search : ".$tosearchid."<br>";
    //select row information
    $result_rinfo=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0'");
    while($row_rinfo = $result_rinfo->fetch_assoc()) {
      $l1 = $row_rinfo['l1'];
      $l2 = $row_rinfo['l2'];
      $l3 = $row_rinfo['l3'];
      $l4 = $row_rinfo['l4'];
      $l5 = $row_rinfo['l5'];
      $section=$row_rinfo['section'];
    }
    //echo "l1: ".$l1." l2: ".$l2." l3:".$l3." l4:".$l4." l5:".$l5."<br>section:".$section."<br>";

    $sql_max="SELECT MAX(l$pgmid_len1) as lvl FROM toc_$study_name WHERE sortorder LIKE '$pgmid%' AND data_currency='SP0' ORDER BY sortorder ASC";
    $result1_max = $conn->query($sql_max);
    while($row_rinfo = $result1_max->fetch_assoc()) {
      $maxvalue = $row_rinfo['lvl']+1;
    }

    // $maxvalue=$result1_max->num_rows;
    //echo '<br>total child number : '.$maxvalue.'<br>';
    if(iconv_strlen (trim($maxvalue))>1){
      $maxvalue1=$maxvalue;
    }
    else{
      $maxvalue1=trim("0".$maxvalue);
    }

    if(($pgmid_len/2)==1)
      $l2 = $maxvalue1;
    if(($pgmid_len/2)==2)
      $l3 = $maxvalue1;
    if(($pgmid_len/2)==3)
      $l4 = $maxvalue1;
    if(($pgmid_len/2)==4)
      $l5 = $maxvalue1;
    $newsortid=$pgmid.$maxvalue1.str_repeat("0",10-$pgmid_len-2);
    //echo "New sort id : ".$newsortid."<br>";

    //insert into toc table;
    $sql5="INSERT INTO toc_$study_name (data_currency ,study,sortorder,l1,l2,l3,l4,l5,section,type,title,pgmloc,pgmname,outno,outloc,population,outname,logname,study_type,therapeutic_area,primary_endpoint,secondary_endpoint,study_medication,phase,tlfnum,shell_name".$param_list_val_name.")
     VALUES ('SP0','$study_name','$newsortid','$l1','$l2','$l3','$l4','$l5',' ','$ne_pgtyp','$ne_title','$ne_pgloc','$ne_pgmnam','$ne_outnum','$ne_outloc','$population','$ne_outnam','$ne_lognam','$study_type','$therapeutic_area','$primary_endpoint','$secondary_endpoint','$study_medication','$phase','$tlfnum','$shell_name'".$param_list_val_value.") ";
    $result5 = $conn->query($sql5);
    if($result5){$text1="iNSERT into toc table --> SUCCESS";}else{$text1= "Failed to insert into toc table";}

    //insert into toc status table
    date_default_timezone_set('Asia/Dhaka');
    $today = date("Y-m-d H:i:s");
    $sql6="INSERT INTO toc_status_$study_name (data_currency , study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('SP0','$study_name','$newsortid','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
    $result6 = $conn->query($sql6);
    if($result6){$text2 = "iNSERT into toc status Table --> SUCCESS<BR>";}else{$text2 = " Failed to insert into toc status table.<br>";}
  }
  else{
    //echo"<h1>this is a entry</h1><br>";
    if($pgmid_len>2){
      $masterid=substr($pgmid,0,$pgmid_len-2);
    }
    else{
      $masterid=$pgmid;
    }
    //echo "Selected pgmid : ".$pgmid."<br>";
    //echo "masterid for Selected pgmid : ".$masterid."<br>";
    $result=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0' ");
    // print_r($result);
    if($result->num_rows > 0){
      while($row = $result->fetch_assoc()) {
        $l1 = $row['l1'];
        $l2 = $row['l2'];
        $l3 = $row['l3'];
        $l4 = $row['l4'];
        $l5 = $row['l5'];
        //echo "l1: ".$l1." l2: ".$l2." l3:".$l3." l4:".$l4." l5:".$l5."<br>";

        $sql2="SELECT * FROM (SELECT *
                            FROM toc_$study_name
                            WHERE sortorder >=$tosearchid) AS a WHERE sortorder LIKE '$masterid%' AND data_currency='SP0' ORDER BY sortorder DESC";
        $result2 = $conn->query($sql2);
        while($row = $result2->fetch_assoc()) {
          $lblpgm=$pgmid_len/2;
          $temp="l$lblpgm";
          $temp1=$row["$temp"]+1;
          if(iconv_strlen (trim($temp1))==0){
            $temp0=trim("00".$temp1);
          }
          else if(iconv_strlen (trim($temp1))==1){
            $temp0=trim("0".$temp1);
          }else {
            $temp0=trim($temp1);
          }

          $sortorder=$row["sortorder"];
          $newsort=substr_replace($sortorder,$temp0,$pgmid_len-2,2);
          //echo "old sortorder: ".$sortorder.">>> new sortorder :".$newsort." new l$lblpgm : $temp0<br>";

          //update toc status table
          $sql4="UPDATE toc_status_$study_name SET sortorder='$newsort' WHERE sortorder ='$sortorder' AND data_currency='SP0' ";
          $result4 = $conn->query($sql4);
          //if($result4){$text3 = "toc status table updated!!<BR>";}else{$text3 = "update failed(toc status table)<BR>";}

          //update toc table
          $sql3="UPDATE toc_$study_name SET sortorder='$newsort', l$lblpgm='$temp0' WHERE sortorder ='$sortorder' AND data_currency='SP0' ";
          $result3 = $conn->query($sql3);
          //if($result3){$text4 = "toc table updated!!<BR>";}else{$text4 = "update failed(toc table)<BR>";}

        }

        //insert into toc table;
        $sql5="INSERT INTO toc_$study_name (data_currency , study,sortorder,l1,l2,l3,l4,l5,section,type,title,pgmloc,pgmname,outno,outloc,population,outname,logname,study_type,therapeutic_area,primary_endpoint,secondary_endpoint,study_medication,phase,tlfnum,shell_name".$param_list_val_name.")
         VALUES ('SP0','$study_name','$tosearchid','$l1','$l2','$l3','$l4','$l5',' ','$ne_pgtyp','$ne_title','$ne_pgloc','$ne_pgmnam','$ne_outnum','$ne_outloc','$population','$ne_outnam','$ne_lognam','$study_type','$therapeutic_area','$primary_endpoint','$secondary_endpoint','$study_medication','$phase','$tlfnum','$shell_name'".$param_list_val_value.") ";
        $result5 = $conn->query($sql5);
        if($result5){$text5 =  "iNSERT into toc table --> SUCCESS<BR>";}else{$text5 =  "Failed to insert into toc table<BR>";}

        //insert into toc status table
        date_default_timezone_set('Asia/Dhaka');
        $today = date("Y-m-d H:i:s");
        $sql6="INSERT INTO toc_status_$study_name (data_currency , study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('SP0','$study_name','$tosearchid','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
        $result6 = $conn->query($sql6);
        if($result6){$text6 =  "iNSERT into toc status Table --> SUCCESS<BR>";}else{$text6 =  " Failed to insert into toc status table.<br>";}
         //echo "not last node <--";
      }
    }
    else{
      // echo "last node added <br>";
      // $section="demo section";
      $l1=substr($tosearchid,0,2);
      $l2=substr($tosearchid,2,2);
      $l3=substr($tosearchid,4,2);
      $l4=substr($tosearchid,6,2);
      $l5=substr($tosearchid,8,2);
      //echo "l1: ".$l1." l2: ".$l2." l3:".$l3." l4:".$l4." l5:".$l5;

      //insert into toc table;
      $sql5="INSERT INTO toc_$study_name (data_currency , study,sortorder,l1,l2,l3,l4,l5,section,type,title,pgmloc,pgmname,outno,outloc,population,outname,logname,study_type,therapeutic_area,primary_endpoint,secondary_endpoint,study_medication,phase,tlfnum,shell_name".$param_list_val_name.")
       VALUES ('SP0','$study_name','$tosearchid','$l1','$l2','$l3','$l4','$l5',' ','$ne_pgtyp','$ne_title','$ne_pgloc','$ne_pgmnam','$ne_outnum','$ne_outloc','$population','$ne_outnam','$ne_lognam','$study_type','$therapeutic_area','$primary_endpoint','$secondary_endpoint','$study_medication','$phase','$tlfnum','$shell_name'".$param_list_val_value.") ";
      $result5 = $conn->query($sql5);
      if($result5){$text7 =  "iNSERT into toc table --> SUCCESS<BR>";}else{$text7 =  "Failed to insert into toc table<BR>";}

      //insert into toc status table
      date_default_timezone_set('Asia/Dhaka');
      $today = date("Y-m-d H:i:s");
      $sql6="INSERT INTO toc_status_$study_name (data_currency , study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('SP0','$study_name','$tosearchid','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
      $result6 = $conn->query($sql6);
      if($result6){$text8 =  "iNSERT into toc status Table --> SUCCESS<BR>";}else{$text8 =  " Failed to insert into toc status table.<br>";}
    }
  }

}

$success="1";


$data = array('success' => $success );

echo json_encode($data);


?>