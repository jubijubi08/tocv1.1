<?php 
  include("../include/connect.php");

  /** Error reporting */
  error_reporting(E_ALL);
  ini_set('display_errors', TRUE);
  ini_set('display_startup_errors', TRUE);
  include("../include/Classes/PHPExcel.php");


  session_start();
  date_default_timezone_set('Asia/Dhaka');
  $today = date("Y-m-d-H-i-s");
  $study_name=$_SESSION["study"];
  $username = $_SESSION['login_user'];
  $dc_db_val='SP'.$_SESSION["dc_selector"];
  $op=$_POST['op'];
  // echo $op;
  
  
  $result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
  while($row = $result44->fetch_assoc()) {
      $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
  }
 $baseloc = $file_server.'/'.$study_name;
 //echo $baseloc;

  $files = glob('./temp/pdf/*'); // get all file names
  foreach($files as $file){ // iterate files
    if(is_file($file)){
      unlink($file);       // delete file
      //print_r($file);
    }
  }
  
//Creating Location for PDF backup;
$folder_name = 'lst/pdf/';
$pdf_loc = $file_server.$study_name.DIRECTORY_SEPARATOR.$folder_name;
  
if (!file_exists($pdf_loc)) {
	mkdir($pdf_loc, 0777, true);
	//echo "not exist >> created now";
}else{
	//echo "exist";
}


//Copying PDF file to the lst/pdf directory;
function copyToDir($pattern, $dir){
    foreach (glob($pattern) as $file){
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);
        }
    }
}


  //create pdf with group of table/listing
if($op=="1"){
	 $grp = $_POST['grp'];
   // echo "$grp";
   // echo "inside op 1";
    if($grp==1){
        $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.outstat!=0 AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
        $result = $conn->query($sql);
        // print_r($result);
    }

    if($grp==2){
        $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.outstat=2 AND x.data_currency='$dc_db_val'  ORDER BY x.sortorder  ";
        $result = $conn->query($sql);
        // print_r($result);
    }

    if($grp==3){
        $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.outstat=1  AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
        $result = $conn->query($sql);
        // print_r($result);
    }

    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator($username)
        ->setLastModifiedBy($username)
        ->setTitle("Office CSV Document")
        ->setSubject("Office CSV Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("CSV file");

    //need to check and update status column when first time inserted in to toc tree
    //select existing validated program
    //update toc tree according to the current status
    //then select all information from toc table

    $columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
      "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
      "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
      "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");

    $rowCount = 1;
    // fetch result set column information
    $finfo = mysqli_fetch_fields($result);

    $columnlenght = 0;
    // foreach ($finfo as $val) {
    // // set column header values
    //     $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
    // }
    
    // Add column headers
    $objPHPExcel->getActiveSheet()
          ->setCellValue('A1', 'tabnum')
          ->setCellValue('B1', 'file_sht')
          ->setCellValue('C1', 'char')
          ->setCellValue('D1', 'file_lng')
          ->setCellValue('E1', 'title')
          ;


    $objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

    $rowCount++;
    // Iterate through each result from the SQL query in turn
    // We fetch each database result row into $row in turn

	while ($row = mysqli_fetch_array($result)) {
		   for ($i = 0; $i < mysqli_num_fields($result); $i++) {
			   $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
		   }
		   $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $rowCount-1);
		   $rowCount++;
	   }

    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('output_list');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('../sas/output_list.csv');
	

	//Macro Run for Multiple lsts to single lst;
	
	//Macro Run & Call for Multiple lsts to single lst;
	
    $myFile ="../sas/lsts2lst.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");

	  $stringData1 = '%include "C:\xampp\htdocs\toc\sas\hfs_lst.sas";';
	
    $stringData2 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $stringData3='Filename hfs_inf "C:\xampp\htdocs\toc\sas\output_list.csv";';

    $stringData4='Filename txt_outf "C:\xampp\htdocs\toc\sas\_results.lst";';

    $stringData5='%hfs_lst(sort=Y, ovallnum=N, autoind=N,line=150, text=%STR(aa.bb.cc.dd));';


    $stringData=$stringData1.$stringData2.$stringData3.$stringData4.$stringData5;
    fwrite($fh, $stringData);
    fclose($fh);
		
    $pgmfileloc = 'C:\xampp\htdocs\toc\sas/lsts2lst.sas';
    $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
    exec($command, $output); 
	
	
	
	//Macro Run and Call for Single lst to PDF;
    $myFile ="../sas/_lst2rtf.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");
	
	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\convert_rtf.sas";';

    $_stringData1 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $_stringData2='%lst2rtf(in  = '. '"' .' C:\xampp\htdocs\toc\sas\_results.lst '. '"' .' ,out = '. '"' .'C:\xampp\htdocs\toc\sas\\'.$today.'_results.rtf'. '"' .'';

    $_stringData3=',linesize=174,fontsize= 8,margint= 2,marginb= 1,marginl= 1';
	  $_stringData4=',marginr= 1,papersize= A4,orient= L,titleline=1,tlineto=2,doctitle=);';
	

    $stringData_1=$stringData1.$_stringData1.$_stringData2.$_stringData3.$_stringData4;
    fwrite($fh, $stringData_1);
    fclose($fh);
	
   
    //$dlink='./temp/pdf/'.$study_name.'_'.$today.'.rtf';
	
	   $dlink='./sas/'.$today.'_results.rtf';
	
    $pgmfileloc3 = 'C:\xampp\htdocs\toc\sas/_lst2rtf.sas';
    $command3= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc3.'"';
    exec($command3, $output); 
	
    $result = '{"success":1,"dlink":"'.$dlink.'"}';
    header('Content-type: application/json;');
    echo $result;
	
	//copyToDir('../temp/pdf/'.$study_name.'_'.$today.'.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/pdf/');
	
	copyToDir('../sas/'.$today.'_results.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/rtf/');
	
		
}

//create pdf with table/listing under section
if($op=="2"){
  $grp = $_POST['grp'];
  // echo $grp;
  // echo "inside op 2";
    $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND x.sortorder IN (".$grp.") AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
    $result = $conn->query($sql);
    // print_r($result);

// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator($username)
        ->setLastModifiedBy($username)
        ->setTitle("Office CSV Document")
        ->setSubject("Office CSV Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("CSV file");

    //need to check and update status column when first time inserted in to toc tree
    //select existing validated program
    //update toc tree according to the current status
    //then select all information from toc table

    $columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
      "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
      "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
      "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");

    $rowCount = 1;
    // fetch result set column information
    $finfo = mysqli_fetch_fields($result);

    $columnlenght = 0;
    // foreach ($finfo as $val) {
    // // set column header values
    //     $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
    // }
    
    // Add column headers
    $objPHPExcel->getActiveSheet()
          ->setCellValue('A1', 'tabnum')
          ->setCellValue('B1', 'file_sht')
          ->setCellValue('C1', 'char')
          ->setCellValue('D1', 'file_lng')
          ->setCellValue('E1', 'title')
          ;


    $objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

    $rowCount++;
    // Iterate through each result from the SQL query in turn
    // We fetch each database result row into $row in turn

	while ($row = mysqli_fetch_array($result)) {
		   for ($i = 0; $i < mysqli_num_fields($result); $i++) {
			   $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
		   }
		   $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $rowCount-1);
		   $rowCount++;
	   }


    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('output_list');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('../sas/output_list.csv');

	

	//Macro Run for Multiple lsts to single lst;
	
    $stdat = 'C:\xampp\htdocs\toc\sas/hfs_lst.sas';
    $command1= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$stdat.'"';
    exec($command1, $output);        
	

	//Macro Call for Multiple lsts to single lst;
	
    $myFile ="../sas/lsts2lst.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");

	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\hfs_lst.sas";';
	
    $stringData2 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $stringData3='Filename hfs_inf "C:\xampp\htdocs\toc\sas\output_list.csv";';

    $stringData4='Filename txt_outf "C:\xampp\htdocs\toc\sas\_results.lst";';

    $stringData5='%hfs_lst(sort=Y, ovallnum=N, autoind=N,line=150, text=%STR(aa.bb.cc.dd));';


    $stringData=$stringData1.$stringData2.$stringData3.$stringData4.$stringData5;
    fwrite($fh, $stringData);
    fclose($fh);
		
    $pgmfileloc = 'C:\xampp\htdocs\toc\sas/lsts2lst.sas';
    $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
    exec($command, $output); 
	
	
	
	//Macro Run for Single LST to RTF;
	
    $pgmfileloc2 = 'C:\xampp\htdocs\toc\sas/convert_rtf.sas';
    $command2= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc2.'"';
    exec($command2, $output);   

	
	//Macro Call for Single LST to RTF;
    $myFile ="../sas/_lst2rtf.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");
	
	  $stringData1 = '%include "C:\xampp\htdocs\toc\sas\convert_rtf.sas";';

    $_stringData1 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $_stringData2='%lst2rtf(in  = '. '"' .' C:\xampp\htdocs\toc\sas\_results.lst '. '"' .' ,out = '. '"' .'C:\xampp\htdocs\toc\sas\\'.$today.'_results.rtf'. '"' .'';

    $_stringData3=',linesize=174,fontsize= 8,margint= 2,marginb= 1,marginl= 1';

	  $_stringData4=',marginr= 1,papersize= A4,orient= L,titleline=1,tlineto=2,doctitle=);';
	

    $stringData_1=$stringData1.$_stringData1.$_stringData2.$_stringData3.$_stringData4;
    fwrite($fh, $stringData_1);
    fclose($fh);
	
    //$dlink='./temp/pdf/'.$study_name.'_'.$today.'.rtf';
	
	$dlink='./sas/'.$today.'_results.rtf';
	
    $pgmfileloc3 = 'C:\xampp\htdocs\toc\sas/_lst2rtf.sas';
    $command3= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc3.'"';
    exec($command3, $output); 
	
    $result = '{"success":1,"dlink":"'.$dlink.'"}';
    header('Content-type: application/json;');
    echo $result;
	
	//copyToDir('../temp/pdf/'.$study_name.'_'.$today.'.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/pdf/');
	
	copyToDir('../sas/'.$today.'_results.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/rtf/');
}

//create table individual from entry
if($op=="3"){
    $grp = $_POST['grp'];
    //echo $grp."<br>";
    $removesemi=str_replace(",","",$grp);
    //echo $removesemi."<br>";
    $idcount= substr_count($grp, ',');
    //echo $idcount."<br>";
    
    $cond="";

    for ($i=0;$i<=$idcount;$i++){ 
       if(substr($removesemi,8,2)=="00"){
          $temp=substr($removesemi, $i*10, 8);  
       }
       if (substr($removesemi,6,2)=="00") {
          $temp=substr($removesemi, $i*10, 6); 
       }
       if (substr($removesemi,4,2)=="00") {
          $temp=substr($removesemi, $i*10, 4); 
       }
       if (substr($removesemi,2,2)=="00") {
          $temp=substr($removesemi, $i*10, 2); 
       }
       //echo $temp."<br>";
       $cond.="x.sortorder LIKE '".$temp."%' OR ";
       //echo $cond."<br>";
    }
    $cond=substr($cond,0,-3);
    //echo "condition will be : ".$cond."<BR>";

    $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.outstat!=0 AND ($cond) AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
    $result = $conn->query($sql);
    // print_r($result);   

// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator($username)
        ->setLastModifiedBy($username)
        ->setTitle("Office CSV Document")
        ->setSubject("Office CSV Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("CSV file");

    //need to check and update status column when first time inserted in to toc tree
    //select existing validated program
    //update toc tree according to the current status
    //then select all information from toc table

    $columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
      "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
      "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
      "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");

    $rowCount = 1;
    // fetch result set column information
    $finfo = mysqli_fetch_fields($result);

    $columnlenght = 0;
    // foreach ($finfo as $val) {
    // // set column header values
    //     $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
    // }
    
    // Add column headers
    $objPHPExcel->getActiveSheet()
          ->setCellValue('A1', 'tabnum')
          ->setCellValue('B1', 'file_sht')
          ->setCellValue('C1', 'char')
          ->setCellValue('D1', 'file_lng')
          ->setCellValue('E1', 'title')
          ;


    $objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

    $rowCount++;
    // Iterate through each result from the SQL query in turn
    // We fetch each database result row into $row in turn

	while ($row = mysqli_fetch_array($result)) {
		   for ($i = 0; $i < mysqli_num_fields($result); $i++) {
			   $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
		   }
		   $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $rowCount-1);
		   $rowCount++;
	   }


    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('output_list');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('../sas/output_list.csv');


	//Macro Run for Multiple lsts to single lst;
	
    $stdat = 'C:\xampp\htdocs\toc\sas/hfs_lst.sas';
    $command1= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$stdat.'"';
    exec($command1, $output);        
	

	//Macro Call for Multiple lsts to single lst;
	
    $myFile ="../sas/lsts2lst.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");

	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\hfs_lst.sas";';
	
    $stringData2 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $stringData3='Filename hfs_inf "C:\xampp\htdocs\toc\sas\output_list.csv";';

    $stringData4='Filename txt_outf "C:\xampp\htdocs\toc\sas\_results.lst";';

    $stringData5='%hfs_lst(sort=Y, ovallnum=N, autoind=N,line=150, text=%STR(aa.bb.cc.dd));';


    $stringData=$stringData1.$stringData2.$stringData3.$stringData4.$stringData5;
    fwrite($fh, $stringData);
    fclose($fh);
		
    $pgmfileloc = 'C:\xampp\htdocs\toc\sas/lsts2lst.sas';
    $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
    exec($command, $output); 
	
	
	
	//Macro Run for Single lst to PDF;
	
    $pgmfileloc2 = 'C:\xampp\htdocs\toc\sas/convert_rtf.sas';
    $command2= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc2.'"';
    exec($command2, $output);   

	
	//Macro Call for Single lst to PDF;
    $myFile ="../sas/_lst2rtf.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");
	
	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\convert_rtf.sas";';

    $_stringData1 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $_stringData2='%lst2rtf(in  = '. '"' .' C:\xampp\htdocs\toc\sas\_results.lst '. '"' .' ,out = '. '"' .'C:\xampp\htdocs\toc\sas\\'.$today.'_results.rtf'. '"' .'';


    $_stringData3=',linesize=174,fontsize= 8,margint= 2,marginb= 1,marginl= 1';

	  $_stringData4=',marginr= 1,papersize= A4,orient= L,titleline=1,tlineto=2,doctitle=);';
	

    $stringData_1=$stringData1.$_stringData1.$_stringData2.$_stringData3.$_stringData4;
    fwrite($fh, $stringData_1);
    fclose($fh);
	
    //$dlink='./temp/pdf/'.$study_name.'_'.$today.'.rtf';
	
	  $dlink='./sas/'.$today.'_results.rtf';
	
    $pgmfileloc3 = 'C:\xampp\htdocs\toc\sas/_lst2rtf.sas';
    $command3= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc3.'"';
    exec($command3, $output);

    $result = '{"success":1,"dlink":"'.$dlink.'"}';
    header('Content-type: application/json;');
    echo $result;
	
	//copyToDir('../temp/pdf/'.$study_name.'_'.$today.'.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/pdf/');
	
	copyToDir('../sas/'.$today.'_results.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/rtf/');

}

//create pdf from entry context menu
if($op=="4"){
    $grp = $_POST['grp'];
    //echo $grp."<br>";
  
    $removesemi=str_replace(",","",$grp);
    //echo $removesemi."<br>";
    $idcount= substr_count($grp, ',');
    //echo $idcount."<br>";
    
    $cond="";
    for ($i=0;$i<=$idcount;$i++){   
       $temp=substr($removesemi, $i*10, 10);
       //echo $temp."<br>";
       $cond.="x.sortorder LIKE '".$temp."%' OR ";
       //echo $cond."<br>";
    }
    $cond=substr($cond,0,-3);
    //echo "condition will be : ".$cond."<BR>";

    $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.outstat!=0 AND ($cond) AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
    $result = $conn->query($sql);
    // print_r($result); 


    // Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator($username)
        ->setLastModifiedBy($username)
        ->setTitle("Office CSV Document")
        ->setSubject("Office CSV Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("CSV file");

    //need to check and update status column when first time inserted in to toc tree
    //select existing validated program
    //update toc tree according to the current status
    //then select all information from toc table

    $columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
      "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
      "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
      "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");

    $rowCount = 1;
    // fetch result set column information
    $finfo = mysqli_fetch_fields($result);

    $columnlenght = 0;
    // foreach ($finfo as $val) {
    // // set column header values
    //     $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
    // }
    
    // Add column headers
    $objPHPExcel->getActiveSheet()
          ->setCellValue('A1', 'tabnum')
          ->setCellValue('B1', 'file_sht')
          ->setCellValue('C1', 'char')
          ->setCellValue('D1', 'file_lng')
          ->setCellValue('E1', 'title')
          ;


    $objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

    $rowCount++;
    // Iterate through each result from the SQL query in turn
    // We fetch each database result row into $row in turn

	while ($row = mysqli_fetch_array($result)) {
		   for ($i = 0; $i < mysqli_num_fields($result); $i++) {
			   $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
		   }
		   $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $rowCount-1);
		   $rowCount++;
	   }


    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('output_list');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('../sas/output_list.csv');


	//Macro Run for Multiple lsts to single lst;
	
    $stdat = 'C:\xampp\htdocs\toc\sas/hfs_lst.sas';
    $command1= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$stdat.'"';
    exec($command1, $output);        
	

	//Macro Call for Multiple lsts to single lst;
	
    $myFile ="../sas/lsts2lst.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");

	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\hfs_lst.sas";';
	
    $stringData2 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $stringData3='Filename hfs_inf "C:\xampp\htdocs\toc\sas\output_list.csv";';

    $stringData4='Filename txt_outf "C:\xampp\htdocs\toc\sas\_results.lst";';

    $stringData5='%hfs_lst(sort=Y, ovallnum=N, autoind=N,line=150, text=%STR(aa.bb.cc.dd));';


    $stringData=$stringData1.$stringData2.$stringData3.$stringData4.$stringData5;
    fwrite($fh, $stringData);
    fclose($fh);
		
    $pgmfileloc = 'C:\xampp\htdocs\toc\sas/lsts2lst.sas';
    $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
    exec($command, $output); 
	
	
	
	//Macro Run for Single lst to PDF;
	
    $pgmfileloc2 = 'C:\xampp\htdocs\toc\sas/convert_rtf.sas';
    $command2= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc2.'"';
    exec($command2, $output);   

	
	//Macro Call for Single lst to PDF;
    $myFile ="../sas/_lst2rtf.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");
	
	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\convert_rtf.sas";';

    $_stringData1 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $_stringData2='%lst2rtf(in  = '. '"' .' C:\xampp\htdocs\toc\sas\_results.lst '. '"' .' ,out = '. '"' .'C:\xampp\htdocs\toc\sas\\'.$today.'_results.rtf'. '"' .'';

    $_stringData3=',linesize=174,fontsize= 8,margint= 2,marginb= 1,marginl= 1';
	$_stringData4=',marginr= 1,papersize= A4,orient= L,titleline=1,tlineto=2,doctitle=);';
	

    $stringData_1=$stringData1.$_stringData1.$_stringData2.$_stringData3.$_stringData4;
    fwrite($fh, $stringData_1);
    fclose($fh);
	
    //$dlink='./temp/pdf/'.$study_name.'_'.$today.'.rtf';
	
	$dlink='./sas/'.$today.'_results.rtf';
	
    $pgmfileloc3 = 'C:\xampp\htdocs\toc\sas/_lst2rtf.sas';
    $command3= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc3.'"';
    exec($command3, $output);
	
    $result = '{"success":1,"dlink":"'.$dlink.'"}';
    header('Content-type: application/json;');
    echo $result;

	//copyToDir('../temp/pdf/'.$study_name.'_'.$today.'.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/pdf/');
	
	copyToDir('../sas/'.$today.'_results.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/rtf/');
	
}  

//create pdf individual from section context menu
if($op=="5"){
    $grp = $_POST['grp'];
    //echo $grp."<br>";
    $removesemi=str_replace(",","",$grp);
    //echo $removesemi."<br>";
    $idcount= substr_count($grp, ',');
    //echo $idcount."<br>";
    
    $cond="";
    for ($i=0;$i<=$idcount;$i++){   
       $temp=substr($removesemi, $i*10, 10);
       //echo $temp."<br>";
       $cond.="x.sortorder LIKE '".$temp."%' OR ";
       //echo $cond."<br>";
    }
    $cond=substr($cond,0,-3);
    //echo "condition will be : ".$cond."<BR>";

    $sql="SELECT tlfnum,outname,type,CONCAT ('$baseloc','/',outloc,outname) as file_lng FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.data_currency=y.data_currency AND x.sortorder=y.sortorder AND y.outstat!=0 AND ($cond) AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
    $result = $conn->query($sql);
    // print_r($result);   

// Create new PHPExcel object
    $objPHPExcel = new PHPExcel();

    // Set document properties
    $objPHPExcel->getProperties()->setCreator($username)
        ->setLastModifiedBy($username)
        ->setTitle("Office CSV Document")
        ->setSubject("Office CSV Document")
        ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
        ->setKeywords("office 2007 openxml php")
        ->setCategory("CSV file");

    //need to check and update status column when first time inserted in to toc tree
    //select existing validated program
    //update toc tree according to the current status
    //then select all information from toc table

    $columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
      "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
      "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
      "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");

    $rowCount = 1;
    // fetch result set column information
    $finfo = mysqli_fetch_fields($result);

    $columnlenght = 0;
    // foreach ($finfo as $val) {
    // // set column header values
    //     $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
    // }
    
    // Add column headers
    $objPHPExcel->getActiveSheet()
          ->setCellValue('A1', 'tabnum')
          ->setCellValue('B1', 'file_sht')
          ->setCellValue('C1', 'char')
          ->setCellValue('D1', 'file_lng')
          ->setCellValue('E1', 'title')
          ;


    $objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

    $rowCount++;
    // Iterate through each result from the SQL query in turn
    // We fetch each database result row into $row in turn

	while ($row = mysqli_fetch_array($result)) {
		   for ($i = 0; $i < mysqli_num_fields($result); $i++) {
			   $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
		   }
		   $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, $rowCount-1);
		   $rowCount++;
	   }


    // Rename worksheet
    $objPHPExcel->getActiveSheet()->setTitle('output_list');

    // Set active sheet index to the first sheet, so Excel opens this as the first sheet
    $objPHPExcel->setActiveSheetIndex(0);
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
    $objWriter->save('../sas/output_list.csv');


	//Macro Run for Multiple lsts to single lst;
	
    $stdat = 'C:\xampp\htdocs\toc\sas/hfs_lst.sas';
    $command1= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$stdat.'"';
    exec($command1, $output);        
	

	//Macro Call for Multiple lsts to single lst;
	
    $myFile ="../sas/lsts2lst.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");

	$stringData1 = '%include "C:\xampp\htdocs\toc\sas\hfs_lst.sas";';
	
    $stringData2 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $stringData3='Filename hfs_inf "C:\xampp\htdocs\toc\sas\output_list.csv";';

    $stringData4='Filename txt_outf "C:\xampp\htdocs\toc\sas\_results.lst";';

    $stringData5='%hfs_lst(sort=Y, ovallnum=N, autoind=N,line=150, text=%STR(aa.bb.cc.dd));';


    $stringData=$stringData1.$stringData2.$stringData3.$stringData4.$stringData5;
    fwrite($fh, $stringData);
    fclose($fh);
		
    $pgmfileloc = 'C:\xampp\htdocs\toc\sas/lsts2lst.sas';
    $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
    exec($command, $output); 
	
	
	
	//Macro Run for Single lst to PDF;
	
    $pgmfileloc2 = 'C:\xampp\htdocs\toc\sas/convert_rtf.sas';
    $command2= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc2.'"';
    exec($command2, $output);   

	
	//Macro Call for Single lst to PDF;
    $myFile ="../sas/_lst2rtf.sas";
    $fh = fopen($myFile, 'w') or die("can't open file");
	
	  $stringData1 = '%include "C:\xampp\htdocs\toc\sas\convert_rtf.sas";';

    $_stringData1 ='OPTIONS MACROGEN SYMBOLGEN MPRINT MLOGIC MTRACE;';

    $_stringData2='%lst2rtf(in  = '. '"' .' C:\xampp\htdocs\toc\sas\_results.lst '. '"' .' ,out = '. '"' .'C:\xampp\htdocs\toc\sas\\'.$today.'_results.rtf'. '"' .'';

    $_stringData3=',linesize=174,fontsize= 8,margint= 2,marginb= 1,marginl= 1';

	  $_stringData4=',marginr= 1,papersize= A4,orient= L,titleline=1,tlineto=2,doctitle=);';
	

    $stringData_1=$stringData1.$_stringData1.$_stringData2.$_stringData3.$_stringData4;
    fwrite($fh, $stringData_1);
    fclose($fh);
	
    //$dlink='./temp/pdf/'.$study_name.'_'.$today.'.rtf';
	
	  $dlink='./sas/'.$today.'_results.rtf';
	
    $pgmfileloc3 = 'C:\xampp\htdocs\toc\sas/_lst2rtf.sas';
    $command3= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc3.'"';
    exec($command3, $output); 
	
    $result = '{"success":1,"dlink":"'.$dlink.'"}';
    header('Content-type: application/json;');
    echo $result;
	
	//copyToDir('../temp/pdf/'.$study_name.'_'.$today.'.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/pdf/');
	
	copyToDir('../sas/'.$today.'_results.rtf', $file_server.$study_name.DIRECTORY_SEPARATOR.'lst/rtf/');
	
}  

?>