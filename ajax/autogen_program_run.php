<?php
ini_set('max_execution_time', 300);
include("../include/connect.php");
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
include("../include/Classes/PHPExcel.php");

session_start();
$study_name=$_SESSION["study"];

//retive study location
$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}
$username = $_SESSION['login_user'];

//Copying all log file to the pgm directory;
function copyToDir($pattern, $dir){
    foreach (glob($pattern) as $file){
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);
            unlink($file);
        }
    }
}

//echo "[inside ajax:]";

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator($username)
    ->setLastModifiedBy($username)
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");

//need to check and update status column when first time inserted in to toc tree
//select existing validated program
//update toc tree according to the current status
//then select all information from toc table


$result00=$conn->query("select * from toc_$study_name WHERE data_currency='SP0' AND section ='' ");
while($row = $result00->fetch_assoc()) {
    $pgmname = $row['pgmname'];
   
    if($pgmname==''){         
        $update_status_in_toc = $conn->query("UPDATE toc_$study_name SET status = 'v' where pgmname = '$pgmname' AND section = '' AND data_currency='SP0'");
    }
}

$result=$conn->query("select * from toc_$study_name WHERE data_currency='SP0'");


$columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
	"AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
	"BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
	"CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");

$rowCount = 1;
// fetch result set column information
$finfo = mysqli_fetch_fields($result);

$columnlenght = 0;
foreach ($finfo as $val) {
// set column header values
    $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
}
    $objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

$rowCount++;
// Iterate through each result from the SQL query in turn
// We fetch each database result row into $row in turn

while ($row = mysqli_fetch_array($result)) {
    for ($i = 0; $i < mysqli_num_fields($result); $i++) {
        $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
    }
    $rowCount++;
}

// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('database');

// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('../sas/database.xlsx');


$myFile ="../sas/temp_autogen.sas";
$fh = fopen($myFile, 'w') or die("can't open file");

$stringData1 ='%INCLUDE "C:\xampp\htdocs\toc\sas\dynamic.sas";';
$stringData2="
		 %dynamic(_author=".$username.",
         _customer=Customer,
		 _study=".$study_name.",
		 _desc=Describtion,
		 _inxlspath=C:\\xampp\\htdocs\\toc\\sas\\database.xlsx,
		 _outpgmpath =".$file_server.$study_name."\ ,
		 _overwrite=N);
    ";
$stringData = $stringData1.$stringData2;
fwrite($fh, $stringData);
fclose($fh);


$pgmfileloc = '../sas/temp_autogen.sas';
$loglocfile = '../sas/temp_autogen.log';
//Execute SAS program
$command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
// echo "<script> console.log(".$command.") </script>";
exec($command, $output);
copyToDir('../ajax/*.log', '../sas/');


$file_handler = fopen($loglocfile, "r")
or die ("Can't open the File.");

$err = 0;
$war = 0;
$listpgm = "";
$pgname_from_log= "";
$pgname_from_log_all = array();
while (!feof($file_handler)) {
    $dataline = fgets($file_handler);


    if (strpos($dataline,'AUTOGEN') !== false) {
        $listpgm=strpos($dataline,'AUTOGEN');
        if($listpgm == "0"){
            $pgname_from_log = substr($dataline,8);
        }
        //$pgname_from_log_all = $pgname_from_log_all.$pgname_from_log;
        array_push($pgname_from_log_all,$pgname_from_log);
    }

    if (strpos($dataline,'ERROR') !== false) {
        $err++;
    }
    if (strpos($dataline,'WARNING') !== false) {
        $war++;
    }
}

//echo "Number of error in dymaic sas program: ".$err;

$unique_pgname_from_log = array_keys(array_flip($pgname_from_log_all));
//echo "[List of programs that come from log]";
//print_r($unique_pgname_from_log);

$newly_created_pgm = $conn->query("SELECT distinct pgmname,pgmloc FROM toc_$study_name WHERE status != 'v' AND pgmname !='' AND data_currency='SP0'");
//print_r($newly_created_pgm);
//if (mysqli_num_rows($val_pgm2) > 0){
//
//    while($row1 = $val_pgm2->fetch_assoc()) {
//
//        $v_pgm2 = $row1['pgmname'];
//        echo "\n [List of programs that have status V in toc table] ".$v_pgm2 ;
//        for ($i = 0 ; $i < count($unique_pgname_from_log); $i++){
//            $pgm_from_autogen = trim($unique_pgname_from_log[$i]);
//            echo "\n [List of programs that come from log, inside loop] ".$pgm_from_autogen ;
//            if(trim($pgm_from_autogen) == trim($v_pgm2)){
//                unset($unique_pgname_from_log[$i]);
//            }
//
//        }
//    }
//}

//for ($i = 1 ; $i < count($unique_pgname_from_log); $i++){
//    $pgm_from_autogen = trim($unique_pgname_from_log[$i]);
//    echo "\n [List of programs that come from log] ".$pgm_from_autogen ;
//
//    if (mysqli_num_rows($val_pgm2) > 0){
//
//        while($row1 = $val_pgm2->fetch_assoc()) {
//
//            $v_pgm2 = $row1['pgmname'];
//            echo "\n [List of programs that have status V in toc table] ".$v_pgm2 ;
//
//                if(trim($pgm_from_autogen) == trim($v_pgm2)){
//                    unset($unique_pgname_from_log[$i]);
//                    print_r($unique_pgname_from_log);
//                }
//
//        }
//    }
//
//}

function copyToDir2($pattern, $dir, $study_name,$pgmname,$file_server,$file_loc){
    foreach (glob($pattern) as $file)
    {
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);
            date_default_timezone_set('Asia/Dhaka');
            $datetime=date("Y_m_d_H_i_s");
            $sepdotfrmfile=substr_replace($file," ",strripos($file,$pgmname));
            //rename('study/'.$study_name.'/backup/'.$pgmname, 'study/'.$study_name.'/backup/'.$datetime.'_'.$pgmname);
            rename(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.DIRECTORY_SEPARATOR.$datetime.'_'.$pgmname));
        }
    }
}

$unique_pgname_from_log = array_keys(array_flip($unique_pgname_from_log));
// echo "[List of programs that created actually]";
// print_r($unique_pgname_from_log);
////Updating output date;
//print_r( $unique_pgname_from_log);
date_default_timezone_set('Asia/Dhaka');
$today = date("Y-m-d H:i:s");
$datetime=date("Y_m_d_H_i_s");
$result1 = "";
if(mysqli_num_rows($newly_created_pgm) > 0){
    while($row = $newly_created_pgm->fetch_assoc()) {
        $pgm_from_autogen = $row['pgmname'];
        $pgmloc = $row['pgmloc'];
        //echo $pgm_from_autogen;

       $update_status = "UPDATE toc_status_$study_name SET pgmstat = 1 , pdate_1 = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname ='$pgm_from_autogen' AND data_currency='SP0') AND data_currency='SP0'";
       $conn->query($update_status);
        
        
        $filelink=$datetime.'_'.$pgm_from_autogen;
        //echo $stat;
        //$status="Generated by System";
        $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
                VALUES ('0000000000','$pgm_from_autogen','$today','$username','Generated by System','In Development','$filelink','SP0')");

        //Copying current pgm file to the pgm/history directory;

        //copyToDir('study/'.$study_name.'/'.$pgmloc, 'study/'.$study_name.'/backup/',$study_name,$pgmname);
        //$file_loc = substr_replace($pgmloc,'',strripos($pgmloc,"/")+1);
        $file_loc = $pgmloc;
        //echo $file_loc;
        $pgmname = $pgm_from_autogen;
        copyToDir2(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$pgmloc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc),$study_name,$pgmname,$file_server,$file_loc);


        //select statsu of the outputs and prgrams for legend update
        $sql_status=$conn->query("SELECT COUNT(*) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='SP0'");

        while($row = $sql_status->fetch_assoc()) {
            $totalpgm = $row['totalpgm'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x WHERE pgmstat = 0 ");

        while($row = $sql_status->fetch_assoc()) {
            $nopgm = $row['nopgm'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x  WHERE pgmstat = 1 ");

        while($row = $sql_status->fetch_assoc()) {
            $indevpgm = $row['indevpgm'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x WHERE pgmstat = 2");

        while($row = $sql_status->fetch_assoc()) {
            $tvalpgm = $row['tvalpgm'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x WHERE pgmstat = 3");

        while($row = $sql_status->fetch_assoc()) {
            $valpgm = $row['valpgm'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x WHERE outstat = 0 ");

        while($row = $sql_status->fetch_assoc()) {
            $nout = $row['nout'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x WHERE outstat = 1");

        while($row = $sql_status->fetch_assoc()) {
            $oout = $row['oout'];
        }
        $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='' AND a.data_currency='SP0') as x WHERE outstat = 2");

        while($row = $sql_status->fetch_assoc()) {
            $cout = $row['cout'];
        }

        $outstat=0;
        $result=$conn->query("SELECT outstat FROM toc_status_$study_name WHERE sortorder = (SELECT sortorder FROM toc_$study_name WHERE pgmname ='$pgm_from_autogen' AND data_currency='SP0' LIMIT 1) AND data_currency='SP0' ");
        while ($row=$result->fetch_assoc()) {
            $outstat=$row['outstat'];
        }
        if($outstat!=0){
            $outstat=1;
            $result=$conn->query("UPDATE toc_status_$study_name SET outstat = 1 , odate_0 = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgm_from_autogen' AND data_currency='SP0') AND data_currency='SP0'");
        }

        $pgmstat=1;
        //$outstat=0;

        $result10 = $conn->query("SELECT * FROM toc_$study_name WHERE data_currency='SP0' ORDER BY sortorder ASC");
        $treeorder=0;
        $dtreeId="0";
        while($row = $result10->fetch_assoc()) {
            $treeorder=$treeorder+1;
            $pgmname2=$row["pgmname"];

            if($pgmname2 == $pgm_from_autogen){
                $dtreeId=$dtreeId.'#'.$treeorder;
            }
        }

        $result1.= '{"success":1,
                      "pgmname":"'. $pgm_from_autogen.'",
                      "nopgm":"'. $nopgm.'",
                      "indevpgm":"'. $indevpgm.'",
                      "tvalpgm":"'. $tvalpgm.'",
                      "valpgm":"'. $valpgm.'",
                      "nout":"'. $nout.'",
                      "oout":"'. $oout.'",
                      "cout":"'. $cout.'",
                      "pgmstat":"'. $pgmstat.'",
                      "outstat":"'. $outstat.'",
                      "dtreeId":"'. $dtreeId.'"},';
    }

}else{
        $result1.= '{"success":0,
                 "error":"null"}}';
}

$result1=substr($result1, 0, -1);
header('Content-type: application/json;');
echo "[".$result1."]";

?>