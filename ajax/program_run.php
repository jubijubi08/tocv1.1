<?php
ini_set('max_execution_time', 300);
include("../include/connect.php");

session_start();
$study_name=$_SESSION["study"];
$username = $_SESSION['login_user'];
$dc_id=$_SESSION["dc_selector"];

if(isset($_SESSION["filter_dc_selector"])){
  $dc2_id = $_SESSION["filter_dc_selector"];  
}
else {
  $dc2_id = $dc_id;
}


$dc_db_val2="'SP".$dc_id."', 'SP".$dc2_id."'";

$op=$_POST['op'];//op=1 2 3
$dc_val=$_POST['dc_val'];
$dc_text=$_POST['dc_text'];

date_default_timezone_set('Asia/Dhaka');
$datetime=date("Y_m_d_H_i_s");
$today = date("Y-m-d H:i:s");

$result444=$conn->query("SELECT DISTINCT id,snap_name FROM snap_$study_name WHERE id = '$dc_val' ");
while($row = $result444->fetch_assoc()) {
  $dc_db_val='SP'.$row['id'];
  if ($row['id']==0){
      $snap_name="";
  }else{
      $snap_name=$row['snap_name'];
  }

}


//retrive study location
$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '$study_name' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

//Copying all log file to the pgm directory;
function copyToDir($pattern, $dir){
    foreach (glob($pattern) as $file){
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);
            unlink($file);
        }
    }
}

//Copying all log file to the pgm directory;
function copyToDir2($pattern, $dir){
    foreach (glob($pattern) as $file){
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);

        }
    }
}


//Op:1 worked when single program run from context menu


if ($op==1) { 
  $pgmid = $_POST['pgmid'];  
  //echo$pgmid."<BR>";
  $pgmid_len=iconv_strlen (trim($pgmid));
  //echo$pgmid_len."<BR>";
  $tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);
  //echo $tosearchid."<BR>"; 


  // Here system will check data currecny and from where user running program, if program running from base but activated snapshot is diffrent from base 
  // System will copy the program first 
  // System will update the program for pgm location and output location in the program
  // System will copy whole entry for that program for that snapshot
  // System will run the program form that specific snapshot area  
  if ($dc_id!=$dc2_id) {
      $db_dc_filed_value='SP'.$dc_val;

        for ($i=0; $i < 10; $i++) { 
          if ($i % 2 == 0) {
            if (substr(trim($tosearchid), $i, 2) != '00'){
              $tosearchid_dc= substr(trim($tosearchid), 0, $i+2).str_repeat("0",10-$i-2);

              //echo $tosearchid_dc." [".$i."] "."<br>";

              $dc_sql1=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value' ");
              ///print_r($dc_sql1);
              if ($dc_sql1->num_rows > 0){
                  $dc_sql1_1=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                  while($row = $dc_sql1_1->fetch_assoc()) {
                          $title = $row['title'];
                          $pgmname = $row['pgmname'];
                          $section = $row['section'];
                          $pgmloc_o = $row['pgmloc'];
                          $logname = $row['logname'];
                          $pgmloc = $snap_name.'/'.$row['pgmloc'];
                          $logname = $row['logname'];
                          $outloc = $snap_name.'/'.$row['outloc'];
                      }
                  if ($section==' ') {
                      $dc_sql1_2 = $conn->query("UPDATE toc_$study_name SET title='$title', pgmloc='$pgmloc', outloc='$outloc' where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                  }else{
                      $dc_sql1_2 = $conn->query("UPDATE toc_$study_name SET title='$title' where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                  }
                           // if ($dc_sql3) {
                            //   echo "[SUCCESS] sp record inserted into toc study table !!";
                            // }else{
                            //   echo "[FAILED] sp record inserted into toc study table !!";
                            //   echo mysqli_error($conn);
                            // }
              }else{
                $dc_sql1_11=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                  while($row = $dc_sql1_11->fetch_assoc()) {
                        $title = $row['title'];                        
                        $pgmname = $row['pgmname'];
                        $section = $row['section'];  
                        $pgmloc_o = $row['pgmloc'];
                        $logname = $row['logname']; 
                        $pgmloc = $snap_name.'/'.$row['pgmloc'];
                        $logname = $row['logname'];
                        $outloc = $snap_name.'/'.$row['outloc'];
                      }

                  if ($section==' ') {
                    $dc_sql1_3=$conn->query("CREATE TEMPORARY TABLE temp_table_$datetime ENGINE=MEMORY SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                    $dc_sql1_4=$conn->query("UPDATE temp_table_$datetime SET pgmloc='$pgmloc',outloc='$outloc',data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                    $dc_sql1_5=$conn->query("INSERT INTO toc_$study_name SELECT * FROM temp_table_$datetime;");
                    $dc_sql1_6=$conn->query("DROP TABLE temp_table_$datetime;");
                  }else{
                    $dc_sql1_3=$conn->query("CREATE TEMPORARY TABLE temp_table_$datetime ENGINE=MEMORY SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                    $dc_sql1_4=$conn->query("UPDATE temp_table_$datetime SET data_currency='$db_dc_filed_value',id=NULL WHERE 1;"); 
                    $dc_sql1_5=$conn->query("INSERT INTO toc_$study_name SELECT * FROM temp_table_$datetime;");
                    $dc_sql1_6=$conn->query("DROP TABLE temp_table_$datetime;");
                  }

              }

              $dc_sql1=$conn->query("SELECT * FROM toc_status_$study_name where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value' ");
              if ($dc_sql1->num_rows > 0){
                  $dc_sql1_1=$conn->query("SELECT * FROM toc_status_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                  while($row = $dc_sql1_1->fetch_assoc()) {
                        $pgmstat = $row['pgmstat'];
                        $outstat = $row['outstat'];
                  }
                  $dc_sql1_2=$conn->query("UPDATE toc_status_$study_name SET pgmstat=$pgmstat , outstat=$outstat where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                                // if ($dc_sql6) {
                //   echo "[SUCCESS] sp record inserted into toc status table !!";
                // }else{
                //   echo "[FAILED] sp record inserted into toc status table !!";
                //   echo mysqli_error($conn);
                // }
              }else{

                $dc_sql3=$conn->query("CREATE TEMPORARY TABLE temp_st_table_$datetime ENGINE=MEMORY SELECT * FROM toc_status_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                $dc_sql4=$conn->query("UPDATE temp_st_table_$datetime SET data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                $dc_sql5=$conn->query("INSERT INTO toc_status_$study_name SELECT * FROM temp_st_table_$datetime;");
                $dc_sql6=$conn->query("DROP TABLE temp_st_table_$datetime;");

              }
            }
          }
        }

        //$pgmfileloc = $file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc_o;
        //echo $pgmfileloc; 
        //echo " <> ".$file_server.$study. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim($pgmloc); 
        //copyToDir2($file_server.$study_name. DIRECTORY_SEPARATOR .trim($logloc). DIRECTORY_SEPARATOR .trim($logname), $file_server.$study_name. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim($logloc));
        copyToDir2($file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc_o.DIRECTORY_SEPARATOR.$pgmname, $file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc);
        //copyToDir2($file_server.$study_name. DIRECTORY_SEPARATOR .trim($outloc), $file_server.$study_name. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim(substr_replace($outloc," ",strripos($outloc,"/"))));
        copyToDir2($file_server.$study_name.DIRECTORY_SEPARATOR.'lst'.DIRECTORY_SEPARATOR.'toc.xlsx', $file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'lst');
  }else{

  }

  // select specific program information under selected snapshot
  $ses_sql=$conn->query("SELECT study, pgmname, pgmloc,logname,outloc FROM toc_$study_name where sortorder='$tosearchid' AND data_currency='$dc_db_val' ");

  while($row = $ses_sql->fetch_assoc()) {
    $study = $row['study'];
    $pgmname = $row['pgmname'];  
    $pgmloc = $row['pgmloc'];
    $logname = $row['logname'];
    $outloc = $row['outloc'];
  }

  //Create log location 
  $logloc=$pgmloc;
  // echo "$logloc";
  $logloc1= $file_server.$study_name.'/'.$pgmloc;
  //physical location for program file 
  $pgmfileloc = $file_server.$study_name.'/'.$pgmloc.'/'.$pgmname;
  //physical location for output file 
  $loglocfile = $file_server.$study_name.'/'.$pgmloc.'/'.$logname; 
  //echo "$loglocfile";
  //physical location for autoexe file
  $autoexepgmfileloc = $file_server.$study_name.'/pgm/autoexec.sas';

  $sysparam = $file_server.$study_name.'/'.$snap_name;
  //Execute SAS program
//  $command= '"'.$sas_soft.'" -autoexec "'.$autoexepgmfileloc.'"  -sysparm="'.$sysparam.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'" -LOG';

  $command= '"'.$sas_soft.'" -autoexec "'.$autoexepgmfileloc.'"  -sysparm="'.$sysparam.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc. '" -LOG "'.$logloc1.'"';
   //echo "<script> console.log(".$command."); </script>";
  exec($command, $output);

  //copy log file to required location
  copyToDir('../ajax/'.$logname, $file_server.$study_name.'/'.$logloc);


  //Check log file for error and warning and send it to the user 
  $myFile = $loglocfile;

  $file_handler = fopen($myFile, "r")
  or die ("Can't open the File.");

  $err=0;
  $war=0;
  while (!feof($file_handler)) {
    $dataline = fgets($file_handler);
    
    if (strpos($dataline,'ERROR') !== false) {
        $err++;
    }
    if (strpos($dataline,'WARNING') !== false) {
        $war++;
    }
  }

//echo $pgmloc;
//Updating output date;

  if($err==0){
	$out_date = "UPDATE toc_status_$study SET odate_0='$today',outstat='2' WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgmname' AND data_currency='$dc_db_val' ) AND data_currency='$dc_db_val' ";
	$conn->query($out_date);

  $filelink=$datetime.'_'.$pgmname;

  $status="Program running";
  $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
            VALUES ('$tosearchid','$pgmname','$today','$username','Run Success','$status','$filelink','$dc_db_val')");

	}
  else{

    $filelink=$datetime.'_'.$pgmname;
    $status="Program running";
    $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
              VALUES ('$tosearchid','$pgmname','$today','$username','Run Failed','$status','$filelink','$dc_db_val')");
  }


  $sql_status=$conn->query("SELECT COUNT(*) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='$dc_db_val'");

    while($row = $sql_status->fetch_assoc()) {
                  $totalpgm = $row['totalpgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 0  AND data_currency='$dc_db_val' ");

    while($row = $sql_status->fetch_assoc()) {
                  $nopgm = $row['nopgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x  WHERE pgmstat = 1  AND data_currency='$dc_db_val' ");

    while($row = $sql_status->fetch_assoc()) {
                  $indevpgm = $row['indevpgm'];                      
    }                 
  $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 2  AND data_currency='$dc_db_val' ");

    while($row = $sql_status->fetch_assoc()) {
                  $tvalpgm = $row['tvalpgm'];                      
    }                 
  $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 3  AND data_currency='$dc_db_val'");

    while($row = $sql_status->fetch_assoc()) {
                  $valpgm = $row['valpgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 0  AND data_currency='$dc_db_val' ");

    while($row = $sql_status->fetch_assoc()) {
                  $nout = $row['nout'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 1  AND data_currency='$dc_db_val' ");

    while($row = $sql_status->fetch_assoc()) {
                  $oout = $row['oout'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 2 AND data_currency='$dc_db_val' ");

    while($row = $sql_status->fetch_assoc()) {
                  $cout = $row['cout'];                      
    }

  $result9 = $conn->query("SELECT * FROM toc_status_$study_name WHERE sortorder LIKE '$tosearchid%'  AND data_currency='$dc_db_val' ");
    while($row = $result9->fetch_assoc()) {
                 $pgmstat=$row["pgmstat"];
                 $outstat=$row["outstat"];                     
    }
    if ($dc_id!=$dc2_id) {
        $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val2) AND x.id NOT IN (SELECT id FROM toc_$study_name WHERE section !='' AND data_currency = 'SP$dc_id')  ORDER BY x.sortorder";
    }else{

        $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val2)  ORDER BY x.sortorder";
    }// echo $sql;
    $result = $conn->query($sql);
    $treeorder=0;
    $dtreeId="0";
    while ($row = $result->fetch_assoc()) {
                 $treeorder=$treeorder+1; 
                 $pgmname2=$row["pgmname"];

                 if($pgmname2 == $pgmname){
                    $dtreeId=$dtreeId.'#'.$treeorder;
                 }                   
    }  


  $result = '{"success":1,
  "err":"'.$err.'",
  "warn":"'.$war.'",
  "nopgm":"'. $nopgm.'",
  "indevpgm":"'. $indevpgm.'",
  "tvalpgm":"'. $tvalpgm.'",
  "valpgm":"'. $valpgm.'",
  "nout":"'. $nout.'",
  "oout":"'. $oout.'",
  "cout":"'. $cout.'",
  "pgmstat":"'. $pgmstat.'",
  "outstat":"'. $outstat.'",
  "dtreeId":"'. $dtreeId.'"}';
  header('Content-type: application/json;');
  echo $result;  

 }



 if($op==2){

   $grp=$_POST['pgmid'];
   $removesemi=str_replace(",","",$grp);
   $idcount= substr_count($grp, ',');
   $result_2="";

   for ($i=0;$i<=$idcount;$i++){

     $pgmid=substr($removesemi, $i*10, 10);
     // $pgmid_len=iconv_strlen (trim($pgmid));
     // $tosearchid=$pgmid.str_repeat("0",5-$pgmid_len);
     $tosearchid=$pgmid;
     // echo "$tosearchid";


       // Here system will check data currecny and from where user running program, if program running from base but activated snapshot is diffrent from base
       // System will copy the program first
       // System will update the program for pgm location and output location in the program
       // System will copy whole entry for that program for that snapshot
       // System will run the program form that specific snapshot area
       if ($dc_id!=$dc2_id) {
           $db_dc_filed_value='SP'.$dc_val;

           for ($i=0; $i < 10; $i++) {
               if ($i % 2 == 0) {
                   if (substr(trim($tosearchid), $i, 2) != '00'){
                       $tosearchid_dc= substr(trim($tosearchid), 0, $i+2).str_repeat("0",10-$i-2);

                       //echo $tosearchid_dc." [".$i."] "."<br>";

                       $dc_sql1=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value' ");
                       ///print_r($dc_sql1);
                       if ($dc_sql1->num_rows > 0){
                           $dc_sql1_1=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                           while($row = $dc_sql1_1->fetch_assoc()) {
                               $title = $row['title'];
                               $pgmname = $row['pgmname'];
                               $section = $row['section'];
                               $pgmloc_o = $row['pgmloc'];
                               $logname = $row['logname'];
                               $pgmloc = $snap_name.'/'.$row['pgmloc'];
                               $logname = $row['logname'];
                               $outloc = $snap_name.'/'.$row['outloc'];
                           }
                           if ($section==' ') {
                               $dc_sql1_2 = $conn->query("UPDATE toc_$study_name SET title='$title', pgmloc='$pgmloc', outloc='$outloc' where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                           }else{
                               $dc_sql1_2 = $conn->query("UPDATE toc_$study_name SET title='$title' where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                           }
                           // if ($dc_sql3) {
                           //   echo "[SUCCESS] sp record inserted into toc study table !!";
                           // }else{
                           //   echo "[FAILED] sp record inserted into toc study table !!";
                           //   echo mysqli_error($conn);
                           // }
                       }else{
                           $dc_sql1_11=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                           while($row = $dc_sql1_11->fetch_assoc()) {
                               $title = $row['title'];
                               $pgmname = $row['pgmname'];
                               $section = $row['section'];
                               $pgmloc_o = $row['pgmloc'];
                               $logname = $row['logname'];
                               $pgmloc = $snap_name.'/'.$row['pgmloc'];
                               $logname = $row['logname'];
                               $outloc = $snap_name.'/'.$row['outloc'];
                           }

                           if ($section==' ') {
                               $dc_sql1_3=$conn->query("CREATE TEMPORARY TABLE temp_table_$datetime ENGINE=MEMORY SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                               $dc_sql1_4=$conn->query("UPDATE temp_table_$datetime SET pgmloc='$pgmloc',outloc='$outloc',data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                               $dc_sql1_5=$conn->query("INSERT INTO toc_$study_name SELECT * FROM temp_table_$datetime;");
                               $dc_sql1_6=$conn->query("DROP TABLE temp_table_$datetime;");
                           }else{
                               $dc_sql1_3=$conn->query("CREATE TEMPORARY TABLE temp_table_$datetime ENGINE=MEMORY SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                               $dc_sql1_4=$conn->query("UPDATE temp_table_$datetime SET data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                               $dc_sql1_5=$conn->query("INSERT INTO toc_$study_name SELECT * FROM temp_table_$datetime;");
                               $dc_sql1_6=$conn->query("DROP TABLE temp_table_$datetime;");
                           }

                       }

                       $dc_sql1=$conn->query("SELECT * FROM toc_status_$study_name where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value' ");
                       if ($dc_sql1->num_rows > 0){
                           $dc_sql1_1=$conn->query("SELECT * FROM toc_status_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                           while($row = $dc_sql1_1->fetch_assoc()) {
                               $pgmstat = $row['pgmstat'];
                               $outstat = $row['outstat'];
                           }
                           $dc_sql1_2=$conn->query("UPDATE toc_status_$study_name SET pgmstat=$pgmstat , outstat=$outstat where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                           // if ($dc_sql6) {
                           //   echo "[SUCCESS] sp record inserted into toc status table !!";
                           // }else{
                           //   echo "[FAILED] sp record inserted into toc status table !!";
                           //   echo mysqli_error($conn);
                           // }
                       }else{

                           $dc_sql3=$conn->query("CREATE TEMPORARY TABLE temp_st_table_$datetime ENGINE=MEMORY SELECT * FROM toc_status_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                           $dc_sql4=$conn->query("UPDATE temp_st_table_$datetime SET data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                           $dc_sql5=$conn->query("INSERT INTO toc_status_$study_name SELECT * FROM temp_st_table_$datetime;");
                           $dc_sql6=$conn->query("DROP TABLE temp_st_table_$datetime;");

                       }
                   }
               }
           }

           //$pgmfileloc = $file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc_o;
           //echo $pgmfileloc;
           //echo " <> ".$file_server.$study. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim($pgmloc);
           //copyToDir2($file_server.$study_name. DIRECTORY_SEPARATOR .trim($logloc). DIRECTORY_SEPARATOR .trim($logname), $file_server.$study_name. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim($logloc));
           copyToDir2($file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc_o.DIRECTORY_SEPARATOR.$pgmname, $file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc);
           //copyToDir2($file_server.$study_name. DIRECTORY_SEPARATOR .trim($outloc), $file_server.$study_name. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim(substr_replace($outloc," ",strripos($outloc,"/"))));
           copyToDir2($file_server.$study_name.DIRECTORY_SEPARATOR.'lst'.DIRECTORY_SEPARATOR.'toc.xlsx', $file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'lst');
       }else{

       }

     $ses_sql=$conn->query("SELECT study, pgmname, pgmloc,logname,title,outloc FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='$dc_db_val'");
     //print_r($ses_sql);

     while($row = $ses_sql->fetch_assoc()) {
       $study = $row['study'];
       $pgmname = $row['pgmname'];
       $pgmloc = $row['pgmloc'];
       $logname = $row['logname'];
       $title= $row['title'];
       $outloc = $row['outloc'];
     }

       //Create log location
       $logloc=$pgmloc;
       // echo "$logloc";

       //physical location for program file
       $pgmfileloc = $file_server.$study_name.'/'.$pgmloc.'/'.$pgmname;
       //physical location for output file
       $loglocfile = $file_server.$study_name.'/'.$pgmloc.'/'.$logname;
       //echo "$loglocfile";
       //physical location for autoexe file
       $autoexepgmfileloc = $file_server.$study_name.'/pgm/autoexec.sas';

       $sysparam = $file_server.$study_name.'/'.$snap_name;
       //Execute SAS program
       $command= '"'.$sas_soft.'" -autoexec "'.$autoexepgmfileloc.'"  -sysparm="'.$sysparam.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
       // echo "<script> console.log(".$command."); </script>";
       exec($command, $output);

       //copy log file to required location
       copyToDir('../ajax/'.$logname, $file_server.$study_name.'/'.$logloc);


     //Check log file for error and warning and send it to the user
     $myFile = $loglocfile;

       $file_handler = fopen($myFile, "r")
       or die ("Can't open the File.");

       $err=0;
       $war=0;
       while (!feof($file_handler)) {
         $dataline = fgets($file_handler);
        
         if (strpos($dataline,'ERROR') !== false) {
             $err++;
         }
         if (strpos($dataline,'WARNING') !== false) {
             $war++;
         }
       }
       $result_2.= '{"success":1,"title":"'.$title.'","err":"'.$err.'","warn":"'.$war.'"},';
      

     //Updating output date;
     date_default_timezone_set('Asia/Dhaka');
     $today = date("Y-m-d H:i:s");
     if($err==0){
       $out_date = "UPDATE toc_status_$study SET odate_0='$today',outstat='2' where sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgmname') AND data_currency='$dc_db_val'";
       $conn->query($out_date);

       date_default_timezone_set('Asia/Dhaka');
       $datetime=date("Y_m_d_H_i_s");
       $filelink=$datetime.'_'.$pgmname;
       //echo $stat;
       $status="Program running";
       $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
                 VALUES ('$tosearchid','$pgmname','$today','$username','Run Success','$status','$filelink','$dc_db_val')");

     }else{
     date_default_timezone_set('Asia/Dhaka');
     $datetime=date("Y_m_d_H_i_s");
     $filelink=$datetime.'_'.$pgmname;
     //echo $stat;
     $status="Program running";
     $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
               VALUES ('$tosearchid','$pgmname','$today','$username','Run Failed','$status','$filelink','$dc_db_val')");
     }

   
   }

       header('Content-type: application/json;');
       $result_2=substr($result_2, 0, -1);
       echo "[".$result_2."]";
 }


 if($op==3){

   $grp = $_POST['pgmid'];
   $result1="";
   $removesemi=str_replace(",","",$grp);
   //echo "removesemi wil be : ".$removesemi."<BR>";
   $idcount= substr_count($grp, ',');
   //echo "idcount wil be : ".$idcount."<BR>";
   $cond="";
  
   for ($i=0;$i<=$idcount;$i++){
     $temp=substr($removesemi, $i*10, 10);
   //echo "temp wil be : ".$temp."<BR>";
  
   $rootid="";
   for ($j=0; $j < 5; $j++) {
     $temp1=substr($temp, $j*2, 2);
     if ($temp1 != "00") {
       $rootid.=$temp1;
     }
   }

     $cond.="x.sortorder LIKE '".$rootid."%' OR ";
   }
   $cond=substr($cond,0,-3);
   //echo "condition wil be : ".$cond."<BR>";

   $sql="SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND y.pgmstat != 0 AND ($cond) AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
   $result555 = $conn->query($sql);

   // print_r($result);
  // var_dump($result) ;
   while($row = $result555->fetch_assoc()) {
     $study = $row['study'];
     $pgmname = $row['pgmname'];
     $pgmloc = $row['pgmloc'];
     $logname = $row['logname'];
     $title= $row['title'];
     $tosearchid=$row['sortorder'];
     $outloc = $row['outloc'];



       // Here system will check data currecny and from where user running program, if program running from base but activated snapshot is diffrent from base
       // System will copy the program first
       // System will update the program for pgm location and output location in the program
       // System will copy whole entry for that program for that snapshot
       // System will run the program form that specific snapshot area
       if ($dc_id!=$dc2_id) {
           $db_dc_filed_value='SP'.$dc_val;

           for ($i=0; $i < 10; $i++) {
               if ($i % 2 == 0) {
                   if (substr(trim($tosearchid), $i, 2) != '00'){
                       $tosearchid_dc= substr(trim($tosearchid), 0, $i+2).str_repeat("0",10-$i-2);

                       //echo $tosearchid_dc." [".$i."] "."<br>";

                       $dc_sql1=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value' ");
                       ///print_r($dc_sql1);
                       if ($dc_sql1->num_rows > 0){
                           $dc_sql1_1=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                           while($row = $dc_sql1_1->fetch_assoc()) {
                               $title = $row['title'];
                               $pgmname = $row['pgmname'];
                               $section = $row['section'];
                               $pgmloc_o = $row['pgmloc'];
                               $logname = $row['logname'];
                               $pgmloc = $snap_name.'/'.$row['pgmloc'];
                               $logname = $row['logname'];
                               $outloc = $snap_name.'/'.$row['outloc'];
                           }
                           if ($section==' ') {
                               $dc_sql1_2 = $conn->query("UPDATE toc_$study_name SET title='$title', pgmloc='$pgmloc', outloc='$outloc' where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                           }else{
                               $dc_sql1_2 = $conn->query("UPDATE toc_$study_name SET title='$title' where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                           }
                           // if ($dc_sql3) {
                           //   echo "[SUCCESS] sp record inserted into toc study table !!";
                           // }else{
                           //   echo "[FAILED] sp record inserted into toc study table !!";
                           //   echo mysqli_error($conn);
                           // }
                       }else{
                           $dc_sql1_11=$conn->query("SELECT * FROM toc_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                           while($row = $dc_sql1_11->fetch_assoc()) {
                               $title = $row['title'];
                               $pgmname = $row['pgmname'];
                               $section = $row['section'];
                               $pgmloc_o = $row['pgmloc'];
                               $logname = $row['logname'];
                               $pgmloc = $snap_name.'/'.$row['pgmloc'];
                               $logname = $row['logname'];
                               $outloc = $snap_name.'/'.$row['outloc'];
                           }

                           if ($section==' ') {
                               $dc_sql1_3=$conn->query("CREATE TEMPORARY TABLE temp_table_$datetime ENGINE=MEMORY SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                               $dc_sql1_4=$conn->query("UPDATE temp_table_$datetime SET pgmloc='$pgmloc',outloc='$outloc',data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                               $dc_sql1_5=$conn->query("INSERT INTO toc_$study_name SELECT * FROM temp_table_$datetime;");
                               $dc_sql1_6=$conn->query("DROP TABLE temp_table_$datetime;");
                           }else{
                               $dc_sql1_3=$conn->query("CREATE TEMPORARY TABLE temp_table_$datetime ENGINE=MEMORY SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                               $dc_sql1_4=$conn->query("UPDATE temp_table_$datetime SET data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                               $dc_sql1_5=$conn->query("INSERT INTO toc_$study_name SELECT * FROM temp_table_$datetime;");
                               $dc_sql1_6=$conn->query("DROP TABLE temp_table_$datetime;");
                           }

                       }

                       $dc_sql1=$conn->query("SELECT * FROM toc_status_$study_name where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value' ");
                       if ($dc_sql1->num_rows > 0){
                           $dc_sql1_1=$conn->query("SELECT * FROM toc_status_$study_name where sortorder='$tosearchid_dc' and data_currency='SP0' ");
                           while($row = $dc_sql1_1->fetch_assoc()) {
                               $pgmstat = $row['pgmstat'];
                               $outstat = $row['outstat'];
                           }
                           $dc_sql1_2=$conn->query("UPDATE toc_status_$study_name SET pgmstat=$pgmstat , outstat=$outstat where sortorder='$tosearchid_dc' and data_currency='$db_dc_filed_value'");
                           // if ($dc_sql6) {
                           //   echo "[SUCCESS] sp record inserted into toc status table !!";
                           // }else{
                           //   echo "[FAILED] sp record inserted into toc status table !!";
                           //   echo mysqli_error($conn);
                           // }
                       }else{

                           $dc_sql3=$conn->query("CREATE TEMPORARY TABLE temp_st_table_$datetime ENGINE=MEMORY SELECT * FROM toc_status_$study_name WHERE sortorder='$tosearchid_dc' and data_currency='SP0';");
                           $dc_sql4=$conn->query("UPDATE temp_st_table_$datetime SET data_currency='$db_dc_filed_value',id=NULL WHERE 1;");
                           $dc_sql5=$conn->query("INSERT INTO toc_status_$study_name SELECT * FROM temp_st_table_$datetime;");
                           $dc_sql6=$conn->query("DROP TABLE temp_st_table_$datetime;");

                       }
                   }
               }
           }

           //$pgmfileloc = $file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc_o;
           //echo $pgmfileloc;
           //echo " <> ".$file_server.$study. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim($pgmloc);
           //copyToDir2($file_server.$study_name. DIRECTORY_SEPARATOR .trim($logloc). DIRECTORY_SEPARATOR .trim($logname), $file_server.$study_name. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim($logloc));
           copyToDir2($file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc_o.DIRECTORY_SEPARATOR.$pgmname, $file_server.$study_name.DIRECTORY_SEPARATOR.$pgmloc);
           //copyToDir2($file_server.$study_name. DIRECTORY_SEPARATOR .trim($outloc), $file_server.$study_name. DIRECTORY_SEPARATOR .'snapshot'.$dc_val. DIRECTORY_SEPARATOR .trim(substr_replace($outloc," ",strripos($outloc,"/"))));
           copyToDir2($file_server.$study_name.DIRECTORY_SEPARATOR.'lst'.DIRECTORY_SEPARATOR.'toc.xlsx', $file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'lst');
       }else{

       }

       //Create log location
       $logloc=$pgmloc;
       // echo "$logloc";

       //physical location for program file
       $pgmfileloc = $file_server.$study_name.'/'.$pgmloc.'/'.$pgmname;
       //physical location for output file
       $loglocfile = $file_server.$study_name.'/'.$pgmloc.'/'.$logname;
       //echo "$loglocfile";
       //physical location for autoexe file
       $autoexepgmfileloc = $file_server.$study_name.'/pgm/autoexec.sas';

       $sysparam = $file_server.$study_name.'/'.$snap_name;
       //Execute SAS program
       $command= '"'.$sas_soft.'" -autoexec "'.$autoexepgmfileloc.'"  -sysparm="'.$sysparam.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
       // echo "<script> console.log(".$command."); </script>";
       exec($command, $output);

       //copy log file to required location
       copyToDir('../ajax/'.$logname, $file_server.$study_name.'/'.$logloc);


     $myFile = $loglocfile;

       $file_handler = fopen($myFile, "r")
       or die ("Can't open the File.");

       $err=0;
       $war=0;
       while (!feof($file_handler)) {
           $dataline = fgets($file_handler);
          
           if (strpos($dataline,'ERROR') !== false) {
               $err++;
             }
           if (strpos($dataline,'WARNING') !== false) {
               $war++;
             }
       }



     //Updating output date;
     date_default_timezone_set('Asia/Dhaka');
     $today = date("Y-m-d H:i:s");
       if($err==0){
         $out_date = "UPDATE toc_status_$study SET odate_0='$today',outstat='2' where sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgmname') AND data_currency='$dc_db_val' ";
         $conn->query($out_date);

       date_default_timezone_set('Asia/Dhaka');
       $datetime=date("Y_m_d_H_i_s");
       $filelink=$datetime.'_'.$pgmname;
       //echo $stat;
       $status="Program running";
       $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
                 VALUES ('$tosearchid','$pgmname','$today','$username','Run Success','$status','$filelink','$dc_db_val')");

       }else{
         date_default_timezone_set('Asia/Dhaka');
         $datetime=date("Y_m_d_H_i_s");
         $filelink=$datetime.'_'.$pgmname;
         //echo $stat;
         $status="Program running";
         $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,data_currency)
                   VALUES ('$tosearchid','$pgmname','$today','$username','Run Failed','$status','$filelink','$dc_db_val')");
       }

         $sql_status=$conn->query("SELECT COUNT(*) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $totalpgm = $row['totalpgm'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 0 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $nopgm = $row['nopgm'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x  WHERE pgmstat = 1 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $indevpgm = $row['indevpgm'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 2 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $tvalpgm = $row['tvalpgm'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 3 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $valpgm = $row['valpgm'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 0 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $nout = $row['nout'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 1 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $oout = $row['oout'];
           }
         $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 2 AND data_currency='$dc_db_val' ");

           while($row = $sql_status->fetch_assoc()) {
                         $cout = $row['cout'];
           }

         $result9 = $conn->query("SELECT * FROM toc_status_$study_name WHERE sortorder LIKE '$tosearchid%' AND data_currency='$dc_db_val' ");
           while($row = $result9->fetch_assoc()) {
                        $pgmstat=$row["pgmstat"];
                        $outstat=$row["outstat"];
           }

           if ($dc_id!=$dc2_id) {
               $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val2) AND x.id NOT IN (SELECT id FROM toc_$study_name WHERE section !='' AND data_currency = 'SP$dc_id')  ORDER BY x.sortorder";
           }else{

               $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val2)  ORDER BY x.sortorder";
           }// echo $sql;
           $result = $conn->query($sql);
           $treeorder=0;
           $dtreeId="0";

           while($row = $result->fetch_assoc()) {
                        $treeorder=$treeorder+1;
                        $pgmname2=$row["pgmname"];

                        if($pgmname2 == $pgmname){
                           $dtreeId=$dtreeId.'#'.$treeorder;
                        }
           }

          $result1.= '{"success":1,
                       "title":"'.$title.'",
                       "err":"'.$err.'",
                       "warn":"'.$war.'",
                       "nopgm":"'. $nopgm.'",
                       "indevpgm":"'. $indevpgm.'",
                       "tvalpgm":"'. $tvalpgm.'",
                       "valpgm":"'. $valpgm.'",
                       "nout":"'. $nout.'",
                       "oout":"'. $oout.'",
                       "cout":"'. $cout.'",
                       "pgmstat":"'. $pgmstat.'",
                       "outstat":"'. $outstat.'",
                       "dtreeId":"'. $dtreeId.'"},';
     }

       header('Content-type: application/json;');
       $result1=substr($result1, 0, -1);
       echo "[".$result1."]";
 }


?>