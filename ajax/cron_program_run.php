<?php 

include("../include/connect.php");	

	//Copying all log file to the pgm directory;
	function copyToDir($pattern, $dir){
	  foreach (glob($pattern) as $file){
	      if(!is_dir($file) && is_readable($file))
	      {
	          $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
	          copy($file, $dest);
	          unlink($file);
	      }
	  }
	}

	function run_cron($study_name, $tosearchid){
		include("../include/connect.php");

		$username = 'By MeD-OMS';
		$ses_sql = $conn->query("select study, pgmname, pgmloc,logname from toc_$study_name where sortorder='$tosearchid' ");

	  while($row = $ses_sql->fetch_assoc()) {
	    $study = $row['study'];
	    $pgmname = $row['pgmname'];  
	    $pgmloc = $row['pgmloc']; 
	    $logname = $row['logname'];                      
	  }
		$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study."' ");
		while($row = $result44->fetch_assoc()) {
			$file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
		}
	  //Create log location 
	  $logloc=substr_replace($pgmloc," ",strripos($pgmloc,"/"));
	  // echo "$logloc";

	  $pgmfileloc = $file_server.$study.'/'.$pgmloc;
	  $loglocfile = $file_server.$study.'/'.substr_replace($pgmloc,$logname,strripos($pgmloc,"/")+1); 
	   //echo "$loglocfile";

	  //Execute SAS program
	  $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
	  // echo "<script> console.log(".$command."); </script>"; 
	  exec($command, $output); 
		copyToDir('../ajax/*.log', $file_server.$study.'/'.$logloc);
	  //Check log file for error and warning and send it to the user 
	  $myFile = $loglocfile;

	  $file_handler = fopen($myFile, "r")
	  or die ("Can't open the File.");

	  $err=0;
	  $war=0;
	  while (!feof($file_handler)) {
	    $dataline = fgets($file_handler);
	    
	    if (strpos($dataline,'ERROR') !== false) {
	        $err++;
	    }
	    if (strpos($dataline,'WARNING') !== false) {
	        $war++;
	    }
	  }


		////Updating output date;
		date_default_timezone_set('Asia/Dhaka');
		$today = date("Y-m-d H:i:s");

	  if($err==0){
	  	$out_date = "UPDATE toc_status_" . $study . " SET odate_0='$today',outstat='2' where sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmloc='$pgmloc')";
	  	$conn->query($out_date);

	  date_default_timezone_set('Asia/Dhaka');
	  $datetime=date("Y_m_d_H_i_s");
	  $filelink=$datetime.'_'.$pgmname;
	  //echo $stat;
	  $status="Program running";
	  $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
	    VALUES ('$tosearchid','$pgmname','$today','$username','Run Program','$status','$filelink')");
		}
		echo $study_name.' '.$tosearchid."<br/>";
	}

	$cron_sql = $conn->query("select * from program_run_time where status = 'ON' ");
	while($data = $cron_sql->fetch_assoc()) {
		date_default_timezone_set('Asia/Dhaka');
		$now = date("Y-m-d H:i:s"); 
		$date = $now;
		$_run = $data['next_run_time'];
		$type = $data['run_type'];
		$now = strtotime($now);
		$_run = strtotime($_run);
		$dif = $now - $last_run;
		$time = round(($now - $_run)/ 60,2); 
		if($time >= 0){
			$study_name = $data['study_name'];
			$tosearchid = $data['program_id'];
			$id = $data['id'];
			if($type == 'daily'){
				$next_date = strtotime("+1 day", $_run);
				$next_date =  date('Y-m-d H:i:s', $next_date);
			}
			else if($type == 'weekly'){
				$next_date = strtotime("+1 week", $_run);
				$next_date =  date('Y-m-d H:i:s', $next_date);
			}
			else if($type == 'monthly'){
				$next_date = strtotime("+1 month", $_run);
				$next_date =  date('Y-m-d H:i:s', $next_date);
			}
			else{
				$next_date = strtotime("+1 year", $_run);
				$next_date =  date('Y-m-d H:i:s', $next_date);
			}
			if($type == 'once'){
				$update = "UPDATE program_run_time SET last_run_time ='$date', status ='OFF' where id = $id";
				$conn->query($update);
			}else{
				$update = "UPDATE program_run_time SET last_run_time ='$date', next_run_time ='$next_date' where id = $id";
				$conn->query($update);
			}			
			run_cron($study_name, $tosearchid);
		}
	}
?>