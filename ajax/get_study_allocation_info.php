<?php
include("../include/connect.php");
$userId=$_REQUEST['user_id'];
$data=array();
$allocation=array();
$roleInfo=array();
$remainStudy=array();

$sql="SELECT * FROM user_info WHERE user_id='$userId'";
$result=$conn->query($sql);

$data['userid']=$userId;
while ($row = $result->fetch_assoc()) {
    $data['username']=$row['username'];
}

$sql1="SELECT study_info.study_id,study_info.study_name,study_allocation.user_type,study_allocation.status, study_allocation.allocation_id FROM study_allocation LEFT JOIN study_info ON study_allocation.study_id=study_info.study_id WHERE study_allocation.user_id='$userId'";
$result1=$conn->query($sql1);

while ($row = $result1->fetch_assoc()) {
    $singleData=array();
    $singleData['study_name']=$row['study_name'];
    $singleData['user_type']=$row['user_type'];
    $singleData['status']=$row['status'];
    $singleData['allocation_id']=$row['allocation_id'];
    $singleData['study_id']=$row['study_id'];
    array_push($allocation,$singleData);
}

$sql2="SELECT * FROM user_type_info WHERE user_type_dc!='Super Admin'";
$result2=$conn->query($sql2);
while ($row = $result2->fetch_assoc()) {
    $singleData=array();
    $singleData['role_id']=$row['user_type'];
    $singleData['role_name']=$row['user_type_dc'];
    array_push($roleInfo,$singleData);
}

$sql3="SELECT study_info.study_id,study_info.study_name FROM study_info WHERE study_info.study_id NOT IN (SELECT study_allocation.study_id FROM study_allocation WHERE user_id ='$userId')";
$result3=$conn->query($sql3);
while ($row = $result3->fetch_assoc()) {
    $singleData=array();
    $singleData['study_id']=$row['study_id'];
    $singleData['study_name']=$row['study_name'];
    array_push($remainStudy,$singleData);
}

$data['allocation_info']=$allocation;
$data['role_info']=$roleInfo;
$data['remain_study']=$remainStudy;

echo json_encode($data);
?>