<?php 
include("../include/connect.php");
session_start();
$study_name=$_SESSION["study"];

$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");

$cp_name=$_POST["cp_name"];
$data = array();

$cng_vs=$_POST["cpgm_status"];
if($cng_vs==2){
	$stat=$cng_vs;
}else{
    $valstat=$_POST["valstat"];
    if($valstat==1){
    	$stat=3;
    }
    elseif ($valstat==0){
     	$stat=1;
    } 
}

$pfcmt=$_POST['pfcmt'];
$v_sys=$_POST['v_sys'];

$Validation_system="";
if(!empty($_POST['v_sys'])) {
    foreach($_POST['v_sys'] as $check){
        if($check == 0){
            $vs_text = "Independent Programming";
        }
        elseif ($check == 1) {
            $vs_text = "Manual Code check";
        }
        else{
            $vs_text = "Manual Output Check";
        }
        $Validation_system =  $Validation_system.",".$vs_text;
    }
}
$Validation_systems = substr($Validation_system,1);

$ses_sql=$conn->query("SELECT * FROM cplist_$study_name WHERE cpname='$cp_name' ");

while($row = $ses_sql->fetch_assoc()) {
    $pgmname = $row['cpname'];
    $pgmloc = $row['cploc'];
    $sortorder = $row['sortorder'];
}

$cp_loc = $file_server.$study_name.DIRECTORY_SEPARATOR.'backup'.DIRECTORY_SEPARATOR.$pgmloc;

if (!file_exists($cp_loc)) {
    mkdir($cp_loc, 0777, true);
    //echo "not exist >> created now";
}else{
    //echo "exist";
}


//Copying current pgm file to the pgm/history directory;
	function copyToDir($pattern, $dir, $study_name,$pgmname,$file_server,$file_loc){
        foreach (glob($pattern) as $file)
        {
            if(!is_dir($file) && is_readable($file))
            {
                $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
                copy($file, $dest);
                date_default_timezone_set('Asia/Dhaka');
                $datetime=date("Y_m_d_H_i_s");
                $sepdotfrmfile=substr_replace($file," ",strripos($file,$pgmname));
                  //rename('study/'.$study_name.'/backup/'.$pgmname, 'study/'.$study_name.'/backup/'.$datetime.'_'.$pgmname);
                 rename(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'backup'.DIRECTORY_SEPARATOR.$file_loc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'backup'.DIRECTORY_SEPARATOR.$file_loc.DIRECTORY_SEPARATOR.$datetime.'_'.$pgmname));
            }
        }
	}

    $file_loc = $pgmloc;
 	copyToDir(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$pgmloc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'backup'.DIRECTORY_SEPARATOR.$pgmloc),$study_name,$pgmname,$file_server,$file_loc);

$validator='';
if($stat==1){
    $cpstatus ='In Development ' ;
    $validator='';
}
if($stat==2){
    $cpstatus ='To Be validated' ;
    $validator='';
}
if($stat==3){
    $cpstatus ='Validated' ;
    $validator=$username;
}
$result=$conn->query("UPDATE cplist_$study_name SET pgmstat = '$stat', status = '$cpstatus', status_date = '$today', validator = '$validator'  WHERE cpname = '$pgmname' ");

if($result){
	$success="1";
	date_default_timezone_set('Asia/Dhaka');
	$datetime=date("Y_m_d_H_i_s");
	$filelink=$datetime.'_'.$pgmname;
	//echo $stat;
	if($stat==1)$status="In Development";
		if($stat==2)$status="To Be validated";
		if($stat==3)$status="Validated";
		$result=$conn->query("INSERT INTO cpgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,validation_system)
							VALUES ('$sortorder','$pgmname','$today','$username','$pfcmt','$status','$filelink','$Validation_systems')");
}else{
	$success="0";
}
$pgm_list = '';
$result=$conn->query("SELECT * FROM cplist_$study_name");
while($row = $result->fetch_assoc()) {
    $cpname = $row['cpname'];
    $cploc = $row['cploc'];;
    $programmer = $row['programmer'];
    $cpdate = $row['cpdate'];
    $pgmstat = $row['pgmstat'];
    $status = $row['status'];
    $validator = $row['validator'];
    $status_date = $row['status_date'];
    $pgm_list.='{"cpname": "'.$cpname.'",
                 "cploc": "'.$cploc.'",
                 "programmer": "'.$programmer.'",
                 "cpdate": "'.$cpdate.'",
                 "status": "'.$status.'",
                 "pgmstat": "'.$pgmstat.'",
                 "validator": "'.$validator.'",
                 "status_date": "'.$status_date.'",
                 "success" : "' .$success.'"
                    },';
}

header('Content-type: application/json;');
$pgmlist=substr($pgm_list, 0, -1);
echo "[".$pgmlist."]";

?>