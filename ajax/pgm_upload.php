<?php 
include("../include/connect.php");
session_start();

$study_name=$_SESSION["study"];
$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
	$file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

$username = $_SESSION['login_user'];
$pgmid = $_POST["dl_sid"];

$today = date("Y-m-d H:i:s");

$pgmid_len=iconv_strlen (trim($pgmid));
//echo$pgmid_len."<BR>";
$tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);
//echo $tosearchid."<BR>"; 
									
$result=$conn->query("SELECT pgmname,pgmloc FROM toc_$study_name WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");
while($row = $result->fetch_assoc()) {
	$pgmname = $row['pgmname'];
	$pgmloc = $row['pgmloc'];
}
//echo $pgmname. " == ". $pgmloc." == ";
$dirchk = $file_server.$study_name."/";

$target_dir = $file_server.$study_name."/".$pgmloc."/".$pgmname;
	// echo" == target dic: ".$target_dir." == ";
$data = array();
$issue ="";
$success=0;
if ($_FILES["file_upload"]["name"] != "") {

	$given_file = basename($_FILES["file_upload"]["name"]);
    //echo " == ".$given_file." == ";

	$target_file = $target_dir;
	$uploadOk = 1;
	$FileType = pathinfo($target_file, PATHINFO_EXTENSION);

	// Check if file name matches with TOC pgmname
	if($pgmname != $given_file){

		$issue = '1';
		$uploadOk = 0;
	}

	// Check if file already exists
	//                                  if (file_exists($target_file)) {
	//                                      echo '<div class="alert alert-danger" role="alert">
	//                                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
	//                                                          <span class="sr-only">Error:</span>
	//                                                          Sorry, file already exists. ! but replaced by new one
	//                                                          </div>';
	//                                      $uploadOk = 1;
	//                                  }
	// Check file size
	if ($_FILES["file_upload"]["size"] > 500000) {
	    $issue = $issue.'2';
		$uploadOk = 0;
	}
	// Allow certain file formats
	if ($FileType != "sas" ) {
		$issue = $issue.'3';
			$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		$issue = $issue.'4';
	// if everything is ok, try to upload file
	}else{
		if(file_exists($dirchk)){
			if(move_uploaded_file($_FILES["file_upload"]["tmp_name"], $target_file)) {
				$success=1;
				//echo "<div class='alert alert-success'>The file ". basename($_FILES["fileToUpload"]["name"]) ." has been uploaded.</div>";
				
				$result=$conn->query("SELECT pgmstat FROM toc_status_$study_name WHERE sortorder = '$tosearchid' AND data_currency='SP0'");
				while ($row=$result->fetch_assoc()) {
					$pgmstat=$row['pgmstat'];
				}

				date_default_timezone_set('Asia/Dhaka');
		     	$datetime=date("Y_m_d_H_i_s");
				$filelink=$datetime.'_'.$pgmname;
				
				//Copying current pgm file to the pgm/history directory;
			  	function copyToDir($pattern, $dir, $study_name,$pgmname,$file_server,$file_loc){
			      	foreach (glob($pattern) as $file) 
			      	{
			          	if(!is_dir($file) && is_readable($file)) 
			          	{
			              	$dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
			              	copy($file, $dest);
			              	date_default_timezone_set('Asia/Dhaka');
						  	$datetime=date("Y_m_d_H_i_s");
						  	$sepdotfrmfile=substr_replace($file," ",strripos($file,$pgmname));
			             	  //rename('study/'.$study_name.'/backup/'.$pgmname, 'study/'.$study_name.'/backup/'.$datetime.'_'.$pgmname);
			             	 rename(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.$datetime.'_'.$pgmname));
			          	}
			      	}             
			  	}
			  	//copyToDir('study/'.$study_name.'/'.$pgmloc, 'study/'.$study_name.'/backup/',$study_name,$pgmname);
                $file_loc = substr_replace($pgmloc,'',strripos($pgmloc,"/")+1);
                //echo $file_loc;
			  	copyToDir(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$pgmloc), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc),$study_name,$pgmname,$file_server,$file_loc);



				if($pgmstat!=0){
						$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
						VALUES ('$tosearchid','$pgmname','$today','$username','Uploaded','In Development','$filelink')");	
				}else{
						$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
						VALUES ('$tosearchid','$pgmname','$today','$username','Uploaded','In Development','$filelink')");
				}															


				$result=$conn->query("UPDATE toc_status_$study_name SET pgmstat = 1 , pdate_1 = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgmname') AND data_currency='SP0' ");
				$result=$conn->query("SELECT outstat FROM toc_status_$study_name WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");
				while ($row=$result->fetch_assoc()) {
					$outstat=$row['outstat'];
				}
				if($outstat!=0){
					$result=$conn->query("UPDATE toc_status_$study_name SET outstat = 1 , odate_0 = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgmname') AND data_currency='SP0' ");
				}
				//  date_default_timezone_set('Asia/Dhaka');
		        //	$datetime=date("Y_m_d_H_i_s");
				//  $filelink=$datetime.'_'.$pgmname;															
				// 	$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
				// 	VALUES ('$tosearchid','$pgmname','$today','$username','First Uploaded','In Development','$filelink')");								
			}
			else{
				$issue.'6';
			}
		}else{
			$issue.'5';
            $uploadOk = 0;   
		} 	
	}
}
else {
	$issue.'7';
}

  $sql_status=$conn->query("SELECT COUNT(*) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $totalpgm = $row['totalpgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x WHERE pgmstat = 0 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $nopgm = $row['nopgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x  WHERE pgmstat = 1 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $indevpgm = $row['indevpgm'];                      
    }                 
  $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x WHERE pgmstat = 2 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $tvalpgm = $row['tvalpgm'];                      
    }                 
  $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x WHERE pgmstat = 3 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $valpgm = $row['valpgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x WHERE outstat = 0 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $nout = $row['nout'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x WHERE outstat = 1 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $oout = $row['oout'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.section ='') as x WHERE outstat = 2 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $cout = $row['cout'];                      
    }

  $result9 = $conn->query("SELECT * FROM toc_status_$study_name WHERE sortorder LIKE '$tosearchid%' AND data_currency='SP0' ");
    while($row = $result9->fetch_assoc()) {
                 $pgmstat=$row["pgmstat"];
                 $outstat=$row["outstat"];                     
    }

  $result10 = $conn->query("SELECT * FROM toc_$study_name WHERE data_currency='SP0'  ORDER BY sortorder ASC");
    $treeorder=0;
    $dtreeId="0";
    while($row = $result10->fetch_assoc()) {
                 $treeorder=$treeorder+1; 
                 $pgmname2=$row["pgmname"];

                 if($pgmname2 == $pgmname){
                    $dtreeId=$dtreeId.'#'.$treeorder;
                 }                   
    }  

$data = array('success' => $success,
			 'issue' => $issue,
			 'file_server' => $file_server,
			 'study_name' => $study_name,
			 'pgmname' => $pgmname,
			 'given_file' => $given_file,
			 'pgmstat' => $pgmstat,
			 'outstat' => $outstat,
			 'dtreeId' => $dtreeId,
			 'nopgm' => $nopgm,
			 'indevpgm' => $indevpgm,
			 'tvalpgm' => $tvalpgm,
			 'valpgm' => $valpgm,
			 'nout' => $nout,
			 'oout' => $oout,
			 'cout' => $cout			 
			 );

echo json_encode($data);
?>