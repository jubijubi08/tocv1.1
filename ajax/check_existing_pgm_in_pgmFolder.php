<?php 

include("../include/connect.php");
/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);

session_start();
$study_name=$_SESSION["study"];

//retive study location
$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

$username = $_SESSION['login_user'];

//Copying all log file to the pgm directory;
function copyToDir($pattern, $dir){
    foreach (glob($pattern) as $file){
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);
            unlink($file);
        }
    }
}

//update program status and output status if program physical file does not exist
$result45=$conn->query("SELECT DISTINCT pgmname,pgmloc FROM toc_$study_name WHERE pgmname != '' AND pgmloc != '' AND data_currency='SP0' ");
while($row = $result45->fetch_assoc()) {
    $pgmname = $row['pgmname'];
    $pgmloc = $row['pgmloc'];

    $actual_file_loc = $file_server.$study_name.'/'.$pgmloc.'/'.$pgmname;
 

    if(!file_exists($actual_file_loc)){
        //echo "this file not exist:".$actual_file_loc."<br>";
        $update_status_pgm_out = $conn->query("UPDATE toc_status_$study_name SET pgmstat = 0 , outstat = 0 where sortorder  IN (SELECT sortorder FROM toc_$study_name  WHERE pgmname = '$pgmname' AND data_currency = 'SP0') AND data_currency='SP0' ");
        $update_status_in_toc = $conn->query("UPDATE toc_$study_name SET status = ' ' where sortorder IN (SELECT sortorder FROM toc_status_$study_name  WHERE pgmstat = 0 AND outstat = 0 AND data_currency='SP0') AND data_currency='SP0'");
        // if($update_status_in_toc){ echo "status updated for: ". $pgmname;} else { echo "status var update failed";}
        // echo "pgm name from database : ".$pgmname;
    }
}


$val_pgm = $conn->query("SELECT distinct pgmname FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency = b.data_currency AND b.pgmstat != 0 AND a.data_currency='SP0'");

//print_r($val_pgm);

//update status column based of program current status 
if (mysqli_num_rows($val_pgm) > 0){
    while($row = $val_pgm->fetch_assoc()) {
        $v_pgm = $row['pgmname'];
        //echo "v:".$v_pgm;
        $update_status_var = $conn->query("UPDATE toc_$study_name SET status = 'v' where pgmname = '$v_pgm' AND data_currency='SP0' ");
        //if($update_status_var){ echo "status updated for: ". $v_pgm;} else { echo "status var update failed";}
    }
} else {
        $update_status_var = $conn->query("UPDATE toc_$study_name SET status = ' ' WHERE data_currency='SP0' ");
}


$pgm_list ="";
$result=$conn->query("SELECT DISTINCT pgmname,pgmloc FROM toc_$study_name WHERE pgmname != '' AND pgmloc != '' AND data_currency='SP0' ");
while($row = $result->fetch_assoc()) {
    $pgmname = $row['pgmname'];
    $pgmloc = $row['pgmloc'];

    $actual_file_loc = $file_server.$study_name.'/'.$pgmloc.'/'.$pgmname;
    //echo "pgm name from database : ".$pgmname;

    foreach (glob($actual_file_loc) as $filename) {
        //$filename  . " This file exist" . "\n";
        $val_pgm = $conn->query("SELECT pgmstat FROM toc_status_$study_name WHERE sortorder = ( SELECT  sortorder FROM toc_$study_name WHERE  pgmname = '$pgmname' AND data_currency='SP0' LIMIT 1) AND data_currency='SP0' ");
        while($row = $val_pgm->fetch_assoc()) {
            $pgm_status = $row['pgmstat'];
        }

        $pgm_list.='{"pgmname": "'.$pgmname.'",
                     "pgmstat": "'.$pgm_status.'"
                    },';
    }
}

//echo "[".$pgm_list."]";
//print_r($pgm_list);
header('Content-type: application/json;');
$pgm_list=substr($pgm_list, 0, -1);
echo "[".$pgm_list."]";

?>