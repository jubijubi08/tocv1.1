<?php
include("../include/connect.php");
session_start();
$study_name=$_SESSION["study"];
$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");
$success="1";
$data = array();

//code for insert new section
if(isset($_POST["sec_id"])){

    $section = $_POST['sec_name'];
    $pgmid = $_POST["sec_id"];

    $pgmid_len=iconv_strlen (trim($pgmid));

    if($pgmid_len>2){
        if($pgmid_len>2){
            $masterid=substr($pgmid,0,$pgmid_len-2);
        }
        else{
            $masterid=$pgmid;
        }
        $pgmid_to_be_add0=$pgmid+1;
        if(iconv_strlen (trim($pgmid_len))<$pgmid_len ){
            $pgmid_to_be_add=trim("0".$pgmid_to_be_add0);
        }
        $pgmid_len=iconv_strlen (trim($pgmid_to_be_add));
        $tosearchid=$pgmid_to_be_add.str_repeat("0",10-$pgmid_len);

        $result=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0' ");


        if($result->num_rows > 0){
            while($row = $result->fetch_assoc()) {
                $l1 = $row['l1'];
                $l2 = $row['l2'];
                $l3 = $row['l3'];
                $l4 = $row['l4'];
                $l5 = $row['l5'];
                //echo "l1: ".$l1." l2: ".$l2." l3:".$l3." l4:".$l4." l5:".$l5."<br>";

                $sql2="SELECT * FROM (SELECT * 
                                FROM toc_$study_name 
                                WHERE sortorder >=$tosearchid) AS a WHERE sortorder LIKE '$masterid%' AND data_currency='SP0' ORDER BY sortorder DESC";
                $result2 = $conn->query($sql2);
                while($row = $result2->fetch_assoc()) {
                    $lblpgm=$pgmid_len/2;
                    $temp="l$lblpgm";
                    $temp1=$row["$temp"]+1;
                    if(iconv_strlen (trim($temp1))==0){
                        $temp0=trim("00".$temp1);
                    }
                    else if(iconv_strlen (trim($temp1))==1){
                        $temp0=trim("0".$temp1);
                    }else {
                        $temp0=trim($temp1);
                    }
                    $sortorder=$row["sortorder"];
                    $newsort=substr_replace($sortorder,$temp0,$pgmid_len-2,2);

                    //update toc status table
                    $sql4="UPDATE toc_status_$study_name SET sortorder='$newsort' WHERE sortorder ='$sortorder' AND data_currency='SP0'";
                    $result4 = $conn->query($sql4);
                    //update toc table
                    $sql3="UPDATE toc_$study_name SET sortorder='$newsort', l$lblpgm='$temp0' WHERE sortorder ='$sortorder' AND data_currency='SP0'";
                    $result3 = $conn->query($sql3);
                }
                $sql5="INSERT INTO toc_$study_name (data_currency,study,l1,l2,l3,l4,l5,section,sortorder) VALUES ('SP0','$study_name','$l1','$l2','$l3','$l4','$l5','$section','$tosearchid')";
                $result5 = $conn->query($sql5);
                date_default_timezone_set('Asia/Dhaka');
                $today = date("Y-m-d H:i:s");
                $sql6="INSERT INTO toc_status_$study_name (data_currency,study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('SP0','$study_name','$tosearchid','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
                $result6 = $conn->query($sql6);

            }
        }
        else{
            $l1=substr($tosearchid,0,2);
            $l2=substr($tosearchid,2,2);
            $l3=substr($tosearchid,4,2);
            $l4=substr($tosearchid,6,2);
            $l5=substr($tosearchid,8,2);

            //insert into toc table
            $sql7="INSERT INTO toc_$study_name (data_currency,study,sortorder,l1,l2,l3,l4,l5,section) VALUES ('SP0','$study_name','$tosearchid','$l1','$l2','$l3','$l4','$l5','$section') ";
            $result7 = $conn->query($sql7);
            date_default_timezone_set('Asia/Dhaka');
            $today = date("Y-m-d H:i:s");
            $sql8="INSERT INTO toc_status_$study_name (data_currency,study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('SP0','$study_name','$tosearchid','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
            $result8 = $conn->query($sql8);
        }
    }
    else{
        $success="0";
/*        echo '<div class="alert alert-danger" role="alert">
                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
                    You can not create section here!!
                    </div>';*/
    }
}

$data = array('success' => $success );
echo json_encode($data);
?>