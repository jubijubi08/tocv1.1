<?php
include("../include/connect.php");
session_start();
$study_name = $_SESSION["study"];


/**
 * Helper library for CryptoJS AES encryption/decryption
 * Allow you to use AES encryption on client side and server side vice versa
 *
 * @author BrainFooLong (bfldev.com)
 * @link https://github.com/brainfoolong/cryptojs-aes-php
 */

/**
 * Decrypt data from a CryptoJS json encoding string
 *
 * @param mixed $passphrase
 * @param mixed $jsonString
 * @return mixed
 */
function cryptoJsAesDecrypt($passphrase, $jsonString){
    $jsondata = json_decode($jsonString, true);
    try {
        $salt = hex2bin($jsondata["s"]);
        $iv  = hex2bin($jsondata["iv"]);
    } catch(Exception $e) { return null; }
    $ct = base64_decode($jsondata["ct"]);
    $concatedPassphrase = $passphrase.$salt;
    $md5 = array();
    $md5[0] = md5($concatedPassphrase, true);
    $result = $md5[0];
    for ($i = 1; $i < 3; $i++) {
        $md5[$i] = md5($md5[$i - 1].$concatedPassphrase, true);
        $result .= $md5[$i];
    }
    $key = substr($result, 0, 32);
    $data = openssl_decrypt($ct, 'aes-256-cbc', $key, true, $iv);
    return json_decode($data, true);
}

/**
 * Encrypt value to a cryptojs compatiable json encoding string
 *
 * @param mixed $passphrase
 * @param mixed $value
 * @return string
 */
function cryptoJsAesEncrypt($passphrase, $value){
    $salt = openssl_random_pseudo_bytes(8);
    $salted = '';
    $dx = '';
    while (strlen($salted) < 48) {
        $dx = md5($dx.$passphrase.$salt, true);
        $salted .= $dx;
    }
    $key = substr($salted, 0, 32);
    $iv  = substr($salted, 32,16);
    $encrypted_data = openssl_encrypt(json_encode($value), 'aes-256-cbc', $key, true, $iv);
    $data = array("ct" => base64_encode($encrypted_data), "iv" => bin2hex($iv), "s" => bin2hex($salt));
    return json_encode($data);
}

//echo cryptoJsAesEncrypt('mz5','i am here');



$pgmid = $_POST['pgmid'];
//echo$pgmid."<BR>";
$pgmid_len = iconv_strlen(trim($pgmid));
//echo$pgmid_len."<BR>";
$tosearchid = $pgmid . str_repeat("0", 10 - $pgmid_len);
//echo $tosearchid."<BR>"; 

//$study_name=$_SESSION["study"];
$ses_sql = $conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0' ");

// print_r($ses_sql);
$finfo = $ses_sql->fetch_fields();

$param_count = 0;
foreach ($finfo as $val) {
    $valname  = $val->name;
    if(trim(substr($valname, 0, 10)) == trim('parameter_')) {
        $param_count++;
    }
}
$param_count_name = $param_count/2;

while ($row = $ses_sql->fetch_assoc()) {

    $title = $row['title'];
    $footnote = $row['footnote'];
    //$title = cryptoJsAesEncrypt('mz5','i am here') ;
    $pgmname = $row['pgmname'];
    $outname = $row['outname'];
    $logname = $row['logname'];
    $section = $row['section'];
    $type = $row['type'];
    $tlfnum = $row['tlfnum'];
    $pgmloc = $row['pgmloc'];
    $outloc = $row['outloc'];
    $outno = $row['outno'];
    $population = $row['population'];
    $shell_name = $row['shell_name'];
    $param_list_count = 0;
    $param_list='';
    for($i=1;$i<=$param_count_name;$i++){
       if($row['parameter_'.$i.'_name'] <> '') {
           $param_list_count++;
           $param_list =  $param_list.'"parameter_'.$i.'_name":"' . $row['parameter_'.$i.'_name'] . '",'.'"parameter_'.$i.'_value":"' . $row['parameter_'.$i.'_value'] . '",';
       }
    }
}

$result = '{"success":1,
		"param_list_count":"' . $param_list_count . '",
		"title":"' . $title . '",
		"footnote":"' . $footnote . '",
		"logname":"' . $logname . '",
		"pgmname":"' . $pgmname . '",
		"outname":"' . $outname . '",
		"section":"' . $section . '",
		"type":"' . $type . '",
		"tlfnum":"' . $tlfnum . '",
		"pgmloc":"' . $pgmloc . '",
		"outloc":"' . $outloc . '",
		"population":"' . $population . '",
		"shell_name":"' . $shell_name . '",
		"outno":"' . $outno . '",
		'.$param_list.'
		"error":null}';
header('Content-type: application/json;');
echo $result;
?>


