<?php
include("../include/connect.php");
$allocationID=$_REQUEST['allocationID'];
$preRoleID=$_REQUEST['preRoleID'];
$preStatus=$_REQUEST['preStatus'];
$user_id=$_REQUEST['user_id'];
$newStudy=$_REQUEST['newStudy'];
$newRole=$_REQUEST['newRole'];

date_default_timezone_set('Asia/Dhaka');
$today = date("Y-m-d H:i:s");
$test=" ";
foreach($allocationID as $key=>$value)
{
    $sql="SELECT * FROM study_allocation WHERE allocation_id='$value'";
    $result=$conn->query($sql);

    while($row = $result->fetch_assoc()){
        if($preRoleID[$key]!=$row['user_type'] || $preStatus[$key]!=$row['status']){
            if($preStatus[$key]=="ON")
                $updateSql="UPDATE study_allocation SET allocation_date='$today',stop_date='null',status='$preStatus[$key]',user_type='$preRoleID[$key]' WHERE allocation_id='$value'";
            else
                $updateSql="UPDATE study_allocation SET stop_date='$today',status='$preStatus[$key]',user_type='$preRoleID[$key]' WHERE allocation_id='$value'";

            $conn->query($updateSql);
        }
    }
}

if(!empty($newStudy) && !empty($newRole)){
    $insertSql="INSERT INTO study_allocation(user_id,study_id,allocation_date,status,user_type) VALUES ('$user_id','$newStudy','$today','ON','$newRole')";
    $conn->query($insertSql);
}

echo json_encode("OK");

?>