<?php
include("../include/connect.php");
include("../include/custom_funtions.php");
session_start();
$study_name = $_SESSION["study"];
$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today = date("Y-m-d H:i:s");
$date = date("Y-m-d H:i:s");

$data = array();
//var_dump($_POST);

if (isset($_POST["edit_sid"])) {
    $pgmid = $_POST["edit_sid"];
    $title = $_POST["title"];
    $footnote = $_POST["footnote"];
    $pgmname = $_POST["pgmname"];
    $outname = $_POST["outname"];
    $logname = $_POST["logname"];
    $type = $_POST["type"];
    $tlfnum = $_POST["tlfnum"];
    $pgmloc = $_POST["pgmloc"];
    $outloc = $_POST["outloc"];
    $population = $_POST["population"];
    $outno = $_POST["outno"];
    $shell_name = $_POST["shell_name"];
    $param_list_count = $_POST["param_list_count"];
    $param_list_count_previous = $_POST["param_list_count_previous"];

    //echo $param_list_count;exit;
    $param_name_list='';
    $param_value_list='';
    for($i=1;$i<=$param_list_count;$i++){
        $paramname="parameter_".$i."_name";
        $$paramname = $_POST['parameter_'.$i.'_name'];

        $paramvalue="parameter_".$i."_value";
        $$paramvalue = $_POST['parameter_'.$i.'_value'];

//        echo "parameter_".$i."_value = '$$paramvalue',";
        $param_value_list =  $param_value_list."parameter_".$i."_value = '".str_replace("'","''",$$paramvalue)."',";

//        echo "parameter_".$i."_value = '$$paramvalue',";
        $param_name_list =  $param_name_list."parameter_".$i."_name = '".str_replace("'","''",$$paramname)."',";

    }

    if($param_list_count<$param_list_count_previous)
    {
        for($i=$param_list_count+1;$i<=$param_list_count_previous;$i++){
            $paramname="parameter_".$i."_name";
            $$paramname ='';

            $paramvalue="parameter_".$i."_value";
            $$paramvalue ='';

            $param_value_list =  $param_value_list."parameter_".$i."_value = '".$$paramvalue."',";

            $param_name_list =  $param_name_list."parameter_".$i."_name = '".$$paramname."',";

        }
    }
    //$filter_value1 = str_replace("'","''",$param_list);
    //echo $param_list;
    $pgmid_len = iconv_strlen(trim($pgmid));
    //echo$pgmid_len."<BR>";
    $tosearchid = $pgmid . str_repeat("0", 10 - $pgmid_len);
    //echo $tosearchid."<BR>";

    $result = $conn->query("UPDATE toc_$study_name
              SET title = '$title',
                footnote = '$footnote',
                pgmname = '$pgmname',
                outname='$outname',
                logname = '$logname',
                type='$type',
                tlfnum='$tlfnum',
                shell_name='$shell_name',
                population='$population',".$param_name_list."
                pgmloc = '$pgmloc',
                outloc='$outloc',".$param_value_list."
                outno='$outno'
              WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");
    $title_text="";
    if ($result) {
        $success = "1";

        $result = $conn->query("SELECT * FROM toc_$study_name WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");
        while ($row = $result->fetch_assoc()) {
            $title_text = $row["type"] . " " . $row["tlfnum"] . " - " . $row["title"];
        }

        $result555 = $conn->query("SELECT * FROM toc_status_$study_name WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");
        while ($row = $result555->fetch_assoc()) {
            $prev_outstat = $row["outstat"];
        }

    } else {
        $success = "0";
    }
    if ($prev_outstat==0){
        $result=$conn->query("UPDATE toc_status_$study_name SET outstat = 0 , odate_0 = '$today'  WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");

    }else{
        $result=$conn->query("UPDATE toc_status_$study_name SET outstat = 1 , odate_0 = '$today'  WHERE sortorder = '$tosearchid' AND data_currency='SP0' ");

    }


    $sql_status=$conn->query("SELECT COUNT(*) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='SP0'");

    while($row = $sql_status->fetch_assoc()) {
        $totalpgm = $row['totalpgm'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 0  AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
        $nopgm = $row['nopgm'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x  WHERE pgmstat = 1  AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
        $indevpgm = $row['indevpgm'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 2  AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
        $tvalpgm = $row['tvalpgm'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 3  AND data_currency='SP0'");

    while($row = $sql_status->fetch_assoc()) {
        $valpgm = $row['valpgm'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 0  AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
        $nout = $row['nout'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 1  AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
        $oout = $row['oout'];
    }
    $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 2 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
        $cout = $row['cout'];
    }

    $result9 = $conn->query("SELECT * FROM toc_status_$study_name WHERE sortorder LIKE '$tosearchid%'  AND data_currency='SP0' ");
    while($row = $result9->fetch_assoc()) {
        $pgmstat=$row["pgmstat"];
        $outstat=$row["outstat"];
    }

    $result10 = $conn->query("SELECT * FROM toc_$study_name  WHERE data_currency='SP0' ORDER BY sortorder ASC");
    $treeorder=0;
    $dtreeId="0";
    while($row = $result10->fetch_assoc()) {
        $treeorder=$treeorder+1;
        $pgmname2=$row["pgmname"];

        if($pgmname2 == $pgmname){
            $dtreeId=$dtreeId.'#'.$treeorder;
        }
    }


    $result = '{
              "success":"'.$success.'",
              "title_text":"'.$title_text.'",
              "nopgm":"'. $nopgm.'",
              "indevpgm":"'. $indevpgm.'",
              "tvalpgm":"'. $tvalpgm.'",
              "valpgm":"'. $valpgm.'",
              "nout":"'. $nout.'",
              "oout":"'. $oout.'",
              "cout":"'. $cout.'",
              "pgmstat":"'. $pgmstat.'",
              "outstat":"'. $outstat.'",
              "dtreeId":"'. $dtreeId.'"
              }';


    toc_gen($conn);

    header('Content-type: application/json;');
    echo $result;
}

//$data = array('success' => $success, 'title_text' => $title_text);



?>