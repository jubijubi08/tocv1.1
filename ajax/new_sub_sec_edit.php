<?php
include("../include/connect.php");
session_start();
$study_name=$_SESSION["study"];
$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");
$success="1";
$data = array();
//code for insert new sub section
if(isset($_POST["sub_sec_id"]))  {

    $section = $_POST['sub_sec_name'];
    $pgmid = $_POST["sub_sec_id"];

    $pgmid_len=iconv_strlen (trim($pgmid));
    if($pgmid_len>2){
        $masterid=substr($pgmid,0,$pgmid_len-2);
    }
    else{
        $masterid=$pgmid;
    }

    // $pgmid_to_be_add = $pgmid+1;
    $pgmid_len=iconv_strlen (trim($pgmid));
    $tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);
    $pgmid_len1=($pgmid_len/2)+1;
    //echo "selected sortorder for search : ".$tosearchid."<br>";

    //select row information
    $result_rinfo=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0'");

    while($row_rinfo = $result_rinfo->fetch_assoc()) {
        $l1 = $row_rinfo['l1'];
        $l2 = $row_rinfo['l2'];
        $l3 = $row_rinfo['l3'];
        $l4 = $row_rinfo['l4'];
        $l5 = $row_rinfo['l5'];
    }
    //echo "l1: ".$l1." l2: ".$l2." l3:".$l3." l4:".$l4." l5:".$l5."<br>";

    $sql_max="SELECT MAX(l$pgmid_len1) as lvl FROM toc_$study_name WHERE sortorder LIKE '$pgmid%' AND data_currency='SP0' ORDER BY sortorder ASC";
    $result1_max = $conn->query($sql_max);
    while($row_rinfo = $result1_max->fetch_assoc()) {
        $maxvalue = $row_rinfo['lvl']+1;
    }
    //echo '<br>total child number : '.$maxvalue.'<br>';

    if(iconv_strlen (trim($maxvalue))>1){
        $maxvalue1=$maxvalue;
    }
    else{
        $maxvalue1=trim("0".$maxvalue);
    }

    if(($pgmid_len/2)==1)
        $l2 = $maxvalue1;
    if(($pgmid_len/2)==2)
        $l3 = $maxvalue1;
    if(($pgmid_len/2)==3)
        $l4 = $maxvalue1;
    if(($pgmid_len/2)==4)
        $l5 = $maxvalue1;

    $newsortid=$pgmid.$maxvalue1.str_repeat("0",10-$pgmid_len-2);

    //insert into toc table;
    $sql5="INSERT INTO toc_$study_name (data_currency,study,sortorder,l1,l2,l3,l4,l5,section) VALUES ('SP0','$study_name','$newsortid','$l1','$l2','$l3','$l4','$l5','$section') ";
    $result5 = $conn->query($sql5);
    //if($result5){echo "iNSERT into toc table --> SUCCESS<BR>";}else{echo "<BR>iNSERT FAILED<BR>";}
    //insert into toc status table
    date_default_timezone_set('Asia/Dhaka');
    $today = date("Y-m-d H:i:s");
    $sql6="INSERT INTO toc_status_$study_name (data_currency,study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('SP0','$study_name','$newsortid','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
    $result6 = $conn->query($sql6);
    if($result6){ $success="1"; } else{ $success="0"; }
}

$data = array('success' => $success );
echo json_encode($data);
?>