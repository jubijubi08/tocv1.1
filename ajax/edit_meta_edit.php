<?php
include("../include/connect.php");
session_start();
$study_name	= $_SESSION["study"];
$username 	= $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today			= date("Y-m-d H:i:s");
$date 			= date("Y-m-d H:i:s");
$data 			= array();
$success 		= $newVal = "";
//var_dump($_POST);
$action = $_POST["action"];

if($action == "update"){
    $id 				= $_POST["id"];
    $name 			= $_POST["Names"];
    $value 			= $_POST["fieldvalue"];
    $prevValue 	= $_POST["prevValue"];
    if(isset($_POST["InSearch"]))
        $search 	= $_POST["InSearch"];
    else $search = "No";

    $sql = $conn->query("SELECT fieldname, searchtitle,value FROM metadata_$study_name  WHERE id ='$id'");
    $fieldName = $originalValue = $prevSearchTitle = "";
    while($row = $sql->fetch_assoc()){
        $fieldName 		= $row["fieldname"];
        $originalValue 		= $row["value"];
        $prevSearchTitle 	= $row["searchtitle"];
    }
    if($search 	= "Yes")
        $srchTitle	= $_POST["searchtitle"];
    else
        $srchTitle	= "";

    if($originalValue != $value)
    {
        $result 		= $conn->query("UPDATE metadata_$study_name
		          			SET is_current = 'NO' WHERE id ='$id'");
        if($result){
            $result2		= $conn->query("INSERT INTO metadata_$study_name (fieldname, searchtitle, value, action, is_current, updated_at, updated_by, new_value, previous_value)
		VALUES ('$name', '$srchTitle', '$value', 'Updated', 'YES', '$date', '$username', '$value', '$prevValue')");
            if($result2){
                $result3 	= $conn->query("UPDATE toc_$study_name
		          			SET $name = '$value' WHERE title !=''");
                if($result3){
                    $success 	= 1;
                    $newVal 	= "<tr><td class='fieldname'>$name</td><td  class='searchtitle'>$srchTitle</td><td class='value'>$value</td>td><a class='btn btn-warning' target='_blank' href='meta_history.php?data=$name' ><span class='fa fa-history'></span></a>&nbsp;&nbsp;<a class='btn btn-info edit' href='#' data-id='3'><span class='glyphicon glyphicon-pencil'></span></a>&nbsp;&nbsp;<a class='btn btn-warning delete' href='#' data-id='3'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
                }else $success = 0;
            }else $success = 0;
        }else $success = 0;
    }else{
        $result 		= $conn->query("UPDATE metadata_$study_name
		          			SET searchtitle = '$srchTitle' WHERE id ='$id'");
        if($result){
            $success 	= 1;
            $newVal 	= "<tr><td class='fieldname'>$name</td><td class='searchtitle'>$srchTitle</td><td class='value'>$value</td><td><a class='btn btn-warning' target='_blank' href='meta_history.php?data=$name' ><span class='fa fa-history'></span></a>&nbsp;&nbsp;<a class='btn btn-info edit' href='#' data-id='$id'><span class='glyphicon glyphicon-pencil'></span></a>&nbsp;&nbsp;<a class='btn btn-warning delete' href='#' data-id='$id'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
        }
    }
}else if($action == "insert"){
    $name 			= $_POST["NameValue"];
    $value 			= $_POST["fieldvalue"];
    if(isset($_POST["InSearch"]))
        $search 	= $_POST["InSearch"];
    else $search = "No";
    if($search 	= "Yes")
        $srchTitle	= $_POST["searchtitle"];
    else
        $srchTitle	= "";

    $result = $conn->query("SHOW COLUMNS FROM toc_$study_name LIKE '$name'");
    $resultCount = $result->num_rows;

    if($resultCount==0)
    {
        $result			= $conn->query("INSERT INTO metadata_$study_name (fieldname, searchtitle, value, action, is_current, created_at, created_by, new_value)
	    VALUES ('$name', '$srchTitle', '$value', 'New', 'YES', '$date', '$username', '$value')");
        $last_id = $conn->insert_id;
        if($result){;
            $result2			= $conn->query("ALTER TABLE toc_$study_name ADD COLUMN $name VARCHAR(250)  NULL AFTER status");
            if($result2){
                $result3 	= $conn->query("UPDATE toc_$study_name
	          			SET $name = '$value' WHERE title !=''");
                if($result3){
                    $success 	= 1;
                    $newVal 	= "<tr><td class='fieldname'>$name</td><td class='searchtitle'>$srchTitle</td><td class='value'>$value</td><td><a class='btn btn-warning' target='_blank' href='meta_history.php?data=$name' ><span class='fa fa-history'></span></a>&nbsp;&nbsp;<a class='btn btn-info edit' href='#' data-id='$last_id'><span class='glyphicon glyphicon-pencil'></span></a>&nbsp;&nbsp;<a class='btn btn-warning delete' href='#' data-id='$last_id'><span class='glyphicon glyphicon-trash'></span></a></td></tr>";
                }else $success = 0;
            }else $success = 0;
        }else $success = 0;

    }else{
        $success = 0;
    }


}else{
    $id 		= $_POST["id"];
    $result = $conn->query("UPDATE metadata_$study_name SET is_current = 'NO', action = 'Deleted' WHERE id ='$id'");
    if($result){
        $result2 = $conn->query("SELECT fieldname FROM metadata_$study_name  WHERE id ='$id'");
        while($row = $result2->fetch_assoc())$fieldName = $row["fieldname"];

        $result3 	= $conn->query("ALTER TABLE toc_$study_name DROP COLUMN $fieldName");
        if($result3){
            $success 	= "1";
            $newVal 	= "";
        }else $success = 0;
    }else $success = 0;
}

$data = array('success' => $success, 'value' =>$newVal );

echo json_encode($data);

?>