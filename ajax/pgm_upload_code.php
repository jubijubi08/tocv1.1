<?php

//Upload Program;
if(isset($_POST["submit"])) {
	$study_name=$_SESSION["study"];
	$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
	while($row = $result44->fetch_assoc()) {
		$file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
	}
	$pgmid = $_POST["dl_sid"];

	$today = date("Y-m-d H:i:s");

	$pgmid_len=iconv_strlen (trim($pgmid));
	//echo$pgmid_len."<BR>";
	$tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);
	//echo $tosearchid."<BR>"; 
										
	$result=$conn->query("SELECT pgmname,pgmloc FROM toc_$study_name WHERE sortorder = '$tosearchid' AND data_currency='C' ");
	while($row = $result->fetch_assoc()) {
		$pgmname = $row['pgmname'];
		$pgmloc = $row['pgmloc'];
	}
	    //echo $pgmname. " == ". $pgmloc." == ";
	    $dirchk = $file_server.$study_name."/";

	$target_dir = $file_server.$study_name."/".$pgmloc;
		// echo" == target dic: ".$target_dir." == ";

	if ($_FILES["fileToUpload"]["name"] != "") {
			$given_file = basename($_FILES["fileToUpload"]["name"]);
	            //echo " == ".$given_file." == ";

			$target_file = $target_dir;

			$uploadOk = 1;

			$FileType = pathinfo($target_file, PATHINFO_EXTENSION);

			// Check if file name matches with TOC pgmname
			if($pgmname != $given_file){
					echo '<div class="alert alert-warning" >
															<span class="glyphicon glyphicon-ban-circle" aria-hidden="true"></span>
															<span class="sr-only">Mismatch:</span>
															IN TOC file name : '.$pgmname.' But selected : '.$given_file.'
															</div>';
					echo '<div class="alert alert-danger" role="alert">
															<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
															<span class="sr-only">Error:</span>
															Sorry, file name does not match. ! Please upload correct file
															</div>';
					$uploadOk = 0;
			}

			// Check if file already exists
			//                                  if (file_exists($target_file)) {
			//                                      echo '<div class="alert alert-danger" role="alert">
			//                                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
			//                                                          <span class="sr-only">Error:</span>
			//                                                          Sorry, file already exists. ! but replaced by new one
			//                                                          </div>';
			//                                      $uploadOk = 1;
			//                                  }
			// Check file size
			if ($_FILES["fileToUpload"]["size"] > 500000) {
					echo "Sorry, your file is too large.";
					$uploadOk = 0;
			}
			// Allow certain file formats
			if ($FileType != "sas" ) {
					echo '<div class="alert alert-danger" role="alert">
															<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
															<span class="sr-only">Error:</span>
															Sorry, Only sas files are allowed !
															</div>';
					$uploadOk = 0;
			}
			// Check if $uploadOk is set to 0 by an error
			if ($uploadOk == 0) {
					echo '<div class="alert alert-danger" role="alert">
					<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
					<span class="sr-only">Error:</span>
					Sorry, your file was not uploaded !
					</div>';
					// if everything is ok, try to upload file
			}else{
				if(file_exists($dirchk)){
					if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						echo "<div class='alert alert-success'>The file ". basename($_FILES["fileToUpload"]["name"]) ." has been uploaded.</div>";
						
						$result=$conn->query("SELECT pgmstat FROM toc_status_$study_name WHERE sortorder = '$tosearchid' AND data_currency='C' ");
						while ($row=$result->fetch_assoc()) {
							$pgmstat=$row['pgmstat'];
						}

						date_default_timezone_set('Asia/Dhaka');
				     	$datetime=date("Y_m_d_H_i_s");
						$filelink=$datetime.'_'.$pgmname;
						
						//Copying current pgm file to the pgm/history directory;
					  	function copyToDir($pattern, $dir, $study_name,$pgmname,$file_server){
					      	foreach (glob($pattern) as $file) 
					      	{
					          	if(!is_dir($file) && is_readable($file)) 
					          	{
					              	$dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
					              	copy($file, $dest);
					              	date_default_timezone_set('Asia/Dhaka');
								  	$datetime=date("Y_m_d_H_i_s");
								  	$sepdotfrmfile=substr_replace($file," ",strripos($file,$pgmname));
					             	  //rename('study/'.$study_name.'/backup/'.$pgmname, 'study/'.$study_name.'/backup/'.$datetime.'_'.$pgmname);
					             	 rename(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$datetime.'_'.$pgmname));
					          	}
					      	}             
					  	}
					  	//copyToDir('study/'.$study_name.'/'.$pgmloc, 'study/'.$study_name.'/backup/',$study_name,$pgmname);
					  	copyToDir(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$pgmloc), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'),$study_name,$pgmname,$file_server);


						if($pgmstat!=0){
	 						$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
	 						VALUES ('$tosearchid','$pgmname','$today','$username','Uploaded','In Development','$filelink')");	
						}else{
	 						$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
	 						VALUES ('$tosearchid','$pgmname','$today','$username','First Uploaded','In Development','$filelink')");	
						}															


						$result=$conn->query("UPDATE toc_status_$study_name SET pgmstat = 1 , pdate_1 = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmloc='$pgmloc') AND data_currency='C' ");
						$result=$conn->query("SELECT outstat FROM toc_status_$study_name WHERE sortorder = '$tosearchid' AND data_currency='C' ");
						while ($row=$result->fetch_assoc()) {
							$outstat=$row['outstat'];
						}
						if($outstat!=0){
							$result=$conn->query("UPDATE toc_status_$study_name SET outstat = 1 , odate_0 = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmloc='$pgmloc') AND data_currency='C' ");
						}
						// date_default_timezone_set('Asia/Dhaka');
				  //    	$datetime=date("Y_m_d_H_i_s");
						// $filelink=$datetime.'_'.$pgmname;															
 					// 	$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
 					// 	VALUES ('$tosearchid','$pgmname','$today','$username','First Uploaded','In Development','$filelink')");								
					}
					else{
						echo '<div class="alert alert-danger" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
						<span class="sr-only">Error:</span>
						Sorry, there was an error uploading your file !
						</div>';
					}
				}else{
					echo '<div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Sorry, Your study folder does not exist in server, Please create study folder first.
                            like  "'.$file_server.$study_name.'"
                            </div>';
                    $uploadOk = 0;   
				} 	
		}
	}
	else {
		echo '<div class="alert alert-danger" role="alert">
		<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
		<span class="sr-only">Error:</span>
		No file selected !
		</div>';
	}

	
	//$_POST = array();
	//print_r($_POST);
	//reset($_POST);

}

?>									