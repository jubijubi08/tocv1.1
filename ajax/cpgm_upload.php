<?php 
include("../include/connect.php");
session_start();
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");
$study_name=$_SESSION["study"];

$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}


$username = $_SESSION['login_user'];
$pgm_loc = $_POST['pgm_loc'];

if(trim($pgm_loc) == ''){
    $pgm_loc = 'pgm/';
}
//echo $pgm_loc;

$given_file = basename($_FILES["fileToUpload"]["name"]);
$cp_name=$given_file;

$cp_loc = $file_server.$study_name.DIRECTORY_SEPARATOR.$pgm_loc;

if (!file_exists($cp_loc)) {
    mkdir($cp_loc, 0777, true);
    //echo "not exist >> created now";
}else{
    //echo "exist";
}


//$dirchk = $file_server.$study_name."/";
//
//$target_dir = $file_server.$study_name."/".$pgmloc;
//	// echo" == target dic: ".$target_dir." == ";

$data = array();
$issue ="";
$success=0;
if ($_FILES["fileToUpload"]["name"] != "") {

	$given_file = basename($_FILES["fileToUpload"]["name"]);
    //echo " == ".$given_file." == ";

	$target_file = $cp_loc.DIRECTORY_SEPARATOR.$cp_name;
	$uploadOk = 1;
	$FileType = pathinfo($target_file, PATHINFO_EXTENSION);

	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 500000) {
	    $issue = $issue.'2';
		$uploadOk = 0;
	}
	// Allow certain file formats
	if ($FileType != "sas" ) {
		$issue = $issue.'3';
			$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		$issue = $issue.'4';
	// if everything is ok, try to upload file
	}else{
		if(file_exists($cp_loc)){
			if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {

                //echo "<div class='alert alert-success'>The file ". basename($_FILES["fileToUpload"]["name"]) ." has been uploaded.</div>";

                date_default_timezone_set('Asia/Dhaka');
                $cp_date = date("Y-m-d H:i:s");
				$result124=$conn->query("SELECT * FROM cplist_$study_name WHERE cpname = '$cp_name'");
                if(mysqli_num_rows($result124) > 0 ){
                    while($row = $result124->fetch_assoc()) {
                        $sortorder = $row['sortorder'];
                    }
                    //echo "inside count proram id db or not";
                    $result=$conn->query("UPDATE cplist_$study_name SET pgmstat = 1 , status = 'In Development' , cpdate  = '$cp_date' , programmer = '$username' ,status_date = '$cp_date' , validator = ''  WHERE cpname  = '$cp_name' ");
                    if($result){
                        //echo "sucse to update";
                    }else{
                        //echo " no sucse to update";
                    }
                }else{
                    $result = $conn->query("INSERT INTO cplist_$study_name (study,cploc,cpname,programmer,cpdate,pgmstat,status,validator,status_date) VALUES
                                                        ('$study_name','$pgm_loc','$cp_name','$username','$cp_date',1,'In Development',' ','$cp_date')");
                    if($result){
                        //echo "sucse to in";
                    }else{
                        //echo " no sucse to in";
                    }
                }

                if($result){
                    $success=1;
                }else{
                    $success=0;
                }
                //Copying current pgm file to the pgm/history directory;
                function copyToDir($pattern, $dir, $study_name,$pgmname,$file_server,$file_loc){
                    foreach (glob($pattern) as $file)
                    {
                        if(!is_dir($file) && is_readable($file))
                        {
                            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
                            copy($file, $dest);
                            date_default_timezone_set('Asia/Dhaka');
                            $datetime=date("Y_m_d_H_i_s");
                            $sepdotfrmfile  =substr_replace($file," ",strripos($file,$pgmname));
                            //rename('study/'.$study_name.'/backup/'.$pgmname, 'study/'.$study_name.'/backup/'.$datetime.'_'.$pgmname);
                            rename(trim($file_server.DIRECTORY_SEPARATOR.$study_name.'/backup/'.$file_loc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.'/backup/'.$file_loc.DIRECTORY_SEPARATOR.$datetime.'_'.$pgmname));
                        }
                    }
                }

                $cp_loc_chk = $file_server.DIRECTORY_SEPARATOR.$study_name.'/backup/'.$pgm_loc;
                if (!file_exists($cp_loc_chk)) {
                    mkdir($cp_loc_chk, 0777, true);
                    //echo "not exist >> created now";
                }else{
                    //echo "exist";
                }

                $file_loc = $pgm_loc;
                $pgmname = $cp_name;
                date_default_timezone_set('Asia/Dhaka');
                $datetime=date("Y_m_d_H_i_s");
                $filelink=$datetime.'_'.$pgmname;
                copyToDir(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$file_loc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'backup'.DIRECTORY_SEPARATOR.$file_loc),$study_name,$pgmname,$file_server,$file_loc);
                $result=$conn->query("INSERT INTO cpgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
							                                        VALUES ('444','$pgmname','$today','$username','Uploaded','In Development','$filelink')");


			}
			else{
				$issue.'6';
			}
		}else{
			$issue.'5';
            $uploadOk = 0;
		}
	}
}
else {
	$issue.'7';
}


$data = array('success' => $success,
			 'issue' => $issue,
			 'file_server' => $file_server,
			 'study_name' => $study_name,
			 'pgmname' => $cp_name,
			 'given_file' => $given_file,
			 'pgmstat' => 1
			 );

echo json_encode($data);
?>