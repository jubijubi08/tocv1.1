<?php 
include("../include/connect.php");
session_start();
$study_name=$_SESSION["study"];
$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");

//Copying all log file to the pgm directory;
function copyToDir($pattern, $dir){
    foreach (glob($pattern) as $file){
        if(!is_dir($file) && is_readable($file))
        {
            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
            copy($file, $dest);
            //unlink($file);
        }
    }
}

$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
  $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}
$data = array();
//var_dump($_POST);

$snap_name=$_POST["snap_name"];
$max_row_id=$_POST["max_row_id"];
$tot_row=$_POST["tot_row"];
$sql_query='';
//echo $max_row_id." ".$tot_row;
for ($i=0; $i < $tot_row; $i++) {

  $lib_name=$_POST["lib_name_".$i];
  // $lib_loc=str_replace("'","''",$_POST["lib_location_".$i]);
  $lib_loc=str_replace("\\","\\\\",trim($_POST["lib_location_".$i]));
  // $lib_loc=$_POST["lib_location_".$i];
  $sql_query.='( '.$max_row_id.', "'.$snap_name.'", "'.$lib_name.'", "'.$lib_loc.'", "'.$today.'", "'.$username.'"),';

}
 $sql_query1='INSERT INTO snap_'.$study_name.' (id, snap_name, lib_name, lib_loc, created_at, created_by) VALUES '.substr($sql_query, 0, -1).' ';

 $result = $conn->query($sql_query1);

 //echo("Error description: " . mysqli_error($conn));
 //echo  $sql_query1;


if($result){
  $success="1";


  //copy log file to required location
  copyToDir($file_server.$study_name.DIRECTORY_SEPARATOR.'pgm'.DIRECTORY_SEPARATOR.'studyini.sas', $file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'pgm');


  //check snap and update studyauto pgm
  $reading = fopen($file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'pgm/studyini.sas', 'r');
  $writing = fopen($file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'pgm/studyini.sas.tmp', 'w');

  $replaced = false;

  while (!feof($reading)) {
    $line = fgets($reading);

    for ($i=0; $i < $tot_row; $i++) {
        $lib_name=$_POST["lib_name_".$i];
        //$lib_loc=str_replace("'","''",$_POST["lib_location_".$i]);
        //$lib_loc=str_replace("\\","\\\\",trim($_POST["lib_location_".$i]));
        $lib_loc=$_POST["lib_location_".$i];

          if (stristr(strtoupper($line),'LIBNAME '.strtoupper($lib_name))) {
          $line = "LIBNAME ".strtoupper($lib_name)."  ".$lib_loc.";\n";
          $replaced = true;
        }
    }


    fputs($writing, $line);
  }


  fclose($reading); fclose($writing);
  // might as well not overwrite the file if we didn't replace anything
  if ($replaced)
  {
    rename($file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'pgm/studyini.sas.tmp', $file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'pgm/studyini.sas');
  } else {
    unlink($file_server.$study_name.DIRECTORY_SEPARATOR.$snap_name.DIRECTORY_SEPARATOR.'pgm/studyini.sas.tmp');
  }



  // $sql_query1="SELECT * FROM  snap_$study_name WHERE snap_name='$snap_name'";
  // $result2 = $conn->query($sql_query1);
  // while($row = $result2->fetch_assoc()){
  //   $sp_id = $row['id'];
  // }

  // $folder_name = 'snapshot'.$sp_id;
  // $sp_loc = $file_server.$study_name.DIRECTORY_SEPARATOR.$folder_name;

  // if (!file_exists($sp_loc)) {
  //   mkdir($sp_loc, 0777, true);
  //   mkdir($sp_loc.DIRECTORY_SEPARATOR.'lst', 0777, true);
  //   mkdir($sp_loc.DIRECTORY_SEPARATOR.'pgm', 0777, true);
  //   mkdir($sp_loc.DIRECTORY_SEPARATOR.'pgm'.DIRECTORY_SEPARATOR.'ctr', 0777, true);
  //   //echo "not exist >> created now";
  // }else{
  //   //echo "exist";
  // }

}else{
  $success="0";
}


$sql_query2="SELECT DISTINCT id, snap_name, is_lock, created_at, created_by , updated_at , updated_by FROM  snap_$study_name";
$result = $conn->query($sql_query2);

$temp="";
while($row = $result->fetch_assoc()){
  $temp.='{"snap_name":"'.$row['snap_name'].'","id":"'.$row['id'].'","is_lock":"'.$row['is_lock'].'","created_at":"'.$row['created_at'].'","created_by":"'.$row['created_by'].'","updated_at":"'.$row['updated_at'].'","updated_by":"'.$row['updated_by'].'","id":"'.$row['id'].'","success":"'.$success.'"},';

}
// echo "$temp";
$temp2=substr($temp, 0, -1);




//sortorder   pgmname   event_date  username  comment   status  link


header('Content-type: application/json;');
header('Access-Control-Allow-Headers: Content-Type;');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS;');

//echo "[".$temp2."]";

$data = array('success' => $success );

echo json_encode("[".$temp2."]");

?>