<?php 
include("../include/connect.php");
session_start();
$study_name=$_SESSION["study"];

$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

$username = $_SESSION['login_user'];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");
$datetime=date("Y_m_d_H_i_s");

$data = array();
//var_dump($_POST);

$pgmid = $_POST["sid"];

$cng_vs=$_POST["pgm_status"];
if($cng_vs==2){
	$stat=$cng_vs;
}else{
    $valstat=$_POST["valstat"];
    if($valstat==1){
    	$stat=3;
    }
    elseif ($valstat==0){
     	$stat=1;
    } 
}

$pfcmt=$_POST['pfcmt'];
$v_sys=$_POST['v_sys'];

$Validation_system="";
if(!empty($_POST['v_sys'])) {
	foreach($_POST['v_sys'] as $check){
    if($check == 0){
      $vs_text = "Independent Programming";
    }
    elseif ($check == 1) {
      $vs_text = "Manual Code check";
    }
    else{
      $vs_text = "Manual Output Check";
    }
        $Validation_system =  $Validation_system.",".$vs_text;
	}
}
$Validation_systems = substr($Validation_system,1);

$pgmid_len=iconv_strlen (trim($pgmid));
//echo$pgmid_len."<BR>";
$tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);
//echo $tosearchid."<BR>"; 

$ses_sql=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid' AND data_currency='SP0' ");

while($row = $ses_sql->fetch_assoc()) {
    $pgmname = $row['pgmname'];  
    $pgmloc = $row['pgmloc']; 
    $logname = $row['logname'];                      
}

//Copying current pgm file to the pgm/history directory;
	function copyToDir($pattern, $dir, $study_name,$pgmname,$file_server,$file_loc){
   	foreach (glob($pattern) as $file) 
   	{
       	if(!is_dir($file) && is_readable($file)) 
       	{
           	$dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
           	copy($file, $dest);
           	date_default_timezone_set('Asia/Dhaka');
		  	$datetime=date("Y_m_d_H_i_s");
		  	$sepdotfrmfile=substr_replace($file," ",strripos($file,$pgmname));
          	  //rename('study/'.$study_name.'/backup/'.$pgmname, 'study/'.$study_name.'/backup/'.$datetime.'_'.$pgmname);
          	 rename(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.'/'.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.'/'.$datetime.'_'.$pgmname));
       	}
   	}             
	}

   $file_loc = $pgmloc;
   copyToDir(trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.$pgmloc.DIRECTORY_SEPARATOR.$pgmname), trim($file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc),$study_name,$pgmname,$file_server,$file_loc);

$result=$conn->query("UPDATE toc_status_$study_name SET pgmstat = '$stat', pdate_$stat = '$today'  WHERE sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmname='$pgmname' AND data_currency='SP0') AND data_currency='SP0' ");

if($result){
	$success="1";
	$filelink=$datetime.'_'.$pgmname;
	//echo $stat;
	if($stat==1)$status="In Development";
		if($stat==2)$status="To Be validated";
		if($stat==3)$status="Validated";
		$result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link,validation_system,data_currency)
							VALUES ('$tosearchid','$pgmname','$today','$username','$pfcmt','$status','$filelink','$Validation_systems','SP0')");	
}else{
	$success="0";
}


  $sql_status=$conn->query("SELECT COUNT(*) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $totalpgm = $row['totalpgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 0 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $nopgm = $row['nopgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x  WHERE pgmstat = 1 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $indevpgm = $row['indevpgm'];                      
    }                 
  $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 2 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $tvalpgm = $row['tvalpgm'];                      
    }                 
  $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 3 AND data_currency='SP0'");

    while($row = $sql_status->fetch_assoc()) {
                  $valpgm = $row['valpgm'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 0 AND data_currency='SP0'");

    while($row = $sql_status->fetch_assoc()) {
                  $nout = $row['nout'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 1 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $oout = $row['oout'];                      
    }
  $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder   AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 2 AND data_currency='SP0' ");

    while($row = $sql_status->fetch_assoc()) {
                  $cout = $row['cout'];                      
    }

  $result9 = $conn->query("SELECT * FROM toc_status_$study_name WHERE sortorder LIKE '$tosearchid%' AND data_currency='SP0' ");
    while($row = $result9->fetch_assoc()) {
                 $pgmstat=$row["pgmstat"];
                 $outstat=$row["outstat"];                     
    }

  $result10 = $conn->query("SELECT * FROM toc_$study_name WHERE data_currency='SP0' ORDER BY sortorder ASC");
    $treeorder=0;
    $dtreeId="0";
    while($row = $result10->fetch_assoc()) {
                 $treeorder=$treeorder+1; 
                 $pgmname2=$row["pgmname"];

                 if($pgmname2 == $pgmname){
                    $dtreeId=$dtreeId.'#'.$treeorder;
                 }                   
    }  
$directoryLocation =$file_server.DIRECTORY_SEPARATOR.$study_name.DIRECTORY_SEPARATOR.'/backup/'.$file_loc.'/'.$datetime.'_'.$pgmname;
$data = array('success' => $success,
			 'pgmname' => $pgmname,
			 'pgmstat' => $pgmstat,
			 'outstat' => $outstat,
			 'dtreeId' => $dtreeId,
			 'nopgm' => $nopgm,
			 'indevpgm' => $indevpgm,
			 'tvalpgm' => $tvalpgm,
			 'valpgm' => $valpgm,
			 'nout' => $nout,
			 'oout' => $oout,
			 'cout' => $cout			 
			 );

echo json_encode($data);

?>