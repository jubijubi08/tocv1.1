<?php 
require('fpdf.php');
session_start();
include("include/connect.php");
$study_name=$_SESSION["study"];

$user_check=$_SESSION['login_user'];
$user = $_SESSION['login_user'];
$snapshot = $_SESSION['snapshot'];
$dc_name1 = $_SESSION["dc_selector"];
$dc_db_val = 'SP'.$dc_name1;

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;


function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data)
{
    //Calculate the height of the row
    $this->SetX(1);
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    

    //If the height h would cause an overflow, add a new page immediately

    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
      $this->SetX(1);

}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}

// Page header
function Header()
{
    include("include/connect.php");
    $study_name=$_SESSION["study"];

    $user_check=$_SESSION['login_user'];
    $user = $_SESSION['login_user'];
	
	date_default_timezone_set('Asia/Dhaka');
	$date_time=date("Y-m-d, H:i:s");
	
    // Logo
    $this->Image('dist/img/logo.png',100,5,100);
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'','C');
    // Line break
    $this->Ln(20);

    $this->SetY(22);
    $this->SetX(1);
    $this->Cell(40,6,"Report Title: Current Status Of Outputs By Table Number",0,"C",1);

    $this->SetY(30);
    $this->SetX(1);
    $this->Cell(40,6,"Study Name: ".$study_name.',',0,"C",1);
	
	$this->SetY(30);
    $this->SetX(45);
    $this->Cell(40,6,"Snapshot Name: ".$_SESSION['snapshot'],0,"C",1);
	
    $this->SetY(38);
    $this->SetX(1);
    $this->Cell(35,6,"Date: ".$date_time,0,"C",1);
    $this->SetFillColor(232,232,232);
    //Bold Font for Field Name
    $this->SetFont('Arial','B',10);



    $this->SetY(47);
    $this->SetX(1);
    $this->MultiCell(10,6,"\nTLF Number",1,'C',1);
	$this->SetY(47);
    $this->SetX(11);
    $this->MultiCell(10,6,"\nTLF \nType",1,'C',1);
	
    $this->SetY(47);
    $this->SetX(21);
    $this->MultiCell(20,8,"\n\nTitle",1,'C',1);
    $this->SetY(47);
    $this->SetX(41);
    $this->MultiCell(20,6,"\n\nProgram Location",1,'C',1);
    $this->SetY(47);
    $this->SetX(61);
    $this->MultiCell(18,6,"\n\nProgram Name",1,'C',1);
    $this->SetY(47);
    $this->SetX(79);
    $this->MultiCell(28,6,"\n\nProgrammer Name",1,'C',1);  
	$this->SetY(47);
    $this->SetX(107);
    $this->MultiCell(20,8,"\nSnapshot \n Name",1,'C',1);
    $this->SetY(47);
    $this->SetX(127);
    $this->MultiCell(25,6,"Date/Time of Last \n Program Update",1,'C',1);
    $this->SetY(47);
    $this->SetX(152);
    $this->MultiCell(18,8,"\nOutput Location",1,'C',1);
    $this->SetY(47);
    $this->SetX(170);
    $this->MultiCell(25,8,"\n\nOutput Name",1,'C',1);
    $this->SetY(47);
    $this->SetX(195);
    $this->MultiCell(25,6,"Date/Time of Last \n Output Update",1,'C',1);
    $this->SetY(47);
    $this->SetX(220);
    $this->MultiCell(25,6,"\n\nValidator Name",1,'C',1);
    $this->SetY(47);
    $this->SetX(245);
    $this->MultiCell(25,6,"\n\nCurrent Status",1,'C',1);
    $this->SetY(47);
    $this->SetX(270);
    $this->MultiCell(25,6,"Date/Time of Last \n Status Update",1,"C",1);
    $this->SetY(65);

    $this->Ln();
}

// Page footer
function Footer()
{
    $user_check=$_SESSION['login_user'];
    $user = $_SESSION['login_user'];
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C'); 
    // Page Print by
    //$this->Cell(0,10,'Print by : ',0,0,'C'); 
    // Print Date
    //$this->Cell(120,10,'Print Date : ');
    $this->SetY(-13);
    $this->SetFont('Arial','',10);
    $this->Cell(30,6,'Printed By : '.$user);

    $this->SetY(-13);
    $this->SetX(-45);
    $this->SetFont('Arial','',10);
    $this->Cell(30,6,'Print Date : '.date("d M Y"));
  // $pdf->SetFont('Arial','',10);
  // $pdf->SetY(270);
  // $pdf->SetX(160);
  // $pdf->Cell(30,6,'Print Date : '.date("Y.m.d"));
}
}




$pdf=new PDF_MC_Table();
$pdf->AddPage('L', 'A4');
$pdf->AliasNbPages();
$pdf->SetFont('Arial','',10);

//Fields Name position
$Y_Fields_Name_position = 20;
//Table position, under Fields Name
$Y_Table_Position = 26;

//First create each Field Name
//Gray color filling each Field Name box




//Table with 20 rows and 4 columns
$pdf->SetWidths(array(10,10,20,20,18,28,20,25,18,25,25,25,25,25));
  include("include/connect.php");
  $study_name=$_SESSION["study"];

  $user_check=$_SESSION['login_user'];
  $user = $_SESSION['login_user'];
  
    $sql = "SELECT q.pgmname,
		   p.pgmuser,
		   q.sortorder,
		   p.event_date,
		   p.username,
		   p.status,
		   p.pgm_date,
		   p.out_date,
		   q.type,
		   q.tlfnum,
		   q.title,
		   q.pgmloc,
		   q.outname,
		   q.outloc
	FROM   (SELECT abc.pgmname,
				   abc.pgmuser,
				   abc.event_date,
				   abc.username,
				   abc.status,
				   abc.pgm_date,
				   d.out_date
			FROM   (SELECT *
					FROM   (SELECT a.pgmname,
								   a.pgmuser,
								   b.event_date,
								   b.validator AS username,
								   b.status,
								   c.pgm_date
							FROM   (SELECT pgmname,
										   username AS pgmuser
									FROM   pgm_hist_$study_name
									WHERE  event_date IN
										   (SELECT
										   Max(event_date) AS event_date
														  FROM
										   pgm_hist_$study_name
														  WHERE
										   comment LIKE '%Uploaded%'
											OR comment LIKE
										   '%Generated by System%'
														  GROUP  BY pgmname)
									ORDER  BY event_date DESC) AS a,
								   (SELECT pgmname,
										   username AS validator,
										   status,
										   event_date
									FROM   pgm_hist_$study_name
									WHERE  event_date IN (SELECT
										   Max(event_date) AS event_date
														  FROM
										   pgm_hist_$study_name
														  WHERE
										   comment NOT LIKE '%Run%'
														  GROUP  BY pgmname)
									ORDER  BY event_date DESC) AS b,
								   (SELECT pgmname,
										   Max(event_date) AS pgm_date
									FROM   pgm_hist_$study_name
									WHERE  comment LIKE '%Edited%'
											OR comment LIKE '%Uploaded%'
											OR comment LIKE '%Generated by System%'
									GROUP  BY pgmname
									ORDER  BY pgm_date DESC) AS c
							WHERE  a.pgmname = b.pgmname
								   AND b.pgmname = c.pgmname
							ORDER  BY pgmname,
									  event_date DESC) AS b_abc
					GROUP  BY pgmname) AS abc
				   LEFT JOIN (SELECT pgmname,
									 Max(event_date) AS out_date
							  FROM   pgm_hist_$study_name
							  WHERE  comment LIKE '%Run Program%'
							  GROUP  BY pgmname) AS d
						  ON abc.pgmname = d.pgmname) AS p
		   RIGHT JOIN (SELECT pgmname,
							  type,
							  tlfnum,
							  title,
							  pgmloc,
							  outname,
							  outloc,
							  sortorder
					   FROM   toc_$study_name
					   WHERE  data_currency = '$dc_db_val'
							  AND title != ' ') AS q
				   ON p.pgmname = q.pgmname
	ORDER  BY q.sortorder ASC"; 
    $result = $conn->query($sql);

        //print_r($ses_sql);

    $temp="";
	$distinct_status = array();
    $distinct_status_date = array();
    $distinct_validator = array();
    $distinct_out_date = array();
    $current_status = '';
    $validator_name = '';
    while($row = $result->fetch_assoc()){
	$pgmname = $row["pgmname"];
	if (!in_array($pgmname, $distinct_status_date)) {
		  $sql2_get_max_date = "SELECT 	MAX(event_date) AS event_date
								FROM pgm_hist_$study_name
								WHERE	pgmname = '$pgmname' 
								AND data_currency = 'SP0'
								AND (status LIKE '%In Development%'
								OR status LIKE '%Validated%'
								OR status LIKE '%To Be validated%')";

		  $result3 = $conn->query($sql2_get_max_date);
		  while($row3 = $result3->fetch_assoc()) {
			  $event_date = $row3['event_date'];
		  }
		  array_push($distinct_status_date,$pgmname);
		  $distinct_status_date[$pgmname] = $event_date;
	  }
	  if (!in_array($pgmname, $distinct_out_date)) {
            $sql_odate_get_max_date = "SELECT	event_date
                                         FROM	pgm_hist_$study_name
                                         WHERE pgmname = '$pgmname' 
                                         AND data_currency = '$dc_db_val'
                                         AND comment LIKE '%Run Success%' 
                                         ORDER BY event_date DESC LIMIT 1";

            $result3 = $conn->query($sql_odate_get_max_date);
            while($row3 = $result3->fetch_assoc()) {
                $event_date = $row3['event_date'];
            }
            array_push($distinct_out_date,$pgmname);
            $distinct_out_date[$pgmname] = $event_date;
      }
	  if (!in_array($pgmname, $distinct_validator)) {
		  $sql5_get_max_date = "SELECT username AS validator																			  
								FROM   pgm_hist_$study_name
								WHERE  pgmname = '$pgmname'
								AND data_currency = 'SP0'				
								AND status LIKE '%Validated%'
								AND event_date IN ('$distinct_status_date[$pgmname]')";

		  $result5 = $conn->query($sql5_get_max_date);
		  while($row5 = $result5->fetch_assoc()) {
			  $validator_name = $row5['validator'];
		  }
		  array_push($distinct_validator,$pgmname);
		  $distinct_validator[$pgmname] = $validator_name;
	  }
	  if (!in_array($pgmname, $distinct_status)) {
			$sortorder = $row["sortorder"];
			$sql3_get_max_date = "SELECT pgmstat
                                    FROM   toc_status_$study_name
                                    WHERE  sortorder = '$sortorder'
                                    AND data_currency = '$dc_db_val'";
								  
		   $result4=$conn->query($sql3_get_max_date);								  
		   while($row4 = $result4->fetch_assoc()) {
			 $current_status = $row4['pgmstat'];
			 if($current_status == '0')
			 {
				$status_name = 'No Program';
			 }
			 else if($current_status == '1')
			 {
				 $status_name = 'In Development';
			 }
			 else if($current_status == '2')
			 {
				 $status_name = 'To Be Validated';
			 }
			 else if($current_status == '3')
			 {
				 $status_name = 'Validated';
			 }											 
			 if($current_status == '3')
			 {
				$validator_name = $distinct_validator[$pgmname];
			 }else{
				 $validator_name = '';
			 }												 
		   }
		   array_push($distinct_status,$pgmname);
		   $distinct_status[$pgmname] = $status_name;
	  }
    //$name=$row["type"]." ".$row["tlfnum"];

    $title = $row['title'];
    $pgmloc = substr_replace($row["pgmloc"],"",strripos($row["pgmloc"],"/")+1);
    $pgmname = $row['pgmname'];
    $pgmuser = $row['pgmuser'];
    $outloc = substr_replace($row["outloc"],"",strripos($row["outloc"],"/")+1);
    $outname = $row['outname'];
    //$study_id = $study_id;
	if($row["status"] == 'Validated')
	{
	  $username = $row['username'];
	}
	else 
	{
	  $username = " ";
	}
    $status = $row['status'];

      $date = $distinct_status_date[$pgmname];

      if ($date != "") {
        $my_date = date('d M Y H:i:s', strtotime($date));
      }
      else{
        $my_date = $date;
      }

      $pdate = $row["pgm_date"];
      if ($pdate != "") {
        $p_date = date('d M Y H:i:s', strtotime($pdate));
      }
      else{
        $p_date = $pdate;
      }

      $odate = $distinct_out_date[$pgmname];
      if ($odate != "") {
        $o_date = date('d M Y H:i:s', strtotime($odate));
      }
      else{
        $o_date = $odate;
      }  

    $pdf->Row(array($row["tlfnum"],$row["type"],$title,$pgmloc,$pgmname,$pgmuser,$snapshot,$p_date,$outloc,$outname,$o_date,$validator_name,$distinct_status[$pgmname],$my_date));
  } 
 



  //$pdf->Output();
$file_name = 'study_report_out_'.date('Y-m-d h:i:s a').'.pdf';
$pdf->Output('D',$file_name);

?>