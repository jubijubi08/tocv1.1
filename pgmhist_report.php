<?php 
require('fpdf.php');
$pgmname="";
class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}

// Page header
function Header()
{
    // Logo
    $this->Image('dist/img/logo.png',80,6,60);
    // Arial bold 15
    $this->SetFont('Arial','B',15);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'','C');
    // Line break
    $this->Ln(20);
}

// Page footer
function Footer()
{
    $user_check=$_SESSION['login_user'];
    $user = $_SESSION['login_user'];
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C'); 
    // Page Print by
    //$this->Cell(0,10,'Print by : ',0,0,'C'); 
    // Print Date
    //$this->Cell(120,10,'Print Date : ');
    $this->SetY(-13);
    $this->SetFont('Arial','',10);
    $this->Cell(30,6,'Print By : '.$user);

    $this->SetY(-13);
    $this->SetX(-45);
    $this->SetFont('Arial','',10);
    $this->Cell(30,6,'Print Date : '.date("d M Y"));
}
}


session_start();

$pdf=new PDF_MC_Table();
$pdf->AddPage();
$pdf->AliasNbPages();
$pdf->SetFont('Arial','',14);

//Fields Name position
$Y_Fields_Name_position = 20;
//Table position, under Fields Name
$Y_Table_Position = 26;

//First create each Field Name
//Gray color filling each Field Name box
$pdf->SetFillColor(232,232,232);
//Bold Font for Field Name
$pdf->SetFont('Arial','B',12);


$pdf->SetY($Y_Fields_Name_position);
$pdf->SetX(10);
$pdf->Cell(30,6,'Date/time',1,0,'C',1);
$pdf->SetX(40);
$pdf->Cell(30,6,'programmer',1,0,'C',1);
$pdf->SetX(70);
$pdf->Cell(90,6,'Comment',1,0,'C',1);
$pdf->SetX(160);
$pdf->Cell(40,6,'Final Status',1,0,'C',1);
$pdf->Ln();

//Table with 20 rows and 4 columns
$pdf->SetWidths(array(30,30,90,40));
  include("include/connect.php");
  $study_name=$_SESSION["study"];

  $user_check=$_SESSION['login_user'];
  $user = $_SESSION['login_user'];
  
  $pgmid = $_POST['pid_hist'];  
  //echo$pgmid."<BR>";
  $pgmid_len=iconv_strlen (trim($pgmid));
  //echo$pgmid_len."<BR>";
  $tosearchid=$pgmid.str_repeat("0",10-$pgmid_len);
  //echo $tosearchid."<BR>"; 

  $ses_sql2=$conn->query("SELECT * FROM study_info WHERE study_name='$study_name'");
    while($row = $ses_sql2->fetch_assoc()){
      $study_id = $row['study_id'];
    } 

    $ses_sql21=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder='$tosearchid'");
    while($row = $ses_sql21->fetch_assoc()){
      $pgmname = $row['pgmname'];
    } 

    $ses_sql=$conn->query("SELECT * FROM pgm_hist_$study_name WHERE pgmname='$pgmname' AND comment NOT LIKE '%Run Program%' ORDER BY event_date DESC");

    //print_r($ses_sql);

    $temp="";
    while($row = $ses_sql->fetch_assoc()){
    $pgmname = $row['pgmname'];
    $event_date = $row['event_date'];
    $username = $row['username'];
    $comment = $row['comment'];
    $status = $row['status'];
    $study_id = $study_id;
    $id = $row['id'];


    $date = $row["event_date"];
    $my_date = date('d M Y H:i:s', strtotime($date));

    $pdf->Row(array($my_date,$username,$comment,$status));
  } 

  $pdf->SetY(13);
  $pdf->SetX(9);
  $pdf->Cell(30,6,'Program History :'.$pgmname);

  $pdf->SetY(13);
  $pdf->SetX(150);
  $pdf->Cell(30,6,'Study Name :'.$study_name);
  
  // $pdf->SetFont('Arial','',10);
  // $pdf->SetY(270);
  // $pdf->SetX(10);
  // $pdf->Cell(30,6,'Print By : '.$user);

  // $pdf->SetFont('Arial','',10);
  // $pdf->SetY(270);
  // $pdf->SetX(160);
  // $pdf->Cell(30,6,'Print Date : '.date("Y.m.d"));

  $pdf->Output();
//sortorder   pgmname   event_date  username  comment   status  link



?>