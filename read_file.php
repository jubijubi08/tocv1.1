<pre  style="border: 1px solid black; display: block; overflow: auto; overflow-wrap: normal; padding: 5px;
background-color: #f5f5f5;border-radius: 4px;color: #333;font-size: 13px;line-height: 1.42857; margin: 0 0 10px; width: 99%;" >
 	<?php
 		if(isset($_POST['file'])){
			$file_server = $_POST['file'];
	 		if (file_exists($file_server)) {
	 			if (is_readable($file_server)) {
					$ext = strtolower(pathinfo($file_server, PATHINFO_EXTENSION));
					if($ext == "lst"){
						$lines = file($file_server, FILE_IGNORE_NEW_LINES);
		 				$title = ""; $skip = 0;
		 				foreach($lines as $line) {
		 					if($title == ""){
		 						if(!empty($line)){
		 							$title = $line;
		 							$skip = 1;
		 						}
		 					}
		 					$line = mb_convert_encoding($line, "UTF-8");
		 					
		 					for($x=0; $x< strlen($line); $x++){
								if(ord($line[$x])=='63') echo "&fnof;";
								else echo $line[$x];
							}
							echo "<br/>";
		 					if(!$skip){
		 						if($line !=""){
									if(strpos($title, $line) !== false) exit;
								}
		 					}
		 					else $skip = 0;
		 				}
					}
					else if($ext == "rtf"){
						include('include/rtf.php');
		      	$reader = new RtfReader();
						$rtf = file_get_contents($file_server); // or use a string
						$reader->Parse($rtf);
						//$reader->root->dump();
						$formatter = new RtfHtml();
						$text = $formatter->Format($reader->root);
			 			$text = mb_convert_encoding($text, "UTF-8");
	
			 			$line = explode("</span>",$text);	
			 			foreach($line as $text) { 
							for($x=3; $x< strlen($text)-3; $x++){
								if(ord($text[$x])=='63') echo "&fnof;";
								else echo $text[$x];
							}
							echo "</span></br><sp";
						}
					}
					else{
						$image = file_get_contents($file_server);

						$image_codes = base64_encode($image);
						echo "<img src='data:image/jpg;charset=utf-8;base64,$image_codes' width:90% height = '100%'/>";
					}
				}
				else echo "File is not readable.";
			}
			else echo "File does not exist in study folder.";
		}
		if(isset($_GET['file'])){
			$file_server = $_GET['file'];
	 		if (file_exists($file_server)) {
	 			if (is_readable($file_server)) {
	 				$ext = strtolower(pathinfo($file_server, PATHINFO_EXTENSION));
					if($ext == "lst"){
				 		$text = fopen($file_server, "r");
				 		while(! feof($text)){
							$str = fgets($text);
							$str = mb_convert_encoding($str, "UTF-8");
							$len = strlen($str);
							for($i=0; $i< $len; $i++){
								if(ord($str[$i])=='63') echo "&fnof;";
								else echo $str[$i];
							}
						}
					}
					else if($ext == "rtf"){
						include('include/rtf.php');
		      	$reader = new RtfReader();
						$rtf = file_get_contents($file_server); // or use a string
						$reader->Parse($rtf);
						//$reader->root->dump();
						$formatter = new RtfHtml();
						$text = $formatter->Format($reader->root);
			 			$text = mb_convert_encoding($text, "UTF-8");
	
			 			$line = explode("</span>",$text);	
			 			foreach($line as $text) { 
							for($x=3; $x< strlen($text)-3; $x++){
								if(ord($text[$x])=='63') echo "&fnof;";
								else echo $text[$x];
							}
							echo "</span></br><sp";
						}
					}
					else{
						$image = file_get_contents($file_server);

						$image_codes = base64_encode($image);
						echo "<img src='data:image/jpg;charset=utf-8;base64,$image_codes' width:90% height = '100%'/>";
					}
				}
				else echo "File is not readable.";
			}
			else echo "File does not exist in study folder.";
		}
 	?>
</pre> 