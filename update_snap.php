<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];
$snap_id=$_GET['snap'];

$sql_sd="SELECT DISTINCT snap_name,is_lock FROM snap_$study_name WHERE id ='$snap_id' ";
$result_sd=$conn->query($sql_sd);
while($row = $result_sd->fetch_assoc()) {
  $snap_name=$row["snap_name"];
  $is_lock=$row["is_lock"];
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Update </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">

        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?>

            </h3>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-12 col-sm-8">
                <div class="pad">

                  <div class="row">
                    <div class="col-md-12">
                      <p style="font-size:18px;">Update Existing Data Currency</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">


                    <form action="#" method="post" id="edit_snap_fd">

                      <input id="snap_id" type="hidden" name="snap_id" value="<?php echo $snap_id; ?>">

                        <div class="form-group has-feedback">
                          <p><b>Data Currency Name:</b></p>
                          <input required type="text" name="snap_name" id="snap_name" class="form-control" value="<?php echo $snap_name; ?>"/>
                        </div>

                        <div class="form-group has-feedback">
                          <p><b>Is Data Currency Lock:</b></p>
                          <input required type="text" name="ads_lib_name" id="ads_lib_name" class="form-control" value="<?php echo $is_lock; ?>"/>
                        </div>



                      <div class="row">
                        <div class="col-xs-8">
                          <div class="checkbox icheck">

                          </div>
                        </div><!-- /.col -->
                        <div class="col-xs-2">

                        </div>
                        <div class="col-xs-4">
                          <button type="button" class="btn btn-primary btn-block btn-flat" id="edit_snap_btn">Update</button>
                        </div><!-- /.col -->
                      </div>
                    </form>
                    </div>
                  </div>


                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!-- /.col -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php
include("include/footer.php");
$conn->close();
?>
      