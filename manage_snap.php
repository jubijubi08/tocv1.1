<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];

//Check if any common programs exist or not
$study_name=strtolower($study_name);
$table="cplist_$study_name";
$result=$conn->query("SHOW TABLES FROM toc_dbv2");
while($row = mysqli_fetch_row($result)){
    $arr[] = $row[0];
}

if(in_array($table,$arr))
{
    $cp_exist=1;
}else{
    $cp_exist=0;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Manage Data Currency </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
      <!-- Main row -->
      <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?>

                </h3>
              </div><!-- /.box-header -->
              <div class="box-body no-padding">
                <div class="row">
                  <div class="col-md-12 col-sm-8">
                    <div class="pad">

                         <div class="row">
                            <div class="col-md-12">
                            <p style="font-size:18px;">List of All Data Currency</p>
                            </div>
                         </div>
                        <div class="row">
                            <div class="col-md-12">
                                
<?php 
                $sql = "SELECT DISTINCT id, snap_name, is_lock, created_at, created_by , updated_at , updated_by  FROM snap_$study_name WHERE id !=0";
                $result = $conn->query($sql);
                // print_r($result);

                // if($result){
                  if ($result->num_rows > 0) {
                    echo "<table id='mysnapTable' class='table table-hover'>";
                    echo "<thead>";
                    echo "<tr>";
                    echo "<th>Data Currency</th>";
                    echo "<th>Lock</th>";
                    echo "<th>Last Modify Date</th>";
                    echo "<th>Last Modify By</th>";
                    echo "<th>Created Date</th>";
                    echo "<th>Created By</th>";
                    echo "<th>Action</th>";
                    echo "</tr>";
                    echo "</thead>";
                    echo "<tbody>";

                    while($row = $result->fetch_assoc()) {
                        echo "<tr>";
                        echo "<td>" . $row["snap_name"].  "</td>";
                        echo "<td>" . $row["is_lock"].  "</td>";
                        echo "<td>" . $row["updated_at"].  "</td>";
                        echo "<td>" . $row["updated_by"].  "</td>";
                        echo "<td>" . $row["created_at"].  "</td>";
                        echo "<td>" . $row["created_by"].  "</td>";
                        echo "<td><a href='update_snap.php?snap=".$row["id"]."'><button  type='button' class='btn btn-primary btn-block btn-flat'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Update</i>
                            </button></a></td>";
                        echo "</tr>";
                    }

                    echo "</tbody>";
                    echo "</table>";       
                  } else {
                        echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Snapshot Created yet</span>";
                  }


?>
															


                            </div> 
                        </div>

                        <div class="row">
                            <div class="col-md-9">
                                <p style="font-size:18px;"> Add a New Data Currency Here</p>
                            </div>
                            <div class="col-md-3">
                                <button  class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target=".bs-example-snap-new-modal-lg">Add</button>
                            </div> 
                        </div>


                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div><!-- /.col -->          
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!--  Edit Common program  model -->
<div class="modal fade bs-example-snap-new-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Create a New Data Currency</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">                                
                                

                                <form action="#" method="post" id="add_snap_fd">

                                    <div class="row">
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

                                            <div class="form-group has-feedback" id="snap_name_field">
                                                <p><b>Data Currency Name:</b></p>
                                                <input required type="text" name="snap_name"  onchange="chk_DC_loc(this.value)"  id="snap_name" class="form-control" value="" />
                                                
                                            </div>
                            
                                        </div>
                                    </div>

                                   
                                  

                                </form>  

                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>


<!---->
<!--$response = array();-->
<!--$response[] = "<a href=''>link</a>";-->
<!--$response[] = 1;-->
<!--echo implode("", $response);-->
<!--$.ajax({-->
<!--type: "POST",-->
<!--dataType: "html",-->
<!--url: "main.php",-->
<!--data: "action=loadall&id=" + id,-->
<!--success: function(response){-->
<!--$('#main').html(response);-->
<!--}-->
<!--});-->


<?php
include("include/footer.php");
$conn->close();
?>
      