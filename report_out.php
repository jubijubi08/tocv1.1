<?php
	include("include/header.php");
	include("include/connect.php");
	$study_name=$_SESSION["study"];


	if(isset($_POST['dc_selector']))
	    $dc_name = $_POST['dc_selector'];
	else $dc_name = 0;
	$_SESSION["dc_selector"] = $dc_name;
	$dc_db_val='SP'.$dc_name;

	$snapshot = get_snapshot_name($dc_name, $study_name, $conn);

	$_SESSION['snapshot'] = $snapshot;


	//Check if any common programs exist or not
	$study_name=strtolower($study_name);
	$table="cplist_$study_name";
	$result=$conn->query("SHOW TABLES FROM toc_dbv2");
	while($row = mysqli_fetch_row($result)){
	    $arr[] = $row[0];
	}

	if(in_array($table,$arr))
	{
	    $cp_exist=1;
	}else{
	    $cp_exist=0;
	}
	//Get the folder location from study table for check manually modified or not.
	$sql = "SELECT * FROM study_info WHERE study_name = '$study_name'";
	$result = $conn->query($sql);
	$info = $result->fetch_assoc();
	$file_folder = $info["study_loc"];
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home </a></li>
      <li class="active">Manage Programs</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
      <!-- Main row -->
      <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                   <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?></h3>
				   <br />
				   <h4 class="box-title">Snapshot Name: <?php echo $snapshot;?></h4>
                    <div class="box-tools pull-right" style="position: initial;">

                        <form action='report_out.php' method='post'>
                            <div class="row" style="margin-right: 0px;">

                                    <label><h3 class="box-title">Data Currency: </h3> </label>

                                    <select id="dc_selector" name="dc_selector">
                                        <?php
                                        while($row = $snap_list->fetch_assoc()) {
                                            $dc_id = $row['id'];
                                            $sp_name = $row['snap_name'];

                                            if($dc_id==$dc_name){
                                                echo "<option value='".$dc_id."' selected='selected'>".$sp_name."</option>";
                                            }else{
                                                echo "<option value=".$dc_id.">".$sp_name."</option>";
                                            }

                                        }
                                        ?>


                                    </select>


                                    <button type="submit" id="ellBtn" class="btn btn-block btn-default btn-xs"  >View</button>

                            </div>
                        </form>

                    </div>
                </div><!-- /.box-header -->


                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-12 col-sm-8">
                      <div class="pad">
                          <div class="row">
                            <div class="col-md-12">
                                <p style="font-size:18px;">Current Status Of Outputs By Table Number</p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                                
                              <?php 
                                // $sql = "SELECT p.sortorder,p.title,p.tlfnum,p.username,p.status,p.event_date,p.pgmname,q.username AS pgmuser,p.type,p.type,p.l1,p.l2,p.l3,p.l4,p.l5 
                                //         FROM (SELECT x.sortorder,x.title,x.tlfnum,y.username,y.status,y.event_date,x.pgmname,x.type,x.l1,x.l2,x.l3,x.l4,x.l5 
                                //         FROM (SELECT * FROM toc_$study_name WHERE title !=' ') as x 
                                //         LEFT OUTER JOIN (SELECT a.sortorder as sortorder ,a.username as username ,a.status as status,a.event_date as event_date 
                                //         FROM pgm_hist_$study_name AS a 
                                //         JOIN toc_$study_name AS b 
                                //         WHERE a.sortorder=b.sortorder AND a.event_date IN (SELECT MAX(event_date) 
                                //                                                               FROM pgm_hist_$study_name 
                                //                                                               GROUP BY sortorder) 
                                //             GROUP BY a.sortorder) as y
                                //         ON x.sortorder=y.sortorder
                                //         ORDER BY x.sortorder ASC) AS p LEFT OUTER JOIN (SELECT DISTINCT pgmname,username FROM pgm_hist_$study_name WHERE comment = 'Uploaded' OR comment = 'First Uploaded') AS q ON p.pgmname=q.pgmname"; 
                                // $result = $conn->query($sql);
                                // print_r($result);
                                $sql = "SELECT q.pgmname,
											   p.pgmuser,
											   q.sortorder,
											   p.event_date,
											   p.username,
											   p.status,
											   p.pgm_date,
											   p.out_date,
											   q.type,
											   q.tlfnum,
											   q.title,
											   q.pgmloc,
											   q.outname,
											   q.outloc
										FROM   (SELECT abc.pgmname,
													   abc.pgmuser,
													   abc.event_date,
													   abc.username,
													   abc.status,
													   abc.pgm_date,
													   d.out_date
												FROM   (SELECT *
														FROM   (SELECT a.pgmname,
																	   a.pgmuser,
																	   b.event_date,
																	   b.validator AS username,
																	   b.status,
																	   c.pgm_date
																FROM   (SELECT pgmname,
																			   username AS pgmuser
																		FROM   pgm_hist_$study_name
																		WHERE  event_date IN
																			   (SELECT
																			   Max(event_date) AS event_date
																							  FROM
																			   pgm_hist_$study_name
																							  WHERE
																			   comment LIKE '%Uploaded%'
																				OR comment LIKE
																			   '%Generated by System%'
																							  GROUP  BY pgmname)
																		ORDER  BY event_date DESC) AS a,
																	   (SELECT pgmname,
																			   username AS validator,
																			   status,
																			   event_date
																		FROM   pgm_hist_$study_name
																		WHERE  event_date IN (SELECT
																			   Max(event_date) AS event_date
																							  FROM
																			   pgm_hist_$study_name
																							  WHERE
																			   comment NOT LIKE '%Run%'
																							  GROUP  BY pgmname)
																		ORDER  BY event_date DESC) AS b,
																	   (SELECT pgmname,
																			   Max(event_date) AS pgm_date
																		FROM   pgm_hist_$study_name
																		WHERE  comment LIKE '%Edited%'
																				OR comment LIKE '%Uploaded%'
																				OR comment LIKE '%Generated by System%'
																		GROUP  BY pgmname
																		ORDER  BY pgm_date DESC) AS c
																WHERE  a.pgmname = b.pgmname
																	   AND b.pgmname = c.pgmname
																ORDER  BY pgmname,
																		  event_date DESC) AS b_abc
														GROUP  BY pgmname) AS abc
													   LEFT JOIN (SELECT pgmname,
																		 Max(event_date) AS out_date
																  FROM   pgm_hist_$study_name
																  WHERE  comment LIKE '%Run Program%'
																  OR comment LIKE '%Run Success%'
																  GROUP  BY pgmname) AS d
															  ON abc.pgmname = d.pgmname) AS p
											   RIGHT JOIN (SELECT pgmname,
																  type,
																  tlfnum,
																  title,
																  pgmloc,
																  outname,
																  outloc,
																  sortorder
														   FROM   toc_$study_name
														   WHERE  data_currency = '$dc_db_val'
																  AND title != ' ') AS q
													   ON p.pgmname = q.pgmname
										ORDER  BY q.sortorder ASC"; 
                                $result = $conn->query($sql);
                                // if($result){
                                if ($result->num_rows > 0) {
								  $distinct_status = array();
								  $distinct_status_date = array();
								  $distinct_out_date = array();
								  $distinct_validator = array();
								  $current_status = '';
								  $validator_name = '';
                                  echo "<table id='myTable1' class='table table-hover'>";
                                  echo "<thead>";
                                  echo "<tr bgcolor='#e8e8e8' >";
                                  echo "<th>TLF <br> Number</th>";
                                  echo "<th>TLF <br> Type</th>";
                                  echo "<th align='center'>Title</th>";
                                  echo "<th>Program <br> Location</th>";
                                  echo "<th>Program <br> Name</th>";
                                  echo "<th>Programmer <br> Name</th>";
								  echo "<th>Snapshot <br/> Name</th>";		
                                  echo "<th>Date/Time of Last <br> Program Update</th>";
                                  echo "<th>Output <br> Location</th>";
                                  echo "<th>Output <br> Name</th>";
                                  echo "<th>Date/Time of Last <br> Output Update</th>";
                                  echo "<th>Validator <br> Name</th>";
                                  echo "<th>Current <br> Status</th>";                    
                                  echo "<th>Date/Time of Last <br> Status Update </th>";
                                  echo "</tr>";
                                  echo "</thead>";
                                  echo "<tbody>";
                                  $str_count = 0;
                                  while($row = $result->fetch_assoc()) {
									  $pgmname = $row["pgmname"];

                                      if (!in_array($pgmname, $distinct_status_date)) {
                                          $sql2_get_max_date = "SELECT
																MAX(event_date) AS event_date
                                                                FROM
																pgm_hist_$study_name
																WHERE
																pgmname = '$pgmname' 
																AND data_currency = 'SP0'
																AND (status LIKE '%In Development%'
																OR status LIKE '%Validated%'
																OR status LIKE '%To Be validated%')";

                                          $result3 = $conn->query($sql2_get_max_date);
                                          while($row3 = $result3->fetch_assoc()) {
                                              $event_date = $row3['event_date'];
                                          }
                                          array_push($distinct_status_date,$pgmname);
                                          $distinct_status_date[$pgmname] = $event_date;
                                      }
                                      if (!in_array($pgmname, $distinct_out_date)) {
                                          $sql_odate_get_max_date = "SELECT	event_date
                                                                     FROM	pgm_hist_$study_name
																     WHERE pgmname = '$pgmname' 
																     AND data_currency = '$dc_db_val'
																     AND comment LIKE '%Run Success%' 
																     ORDER BY event_date DESC LIMIT 1";

                                          $result3 = $conn->query($sql_odate_get_max_date);
                                          while($row3 = $result3->fetch_assoc()) {
                                              $event_date = $row3['event_date'];
                                          }
                                          array_push($distinct_out_date,$pgmname);
                                          $distinct_out_date[$pgmname] = $event_date;
                                      }
                                      if (!in_array($pgmname, $distinct_validator)) {
                                          $sql5_get_max_date = "SELECT 
																username AS validator																			  
																FROM   pgm_hist_$study_name
																WHERE  pgmname = '$pgmname'
																AND data_currency = 'SP0'				
																AND status LIKE '%Validated%'
																AND event_date IN ('$distinct_status_date[$pgmname]')";

                                          $result5 = $conn->query($sql5_get_max_date);
                                          while($row5 = $result5->fetch_assoc()) {
                                              $validator_name = $row5['validator'];
                                          }
                                          array_push($distinct_validator,$pgmname);
                                          $distinct_validator[$pgmname] = $validator_name;
                                      }
									  if (!in_array($pgmname, $distinct_status)) {
										    $sortorder = $row["sortorder"];
											$sql3_get_max_date = "SELECT pgmstat
																		FROM   toc_status_$study_name
																		WHERE  sortorder = '$sortorder'
																		AND data_currency = '$dc_db_val'"; 
																  
										   $result4=$conn->query($sql3_get_max_date);								  
										   while($row4 = $result4->fetch_assoc()) {
											 $current_status = $row4['pgmstat'];
											 if($current_status == '0')
											 {
												$status_name = 'No Program';
											 }
											 else if($current_status == '1')
											 {
												 $status_name = 'In Development';
											 }
											 else if($current_status == '2')
											 {
												 $status_name = 'To Be Validated';
											 }
											 else if($current_status == '3')
											 {
												 $status_name = 'Validated';
											 }											 
											 if($current_status == '3')
											 {
												$validator_name = $distinct_validator[$pgmname];
											 }else{
												 $validator_name = '';
											 }												 
										   }
										   array_push($distinct_status,$pgmname);
										   $distinct_status[$pgmname] = $status_name;
									  }


                                      $date = $distinct_status_date[$pgmname];
                                      if ($date != "") {
                                        $my_date = date('d M Y H:i:s', strtotime($date));
                                      }
                                      else{
                                        $my_date = $date;
                                      }

                                      $pdate = $row["pgm_date"];
                                      if ($pdate != "") {
                                        $p_date = date('d M Y H:i:s', strtotime($pdate));
                                      }
                                      else{
                                        $p_date = $pdate;
                                      }

                                      $odate = $distinct_out_date[$pgmname];
                                      if ($odate != "") {
                                        $o_date = date('d M Y H:i:s', strtotime($odate));
                                      }
                                      else{
                                        $o_date = $odate;
                                      }
									  
                                      //$name=$row["type"]." ".$row["tlfnum"];
                                      $row["tlfnum"];
                                      
                                      //Read the files modified date
																			$file_url = $file_folder."/".$study_name."/".$row["outloc"].$row["outname"];
                                    	$file_modified_date = date("d M Y H:i:s",filemtime("$file_url"));
                                    	$seconds = 0;
                                    	if($o_date != "")
                                    		$seconds = abs(strtotime($file_modified_date) - strtotime($o_date));
                                    	
                                    	if($seconds < 60)
                                    		$star = "";
                                    	else{
		                                		$star = "*";
		                                		$str_count++;
		                                	}
                                    
                                      // echo "<tr bgcolor='#FF0000'>";
                                      echo "<tr >";
                                      echo "<td class='col-md-1'>" . $row["tlfnum"].  "</td>";
                                      echo "<td class='col-md-1'>" . $row["type"].  "</td>";
                                      echo "<td class='col-md-4'>" . $row["title"].  "</td>";
                                      echo "<td class='col-md-1'>" . substr_replace($row["pgmloc"],"",strripos($row["pgmloc"],"/")+1).  "</td>";
                                      echo "<td class='col-md-1'>" . $row["pgmname"].  "</td>";
                                      echo "<td class='col-md-1'>" . $row["pgmuser"].  "</td>";
                                      echo "<td class='col-md-1'>" . $snapshot.  "</td>";
                                      echo "<td class='col-md-1'>" . $p_date.  "</td>";
                                      echo "<td class='col-md-1'>" . substr_replace($row["outloc"],"",strripos($row["outloc"],"/")+1).  "</td>";
                                      //Put star beside output name
                                      echo "<td class='col-md-1'>" . $row["outname"].  "$star</td>";
                                      echo "<td class='col-md-1'>" . $o_date.  "</td>";
                  					  echo "<td>" . $validator_name.  "</td>";
                                      echo "<td class='col-md-1'>" . $distinct_status[$pgmname].  "</td>";
                                      echo "<td class='col-md-1'>" . $my_date.  "</td>";
                                      echo "</tr>";
                                  }
                                  echo "</tbody>";
                                  echo "</table>";   
                                } else {
                                      echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                }
                                ?>
															
                            </div> 
                          </div>
                          
                          <?php if($str_count >0){?>
                          <div class="row col-md-12">
                          	<span class="text-left" style="font-size:14px;"><b>*</b> File was modified manually.</span>
                          </div>
													<?php }?>

                          <div class="row">
                            <div class="col-md-9">
                                <p style="font-size:18px;"> Download Report in PDF format</p>
                            </div>
                            <div class="col-md-3">                                
                                <a href='study_report_out.php' target='_blank' class='demo'>
									<button  class="btn btn-primary btn-block btn-flat" >Download</button>
								</a>
                            </div> 
                          </div>
                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->          
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php
include("include/footer.php");
$conn->close();
?>
      