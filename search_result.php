<?php
	include("include/connect.php");
	session_start();

	$user_check	=	$_SESSION['login_user'];

	$data_set = array();

	$ses_sql = $conn->query("select user_id from user_info where username = '$user_check' ");
	$row = $ses_sql->fetch_assoc();
	//Fetch user id from user_info table.
	$user_id = $row['user_id'];
	$ind = 0; $count_study = 0; $title = "";
	/**
	* 
	* @var fetch all active  study information from study_info table.
	* prepare the fetch data for showung as search result
	*/
	$study_sql = $conn->query("select * from study_info where status = 'ON' ");
	while($row = $study_sql->fetch_assoc()) { 
	  $data_set[$ind]['study_id'] = $study_id = $row['study_id'];// store study_id in variable
	  $data_set[$ind]['study_name'] = $study_name = $row['study_name'];// store study_name in variable
	  $data_set[$ind]['study_loc'] = $row['study_loc'];// store study_loc in variable
	  //get the allocated status from study_allocation table
	  $alloc_sql = $conn->query("select count(allocation_id) as total from study_allocation where user_id = $user_id AND study_id = $study_id AND status = 'ON'");
	  $alloc = $alloc_sql->fetch_assoc();
	  
	  $data_set[$ind]['is_allocate'] = $alloc['total'];//Store the allocated number in variable. Value 0 means not allocated.
	  $tableName = 'toc_'.$study_name;
	  $data = $_POST['data'];
	  $i = 0;
	  
	  $sql = "SELECT type, tlfnum, title, pgmloc, pgmname, outloc, outname FROM $tableName WHERE ";
	  $condition = "";
		foreach($data as $d){
			$column = $d[0];
			$column_values = $d[1];
			$level = $d[2];
			$result = $conn->query("SHOW COLUMNS FROM `$tableName` LIKE '$column'");
			$exists = ($result->num_rows > 0)?TRUE:FALSE;
			
			if($exists){
				if($i){
					$condition = $condition. ' AND ';
					if(!$ind)$title = $title. ' AND ';
				}
				if(!$ind)$title = $title.$level.': ' .$column_values;
				
				$values = explode(",",$column_values);
				$j = 0;
				$condition = $condition.' ( ';
				foreach($values as $val){
					if($j) $condition = $condition.' OR ';
					$condition = $condition.$column.' LIKE '."'%$val%'";
					$j++;
				}
				$condition = $condition.' ) ';
				$i++;
			}
		}//echo "Study Name: $row[study_name], Condition ".$condition;
		if($condition != ""){
			$sql = $sql.' '.$condition."AND data_currency = 'SP0' AND type != '' AND tlfnum != '' AND title !=''";
		}else $sql = $sql."data_currency = 'SP0' AND type != '' AND tlfnum != '' AND title !=''";
		//echo $sql;
		$search_sql = $conn->query($sql);

	  $findings = array(); $i = 0;
	  
		if($search_sql->num_rows > 0)$count_study++;
	
	  while($row = $search_sql->fetch_assoc()){
	  	$findings[$i++] = $row;
	  }	  
	  $data_set[$ind]['findings'] = $findings;
	  $ind++;
	}

?>
    

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-12">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Search Result</h3>
              <div class="box-tools pull-right">      </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-12 col-sm-12">
                  <div class="pad">
                  	<?php if($count_study > 0){ ?>
                    	<div class="row">
                       <div class="col-md-12">
                         <div class="results-summary"><strong>
                         	<?php 
                         		if($count_study > 1)echo $count_study." studies";
                         		else echo $count_study." study";
                         	?> 
                         	found for</strong>:&nbsp;&nbsp; <?php echo $title;?>
                        </div>
                       </div>
                       <hr>
                    	</div>
                     
	                    <div class="row">
	                      <div class="col-md-6">
	                      	<div id="data">
														<table id="data" class="table table-hover table-responsive" cellspacing="0">
											        <thead>
										            <tr>
									                <th>#</th>
									                <th>Access</th>
									                <th>Study </th>
									                <th>Table Number And Output Name</th>
										            </tr>
											        </thead>
											        <tbody>
											        	<?php
											        		$sn = 1;
											        		foreach($data_set as $data){
											        			$finds = $data['findings'];
											        			if(is_array($finds)){
											        				foreach($finds as $find){?>
																				<tr>
																					<td><?php echo $sn;?></td>
																					<td>&nbsp;&nbsp;<img src="stree/metroStyle/img/<?php if($data['is_allocate']) echo "p3"; else echo "p2";?>.png" height="13" width="13"></td>
																					<td><?php echo $data['study_name'];?></td> 
																					<?php 
																						$file_server = $data['study_loc']."/".$data['study_name']."/".$find["outloc"]."/".$find["outname"];
																						$pgmloc = $data['study_loc']."/".$data['study_name']."/".$find["pgmloc"]."/".$find["pgmname"];
																					?>
																					<td><a id="link_<?php echo $sn;?>" class="file-link text" file="<?php echo $file_server;?>" <?php if($data['is_allocate']){?> href="study.php?study=<?php echo $data['study_name'];?>" out_loc = "read_file.php?file=<?php echo $file_server;?>" prog_loc = "view_program.php?file=<?php echo $pgmloc;?>"  access = "Yes" onclick="window.open('read_file.php?file=<?php echo $file_server;?>', 'newwindow', 'width=1200px, height=800px'); return false;" <?php } else{?>href="#" onclick="alert('You do not have access to this study.');"<?php }?> ><?php echo $find["type"].' '.$find["tlfnum"].' '.$find["title"]?></a></td>
																				</tr>
																<?php $sn++;	}}}?>
											        </tbody>
											    	</table>
													</div>
			                  </div>
			                          
								        <div class="col-md-6">
								          <div class="box">
								            <div class="box-header">
								              <h3 class="box-title">Preview Window</h3>
								              <div class="box-tools pull-right">      </div>
								            </div><!-- /.box-header -->
								            <div class="box-body no-padding">
								              <div class="row">
								                <div class="col-md-12 col-sm-12">
								                  <div class="pad">
								                     
								                    <div class="row">
								                    	<img class="loading-img" src="images/loading.gif" style="display: none;"/>
									                    <div class="col-md-12" id="preview">
									                    	<pre style="border: 1px solid black; display: block; overflow: auto; overflow-wrap: normal; padding: 5px; background-color: #f5f5f5;border-radius: 4px;color: #333;font-size: 13px;line-height: 1.42857; margin: 0 0 10px; width: 99%;"><p>Hint: Hover the mouse on table number and output name.</p></pre>
									                    </div>
								                    </div>


								                  </div>
								                </div><!-- /.col -->


								              </div><!-- /.row -->
								            </div><!-- /.box-body -->
								          </div><!-- /.box -->
								        </div><!-- /.col -->
			                </div>
			              <?php } else{?>
			              	<div class="row">
                      	<div class="col-md-12">
                        	<div class="results-summary">
                         		<strong>No result found for </strong>:&nbsp;&nbsp; <?php echo $title;?>
                        	</div>
                       </div>
                       <hr>
                    	</div>
			              <?php }?>
		              </div>
		            </div><!-- /.col -->


		          </div><!-- /.row -->
		        </div><!-- /.box-body -->
		      </div><!-- /.box -->
		    </div><!-- /.col -->

		  </div><!-- /.row (main row) -->
    </section><!-- /.content -->

<script>
	$(document).ready(function() {
		//$(".dataTable").on("mouseover","a.file-link",function(event){
		$("a.file-link").mouseover(function(event){
			$('#preview').children().remove();
			var file = $(this).attr("file");
			var href = $(this).attr("href");
			if(href == '#'){
				$('#preview').append("<pre>You do not have access to this study.</pre>");
			}
			else{
				$('img.loading-img').show();
				$.ajax({
					type: "POST", 
					async: false, 
					url: "read_file.php",
					data: {
					 	file:file
					},
					beforeSend: function(){
						
					},
				 	success: function(response){
				 		$('img.loading-img').hide();
						$('#preview').append(response);
				 	}
			 	});	
			}
		});
	});
</script>

<script>
	$(document).ready(function(){
		$('#data').after('<ul class="pagination"></ul>');
	  var rowsShown = 10;
	  var rowsTotal = $('#data tbody tr').length;
	  if(rowsTotal > rowsShown)
		{
			$('.pagination').append('<li class="disabled"><a href="#" rel="'+0+'"><span aria-hidden="true">&laquo;</span></a></li>');
			var numPages = rowsTotal/rowsShown;
		  for(i = 0;i < numPages;i++) {
		    var pageNum = i + 1;
		    $('.pagination').append('<li><a href="#" rel="'+i+'">'+pageNum+'</a></li>');
		  }
		  $('.pagination').append('<li><a href="#" rel="'+(i-1)+'"><span aria-hidden="true">&raquo;</span></a></li>');
		  $('#data tbody tr').hide();
		  $('#data tbody tr').slice(0, rowsShown).show();
		  $('.pagination a:first').addClass('active');
		  
		  $('.pagination a').bind('click', function(){
		    $('.pagination a').removeClass('active');
		    $('.pagination li').removeClass('disabled');
		    $(this).addClass('active');
		    $(this).parent().addClass('disabled');
		    
		    var currPage = $(this).attr('rel');
		    var startItem = currPage * rowsShown;
		    var endItem = startItem + rowsShown;
		    if((numPages-1) == currPage){
					$(this).parent().next().addClass('disabled');
				}
				
				if(currPage==0){
					$(this).parent().prev().addClass('disabled');
				}

		    $('#data tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
		      css('display','table-row').animate({opacity:1}, 300);
		  });
		}
	});
</script>
    <link href="contextmenu/jquery.contextMenu.css" rel="stylesheet" type="text/css" />
		<script src="js/jquery.min.js"></script>
    <script src="contextmenu/jquery.contextMenu.js" type="text/javascript"></script>

    <script src="contextmenu/jquery.ui.position.min.js" type="text/javascript"></script>

    <script src="js/main.js" type="text/javascript"></script>
<script>
function downloadURI(uri, name) 
{
    var link = document.createElement("a");
    link.download = name;
    link.href = uri;
    link.click();
}
	$(function() {
    $.contextMenu({
      selector: '.text', 
      callback: function(key, options) {
      	var access = options.$trigger.attr('access');
      	if(key == "Output"){
      		var out_loc = options.$trigger.attr('out_loc');
      		if(access == "Yes") window.open(out_loc,'GoogleWindow', 'width=1200, height=800');
      		else alert('You do not have access to this study.');
				}
				if(key == "Program"){
					var progloc = options.$trigger.attr('prog_loc');
					if(access == "Yes") window.location.href = progloc;
      		else alert('You do not have access to this study.');
				}
      },
      items: {
        "Output": {name: "View Output"},
        "Program": {name: "View Program"},
        //"quit": {name: "Quit"}
      }
    });    
  });
</script>
