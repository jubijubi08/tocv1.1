	<?php 
		include("include/connect.php");
		$valueOriginal 	= $_POST['value'];
		$index 					= $_POST['ind'];
		$valArr					= explode("+",$valueOriginal);
		$value 					= trim($valArr[0]);
		$title 					= trim($valArr[1]);
		/**
		*Query for select all available options for the selected field
		*/
		$list = array(); $ind = 0;
		if($value == 'title'){
			$list[$value] = $value;
		}else
		{
			$study_sql = $conn->query("SELECT * FROM study_info where status = 'ON' ");
			while($row = $study_sql->fetch_assoc()) {
				$study_name = $row['study_name'];

				$column = $conn->query("SHOW COLUMNS FROM `toc_$study_name` LIKE '$value'");
				$exists = ($column->num_rows)?TRUE:FALSE;
				if($exists)
				{
					$result			= $conn->query("SELECT DISTINCT $value FROM toc_$study_name  WHERE title !=''");
					while($data = $result->fetch_assoc()){
						if(!array_key_exists($data[$value], $list) && trim($data[$value]))
						    if($data[$value] == "PG"){
                                $list[$data[$value]] = "Parallel Study";
                            }
                            else if($data[$value] == "PG"){
                                $list[$data[$value]] = "Cross Study";
                            }
                            else $list[$data[$value]] = $data[$value];
					}
				}
			}
		}
	?>
<div id="input_<?php echo $index;?>" class="input">
	<hr/>
	<?php if(sizeof($list) > 0 && sizeof($list) <=10 && $value != "title"){?>
		<script type="text/javascript" src="bootstrap/js/bootstrap-multiselect.js"></script>
		<div class="form-group row criteria">
		  <label for="<?php echo $value;?>" class="col-xs-3 col-form-label">Criteria - <?php echo $title;?>:</label>
		  <div class="col-xs-8">
		    <select class="form-control added" name="<?php echo $value;?>" data="<?php echo $value;?>" data-val="" lvl = "<?php echo $title;?>"  id="<?php echo $value;?>" multiple="multiple">
		    	<?php
		    		foreach($list as $l) echo "<option value='$l'>$l</option>";
					?>
		    </select>    
		    <input type="hidden" name="<?php echo $value;?>" value="" id="<?php echo $value;?>" class="array"/>
		  </div>
		  <div class="col-xs-1">
	    	<a class="btn" href="#" id="delete_<?php echo $index;?>" data="<?php echo $valueOriginal;?>" onclick="myFunction(<?php echo $index;?>)"><span class='text-center glyphicon glyphicon glyphicon-remove'></span></a>
	    </div>
	    <script>
				$(document).ready(function() {
					$('#<?php echo $value;?>').multiselect();
					$("select#<?php echo $value;?>").on("change", function(event){ 
						var val = $(this).val();
						var len = $("select[name='<?php echo $value;?>'] option:selected").length;
						$("select#<?php echo $value;?>").attr("data-val",val);
						if(len > 0)$("select#<?php echo $value;?>").addClass("selected");
						else $("select#<?php echo $value;?>").removeClass("selected");
					});
				});
			</script>
		</div>
	<?php } else{?>
		<div class="form-group row criteria">
		  <label for="<?php echo $value;?>" class="col-xs-3 col-form-label">Criteria - <?php echo $title;?>:</label>
		  <div class="col-xs-8">
		    <input type="text" class="form-control added" name="<?php echo $value;?>" data="<?php echo $value;?>" data-val="" lvl = "<?php echo $title;?>"  id="<?php echo $value;?>">
		  </div>
		  <div class="col-xs-1">
	    	<a class="btn" href="#" id="delete_<?php echo $index;?>" data="<?php echo $valueOriginal;?>" onclick="myFunction(<?php echo $index;?>)"><span class='text-center glyphicon glyphicon glyphicon-remove'></span></a>
	    </div>
	    <script>
				$(document).ready(function() {
					$("input#<?php echo $value;?>").on("blur", function(event){ 
						var val = $(this).val();
						$("input#<?php echo $value;?>").attr("data-val",val);
						if($.trim(val))$("input#<?php echo $value;?>").addClass("selected");
						else $("input#<?php echo $value;?>").removeClass("selected");
					});
				});
			</script>
		</div>
	<?php }?>
	
	
	<script>
		function myFunction(ind){
			var data = $('#delete_'+ind).attr("data");
			var dataArr = data.split("+");
			$('#input_'+ind).remove();
			$('#category').append(
	      $('<option></option>').val(data).html(dataArr[1]));
		}
	</script>	
</div>