<?php

include("include/connect.php");
session_start();

$user_check=$_SESSION['login_user'];

$ses_sql=$conn->query("select username,firstlog from user_info where username='$user_check' ");
//print_r($ses_sql);

while($row = $ses_sql->fetch_assoc()) {
    $login_session = $row['username'];
    $firstlog = $row['firstlog'];
}

if(!isset($login_session))
{
    header("Location: index.php");
}

$username = $_SESSION['login_user'];
$sql="SELECT count(y.allocation_id) as snum FROM user_info as x , study_allocation as y WHERE x.username = '$username' AND x.user_id = y.user_id AND y.status='ON'";
$countstudy = $conn->query($sql);

while($row = $countstudy->fetch_assoc()) {
  $snum = $row['snum'];
}
	/**
	*Query for select all meta data which is set by trial admin
	*/
	$list = array();
	$study_sql = $conn->query("SELECT * FROM study_info where status = 'ON' ");
	while($row = $study_sql->fetch_assoc()) {
		$study_name = $row['study_name'];
		$result			= $conn->query("SELECT fieldname, searchtitle FROM metadata_$study_name  WHERE searchtitle !='' AND is_current = 'YES'");

		while($data = $result->fetch_assoc()){
			if(!array_key_exists($data["fieldname"], $list))
				$list["$data[fieldname]"] = $data["searchtitle"];
		}
	}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MeD-OMS</title>
<link rel="stylesheet" href="stree/demo.css" type="text/css">

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- Include Twitter Bootstrap and jQuery: -->
<link rel="stylesheet" href="bootstrap/css/bootstrap.min.css" type="text/css"/>
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
 
<!-- Include the plugin's CSS and JS: -->
<script type="text/javascript" src="bootstrap/js/bootstrap-multiselect.js"></script>
<link rel="stylesheet" href="bootstrap/css/bootstrap-multiselect.css" type="text/css"/>

<!-- FontAwesome 4.3.0 -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons 2.0.0 -->
<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
<!-- css top menu -->
<link rel="stylesheet" href="cssmenu/menustyles.css">
<!-- bootstrap wysihtml5 - text editor -->
<link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="dist/img/scbd.ico">

<link rel="stylesheet" href="stree/metroStyle/metroStyle.css" type="text/css">

<style>
  .scbd {
    height:65px;
    background:#ffffff url('dist/img/logo.png') no-repeat;
    text-align: center;
    position: relative;
    background-position:center;
    z-index:1000;
  }
  
  li, span{
    font-size:17px;
  }
</style>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>

</head>


<body class="skin-blue layout-wide">
<div class="scbd"></div>
<div class="wrapper">
<header class="main-header">
<!-- Logo -->
<a href="home.php" class="logo" ><b>MeD-OMS</b></a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation" > 
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>


    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <span class="hidden-xs"><?php  echo "$username";  ?> </span>
                </a>
                <ul class="dropdown-menu">

                    <li class="user-footer">
                        <div class="pull-left">
                            <p> <small>New Study:<?php  echo " $snum";  ?> Total Study:<?php  echo " $snum";  ?></small></p>
                        </div>

                        <div class="pull-right">
                            <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- drop down menu for user-->
        </ul>
    </div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar" style="padding-top: 80px;">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>



      <?php
           if ($firstlog == 'No'){

            echo "


        <li>
          <a href='search.php'>
            <i class='fa fa-search'></i> <span>Search</span> <small class='label pull-right bg-green'></small>
          </a>
        </li>
        
        <li>
            <a href='home.php'>
                <i class='fa fa-th'></i> <span>Home</span> <small class='label pull-right bg-green'></small>
            </a>
        </li>

                 ";
           }

      ?>
        <li>
          <a href="change_pass.php">
            <i class="fa fa-th"></i> <span>Change Password</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


    </ul>
</section>
<!-- /.sidebar -->
</aside>



<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      </ol>
<!--   <h1>    </h1> -->

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Study Across Advanced Search</h3>
             
              <div class="box-tools pull-right">      </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-9 col-sm-8">
                  <div class="pad">
                    <div class="row">
	                    <div class="col-md-12">
	                    	<div class="form-group has-feedback" id="criteria">
	                    		<div class="form-group row">
                            <label for="example-search-input" class="col-xs-3 col-form-label">Select Criteria:</label>
                            <div class="col-xs-8">
                              <select class="form-control" id="category">
                             		<?php
                             			if(sizeof($list) > 0){
                             				echo "<option value=''>Select</option>";
                             				echo "<option value='title+Title'>Search Title</option>";
																		foreach($list as $key => $l){ //echo $key.' '.$l;
																			echo "<option value='$key+$l'>$l</option>";
																		}
																	}
																?>
                              </select>
                            </div>
                          </div>                 
	                      </div>	
	                      
	                      <div class="alert alert-error select1" role = "alert" style="display: none;" id="text">
	                      	<p class="text-center">Please provide minimum one criteria.</p>
	                      </div>                                   
	                    </div>
		                  <form action="search_result.php" method="post"> 
		                  <div class="col-md-4"></div>
		                    <div class="col-md-3">
		                    	<input type="hidden" name="condition" id="condition" value=""/>
		                    	<input type="hidden" name="fors" id="fors" value=""/>
		                    	<input type="button" class="btn btn-primary btn-block btn-flat" id="search_submit" value="Search Now"/>
		                    </div>
		                   <div class="col-md-4"></div>
		                  </form>
                    </div>
                    
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row (main row) -->
    </section><!-- /.content -->
    <div id="search_result"></div>
    
  </div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>

<script>
	$(document).ready(function() { 
		$("input#search_submit").on("click", function(event){ 
			var inputs = false, selected = false;
			$('.input').each(function( index ) {
				inputs = true;
			});
			data = new Array(),indx = 0;
			if(inputs){
				$('.selected').each(function( index ) {
					selected = true;
					var ind = $(this).attr('data');
					var ind_val = $(this).attr('data-val');
					var lvl = $(this).attr('lvl');
					data[indx++] = new Array (ind,ind_val,lvl);
				});
				if(selected){
					console.log(data);
					//console.log(JSON.stringify(data));
					$.ajax({
						type: "POST",
						url: "search_result.php",
						data: {
						 	data:data
						},
						beforeSend: function(){
							
						},
					 	success: function(response){
							$('#search_result').children().remove();
							$('#search_result').append(response);
					 	}
				 	});
				}else{
					event.preventDefault();
					$('.select1').slideDown(function() {
				    setTimeout(function() {
				        $(".select1").slideUp();
				    }, 5000);
					});
				}
			}else{
				event.preventDefault();
				$('.select1').slideDown(function() {
			    setTimeout(function() {
			        $(".select1").slideUp();
			    }, 5000);
				});
			}
		});
		
		$("#category").on("change", function(event){
			var val = $(this).val();
			if(val == "") {
				return;
			}
			var ind = 1;
			$("div.criteria").each(function( index ) {
			  ind++;
			});
			
			$.ajax({
				type: "POST", 
				async: false, 
				url: "add_field.php",
				data: {
				 	ind:ind,value:val
				},
				beforeSend: function(){
					
				},
			 	success: function(response){
					$('#criteria').append(response);
					$("#category:visible option[value='"+val+"']").remove();
			 	}
		 	});
		});
	});
</script>