<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];

//Check if any common programs exist or not
$study_name=strtolower($study_name);
$table="cplist_$study_name";
$result=$conn->query("SHOW TABLES");
while($row = mysqli_fetch_row($result)){
    $arr[] = $row[0];
}
if(in_array($table,$arr))
{
    $cp_exist=1;
}else{
    $cp_exist=0;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Manage Programs</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">

                <!-- MAP & BOX PANE -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?>&nbsp;
                            <a href="#" data-toggle="tooltip" title="Refresh Page"><i class="fa fa-refresh" aria-hidden="true" onclick="location.reload();"></i></a>
                        </h3>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-12 col-sm-8">
                                <div class="pad">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <p style="font-size:18px;">List of All Common Programs</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">

                                            <?php
                                            $sql = "SELECT * FROM cplist_$study_name";
                                            $result = $conn->query($sql);
                                            // print_r($result);

                                            // if($result){
                                            if ($result->num_rows > 0) {
                                                echo "<table id='myTable' class='table table-hover'>";
                                                echo "<thead>";
                                                echo "<tr>";
                                                echo "<th>Program Location</th>";
                                                echo "<th>Program Name</th>";
                                                echo "<th>Programmer Name</th>";
                                                echo "<th>Last Modify Date</th>";
                                                echo "<th>Validator Name</th>";
                                                echo "<th>Last Status</th>";
                                                echo "<th>Last Status Chnage Date</th>";
                                                echo "</tr>";
                                                echo "</thead>";
                                                echo "<tbody>";

                                                while($row = $result->fetch_assoc()) {
                                                    echo "<tr>";
                                                    echo "<td class='pg'>" . $row["cploc"].  "</td>";
                                                    echo "<td class='pg'>" . $row["cpname"].  "</td>";
                                                    echo "<td class='pg'>" . $row["programmer"].  "</td>";
                                                    echo "<td class='pg'>" . $row["cpdate"].  "</td>";
                                                    echo "<td class='pg'>" . $row["validator"].  "</td>";
                                                    if($row["pgmstat"] == 1){
                                                        echo "<td class='pg'><span class='label label-warning'>".$row["status"]."</span></td>";
                                                    }
                                                    elseif($row["pgmstat"] == 2){
                                                        echo "<td class='pg'><span class='label label-danger'>".$row["status"]."</span></td>";
                                                    }
                                                    else{
                                                        echo "<td class='pg'><span class='label label-success'>".$row["status"]."</span></td>";
                                                    }
                                                    echo "<td class='pg'>" . $row["status_date"].  "</td>";
                                                    echo "</tr>";
                                                }

                                                echo "</tbody>";
                                                echo "</table>";
                                            } else {
                                                echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                            }
                                            // }else {
                                            // echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                            // }

                                            ?>

                                            <ul id="contextMenu" class="dropdown-menu" role="menu" style="left:230px;" >
                                                <li><a tabindex="-1" href="#">Edit Program</a></li>
                                                <li><a tabindex="-1" href="#">Validation Status</a></li>
                                                <li><a tabindex="-1" href="#">Program History</a></li>
                                                <li><a tabindex="-1" href="#">Download</a></li>
                                            </ul>

                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-9">
                                            <p style="font-size:18px;"> Upload new Programs here</p>
                                        </div>
                                        <div class="col-md-3">
                                            <button  class="btn btn-primary btn-block btn-flat" data-toggle="modal" data-target=".bs-example-common-up-modal-lg">Upload</button>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<!--  Edit Common program  model -->
<div class="modal fade bs-example-editcp-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Programs</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="editpgm"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <!-- HTML form -->
                                    <form id="edtfrm" action="" method="post">
                                        <input id="editcpgm_sid" type="hidden" name="editcpgm_sid" value= >
                                        <pre><code contenteditable id="prgm" class="language-sas"></code></pre>
                                        <textarea name="prgm" id="prgm1" style="display:none;"></textarea>
                                        <input type="submit" style="margin:10px 20px 10px 30px;" Value="Save" onclick="var x = $('#prgm').text(); $('#prgm1').text(x); $('#edtfrm').submit();"/>
                                        <input type="reset" />
                                    </form>

                                </div><!-- /.col -->
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include("include/footer.php");
$conn->close();
?>
