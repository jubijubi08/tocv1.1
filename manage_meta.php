<?php
	include("include/header.php");
	include("include/connect.php");
	$study_name=$_SESSION["study"];

	$sql = "SELECT id, fieldname, searchtitle, value FROM metadata_$study_name WHERE is_current = 'YES'";
  $result = $conn->query($sql);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Manage Metadata</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">

        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?>
                <div class="box-tools pull-right">

                    <div class="col-md-12">

                    </div>
                </div>
            </h3>
          </div><!-- /.box-header -->
          
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-12 col-sm-8">
                <div class="pad">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-center" style="font-size:18px;">Manage Metadata</p>
                    </div>
                  </div>
                  
                  <div class="row">
                  	<div class="col-md-2"></div>
                  	<div class="col-md-8">
                  		<table class="table table-hover" id="metadata">
                  			<thead>
                  				<tr>
                  					<th>Field Name</th>
                  					<th>Search Title</th>
                  					<th>Value</th>
                  					<th style="width: 25%;">
                  						<a class="btn btn-warning history" target="_blank" href="meta_history.php" ><span class="fa fa-history"></span></a>&nbsp; &nbsp;
                  						<a class="btn btn-info add" href="#" ><span class="glyphicon glyphicon-plus"></span></a>
                  					</th>
                  				</tr>
                  			</thead>
                  			<tbody>
                  				<?php
                  					while($row = $result->fetch_assoc()) {?>
                  						<tr>
                  							<td class="fieldname"><?php echo $row["fieldname"]?></td>
                  							<td class="searchtitle"><?php echo $row["searchtitle"]?></td>
                  							<td class="value"><?php echo $row["value"]?></td>
                  							<td><a class="btn btn-warning" target="_blank" href="meta_history.php?data=<?php echo $row["fieldname"]?>" ><span class="fa fa-history"></span></a>&nbsp;&nbsp;<a class="btn btn-success edit" href="#" data-id = "<?php echo $row["id"]?>"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;&nbsp;<a class="btn btn-danger delete" href="#" data-id = "<?php echo $row["id"]?>"><span class="glyphicon glyphicon-trash"></span></a></td>
                  						</tr>
                  					<?php }?>
                  			</tbody>
                  		</table>
                  	</div>
                  	<div class="col-md-2"></div>
                  </div>

                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!-- /.col -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->
<script>
	$(document).ready(function(){
		
		$('#save').on('click', function () {
			var fd = new FormData(document.getElementById("edit-form"));
			$.ajax({
        url: 'ajax/edit_meta_edit.php',
        type: 'POST',
        data:fd,
        cache: false,
        dataType: 'json',
        processData: false,
        contentType: false,
        success: function (data) {
          if (data.success == "1") {
            $("#meta-data-modal").modal('hide');
            //console.log(data.value);
            $("#foredit").parent().replaceWith(data.value);
            $.notify({
                title: '<strong>SUCCESS!</strong>',
                message: 'Meta information has been updated'
            }, {
                type: 'success'
            });
          }
          else {
            $("#meta-data-modal").modal('hide');
            $.notify({
                title: '<strong>Failed to Update!</strong>',
                message: 'Something went wrong, Please try again'
            }, {
                type: 'danger'
            });
          }
        }
    	});
		});

        $(document).on('click', '.delete', function (e) {
			if(confirm("Are you sure you want to delete this meta information?"))
			{
				var id 		= $(this).attr("data-id");
				$("#delete-id").val(id);
				$(this).parent().parent().attr("id", "fordelete");
				var fd = new FormData(document.getElementById("delete-form"));
				$.ajax({
	        url: 'ajax/edit_meta_edit.php',
	        type: 'POST',
	        data:fd,
	        cache: false,
	        dataType: 'json',
	        processData: false,
	        contentType: false,
	        success: function (data) {
	          if (data.success == "1") {
	            $("#meta-data-modal").modal('hide');
	            $('#fordelete').remove();
	            $.notify({
	                title: '<strong>SUCCESS!</strong>',
	                message: 'Meta information has been updated'
	            }, {
	                type: 'success'
	            });
	          }
	          else {
	            $("#meta-data-modal").modal('hide');
	            $.notify({
	                title: '<strong>Failed to Update!</strong>',
	                message: 'Something went wrong, Please try again'
	            }, {
	                type: 'danger'
	            });
	          }
	        }
	    	});	
			}
		});	
		
		$('.add').on('click', function () {
			$("#add-meta-data-modal").modal('show');
		});
		
		$('#InSearch').on('click', function () {
			if($(this). prop("checked") == true){
				$("#titlesrch").show();
				var val = $(this).attr("data-val");
				$("input#searchtitle").val(val);
			}else{
				$("#titlesrch").hide();
				$("input#searchtitle").val('');
			}
		});
		
		$('#InSearchInsert').on('click', function () {
			if($(this). prop("checked") == true){
				$("#titlesrchInsert").show();
				$("input#searchtitle").attr("required", true);
			}else{
				$("#titlesrchInsert").hide();
				$("input#searchtitle").attr("required", false);
			}
		});
		
		
		$('#insert').on('click', function () {
    	if(!$('#form')[0].checkValidity()) {
	      $('#submit').click();
	    }else{
				var fd = new FormData(document.getElementById("form"));
				$.ajax({
	        url: 'ajax/edit_meta_edit.php',
	        type: 'POST',
	        data:fd,
	        cache: false,
	        dataType: 'json',
	        processData: false,
	        contentType: false,
	        success: function (data) {
	          if (data.success == "1") {
	            $("#add-meta-data-modal").modal('hide');
	            $('#metadata > tbody:last-child').append(data.value);
	            $.notify({
	                title: '<strong>SUCCESS!</strong>',
	                message: 'New Meta information has inserted'
	            }, {
	                type: 'success'
	            });
	          }
	          else {
	            $("#add-meta-data-modal").modal('hide');
	            $.notify({
	                title: '<strong>Failed to Insert!</strong>',
	                message: 'Something went wrong, Please try again'
	            }, {
	                type: 'danger'
	            });
	          }
	        }
	    	});
			}
		});
		
		$('[data-dismiss=modal]').on('click', function (e) {
    	var $t = $(this),
        target = $t[0].href || $t.data("target") || $t.parents('.modal') || [];
    
	  	$(target)
	    	.find("input,textarea,select")
	      	.val('')
	      	.end()
	    	.find("input[type=checkbox], input[type=radio]")
	      	.prop("checked", "")
	      	.end();
		});
	});
    $(document).on('click', '.edit', function (e) {
        e.preventDefault();
        console.log("clicked");
        var id 			= $(this).attr("data-id");
        var fieldName = $(this).parent().parent().find(".fieldname").text();
        var title = $(this).parent().parent().find(".searchtitle").text();
        var value = $(this).parent().parent().find(".value").text();
        $(this).parent().prev('td').attr("id", "foredit");

        $(".modal-body").find('#FieldName').html(fieldName);
        $(".modal-body").find('#id').val(id);
        $(".modal-body").find('#Names').val(fieldName);
        $(".modal-body").find('#prevValue').val(value);
        $(".modal-body").find('#fieldvalue').val(value);

        if($.trim(title)){
            $(".modal-body").find('#InSearch').attr('checked',true);
            $(".modal-body").find('#InSearch').attr('data-val',title);
            $(".modal-body").find('#searchtitle').val(title);
            $(".modal-body").find("#titlesrch").show();
        }
        $("#meta-data-modal").modal('show');
    });
</script>
		<form id="delete-form" style="display: none;">
			<input type="hidden" name="id" id="delete-id" value="id"/>
			<input type="hidden" name="action" id="action" value="delete"/>
		</form>
		<div class="modal fade in" id="meta-data-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span></button>
						<h4 class="modal-title" id="myModalLabel">
							<span class="glyphicon glyphicon-pencil"></span> Edit Metadata</h4>
					</div>
					<div class="modal-body">
						<form id="edit-form" class="form-horizontal" action="#" method="post"  enctype="multipart/form-data">
							<input type="hidden" name="id" id="id" value=""/>
							<input type="hidden" name="Names" id="Names" value=""/>
							<input type="hidden" name="prevValue" id="prevValue" value=""/>
							<input type="hidden" name="action" id="action" value="update"/>
							
							<div class="form-group field-exam-title required">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="FieldName" for="FieldName" class="control-label"></label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="text" maxlength="50" name="fieldvalue" class="form-control" id="fieldvalue" value="test value" placeholder="" required>
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
							<div class="form-group field-is_search">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="isSearch" for="isSearch" class="control-label"></label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="checkbox" maxlength="50" name="InSearch" id="InSearch" data-val="" value="Yes">Search
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
							<div class="form-group field-search-title" id="titlesrch" style="display: none;">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="srchtitle" for="searchtitles" class="control-label">Search Title</label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="text" maxlength="50" name="searchtitle" id="searchtitle" class="form-control" value=""  data-val ="" placeholder="">
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" data-val = "" data-id ="" id="save">Save</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
		
		<div class="modal fade in" id="add-meta-data-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">x</span></button>
						<h4 class="modal-title" id="myModalLabel">
							<span class="glyphicon glyphicon-plus"></span> Add Metadata</h4>
					</div>
					<div class="modal-body">
						<form id="form" class="form-horizontal" action="#" method="post"  enctype="multipart/form-data">
							<input type="hidden" name="action" id="action" value="insert"/>
							
							<div class="form-group field-exam-title required">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="Name" for="Name" class="control-label">Name</label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="text" maxlength="50" name="NameValue" class="form-control" id="NameValue" value="" placeholder="Phase" required>
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
							<div class="form-group field-exam-title required">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="fieldvalue" for="fieldvalue" class="control-label">Value</label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="text" maxlength="50" name="fieldvalue" class="form-control" id="fieldvalue" value="" placeholder="1" required>
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
							<div class="form-group field-is_search">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="isSearch" for="isSearch" class="control-label"></label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="checkbox" maxlength="50" name="InSearch" id="InSearchInsert" value="">Search
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
							<div class="form-group field-search-title" id="titlesrchInsert" style="display: none;">
								<label class="col-xs-8 col-sm-7 col-md-5 col-lg-4 control-label">
								<label id="srchtitle" for="searchtitle" class="control-label">Search Title</label></label>
								<div class="col-xs-8 col-sm-7 col-md-5 col-lg-6">
									<input type="text" maxlength="50" name="searchtitle" id="searchtitle" class="form-control" value="" placeholder="Search Title">
								</div>
								<div class="col-xs-6 col-sm-5 col-md-3 col-lg-3">
									<div class="help-block"></div>
								</div>
							</div>
							
    					<input type="submit" id="submit" style="display:none;" />
					</form>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-primary" id="insert">Insert</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>

<?php
include("include/footer.php");
$conn->close();
?>  