-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 11, 2016 at 08:51 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toc_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_prma_2016_04_06_12_39_03`
--

CREATE TABLE IF NOT EXISTS `bk_toc_prma_2016_04_06_12_39_03` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_prma_2016_04_06_12_39_03`
--

INSERT INTO `bk_toc_prma_2016_04_06_12_39_03` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('PRMA', '0100000000', '01', '00', '00', '00', '00', 'TURKEY', '', '', '', '', '', '', '', '', ''),
('PRMA', '0101000000', '01', '01', '00', '00', '00', '', 'TABLE', 'Table A1', 'Clinical role of clinicians who participated in the survey', 'pgm/tlf/tlf_tur/table_a1.sas', 'table_a1.sas', 'T1', 'tlf/turkey/table_a1.rtf', 'table_a1.rtf', 'table_a1.lst'),
('PRMA', '0102000000', '01', '02', '00', '00', '00', '', 'TABLE', 'Table A2', 'Guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_a2.sas', 'table_a2.sas', 'T2', 'tlf/turkey/table_a2.rtf', 'table_a2.rtf', 'table_a2.lst'),
('PRMA', '0103000000', '01', '03', '00', '00', '00', '', 'TABLE', 'Table A3', 'Care of patients newly receiving statins', 'pgm/tlf/tlf_tur/table_a3.sas', 'table_a3.sas', 'T3', 'tlf/turkey/table_a3.rtf', 'table_a3.rtf', 'table_a3.lst'),
('PRMA', '0104000000', '01', '04', '00', '00', '00', '', 'TABLE', 'Table A4', 'Care of patients newly receiving statins: specialists/consultants', 'pgm/tlf/tlf_tur/table_a4.sas', 'table_a4.sas', 'T3', 'tlf/turkey/table_a4.rtf', 'table_a4.rtf', 'table_a4.lst'),
('PRMA', '0105000000', '01', '05', '00', '00', '00', '', 'TABLE', 'Table A5', 'Care of patients newly receiving statins: general/family physicians', 'pgm/tlf/tlf_tur/table_a5.sas', 'table_a5.sas', 'T3', 'tlf/turkey/table_a5.rtf', 'table_a5.rtf', 'table_a5.lst'),
('PRMA', '0106000000', '01', '06', '00', '00', '00', '', 'TABLE', 'Table A6', 'Number of clinicians who perform transaminase (ALT/AST) testing for patients newly prescribed statins', 'pgm/tlf/tlf_tur/table_a6.sas', 'table_a6.sas', '', 'tlf/turkey/table_a6.rtf', 'table_a6.rtf', 'table_a6.lst'),
('PRMA', '0107000000', '01', '07', '00', '00', '00', '', 'TABLE', 'Table A7', 'Reasons for transaminase testing', 'pgm/tlf/tlf_tur/table_a7.sas', 'table_a7.sas', 'T5', 'tlf/turkey/table_a7.rtf', 'table_a7.rtf', 'table_a7.lst'),
('PRMA', '0108000000', '01', '08', '00', '00', '00', '', 'TABLE', 'Table A8', 'Number of clinicians who perform creatine kinase testing for patients newly prescribed statins', 'pgm/tlf/tlf_tur/table_a8.sas', 'table_a8.sas', 'T5', 'tlf/turkey/table_a8.rtf', 'table_a8.rtf', 'table_a8.lst'),
('PRMA', '0109000000', '01', '09', '00', '00', '00', '', 'TABLE', 'Table A9', 'Reasons for creatine kinase testing', 'pgm/tlf/tlf_tur/table_a9.sas', 'table_a9.sas', 'T2', 'tlf/turkey/table_a9.rtf', 'table_a9.rtf', 'table_a9.lst'),
('PRMA', '0110000000', '01', '10', '00', '00', '00', '', 'TABLE', 'Table A10', 'Association of specific symptoms with use of statins', 'pgm/tlf/tlf_tur/table_a10.sas', 'table_a10.sas', 'T2', 'tlf/turkey/table_a10.rtf', 'table_a10.rtf', 'table_a10.lst'),
('PRMA', '0111000000', '01', '11', '00', '00', '00', '', 'TABLE', 'Table A11', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_tur/table_a11.sas', 'table_a11.sas', 'T1', 'tlf/turkey/table_a11.rtf', 'table_a11.rtf', 'table_a11.lst'),
('PRMA', '0112000000', '01', '12', '00', '00', '00', '', 'TABLE', 'Table A12', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_a12.sas', 'table_a12.sas', 'T3', 'tlf/turkey/table_a12.rtf', 'table_a12.rtf', 'table_a12.lst'),
('PRMA', '0113000000', '01', '13', '00', '00', '00', '', 'TABLE', 'Table A13', 'Signs or symptoms that might be associated with intolerance to statin therapy specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_a13.sas', 'table_a13.sas', 'T2', 'tlf/turkey/table_a13.rtf', 'table_a13.rtf', 'table_a13.lst'),
('PRMA', '0114000000', '01', '14', '00', '00', '00', '', 'TABLE', 'Table A14', 'Importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_a14.sas', 'table_a14.sas', 'T3', 'tlf/turkey/table_a14.rtf', 'table_a14.rtf', 'table_a14.lst'),
('PRMA', '0115000000', '01', '15', '00', '00', '00', '', 'TABLE', 'Table A15', 'Minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_a15.sas', 'table_a15.sas', 'T2', 'tlf/turkey/table_a15.rtf', 'table_a15.rtf', 'table_a15.lst'),
('PRMA', '0116000000', '01', '16', '00', '00', '00', '', 'TABLE', 'Table A16', 'Number of statins tried before considering patients to be unable to tolerate statins', 'pgm/tlf/tlf_tur/table_a16.sas', 'table_a16.sas', 'T2', 'tlf/turkey/table_a16.rtf', 'table_a16.rtf', 'table_a16.lst'),
('PRMA', '0117000000', '01', '17', '00', '00', '00', '', 'TABLE', 'Table A17', 'Identification of patients who cannot tolerate statins at the recommended therapeutic dose', 'pgm/tlf/tlf_tur/table_a17.sas', 'table_a17.sas', 'T2', 'tlf/turkey/table_a17.rtf', 'table_a17.rtf', 'table_a17.lst'),
('PRMA', '0118000000', '01', '18', '00', '00', '00', '', 'TABLE', 'Table A18', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_tur/table_a18.sas', 'table_a18.sas', 'T4', 'tlf/turkey/table_a18.rtf', 'table_a18.rtf', 'table_a18.lst'),
('PRMA', '0119000000', '01', '19', '00', '00', '00', '', 'TABLE', 'Table A19', 'Guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_a19.sas', 'table_a19.sas', 'T2', 'tlf/turkey/table_a19.rtf', 'table_a19.rtf', 'table_a19.lst'),
('PRMA', '0120000000', '01', '20', '00', '00', '00', '', 'TABLE', 'Table A20', 'Clinical characteristics of patients unable to tolerate statins', 'pgm/tlf/tlf_tur/table_a20.sas', 'table_a20.sas', 'T2', 'tlf/turkey/table_a20.rtf', 'table_a20.rtf', 'table_a20.lst'),
('PRMA', '0121000000', '01', '21', '00', '00', '00', '', 'TABLE', 'Table A21', 'Treatment strategy', 'pgm/tlf/tlf_tur/table_a21.sas', 'table_a21.sas', 'T2', 'tlf/turkey/table_a21.rtf', 'table_a21.rtf', 'table_a21.lst'),
('PRMA', '0122000000', '01', '22', '00', '00', '00', '', 'TABLE', 'Table A22', 'Non-statin lipid-lowering therapies used as monotherapy', 'pgm/tlf/tlf_tur/table_a22.sas', 'table_a22.sas', 'T5', 'tlf/turkey/table_a22.rtf', 'table_a22.rtf', 'table_a22.lst'),
('PRMA', '0123000000', '01', '23', '00', '00', '00', '', 'TABLE', 'Table A23', 'Non-statin lipid-lowering therapies used in combination with a statin', 'pgm/tlf/tlf_tur/table_a23.sas', 'table_a23.sas', 'T5', 'tlf/turkey/table_a23.rtf', 'table_a23.rtf', 'table_a23.lst'),
('PRMA', '0124000000', '01', '24', '00', '00', '00', '', 'TABLE', 'Table A23a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose', 'pgm/tlf/tlf_tur/table_a23a.sas', 'table_a23a.sas', 'T5', 'tlf/turkey/table_a23a.rtf', 'table_a23a.rtf', 'table_a23a.lst'),
('PRMA', '0125000000', '01', '25', '00', '00', '00', '', 'TABLE', 'Table A24', 'Use of lipid goals to determine treatment efficacy in clinical practice', 'pgm/tlf/tlf_tur/table_a24.sas', 'table_a24.sas', 'T2', 'tlf/turkey/table_a24.rtf', 'table_a24.rtf', 'table_a24.lst'),
('PRMA', '0126000000', '01', '26', '00', '00', '00', '', 'TABLE', 'Table A25', 'Perceived clinical benefit of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_tur/table_a25.sas', 'table_a25.sas', 'T5', 'tlf/turkey/table_a25.rtf', 'table_a25.rtf', 'table_a25.lst'),
('PRMA', '0127000000', '01', '27', '00', '00', '00', '', 'TABLE', 'Table A26', 'Reason for discontinuation of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_tur/table_a26.sas', 'table_a26.sas', 'T2', 'tlf/turkey/table_a26.rtf', 'table_a26.rtf', 'table_a26.lst'),
('PRMA', '0128000000', '01', '28', '00', '00', '00', '', 'TABLE', 'Table B1', 'Characteristics of clinicians who participated in the survey', 'pgm/tlf/tlf_tur/table_b1.sas', 'table_b1.sas', 'T5', 'tlf/turkey/table_b1.rtf', 'table_b1.rtf', 'table_b1.lst'),
('PRMA', '0129000000', '01', '29', '00', '00', '00', '', 'TABLE', 'Table B2', 'List of other guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_b2.sas', 'table_b2.sas', 'T5', 'tlf/turkey/table_b2.rtf', 'table_b2.rtf', 'table_b2.lst'),
('PRMA', '0130000000', '01', '30', '00', '00', '00', '', 'TABLE', 'Table B3', 'Combinations of guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_b3.sas', 'table_b3.sas', '', 'tlf/turkey/table_b3.rtf', 'table_b3.rtf', 'table_b3.lst'),
('PRMA', '0131000000', '01', '31', '00', '00', '00', '', 'TABLE', 'Table B4', 'Modification of guidelines', 'pgm/tlf/tlf_tur/table_b4.sas', 'table_b4.sas', 'T2', 'tlf/turkey/table_b4.rtf', 'table_b4.rtf', 'table_b4.lst'),
('PRMA', '0132000000', '01', '32', '00', '00', '00', '', 'TABLE', 'Table B5', 'Timing of transaminase testing', 'pgm/tlf/tlf_tur/table_b5.sas', 'table_b5.sas', 'T5', 'tlf/turkey/table_b5.rtf', 'table_b5.rtf', 'table_b5.lst'),
('PRMA', '0133000000', '01', '33', '00', '00', '00', '', 'TABLE', 'Table B6', 'Timing of transaminase testing â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b6.sas', 'table_b6.sas', 'T5', 'tlf/turkey/table_b6.rtf', 'table_b6.rtf', 'table_b6.lst'),
('PRMA', '0134000000', '01', '34', '00', '00', '00', '', 'TABLE', 'Table B7', 'Timing of transaminase testing â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b7.sas', 'table_b7.sas', 'T5', 'tlf/turkey/table_b7.rtf', 'table_b7.rtf', 'table_b7.lst'),
('PRMA', '0135000000', '01', '35', '00', '00', '00', '', 'TABLE', 'Table B8', 'Timing of creatine kinase testing', 'pgm/tlf/tlf_tur/table_b8.sas', 'table_b8.sas', 'T5', 'tlf/turkey/table_b8.rtf', 'table_b8.rtf', 'table_b8.lst'),
('PRMA', '0136000000', '01', '36', '00', '00', '00', '', 'TABLE', 'Table B9', 'Timing of creatine kinase testing â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b9.sas', 'table_b9.sas', 'T5', 'tlf/turkey/table_b9.rtf', 'table_b9.rtf', 'table_b9.lst'),
('PRMA', '0137000000', '01', '37', '00', '00', '00', '', 'TABLE', 'Table B10', 'Timing of creatine kinase testing â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b10.sas', 'table_b10.sas', 'T5', 'tlf/turkey/table_b10.rtf', 'table_b10.rtf', 'table_b10.lst'),
('PRMA', '0138000000', '01', '38', '00', '00', '00', '', 'TABLE', 'Table B11', 'Association of specific symptoms to use of statins', 'pgm/tlf/tlf_tur/table_b11.sas', 'table_b11.sas', 'T2', 'tlf/turkey/table_b11.rtf', 'table_b11.rtf', 'table_b11.lst'),
('PRMA', '0139000000', '01', '39', '00', '00', '00', '', 'TABLE', 'Table B12', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_tur/table_b12.sas', 'table_b12.sas', 'T2', 'tlf/turkey/table_b12.rtf', 'table_b12.rtf', 'table_b12.lst'),
('PRMA', '0140000000', '01', '40', '00', '00', '00', '', 'TABLE', 'Table B13', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_b13.sas', 'table_b13.sas', 'T3', 'tlf/turkey/table_b13.rtf', 'table_b13.rtf', 'table_b13.lst'),
('PRMA', '0141000000', '01', '41', '00', '00', '00', '', 'TABLE', 'Table B14', 'Magnitude of increase in transaminases that leads to modifying/discontinuing statin regimen', 'pgm/tlf/tlf_tur/table_b14.sas', 'table_b14.sas', 'T2', 'tlf/turkey/table_b14.rtf', 'table_b14.rtf', 'table_b14.lst'),
('PRMA', '0142000000', '01', '42', '00', '00', '00', '', 'TABLE', 'Table B15', 'Use of questionnaires/tools to guide assessment of statin intolerance', 'pgm/tlf/tlf_tur/table_b15.sas', 'table_b15.sas', 'T2', 'tlf/turkey/table_b15.rtf', 'table_b15.rtf', 'table_b15.lst'),
('PRMA', '0143000000', '01', '43', '00', '00', '00', '', 'TABLE', 'Table B15b', 'Magnitude of increase in serum creatine kinase that leads to consider intolerance to statins', 'pgm/tlf/tlf_tur/table_b15b.sas', 'table_b15b.sas', 'T2', 'tlf/turkey/table_b15b.rtf', 'table_b15b.rtf', 'table_b15b.lst'),
('PRMA', '0144000000', '01', '44', '00', '00', '00', '', 'TABLE', 'Table B16', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b16.sas', 'table_b16.sas', 'T3', 'tlf/turkey/table_b16.rtf', 'table_b16.rtf', 'table_b16.lst'),
('PRMA', '0145000000', '01', '45', '00', '00', '00', '', 'TABLE', 'Table B17', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b17.sas', 'table_b17.sas', 'T3', 'tlf/turkey/table_b17.rtf', 'table_b17.rtf', 'table_b17.lst'),
('PRMA', '0146000000', '01', '46', '00', '00', '00', '', 'TABLE', 'Table B18', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b18.sas', 'table_b18.sas', 'T3', 'tlf/turkey/table_b18.rtf', 'table_b18.rtf', 'table_b18.lst'),
('PRMA', '0147000000', '01', '47', '00', '00', '00', '', 'TABLE', 'Table B19', 'Distribution of importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b19.sas', 'table_b19.sas', 'T4', 'tlf/turkey/table_b19.rtf', 'table_b19.rtf', 'table_b19.lst'),
('PRMA', '0148000000', '01', '48', '00', '00', '00', '', 'TABLE', 'Table B20', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b20.sas', 'table_b20.sas', 'T4', 'tlf/turkey/table_b20.rtf', 'table_b20.rtf', 'table_b20.lst'),
('PRMA', '0149000000', '01', '49', '00', '00', '00', '', 'TABLE', 'Table B21', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b21.sas', 'table_b21.sas', 'T4', 'tlf/turkey/table_b21.rtf', 'table_b21.rtf', 'table_b21.lst'),
('PRMA', '0150000000', '01', '50', '00', '00', '00', '', 'TABLE', 'Table B22', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b22.sas', 'table_b22.sas', 'T4', 'tlf/turkey/table_b22.rtf', 'table_b22.rtf', 'table_b22.lst'),
('PRMA', '0151000000', '01', '51', '00', '00', '00', '', 'TABLE', 'Table B23', 'Comparative importance of symptoms to establish intolerance to statins', 'pgm/tlf/tlf_tur/table_b23.sas', 'table_b23.sas', 'T4', 'tlf/turkey/table_b23.rtf', 'table_b23.rtf', 'table_b23.lst'),
('PRMA', '0152000000', '01', '52', '00', '00', '00', '', 'TABLE', 'Table B24', 'Other criteria considered as important to establish intolerance to statins', 'pgm/tlf/tlf_tur/table_b24.sas', 'table_b24.sas', 'T2', 'tlf/turkey/table_b24.rtf', 'table_b24.rtf', 'table_b24.lst'),
('PRMA', '0153000000', '01', '53', '00', '00', '00', '', 'TABLE', 'Table B25', 'List of additionalbminimum criteria used to establish intolerance to statinsain patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b25.sas', 'table_b25.sas', 'T2', 'tlf/turkey/table_b25.rtf', 'table_b25.rtf', 'table_b25.lst'),
('PRMA', '0154000000', '01', '54', '00', '00', '00', '', 'TABLE', 'Table B26', 'Minimum criteria used to establish intolerance to statins in patients with elevated serum creatine kinase but no muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b26.sas', 'table_b26.sas', 'T2', 'tlf/turkey/table_b26.rtf', 'table_b26.rtf', 'table_b26.lst'),
('PRMA', '0155000000', '01', '55', '00', '00', '00', '', 'TABLE', 'Table B27', 'Minimum criteria used to establish intolerance to statins in patients with CNS effects', 'pgm/tlf/tlf_tur/table_b27.sas', 'table_b27.sas', 'T2', 'tlf/turkey/table_b27.rtf', 'table_b27.rtf', 'table_b27.lst'),
('PRMA', '0156000000', '01', '56', '00', '00', '00', '', 'TABLE', 'Table B28', 'List of additional minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b28.sas', 'table_b28.sas', 'T2', 'tlf/turkey/table_b28.rtf', 'table_b28.rtf', 'table_b28.lst'),
('PRMA', '0157000000', '01', '57', '00', '00', '00', '', 'TABLE', 'Table B29', 'Minimum criteria used to establish intolerance to statins in patients with other symptoms', 'pgm/tlf/tlf_tur/table_b29.sas', 'table_b29.sas', 'T2', 'tlf/turkey/table_b29.rtf', 'table_b29.rtf', 'table_b29.lst'),
('PRMA', '0158000000', '01', '58', '00', '00', '00', '', 'TABLE', 'Table B30', 'Criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b30.sas', 'table_b30.sas', 'T2', 'tlf/turkey/table_b30.rtf', 'table_b30.rtf', 'table_b30.lst'),
('PRMA', '0159000000', '01', '59', '00', '00', '00', '', 'TABLE', 'Table B31', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b31.sas', 'table_b31.sas', 'T2', 'tlf/turkey/table_b31.rtf', 'table_b31.rtf', 'table_b31.lst'),
('PRMA', '0160000000', '01', '60', '00', '00', '00', '', 'TABLE', 'Table B32', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b32.sas', 'table_b32.sas', 'T2', 'tlf/turkey/table_b32.rtf', 'table_b32.rtf', 'table_b32.lst'),
('PRMA', '0161000000', '01', '61', '00', '00', '00', '', 'TABLE', 'Table B33', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b33.sas', 'table_b33.sas', 'T2', 'tlf/turkey/table_b33.rtf', 'table_b33.rtf', 'table_b33.lst'),
('PRMA', '0162000000', '01', '62', '00', '00', '00', '', 'TABLE', 'Table B34', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b34.sas', 'table_b34.sas', 'T4', 'tlf/turkey/table_b34.rtf', 'table_b34.rtf', 'table_b34.lst'),
('PRMA', '0163000000', '01', '63', '00', '00', '00', '', 'TABLE', 'Table B35', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b35.sas', 'table_b35.sas', 'T4', 'tlf/turkey/table_b35.rtf', 'table_b35.rtf', 'table_b35.lst'),
('PRMA', '0164000000', '01', '64', '00', '00', '00', '', 'TABLE', 'Table B36', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_tur/table_b36.sas', 'table_b36.sas', 'T2', 'tlf/turkey/table_b36.rtf', 'table_b36.rtf', 'table_b36.lst'),
('PRMA', '0165000000', '01', '65', '00', '00', '00', '', 'TABLE', 'Table B37', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b37.sas', 'table_b37.sas', 'T2', 'tlf/turkey/table_b37.rtf', 'table_b37.rtf', 'table_b37.lst'),
('PRMA', '0166000000', '01', '66', '00', '00', '00', '', 'TABLE', 'Table B38', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b38.sas', 'table_b38.sas', 'T2', 'tlf/turkey/table_b38.rtf', 'table_b38.rtf', 'table_b38.lst'),
('PRMA', '0167000000', '01', '67', '00', '00', '00', '', 'TABLE', 'Table B39', 'List of other guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_b39.sas', 'table_b39.sas', 'T2', 'tlf/turkey/table_b39.rtf', 'table_b39.rtf', 'table_b39.lst'),
('PRMA', '0168000000', '01', '68', '00', '00', '00', '', 'TABLE', 'Table B40', 'Combination of guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_b40.sas', 'table_b40.sas', 'T2', 'tlf/turkey/table_b40.rtf', 'table_b40.rtf', 'table_b40.lst'),
('PRMA', '0169000000', '01', '69', '00', '00', '00', '', 'TABLE', 'Table B41', 'Modification of guidelines', 'pgm/tlf/tlf_tur/table_b41.sas', 'table_b41.sas', 'T2', 'tlf/turkey/table_b41.rtf', 'table_b41.rtf', 'table_b41.lst'),
('PRMA', '0170000000', '01', '70', '00', '00', '00', '', 'TABLE', 'Table B41B', 'Use of vitamin D supplementation to ameliorate statin-associated symptoms', 'pgm/tlf/tlf_tur/table_b41b.sas', 'table_b41b.sas', 'T2', 'tlf/turkey/table_b41b.rtf', 'table_b41b.rtf', 'table_b41b.lst'),
('PRMA', '0171000000', '01', '71', '00', '00', '00', '', 'TABLE', 'Table B42', 'Non-statin lipid-lowering therapies used as monotherapy by specialists/consultants', 'pgm/tlf/tlf_tur/table_b42.sas', 'table_b42.sas', 'T5', 'tlf/turkey/table_b42.rtf', 'table_b42.rtf', 'table_b42.lst'),
('PRMA', '0172000000', '01', '72', '00', '00', '00', '', 'TABLE', 'Table B43', 'Non-statin lipid-lowering therapies used as monotherapy by general/family physicians', 'pgm/tlf/tlf_tur/table_b43.sas', 'table_b43.sas', 'T5', 'tlf/turkey/table_b43.rtf', 'table_b43.rtf', 'table_b43.lst'),
('PRMA', '0173000000', '01', '73', '00', '00', '00', '', 'TABLE', 'Table B44', 'Non-statin lipid-lowering therapies used in combination with a statin by specialists/consultants', 'pgm/tlf/tlf_tur/table_b44.sas', 'table_b44.sas', 'T5', 'tlf/turkey/table_b44.rtf', 'table_b44.rtf', 'table_b44.lst'),
('PRMA', '0174000000', '01', '74', '00', '00', '00', '', 'TABLE', 'Table B44a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by specialists/consultants', 'pgm/tlf/tlf_tur/table_b44a.sas', 'table_b44a.sas', 'T5', 'tlf/turkey/table_b44a.rtf', 'table_b44a.rtf', 'table_b44a.lst'),
('PRMA', '0175000000', '01', '75', '00', '00', '00', '', 'TABLE', 'Table B45', 'Non-statin lipid-lowering therapies used in combination with a statin by general/family physicians', 'pgm/tlf/tlf_tur/table_b45.sas', 'table_b45.sas', 'T5', 'tlf/turkey/table_b45.rtf', 'table_b45.rtf', 'table_b45.lst'),
('PRMA', '0176000000', '01', '76', '00', '00', '00', '', 'TABLE', 'Table B45a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by general/family physicians', 'pgm/tlf/tlf_tur/table_b45a.sas', 'table_b45a.sas', 'T5', 'tlf/turkey/table_b45a.rtf', 'table_b45a.rtf', 'table_b45a.lst'),
('PRMA', '0177000000', '01', '77', '00', '00', '00', '', 'TABLE', 'Table B46', 'List of other lipid-lowering therapies used', 'pgm/tlf/tlf_tur/table_b46.sas', 'table_b46.sas', 'T1', 'tlf/turkey/table_b46.rtf', 'table_b46.rtf', 'table_b46.lst'),
('PRMA', '0178000000', '01', '78', '00', '00', '00', '', 'TABLE', 'Table B47', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients', 'pgm/tlf/tlf_tur/table_b47.sas', 'table_b47.sas', 'T5', 'tlf/turkey/table_b47.rtf', 'table_b47.rtf', 'table_b47.lst'),
('PRMA', '0179000000', '01', '79', '00', '00', '00', '', 'TABLE', 'Table B48', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by specialists/consultants', 'pgm/tlf/tlf_tur/table_b48.sas', 'table_b48.sas', 'T5', 'tlf/turkey/table_b48.rtf', 'table_b48.rtf', 'table_b48.lst'),
('PRMA', '0180000000', '01', '80', '00', '00', '00', '', 'TABLE', 'Table B49', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by general/family physicians', 'pgm/tlf/tlf_tur/table_b49.sas', 'table_b49.sas', 'T5', 'tlf/turkey/table_b49.rtf', 'table_b49.rtf', 'table_b49.lst'),
('PRMA', '0181000000', '01', '81', '00', '00', '00', '', 'TABLE', 'Table B50', 'Impact of reimbursement status on treatment choice', 'pgm/tlf/tlf_tur/table_b50.sas', 'table_b50.sas', 'T2', 'tlf/turkey/table_b50.rtf', 'table_b50.rtf', 'table_b50.lst'),
('PRMA', '0182000000', '01', '82', '00', '00', '00', '', 'TABLE', 'Table B51', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_tur/table_b51.sas', 'table_b51.sas', 'T5', 'tlf/turkey/table_b51.rtf', 'table_b51.rtf', 'table_b51.lst'),
('PRMA', '0183000000', '01', '83', '00', '00', '00', '', 'TABLE', 'Table B52', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_tur/table_b52.sas', 'table_b52.sas', 'T5', 'tlf/turkey/table_b52.rtf', 'table_b52.rtf', 'table_b52.lst'),
('PRMA', '0184000000', '01', '84', '00', '00', '00', '', 'TABLE', 'Table B53', 'Reason for discontinuation of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_tur/table_b53.sas', 'table_b53.sas', 'T5', 'tlf/turkey/table_b53.rtf', 'table_b53.rtf', 'table_b53.lst'),
('PRMA', '0185000000', '01', '85', '00', '00', '00', '', 'TABLE', 'Table B54', 'Reason for discontinuation of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_tur/table_b54.sas', 'table_b54.sas', 'T5', 'tlf/turkey/table_b54.rtf', 'table_b54.rtf', 'table_b54.lst'),
('PRMA', '0200000000', '02', '00', '00', '00', '00', 'Belgium', '', '', '', '', '', '', '', '', ''),
('PRMA', '0201000000', '02', '01', '00', '00', '00', '', 'TABLE', 'Table A1', 'Clinical role of clinicians who participated in the survey', 'pgm/tlf/tlf_bel/table_a1.sas', 'table_a1.sas', 'T1', 'tlf/Belgium/table_a1.rtf', 'table_a1.rtf', 'table_a1.lst'),
('PRMA', '0202000000', '02', '02', '00', '00', '00', '', 'TABLE', 'Table A2', 'Guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_a2.sas', 'table_a2.sas', 'T2', 'tlf/Belgium/table_a2.rtf', 'table_a2.rtf', 'table_a2.lst'),
('PRMA', '0203000000', '02', '03', '00', '00', '00', '', 'TABLE', 'Table A3', 'Care of patients newly receiving statins', 'pgm/tlf/tlf_bel/table_a3.sas', 'table_a3.sas', 'T3', 'tlf/Belgium/table_a3.rtf', 'table_a3.rtf', 'table_a3.lst'),
('PRMA', '0204000000', '02', '04', '00', '00', '00', '', 'TABLE', 'Table A4', 'Care of patients newly receiving statins: specialists/consultants', 'pgm/tlf/tlf_bel/table_a4.sas', 'table_a4.sas', 'T3', 'tlf/Belgium/table_a4.rtf', 'table_a4.rtf', 'table_a4.lst'),
('PRMA', '0205000000', '02', '05', '00', '00', '00', '', 'TABLE', 'Table A5', 'Care of patients newly receiving statins: general/family physicians', 'pgm/tlf/tlf_bel/table_a5.sas', 'table_a5.sas', 'T3', 'tlf/Belgium/table_a5.rtf', 'table_a5.rtf', 'table_a5.lst'),
('PRMA', '0206000000', '02', '06', '00', '00', '00', '', 'TABLE', 'Table A6', 'Number of clinicians who perform transaminase (ALT/AST) testing for patients newly prescribed statins', 'pgm/tlf/tlf_bel/table_a6.sas', 'table_a6.sas', '', 'tlf/Belgium/table_a6.rtf', 'table_a6.rtf', 'table_a6.lst'),
('PRMA', '0207000000', '02', '07', '00', '00', '00', '', 'TABLE', 'Table A7', 'Reasons for transaminase testing', 'pgm/tlf/tlf_bel/table_a7.sas', 'table_a7.sas', 'T5', 'tlf/Belgium/table_a7.rtf', 'table_a7.rtf', 'table_a7.lst'),
('PRMA', '0208000000', '02', '08', '00', '00', '00', '', 'TABLE', 'Table A8', 'Number of clinicians who perform creatine kinase testing for patients newly prescribed statins', 'pgm/tlf/tlf_bel/table_a8.sas', 'table_a8.sas', 'T5', 'tlf/Belgium/table_a8.rtf', 'table_a8.rtf', 'table_a8.lst'),
('PRMA', '0209000000', '02', '09', '00', '00', '00', '', 'TABLE', 'Table A9', 'Reasons for creatine kinase testing', 'pgm/tlf/tlf_bel/table_a9.sas', 'table_a9.sas', 'T2', 'tlf/Belgium/table_a9.rtf', 'table_a9.rtf', 'table_a9.lst'),
('PRMA', '0210000000', '02', '10', '00', '00', '00', '', 'TABLE', 'Table A10', 'Association of specific symptoms with use of statins', 'pgm/tlf/tlf_bel/table_a10.sas', 'table_a10.sas', 'T2', 'tlf/Belgium/table_a10.rtf', 'table_a10.rtf', 'table_a10.lst'),
('PRMA', '0211000000', '02', '11', '00', '00', '00', '', 'TABLE', 'Table A11', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_bel/table_a11.sas', 'table_a11.sas', 'T1', 'tlf/Belgium/table_a11.rtf', 'table_a11.rtf', 'table_a11.lst'),
('PRMA', '0212000000', '02', '12', '00', '00', '00', '', 'TABLE', 'Table A12', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_a12.sas', 'table_a12.sas', 'T3', 'tlf/Belgium/table_a12.rtf', 'table_a12.rtf', 'table_a12.lst'),
('PRMA', '0213000000', '02', '13', '00', '00', '00', '', 'TABLE', 'Table A13', 'Signs or symptoms that might be associated with intolerance to statin therapy specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_a13.sas', 'table_a13.sas', 'T2', 'tlf/Belgium/table_a13.rtf', 'table_a13.rtf', 'table_a13.lst'),
('PRMA', '0214000000', '02', '14', '00', '00', '00', '', 'TABLE', 'Table A14', 'Importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_a14.sas', 'table_a14.sas', 'T3', 'tlf/Belgium/table_a14.rtf', 'table_a14.rtf', 'table_a14.lst'),
('PRMA', '0215000000', '02', '15', '00', '00', '00', '', 'TABLE', 'Table A15', 'Minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_a15.sas', 'table_a15.sas', 'T2', 'tlf/Belgium/table_a15.rtf', 'table_a15.rtf', 'table_a15.lst'),
('PRMA', '0216000000', '02', '16', '00', '00', '00', '', 'TABLE', 'Table A16', 'Number of statins tried before considering patients to be unable to tolerate statins', 'pgm/tlf/tlf_bel/table_a16.sas', 'table_a16.sas', 'T2', 'tlf/Belgium/table_a16.rtf', 'table_a16.rtf', 'table_a16.lst'),
('PRMA', '0217000000', '02', '17', '00', '00', '00', '', 'TABLE', 'Table A17', 'Identification of patients who cannot tolerate statins at the recommended therapeutic dose', 'pgm/tlf/tlf_bel/table_a17.sas', 'table_a17.sas', 'T2', 'tlf/Belgium/table_a17.rtf', 'table_a17.rtf', 'table_a17.lst'),
('PRMA', '0218000000', '02', '18', '00', '00', '00', '', 'TABLE', 'Table A18', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_bel/table_a18.sas', 'table_a18.sas', 'T4', 'tlf/Belgium/table_a18.rtf', 'table_a18.rtf', 'table_a18.lst'),
('PRMA', '0219000000', '02', '19', '00', '00', '00', '', 'TABLE', 'Table A19', 'Guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_a19.sas', 'table_a19.sas', 'T2', 'tlf/Belgium/table_a19.rtf', 'table_a19.rtf', 'table_a19.lst'),
('PRMA', '0220000000', '02', '20', '00', '00', '00', '', 'TABLE', 'Table A20', 'Clinical characteristics of patients unable to tolerate statins', 'pgm/tlf/tlf_bel/table_a20.sas', 'table_a20.sas', 'T2', 'tlf/Belgium/table_a20.rtf', 'table_a20.rtf', 'table_a20.lst'),
('PRMA', '0221000000', '02', '21', '00', '00', '00', '', 'TABLE', 'Table A21', 'Treatment strategy', 'pgm/tlf/tlf_bel/table_a21.sas', 'table_a21.sas', 'T2', 'tlf/Belgium/table_a21.rtf', 'table_a21.rtf', 'table_a21.lst'),
('PRMA', '0222000000', '02', '22', '00', '00', '00', '', 'TABLE', 'Table A22', 'Non-statin lipid-lowering therapies used as monotherapy', 'pgm/tlf/tlf_bel/table_a22.sas', 'table_a22.sas', 'T5', 'tlf/Belgium/table_a22.rtf', 'table_a22.rtf', 'table_a22.lst'),
('PRMA', '0223000000', '02', '23', '00', '00', '00', '', 'TABLE', 'Table A23', 'Non-statin lipid-lowering therapies used in combination with a statin', 'pgm/tlf/tlf_bel/table_a23.sas', 'table_a23.sas', 'T5', 'tlf/Belgium/table_a23.rtf', 'table_a23.rtf', 'table_a23.lst'),
('PRMA', '0224000000', '02', '24', '00', '00', '00', '', 'TABLE', 'Table A23a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose', 'pgm/tlf/tlf_bel/table_a23a.sas', 'table_a23a.sas', 'T5', 'tlf/Belgium/table_a23a.rtf', 'table_a23a.rtf', 'table_a23a.lst'),
('PRMA', '0225000000', '02', '25', '00', '00', '00', '', 'TABLE', 'Table A24', 'Use of lipid goals to determine treatment efficacy in clinical practice', 'pgm/tlf/tlf_bel/table_a24.sas', 'table_a24.sas', 'T2', 'tlf/Belgium/table_a24.rtf', 'table_a24.rtf', 'table_a24.lst'),
('PRMA', '0226000000', '02', '26', '00', '00', '00', '', 'TABLE', 'Table A25', 'Perceived clinical benefit of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_bel/table_a25.sas', 'table_a25.sas', 'T5', 'tlf/Belgium/table_a25.rtf', 'table_a25.rtf', 'table_a25.lst'),
('PRMA', '0227000000', '02', '27', '00', '00', '00', '', 'TABLE', 'Table A26', 'Reason for discontinuation of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_bel/table_a26.sas', 'table_a26.sas', 'T2', 'tlf/Belgium/table_a26.rtf', 'table_a26.rtf', 'table_a26.lst'),
('PRMA', '0228000000', '02', '28', '00', '00', '00', '', 'TABLE', 'Table B1', 'Characteristics of clinicians who participated in the survey', 'pgm/tlf/tlf_bel/table_b1.sas', 'table_b1.sas', 'T5', 'tlf/Belgium/table_b1.rtf', 'table_b1.rtf', 'table_b1.lst'),
('PRMA', '0229000000', '02', '29', '00', '00', '00', '', 'TABLE', 'Table B2', 'List of other guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_b2.sas', 'table_b2.sas', 'T5', 'tlf/Belgium/table_b2.rtf', 'table_b2.rtf', 'table_b2.lst'),
('PRMA', '0230000000', '02', '30', '00', '00', '00', '', 'TABLE', 'Table B3', 'Combinations of guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_b3.sas', 'table_b3.sas', '', 'tlf/Belgium/table_b3.rtf', 'table_b3.rtf', 'table_b3.lst'),
('PRMA', '0231000000', '02', '31', '00', '00', '00', '', 'TABLE', 'Table B4', 'Modification of guidelines', 'pgm/tlf/tlf_bel/table_b4.sas', 'table_b4.sas', 'T2', 'tlf/Belgium/table_b4.rtf', 'table_b4.rtf', 'table_b4.lst'),
('PRMA', '0232000000', '02', '32', '00', '00', '00', '', 'TABLE', 'Table B5', 'Timing of transaminase testing', 'pgm/tlf/tlf_bel/table_b5.sas', 'table_b5.sas', 'T5', 'tlf/Belgium/table_b5.rtf', 'table_b5.rtf', 'table_b5.lst'),
('PRMA', '0233000000', '02', '33', '00', '00', '00', '', 'TABLE', 'Table B6', 'Timing of transaminase testing â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b6.sas', 'table_b6.sas', 'T5', 'tlf/Belgium/table_b6.rtf', 'table_b6.rtf', 'table_b6.lst'),
('PRMA', '0234000000', '02', '34', '00', '00', '00', '', 'TABLE', 'Table B7', 'Timing of transaminase testing â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b7.sas', 'table_b7.sas', 'T5', 'tlf/Belgium/table_b7.rtf', 'table_b7.rtf', 'table_b7.lst'),
('PRMA', '0235000000', '02', '35', '00', '00', '00', '', 'TABLE', 'Table B8', 'Timing of creatine kinase testing', 'pgm/tlf/tlf_bel/table_b8.sas', 'table_b8.sas', 'T5', 'tlf/Belgium/table_b8.rtf', 'table_b8.rtf', 'table_b8.lst'),
('PRMA', '0236000000', '02', '36', '00', '00', '00', '', 'TABLE', 'Table B9', 'Timing of creatine kinase testing â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b9.sas', 'table_b9.sas', 'T5', 'tlf/Belgium/table_b9.rtf', 'table_b9.rtf', 'table_b9.lst'),
('PRMA', '0237000000', '02', '37', '00', '00', '00', '', 'TABLE', 'Table B10', 'Timing of creatine kinase testing â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b10.sas', 'table_b10.sas', 'T5', 'tlf/Belgium/table_b10.rtf', 'table_b10.rtf', 'table_b10.lst'),
('PRMA', '0238000000', '02', '38', '00', '00', '00', '', 'TABLE', 'Table B11', 'Association of specific symptoms to use of statins', 'pgm/tlf/tlf_bel/table_b11.sas', 'table_b11.sas', 'T2', 'tlf/Belgium/table_b11.rtf', 'table_b11.rtf', 'table_b11.lst'),
('PRMA', '0239000000', '02', '39', '00', '00', '00', '', 'TABLE', 'Table B12', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_bel/table_b12.sas', 'table_b12.sas', 'T2', 'tlf/Belgium/table_b12.rtf', 'table_b12.rtf', 'table_b12.lst'),
('PRMA', '0240000000', '02', '40', '00', '00', '00', '', 'TABLE', 'Table B13', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_b13.sas', 'table_b13.sas', 'T3', 'tlf/Belgium/table_b13.rtf', 'table_b13.rtf', 'table_b13.lst'),
('PRMA', '0241000000', '02', '41', '00', '00', '00', '', 'TABLE', 'Table B14', 'Magnitude of increase in transaminases that leads to modifying/discontinuing statin regimen', 'pgm/tlf/tlf_bel/table_b14.sas', 'table_b14.sas', 'T2', 'tlf/Belgium/table_b14.rtf', 'table_b14.rtf', 'table_b14.lst'),
('PRMA', '0242000000', '02', '42', '00', '00', '00', '', 'TABLE', 'Table B15', 'Use of questionnaires/tools to guide assessment of statin intolerance', 'pgm/tlf/tlf_bel/table_b15.sas', 'table_b15.sas', 'T2', 'tlf/Belgium/table_b15.rtf', 'table_b15.rtf', 'table_b15.lst'),
('PRMA', '0243000000', '02', '43', '00', '00', '00', '', 'TABLE', 'Table B15b', 'Magnitude of increase in serum creatine kinase that leads to consider intolerance to statins', 'pgm/tlf/tlf_bel/table_b15b.sas', 'table_b15b.sas', 'T2', 'tlf/Belgium/table_b15b.rtf', 'table_b15b.rtf', 'table_b15b.lst'),
('PRMA', '0244000000', '02', '44', '00', '00', '00', '', 'TABLE', 'Table B16', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b16.sas', 'table_b16.sas', 'T3', 'tlf/Belgium/table_b16.rtf', 'table_b16.rtf', 'table_b16.lst'),
('PRMA', '0245000000', '02', '45', '00', '00', '00', '', 'TABLE', 'Table B17', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b17.sas', 'table_b17.sas', 'T3', 'tlf/Belgium/table_b17.rtf', 'table_b17.rtf', 'table_b17.lst'),
('PRMA', '0246000000', '02', '46', '00', '00', '00', '', 'TABLE', 'Table B18', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b18.sas', 'table_b18.sas', 'T3', 'tlf/Belgium/table_b18.rtf', 'table_b18.rtf', 'table_b18.lst'),
('PRMA', '0247000000', '02', '47', '00', '00', '00', '', 'TABLE', 'Table B19', 'Distribution of importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b19.sas', 'table_b19.sas', 'T4', 'tlf/Belgium/table_b19.rtf', 'table_b19.rtf', 'table_b19.lst'),
('PRMA', '0248000000', '02', '48', '00', '00', '00', '', 'TABLE', 'Table B20', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b20.sas', 'table_b20.sas', 'T4', 'tlf/Belgium/table_b20.rtf', 'table_b20.rtf', 'table_b20.lst'),
('PRMA', '0249000000', '02', '49', '00', '00', '00', '', 'TABLE', 'Table B21', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b21.sas', 'table_b21.sas', 'T4', 'tlf/Belgium/table_b21.rtf', 'table_b21.rtf', 'table_b21.lst'),
('PRMA', '0250000000', '02', '50', '00', '00', '00', '', 'TABLE', 'Table B22', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b22.sas', 'table_b22.sas', 'T4', 'tlf/Belgium/table_b22.rtf', 'table_b22.rtf', 'table_b22.lst'),
('PRMA', '0251000000', '02', '51', '00', '00', '00', '', 'TABLE', 'Table B23', 'Comparative importance of symptoms to establish intolerance to statins', 'pgm/tlf/tlf_bel/table_b23.sas', 'table_b23.sas', 'T4', 'tlf/Belgium/table_b23.rtf', 'table_b23.rtf', 'table_b23.lst'),
('PRMA', '0252000000', '02', '52', '00', '00', '00', '', 'TABLE', 'Table B24', 'Other criteria considered as important to establish intolerance to statins', 'pgm/tlf/tlf_bel/table_b24.sas', 'table_b24.sas', 'T2', 'tlf/Belgium/table_b24.rtf', 'table_b24.rtf', 'table_b24.lst'),
('PRMA', '0253000000', '02', '53', '00', '00', '00', '', 'TABLE', 'Table B25', 'List of additionalbminimum criteria used to establish intolerance to statinsain patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b25.sas', 'table_b25.sas', 'T2', 'tlf/Belgium/table_b25.rtf', 'table_b25.rtf', 'table_b25.lst'),
('PRMA', '0254000000', '02', '54', '00', '00', '00', '', 'TABLE', 'Table B26', 'Minimum criteria used to establish intolerance to statins in patients with elevated serum creatine kinase but no muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b26.sas', 'table_b26.sas', 'T2', 'tlf/Belgium/table_b26.rtf', 'table_b26.rtf', 'table_b26.lst'),
('PRMA', '0255000000', '02', '55', '00', '00', '00', '', 'TABLE', 'Table B27', 'Minimum criteria used to establish intolerance to statins in patients with CNS effects', 'pgm/tlf/tlf_bel/table_b27.sas', 'table_b27.sas', 'T2', 'tlf/Belgium/table_b27.rtf', 'table_b27.rtf', 'table_b27.lst'),
('PRMA', '0256000000', '02', '56', '00', '00', '00', '', 'TABLE', 'Table B28', 'List of additional minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b28.sas', 'table_b28.sas', 'T2', 'tlf/Belgium/table_b28.rtf', 'table_b28.rtf', 'table_b28.lst'),
('PRMA', '0257000000', '02', '57', '00', '00', '00', '', 'TABLE', 'Table B29', 'Minimum criteria used to establish intolerance to statins in patients with other symptoms', 'pgm/tlf/tlf_bel/table_b29.sas', 'table_b29.sas', 'T2', 'tlf/Belgium/table_b29.rtf', 'table_b29.rtf', 'table_b29.lst'),
('PRMA', '0258000000', '02', '58', '00', '00', '00', '', 'TABLE', 'Table B30', 'Criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b30.sas', 'table_b30.sas', 'T2', 'tlf/Belgium/table_b30.rtf', 'table_b30.rtf', 'table_b30.lst'),
('PRMA', '0259000000', '02', '59', '00', '00', '00', '', 'TABLE', 'Table B31', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b31.sas', 'table_b31.sas', 'T2', 'tlf/Belgium/table_b31.rtf', 'table_b31.rtf', 'table_b31.lst'),
('PRMA', '0260000000', '02', '60', '00', '00', '00', '', 'TABLE', 'Table B32', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b32.sas', 'table_b32.sas', 'T2', 'tlf/Belgium/table_b32.rtf', 'table_b32.rtf', 'table_b32.lst'),
('PRMA', '0261000000', '02', '61', '00', '00', '00', '', 'TABLE', 'Table B33', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b33.sas', 'table_b33.sas', 'T2', 'tlf/Belgium/table_b33.rtf', 'table_b33.rtf', 'table_b33.lst'),
('PRMA', '0262000000', '02', '62', '00', '00', '00', '', 'TABLE', 'Table B34', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b34.sas', 'table_b34.sas', 'T4', 'tlf/Belgium/table_b34.rtf', 'table_b34.rtf', 'table_b34.lst'),
('PRMA', '0263000000', '02', '63', '00', '00', '00', '', 'TABLE', 'Table B35', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b35.sas', 'table_b35.sas', 'T4', 'tlf/Belgium/table_b35.rtf', 'table_b35.rtf', 'table_b35.lst'),
('PRMA', '0264000000', '02', '64', '00', '00', '00', '', 'TABLE', 'Table B36', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_bel/table_b36.sas', 'table_b36.sas', 'T2', 'tlf/Belgium/table_b36.rtf', 'table_b36.rtf', 'table_b36.lst'),
('PRMA', '0265000000', '02', '65', '00', '00', '00', '', 'TABLE', 'Table B37', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b37.sas', 'table_b37.sas', 'T2', 'tlf/Belgium/table_b37.rtf', 'table_b37.rtf', 'table_b37.lst'),
('PRMA', '0266000000', '02', '66', '00', '00', '00', '', 'TABLE', 'Table B38', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b38.sas', 'table_b38.sas', 'T2', 'tlf/Belgium/table_b38.rtf', 'table_b38.rtf', 'table_b38.lst'),
('PRMA', '0267000000', '02', '67', '00', '00', '00', '', 'TABLE', 'Table B39', 'List of other guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_b39.sas', 'table_b39.sas', 'T2', 'tlf/Belgium/table_b39.rtf', 'table_b39.rtf', 'table_b39.lst'),
('PRMA', '0268000000', '02', '68', '00', '00', '00', '', 'TABLE', 'Table B40', 'Combination of guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_b40.sas', 'table_b40.sas', 'T2', 'tlf/Belgium/table_b40.rtf', 'table_b40.rtf', 'table_b40.lst'),
('PRMA', '0269000000', '02', '69', '00', '00', '00', '', 'TABLE', 'Table B41', 'Modification of guidelines', 'pgm/tlf/tlf_bel/table_b41.sas', 'table_b41.sas', 'T2', 'tlf/Belgium/table_b41.rtf', 'table_b41.rtf', 'table_b41.lst'),
('PRMA', '0270000000', '02', '70', '00', '00', '00', '', 'TABLE', 'Table B41B', 'Use of vitamin D supplementation to ameliorate statin-associated symptoms', 'pgm/tlf/tlf_bel/table_b41b.sas', 'table_b41b.sas', 'T2', 'tlf/Belgium/table_b41b.rtf', 'table_b41b.rtf', 'table_b41b.lst'),
('PRMA', '0271000000', '02', '71', '00', '00', '00', '', 'TABLE', 'Table B42', 'Non-statin lipid-lowering therapies used as monotherapy by specialists/consultants', 'pgm/tlf/tlf_bel/table_b42.sas', 'table_b42.sas', 'T5', 'tlf/Belgium/table_b42.rtf', 'table_b42.rtf', 'table_b42.lst'),
('PRMA', '0272000000', '02', '72', '00', '00', '00', '', 'TABLE', 'Table B43', 'Non-statin lipid-lowering therapies used as monotherapy by general/family physicians', 'pgm/tlf/tlf_bel/table_b43.sas', 'table_b43.sas', 'T5', 'tlf/Belgium/table_b43.rtf', 'table_b43.rtf', 'table_b43.lst'),
('PRMA', '0273000000', '02', '73', '00', '00', '00', '', 'TABLE', 'Table B44', 'Non-statin lipid-lowering therapies used in combination with a statin by specialists/consultants', 'pgm/tlf/tlf_bel/table_b44.sas', 'table_b44.sas', 'T5', 'tlf/Belgium/table_b44.rtf', 'table_b44.rtf', 'table_b44.lst'),
('PRMA', '0274000000', '02', '74', '00', '00', '00', '', 'TABLE', 'Table B44a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by specialists/consultants', 'pgm/tlf/tlf_bel/table_b44a.sas', 'table_b44a.sas', 'T5', 'tlf/Belgium/table_b44a.rtf', 'table_b44a.rtf', 'table_b44a.lst'),
('PRMA', '0275000000', '02', '75', '00', '00', '00', '', 'TABLE', 'Table B45', 'Non-statin lipid-lowering therapies used in combination with a statin by general/family physicians', 'pgm/tlf/tlf_bel/table_b45.sas', 'table_b45.sas', 'T5', 'tlf/Belgium/table_b45.rtf', 'table_b45.rtf', 'table_b45.lst'),
('PRMA', '0276000000', '02', '76', '00', '00', '00', '', 'TABLE', 'Table B45a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by general/family physicians', 'pgm/tlf/tlf_bel/table_b45a.sas', 'table_b45a.sas', 'T5', 'tlf/Belgium/table_b45a.rtf', 'table_b45a.rtf', 'table_b45a.lst'),
('PRMA', '0277000000', '02', '77', '00', '00', '00', '', 'TABLE', 'Table B46', 'List of other lipid-lowering therapies used', 'pgm/tlf/tlf_bel/table_b46.sas', 'table_b46.sas', 'T1', 'tlf/Belgium/table_b46.rtf', 'table_b46.rtf', 'table_b46.lst'),
('PRMA', '0278000000', '02', '78', '00', '00', '00', '', 'TABLE', 'Table B47', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients', 'pgm/tlf/tlf_bel/table_b47.sas', 'table_b47.sas', 'T5', 'tlf/Belgium/table_b47.rtf', 'table_b47.rtf', 'table_b47.lst'),
('PRMA', '0279000000', '02', '79', '00', '00', '00', '', 'TABLE', 'Table B48', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by specialists/consultants', 'pgm/tlf/tlf_bel/table_b48.sas', 'table_b48.sas', 'T5', 'tlf/Belgium/table_b48.rtf', 'table_b48.rtf', 'table_b48.lst'),
('PRMA', '0280000000', '02', '80', '00', '00', '00', '', 'TABLE', 'Table B49', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by general/family physicians', 'pgm/tlf/tlf_bel/table_b49.sas', 'table_b49.sas', 'T5', 'tlf/Belgium/table_b49.rtf', 'table_b49.rtf', 'table_b49.lst'),
('PRMA', '0281000000', '02', '81', '00', '00', '00', '', 'TABLE', 'Table B50', 'Impact of reimbursement status on treatment choice', 'pgm/tlf/tlf_bel/table_b50.sas', 'table_b50.sas', 'T2', 'tlf/Belgium/table_b50.rtf', 'table_b50.rtf', 'table_b50.lst'),
('PRMA', '0282000000', '02', '82', '00', '00', '00', '', 'TABLE', 'Table B51', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_bel/table_b51.sas', 'table_b51.sas', 'T5', 'tlf/Belgium/table_b51.rtf', 'table_b51.rtf', 'table_b51.lst'),
('PRMA', '0283000000', '02', '83', '00', '00', '00', '', 'TABLE', 'Table B52', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_bel/table_b52.sas', 'table_b52.sas', 'T5', 'tlf/Belgium/table_b52.rtf', 'table_b52.rtf', 'table_b52.lst'),
('PRMA', '0284000000', '02', '84', '00', '00', '00', '', 'TABLE', 'Table B53', 'Reason for discontinuation of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_bel/table_b53.sas', 'table_b53.sas', 'T5', 'tlf/Belgium/table_b53.rtf', 'table_b53.rtf', 'table_b53.lst'),
('PRMA', '0285000000', '02', '85', '00', '00', '00', '', 'TABLE', 'Table B54', 'Reason for discontinuation of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_bel/table_b54.sas', 'table_b54.sas', 'T5', 'tlf/Belgium/table_b54.rtf', 'table_b54.rtf', 'table_b54.lst');

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_prma_2016_04_07_14_58_07`
--

CREATE TABLE IF NOT EXISTS `bk_toc_prma_2016_04_07_14_58_07` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_prma_2016_04_07_14_58_07`
--

INSERT INTO `bk_toc_prma_2016_04_07_14_58_07` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('PRMA', '0100000000', '01', '00', '00', '00', '00', 'TURKEY', '', '', '', '', '', '', '', '', ''),
('PRMA', '0101000000', '01', '01', '00', '00', '00', '', 'TABLE', ' A1', 'Clinical role of clinicians who participated in the survey', 'pgm/tlf/tlf_tur/table_a1.sas', 'table_a1.sas', 'T1', 'tlf/turkey/table_a1.rtf', 'table_a1.rtf', 'table_a1.lst'),
('PRMA', '0102000000', '01', '02', '00', '00', '00', '', 'TABLE', ' A2', 'Guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_a2.sas', 'table_a2.sas', 'T2', 'tlf/turkey/table_a2.rtf', 'table_a2.rtf', 'table_a2.lst'),
('PRMA', '0103000000', '01', '03', '00', '00', '00', '', 'TABLE', ' A3', 'Care of patients newly receiving statins', 'pgm/tlf/tlf_tur/table_a3.sas', 'table_a3.sas', 'T3', 'tlf/turkey/table_a3.rtf', 'table_a3.rtf', 'table_a3.lst'),
('PRMA', '0104000000', '01', '04', '00', '00', '00', '', 'TABLE', ' A4', 'Care of patients newly receiving statins: specialists/consultants', 'pgm/tlf/tlf_tur/table_a4.sas', 'table_a4.sas', 'T3', 'tlf/turkey/table_a4.rtf', 'table_a4.rtf', 'table_a4.lst'),
('PRMA', '0105000000', '01', '05', '00', '00', '00', '', 'TABLE', ' A5', 'Care of patients newly receiving statins: general/family physicians', 'pgm/tlf/tlf_tur/table_a5.sas', 'table_a5.sas', 'T3', 'tlf/turkey/table_a5.rtf', 'table_a5.rtf', 'table_a5.lst'),
('PRMA', '0106000000', '01', '06', '00', '00', '00', '', 'TABLE', ' A6', 'Number of clinicians who perform transaminase (ALT/AST) testing for patients newly prescribed statins', 'pgm/tlf/tlf_tur/table_a6.sas', 'table_a6.sas', '', 'tlf/turkey/table_a6.rtf', 'table_a6.rtf', 'table_a6.lst'),
('PRMA', '0107000000', '01', '07', '00', '00', '00', '', 'TABLE', ' A7', 'Reasons for transaminase testing', 'pgm/tlf/tlf_tur/table_a7.sas', 'table_a7.sas', 'T5', 'tlf/turkey/table_a7.rtf', 'table_a7.rtf', 'table_a7.lst'),
('PRMA', '0108000000', '01', '08', '00', '00', '00', '', 'TABLE', ' A8', 'Number of clinicians who perform creatine kinase testing for patients newly prescribed statins', 'pgm/tlf/tlf_tur/table_a8.sas', 'table_a8.sas', 'T5', 'tlf/turkey/table_a8.rtf', 'table_a8.rtf', 'table_a8.lst'),
('PRMA', '0109000000', '01', '09', '00', '00', '00', '', 'TABLE', ' A9', 'Reasons for creatine kinase testing', 'pgm/tlf/tlf_tur/table_a9.sas', 'table_a9.sas', 'T2', 'tlf/turkey/table_a9.rtf', 'table_a9.rtf', 'table_a9.lst'),
('PRMA', '0110000000', '01', '10', '00', '00', '00', '', 'TABLE', ' A10', 'Association of specific symptoms with use of statins', 'pgm/tlf/tlf_tur/table_a10.sas', 'table_a10.sas', 'T2', 'tlf/turkey/table_a10.rtf', 'table_a10.rtf', 'table_a10.lst'),
('PRMA', '0111000000', '01', '11', '00', '00', '00', '', 'TABLE', ' A11', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_tur/table_a11.sas', 'table_a11.sas', 'T1', 'tlf/turkey/table_a11.rtf', 'table_a11.rtf', 'table_a11.lst'),
('PRMA', '0112000000', '01', '12', '00', '00', '00', '', 'TABLE', ' A12', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_a12.sas', 'table_a12.sas', 'T3', 'tlf/turkey/table_a12.rtf', 'table_a12.rtf', 'table_a12.lst'),
('PRMA', '0113000000', '01', '13', '00', '00', '00', '', 'TABLE', ' A13', 'Signs or symptoms that might be associated with intolerance to statin therapy specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_a13.sas', 'table_a13.sas', 'T2', 'tlf/turkey/table_a13.rtf', 'table_a13.rtf', 'table_a13.lst'),
('PRMA', '0114000000', '01', '14', '00', '00', '00', '', 'TABLE', ' A14', 'Importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_a14.sas', 'table_a14.sas', 'T3', 'tlf/turkey/table_a14.rtf', 'table_a14.rtf', 'table_a14.lst'),
('PRMA', '0115000000', '01', '15', '00', '00', '00', '', 'TABLE', ' A15', 'Minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_a15.sas', 'table_a15.sas', 'T2', 'tlf/turkey/table_a15.rtf', 'table_a15.rtf', 'table_a15.lst'),
('PRMA', '0116000000', '01', '16', '00', '00', '00', '', 'TABLE', ' A16', 'Number of statins tried before considering patients to be unable to tolerate statins', 'pgm/tlf/tlf_tur/table_a16.sas', 'table_a16.sas', 'T2', 'tlf/turkey/table_a16.rtf', 'table_a16.rtf', 'table_a16.lst'),
('PRMA', '0117000000', '01', '17', '00', '00', '00', '', 'TABLE', ' A17', 'Identification of patients who cannot tolerate statins at the recommended therapeutic dose', 'pgm/tlf/tlf_tur/table_a17.sas', 'table_a17.sas', 'T2', 'tlf/turkey/table_a17.rtf', 'table_a17.rtf', 'table_a17.lst'),
('PRMA', '0118000000', '01', '18', '00', '00', '00', '', 'TABLE', ' A18', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_tur/table_a18.sas', 'table_a18.sas', 'T4', 'tlf/turkey/table_a18.rtf', 'table_a18.rtf', 'table_a18.lst'),
('PRMA', '0119000000', '01', '19', '00', '00', '00', '', 'TABLE', ' A19', 'Guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_a19.sas', 'table_a19.sas', 'T2', 'tlf/turkey/table_a19.rtf', 'table_a19.rtf', 'table_a19.lst'),
('PRMA', '0120000000', '01', '20', '00', '00', '00', '', 'TABLE', ' A20', 'Clinical characteristics of patients unable to tolerate statins', 'pgm/tlf/tlf_tur/table_a20.sas', 'table_a20.sas', 'T2', 'tlf/turkey/table_a20.rtf', 'table_a20.rtf', 'table_a20.lst'),
('PRMA', '0121000000', '01', '21', '00', '00', '00', '', 'TABLE', ' A21', 'Treatment strategy', 'pgm/tlf/tlf_tur/table_a21.sas', 'table_a21.sas', 'T2', 'tlf/turkey/table_a21.rtf', 'table_a21.rtf', 'table_a21.lst'),
('PRMA', '0122000000', '01', '22', '00', '00', '00', '', 'TABLE', ' A22', 'Non-statin lipid-lowering therapies used as monotherapy', 'pgm/tlf/tlf_tur/table_a22.sas', 'table_a22.sas', 'T5', 'tlf/turkey/table_a22.rtf', 'table_a22.rtf', 'table_a22.lst'),
('PRMA', '0123000000', '01', '23', '00', '00', '00', '', 'TABLE', ' A23', 'Non-statin lipid-lowering therapies used in combination with a statin', 'pgm/tlf/tlf_tur/table_a23.sas', 'table_a23.sas', 'T5', 'tlf/turkey/table_a23.rtf', 'table_a23.rtf', 'table_a23.lst'),
('PRMA', '0124000000', '01', '24', '00', '00', '00', '', 'TABLE', ' A23a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose', 'pgm/tlf/tlf_tur/table_a23a.sas', 'table_a23a.sas', 'T5', 'tlf/turkey/table_a23a.rtf', 'table_a23a.rtf', 'table_a23a.lst'),
('PRMA', '0125000000', '01', '25', '00', '00', '00', '', 'TABLE', ' A24', 'Use of lipid goals to determine treatment efficacy in clinical practice', 'pgm/tlf/tlf_tur/table_a24.sas', 'table_a24.sas', 'T2', 'tlf/turkey/table_a24.rtf', 'table_a24.rtf', 'table_a24.lst'),
('PRMA', '0126000000', '01', '26', '00', '00', '00', '', 'TABLE', ' A25', 'Perceived clinical benefit of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_tur/table_a25.sas', 'table_a25.sas', 'T5', 'tlf/turkey/table_a25.rtf', 'table_a25.rtf', 'table_a25.lst'),
('PRMA', '0127000000', '01', '27', '00', '00', '00', '', 'TABLE', ' A26', 'Reason for discontinuation of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_tur/table_a26.sas', 'table_a26.sas', 'T2', 'tlf/turkey/table_a26.rtf', 'table_a26.rtf', 'table_a26.lst'),
('PRMA', '0128000000', '01', '28', '00', '00', '00', '', 'TABLE', ' B1', 'Characteristics of clinicians who participated in the survey', 'pgm/tlf/tlf_tur/table_b1.sas', 'table_b1.sas', 'T5', 'tlf/turkey/table_b1.rtf', 'table_b1.rtf', 'table_b1.lst'),
('PRMA', '0129000000', '01', '29', '00', '00', '00', '', 'TABLE', ' B2', 'List of other guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_b2.sas', 'table_b2.sas', 'T5', 'tlf/turkey/table_b2.rtf', 'table_b2.rtf', 'table_b2.lst'),
('PRMA', '0130000000', '01', '30', '00', '00', '00', '', 'TABLE', ' B3', 'Combinations of guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_b3.sas', 'table_b3.sas', '', 'tlf/turkey/table_b3.rtf', 'table_b3.rtf', 'table_b3.lst'),
('PRMA', '0131000000', '01', '31', '00', '00', '00', '', 'TABLE', ' B4', 'Modification of guidelines', 'pgm/tlf/tlf_tur/table_b4.sas', 'table_b4.sas', 'T2', 'tlf/turkey/table_b4.rtf', 'table_b4.rtf', 'table_b4.lst'),
('PRMA', '0132000000', '01', '32', '00', '00', '00', '', 'TABLE', ' B5', 'Timing of transaminase testing', 'pgm/tlf/tlf_tur/table_b5.sas', 'table_b5.sas', 'T5', 'tlf/turkey/table_b5.rtf', 'table_b5.rtf', 'table_b5.lst'),
('PRMA', '0133000000', '01', '33', '00', '00', '00', '', 'TABLE', ' B6', 'Timing of transaminase testing â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b6.sas', 'table_b6.sas', 'T5', 'tlf/turkey/table_b6.rtf', 'table_b6.rtf', 'table_b6.lst'),
('PRMA', '0134000000', '01', '34', '00', '00', '00', '', 'TABLE', ' B7', 'Timing of transaminase testing â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b7.sas', 'table_b7.sas', 'T5', 'tlf/turkey/table_b7.rtf', 'table_b7.rtf', 'table_b7.lst'),
('PRMA', '0135000000', '01', '35', '00', '00', '00', '', 'TABLE', ' B8', 'Timing of creatine kinase testing', 'pgm/tlf/tlf_tur/table_b8.sas', 'table_b8.sas', 'T5', 'tlf/turkey/table_b8.rtf', 'table_b8.rtf', 'table_b8.lst'),
('PRMA', '0136000000', '01', '36', '00', '00', '00', '', 'TABLE', ' B9', 'Timing of creatine kinase testing â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b9.sas', 'table_b9.sas', 'T5', 'tlf/turkey/table_b9.rtf', 'table_b9.rtf', 'table_b9.lst'),
('PRMA', '0137000000', '01', '37', '00', '00', '00', '', 'TABLE', ' B10', 'Timing of creatine kinase testing â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b10.sas', 'table_b10.sas', 'T5', 'tlf/turkey/table_b10.rtf', 'table_b10.rtf', 'table_b10.lst'),
('PRMA', '0138000000', '01', '38', '00', '00', '00', '', 'TABLE', ' B11', 'Association of specific symptoms to use of statins', 'pgm/tlf/tlf_tur/table_b11.sas', 'table_b11.sas', 'T2', 'tlf/turkey/table_b11.rtf', 'table_b11.rtf', 'table_b11.lst'),
('PRMA', '0139000000', '01', '39', '00', '00', '00', '', 'TABLE', ' B12', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_tur/table_b12.sas', 'table_b12.sas', 'T2', 'tlf/turkey/table_b12.rtf', 'table_b12.rtf', 'table_b12.lst'),
('PRMA', '0140000000', '01', '40', '00', '00', '00', '', 'TABLE', ' B13', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_b13.sas', 'table_b13.sas', 'T3', 'tlf/turkey/table_b13.rtf', 'table_b13.rtf', 'table_b13.lst'),
('PRMA', '0141000000', '01', '41', '00', '00', '00', '', 'TABLE', ' B14', 'Magnitude of increase in transaminases that leads to modifying/discontinuing statin regimen', 'pgm/tlf/tlf_tur/table_b14.sas', 'table_b14.sas', 'T2', 'tlf/turkey/table_b14.rtf', 'table_b14.rtf', 'table_b14.lst'),
('PRMA', '0142000000', '01', '42', '00', '00', '00', '', 'TABLE', ' B15', 'Use of questionnaires/tools to guide assessment of statin intolerance', 'pgm/tlf/tlf_tur/table_b15.sas', 'table_b15.sas', 'T2', 'tlf/turkey/table_b15.rtf', 'table_b15.rtf', 'table_b15.lst'),
('PRMA', '0143000000', '01', '43', '00', '00', '00', '', 'TABLE', ' B15b', 'Magnitude of increase in serum creatine kinase that leads to consider intolerance to statins', 'pgm/tlf/tlf_tur/table_b15b.sas', 'table_b15b.sas', 'T2', 'tlf/turkey/table_b15b.rtf', 'table_b15b.rtf', 'table_b15b.lst'),
('PRMA', '0144000000', '01', '44', '00', '00', '00', '', 'TABLE', ' B16', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b16.sas', 'table_b16.sas', 'T3', 'tlf/turkey/table_b16.rtf', 'table_b16.rtf', 'table_b16.lst'),
('PRMA', '0145000000', '01', '45', '00', '00', '00', '', 'TABLE', ' B17', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b17.sas', 'table_b17.sas', 'T3', 'tlf/turkey/table_b17.rtf', 'table_b17.rtf', 'table_b17.lst'),
('PRMA', '0146000000', '01', '46', '00', '00', '00', '', 'TABLE', ' B18', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b18.sas', 'table_b18.sas', 'T3', 'tlf/turkey/table_b18.rtf', 'table_b18.rtf', 'table_b18.lst'),
('PRMA', '0147000000', '01', '47', '00', '00', '00', '', 'TABLE', ' B19', 'Distribution of importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b19.sas', 'table_b19.sas', 'T4', 'tlf/turkey/table_b19.rtf', 'table_b19.rtf', 'table_b19.lst'),
('PRMA', '0148000000', '01', '48', '00', '00', '00', '', 'TABLE', ' B20', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b20.sas', 'table_b20.sas', 'T4', 'tlf/turkey/table_b20.rtf', 'table_b20.rtf', 'table_b20.lst'),
('PRMA', '0149000000', '01', '49', '00', '00', '00', '', 'TABLE', ' B21', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b21.sas', 'table_b21.sas', 'T4', 'tlf/turkey/table_b21.rtf', 'table_b21.rtf', 'table_b21.lst'),
('PRMA', '0150000000', '01', '50', '00', '00', '00', '', 'TABLE', ' B22', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b22.sas', 'table_b22.sas', 'T4', 'tlf/turkey/table_b22.rtf', 'table_b22.rtf', 'table_b22.lst'),
('PRMA', '0151000000', '01', '51', '00', '00', '00', '', 'TABLE', ' B23', 'Comparative importance of symptoms to establish intolerance to statins', 'pgm/tlf/tlf_tur/table_b23.sas', 'table_b23.sas', 'T4', 'tlf/turkey/table_b23.rtf', 'table_b23.rtf', 'table_b23.lst'),
('PRMA', '0152000000', '01', '52', '00', '00', '00', '', 'TABLE', ' B24', 'Other criteria considered as important to establish intolerance to statins', 'pgm/tlf/tlf_tur/table_b24.sas', 'table_b24.sas', 'T2', 'tlf/turkey/table_b24.rtf', 'table_b24.rtf', 'table_b24.lst'),
('PRMA', '0153000000', '01', '53', '00', '00', '00', '', 'TABLE', ' B25', 'List of additionalbminimum criteria used to establish intolerance to statinsain patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b25.sas', 'table_b25.sas', 'T2', 'tlf/turkey/table_b25.rtf', 'table_b25.rtf', 'table_b25.lst'),
('PRMA', '0154000000', '01', '54', '00', '00', '00', '', 'TABLE', ' B26', 'Minimum criteria used to establish intolerance to statins in patients with elevated serum creatine kinase but no muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b26.sas', 'table_b26.sas', 'T2', 'tlf/turkey/table_b26.rtf', 'table_b26.rtf', 'table_b26.lst'),
('PRMA', '0155000000', '01', '55', '00', '00', '00', '', 'TABLE', ' B27', 'Minimum criteria used to establish intolerance to statins in patients with CNS effects', 'pgm/tlf/tlf_tur/table_b27.sas', 'table_b27.sas', 'T2', 'tlf/turkey/table_b27.rtf', 'table_b27.rtf', 'table_b27.lst'),
('PRMA', '0156000000', '01', '56', '00', '00', '00', '', 'TABLE', ' B28', 'List of additional minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b28.sas', 'table_b28.sas', 'T2', 'tlf/turkey/table_b28.rtf', 'table_b28.rtf', 'table_b28.lst'),
('PRMA', '0157000000', '01', '57', '00', '00', '00', '', 'TABLE', ' B29', 'Minimum criteria used to establish intolerance to statins in patients with other symptoms', 'pgm/tlf/tlf_tur/table_b29.sas', 'table_b29.sas', 'T2', 'tlf/turkey/table_b29.rtf', 'table_b29.rtf', 'table_b29.lst'),
('PRMA', '0158000000', '01', '58', '00', '00', '00', '', 'TABLE', ' B30', 'Criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b30.sas', 'table_b30.sas', 'T2', 'tlf/turkey/table_b30.rtf', 'table_b30.rtf', 'table_b30.lst'),
('PRMA', '0159000000', '01', '59', '00', '00', '00', '', 'TABLE', ' B31', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b31.sas', 'table_b31.sas', 'T2', 'tlf/turkey/table_b31.rtf', 'table_b31.rtf', 'table_b31.lst'),
('PRMA', '0160000000', '01', '60', '00', '00', '00', '', 'TABLE', ' B32', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b32.sas', 'table_b32.sas', 'T2', 'tlf/turkey/table_b32.rtf', 'table_b32.rtf', 'table_b32.lst'),
('PRMA', '0161000000', '01', '61', '00', '00', '00', '', 'TABLE', ' B33', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b33.sas', 'table_b33.sas', 'T2', 'tlf/turkey/table_b33.rtf', 'table_b33.rtf', 'table_b33.lst'),
('PRMA', '0162000000', '01', '62', '00', '00', '00', '', 'TABLE', ' B34', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b34.sas', 'table_b34.sas', 'T4', 'tlf/turkey/table_b34.rtf', 'table_b34.rtf', 'table_b34.lst'),
('PRMA', '0163000000', '01', '63', '00', '00', '00', '', 'TABLE', ' B35', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b35.sas', 'table_b35.sas', 'T4', 'tlf/turkey/table_b35.rtf', 'table_b35.rtf', 'table_b35.lst'),
('PRMA', '0164000000', '01', '64', '00', '00', '00', '', 'TABLE', ' B36', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_tur/table_b36.sas', 'table_b36.sas', 'T2', 'tlf/turkey/table_b36.rtf', 'table_b36.rtf', 'table_b36.lst'),
('PRMA', '0165000000', '01', '65', '00', '00', '00', '', 'TABLE', ' B37', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b37.sas', 'table_b37.sas', 'T2', 'tlf/turkey/table_b37.rtf', 'table_b37.rtf', 'table_b37.lst'),
('PRMA', '0166000000', '01', '66', '00', '00', '00', '', 'TABLE', ' B38', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b38.sas', 'table_b38.sas', 'T2', 'tlf/turkey/table_b38.rtf', 'table_b38.rtf', 'table_b38.lst'),
('PRMA', '0167000000', '01', '67', '00', '00', '00', '', 'TABLE', ' B39', 'List of other guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_b39.sas', 'table_b39.sas', 'T2', 'tlf/turkey/table_b39.rtf', 'table_b39.rtf', 'table_b39.lst'),
('PRMA', '0168000000', '01', '68', '00', '00', '00', '', 'TABLE', ' B40', 'Combination of guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_b40.sas', 'table_b40.sas', 'T2', 'tlf/turkey/table_b40.rtf', 'table_b40.rtf', 'table_b40.lst'),
('PRMA', '0169000000', '01', '69', '00', '00', '00', '', 'TABLE', ' B41', 'Modification of guidelines', 'pgm/tlf/tlf_tur/table_b41.sas', 'table_b41.sas', 'T2', 'tlf/turkey/table_b41.rtf', 'table_b41.rtf', 'table_b41.lst'),
('PRMA', '0170000000', '01', '70', '00', '00', '00', '', 'TABLE', ' B41B', 'Use of vitamin D supplementation to ameliorate statin-associated symptoms', 'pgm/tlf/tlf_tur/table_b41b.sas', 'table_b41b.sas', 'T2', 'tlf/turkey/table_b41b.rtf', 'table_b41b.rtf', 'table_b41b.lst'),
('PRMA', '0171000000', '01', '71', '00', '00', '00', '', 'TABLE', ' B42', 'Non-statin lipid-lowering therapies used as monotherapy by specialists/consultants', 'pgm/tlf/tlf_tur/table_b42.sas', 'table_b42.sas', 'T5', 'tlf/turkey/table_b42.rtf', 'table_b42.rtf', 'table_b42.lst'),
('PRMA', '0172000000', '01', '72', '00', '00', '00', '', 'TABLE', ' B43', 'Non-statin lipid-lowering therapies used as monotherapy by general/family physicians', 'pgm/tlf/tlf_tur/table_b43.sas', 'table_b43.sas', 'T5', 'tlf/turkey/table_b43.rtf', 'table_b43.rtf', 'table_b43.lst'),
('PRMA', '0173000000', '01', '73', '00', '00', '00', '', 'TABLE', ' B44', 'Non-statin lipid-lowering therapies used in combination with a statin by specialists/consultants', 'pgm/tlf/tlf_tur/table_b44.sas', 'table_b44.sas', 'T5', 'tlf/turkey/table_b44.rtf', 'table_b44.rtf', 'table_b44.lst'),
('PRMA', '0174000000', '01', '74', '00', '00', '00', '', 'TABLE', ' B44a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by specialists/consultants', 'pgm/tlf/tlf_tur/table_b44a.sas', 'table_b44a.sas', 'T5', 'tlf/turkey/table_b44a.rtf', 'table_b44a.rtf', 'table_b44a.lst'),
('PRMA', '0175000000', '01', '75', '00', '00', '00', '', 'TABLE', ' B45', 'Non-statin lipid-lowering therapies used in combination with a statin by general/family physicians', 'pgm/tlf/tlf_tur/table_b45.sas', 'table_b45.sas', 'T5', 'tlf/turkey/table_b45.rtf', 'table_b45.rtf', 'table_b45.lst'),
('PRMA', '0176000000', '01', '76', '00', '00', '00', '', 'TABLE', ' B45a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by general/family physicians', 'pgm/tlf/tlf_tur/table_b45a.sas', 'table_b45a.sas', 'T5', 'tlf/turkey/table_b45a.rtf', 'table_b45a.rtf', 'table_b45a.lst'),
('PRMA', '0177000000', '01', '77', '00', '00', '00', '', 'TABLE', ' B46', 'List of other lipid-lowering therapies used', 'pgm/tlf/tlf_tur/table_b46.sas', 'table_b46.sas', 'T1', 'tlf/turkey/table_b46.rtf', 'table_b46.rtf', 'table_b46.lst'),
('PRMA', '0178000000', '01', '78', '00', '00', '00', '', 'TABLE', ' B47', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients', 'pgm/tlf/tlf_tur/table_b47.sas', 'table_b47.sas', 'T5', 'tlf/turkey/table_b47.rtf', 'table_b47.rtf', 'table_b47.lst'),
('PRMA', '0179000000', '01', '79', '00', '00', '00', '', 'TABLE', ' B48', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by specialists/consultants', 'pgm/tlf/tlf_tur/table_b48.sas', 'table_b48.sas', 'T5', 'tlf/turkey/table_b48.rtf', 'table_b48.rtf', 'table_b48.lst'),
('PRMA', '0180000000', '01', '80', '00', '00', '00', '', 'TABLE', ' B49', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by general/family physicians', 'pgm/tlf/tlf_tur/table_b49.sas', 'table_b49.sas', 'T5', 'tlf/turkey/table_b49.rtf', 'table_b49.rtf', 'table_b49.lst'),
('PRMA', '0181000000', '01', '81', '00', '00', '00', '', 'TABLE', ' B50', 'Impact of reimbursement status on treatment choice', 'pgm/tlf/tlf_tur/table_b50.sas', 'table_b50.sas', 'T2', 'tlf/turkey/table_b50.rtf', 'table_b50.rtf', 'table_b50.lst'),
('PRMA', '0182000000', '01', '82', '00', '00', '00', '', 'TABLE', ' B51', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_tur/table_b51.sas', 'table_b51.sas', 'T5', 'tlf/turkey/table_b51.rtf', 'table_b51.rtf', 'table_b51.lst'),
('PRMA', '0183000000', '01', '83', '00', '00', '00', '', 'TABLE', ' B52', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_tur/table_b52.sas', 'table_b52.sas', 'T5', 'tlf/turkey/table_b52.rtf', 'table_b52.rtf', 'table_b52.lst'),
('PRMA', '0184000000', '01', '84', '00', '00', '00', '', 'TABLE', ' B53', 'Reason for discontinuation of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_tur/table_b53.sas', 'table_b53.sas', 'T5', 'tlf/turkey/table_b53.rtf', 'table_b53.rtf', 'table_b53.lst'),
('PRMA', '0185000000', '01', '85', '00', '00', '00', '', 'TABLE', ' B54', 'Reason for discontinuation of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_tur/table_b54.sas', 'table_b54.sas', 'T5', 'tlf/turkey/table_b54.rtf', 'table_b54.rtf', 'table_b54.lst'),
('PRMA', '0200000000', '02', '00', '00', '00', '00', 'Belgium', '', '', '', '', '', '', '', '', ''),
('PRMA', '0201000000', '02', '01', '00', '00', '00', '', 'TABLE', ' A1', 'Clinical role of clinicians who participated in the survey', 'pgm/tlf/tlf_bel/table_a1.sas', 'table_a1.sas', 'T1', 'tlf/Belgium/table_a1.rtf', 'table_a1.rtf', 'table_a1.lst'),
('PRMA', '0202000000', '02', '02', '00', '00', '00', '', 'TABLE', ' A2', 'Guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_a2.sas', 'table_a2.sas', 'T2', 'tlf/Belgium/table_a2.rtf', 'table_a2.rtf', 'table_a2.lst'),
('PRMA', '0203000000', '02', '03', '00', '00', '00', '', 'TABLE', ' A3', 'Care of patients newly receiving statins', 'pgm/tlf/tlf_bel/table_a3.sas', 'table_a3.sas', 'T3', 'tlf/Belgium/table_a3.rtf', 'table_a3.rtf', 'table_a3.lst'),
('PRMA', '0204000000', '02', '04', '00', '00', '00', '', 'TABLE', ' A4', 'Care of patients newly receiving statins: specialists/consultants', 'pgm/tlf/tlf_bel/table_a4.sas', 'table_a4.sas', 'T3', 'tlf/Belgium/table_a4.rtf', 'table_a4.rtf', 'table_a4.lst'),
('PRMA', '0205000000', '02', '05', '00', '00', '00', '', 'TABLE', ' A5', 'Care of patients newly receiving statins: general/family physicians', 'pgm/tlf/tlf_bel/table_a5.sas', 'table_a5.sas', 'T3', 'tlf/Belgium/table_a5.rtf', 'table_a5.rtf', 'table_a5.lst'),
('PRMA', '0206000000', '02', '06', '00', '00', '00', '', 'TABLE', ' A6', 'Number of clinicians who perform transaminase (ALT/AST) testing for patients newly prescribed statins', 'pgm/tlf/tlf_bel/table_a6.sas', 'table_a6.sas', '', 'tlf/Belgium/table_a6.rtf', 'table_a6.rtf', 'table_a6.lst'),
('PRMA', '0207000000', '02', '07', '00', '00', '00', '', 'TABLE', ' A7', 'Reasons for transaminase testing', 'pgm/tlf/tlf_bel/table_a7.sas', 'table_a7.sas', 'T5', 'tlf/Belgium/table_a7.rtf', 'table_a7.rtf', 'table_a7.lst'),
('PRMA', '0208000000', '02', '08', '00', '00', '00', '', 'TABLE', ' A8', 'Number of clinicians who perform creatine kinase testing for patients newly prescribed statins', 'pgm/tlf/tlf_bel/table_a8.sas', 'table_a8.sas', 'T5', 'tlf/Belgium/table_a8.rtf', 'table_a8.rtf', 'table_a8.lst'),
('PRMA', '0209000000', '02', '09', '00', '00', '00', '', 'TABLE', ' A9', 'Reasons for creatine kinase testing', 'pgm/tlf/tlf_bel/table_a9.sas', 'table_a9.sas', 'T2', 'tlf/Belgium/table_a9.rtf', 'table_a9.rtf', 'table_a9.lst'),
('PRMA', '0210000000', '02', '10', '00', '00', '00', '', 'TABLE', ' A10', 'Association of specific symptoms with use of statins', 'pgm/tlf/tlf_bel/table_a10.sas', 'table_a10.sas', 'T2', 'tlf/Belgium/table_a10.rtf', 'table_a10.rtf', 'table_a10.lst'),
('PRMA', '0211000000', '02', '11', '00', '00', '00', '', 'TABLE', ' A11', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_bel/table_a11.sas', 'table_a11.sas', 'T1', 'tlf/Belgium/table_a11.rtf', 'table_a11.rtf', 'table_a11.lst'),
('PRMA', '0212000000', '02', '12', '00', '00', '00', '', 'TABLE', ' A12', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_a12.sas', 'table_a12.sas', 'T3', 'tlf/Belgium/table_a12.rtf', 'table_a12.rtf', 'table_a12.lst'),
('PRMA', '0213000000', '02', '13', '00', '00', '00', '', 'TABLE', ' A13', 'Signs or symptoms that might be associated with intolerance to statin therapy specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_a13.sas', 'table_a13.sas', 'T2', 'tlf/Belgium/table_a13.rtf', 'table_a13.rtf', 'table_a13.lst'),
('PRMA', '0214000000', '02', '14', '00', '00', '00', '', 'TABLE', ' A14', 'Importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_a14.sas', 'table_a14.sas', 'T3', 'tlf/Belgium/table_a14.rtf', 'table_a14.rtf', 'table_a14.lst'),
('PRMA', '0215000000', '02', '15', '00', '00', '00', '', 'TABLE', ' A15', 'Minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_a15.sas', 'table_a15.sas', 'T2', 'tlf/Belgium/table_a15.rtf', 'table_a15.rtf', 'table_a15.lst'),
('PRMA', '0216000000', '02', '16', '00', '00', '00', '', 'TABLE', ' A16', 'Number of statins tried before considering patients to be unable to tolerate statins', 'pgm/tlf/tlf_bel/table_a16.sas', 'table_a16.sas', 'T2', 'tlf/Belgium/table_a16.rtf', 'table_a16.rtf', 'table_a16.lst'),
('PRMA', '0217000000', '02', '17', '00', '00', '00', '', 'TABLE', ' A17', 'Identification of patients who cannot tolerate statins at the recommended therapeutic dose', 'pgm/tlf/tlf_bel/table_a17.sas', 'table_a17.sas', 'T2', 'tlf/Belgium/table_a17.rtf', 'table_a17.rtf', 'table_a17.lst'),
('PRMA', '0218000000', '02', '18', '00', '00', '00', '', 'TABLE', ' A18', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_bel/table_a18.sas', 'table_a18.sas', 'T4', 'tlf/Belgium/table_a18.rtf', 'table_a18.rtf', 'table_a18.lst'),
('PRMA', '0219000000', '02', '19', '00', '00', '00', '', 'TABLE', ' A19', 'Guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_a19.sas', 'table_a19.sas', 'T2', 'tlf/Belgium/table_a19.rtf', 'table_a19.rtf', 'table_a19.lst'),
('PRMA', '0220000000', '02', '20', '00', '00', '00', '', 'TABLE', ' A20', 'Clinical characteristics of patients unable to tolerate statins', 'pgm/tlf/tlf_bel/table_a20.sas', 'table_a20.sas', 'T2', 'tlf/Belgium/table_a20.rtf', 'table_a20.rtf', 'table_a20.lst'),
('PRMA', '0221000000', '02', '21', '00', '00', '00', '', 'TABLE', ' A21', 'Treatment strategy', 'pgm/tlf/tlf_bel/table_a21.sas', 'table_a21.sas', 'T2', 'tlf/Belgium/table_a21.rtf', 'table_a21.rtf', 'table_a21.lst'),
('PRMA', '0222000000', '02', '22', '00', '00', '00', '', 'TABLE', ' A22', 'Non-statin lipid-lowering therapies used as monotherapy', 'pgm/tlf/tlf_bel/table_a22.sas', 'table_a22.sas', 'T5', 'tlf/Belgium/table_a22.rtf', 'table_a22.rtf', 'table_a22.lst'),
('PRMA', '0223000000', '02', '23', '00', '00', '00', '', 'TABLE', ' A23', 'Non-statin lipid-lowering therapies used in combination with a statin', 'pgm/tlf/tlf_bel/table_a23.sas', 'table_a23.sas', 'T5', 'tlf/Belgium/table_a23.rtf', 'table_a23.rtf', 'table_a23.lst'),
('PRMA', '0224000000', '02', '24', '00', '00', '00', '', 'TABLE', ' A23a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose', 'pgm/tlf/tlf_bel/table_a23a.sas', 'table_a23a.sas', 'T5', 'tlf/Belgium/table_a23a.rtf', 'table_a23a.rtf', 'table_a23a.lst'),
('PRMA', '0225000000', '02', '25', '00', '00', '00', '', 'TABLE', ' A24', 'Use of lipid goals to determine treatment efficacy in clinical practice', 'pgm/tlf/tlf_bel/table_a24.sas', 'table_a24.sas', 'T2', 'tlf/Belgium/table_a24.rtf', 'table_a24.rtf', 'table_a24.lst'),
('PRMA', '0226000000', '02', '26', '00', '00', '00', '', 'TABLE', ' A25', 'Perceived clinical benefit of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_bel/table_a25.sas', 'table_a25.sas', 'T5', 'tlf/Belgium/table_a25.rtf', 'table_a25.rtf', 'table_a25.lst'),
('PRMA', '0227000000', '02', '27', '00', '00', '00', '', 'TABLE', ' A26', 'Reason for discontinuation of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_bel/table_a26.sas', 'table_a26.sas', 'T2', 'tlf/Belgium/table_a26.rtf', 'table_a26.rtf', 'table_a26.lst'),
('PRMA', '0228000000', '02', '28', '00', '00', '00', '', 'TABLE', ' B1', 'Characteristics of clinicians who participated in the survey', 'pgm/tlf/tlf_bel/table_b1.sas', 'table_b1.sas', 'T5', 'tlf/Belgium/table_b1.rtf', 'table_b1.rtf', 'table_b1.lst'),
('PRMA', '0229000000', '02', '29', '00', '00', '00', '', 'TABLE', ' B2', 'List of other guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_b2.sas', 'table_b2.sas', 'T5', 'tlf/Belgium/table_b2.rtf', 'table_b2.rtf', 'table_b2.lst'),
('PRMA', '0230000000', '02', '30', '00', '00', '00', '', 'TABLE', ' B3', 'Combinations of guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_b3.sas', 'table_b3.sas', '', 'tlf/Belgium/table_b3.rtf', 'table_b3.rtf', 'table_b3.lst'),
('PRMA', '0231000000', '02', '31', '00', '00', '00', '', 'TABLE', ' B4', 'Modification of guidelines', 'pgm/tlf/tlf_bel/table_b4.sas', 'table_b4.sas', 'T2', 'tlf/Belgium/table_b4.rtf', 'table_b4.rtf', 'table_b4.lst'),
('PRMA', '0232000000', '02', '32', '00', '00', '00', '', 'TABLE', ' B5', 'Timing of transaminase testing', 'pgm/tlf/tlf_bel/table_b5.sas', 'table_b5.sas', 'T5', 'tlf/Belgium/table_b5.rtf', 'table_b5.rtf', 'table_b5.lst'),
('PRMA', '0233000000', '02', '33', '00', '00', '00', '', 'TABLE', ' B6', 'Timing of transaminase testing â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b6.sas', 'table_b6.sas', 'T5', 'tlf/Belgium/table_b6.rtf', 'table_b6.rtf', 'table_b6.lst'),
('PRMA', '0234000000', '02', '34', '00', '00', '00', '', 'TABLE', ' B7', 'Timing of transaminase testing â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b7.sas', 'table_b7.sas', 'T5', 'tlf/Belgium/table_b7.rtf', 'table_b7.rtf', 'table_b7.lst'),
('PRMA', '0235000000', '02', '35', '00', '00', '00', '', 'TABLE', ' B8', 'Timing of creatine kinase testing', 'pgm/tlf/tlf_bel/table_b8.sas', 'table_b8.sas', 'T5', 'tlf/Belgium/table_b8.rtf', 'table_b8.rtf', 'table_b8.lst'),
('PRMA', '0236000000', '02', '36', '00', '00', '00', '', 'TABLE', ' B9', 'Timing of creatine kinase testing â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b9.sas', 'table_b9.sas', 'T5', 'tlf/Belgium/table_b9.rtf', 'table_b9.rtf', 'table_b9.lst'),
('PRMA', '0237000000', '02', '37', '00', '00', '00', '', 'TABLE', ' B10', 'Timing of creatine kinase testing â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b10.sas', 'table_b10.sas', 'T5', 'tlf/Belgium/table_b10.rtf', 'table_b10.rtf', 'table_b10.lst'),
('PRMA', '0238000000', '02', '38', '00', '00', '00', '', 'TABLE', ' B11', 'Association of specific symptoms to use of statins', 'pgm/tlf/tlf_bel/table_b11.sas', 'table_b11.sas', 'T2', 'tlf/Belgium/table_b11.rtf', 'table_b11.rtf', 'table_b11.lst'),
('PRMA', '0239000000', '02', '39', '00', '00', '00', '', 'TABLE', ' B12', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_bel/table_b12.sas', 'table_b12.sas', 'T2', 'tlf/Belgium/table_b12.rtf', 'table_b12.rtf', 'table_b12.lst'),
('PRMA', '0240000000', '02', '40', '00', '00', '00', '', 'TABLE', ' B13', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_b13.sas', 'table_b13.sas', 'T3', 'tlf/Belgium/table_b13.rtf', 'table_b13.rtf', 'table_b13.lst'),
('PRMA', '0241000000', '02', '41', '00', '00', '00', '', 'TABLE', ' B14', 'Magnitude of increase in transaminases that leads to modifying/discontinuing statin regimen', 'pgm/tlf/tlf_bel/table_b14.sas', 'table_b14.sas', 'T2', 'tlf/Belgium/table_b14.rtf', 'table_b14.rtf', 'table_b14.lst'),
('PRMA', '0242000000', '02', '42', '00', '00', '00', '', 'TABLE', ' B15', 'Use of questionnaires/tools to guide assessment of statin intolerance', 'pgm/tlf/tlf_bel/table_b15.sas', 'table_b15.sas', 'T2', 'tlf/Belgium/table_b15.rtf', 'table_b15.rtf', 'table_b15.lst'),
('PRMA', '0243000000', '02', '43', '00', '00', '00', '', 'TABLE', ' B15b', 'Magnitude of increase in serum creatine kinase that leads to consider intolerance to statins', 'pgm/tlf/tlf_bel/table_b15b.sas', 'table_b15b.sas', 'T2', 'tlf/Belgium/table_b15b.rtf', 'table_b15b.rtf', 'table_b15b.lst'),
('PRMA', '0244000000', '02', '44', '00', '00', '00', '', 'TABLE', ' B16', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b16.sas', 'table_b16.sas', 'T3', 'tlf/Belgium/table_b16.rtf', 'table_b16.rtf', 'table_b16.lst'),
('PRMA', '0245000000', '02', '45', '00', '00', '00', '', 'TABLE', ' B17', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b17.sas', 'table_b17.sas', 'T3', 'tlf/Belgium/table_b17.rtf', 'table_b17.rtf', 'table_b17.lst'),
('PRMA', '0246000000', '02', '46', '00', '00', '00', '', 'TABLE', ' B18', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b18.sas', 'table_b18.sas', 'T3', 'tlf/Belgium/table_b18.rtf', 'table_b18.rtf', 'table_b18.lst'),
('PRMA', '0247000000', '02', '47', '00', '00', '00', '', 'TABLE', ' B19', 'Distribution of importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b19.sas', 'table_b19.sas', 'T4', 'tlf/Belgium/table_b19.rtf', 'table_b19.rtf', 'table_b19.lst'),
('PRMA', '0248000000', '02', '48', '00', '00', '00', '', 'TABLE', ' B20', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b20.sas', 'table_b20.sas', 'T4', 'tlf/Belgium/table_b20.rtf', 'table_b20.rtf', 'table_b20.lst'),
('PRMA', '0249000000', '02', '49', '00', '00', '00', '', 'TABLE', ' B21', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b21.sas', 'table_b21.sas', 'T4', 'tlf/Belgium/table_b21.rtf', 'table_b21.rtf', 'table_b21.lst'),
('PRMA', '0250000000', '02', '50', '00', '00', '00', '', 'TABLE', ' B22', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b22.sas', 'table_b22.sas', 'T4', 'tlf/Belgium/table_b22.rtf', 'table_b22.rtf', 'table_b22.lst'),
('PRMA', '0251000000', '02', '51', '00', '00', '00', '', 'TABLE', ' B23', 'Comparative importance of symptoms to establish intolerance to statins', 'pgm/tlf/tlf_bel/table_b23.sas', 'table_b23.sas', 'T4', 'tlf/Belgium/table_b23.rtf', 'table_b23.rtf', 'table_b23.lst'),
('PRMA', '0252000000', '02', '52', '00', '00', '00', '', 'TABLE', ' B24', 'Other criteria considered as important to establish intolerance to statins', 'pgm/tlf/tlf_bel/table_b24.sas', 'table_b24.sas', 'T2', 'tlf/Belgium/table_b24.rtf', 'table_b24.rtf', 'table_b24.lst'),
('PRMA', '0253000000', '02', '53', '00', '00', '00', '', 'TABLE', ' B25', 'List of additionalbminimum criteria used to establish intolerance to statinsain patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b25.sas', 'table_b25.sas', 'T2', 'tlf/Belgium/table_b25.rtf', 'table_b25.rtf', 'table_b25.lst'),
('PRMA', '0254000000', '02', '54', '00', '00', '00', '', 'TABLE', ' B26', 'Minimum criteria used to establish intolerance to statins in patients with elevated serum creatine kinase but no muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b26.sas', 'table_b26.sas', 'T2', 'tlf/Belgium/table_b26.rtf', 'table_b26.rtf', 'table_b26.lst'),
('PRMA', '0255000000', '02', '55', '00', '00', '00', '', 'TABLE', ' B27', 'Minimum criteria used to establish intolerance to statins in patients with CNS effects', 'pgm/tlf/tlf_bel/table_b27.sas', 'table_b27.sas', 'T2', 'tlf/Belgium/table_b27.rtf', 'table_b27.rtf', 'table_b27.lst'),
('PRMA', '0256000000', '02', '56', '00', '00', '00', '', 'TABLE', ' B28', 'List of additional minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b28.sas', 'table_b28.sas', 'T2', 'tlf/Belgium/table_b28.rtf', 'table_b28.rtf', 'table_b28.lst'),
('PRMA', '0257000000', '02', '57', '00', '00', '00', '', 'TABLE', ' B29', 'Minimum criteria used to establish intolerance to statins in patients with other symptoms', 'pgm/tlf/tlf_bel/table_b29.sas', 'table_b29.sas', 'T2', 'tlf/Belgium/table_b29.rtf', 'table_b29.rtf', 'table_b29.lst'),
('PRMA', '0258000000', '02', '58', '00', '00', '00', '', 'TABLE', ' B30', 'Criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b30.sas', 'table_b30.sas', 'T2', 'tlf/Belgium/table_b30.rtf', 'table_b30.rtf', 'table_b30.lst'),
('PRMA', '0259000000', '02', '59', '00', '00', '00', '', 'TABLE', ' B31', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b31.sas', 'table_b31.sas', 'T2', 'tlf/Belgium/table_b31.rtf', 'table_b31.rtf', 'table_b31.lst'),
('PRMA', '0260000000', '02', '60', '00', '00', '00', '', 'TABLE', ' B32', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b32.sas', 'table_b32.sas', 'T2', 'tlf/Belgium/table_b32.rtf', 'table_b32.rtf', 'table_b32.lst'),
('PRMA', '0261000000', '02', '61', '00', '00', '00', '', 'TABLE', ' B33', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b33.sas', 'table_b33.sas', 'T2', 'tlf/Belgium/table_b33.rtf', 'table_b33.rtf', 'table_b33.lst'),
('PRMA', '0262000000', '02', '62', '00', '00', '00', '', 'TABLE', ' B34', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b34.sas', 'table_b34.sas', 'T4', 'tlf/Belgium/table_b34.rtf', 'table_b34.rtf', 'table_b34.lst'),
('PRMA', '0263000000', '02', '63', '00', '00', '00', '', 'TABLE', ' B35', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b35.sas', 'table_b35.sas', 'T4', 'tlf/Belgium/table_b35.rtf', 'table_b35.rtf', 'table_b35.lst'),
('PRMA', '0264000000', '02', '64', '00', '00', '00', '', 'TABLE', ' B36', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_bel/table_b36.sas', 'table_b36.sas', 'T2', 'tlf/Belgium/table_b36.rtf', 'table_b36.rtf', 'table_b36.lst'),
('PRMA', '0265000000', '02', '65', '00', '00', '00', '', 'TABLE', ' B37', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b37.sas', 'table_b37.sas', 'T2', 'tlf/Belgium/table_b37.rtf', 'table_b37.rtf', 'table_b37.lst'),
('PRMA', '0266000000', '02', '66', '00', '00', '00', '', 'TABLE', ' B38', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b38.sas', 'table_b38.sas', 'T2', 'tlf/Belgium/table_b38.rtf', 'table_b38.rtf', 'table_b38.lst'),
('PRMA', '0267000000', '02', '67', '00', '00', '00', '', 'TABLE', ' B39', 'List of other guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_b39.sas', 'table_b39.sas', 'T2', 'tlf/Belgium/table_b39.rtf', 'table_b39.rtf', 'table_b39.lst'),
('PRMA', '0268000000', '02', '68', '00', '00', '00', '', 'TABLE', ' B40', 'Combination of guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_b40.sas', 'table_b40.sas', 'T2', 'tlf/Belgium/table_b40.rtf', 'table_b40.rtf', 'table_b40.lst'),
('PRMA', '0269000000', '02', '69', '00', '00', '00', '', 'TABLE', ' B41', 'Modification of guidelines', 'pgm/tlf/tlf_bel/table_b41.sas', 'table_b41.sas', 'T2', 'tlf/Belgium/table_b41.rtf', 'table_b41.rtf', 'table_b41.lst'),
('PRMA', '0270000000', '02', '70', '00', '00', '00', '', 'TABLE', ' B41B', 'Use of vitamin D supplementation to ameliorate statin-associated symptoms', 'pgm/tlf/tlf_bel/table_b41b.sas', 'table_b41b.sas', 'T2', 'tlf/Belgium/table_b41b.rtf', 'table_b41b.rtf', 'table_b41b.lst'),
('PRMA', '0271000000', '02', '71', '00', '00', '00', '', 'TABLE', ' B42', 'Non-statin lipid-lowering therapies used as monotherapy by specialists/consultants', 'pgm/tlf/tlf_bel/table_b42.sas', 'table_b42.sas', 'T5', 'tlf/Belgium/table_b42.rtf', 'table_b42.rtf', 'table_b42.lst'),
('PRMA', '0272000000', '02', '72', '00', '00', '00', '', 'TABLE', ' B43', 'Non-statin lipid-lowering therapies used as monotherapy by general/family physicians', 'pgm/tlf/tlf_bel/table_b43.sas', 'table_b43.sas', 'T5', 'tlf/Belgium/table_b43.rtf', 'table_b43.rtf', 'table_b43.lst'),
('PRMA', '0273000000', '02', '73', '00', '00', '00', '', 'TABLE', ' B44', 'Non-statin lipid-lowering therapies used in combination with a statin by specialists/consultants', 'pgm/tlf/tlf_bel/table_b44.sas', 'table_b44.sas', 'T5', 'tlf/Belgium/table_b44.rtf', 'table_b44.rtf', 'table_b44.lst'),
('PRMA', '0274000000', '02', '74', '00', '00', '00', '', 'TABLE', ' B44a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by specialists/consultants', 'pgm/tlf/tlf_bel/table_b44a.sas', 'table_b44a.sas', 'T5', 'tlf/Belgium/table_b44a.rtf', 'table_b44a.rtf', 'table_b44a.lst'),
('PRMA', '0275000000', '02', '75', '00', '00', '00', '', 'TABLE', ' B45', 'Non-statin lipid-lowering therapies used in combination with a statin by general/family physicians', 'pgm/tlf/tlf_bel/table_b45.sas', 'table_b45.sas', 'T5', 'tlf/Belgium/table_b45.rtf', 'table_b45.rtf', 'table_b45.lst'),
('PRMA', '0276000000', '02', '76', '00', '00', '00', '', 'TABLE', ' B45a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by general/family physicians', 'pgm/tlf/tlf_bel/table_b45a.sas', 'table_b45a.sas', 'T5', 'tlf/Belgium/table_b45a.rtf', 'table_b45a.rtf', 'table_b45a.lst'),
('PRMA', '0277000000', '02', '77', '00', '00', '00', '', 'TABLE', ' B46', 'List of other lipid-lowering therapies used', 'pgm/tlf/tlf_bel/table_b46.sas', 'table_b46.sas', 'T1', 'tlf/Belgium/table_b46.rtf', 'table_b46.rtf', 'table_b46.lst'),
('PRMA', '0278000000', '02', '78', '00', '00', '00', '', 'TABLE', ' B47', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients', 'pgm/tlf/tlf_bel/table_b47.sas', 'table_b47.sas', 'T5', 'tlf/Belgium/table_b47.rtf', 'table_b47.rtf', 'table_b47.lst'),
('PRMA', '0279000000', '02', '79', '00', '00', '00', '', 'TABLE', ' B48', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by specialists/consultants', 'pgm/tlf/tlf_bel/table_b48.sas', 'table_b48.sas', 'T5', 'tlf/Belgium/table_b48.rtf', 'table_b48.rtf', 'table_b48.lst'),
('PRMA', '0280000000', '02', '80', '00', '00', '00', '', 'TABLE', ' B49', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by general/family physicians', 'pgm/tlf/tlf_bel/table_b49.sas', 'table_b49.sas', 'T5', 'tlf/Belgium/table_b49.rtf', 'table_b49.rtf', 'table_b49.lst'),
('PRMA', '0281000000', '02', '81', '00', '00', '00', '', 'TABLE', ' B50', 'Impact of reimbursement status on treatment choice', 'pgm/tlf/tlf_bel/table_b50.sas', 'table_b50.sas', 'T2', 'tlf/Belgium/table_b50.rtf', 'table_b50.rtf', 'table_b50.lst'),
('PRMA', '0282000000', '02', '82', '00', '00', '00', '', 'TABLE', ' B51', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_bel/table_b51.sas', 'table_b51.sas', 'T5', 'tlf/Belgium/table_b51.rtf', 'table_b51.rtf', 'table_b51.lst'),
('PRMA', '0283000000', '02', '83', '00', '00', '00', '', 'TABLE', ' B52', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_bel/table_b52.sas', 'table_b52.sas', 'T5', 'tlf/Belgium/table_b52.rtf', 'table_b52.rtf', 'table_b52.lst'),
('PRMA', '0284000000', '02', '84', '00', '00', '00', '', 'TABLE', ' B53', 'Reason for discontinuation of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_bel/table_b53.sas', 'table_b53.sas', 'T5', 'tlf/Belgium/table_b53.rtf', 'table_b53.rtf', 'table_b53.lst'),
('PRMA', '0285000000', '02', '85', '00', '00', '00', '', 'TABLE', ' B54', 'Reason for discontinuation of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_bel/table_b54.sas', 'table_b54.sas', 'T5', 'tlf/Belgium/table_b54.rtf', 'table_b54.rtf', 'table_b54.lst');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
`client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=112 ;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_1264_0003`
--

CREATE TABLE IF NOT EXISTS `cplist_1264_0003` (
  `study` varchar(20) NOT NULL DEFAULT '',
`sortorder` int(10) NOT NULL,
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cplist_1264_0003`
--

INSERT INTO `cplist_1264_0003` (`study`, `sortorder`, `cploc`, `cpname`, `cpdate`) VALUES
('1264_0003', 1, 'pgm/', 'titles.sas', '2015-09-20 10:37:21'),
('1264_0003', 2, 'pgm/', 'studyauto.sas', '2015-09-20 10:52:43'),
('1264_0003', 3, 'pgm/', 'lst2rtf.sas', '2015-09-20 10:52:54');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_dmc3`
--

CREATE TABLE IF NOT EXISTS `cplist_dmc3` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cplist_iss_cariprazine`
--

CREATE TABLE IF NOT EXISTS `cplist_iss_cariprazine` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cplist_prma`
--

CREATE TABLE IF NOT EXISTS `cplist_prma` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cplist_r101`
--

CREATE TABLE IF NOT EXISTS `cplist_r101` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_1264_0003`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_1264_0003` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=102 ;

--
-- Dumping data for table `pgm_hist_1264_0003`
--

INSERT INTO `pgm_hist_1264_0003` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(1, '0101010000', 'disp.sas', '2015-09-10 11:59:30', 'ahmed', '', 'To Be validated', '2015_09_10_11_59_31_disp.sas'),
(2, '0101010000', 'disp.sas', '2015-09-10 12:00:08', 'ahmed', 'yesa', 'Validated', '2015_09_10_12_00_08_disp.sas'),
(3, '0101010000', 'disp.sas', '2015-09-10 12:00:44', 'ahmed', 'nope', 'In Development', '2015_09_10_12_00_44_disp.sas'),
(4, '0101010000', 'disp.sas', '2015-09-14 17:27:44', 'ahmed', '', 'To Be validated', '2015_09_14_17_27_45_disp.sas'),
(5, '0101030000', 'disp2.sas', '2015-10-12 17:15:52', 'ahmed', '', 'To Be validated', '2015_10_12_17_15_52_disp2.sas'),
(6, '0101020000', 'disp.sas', '2015-10-12 22:59:57', 'ahmed', '', 'To Be validated', '2015_10_12_22_59_57_disp.sas'),
(7, '0101020000', 'disp.sas', '2015-10-12 23:00:49', 'ahmed', 'ok', 'Validated', '2015_10_12_23_00_49_disp.sas'),
(8, '0101020000', 'disp.sas', '2015-10-12 23:01:11', 'ahmed', 'wrong', 'In Development', '2015_10_12_23_01_11_disp.sas'),
(9, '0101020000', 'disp.sas', '2015-10-12 23:02:52', 'ahmed', 'correct', 'Validated', '2015_10_12_23_02_52_disp.sas'),
(10, '0101030000', 'disp2.sas', '2015-10-13 12:43:02', 'user_mz', '', 'Validated', '2015_10_13_12_43_02_disp2.sas'),
(11, '0101010000', 'disp.sas', '2015-10-13 12:53:05', 'user_mz', '', 'To Be validated', '2015_10_13_12_53_05_disp.sas'),
(12, '0101010000', 'disp.sas', '2015-10-13 12:53:30', 'user_mz', 'ok', 'Validated', '2015_10_13_12_53_30_disp.sas'),
(13, '0101010000', 'disp.sas', '2015-10-13 13:11:22', 'user_mz', '', 'In Development', '2015_10_13_13_11_22_disp.sas'),
(14, '0101010000', 'disp.sas', '2015-12-06 15:08:57', 'ahmed', 'ss', 'Validated', '2015_12_06_15_08_57_disp.sas'),
(15, '0101010000', 'disp.sas', '2015-12-06 15:23:57', 'ahmed', 'ss', 'Validated', '2015_12_06_15_23_57_disp.sas'),
(16, '0101010000', 'disp.sas', '2016-01-03 15:36:59', 'ahmed', 'ggg', 'In Development', '2016_01_03_15_37_00_disp.sas'),
(17, '0101020000', 'disp.sas', '2016-01-05 11:44:18', 'ahmed', 'failed', 'In Development', '2016_01_05_11_44_18_disp.sas'),
(18, '0101040000', 'scrandtrt.sas', '2016-01-07 11:05:13', 'ahmed', '', 'To Be validated', '2016_01_07_11_05_13_scrandtrt.sas'),
(19, '0101040000', 'scrandtrt.sas', '2016-01-07 11:10:51', 'ahmed', '', 'To Be validated', '2016_01_07_11_10_51_scrandtrt.sas'),
(20, '0104010200', 'demo.sas', '2016-01-07 12:14:05', 'ahmed', 'First Inserted', 'In Development', '2016_01_07_12_14_05_demo.sas'),
(21, '0104010200', 'demo.sas', '2016-01-07 12:45:35', 'ahmed', 'First Inserted', 'In Development', '2016_01_07_12_45_35_demo.sas'),
(22, '0104010200', 'demo.sas', '2016-01-07 13:03:39', 'ahmed', 'Uploaded', 'In Development', '2016_01_07_13_03_39_demo.sas'),
(23, '0104010200', 'demo.sas', '2016-01-07 13:03:39', 'ahmed', 'First Uploaded', 'In Development', '2016_01_07_13_03_39_demo.sas'),
(24, '0104010200', 'demo.sas', '2016-01-07 13:06:35', 'ahmed', 'Uploaded', 'In Development', '2016_01_07_13_06_35_demo.sas'),
(25, '0104010200', 'demo.sas', '2016-01-07 13:06:35', 'ahmed', 'First Uploaded', 'In Development', '2016_01_07_13_06_35_demo.sas'),
(26, '0104010200', 'demo.sas', '2016-01-07 13:06:41', 'ahmed', 'Uploaded', 'In Development', '2016_01_07_13_06_41_demo.sas'),
(27, '0104010200', 'demo.sas', '2016-01-07 13:06:41', 'ahmed', 'First Uploaded', 'In Development', '2016_01_07_13_06_41_demo.sas'),
(28, '0104010400', 'conmed.sas', '2016-01-11 09:52:25', 'ahmed', 'First Uploaded', 'In Development', '2016_01_11_09_52_25_conmed.sas'),
(29, '0104010400', 'conmed.sas', '2016-01-11 09:52:25', 'ahmed', 'First Uploaded', 'In Development', '2016_01_11_09_52_25_conmed.sas'),
(30, '0104010400', 'conmed.sas', '2016-01-11 09:53:03', 'ahmed', 'Uploaded', 'In Development', '2016_01_11_09_53_03_conmed.sas'),
(31, '0104010400', 'conmed.sas', '2016-01-11 09:53:03', 'ahmed', 'First Uploaded', 'In Development', '2016_01_11_09_53_03_conmed.sas'),
(32, '0104010500', 'conmed.sas', '2016-01-11 10:06:31', 'ahmed', 'First Uploaded', 'In Development', '2016_01_11_10_06_31_conmed.sas'),
(33, '0104010500', 'conmed.sas', '2016-01-11 10:07:00', 'ahmed', 'Uploaded', 'In Development', '2016_01_11_10_07_00_conmed.sas'),
(34, '0104010500', 'conmed.sas', '2016-01-11 10:07:47', 'ahmed', 'Uploaded', 'In Development', '2016_01_11_10_07_47_conmed.sas'),
(35, '0104010600', 'conmed.sas', '2016-01-11 15:15:59', 'ahmed', 'First Uploaded', 'In Development', '2016_01_11_15_15_59_conmed.sas'),
(36, '0104010600', 'conmed.sas', '2016-01-11 15:16:39', 'ahmed', 'Uploaded', 'In Development', '2016_01_11_15_16_39_conmed.sas'),
(37, '0101010000', 'disp.sas', '2016-01-11 15:29:54', 'ahmed', '', 'To Be validated', '2016_01_11_15_29_54_disp.sas'),
(38, '0101010000', 'disp.sas', '2016-01-11 15:31:40', 'ahmed', 'yes', 'Validated', '2016_01_11_15_31_40_disp.sas'),
(39, '0101010000', 'disp.sas', '2016-01-11 15:48:41', 'ahmed', 'yes', 'Validated', '2016_01_11_15_48_41_disp.sas'),
(40, '0101020000', 'disp.sas', '2016-01-13 10:07:20', 'ahmed', '', 'Validated', '2016_01_13_10_07_20_disp.sas'),
(41, '0101020000', 'disp.sas', '2016-01-13 10:08:06', 'ahmed', '', 'In Development', '2016_01_13_10_08_06_disp.sas'),
(42, '0101020000', 'disp.sas', '2016-01-13 10:08:39', 'ahmed', '', 'In Development', '2016_01_13_10_08_39_disp.sas'),
(43, '0101020000', 'disp.sas', '2016-01-13 10:08:53', 'ahmed', '', 'Validated', '2016_01_13_10_08_53_disp.sas'),
(44, '0101020000', 'disp.sas', '2016-01-13 10:15:18', 'ahmed', '', 'Validated', '2016_01_13_10_15_18_disp.sas'),
(45, '0101020000', 'disp.sas', '2016-01-13 11:11:35', 'ahmed', '', 'Validated', '2016_01_13_11_11_35_disp.sas'),
(46, '0101020000', 'disp.sas', '2016-01-13 11:17:32', 'ahmed', '', 'Validated', '2016_01_13_11_17_32_disp.sas'),
(47, '0101020000', 'disp.sas', '2016-01-13 11:26:21', 'ahmed', '', 'Validated', '2016_01_13_11_26_21_disp.sas'),
(48, '0101020000', 'disp.sas', '2016-01-13 11:27:49', 'ahmed', '', 'Validated', '2016_01_13_11_27_49_disp.sas'),
(49, '0101040000', 'scrandtrt.sas', '2016-01-14 10:31:34', 'ahmed', '', 'Validated', '2016_01_14_10_31_34_scrandtrt.sas'),
(50, '0101010000', 'disp.sas', '2016-01-14 13:09:23', 'ahmed', 'fail', 'In Development', '2016_01_14_13_09_23_disp.sas'),
(51, '0103010000', 'tpopu.sas', '2016-02-04 10:26:00', 'ahmed', 'First Uploaded', 'In Development', '2016_02_04_10_26_00_tpopu.sas'),
(52, '0103010000', 'tpopu.sas', '2016-02-04 10:31:02', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_31_02_tpopu.sas'),
(53, '0103010000', 'tpopu.sas', '2016-02-04 10:31:40', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_31_40_tpopu.sas'),
(54, '0101010000', 'disp.sas', '2016-02-04 10:33:09', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_33_09_disp.sas'),
(55, '0104010700', 'cttass.sas', '2016-02-04 10:38:13', 'ahmed', 'First Uploaded', 'In Development', '2016_02_04_10_38_13_cttass.sas'),
(56, '0104010700', 'cttass.sas', '2016-02-04 10:38:38', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_38_38_cttass.sas'),
(57, '0104010700', 'cttass.sas', '2016-02-04 10:40:19', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_40_19_cttass.sas'),
(58, '0104010700', 'cttass.sas', '2016-02-04 10:40:26', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_40_26_cttass.sas'),
(59, '0104010700', 'cttass.sas', '2016-02-04 10:41:57', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_41_57_cttass.sas'),
(60, '0104010700', 'cttass.sas', '2016-02-04 10:42:04', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_42_04_cttass.sas'),
(61, '0104010700', 'cttass.sas', '2016-02-04 10:50:15', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_50_15_cttass.sas'),
(62, '0104010700', 'cttass.sas', '2016-02-04 10:51:41', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_51_41_cttass.sas'),
(63, '0104010700', 'cttass.sas', '2016-02-04 10:52:37', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_52_37_cttass.sas'),
(64, '0104010700', 'cttass.sas', '2016-02-04 10:52:44', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_52_44_cttass.sas'),
(65, '0104010700', 'cttass.sas', '2016-02-04 10:58:10', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_58_10_cttass.sas'),
(66, '0104010700', 'cttass.sas', '2016-02-04 10:58:19', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_58_19_cttass.sas'),
(67, '0104010700', 'cttass.sas', '2016-02-04 10:59:20', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_59_20_cttass.sas'),
(68, '0104010700', 'cttass.sas', '2016-02-04 10:59:27', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_10_59_27_cttass.sas'),
(69, '0104010700', 'cttass.sas', '2016-02-04 11:00:00', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_11_00_00_cttass.sas'),
(70, '0104010700', 'cttass.sas', '2016-02-04 11:00:06', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_11_00_06_cttass.sas'),
(71, '0104010700', 'cttass.sas', '2016-02-04 11:00:09', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_11_00_09_cttass.sas'),
(72, '0104010700', 'cttass.sas', '2016-02-04 11:00:13', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_11_00_13_cttass.sas'),
(73, '0104010700', 'cttass.sas', '2016-02-04 11:00:14', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_11_00_14_cttass.sas'),
(74, '0104010700', 'cttass.sas', '2016-02-04 11:00:15', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_11_00_15_cttass.sas'),
(82, '0101020000', 'disp.sas', '2016-02-04 12:53:38', 'ahmed', 'Uploaded', 'In Development', '2016_02_04_12_53_38_disp.sas'),
(83, '0101010000', 'disp.sas', '2016-02-22 12:06:41', 'ahmed', '', 'Validated', '2016_02_22_12_06_41_disp.sas'),
(84, '0101010000', 'disp.sas', '2016-02-22 12:09:53', 'ahmed', 'Uploaded', 'In Development', '2016_02_22_12_09_53_disp.sas'),
(85, '0101010000', 'disp.sas', '2016-02-22 12:10:07', 'ahmed', 'Uploaded', 'In Development', '2016_02_22_12_10_07_disp.sas'),
(86, '0101010000', 'disp.sas', '2016-02-22 12:10:15', 'ahmed', 'Uploaded', 'In Development', '2016_02_22_12_10_15_disp.sas'),
(87, '0101020000', 'disp.sas', '2016-02-22 12:47:50', 'ahmed', '', 'To Be validated', '2016_02_22_12_47_50_disp.sas'),
(88, '0101020000', 'disp.sas', '2016-02-22 12:48:01', 'ahmed', '', 'To Be validated', '2016_02_22_12_48_02_disp.sas'),
(89, '0102010000', 'ipvsummary.sas', '2016-02-23 12:30:59', 'ahmed', 'Uploaded', 'In Development', '2016_02_23_12_30_59_ipvsummary.sas'),
(90, '0105010000', 'comp.sas', '2016-02-23 12:33:53', 'ahmed', 'First Uploaded', 'In Development', '2016_02_23_12_33_53_comp.sas'),
(91, '0101030000', 'disp2.sas', '2016-02-23 12:48:57', 'ahmed', 'Uploaded', 'In Development', '2016_02_23_12_48_57_disp2.sas'),
(92, '0101030000', 'disp2.sas', '2016-02-23 12:52:54', 'user_mz', '', 'To Be validated', '2016_02_23_12_52_54_disp2.sas'),
(93, '0101020000', 'disp.sas', '2016-02-28 12:55:43', 'ahmed', 'yes', 'Validated', '2016_02_28_12_55_43_disp.sas'),
(94, '0101020000', 'disp.sas', '2016-03-29 15:59:47', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_15_59_47_disp.sas'),
(95, '0101020000', 'disp.sas', '2016-03-29 16:05:51', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_16_05_51_disp.sas'),
(96, '0101020000', 'disp.sas', '2016-03-29 16:12:49', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_16_12_49_disp.sas'),
(97, '0101020000', 'disp.sas', '2016-03-29 16:16:53', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_16_16_53_disp.sas'),
(98, '0101020000', 'disp.sas', '2016-03-29 16:17:30', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_16_17_30_disp.sas'),
(99, '0101020000', 'disp.sas', '2016-03-29 16:21:46', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_16_21_46_disp.sas'),
(100, '0101020000', 'disp.sas', '2016-03-29 16:25:23', 'ahmed', 'Uploaded', 'In Development', '2016_03_29_16_25_23_disp.sas'),
(101, '0101010000', 'disp.sas', '2016-04-05 16:45:24', 'ahmed', 'Uploaded', 'In Development', '2016_04_05_16_45_24_disp.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_dmc3`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_dmc3` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_iss_cariprazine`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_iss_cariprazine` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_prma`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_prma` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pgm_hist_prma`
--

INSERT INTO `pgm_hist_prma` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(1, '0101000000', 'table_a1.sas', '2016-04-07 12:00:31', 'ahmed', 'First Uploaded', 'In Development', '2016_04_07_12_00_31_table_a1.sas'),
(2, '0102000000', 'table_a2.sas', '2016-04-07 15:14:13', 'ahmed', 'First Uploaded', 'In Development', '2016_04_07_15_14_13_table_a2.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_r101`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_r101` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_lock_status`
--

CREATE TABLE IF NOT EXISTS `pgm_lock_status` (
  `id` varchar(200) NOT NULL,
  `study` varchar(20) NOT NULL,
  `pgmloc` varchar(200) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `macaddr` varchar(100) NOT NULL,
  `lock_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pgm_lock_status`
--

INSERT INTO `pgm_lock_status` (`id`, `study`, `pgmloc`, `pgmname`, `username`, `password`, `macaddr`, `lock_date_time`) VALUES
('02d5e4916dbba62741ca4e0b50b2f7924b226c35', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'user_mz', 'ad2165eb7e65074eeb48', 'B8-88-E3-05-B7-AE', '2015-10-13 12:57:21'),
('02fddf2ae342d449f37b579bebe4f576e1ddbdd0', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'admin_test', 'bdbf71578229d0ba3172', '70-F1-A1-49-DC-BC', '2015-08-27 14:11:34'),
('03dd5dd67694542a8b628160bb06438ed8d8be71', '1264_0003', 'http://localhost/toc/temp/1264_0003/scrandtrt.sas', 'scrandtrt.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:33:55'),
('05af5c7416b6c1f30aa792c1d748b71592dc0bb7', '1264_0003', 'temp/1264_0003/tpopu.sas', 'tpopu.sas', 'qa_admin', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-27 13:13:45'),
('08c65c1a2f5dc738575c46bf7e45e2c137cbc063', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-03 11:28:49'),
('1306402668195e53e52c41a555ff74d136774bcd', '1264_0003', 'http://localhost/toc/temp/1264_0003/scrandtrt.sas', 'scrandtrt.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-23 14:17:36'),
('1a05915469dc49a52891ea621bb75c48757d6abe', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-20 12:07:41'),
('239bc7bcc6b6f9ad9817cfc20377a463bd592d45', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 12:09:03'),
('29361f3da595cf9c7928c8011be47766185cff5a', '1264_0003', 'temp/1264_0003/inv.sas', 'inv.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-02 14:48:50'),
('2abe256247accbb48c2d8a0bacabdae088f15d38', '1264_0003', 'toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:30:13'),
('2b68bc74a66df71f2c13d5027f1e3bb905f59b60', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-03 10:47:02'),
('3106ae5c805d8b1e10b2f9079d8f70a557afb47e', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'qa_admin', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-27 13:33:44'),
('345b3dabef6e69e14446e43f17b53b61239b7a8f', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-03 08:42:15'),
('389be3150496e1fe6315cd4175bc1152d77ab84d', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '474c909a68ce9b63e7f6', 'B8-88-E3-05-B7-AE', '2015-10-12 23:29:14'),
('3ef8334b0477fa5eafa52f6991ae1f1fe736279f', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:27:52'),
('42b16647cae96f92dc1603d02d2384ed28b9e053', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 12:09:14'),
('4b6574aef6382458938117bfccaf0119328a3e87', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:30:42'),
('4cecc6e7dc0a45ed13c54e37fd77d1681078cce0', 'DMC3', 'temp/DMC3/disp.sas', 'disp.sas', 'qa_zr', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-26 12:02:08'),
('4ec156e4ad9b3b032f8bd46897e8d048db28816a', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-23 14:48:47'),
('50b086ac2d4cbc931254b1c499a99eea559ca0ab', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-02-22 12:07:37'),
('53a8c3cbe9cb34dbffde71f5fc4f6d61eaf55ad8', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:18:06'),
('5c307378d5a71b2568aa71eaa14322cf1d87ae3e', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-02-22 12:06:55'),
('5d4da0bfaa23924e61a72331f127c7f29eea3c08', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-02-17 12:23:07'),
('5efb24fa3181a9467554deaea055930a6e6163fe', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:12:11'),
('6877a4fde9ee401768a9c2cd7784687678844591', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:17:53'),
('6ee00cec83209d7b74955509a74fef795842c738', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-20 12:07:46'),
('6fae228cdd2c3e08f6c70d2093649912421ee432', '1264_0003', 'temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-20 12:07:51'),
('749ad62f1de0a1b9b9691f1ff0d3035cd371816e', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:10:06'),
('754ff402fc1af5fb3330a779aa803c7441f86af1', '1264_0003', 'temp/sas/conmed.sas', 'conmed.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-02-04 11:01:46'),
('75ba745075cdef9875bd5d4c36cbc90511714187', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:18:17'),
('75de917f74cab70d1797a924fe0014c3cbe7a23a', '1264_0003', 'temp/sas/disp2.sas', 'disp2.sas', 'ahmed', '474c909a68ce9b63e7f6', 'B8-88-E3-05-B7-AE', '2015-10-13 10:39:11'),
('7f6540387a6e2871dc4b59f1c6bdf39aec344a12', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-03 08:20:00'),
('82f67577d3a27cc25b44cd384cbc903620a4715f', '1264_0003', 'http://localhost/toc/temp/1264_0003/inv.sas', 'inv.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:18:42'),
('8432154049ee2084390ca9963b651a60ee924b47', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:49:25'),
('854b066dd96b7d5386ca0c5f7667e6b98c814a5d', 'DMC3', 'temp/DMC3/disp2.sas', 'disp2.sas', 'qa_zr', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-26 12:02:04'),
('87933996a87f0986a320d01bae0d2c673cbb5e5b', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-03-27 12:53:30'),
('8b0764061e19883f45ddd8e3ffe3377a3675aead', 'DMC3', 'temp/DMC3/disp.sas', 'disp.sas', 'qa_zr', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-26 12:01:59'),
('8b3c91be620bc66c6495b6a6ba1d1470e151e25a', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-02 14:41:12'),
('8c28c2181ed1dc6350959b3b60738226dbd536d3', '1264_0003', 'temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-20 12:08:41'),
('9097904d0efd9bfbed81101f46b030b039bf0eb8', 'DMC3', 'temp/DMC3/disp.sas', 'disp.sas', 'qa_zr', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-26 12:01:54'),
('94fac64548aba6268b5086a5febcf9b389a33285', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-02-17 12:22:28'),
('95a9476a971676d4792f866e896e17534f681d13', 'DMC3', 'temp/DMC3/ipvsummary.sas', 'ipvsummary.sas', 'editor7', '474c909a68ce9b63e7f6', 'B8-88-E3-05-B7-AE', '2015-08-24 09:02:45'),
('97398ce7881da19255280f88d62fa6907465514e', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:10:22'),
('98903d5edb197130ce5d097016913a6e3f768913', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:46:41'),
('9cab10a4b615162cb772f3e4bde83c7a3e0d5fb0', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:24:48'),
('9ce3dd00be8e4bf5deaac8c9bcb5285903cd8aff', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:30:08'),
('9f6dcf4dbe5b81690ddc3dd4de1bb787e13b54d5', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:29:22'),
('a0d96e3fd2f5109055a3b4c71c557343d50b6b85', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'qa_admin', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-27 13:28:07'),
('a0fe22f89307dabfb8b3249a17590f718aed41ae', '1264_0003', 'temp/1264_0003/scrandtrt.sas', 'scrandtrt.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-20 12:02:12'),
('a9a9005838673921e1ad106a72c659db322f9043', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:10:58'),
('ac929f60fd425d3ca9a22892bfa923f48c63815c', '1264_0003', 'temp/1264_0003/inv.sas', 'inv.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-02 14:53:02'),
('ae679b231426964ed6c57da3614e739ef7d7cb1d', '1264_0003', 'http://localhost/toc/temp/1264_0003/ipvsummary.sas', 'ipvsummary.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-23 14:15:05'),
('bc56d352a82d9118c6545edcb08859e99ddcf026', '1264_0003', 'temp/1264_0003/disp2.sas', 'disp2.sas', 'editor7', '474c909a68ce9b63e7f6', 'B8-88-E3-05-B7-AE', '2015-08-24 08:49:09'),
('be489b2dc465a0371cc64916cab6afdafb8fa782', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'qa_admin', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-27 13:20:22'),
('becf5ae8016e2e57045bc673df5afe90e3f2b4f8', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-03 10:55:16'),
('c3771ccd31894014dabf3f64dafec16da23db72b', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 12:11:56'),
('c49fb1798b99c05c86ae840503e1610f4a806d5e', '1264_0003', 'temp/sas/disp2.sas', 'disp2.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-01-11 15:50:55'),
('c978767b6c2a9ea08b3ad1e84753af64ed41fa33', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-03 08:28:21'),
('ce810d50d968b07ebe6602c21a83f7ce4488978a', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '8cf44671859ba9e5118c', 'D2-53-49-14-9F-AB', '2016-03-10 11:14:26'),
('d22cc36459bec71b9a9ebff356b5215a10b42f32', '1264_0003', 'temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '38b3eff8baf56627478e', 'B8-88-E3-05-B7-AE', '2015-08-20 12:10:24'),
('d881173373b8f4c77c30ae5d8d016ba612299901', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:04:07'),
('e86b9e25fafe1a7e8f7762523b5af11ce5740fcc', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:54:12'),
('ea9b31505ce1b432cfd15a527202c1620a18e84c', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'qa_admin', '08f5b04545cbf7eaa238', '70-F1-A1-49-DC-BC', '2015-08-27 15:45:14'),
('f1a9af4191fc5ed442617628a831b9aba05a1a2d', '1264_0003', 'temp/sas/disp.sas', 'disp.sas', 'ahmed', '474c909a68ce9b63e7f6', 'B8-88-E3-05-B7-AE', '2015-10-12 23:31:04'),
('f5d9fc20af2368e6950f6e37b08f90ed2f6edbcb', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:37:11'),
('fd4c6442f89c539f22845fd3bf19c59d0161a8bc', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `code` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Price` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`code`, `Name`, `Price`) VALUES
(100, 'laptop', 50000),
(100, 'laptop', 50000),
(101, 'Keyboard', 2500),
(101, 'Keyboard', 2500),
(102, 'Mouse', 500),
(102, 'Mouse', 500),
(103, 'Monitor', 12000),
(104, 'Pen Drive', 750),
(104, 'Pen Drive', 750);

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=243 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`, `user_type`) VALUES
(231, 1, 1265, '2015-09-09 15:46:03', 'ON', 3),
(232, 1, 1266, '2015-09-09 15:46:06', 'ON', 1),
(233, 1, 1267, '2015-09-09 15:46:09', 'ON', 1),
(235, 12372, 1265, '2015-10-12 21:01:25', 'ON', 2),
(236, 12372, 1266, '2015-10-12 21:01:29', 'ON', 2),
(237, 12372, 1267, '2015-10-12 21:01:31', 'ON', 1),
(240, 1, 1291, '2015-10-26 08:24:34', 'ON', 1),
(241, 4001, 1266, '2016-01-11 15:11:30', 'OF', 3),
(242, 1, 1294, '2016-04-06 12:20:32', 'ON', 1);

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1295 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(1260, '1260_03', 111, '2015-03-30 00:00:00', 'ON'),
(1265, 'R101', 111, '2015-09-08 21:28:50', 'ON'),
(1266, 'DMC3', 111, '2015-09-09 15:45:16', 'ON'),
(1267, '1264_0003', 111, '2015-09-09 15:45:35', 'ON'),
(1291, 'ISS_Cariprazine', 111, '2015-10-26 08:23:18', 'ON'),
(1294, 'PRMA', 111, '2016-04-05 17:56:11', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `toc_1264_0003`
--

CREATE TABLE IF NOT EXISTS `toc_1264_0003` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_1264_0003`
--

INSERT INTO `toc_1264_0003` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('1264_0003', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', '', ''),
('1264_0003', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('1264_0003', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('1264_0003', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('1264_0003', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('1264_0003', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('1264_0003', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('1264_0003', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('1264_0003', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('1264_0003', '0103000000', '01', '03', '00', '00', '00', 'Definition of analysis sets', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('1264_0003', '0104000000', '01', '04', '00', '00', '00', 'Demographic data and baseline characteristics', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', '0104010000', '01', '04', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', '0104010100', '01', '04', '01', '01', '00', ' ', 'Table', '1.4.1.1', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('1264_0003', '0104010200', '01', '04', '01', '02', '00', ' ', 'Table', '1.4.1.2', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('1264_0003', '0104010300', '01', '04', '01', '03', '00', ' ', 'Table', '1.4.1.3', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('1264_0003', '0104010400', '01', '04', '01', '04', '00', ' ', 'Table', '1.4.1.4', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('1264_0003', '0104010500', '01', '04', '01', '05', '00', ' ', 'Table', '1.4.1.5', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('1264_0003', '0104010600', '01', '04', '01', '06', '00', ' ', 'Table', '1.4.1.6', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('1264_0003', '0104010700', '01', '04', '01', '07', '00', ' ', 'Table', '1.4.1.7', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('1264_0003', '0104010800', '01', '04', '01', '08', '00', ' ', 'Table', '1.4.1.8', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('1264_0003', '0104010900', '01', '04', '01', '09', '00', ' ', 'Table', '1.4.1.9', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('1264_0003', '0104020000', '01', '04', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', '0104020100', '01', '04', '02', '01', '00', ' ', 'Table', '1.4.2.1', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('1264_0003', '0104020200', '01', '04', '02', '02', '00', ' ', 'Table', '1.4.2.2', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('1264_0003', '0104020300', '01', '04', '02', '03', '00', ' ', 'Table', '1.4.2.3', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('1264_0003', '0105000000', '01', '05', '00', '00', '00', 'Compliance data', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', '0105010000', '01', '05', '01', '00', '00', ' ', 'Table', '1.5.1', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('1264_0003', '0105020000', '01', '05', '02', '00', '00', ' ', 'Table', '1.5.1', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_dmc3`
--

CREATE TABLE IF NOT EXISTS `toc_dmc3` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_dmc3`
--

INSERT INTO `toc_dmc3` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('dmc3', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', ''),
('dmc3', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', ''),
('dmc3', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('dmc3', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('dmc3', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('dmc3', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('dmc3', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('dmc3', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('dmc3', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('dmc3', '0103000000', '01', '03', '00', '00', '00', 'Definition of analysis sets', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('dmc3', '0104000000', '01', '04', '00', '00', '00', 'Demographic data and baseline characteristics', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', '0104010000', '01', '04', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', '0104010100', '01', '04', '01', '01', '00', ' ', 'Table', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('dmc3', '0104010200', '01', '04', '01', '02', '00', ' ', 'Table', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('dmc3', '0104010300', '01', '04', '01', '03', '00', ' ', 'Table', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('dmc3', '0104010400', '01', '04', '01', '04', '00', ' ', 'Table', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('dmc3', '0104010500', '01', '04', '01', '05', '00', ' ', 'Table', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('dmc3', '0104010600', '01', '04', '01', '06', '00', ' ', 'Table', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('dmc3', '0104010700', '01', '04', '01', '07', '00', ' ', 'Table', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('dmc3', '0104010800', '01', '04', '01', '08', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('dmc3', '0104010900', '01', '04', '01', '09', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('dmc3', '0104020000', '01', '04', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', '0104020100', '01', '04', '02', '01', '00', ' ', 'Table', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('dmc3', '0104020200', '01', '04', '02', '02', '00', ' ', 'Table', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('dmc3', '0104020300', '01', '04', '02', '03', '00', ' ', 'Table', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('dmc3', '0105000000', '01', '05', '00', '00', '00', 'Compliance data', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', '0105010000', '01', '05', '01', '00', '00', ' ', 'Table', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('dmc3', '0105020000', '01', '05', '02', '00', '00', ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log'),
('dmc3', '0105030000', '01', '05', '03', '00', '00', ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T3', 'lst/comp_t2.lst', 'comp_t3.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_iss_cariprazine`
--

CREATE TABLE IF NOT EXISTS `toc_iss_cariprazine` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_iss_cariprazine`
--

INSERT INTO `toc_iss_cariprazine` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('ISS_Cariprazine', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101000000', '01', '01', '00', '00', '00', 'ISS', '', '', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101010000', '01', '01', '01', '00', '00', ' ', 'Listing', 'List of Patients with Seizure/Convulsions Treatment Emergent Adverse Even+C2:C217ts during Treatment Period in Study RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101020000', '01', '01', '02', '00', '00', ' ', 'Listing', 'List of Patients with Seizure/Convulsions Treatment Emergent Adverse Events during Treatment Period in Study RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101030000', '01', '01', '03', '00', '00', ' ', 'Listing', 'List of Patients with Seizure/Convulsions Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101040000', '01', '01', '04', '00', '00', ' ', 'Listing', 'List of Patients with Suicidality Treatment Emergent Adverse Events during Treatment Period - In Study RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101050000', '01', '01', '05', '00', '00', ' ', 'Listing', 'List of Patients with Suicidality Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101060000', '01', '01', '06', '00', '00', ' ', 'Listing', 'List of Patients with Suicidality Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101070000', '01', '01', '07', '00', '00', ' ', 'Listing', 'List of Patients with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101080000', '01', '01', '08', '00', '00', ' ', 'Listing', 'List of Patients with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101090000', '01', '01', '09', '00', '00', ' ', 'Listing', 'List of Patients with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101100000', '01', '01', '10', '00', '00', ' ', 'Listing', 'List of Patients with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101110000', '01', '01', '11', '00', '00', ' ', 'Listing', 'List of Patients with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101120000', '01', '01', '12', '00', '00', ' ', 'Listing', 'List of Patients  with DTV/PE Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101130000', '01', '01', '13', '00', '00', ' ', 'Listing', 'List of Patients with DTV/PE Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101140000', '01', '01', '14', '00', '00', ' ', 'Listing', 'List of Patients with DTV/PE Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101150000', '01', '01', '15', '00', '00', ' ', 'Listing', 'List of Patients with DTV/PE Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101160000', '01', '01', '16', '00', '00', ' ', 'Listing', 'List of Patients with DTV/PE Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101170000', '01', '01', '17', '00', '00', ' ', 'Listing', 'List of Patients with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101180000', '01', '01', '18', '00', '00', ' ', 'Listing', 'List of Patients with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101190000', '01', '01', '19', '00', '00', ' ', 'Listing', 'List of Patients with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101200000', '01', '01', '20', '00', '00', ' ', 'Listing', 'List of Patients with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101210000', '01', '01', '21', '00', '00', ' ', 'Listing', 'List of Patients with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101220000', '01', '01', '22', '00', '00', ' ', 'Listing', 'List of Patients with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101230000', '01', '01', '23', '00', '00', ' ', 'Listing', 'List of Patients with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101240000', '01', '01', '24', '00', '00', ' ', 'Listing', 'List of Patients with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101250000', '01', '01', '25', '00', '00', ' ', 'Listing', 'List of Patients with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101260000', '01', '01', '26', '00', '00', ' ', 'Listing', 'List of Patients with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101270000', '01', '01', '27', '00', '00', ' ', 'Listing', 'List of Patients with sedation Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101280000', '01', '01', '28', '00', '00', ' ', 'Listing', 'List of Patients with sedation Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101290000', '01', '01', '29', '00', '00', ' ', 'Listing', 'List of Patients with sedation Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101300000', '01', '01', '30', '00', '00', ' ', 'Listing', 'List of Patients with sedation Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101310000', '01', '01', '31', '00', '00', ' ', 'Listing', 'List of Patients with sedation Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101320000', '01', '01', '32', '00', '00', ' ', 'Listing', 'List of Patients with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101330000', '01', '01', '33', '00', '00', ' ', 'Listing', 'List of Patients with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101340000', '01', '01', '34', '00', '00', ' ', 'Listing', 'List of Patients with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101350000', '01', '01', '35', '00', '00', ' ', 'Listing', 'List of Patients with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101360000', '01', '01', '36', '00', '00', ' ', 'Listing', 'List of Patients with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101370000', '01', '01', '37', '00', '00', ' ', 'Listing', 'List of Patients with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101380000', '01', '01', '38', '00', '00', ' ', 'Listing', 'List of Patients with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101390000', '01', '01', '39', '00', '00', ' ', 'Listing', 'List of Patients with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101400000', '01', '01', '40', '00', '00', ' ', 'Listing', 'List of Patients with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101410000', '01', '01', '41', '00', '00', ' ', 'Listing', 'List of Patients with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0101420000', '01', '01', '42', '00', '00', ' ', 'Listing', 'List of Patients with Treatment Emergent Parkinsonism or Treatment-Emergent Akathisia based on SAS and BARS data in Study RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102000000', '01', '02', '00', '00', '00', 'ISS2', '', '', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', 'Patient Completion and Reason for Premature Discontinuation During the Run-In Phase - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102020000', '01', '02', '02', '00', '00', ' ', 'Table', 'Patient Completion and Reason for Premature Discontinuation During the Stabilization Phase - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102030000', '01', '02', '03', '00', '00', ' ', 'Table', 'Patient Disposition by Cariprazine Modal Daily Doses and Region - Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102040000', '01', '02', '04', '00', '00', ' ', 'Table', 'Patient Disposition by Cariprazine Modal Daily Doses and Race - Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102050000', '01', '02', '05', '00', '00', ' ', 'Table', 'Patient Disposition by Cariprazine Modal Daily Doses and Region - Group 1B (Long-term, Open-label Schizophrenia)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102060000', '01', '02', '06', '00', '00', ' ', 'Table', 'Patient Disposition by Cariprazine Modal Daily Doses and Race - Group 1B (Long-term, Open-label Schizophrenia)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102070000', '01', '02', '07', '00', '00', ' ', 'Table', 'Patient Disposition by Cariprazine Treatment Groups - Fixed-Dose Controlled Schizophrenia Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102080000', '01', '02', '08', '00', '00', ' ', 'Table', 'Patient Disposition by Cariprazine Treatment Groups - Flexible-Dose Controlled Schizophrenia Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102090000', '01', '02', '09', '00', '00', ' ', 'Table', 'Patient Disposition by Study Center - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102100000', '01', '02', '10', '00', '00', ' ', 'Table', 'Extent of Exposure by Regions - Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102110000', '01', '02', '11', '00', '00', ' ', 'Table', 'Patient Disposition - Group 1B (Long-term, Open-label Schizophrenia)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102120000', '01', '02', '12', '00', '00', ' ', 'Table', 'Extent of Exposure by Regions - Group 1B (Long-term, Open-label Schizophrenia)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102130000', '01', '02', '13', '00', '00', ' ', 'Table', 'Extent of Exposure During the Open-Label Phase - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102140000', '01', '02', '14', '00', '00', ' ', 'Table', 'Extent of Exposure During the Run-In Phase - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102150000', '01', '02', '15', '00', '00', ' ', 'Table', 'Extent of Exposure During the Stabilization Phase - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102160000', '01', '02', '16', '00', '00', ' ', 'Table', 'Extent of Exposure During the Double-Blind Treatment Period - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102170000', '01', '02', '17', '00', '00', ' ', 'Table', 'Extent of Exposure to Cariprazine During the Entire Study (Run-In Phase, Stabilization Phase and Double-Blind Phase) - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102180000', '01', '02', '18', '00', '00', ' ', 'Table', 'Extent of Exposure PK/PD Studies in Healthy Volunteers', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102190000', '01', '02', '19', '00', '00', ' ', 'Table', 'Extent of Exposure PK/PD Studies in Schizophrenia Patients', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102200000', '01', '02', '20', '00', '00', ' ', 'Table', 'Extent of Exposure Completed Studies in Depression', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102210000', '01', '02', '21', '00', '00', ' ', 'Table', 'Demographic and Other Baseline Characteristics by Cariprazine Modal Daily Dose and Region - Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102220000', '01', '02', '22', '00', '00', ' ', 'Table', 'Demographic and Other Baseline Characteristics by Cariprazine Modal Daily Dose and Region - Group 1B (Long-term, Open-label Schizophrenia)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102230000', '01', '02', '23', '00', '00', ' ', 'Table', 'Demographics and Baseline Characteristics at Visit 2 - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102240000', '01', '02', '24', '00', '00', ' ', 'Table', 'Demographics and Baseline Characteristics at Visit 2 - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102250000', '01', '02', '25', '00', '00', ' ', 'Table', 'Medical and Surgical History - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102260000', '01', '02', '26', '00', '00', ' ', 'Table', 'Psychiatric History - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102270000', '01', '02', '27', '00', '00', ' ', 'Table', 'Concomitant Medications During the Stabilization Phase - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0102280000', '01', '02', '28', '00', '00', ' ', 'Table', 'Concomitant Medications During the Double-Blind Treatment Period - RGH-MD-06 (Maintenance of effect study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103000000', '01', '03', '00', '00', '00', 'ISS3', '', '', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', 'Number of Patients (%) with Seizure/Convulsions Treatment Emergent Adverse Events during Treatment Period in Study RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103020000', '01', '03', '02', '00', '00', ' ', 'Table', 'Number of Patients (%) with Seizure/Convulsions Treatment Emergent Adverse Events during Treatment Period in Study RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103030000', '01', '03', '03', '00', '00', ' ', 'Table', 'Number (%) of Patients with Seizure/Convulsions Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103040000', '01', '03', '04', '00', '00', ' ', 'Table', 'Number of Patients (%) with Suicidality Treatment Emergent Adverse Events during Treatment Period - In Study RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103050000', '01', '03', '05', '00', '00', ' ', 'Table', 'Number of Patients (%) with Suicidality Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103060000', '01', '03', '06', '00', '00', ' ', 'Table', 'Number of Patients (%) with Suicidality Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103070000', '01', '03', '07', '00', '00', ' ', 'Table', 'Number of Patients (%) with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103080000', '01', '03', '08', '00', '00', ' ', 'Table', 'Number of Patients (%) with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103090000', '01', '03', '09', '00', '00', ' ', 'Table', 'Number of Patients (%) with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103100000', '01', '03', '10', '00', '00', ' ', 'Table', 'Number of Patients (%) with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103110000', '01', '03', '11', '00', '00', ' ', 'Table', 'Number of Patients (%) with Neuroleptic Malignant Syndrome Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103120000', '01', '03', '12', '00', '00', ' ', 'Table', 'Number of Patients (%) with DTV/PE Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103130000', '01', '03', '13', '00', '00', ' ', 'Table', 'Number of Patients (%) with DTV/PE Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103140000', '01', '03', '14', '00', '00', ' ', 'Table', 'Number of Patients (%) with DTV/PE Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103150000', '01', '03', '15', '00', '00', ' ', 'Table', 'Number of Patients (%) with DTV/PE Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103160000', '01', '03', '16', '00', '00', ' ', 'Table', 'Number of Patients (%) with DTV/PE Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103170000', '01', '03', '17', '00', '00', ' ', 'Table', 'Number of Patients (%) with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103180000', '01', '03', '18', '00', '00', ' ', 'Table', 'Number of Patients (%) with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103190000', '01', '03', '19', '00', '00', ' ', 'Table', 'Number of Patients (%) with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103200000', '01', '03', '20', '00', '00', ' ', 'Table', 'Number of Patients (%) with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103210000', '01', '03', '21', '00', '00', ' ', 'Table', 'Number of Patients (%) with cognitive impairment and delirium Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103220000', '01', '03', '22', '00', '00', ' ', 'Table', 'Number of Patients (%) with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103230000', '01', '03', '23', '00', '00', ' ', 'Table', 'Number of Patients (%) with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103240000', '01', '03', '24', '00', '00', ' ', 'Table', 'Number of Patients (%) with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103250000', '01', '03', '25', '00', '00', ' ', 'Table', 'Number of Patients (%) with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period  - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103260000', '01', '03', '26', '00', '00', ' ', 'Table', 'Number of Patients (%) with cerebrovascular disease Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103270000', '01', '03', '27', '00', '00', ' ', 'Table', 'Number of Patients (%) with Sedation Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103280000', '01', '03', '28', '00', '00', ' ', 'Table', 'Number of Patients (%) with sedation Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103290000', '01', '03', '29', '00', '00', ' ', 'Table', 'Number of Patients (%) with sedation Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103300000', '01', '03', '30', '00', '00', ' ', 'Table', 'Number of Patients (%) with sedation Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103310000', '01', '03', '31', '00', '00', ' ', 'Table', 'Number of Patients (%) with sedation Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103320000', '01', '03', '32', '00', '00', ' ', 'Table', 'Number of Patients (%) with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103330000', '01', '03', '33', '00', '00', ' ', 'Table', 'Number of Patients (%) with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103340000', '01', '03', '34', '00', '00', ' ', 'Table', 'Number of Patients (%) with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103350000', '01', '03', '35', '00', '00', ' ', 'Table', 'Number of Patients (%) with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103360000', '01', '03', '36', '00', '00', ' ', 'Table', 'Number of Patients (%) with sexual dysfunction Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103370000', '01', '03', '37', '00', '00', ' ', 'Table', 'Number of Patients (%) with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period - In Group 1A (Controlled Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103380000', '01', '03', '38', '00', '00', ' ', 'Table', 'Number of Patients (%) with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period - In Group 1B (Long-term, Open-label Schizophrenia Studies)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103390000', '01', '03', '39', '00', '00', ' ', 'Table', 'Number of Patients (%) with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period - In RGH-MD-06 (Maintenance of Effect Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103400000', '01', '03', '40', '00', '00', ' ', 'Table', 'Number of Patients (%) with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period - In RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103410000', '01', '03', '41', '00', '00', ' ', 'Table', 'Number of Patients (%) with autonomic nervous system disorders Treatment Emergent Adverse Events during Treatment Period in Supportive Studies', '', '', '', '', '', ''),
('ISS_Cariprazine', '0103420000', '01', '03', '42', '00', '00', ' ', 'Table', 'Number of Patients (%) with Treatment Emergent Parkinsonism or Treatment-Emergent Akathisia - based on SAS and BARS data in Study RGH-188-005 (Negative Symptom Study)', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `toc_prma`
--

CREATE TABLE IF NOT EXISTS `toc_prma` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_prma`
--

INSERT INTO `toc_prma` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('PRMA', '0100000000', '01', '00', '00', '00', '00', 'TURKEY', '', '', '', '', '', '', '', '', ''),
('PRMA', '0101000000', '01', '01', '00', '00', '00', '', 'TABLE', ' A1', 'Clinical role of clinicians who participated in the survey', 'pgm/tlf/tlf_tur/table_a1.sas', 'table_a1.sas', 'T1', 'tlf/turkey/table_a1.rtf', 'table_a1.rtf', 'table_a1.log'),
('PRMA', '0102000000', '01', '02', '00', '00', '00', '', 'TABLE', ' A2', 'Guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_a2.sas', 'table_a2.sas', 'T2', 'tlf/turkey/table_a2.rtf', 'table_a2.rtf', 'table_a2.log'),
('PRMA', '0103000000', '01', '03', '00', '00', '00', '', 'TABLE', ' A3', 'Care of patients newly receiving statins', 'pgm/tlf/tlf_tur/table_a3.sas', 'table_a3.sas', 'T3', 'tlf/turkey/table_a3.rtf', 'table_a3.rtf', 'table_a3.log'),
('PRMA', '0104000000', '01', '04', '00', '00', '00', '', 'TABLE', ' A4', 'Care of patients newly receiving statins: specialists/consultants', 'pgm/tlf/tlf_tur/table_a4.sas', 'table_a4.sas', 'T3', 'tlf/turkey/table_a4.rtf', 'table_a4.rtf', 'table_a4.log'),
('PRMA', '0105000000', '01', '05', '00', '00', '00', '', 'TABLE', ' A5', 'Care of patients newly receiving statins: general/family physicians', 'pgm/tlf/tlf_tur/table_a5.sas', 'table_a5.sas', 'T3', 'tlf/turkey/table_a5.rtf', 'table_a5.rtf', 'table_a5.log'),
('PRMA', '0106000000', '01', '06', '00', '00', '00', '', 'TABLE', ' A6', 'Number of clinicians who perform transaminase (ALT/AST) testing for patients newly prescribed statins', 'pgm/tlf/tlf_tur/table_a6.sas', 'table_a6.sas', '', 'tlf/turkey/table_a6.rtf', 'table_a6.rtf', 'table_a6.log'),
('PRMA', '0107000000', '01', '07', '00', '00', '00', '', 'TABLE', ' A7', 'Reasons for transaminase testing', 'pgm/tlf/tlf_tur/table_a7.sas', 'table_a7.sas', 'T5', 'tlf/turkey/table_a7.rtf', 'table_a7.rtf', 'table_a7.log'),
('PRMA', '0108000000', '01', '08', '00', '00', '00', '', 'TABLE', ' A8', 'Number of clinicians who perform creatine kinase testing for patients newly prescribed statins', 'pgm/tlf/tlf_tur/table_a8.sas', 'table_a8.sas', 'T5', 'tlf/turkey/table_a8.rtf', 'table_a8.rtf', 'table_a8.log'),
('PRMA', '0109000000', '01', '09', '00', '00', '00', '', 'TABLE', ' A9', 'Reasons for creatine kinase testing', 'pgm/tlf/tlf_tur/table_a9.sas', 'table_a9.sas', 'T2', 'tlf/turkey/table_a9.rtf', 'table_a9.rtf', 'table_a9.log'),
('PRMA', '0110000000', '01', '10', '00', '00', '00', '', 'TABLE', ' A10', 'Association of specific symptoms with use of statins', 'pgm/tlf/tlf_tur/table_a10.sas', 'table_a10.sas', 'T2', 'tlf/turkey/table_a10.rtf', 'table_a10.rtf', 'table_a10.log'),
('PRMA', '0111000000', '01', '11', '00', '00', '00', '', 'TABLE', ' A11', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_tur/table_a11.sas', 'table_a11.sas', 'T1', 'tlf/turkey/table_a11.rtf', 'table_a11.rtf', 'table_a11.log'),
('PRMA', '0112000000', '01', '12', '00', '00', '00', '', 'TABLE', ' A12', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_a12.sas', 'table_a12.sas', 'T3', 'tlf/turkey/table_a12.rtf', 'table_a12.rtf', 'table_a12.log'),
('PRMA', '0113000000', '01', '13', '00', '00', '00', '', 'TABLE', ' A13', 'Signs or symptoms that might be associated with intolerance to statin therapy specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_a13.sas', 'table_a13.sas', 'T2', 'tlf/turkey/table_a13.rtf', 'table_a13.rtf', 'table_a13.log'),
('PRMA', '0114000000', '01', '14', '00', '00', '00', '', 'TABLE', ' A14', 'Importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_a14.sas', 'table_a14.sas', 'T3', 'tlf/turkey/table_a14.rtf', 'table_a14.rtf', 'table_a14.log'),
('PRMA', '0115000000', '01', '15', '00', '00', '00', '', 'TABLE', ' A15', 'Minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_a15.sas', 'table_a15.sas', 'T2', 'tlf/turkey/table_a15.rtf', 'table_a15.rtf', 'table_a15.log'),
('PRMA', '0116000000', '01', '16', '00', '00', '00', '', 'TABLE', ' A16', 'Number of statins tried before considering patients to be unable to tolerate statins', 'pgm/tlf/tlf_tur/table_a16.sas', 'table_a16.sas', 'T2', 'tlf/turkey/table_a16.rtf', 'table_a16.rtf', 'table_a16.log'),
('PRMA', '0117000000', '01', '17', '00', '00', '00', '', 'TABLE', ' A17', 'Identification of patients who cannot tolerate statins at the recommended therapeutic dose', 'pgm/tlf/tlf_tur/table_a17.sas', 'table_a17.sas', 'T2', 'tlf/turkey/table_a17.rtf', 'table_a17.rtf', 'table_a17.log'),
('PRMA', '0118000000', '01', '18', '00', '00', '00', '', 'TABLE', ' A18', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_tur/table_a18.sas', 'table_a18.sas', 'T4', 'tlf/turkey/table_a18.rtf', 'table_a18.rtf', 'table_a18.log'),
('PRMA', '0119000000', '01', '19', '00', '00', '00', '', 'TABLE', ' A19', 'Guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_a19.sas', 'table_a19.sas', 'T2', 'tlf/turkey/table_a19.rtf', 'table_a19.rtf', 'table_a19.log'),
('PRMA', '0120000000', '01', '20', '00', '00', '00', '', 'TABLE', ' A20', 'Clinical characteristics of patients unable to tolerate statins', 'pgm/tlf/tlf_tur/table_a20.sas', 'table_a20.sas', 'T2', 'tlf/turkey/table_a20.rtf', 'table_a20.rtf', 'table_a20.log'),
('PRMA', '0121000000', '01', '21', '00', '00', '00', '', 'TABLE', ' A21', 'Treatment strategy', 'pgm/tlf/tlf_tur/table_a21.sas', 'table_a21.sas', 'T2', 'tlf/turkey/table_a21.rtf', 'table_a21.rtf', 'table_a21.log'),
('PRMA', '0122000000', '01', '22', '00', '00', '00', '', 'TABLE', ' A22', 'Non-statin lipid-lowering therapies used as monotherapy', 'pgm/tlf/tlf_tur/table_a22.sas', 'table_a22.sas', 'T5', 'tlf/turkey/table_a22.rtf', 'table_a22.rtf', 'table_a22.log'),
('PRMA', '0123000000', '01', '23', '00', '00', '00', '', 'TABLE', ' A23', 'Non-statin lipid-lowering therapies used in combination with a statin', 'pgm/tlf/tlf_tur/table_a23.sas', 'table_a23.sas', 'T5', 'tlf/turkey/table_a23.rtf', 'table_a23.rtf', 'table_a23.log'),
('PRMA', '0124000000', '01', '24', '00', '00', '00', '', 'TABLE', ' A23a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose', 'pgm/tlf/tlf_tur/table_a23a.sas', 'table_a23a.sas', 'T5', 'tlf/turkey/table_a23a.rtf', 'table_a23a.rtf', 'table_a23a.log'),
('PRMA', '0125000000', '01', '25', '00', '00', '00', '', 'TABLE', ' A24', 'Use of lipid goals to determine treatment efficacy in clinical practice', 'pgm/tlf/tlf_tur/table_a24.sas', 'table_a24.sas', 'T2', 'tlf/turkey/table_a24.rtf', 'table_a24.rtf', 'table_a24.log'),
('PRMA', '0126000000', '01', '26', '00', '00', '00', '', 'TABLE', ' A25', 'Perceived clinical benefit of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_tur/table_a25.sas', 'table_a25.sas', 'T5', 'tlf/turkey/table_a25.rtf', 'table_a25.rtf', 'table_a25.log'),
('PRMA', '0127000000', '01', '27', '00', '00', '00', '', 'TABLE', ' A26', 'Reason for discontinuation of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_tur/table_a26.sas', 'table_a26.sas', 'T2', 'tlf/turkey/table_a26.rtf', 'table_a26.rtf', 'table_a26.log'),
('PRMA', '0128000000', '01', '28', '00', '00', '00', '', 'TABLE', ' B1', 'Characteristics of clinicians who participated in the survey', 'pgm/tlf/tlf_tur/table_b1.sas', 'table_b1.sas', 'T5', 'tlf/turkey/table_b1.rtf', 'table_b1.rtf', 'table_b1.log'),
('PRMA', '0129000000', '01', '29', '00', '00', '00', '', 'TABLE', ' B2', 'List of other guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_b2.sas', 'table_b2.sas', 'T5', 'tlf/turkey/table_b2.rtf', 'table_b2.rtf', 'table_b2.log'),
('PRMA', '0130000000', '01', '30', '00', '00', '00', '', 'TABLE', ' B3', 'Combinations of guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_tur/table_b3.sas', 'table_b3.sas', '', 'tlf/turkey/table_b3.rtf', 'table_b3.rtf', 'table_b3.log'),
('PRMA', '0131000000', '01', '31', '00', '00', '00', '', 'TABLE', ' B4', 'Modification of guidelines', 'pgm/tlf/tlf_tur/table_b4.sas', 'table_b4.sas', 'T2', 'tlf/turkey/table_b4.rtf', 'table_b4.rtf', 'table_b4.log'),
('PRMA', '0132000000', '01', '32', '00', '00', '00', '', 'TABLE', ' B5', 'Timing of transaminase testing', 'pgm/tlf/tlf_tur/table_b5.sas', 'table_b5.sas', 'T5', 'tlf/turkey/table_b5.rtf', 'table_b5.rtf', 'table_b5.log'),
('PRMA', '0133000000', '01', '33', '00', '00', '00', '', 'TABLE', ' B6', 'Timing of transaminase testing â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b6.sas', 'table_b6.sas', 'T5', 'tlf/turkey/table_b6.rtf', 'table_b6.rtf', 'table_b6.log'),
('PRMA', '0134000000', '01', '34', '00', '00', '00', '', 'TABLE', ' B7', 'Timing of transaminase testing â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b7.sas', 'table_b7.sas', 'T5', 'tlf/turkey/table_b7.rtf', 'table_b7.rtf', 'table_b7.log'),
('PRMA', '0135000000', '01', '35', '00', '00', '00', '', 'TABLE', ' B8', 'Timing of creatine kinase testing', 'pgm/tlf/tlf_tur/table_b8.sas', 'table_b8.sas', 'T5', 'tlf/turkey/table_b8.rtf', 'table_b8.rtf', 'table_b8.log'),
('PRMA', '0136000000', '01', '36', '00', '00', '00', '', 'TABLE', ' B9', 'Timing of creatine kinase testing â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b9.sas', 'table_b9.sas', 'T5', 'tlf/turkey/table_b9.rtf', 'table_b9.rtf', 'table_b9.log'),
('PRMA', '0137000000', '01', '37', '00', '00', '00', '', 'TABLE', ' B10', 'Timing of creatine kinase testing â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b10.sas', 'table_b10.sas', 'T5', 'tlf/turkey/table_b10.rtf', 'table_b10.rtf', 'table_b10.log'),
('PRMA', '0138000000', '01', '38', '00', '00', '00', '', 'TABLE', ' B11', 'Association of specific symptoms to use of statins', 'pgm/tlf/tlf_tur/table_b11.sas', 'table_b11.sas', 'T2', 'tlf/turkey/table_b11.rtf', 'table_b11.rtf', 'table_b11.log'),
('PRMA', '0139000000', '01', '39', '00', '00', '00', '', 'TABLE', ' B12', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_tur/table_b12.sas', 'table_b12.sas', 'T2', 'tlf/turkey/table_b12.rtf', 'table_b12.rtf', 'table_b12.log'),
('PRMA', '0140000000', '01', '40', '00', '00', '00', '', 'TABLE', ' B13', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_tur/table_b13.sas', 'table_b13.sas', 'T3', 'tlf/turkey/table_b13.rtf', 'table_b13.rtf', 'table_b13.log'),
('PRMA', '0141000000', '01', '41', '00', '00', '00', '', 'TABLE', ' B14', 'Magnitude of increase in transaminases that leads to modifying/discontinuing statin regimen', 'pgm/tlf/tlf_tur/table_b14.sas', 'table_b14.sas', 'T2', 'tlf/turkey/table_b14.rtf', 'table_b14.rtf', 'table_b14.log'),
('PRMA', '0142000000', '01', '42', '00', '00', '00', '', 'TABLE', ' B15', 'Use of questionnaires/tools to guide assessment of statin intolerance', 'pgm/tlf/tlf_tur/table_b15.sas', 'table_b15.sas', 'T2', 'tlf/turkey/table_b15.rtf', 'table_b15.rtf', 'table_b15.log'),
('PRMA', '0143000000', '01', '43', '00', '00', '00', '', 'TABLE', ' B15b', 'Magnitude of increase in serum creatine kinase that leads to consider intolerance to statins', 'pgm/tlf/tlf_tur/table_b15b.sas', 'table_b15b.sas', 'T2', 'tlf/turkey/table_b15b.rtf', 'table_b15b.rtf', 'table_b15b.log'),
('PRMA', '0144000000', '01', '44', '00', '00', '00', '', 'TABLE', ' B16', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b16.sas', 'table_b16.sas', 'T3', 'tlf/turkey/table_b16.rtf', 'table_b16.rtf', 'table_b16.log'),
('PRMA', '0145000000', '01', '45', '00', '00', '00', '', 'TABLE', ' B17', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b17.sas', 'table_b17.sas', 'T3', 'tlf/turkey/table_b17.rtf', 'table_b17.rtf', 'table_b17.log'),
('PRMA', '0146000000', '01', '46', '00', '00', '00', '', 'TABLE', ' B18', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b18.sas', 'table_b18.sas', 'T3', 'tlf/turkey/table_b18.rtf', 'table_b18.rtf', 'table_b18.log'),
('PRMA', '0147000000', '01', '47', '00', '00', '00', '', 'TABLE', ' B19', 'Distribution of importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b19.sas', 'table_b19.sas', 'T4', 'tlf/turkey/table_b19.rtf', 'table_b19.rtf', 'table_b19.log'),
('PRMA', '0148000000', '01', '48', '00', '00', '00', '', 'TABLE', ' B20', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b20.sas', 'table_b20.sas', 'T4', 'tlf/turkey/table_b20.rtf', 'table_b20.rtf', 'table_b20.log'),
('PRMA', '0149000000', '01', '49', '00', '00', '00', '', 'TABLE', ' B21', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b21.sas', 'table_b21.sas', 'T4', 'tlf/turkey/table_b21.rtf', 'table_b21.rtf', 'table_b21.log'),
('PRMA', '0150000000', '01', '50', '00', '00', '00', '', 'TABLE', ' B22', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b22.sas', 'table_b22.sas', 'T4', 'tlf/turkey/table_b22.rtf', 'table_b22.rtf', 'table_b22.log'),
('PRMA', '0151000000', '01', '51', '00', '00', '00', '', 'TABLE', ' B23', 'Comparative importance of symptoms to establish intolerance to statins', 'pgm/tlf/tlf_tur/table_b23.sas', 'table_b23.sas', 'T4', 'tlf/turkey/table_b23.rtf', 'table_b23.rtf', 'table_b23.log'),
('PRMA', '0152000000', '01', '52', '00', '00', '00', '', 'TABLE', ' B24', 'Other criteria considered as important to establish intolerance to statins', 'pgm/tlf/tlf_tur/table_b24.sas', 'table_b24.sas', 'T2', 'tlf/turkey/table_b24.rtf', 'table_b24.rtf', 'table_b24.log'),
('PRMA', '0153000000', '01', '53', '00', '00', '00', '', 'TABLE', ' B25', 'List of additionalbminimum criteria used to establish intolerance to statinsain patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b25.sas', 'table_b25.sas', 'T2', 'tlf/turkey/table_b25.rtf', 'table_b25.rtf', 'table_b25.log'),
('PRMA', '0154000000', '01', '54', '00', '00', '00', '', 'TABLE', ' B26', 'Minimum criteria used to establish intolerance to statins in patients with elevated serum creatine kinase but no muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b26.sas', 'table_b26.sas', 'T2', 'tlf/turkey/table_b26.rtf', 'table_b26.rtf', 'table_b26.log'),
('PRMA', '0155000000', '01', '55', '00', '00', '00', '', 'TABLE', ' B27', 'Minimum criteria used to establish intolerance to statins in patients with CNS effects', 'pgm/tlf/tlf_tur/table_b27.sas', 'table_b27.sas', 'T2', 'tlf/turkey/table_b27.rtf', 'table_b27.rtf', 'table_b27.log'),
('PRMA', '0156000000', '01', '56', '00', '00', '00', '', 'TABLE', ' B28', 'List of additional minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b28.sas', 'table_b28.sas', 'T2', 'tlf/turkey/table_b28.rtf', 'table_b28.rtf', 'table_b28.log'),
('PRMA', '0157000000', '01', '57', '00', '00', '00', '', 'TABLE', ' B29', 'Minimum criteria used to establish intolerance to statins in patients with other symptoms', 'pgm/tlf/tlf_tur/table_b29.sas', 'table_b29.sas', 'T2', 'tlf/turkey/table_b29.rtf', 'table_b29.rtf', 'table_b29.log'),
('PRMA', '0158000000', '01', '58', '00', '00', '00', '', 'TABLE', ' B30', 'Criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b30.sas', 'table_b30.sas', 'T2', 'tlf/turkey/table_b30.rtf', 'table_b30.rtf', 'table_b30.log'),
('PRMA', '0159000000', '01', '59', '00', '00', '00', '', 'TABLE', ' B31', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_tur/table_b31.sas', 'table_b31.sas', 'T2', 'tlf/turkey/table_b31.rtf', 'table_b31.rtf', 'table_b31.log'),
('PRMA', '0160000000', '01', '60', '00', '00', '00', '', 'TABLE', ' B32', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b32.sas', 'table_b32.sas', 'T2', 'tlf/turkey/table_b32.rtf', 'table_b32.rtf', 'table_b32.log'),
('PRMA', '0161000000', '01', '61', '00', '00', '00', '', 'TABLE', ' B33', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b33.sas', 'table_b33.sas', 'T2', 'tlf/turkey/table_b33.rtf', 'table_b33.rtf', 'table_b33.log'),
('PRMA', '0162000000', '01', '62', '00', '00', '00', '', 'TABLE', ' B34', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b34.sas', 'table_b34.sas', 'T4', 'tlf/turkey/table_b34.rtf', 'table_b34.rtf', 'table_b34.log'),
('PRMA', '0163000000', '01', '63', '00', '00', '00', '', 'TABLE', ' B35', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b35.sas', 'table_b35.sas', 'T4', 'tlf/turkey/table_b35.rtf', 'table_b35.rtf', 'table_b35.log'),
('PRMA', '0164000000', '01', '64', '00', '00', '00', '', 'TABLE', ' B36', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_tur/table_b36.sas', 'table_b36.sas', 'T2', 'tlf/turkey/table_b36.rtf', 'table_b36.rtf', 'table_b36.log'),
('PRMA', '0165000000', '01', '65', '00', '00', '00', '', 'TABLE', ' B37', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_tur/table_b37.sas', 'table_b37.sas', 'T2', 'tlf/turkey/table_b37.rtf', 'table_b37.rtf', 'table_b37.log'),
('PRMA', '0166000000', '01', '66', '00', '00', '00', '', 'TABLE', ' B38', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_tur/table_b38.sas', 'table_b38.sas', 'T2', 'tlf/turkey/table_b38.rtf', 'table_b38.rtf', 'table_b38.log'),
('PRMA', '0167000000', '01', '67', '00', '00', '00', '', 'TABLE', ' B39', 'List of other guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_b39.sas', 'table_b39.sas', 'T2', 'tlf/turkey/table_b39.rtf', 'table_b39.rtf', 'table_b39.log'),
('PRMA', '0168000000', '01', '68', '00', '00', '00', '', 'TABLE', ' B40', 'Combination of guidelines used for lipid management', 'pgm/tlf/tlf_tur/table_b40.sas', 'table_b40.sas', 'T2', 'tlf/turkey/table_b40.rtf', 'table_b40.rtf', 'table_b40.log'),
('PRMA', '0169000000', '01', '69', '00', '00', '00', '', 'TABLE', ' B41', 'Modification of guidelines', 'pgm/tlf/tlf_tur/table_b41.sas', 'table_b41.sas', 'T2', 'tlf/turkey/table_b41.rtf', 'table_b41.rtf', 'table_b41.log'),
('PRMA', '0170000000', '01', '70', '00', '00', '00', '', 'TABLE', ' B41B', 'Use of vitamin D supplementation to ameliorate statin-associated symptoms', 'pgm/tlf/tlf_tur/table_b41b.sas', 'table_b41b.sas', 'T2', 'tlf/turkey/table_b41b.rtf', 'table_b41b.rtf', 'table_b41b.log'),
('PRMA', '0171000000', '01', '71', '00', '00', '00', '', 'TABLE', ' B42', 'Non-statin lipid-lowering therapies used as monotherapy by specialists/consultants', 'pgm/tlf/tlf_tur/table_b42.sas', 'table_b42.sas', 'T5', 'tlf/turkey/table_b42.rtf', 'table_b42.rtf', 'table_b42.log'),
('PRMA', '0172000000', '01', '72', '00', '00', '00', '', 'TABLE', ' B43', 'Non-statin lipid-lowering therapies used as monotherapy by general/family physicians', 'pgm/tlf/tlf_tur/table_b43.sas', 'table_b43.sas', 'T5', 'tlf/turkey/table_b43.rtf', 'table_b43.rtf', 'table_b43.log'),
('PRMA', '0173000000', '01', '73', '00', '00', '00', '', 'TABLE', ' B44', 'Non-statin lipid-lowering therapies used in combination with a statin by specialists/consultants', 'pgm/tlf/tlf_tur/table_b44.sas', 'table_b44.sas', 'T5', 'tlf/turkey/table_b44.rtf', 'table_b44.rtf', 'table_b44.log'),
('PRMA', '0174000000', '01', '74', '00', '00', '00', '', 'TABLE', ' B44a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by specialists/consultants', 'pgm/tlf/tlf_tur/table_b44a.sas', 'table_b44a.sas', 'T5', 'tlf/turkey/table_b44a.rtf', 'table_b44a.rtf', 'table_b44a.log'),
('PRMA', '0175000000', '01', '75', '00', '00', '00', '', 'TABLE', ' B45', 'Non-statin lipid-lowering therapies used in combination with a statin by general/family physicians', 'pgm/tlf/tlf_tur/table_b45.sas', 'table_b45.sas', 'T5', 'tlf/turkey/table_b45.rtf', 'table_b45.rtf', 'table_b45.log'),
('PRMA', '0176000000', '01', '76', '00', '00', '00', '', 'TABLE', ' B45a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by general/family physicians', 'pgm/tlf/tlf_tur/table_b45a.sas', 'table_b45a.sas', 'T5', 'tlf/turkey/table_b45a.rtf', 'table_b45a.rtf', 'table_b45a.log'),
('PRMA', '0177000000', '01', '77', '00', '00', '00', '', 'TABLE', ' B46', 'List of other lipid-lowering therapies used', 'pgm/tlf/tlf_tur/table_b46.sas', 'table_b46.sas', 'T1', 'tlf/turkey/table_b46.rtf', 'table_b46.rtf', 'table_b46.log'),
('PRMA', '0178000000', '01', '78', '00', '00', '00', '', 'TABLE', ' B47', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients', 'pgm/tlf/tlf_tur/table_b47.sas', 'table_b47.sas', 'T5', 'tlf/turkey/table_b47.rtf', 'table_b47.rtf', 'table_b47.log'),
('PRMA', '0179000000', '01', '79', '00', '00', '00', '', 'TABLE', ' B48', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by specialists/consultants', 'pgm/tlf/tlf_tur/table_b48.sas', 'table_b48.sas', 'T5', 'tlf/turkey/table_b48.rtf', 'table_b48.rtf', 'table_b48.log'),
('PRMA', '0180000000', '01', '80', '00', '00', '00', '', 'TABLE', ' B49', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by general/family physicians', 'pgm/tlf/tlf_tur/table_b49.sas', 'table_b49.sas', 'T5', 'tlf/turkey/table_b49.rtf', 'table_b49.rtf', 'table_b49.log'),
('PRMA', '0181000000', '01', '81', '00', '00', '00', '', 'TABLE', ' B50', 'Impact of reimbursement status on treatment choice', 'pgm/tlf/tlf_tur/table_b50.sas', 'table_b50.sas', 'T2', 'tlf/turkey/table_b50.rtf', 'table_b50.rtf', 'table_b50.log'),
('PRMA', '0182000000', '01', '82', '00', '00', '00', '', 'TABLE', ' B51', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_tur/table_b51.sas', 'table_b51.sas', 'T5', 'tlf/turkey/table_b51.rtf', 'table_b51.rtf', 'table_b51.log'),
('PRMA', '0183000000', '01', '83', '00', '00', '00', '', 'TABLE', ' B52', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_tur/table_b52.sas', 'table_b52.sas', 'T5', 'tlf/turkey/table_b52.rtf', 'table_b52.rtf', 'table_b52.log'),
('PRMA', '0184000000', '01', '84', '00', '00', '00', '', 'TABLE', ' B53', 'Reason for discontinuation of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_tur/table_b53.sas', 'table_b53.sas', 'T5', 'tlf/turkey/table_b53.rtf', 'table_b53.rtf', 'table_b53.log'),
('PRMA', '0185000000', '01', '85', '00', '00', '00', '', 'TABLE', ' B54', 'Reason for discontinuation of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_tur/table_b54.sas', 'table_b54.sas', 'T5', 'tlf/turkey/table_b54.rtf', 'table_b54.rtf', 'table_b54.log'),
('PRMA', '0200000000', '02', '00', '00', '00', '00', 'Belgium', '', '', '', '', '', '', '', '', ''),
('PRMA', '0201000000', '02', '01', '00', '00', '00', '', 'TABLE', ' A1', 'Clinical role of clinicians who participated in the survey', 'pgm/tlf/tlf_bel/table_a1.sas', 'table_a1.sas', 'T1', 'tlf/Belgium/table_a1.rtf', 'table_a1.rtf', 'table_a1.log'),
('PRMA', '0202000000', '02', '02', '00', '00', '00', '', 'TABLE', ' A2', 'Guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_a2.sas', 'table_a2.sas', 'T2', 'tlf/Belgium/table_a2.rtf', 'table_a2.rtf', 'table_a2.log'),
('PRMA', '0203000000', '02', '03', '00', '00', '00', '', 'TABLE', ' A3', 'Care of patients newly receiving statins', 'pgm/tlf/tlf_bel/table_a3.sas', 'table_a3.sas', 'T3', 'tlf/Belgium/table_a3.rtf', 'table_a3.rtf', 'table_a3.log'),
('PRMA', '0204000000', '02', '04', '00', '00', '00', '', 'TABLE', ' A4', 'Care of patients newly receiving statins: specialists/consultants', 'pgm/tlf/tlf_bel/table_a4.sas', 'table_a4.sas', 'T3', 'tlf/Belgium/table_a4.rtf', 'table_a4.rtf', 'table_a4.log'),
('PRMA', '0205000000', '02', '05', '00', '00', '00', '', 'TABLE', ' A5', 'Care of patients newly receiving statins: general/family physicians', 'pgm/tlf/tlf_bel/table_a5.sas', 'table_a5.sas', 'T3', 'tlf/Belgium/table_a5.rtf', 'table_a5.rtf', 'table_a5.log'),
('PRMA', '0206000000', '02', '06', '00', '00', '00', '', 'TABLE', ' A6', 'Number of clinicians who perform transaminase (ALT/AST) testing for patients newly prescribed statins', 'pgm/tlf/tlf_bel/table_a6.sas', 'table_a6.sas', '', 'tlf/Belgium/table_a6.rtf', 'table_a6.rtf', 'table_a6.log'),
('PRMA', '0207000000', '02', '07', '00', '00', '00', '', 'TABLE', ' A7', 'Reasons for transaminase testing', 'pgm/tlf/tlf_bel/table_a7.sas', 'table_a7.sas', 'T5', 'tlf/Belgium/table_a7.rtf', 'table_a7.rtf', 'table_a7.log'),
('PRMA', '0208000000', '02', '08', '00', '00', '00', '', 'TABLE', ' A8', 'Number of clinicians who perform creatine kinase testing for patients newly prescribed statins', 'pgm/tlf/tlf_bel/table_a8.sas', 'table_a8.sas', 'T5', 'tlf/Belgium/table_a8.rtf', 'table_a8.rtf', 'table_a8.log'),
('PRMA', '0209000000', '02', '09', '00', '00', '00', '', 'TABLE', ' A9', 'Reasons for creatine kinase testing', 'pgm/tlf/tlf_bel/table_a9.sas', 'table_a9.sas', 'T2', 'tlf/Belgium/table_a9.rtf', 'table_a9.rtf', 'table_a9.log'),
('PRMA', '0210000000', '02', '10', '00', '00', '00', '', 'TABLE', ' A10', 'Association of specific symptoms with use of statins', 'pgm/tlf/tlf_bel/table_a10.sas', 'table_a10.sas', 'T2', 'tlf/Belgium/table_a10.rtf', 'table_a10.rtf', 'table_a10.log'),
('PRMA', '0211000000', '02', '11', '00', '00', '00', '', 'TABLE', ' A11', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_bel/table_a11.sas', 'table_a11.sas', 'T1', 'tlf/Belgium/table_a11.rtf', 'table_a11.rtf', 'table_a11.log'),
('PRMA', '0212000000', '02', '12', '00', '00', '00', '', 'TABLE', ' A12', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_a12.sas', 'table_a12.sas', 'T3', 'tlf/Belgium/table_a12.rtf', 'table_a12.rtf', 'table_a12.log'),
('PRMA', '0213000000', '02', '13', '00', '00', '00', '', 'TABLE', ' A13', 'Signs or symptoms that might be associated with intolerance to statin therapy specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_a13.sas', 'table_a13.sas', 'T2', 'tlf/Belgium/table_a13.rtf', 'table_a13.rtf', 'table_a13.log'),
('PRMA', '0214000000', '02', '14', '00', '00', '00', '', 'TABLE', ' A14', 'Importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_a14.sas', 'table_a14.sas', 'T3', 'tlf/Belgium/table_a14.rtf', 'table_a14.rtf', 'table_a14.log'),
('PRMA', '0215000000', '02', '15', '00', '00', '00', '', 'TABLE', ' A15', 'Minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_a15.sas', 'table_a15.sas', 'T2', 'tlf/Belgium/table_a15.rtf', 'table_a15.rtf', 'table_a15.log'),
('PRMA', '0216000000', '02', '16', '00', '00', '00', '', 'TABLE', ' A16', 'Number of statins tried before considering patients to be unable to tolerate statins', 'pgm/tlf/tlf_bel/table_a16.sas', 'table_a16.sas', 'T2', 'tlf/Belgium/table_a16.rtf', 'table_a16.rtf', 'table_a16.log'),
('PRMA', '0217000000', '02', '17', '00', '00', '00', '', 'TABLE', ' A17', 'Identification of patients who cannot tolerate statins at the recommended therapeutic dose', 'pgm/tlf/tlf_bel/table_a17.sas', 'table_a17.sas', 'T2', 'tlf/Belgium/table_a17.rtf', 'table_a17.rtf', 'table_a17.log'),
('PRMA', '0218000000', '02', '18', '00', '00', '00', '', 'TABLE', ' A18', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_bel/table_a18.sas', 'table_a18.sas', 'T4', 'tlf/Belgium/table_a18.rtf', 'table_a18.rtf', 'table_a18.log'),
('PRMA', '0219000000', '02', '19', '00', '00', '00', '', 'TABLE', ' A19', 'Guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_a19.sas', 'table_a19.sas', 'T2', 'tlf/Belgium/table_a19.rtf', 'table_a19.rtf', 'table_a19.log'),
('PRMA', '0220000000', '02', '20', '00', '00', '00', '', 'TABLE', ' A20', 'Clinical characteristics of patients unable to tolerate statins', 'pgm/tlf/tlf_bel/table_a20.sas', 'table_a20.sas', 'T2', 'tlf/Belgium/table_a20.rtf', 'table_a20.rtf', 'table_a20.log'),
('PRMA', '0221000000', '02', '21', '00', '00', '00', '', 'TABLE', ' A21', 'Treatment strategy', 'pgm/tlf/tlf_bel/table_a21.sas', 'table_a21.sas', 'T2', 'tlf/Belgium/table_a21.rtf', 'table_a21.rtf', 'table_a21.log'),
('PRMA', '0222000000', '02', '22', '00', '00', '00', '', 'TABLE', ' A22', 'Non-statin lipid-lowering therapies used as monotherapy', 'pgm/tlf/tlf_bel/table_a22.sas', 'table_a22.sas', 'T5', 'tlf/Belgium/table_a22.rtf', 'table_a22.rtf', 'table_a22.log'),
('PRMA', '0223000000', '02', '23', '00', '00', '00', '', 'TABLE', ' A23', 'Non-statin lipid-lowering therapies used in combination with a statin', 'pgm/tlf/tlf_bel/table_a23.sas', 'table_a23.sas', 'T5', 'tlf/Belgium/table_a23.rtf', 'table_a23.rtf', 'table_a23.log'),
('PRMA', '0224000000', '02', '24', '00', '00', '00', '', 'TABLE', ' A23a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose', 'pgm/tlf/tlf_bel/table_a23a.sas', 'table_a23a.sas', 'T5', 'tlf/Belgium/table_a23a.rtf', 'table_a23a.rtf', 'table_a23a.log'),
('PRMA', '0225000000', '02', '25', '00', '00', '00', '', 'TABLE', ' A24', 'Use of lipid goals to determine treatment efficacy in clinical practice', 'pgm/tlf/tlf_bel/table_a24.sas', 'table_a24.sas', 'T2', 'tlf/Belgium/table_a24.rtf', 'table_a24.rtf', 'table_a24.log'),
('PRMA', '0226000000', '02', '26', '00', '00', '00', '', 'TABLE', ' A25', 'Perceived clinical benefit of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_bel/table_a25.sas', 'table_a25.sas', 'T5', 'tlf/Belgium/table_a25.rtf', 'table_a25.rtf', 'table_a25.log'),
('PRMA', '0227000000', '02', '27', '00', '00', '00', '', 'TABLE', ' A26', 'Reason for discontinuation of non-statin lipid-lowering therapies', 'pgm/tlf/tlf_bel/table_a26.sas', 'table_a26.sas', 'T2', 'tlf/Belgium/table_a26.rtf', 'table_a26.rtf', 'table_a26.log'),
('PRMA', '0228000000', '02', '28', '00', '00', '00', '', 'TABLE', ' B1', 'Characteristics of clinicians who participated in the survey', 'pgm/tlf/tlf_bel/table_b1.sas', 'table_b1.sas', 'T5', 'tlf/Belgium/table_b1.rtf', 'table_b1.rtf', 'table_b1.log'),
('PRMA', '0229000000', '02', '29', '00', '00', '00', '', 'TABLE', ' B2', 'List of other guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_b2.sas', 'table_b2.sas', 'T5', 'tlf/Belgium/table_b2.rtf', 'table_b2.rtf', 'table_b2.log'),
('PRMA', '0230000000', '02', '30', '00', '00', '00', '', 'TABLE', ' B3', 'Combinations of guidelines used to categorize cardiovascular risk', 'pgm/tlf/tlf_bel/table_b3.sas', 'table_b3.sas', '', 'tlf/Belgium/table_b3.rtf', 'table_b3.rtf', 'table_b3.log'),
('PRMA', '0231000000', '02', '31', '00', '00', '00', '', 'TABLE', ' B4', 'Modification of guidelines', 'pgm/tlf/tlf_bel/table_b4.sas', 'table_b4.sas', 'T2', 'tlf/Belgium/table_b4.rtf', 'table_b4.rtf', 'table_b4.log'),
('PRMA', '0232000000', '02', '32', '00', '00', '00', '', 'TABLE', ' B5', 'Timing of transaminase testing', 'pgm/tlf/tlf_bel/table_b5.sas', 'table_b5.sas', 'T5', 'tlf/Belgium/table_b5.rtf', 'table_b5.rtf', 'table_b5.log'),
('PRMA', '0233000000', '02', '33', '00', '00', '00', '', 'TABLE', ' B6', 'Timing of transaminase testing â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b6.sas', 'table_b6.sas', 'T5', 'tlf/Belgium/table_b6.rtf', 'table_b6.rtf', 'table_b6.log'),
('PRMA', '0234000000', '02', '34', '00', '00', '00', '', 'TABLE', ' B7', 'Timing of transaminase testing â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b7.sas', 'table_b7.sas', 'T5', 'tlf/Belgium/table_b7.rtf', 'table_b7.rtf', 'table_b7.log'),
('PRMA', '0235000000', '02', '35', '00', '00', '00', '', 'TABLE', ' B8', 'Timing of creatine kinase testing', 'pgm/tlf/tlf_bel/table_b8.sas', 'table_b8.sas', 'T5', 'tlf/Belgium/table_b8.rtf', 'table_b8.rtf', 'table_b8.log'),
('PRMA', '0236000000', '02', '36', '00', '00', '00', '', 'TABLE', ' B9', 'Timing of creatine kinase testing â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b9.sas', 'table_b9.sas', 'T5', 'tlf/Belgium/table_b9.rtf', 'table_b9.rtf', 'table_b9.log'),
('PRMA', '0237000000', '02', '37', '00', '00', '00', '', 'TABLE', ' B10', 'Timing of creatine kinase testing â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b10.sas', 'table_b10.sas', 'T5', 'tlf/Belgium/table_b10.rtf', 'table_b10.rtf', 'table_b10.log'),
('PRMA', '0238000000', '02', '38', '00', '00', '00', '', 'TABLE', ' B11', 'Association of specific symptoms to use of statins', 'pgm/tlf/tlf_bel/table_b11.sas', 'table_b11.sas', 'T2', 'tlf/Belgium/table_b11.rtf', 'table_b11.rtf', 'table_b11.log'),
('PRMA', '0239000000', '02', '39', '00', '00', '00', '', 'TABLE', ' B12', 'Signs or symptoms that might be associated with intolerance to statin therapy', 'pgm/tlf/tlf_bel/table_b12.sas', 'table_b12.sas', 'T2', 'tlf/Belgium/table_b12.rtf', 'table_b12.rtf', 'table_b12.log'),
('PRMA', '0240000000', '02', '40', '00', '00', '00', '', 'TABLE', ' B13', 'Association of specific symptoms with use of statins: specialists/consultants and general/family physicians', 'pgm/tlf/tlf_bel/table_b13.sas', 'table_b13.sas', 'T3', 'tlf/Belgium/table_b13.rtf', 'table_b13.rtf', 'table_b13.log'),
('PRMA', '0241000000', '02', '41', '00', '00', '00', '', 'TABLE', ' B14', 'Magnitude of increase in transaminases that leads to modifying/discontinuing statin regimen', 'pgm/tlf/tlf_bel/table_b14.sas', 'table_b14.sas', 'T2', 'tlf/Belgium/table_b14.rtf', 'table_b14.rtf', 'table_b14.log'),
('PRMA', '0242000000', '02', '42', '00', '00', '00', '', 'TABLE', ' B15', 'Use of questionnaires/tools to guide assessment of statin intolerance', 'pgm/tlf/tlf_bel/table_b15.sas', 'table_b15.sas', 'T2', 'tlf/Belgium/table_b15.rtf', 'table_b15.rtf', 'table_b15.log'),
('PRMA', '0243000000', '02', '43', '00', '00', '00', '', 'TABLE', ' B15b', 'Magnitude of increase in serum creatine kinase that leads to consider intolerance to statins', 'pgm/tlf/tlf_bel/table_b15b.sas', 'table_b15b.sas', 'T2', 'tlf/Belgium/table_b15b.rtf', 'table_b15b.rtf', 'table_b15b.log'),
('PRMA', '0244000000', '02', '44', '00', '00', '00', '', 'TABLE', ' B16', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b16.sas', 'table_b16.sas', 'T3', 'tlf/Belgium/table_b16.rtf', 'table_b16.rtf', 'table_b16.log'),
('PRMA', '0245000000', '02', '45', '00', '00', '00', '', 'TABLE', ' B17', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b17.sas', 'table_b17.sas', 'T3', 'tlf/Belgium/table_b17.rtf', 'table_b17.rtf', 'table_b17.log'),
('PRMA', '0246000000', '02', '46', '00', '00', '00', '', 'TABLE', ' B18', 'Importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b18.sas', 'table_b18.sas', 'T3', 'tlf/Belgium/table_b18.rtf', 'table_b18.rtf', 'table_b18.log'),
('PRMA', '0247000000', '02', '47', '00', '00', '00', '', 'TABLE', ' B19', 'Distribution of importance of criteria to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b19.sas', 'table_b19.sas', 'T4', 'tlf/Belgium/table_b19.rtf', 'table_b19.rtf', 'table_b19.log'),
('PRMA', '0248000000', '02', '48', '00', '00', '00', '', 'TABLE', ' B20', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b20.sas', 'table_b20.sas', 'T4', 'tlf/Belgium/table_b20.rtf', 'table_b20.rtf', 'table_b20.log'),
('PRMA', '0249000000', '02', '49', '00', '00', '00', '', 'TABLE', ' B21', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b21.sas', 'table_b21.sas', 'T4', 'tlf/Belgium/table_b21.rtf', 'table_b21.rtf', 'table_b21.log'),
('PRMA', '0250000000', '02', '50', '00', '00', '00', '', 'TABLE', ' B22', 'Distribution of importance of criteria to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b22.sas', 'table_b22.sas', 'T4', 'tlf/Belgium/table_b22.rtf', 'table_b22.rtf', 'table_b22.log'),
('PRMA', '0251000000', '02', '51', '00', '00', '00', '', 'TABLE', ' B23', 'Comparative importance of symptoms to establish intolerance to statins', 'pgm/tlf/tlf_bel/table_b23.sas', 'table_b23.sas', 'T4', 'tlf/Belgium/table_b23.rtf', 'table_b23.rtf', 'table_b23.log'),
('PRMA', '0252000000', '02', '52', '00', '00', '00', '', 'TABLE', ' B24', 'Other criteria considered as important to establish intolerance to statins', 'pgm/tlf/tlf_bel/table_b24.sas', 'table_b24.sas', 'T2', 'tlf/Belgium/table_b24.rtf', 'table_b24.rtf', 'table_b24.log'),
('PRMA', '0253000000', '02', '53', '00', '00', '00', '', 'TABLE', ' B25', 'List of additionalbminimum criteria used to establish intolerance to statinsain patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b25.sas', 'table_b25.sas', 'T2', 'tlf/Belgium/table_b25.rtf', 'table_b25.rtf', 'table_b25.log'),
('PRMA', '0254000000', '02', '54', '00', '00', '00', '', 'TABLE', ' B26', 'Minimum criteria used to establish intolerance to statins in patients with elevated serum creatine kinase but no muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b26.sas', 'table_b26.sas', 'T2', 'tlf/Belgium/table_b26.rtf', 'table_b26.rtf', 'table_b26.log'),
('PRMA', '0255000000', '02', '55', '00', '00', '00', '', 'TABLE', ' B27', 'Minimum criteria used to establish intolerance to statins in patients with CNS effects', 'pgm/tlf/tlf_bel/table_b27.sas', 'table_b27.sas', 'T2', 'tlf/Belgium/table_b27.rtf', 'table_b27.rtf', 'table_b27.log'),
('PRMA', '0256000000', '02', '56', '00', '00', '00', '', 'TABLE', ' B28', 'List of additional minimum criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b28.sas', 'table_b28.sas', 'T2', 'tlf/Belgium/table_b28.rtf', 'table_b28.rtf', 'table_b28.log'),
('PRMA', '0257000000', '02', '57', '00', '00', '00', '', 'TABLE', ' B29', 'Minimum criteria used to establish intolerance to statins in patients with other symptoms', 'pgm/tlf/tlf_bel/table_b29.sas', 'table_b29.sas', 'T2', 'tlf/Belgium/table_b29.rtf', 'table_b29.rtf', 'table_b29.log'),
('PRMA', '0258000000', '02', '58', '00', '00', '00', '', 'TABLE', ' B30', 'Criteria used to establish intolerance to statins in patients with muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b30.sas', 'table_b30.sas', 'T2', 'tlf/Belgium/table_b30.rtf', 'table_b30.rtf', 'table_b30.log'),
('PRMA', '0259000000', '02', '59', '00', '00', '00', '', 'TABLE', ' B31', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms', 'pgm/tlf/tlf_bel/table_b31.sas', 'table_b31.sas', 'T2', 'tlf/Belgium/table_b31.rtf', 'table_b31.rtf', 'table_b31.log'),
('PRMA', '0260000000', '02', '60', '00', '00', '00', '', 'TABLE', ' B32', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b32.sas', 'table_b32.sas', 'T2', 'tlf/Belgium/table_b32.rtf', 'table_b32.rtf', 'table_b32.log'),
('PRMA', '0261000000', '02', '61', '00', '00', '00', '', 'TABLE', ' B33', 'Criteria used to establish intolerance to statins in patients with non-muscle-related symptoms â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b33.sas', 'table_b33.sas', 'T2', 'tlf/Belgium/table_b33.rtf', 'table_b33.rtf', 'table_b33.log'),
('PRMA', '0262000000', '02', '62', '00', '00', '00', '', 'TABLE', ' B34', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b34.sas', 'table_b34.sas', 'T4', 'tlf/Belgium/table_b34.rtf', 'table_b34.rtf', 'table_b34.log'),
('PRMA', '0263000000', '02', '63', '00', '00', '00', '', 'TABLE', ' B35', 'Additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b35.sas', 'table_b35.sas', 'T4', 'tlf/Belgium/table_b35.rtf', 'table_b35.rtf', 'table_b35.log'),
('PRMA', '0264000000', '02', '64', '00', '00', '00', '', 'TABLE', ' B36', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event', 'pgm/tlf/tlf_bel/table_b36.sas', 'table_b36.sas', 'T2', 'tlf/Belgium/table_b36.rtf', 'table_b36.rtf', 'table_b36.log'),
('PRMA', '0265000000', '02', '65', '00', '00', '00', '', 'TABLE', ' B37', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ specialists/consultants', 'pgm/tlf/tlf_bel/table_b37.sas', 'table_b37.sas', 'T2', 'tlf/Belgium/table_b37.rtf', 'table_b37.rtf', 'table_b37.log'),
('PRMA', '0266000000', '02', '66', '00', '00', '00', '', 'TABLE', ' B38', 'Summary of additional criteria to identify statin intolerance in subgroups of patients at high risk for a CV event â€“ general/family physicians', 'pgm/tlf/tlf_bel/table_b38.sas', 'table_b38.sas', 'T2', 'tlf/Belgium/table_b38.rtf', 'table_b38.rtf', 'table_b38.log'),
('PRMA', '0267000000', '02', '67', '00', '00', '00', '', 'TABLE', ' B39', 'List of other guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_b39.sas', 'table_b39.sas', 'T2', 'tlf/Belgium/table_b39.rtf', 'table_b39.rtf', 'table_b39.log'),
('PRMA', '0268000000', '02', '68', '00', '00', '00', '', 'TABLE', ' B40', 'Combination of guidelines used for lipid management', 'pgm/tlf/tlf_bel/table_b40.sas', 'table_b40.sas', 'T2', 'tlf/Belgium/table_b40.rtf', 'table_b40.rtf', 'table_b40.log'),
('PRMA', '0269000000', '02', '69', '00', '00', '00', '', 'TABLE', ' B41', 'Modification of guidelines', 'pgm/tlf/tlf_bel/table_b41.sas', 'table_b41.sas', 'T2', 'tlf/Belgium/table_b41.rtf', 'table_b41.rtf', 'table_b41.log'),
('PRMA', '0270000000', '02', '70', '00', '00', '00', '', 'TABLE', ' B41B', 'Use of vitamin D supplementation to ameliorate statin-associated symptoms', 'pgm/tlf/tlf_bel/table_b41b.sas', 'table_b41b.sas', 'T2', 'tlf/Belgium/table_b41b.rtf', 'table_b41b.rtf', 'table_b41b.log'),
('PRMA', '0271000000', '02', '71', '00', '00', '00', '', 'TABLE', ' B42', 'Non-statin lipid-lowering therapies used as monotherapy by specialists/consultants', 'pgm/tlf/tlf_bel/table_b42.sas', 'table_b42.sas', 'T5', 'tlf/Belgium/table_b42.rtf', 'table_b42.rtf', 'table_b42.log'),
('PRMA', '0272000000', '02', '72', '00', '00', '00', '', 'TABLE', ' B43', 'Non-statin lipid-lowering therapies used as monotherapy by general/family physicians', 'pgm/tlf/tlf_bel/table_b43.sas', 'table_b43.sas', 'T5', 'tlf/Belgium/table_b43.rtf', 'table_b43.rtf', 'table_b43.log'),
('PRMA', '0273000000', '02', '73', '00', '00', '00', '', 'TABLE', ' B44', 'Non-statin lipid-lowering therapies used in combination with a statin by specialists/consultants', 'pgm/tlf/tlf_bel/table_b44.sas', 'table_b44.sas', 'T5', 'tlf/Belgium/table_b44.rtf', 'table_b44.rtf', 'table_b44.log'),
('PRMA', '0274000000', '02', '74', '00', '00', '00', '', 'TABLE', ' B44a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by specialists/consultants', 'pgm/tlf/tlf_bel/table_b44a.sas', 'table_b44a.sas', 'T5', 'tlf/Belgium/table_b44a.rtf', 'table_b44a.rtf', 'table_b44a.log'),
('PRMA', '0275000000', '02', '75', '00', '00', '00', '', 'TABLE', ' B45', 'Non-statin lipid-lowering therapies used in combination with a statin by general/family physicians', 'pgm/tlf/tlf_bel/table_b45.sas', 'table_b45.sas', 'T5', 'tlf/Belgium/table_b45.rtf', 'table_b45.rtf', 'table_b45.log'),
('PRMA', '0276000000', '02', '76', '00', '00', '00', '', 'TABLE', ' B45a', 'Lipid-lowering therapies used for patients who are prescribed a dose of statin lower than the recommended therapeutic dose by general/family physicians', 'pgm/tlf/tlf_bel/table_b45a.sas', 'table_b45a.sas', 'T5', 'tlf/Belgium/table_b45a.rtf', 'table_b45a.rtf', 'table_b45a.log'),
('PRMA', '0277000000', '02', '77', '00', '00', '00', '', 'TABLE', ' B46', 'List of other lipid-lowering therapies used', 'pgm/tlf/tlf_bel/table_b46.sas', 'table_b46.sas', 'T1', 'tlf/Belgium/table_b46.rtf', 'table_b46.rtf', 'table_b46.log'),
('PRMA', '0278000000', '02', '78', '00', '00', '00', '', 'TABLE', ' B47', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients', 'pgm/tlf/tlf_bel/table_b47.sas', 'table_b47.sas', 'T5', 'tlf/Belgium/table_b47.rtf', 'table_b47.rtf', 'table_b47.log'),
('PRMA', '0279000000', '02', '79', '00', '00', '00', '', 'TABLE', ' B48', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by specialists/consultants', 'pgm/tlf/tlf_bel/table_b48.sas', 'table_b48.sas', 'T5', 'tlf/Belgium/table_b48.rtf', 'table_b48.rtf', 'table_b48.log'),
('PRMA', '0280000000', '02', '80', '00', '00', '00', '', 'TABLE', ' B49', 'Difference in non-statin lipid-lowering therapies used for subgroups of patients by general/family physicians', 'pgm/tlf/tlf_bel/table_b49.sas', 'table_b49.sas', 'T5', 'tlf/Belgium/table_b49.rtf', 'table_b49.rtf', 'table_b49.log'),
('PRMA', '0281000000', '02', '81', '00', '00', '00', '', 'TABLE', ' B50', 'Impact of reimbursement status on treatment choice', 'pgm/tlf/tlf_bel/table_b50.sas', 'table_b50.sas', 'T2', 'tlf/Belgium/table_b50.rtf', 'table_b50.rtf', 'table_b50.log'),
('PRMA', '0282000000', '02', '82', '00', '00', '00', '', 'TABLE', ' B51', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_bel/table_b51.sas', 'table_b51.sas', 'T5', 'tlf/Belgium/table_b51.rtf', 'table_b51.rtf', 'table_b51.log'),
('PRMA', '0283000000', '02', '83', '00', '00', '00', '', 'TABLE', ' B52', 'Perceived clinical benefit of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_bel/table_b52.sas', 'table_b52.sas', 'T5', 'tlf/Belgium/table_b52.rtf', 'table_b52.rtf', 'table_b52.log'),
('PRMA', '0284000000', '02', '84', '00', '00', '00', '', 'TABLE', ' B53', 'Reason for discontinuation of non-statin lipid-lowering therapies according to specialists/consultants', 'pgm/tlf/tlf_bel/table_b53.sas', 'table_b53.sas', 'T5', 'tlf/Belgium/table_b53.rtf', 'table_b53.rtf', 'table_b53.log'),
('PRMA', '0285000000', '02', '85', '00', '00', '00', '', 'TABLE', ' B54', 'Reason for discontinuation of non-statin lipid-lowering therapies according to general/family physicians', 'pgm/tlf/tlf_bel/table_b54.sas', 'table_b54.sas', 'T5', 'tlf/Belgium/table_b54.rtf', 'table_b54.rtf', 'table_b54.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_r101`
--

CREATE TABLE IF NOT EXISTS `toc_r101` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_r101`
--

INSERT INTO `toc_r101` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('R101', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', ''),
('R101', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', ''),
('R101', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('R101', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('R101', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('R101', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('R101', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('R101', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('R101', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('r101', '0102030000', '01', '02', '03', '00', '00', ' ', 'Table', 'demo entry', 'pgm/ctr/demotest.sas', 'demotest.sas', 'T', 'pgm/ctr/demotest.sas', 'demotest_t.lst', 'demotest.log'),
('r101', '0102040000', '01', '02', '04', '00', '00', ' ', 'Table', 'demo entry another', 'pgm/ctr/demopgm.sas', 'demopgm.sas', 'T', 'pgm/ctr/demopgm.sas', 'demopgm_t.lst', 'demopgm.log'),
('R101', '0103000000', '01', '03', '00', '00', '00', 'Definition of analysis sets', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('R101', '0104000000', '01', '04', '00', '00', '00', 'Demographic data and baseline characteristics', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0104010000', '01', '04', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0104010100', '01', '04', '01', '01', '00', ' ', 'Table', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('R101', '0104010200', '01', '04', '01', '02', '00', ' ', 'Table', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('R101', '0104010300', '01', '04', '01', '03', '00', ' ', 'Table', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('R101', '0104010400', '01', '04', '01', '04', '00', ' ', 'Table', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('r101', '0104010500', '01', '04', '01', '05', '00', ' ', 'Table', 'this this test case', 'pgm/ctr/dert.sas', 'dert.sas', 'T', 'pgm/ctr/dert.sas', 'dert_t.lst', 'dert.log'),
('R101', '0104010600', '01', '04', '01', '06', '00', ' ', 'Table', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('R101', '0104010700', '01', '04', '01', '07', '00', ' ', 'Table', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('R101', '0104010800', '01', '04', '01', '08', '00', ' ', 'Table', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('R101', '0104010900', '01', '04', '01', '09', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('R101', '0104011000', '01', '04', '01', '10', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('R101', '0104011100', '01', '04', '01', '11', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T4', 'lst/cttass_t3.lst', 'cttass_t4.lst', 'cttass.log'),
('R101', '0104020000', '01', '04', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0104020100', '01', '04', '02', '01', '00', ' ', 'Table', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('R101', '0104020200', '01', '04', '02', '02', '00', ' ', 'Table', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('R101', '0104020300', '01', '04', '02', '03', '00', ' ', 'Table', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('R101', '0106000000', '01', '06', '00', '00', '00', 'Compliance data', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0106010000', '01', '06', '01', '00', '00', ' ', 'Table', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('R101', '0106020000', '01', '06', '02', '00', '00', ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log'),
('r101', '0107000000', '01', '07', '00', '00', '00', 'dummy section', '', '', '', '', '', '', '', ''),
('r101', '0107010000', '01', '07', '01', '00', '00', ' ', 'Table', 'test title', 'pgm/ctr/daess.sas', 'daess.sas', 'T', 'pgm/ctr/daess.sas', 'daess_t.lst', 'daess.log'),
('r101', '0107020000', '01', '07', '02', '00', '00', ' ', 'Table', 'demo entry another2', 'pgm/ctr/dd.sas', 'dd.sas', 'T', 'pgm/ctr/dd.sas', 'dd_t.lst', 'dd.log'),
('R101', '0200000000', '02', '00', '00', '00', '00', 'TRIAL SUBJECTS 2', '', '', '', '', '', '', '', ''),
('R101', '0201000000', '02', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', ''),
('R101', '0201010000', '02', '01', '01', '00', '00', ' ', 'Table', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('R101', '0201020000', '02', '01', '02', '00', '00', ' ', 'Table', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('R101', '0201030000', '02', '01', '03', '00', '00', ' ', 'Table', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('R101', '0201040000', '02', '01', '04', '00', '00', ' ', 'Table', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('R101', '0201050000', '02', '01', '05', '00', '00', ' ', 'Table', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('R101', '0201060000', '02', '01', '06', '00', '00', ' ', 'Table', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('R101', '0202000000', '02', '02', '00', '00', '00', 'Important protocol violations', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0202010000', '02', '02', '01', '00', '00', ' ', 'Table', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('R101', '0203000000', '02', '03', '00', '00', '00', 'Definition of analysis sets', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0203010000', '02', '03', '01', '00', '00', ' ', 'Table', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('R101', '0204000000', '02', '04', '00', '00', '00', 'Demographic data and baseline characteristics', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0204010000', '02', '04', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0204010100', '02', '04', '01', '01', '00', ' ', 'Table', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('R101', '0204010200', '02', '04', '01', '02', '00', ' ', 'Table', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('R101', '0204010300', '02', '04', '01', '03', '00', ' ', 'Table', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('R101', '0204010400', '02', '04', '01', '04', '00', ' ', 'Table', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('R101', '0204010500', '02', '04', '01', '05', '00', ' ', 'Table', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('R101', '0204010600', '02', '04', '01', '06', '00', ' ', 'Table', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('R101', '0204010700', '02', '04', '01', '07', '00', ' ', 'Table', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('R101', '0204010800', '02', '04', '01', '08', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('R101', '0204010900', '02', '04', '01', '09', '00', ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('R101', '0204020000', '02', '04', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0204020100', '02', '04', '02', '01', '00', ' ', 'Table', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('R101', '0204020200', '02', '04', '02', '02', '00', ' ', 'Table', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('R101', '0204020300', '02', '04', '02', '03', '00', ' ', 'Table', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('R101', '0205000000', '02', '05', '00', '00', '00', 'Compliance data', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('R101', '0205010000', '02', '05', '01', '00', '00', ' ', 'Table', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('R101', '0205020000', '02', '05', '02', '00', '00', ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_1264_0003`
--

CREATE TABLE IF NOT EXISTS `toc_status_1264_0003` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_1264_0003`
--

INSERT INTO `toc_status_1264_0003` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('1264_0003', '0100000000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0101000000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0101010000', 1, 'No Program', '2015-08-26 14:14:46', '2016-04-05 16:45:24', '2016-01-11 15:29:54', '2016-02-22 12:06:41', 2, 'No Output', '2016-04-07 15:36:03', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0101020000', 1, 'No Program', '2015-08-26 14:14:46', '2016-04-05 16:45:24', '2016-02-22 12:48:01', '2016-02-28 12:55:43', 2, 'No Output', '2016-04-07 15:36:03', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0101030000', 2, 'No Program', '2015-08-26 14:14:46', '2016-02-23 12:48:57', '2016-02-23 12:52:54', '2015-10-13 12:43:02', 1, 'No Output', '2016-02-23 12:48:57', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0101040000', 3, 'No Program', '2015-08-26 14:14:46', '2015-10-13 12:51:44', '2016-01-07 11:10:51', '2016-01-14 10:31:34', 1, 'No Output', '2015-10-13 12:51:44', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0101050000', 1, 'No Program', '2015-08-26 14:14:46', '2015-08-26 14:18:20', '2015-08-26 14:14:46', '2015-08-26 14:14:46', 0, 'No Output', '2015-08-26 14:14:46', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0101060000', 1, 'No Program', '2015-08-26 14:14:46', '2015-08-26 14:19:04', '2015-08-26 14:14:46', '2015-08-26 14:14:46', 0, 'No Output', '2015-08-26 14:14:46', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0102000000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0102010000', 1, 'No Program', '2015-08-26 14:14:46', '2016-02-23 12:30:59', '2015-08-26 14:14:46', '2015-08-26 14:14:46', 0, 'No Output', '2015-08-26 14:14:46', '2015-08-26 14:14:46', '2015-08-26 14:14:46'),
('1264_0003', '0103000000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0103010000', 1, 'No Program', '2015-10-13 12:29:54', '2016-02-04 10:31:40', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0104000000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104010000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104010100', 1, 'No Program', '2015-09-20 15:43:02', '2015-10-12 21:33:43', '2015-09-20 15:43:02', '2015-09-20 15:43:02', 0, 'No Output', '2015-09-20 15:43:02', '2015-09-20 15:43:02', '2015-09-20 15:43:02'),
('1264_0003', '0104010200', 1, 'No Program', '2015-10-13 12:29:54', '2016-01-07 13:06:41', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0104010300', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104010400', 1, 'No Program', '2015-10-13 12:29:54', '2016-01-11 09:53:03', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0104010500', 1, 'No Program', '2015-10-13 12:29:54', '2016-01-11 10:07:47', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0104010600', 1, 'No Program', '2015-10-13 12:29:54', '2016-01-11 15:16:39', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0104010700', 1, 'No Program', '2015-10-13 12:29:54', '2016-02-04 11:00:20', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0104010800', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104010900', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104020000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104020100', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104020200', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0104020300', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0105000000', 0, 'No Program', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54', 0, 'No Output', '2016-04-05 15:26:54', '2016-04-05 15:26:54', '2016-04-05 15:26:54'),
('1264_0003', '0105010000', 1, 'No Program', '2015-10-13 12:29:54', '2016-02-23 12:33:53', '2015-10-13 12:29:54', '2015-10-13 12:29:54', 0, 'No Output', '2015-10-13 12:29:54', '2015-10-13 12:29:54', '2015-10-13 12:29:54'),
('1264_0003', '0105020000', 0, 'No Program', '2016-04-05 15:26:55', '2016-04-05 15:26:55', '2016-04-05 15:26:55', '2016-04-05 15:26:55', 0, 'No Output', '2016-04-05 15:26:55', '2016-04-05 15:26:55', '2016-04-05 15:26:55');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_dmc3`
--

CREATE TABLE IF NOT EXISTS `toc_status_dmc3` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_dmc3`
--

INSERT INTO `toc_status_dmc3` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('dmc3', '0100000000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0101000000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0101010000', 1, 'No Program', '2015-10-12 21:57:28', '2015-10-12 21:59:35', '2015-10-12 21:57:28', '2015-10-12 21:57:28', 0, 'No Output', '2015-10-12 21:57:28', '2015-10-12 21:57:28', '2015-10-12 21:57:28'),
('dmc3', '0101020000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0101030000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0101040000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0101050000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0101060000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0102000000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0102010000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0103000000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0103010000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104000000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010100', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010200', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010300', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010400', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010500', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010600', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010700', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010800', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104010900', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104020000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104020100', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104020200', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0104020300', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0105000000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0105010000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0105020000', 0, 'No Program', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43', 0, 'No Output', '2015-10-13 11:05:43', '2015-10-13 11:05:43', '2015-10-13 11:05:43'),
('dmc3', '0105030000', 0, 'No Program', '2015-10-13 11:05:44', '2015-10-13 11:05:44', '2015-10-13 11:05:44', '2015-10-13 11:05:44', 0, 'No Output', '2015-10-13 11:05:44', '2015-10-13 11:05:44', '2015-10-13 11:05:44');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_iss_cariprazine`
--

CREATE TABLE IF NOT EXISTS `toc_status_iss_cariprazine` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_iss_cariprazine`
--

INSERT INTO `toc_status_iss_cariprazine` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('', '0000000000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0100000000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101000000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101010000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101020000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101030000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101040000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101050000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101060000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101070000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101080000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101090000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101100000', 0, 'No Program', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15', 0, 'No Output', '2015-10-26 08:23:15', '2015-10-26 08:23:15', '2015-10-26 08:23:15'),
('ISS_Cariprazine', '0101110000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101120000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101130000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101140000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101150000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101160000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101170000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101180000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101190000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101200000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101210000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101220000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101230000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101240000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101250000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101260000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101270000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101280000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101290000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101300000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101310000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101320000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101330000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101340000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101350000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101360000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101370000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101380000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101390000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101400000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101410000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0101420000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102000000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102010000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102020000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102030000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102040000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102050000', 0, 'No Program', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16', 0, 'No Output', '2015-10-26 08:23:16', '2015-10-26 08:23:16', '2015-10-26 08:23:16'),
('ISS_Cariprazine', '0102060000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102070000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102080000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102090000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102100000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102110000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102120000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102130000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102140000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102150000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102160000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102170000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102180000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102190000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102200000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102210000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102220000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102230000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102240000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102250000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102260000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102270000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0102280000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103000000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103010000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103020000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103030000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103040000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103050000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103060000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103070000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103080000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103090000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103100000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103110000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103120000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103130000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103140000', 0, 'No Program', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17', 0, 'No Output', '2015-10-26 08:23:17', '2015-10-26 08:23:17', '2015-10-26 08:23:17'),
('ISS_Cariprazine', '0103150000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103160000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103170000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103180000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103190000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103200000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103210000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103220000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103230000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103240000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103250000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103260000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103270000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103280000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103290000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103300000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103310000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103320000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103330000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103340000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103350000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103360000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103370000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103380000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103390000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103400000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103410000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18'),
('ISS_Cariprazine', '0103420000', 0, 'No Program', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18', 0, 'No Output', '2015-10-26 08:23:18', '2015-10-26 08:23:18', '2015-10-26 08:23:18');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_prma`
--

CREATE TABLE IF NOT EXISTS `toc_status_prma` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_prma`
--

INSERT INTO `toc_status_prma` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('', '0000000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0100000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0101000000', 1, 'No Program', '2016-04-06 12:39:12', '2016-04-07 12:00:31', '2016-04-06 12:39:12', '2016-04-06 12:39:12', 2, 'No Output', '2016-04-07 15:01:16', '2016-04-06 12:39:12', '2016-04-06 12:39:12'),
('PRMA', '0102000000', 1, 'No Program', '2016-04-07 14:58:13', '2016-04-07 15:14:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 2, 'No Output', '2016-04-07 15:29:38', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0103000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0104000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0105000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0106000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0107000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0108000000', 0, 'No Program', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13', 0, 'No Output', '2016-04-07 14:58:13', '2016-04-07 14:58:13', '2016-04-07 14:58:13'),
('PRMA', '0109000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0110000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0111000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0112000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0113000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0114000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0115000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0116000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0117000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0118000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0119000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0120000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0121000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0122000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0123000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0124000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0125000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0126000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0127000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0128000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0129000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0130000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0131000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0132000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0133000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0134000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0135000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0136000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0137000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0138000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0139000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0140000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0141000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0142000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0143000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0144000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0145000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0146000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0147000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0148000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0149000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0150000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0151000000', 0, 'No Program', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14', 0, 'No Output', '2016-04-07 14:58:14', '2016-04-07 14:58:14', '2016-04-07 14:58:14'),
('PRMA', '0152000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0153000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0154000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0155000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0156000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0157000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0158000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0159000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0160000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0161000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0162000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0163000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0164000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0165000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0166000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0167000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0168000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0169000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0170000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0171000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0172000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0173000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0174000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0175000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0176000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0177000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0178000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0179000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0180000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0181000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0182000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0183000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0184000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0185000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0200000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0201000000', 1, 'No Program', '2016-04-06 12:39:12', '2016-04-07 12:00:31', '2016-04-06 12:39:12', '2016-04-06 12:39:12', 2, 'No Output', '2016-04-07 15:01:16', '2016-04-06 12:39:12', '2016-04-06 12:39:12'),
('PRMA', '0202000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0203000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0204000000', 0, 'No Program', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15', 0, 'No Output', '2016-04-07 14:58:15', '2016-04-07 14:58:15', '2016-04-07 14:58:15'),
('PRMA', '0205000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0206000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0207000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0208000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0209000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0210000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0211000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0212000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0213000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0214000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0215000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0216000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0217000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0218000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0219000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0220000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0221000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0222000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0223000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0224000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0225000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0226000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0227000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0228000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0229000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0230000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0231000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0232000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0233000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0234000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0235000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0236000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0237000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0238000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0239000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0240000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0241000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0242000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0243000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0244000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0245000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0246000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0247000000', 0, 'No Program', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16', 0, 'No Output', '2016-04-07 14:58:16', '2016-04-07 14:58:16', '2016-04-07 14:58:16'),
('PRMA', '0248000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0249000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0250000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0251000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0252000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0253000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0254000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0255000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0256000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0257000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0258000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0259000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0260000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0261000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0262000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0263000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0264000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0265000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0266000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0267000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0268000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0269000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0270000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0271000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0272000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0273000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0274000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0275000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0276000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0277000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0278000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0279000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0280000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0281000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0282000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0283000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0284000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17'),
('PRMA', '0285000000', 0, 'No Program', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17', 0, 'No Output', '2016-04-07 14:58:17', '2016-04-07 14:58:17', '2016-04-07 14:58:17');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_r101`
--

CREATE TABLE IF NOT EXISTS `toc_status_r101` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_r101`
--

INSERT INTO `toc_status_r101` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('r101', '', 0, 'No Program', '2015-09-14 13:21:49', '2015-09-14 13:21:49', '2015-09-14 13:21:49', '2015-09-14 13:21:49', 0, 'No Output', '2015-09-14 13:21:49', '2015-09-14 13:21:49', '2015-09-14 13:21:49'),
('R101', '0100000000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101000000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101010000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101020000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101030000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101040000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101050000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0101060000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0102000000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0102010000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('r101', '0102030000', 0, 'No Program', '2015-09-14 13:13:29', '2015-09-14 13:13:29', '2015-09-14 13:13:29', '2015-09-14 13:13:29', 0, 'No Output', '2015-09-14 13:13:29', '2015-09-14 13:13:29', '2015-09-14 13:13:29'),
('r101', '0102040000', 0, 'No Program', '2015-09-14 13:21:00', '2015-09-14 13:21:00', '2015-09-14 13:21:00', '2015-09-14 13:21:00', 0, 'No Output', '2015-09-14 13:21:00', '2015-09-14 13:21:00', '2015-09-14 13:21:00'),
('R101', '0103000000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0103010000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104000000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104010000', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104010100', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104010200', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104010300', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104010400', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('r101', '0104010500', 0, 'No Program', '2015-09-14 13:30:47', '2015-09-14 13:30:47', '2015-09-14 13:30:47', '2015-09-14 13:30:47', 0, 'No Output', '2015-09-14 13:30:47', '2015-09-14 13:30:47', '2015-09-14 13:30:47'),
('R101', '0104010600', 0, 'No Program', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00', 0, 'No Output', '2015-09-14 10:36:00', '2015-09-14 10:36:00', '2015-09-14 10:36:00'),
('R101', '0104010700', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104010800', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104010900', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104011000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104011100', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104020000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104020100', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104020200', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0104020300', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0106000000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0106010000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0106020000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('r101', '0107000000', 0, 'No Program', '2015-09-14 13:27:44', '2015-09-14 13:27:44', '2015-09-14 13:27:44', '2015-09-14 13:27:44', 0, 'No Output', '2015-09-14 13:27:44', '2015-09-14 13:27:44', '2015-09-14 13:27:44'),
('r101', '0107010000', 0, 'No Program', '2015-09-14 13:30:13', '2015-09-14 13:30:13', '2015-09-14 13:30:13', '2015-09-14 13:30:13', 0, 'No Output', '2015-09-14 13:30:13', '2015-09-14 13:30:13', '2015-09-14 13:30:13'),
('r101', '0107020000', 0, 'No Program', '2015-09-14 13:29:42', '2015-09-14 13:29:42', '2015-09-14 13:29:42', '2015-09-14 13:29:42', 0, 'No Output', '2015-09-14 13:29:42', '2015-09-14 13:29:42', '2015-09-14 13:29:42'),
('R101', '0200000000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201000000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201010000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201020000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201030000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201040000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201050000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0201060000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0202000000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0202010000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0203000000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0203010000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204000000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010000', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010100', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010200', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010300', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010400', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010500', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010600', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010700', 0, 'No Program', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01', 0, 'No Output', '2015-09-14 10:36:01', '2015-09-14 10:36:01', '2015-09-14 10:36:01'),
('R101', '0204010800', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0204010900', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0204020000', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0204020100', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0204020200', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0204020300', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0205000000', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0205010000', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02'),
('R101', '0205020000', 0, 'No Program', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02', 0, 'No Output', '2015-09-14 10:36:02', '2015-09-14 10:36:02', '2015-09-14 10:36:02');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstlog` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL DEFAULT '1',
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12373 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstlog`, `user_id`, `user_type`, `password`, `username`, `email`, `creation_date`, `status`, `fullname`) VALUES
('No', 1, 2, '8cf44671859ba9e5118c9ea35ace86b3', 'ahmed', '', '2016-01-11 15:13:53', 'ON', ''),
('No', 2, 3, 'aa68c75c4a77c87f97fb686b2f068676', 'mark', '', '2015-03-11 00:00:00', 'ON', ''),
('', 3, 1, '6974ce5ac660610b44d9b9fed0ff9548', 'jack', 'editor7@gmail.com', '2015-03-11 00:00:00', 'OFF', 'mr jack'),
('', 4001, 2, '474c909a68ce9b63e7f694d09a8f37f0', 'editor1', 'editor1@tocsystsem.com', '2015-04-21 09:00:00', 'OFF', 'md editor1'),
('', 12345, 0, '474c909a68ce9b63e7f694d09a8f37f0', 'admin', '', '2015-03-30 00:00:00', 'ON', ''),
('', 12346, 2, 'e10adc3949ba59abbe56e057f20f883e', 'mizan22', 'mm@tt.com', '0000-00-00 00:00:00', 'ON', 'mizan alam'),
('', 12347, 1, 'e10adc3949ba59abbe56e057f20f883e', 'Jabrul', 'jj@tt.com', '0000-00-00 00:00:00', 'ON', 'jabrul islam'),
('', 12348, 2, 'e10adc3949ba59abbe56e057f20f883e', 'editor2', 'editor2@gmail.com', '0000-00-00 00:00:00', 'ON', 'mr editor2'),
('No', 12349, 3, '38b3eff8baf56627478ec76a704e9b52', 'viewer1', 'editor2@gmail.com', '0000-00-00 00:00:00', 'ON', 'mr viewer1'),
('No', 12350, 1, 'e10adc3949ba59abbe56e057f20f883e', 'trailadmin', 'trailadmin@gmail.com', '0000-00-00 00:00:00', 'ON', 'mr trail admin'),
('No', 12351, 2, 'e10adc3949ba59abbe56e057f20f883e', 'editor3', 'editor2@gmail.com', '2015-07-24 00:00:00', 'ON', 'mr editor3'),
('Yes', 12352, 1, '9fb17ccab478814d8f64cc6135fe014d', 'editor5', 'editor5@gmail.com', '2015-08-18 14:42:12', 'ON', 'mr editor5'),
('Yes', 12353, 1, '9fb17ccab478814d8f64cc6135fe014d', 'editor6', 'editor6@gmail.com', '2015-08-18 14:44:21', 'ON', 'mr editor6'),
('No', 12354, 1, '474c909a68ce9b63e7f694d09a8f37f0', 'editor7', 'editor7@gmail.com', '2015-08-23 12:51:28', 'ON', 'mr editor7'),
('No', 12355, 1, '474c909a68ce9b63e7f694d09a8f37f0', 'editor8', 'editor2@gmail.com', '2015-08-25 19:00:52', 'ON', 'mr editor8'),
('No', 12356, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_zr', 'zahir@shaficonsultancy.com', '2015-08-26 11:14:56', 'ON', 'QA Tester'),
('Yes', 12357, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_zahir', 'zahir@shaficonsultancycom', '2015-08-26 11:19:26', 'ON', 'QA tester'),
('Yes', 12358, 1, '474c909a68ce9b63e7f694d09a8f37f0', 'userssssss', 'use@gmailcom', '2015-08-26 12:21:15', 'ON', 'mr editor9'),
('No', 12359, 1, '8310b1de9555c179ac4ca52b4d5db9d0', 'lucky', 'lucky@shaficonsultancy.com', '2015-08-26 12:23:11', 'OFF', 'lr'),
('No', 12360, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_editor', 'zahir_raihan@outlook.com', '2015-08-26 13:56:58', 'ON', 'Editor Tester'),
('No', 12361, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_admin', 'zahir_raihan@outlookcom', '2015-08-27 16:06:18', 'ON', 'QA admin'),
('No', 12362, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_viewer', 'zahir_raihan@outlook.com', '2015-08-26 14:45:27', 'ON', 'QA viewer'),
('Yes', 12363, 1, 'ba8d90d351b8ddbddc4eea517bde620f', 'test_admin', 'lucky@shaficonsultancy.com', '2015-08-26 14:53:41', 'ON', 'lr'),
('No', 12364, 1, 'bdbf71578229d0ba317254416e2606ac', 'test_editor', 'lucky@shaficonsultancy.com', '2015-08-27 09:59:27', 'ON', 'editor'),
('No', 12365, 1, 'bdbf71578229d0ba317254416e2606ac', 'admin_test', 'lucky@shaficonsultancy.com', '2015-08-27 10:22:46', 'ON', 'lr'),
('Yes', 12366, 1, 'e79a086dc8c6fa7e8aa5b9ecd7f81c25', 'test_editor1', 'lucky@shaficonsultancy.com', '2015-08-27 11:30:41', 'ON', 'editor'),
('No', 12367, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_tester007', 'zahir_raihan@outlook.com', '2015-08-27 16:02:40', 'ON', 'QA tester'),
('Yes', 12368, 1, '08f5b04545cbf7eaa238621b9ab84734', 'qa_tester000', 'zahir_raihan@outlook.com', '2015-08-27 16:04:38', 'ON', 'QA Tester'),
('No', 12369, 1, '474c909a68ce9b63e7f694d09a8f37f0', 'editor78', 'ahmed@gmail.com', '2015-08-31 11:44:31', 'ON', 'mr editor78'),
('No', 12370, 1, '8beaff72b863d1118f5c8cd52f0b2773', 'nur1', 'nur@gmail.com', '2015-08-31 14:56:44', 'ON', 'nur'),
('Yes', 12371, 1, '8a1e9bb4f010d7a4656721c17b96e62c', 'zobair', 'zobair@shaficonsultancy.com', '2015-09-23 11:07:18', 'ON', 'mr zobair'),
('Yes', 12372, 1, '8cf44671859ba9e5118c9ea35ace86b3', 'user_mz', 'jubijubi08@yahoo.com', '2015-10-12 21:01:12', 'ON', 'mr user_mz');

-- --------------------------------------------------------

--
-- Table structure for table `user_pwd`
--

CREATE TABLE IF NOT EXISTS `user_pwd` (
  `name` char(30) COLLATE latin1_general_ci NOT NULL DEFAULT '',
  `pass` char(32) COLLATE latin1_general_ci NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `user_pwd`
--

INSERT INTO `user_pwd` (`name`, `pass`) VALUES
('xampp', 'wampp');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_info`
--

CREATE TABLE IF NOT EXISTS `user_type_info` (
  `user_type` int(11) NOT NULL,
  `user_type_dc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_info`
--

INSERT INTO `user_type_info` (`user_type`, `user_type_dc`) VALUES
(0, 'Super Admin'),
(1, 'Trail Admin'),
(2, 'Editor'),
(3, 'Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `cplist_1264_0003`
--
ALTER TABLE `cplist_1264_0003`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_dmc3`
--
ALTER TABLE `cplist_dmc3`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_iss_cariprazine`
--
ALTER TABLE `cplist_iss_cariprazine`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_prma`
--
ALTER TABLE `cplist_prma`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_r101`
--
ALTER TABLE `cplist_r101`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `pgm_hist_1264_0003`
--
ALTER TABLE `pgm_hist_1264_0003`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_dmc3`
--
ALTER TABLE `pgm_hist_dmc3`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_iss_cariprazine`
--
ALTER TABLE `pgm_hist_iss_cariprazine`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_prma`
--
ALTER TABLE `pgm_hist_prma`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_r101`
--
ALTER TABLE `pgm_hist_r101`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_lock_status`
--
ALTER TABLE `pgm_lock_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `toc_1264_0003`
--
ALTER TABLE `toc_1264_0003`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_dmc3`
--
ALTER TABLE `toc_dmc3`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_iss_cariprazine`
--
ALTER TABLE `toc_iss_cariprazine`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_prma`
--
ALTER TABLE `toc_prma`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_r101`
--
ALTER TABLE `toc_r101`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_1264_0003`
--
ALTER TABLE `toc_status_1264_0003`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_dmc3`
--
ALTER TABLE `toc_status_dmc3`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_iss_cariprazine`
--
ALTER TABLE `toc_status_iss_cariprazine`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_prma`
--
ALTER TABLE `toc_status_prma`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_r101`
--
ALTER TABLE `toc_status_r101`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_pwd`
--
ALTER TABLE `user_pwd`
 ADD PRIMARY KEY (`name`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `cplist_1264_0003`
--
ALTER TABLE `cplist_1264_0003`
MODIFY `sortorder` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `pgm_hist_1264_0003`
--
ALTER TABLE `pgm_hist_1264_0003`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=102;
--
-- AUTO_INCREMENT for table `pgm_hist_dmc3`
--
ALTER TABLE `pgm_hist_dmc3`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pgm_hist_iss_cariprazine`
--
ALTER TABLE `pgm_hist_iss_cariprazine`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pgm_hist_prma`
--
ALTER TABLE `pgm_hist_prma`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pgm_hist_r101`
--
ALTER TABLE `pgm_hist_r101`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=243;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1295;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12373;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `study_allocation`
--
ALTER TABLE `study_allocation`
ADD CONSTRAINT `study_allocation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`),
ADD CONSTRAINT `study_allocation_ibfk_2` FOREIGN KEY (`study_id`) REFERENCES `study_info` (`study_id`);

--
-- Constraints for table `study_info`
--
ALTER TABLE `study_info`
ADD CONSTRAINT `study_info_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client_info` (`client_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
