-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 10, 2016 at 08:36 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toc_dbv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_study_001_2016_05_18_08_47_49`
--

CREATE TABLE IF NOT EXISTS `bk_toc_study_001_2016_05_18_08_47_49` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_study_001_2016_05_18_08_47_49`
--

INSERT INTO `bk_toc_study_001_2016_05_18_08_47_49` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_study_001_2016_06_01_15_39_22`
--

CREATE TABLE IF NOT EXISTS `bk_toc_study_001_2016_06_01_15_39_22` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_study_001_2016_06_01_15_39_22`
--

INSERT INTO `bk_toc_study_001_2016_06_01_15_39_22` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations ', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102080000', '01', '02', '08', '00', '00', ' ', 'Table', '', 'demo entry', 'pgm/ctr/demopgm.sas', 'demopgm.sas', 'T1', 'pgm/ctr/demopgm.sas', 'demopgm_t1.lst', 'demopgm.log'),
('study_001', '0102090000', '01', '02', '09', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS2', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
  `client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_001`
--

CREATE TABLE IF NOT EXISTS `cplist_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_002`
--

CREATE TABLE IF NOT EXISTS `cplist_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_001`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_001` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=278 ;

--
-- Dumping data for table `pgm_hist_study_001`
--

INSERT INTO `pgm_hist_study_001` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(44, '0101010000', 'disp.sas', '2016-05-23 12:02:52', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_12_02_52_disp.sas'),
(48, '', 'disp.sas', '2016-05-23 12:51:33', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_12_51_33_disp.sas'),
(49, '0101010000', 'disp.sas', '2016-05-23 12:55:30', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_23_12_55_30_disp.sas'),
(61, '0101010000', 'disp.sas', '2016-05-23 13:46:36', 'dummy_user1', '', 'Validated', '2016_05_23_13_46_36_disp.sas'),
(62, '0101010000', 'disp.sas', '2016-05-23 13:47:33', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_13_47_33_disp.sas'),
(66, '0101010000', 'disp.sas', '2016-05-23 14:26:14', 'dummy_user2', 'Uploaded', 'In Development', '2016_05_23_14_26_14_disp.sas'),
(69, '0101010000', 'disp.sas', '2016-05-23 14:27:19', 'dummy_user2', '', 'Validated', '2016_05_23_14_27_19_disp.sas'),
(70, '0101010000', 'disp.sas', '2016-05-23 14:27:28', 'dummy_user2', 'qa', 'In Development', '2016_05_23_14_27_28_disp.sas'),
(71, '0101010000', 'disp.sas', '2016-05-23 17:26:52', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_17_26_52_disp.sas'),
(72, '0101010000', 'disp.sas', '2016-05-23 18:30:42', 'dummy_user1', '', 'Validated', '2016_05_23_18_30_42_disp.sas'),
(73, '0101010000', 'disp.sas', '2016-05-23 18:31:14', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_18_31_14_disp.sas'),
(74, '0101010000', 'disp.sas', '2016-05-24 08:42:51', 'dummy_user2', 'qa', 'In Development', '2016_05_24_08_42_51_disp.sas'),
(75, '0101010000', 'disp.sas', '2016-05-24 08:44:54', 'dummy_user2', '', 'Validated', '2016_05_24_08_44_54_disp.sas'),
(76, '0101010000', 'disp.sas', '2016-05-24 08:53:06', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_08_53_06_disp.sas'),
(77, '0101010000', 'disp.sas', '2016-05-24 09:19:38', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_09_19_38_disp.sas'),
(78, '0101040000', 'scrandtrt.sas', '2016-05-24 10:05:36', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_10_05_36_scrandtrt.sas'),
(79, '0101010000', 'disp.sas', '2016-05-24 11:07:57', 'dummy_user2', '', 'Validated', '2016_05_24_11_07_57_disp.sas'),
(80, '0101010000', 'disp.sas', '2016-05-24 11:10:50', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_11_10_50_disp.sas'),
(81, '0101010000', 'disp.sas', '2016-05-24 11:33:19', 'dummy_user1', '', 'Validated', '2016_05_24_11_33_19_disp.sas'),
(82, '0101010000', 'disp.sas', '2016-05-24 11:33:58', 'dummy_user1', 'ss', 'In Development', '2016_05_24_11_33_58_disp.sas'),
(83, '0101010000', 'disp.sas', '2016-05-24 11:42:00', 'dummy_user1', '', 'Validated', '2016_05_24_11_42_00_disp.sas'),
(84, '0101010000', 'disp.sas', '2016-05-24 11:42:36', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_11_42_36_disp.sas'),
(85, '0101040000', 'scrandtrt.sas', '2016-05-24 11:49:55', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_11_49_55_scrandtrt.sas'),
(86, '0101040000', 'scrandtrt.sas', '2016-05-24 11:50:03', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_11_50_03_scrandtrt.sas'),
(87, '0101040000', 'scrandtrt.sas', '2016-05-24 11:50:31', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_11_50_31_scrandtrt.sas'),
(88, '0101010000', 'disp.sas', '2016-05-24 12:04:35', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_12_04_35_disp.sas'),
(89, '0101010000', 'disp.sas', '2016-05-24 12:43:51', 'dummy_user1', '', 'Validated', '2016_05_24_12_43_51_disp.sas'),
(90, '0101030000', 'disp2.sas', '2016-05-24 12:45:50', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_12_45_50_disp2.sas'),
(91, '0101030000', 'disp2.sas', '2016-05-24 15:06:27', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_15_06_27_disp2.sas'),
(92, '0101030000', 'disp2.sas', '2016-05-24 15:13:21', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_15_13_21_disp2.sas'),
(93, '0101030000', 'disp2.sas', '2016-05-24 15:26:50', 'dummy_user1', '', 'Validated', '2016_05_24_15_26_50_disp2.sas'),
(94, '0101010000', 'disp.sas', '2016-05-24 16:23:39', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_16_23_39_disp.sas'),
(95, '0101030000', 'disp2.sas', '2016-05-24 18:01:28', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_18_01_28_disp2.sas'),
(96, '0101010000', 'disp.sas', '2016-05-24 18:12:47', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_18_12_48_disp.sas'),
(97, '0101010000', 'disp.sas', '2016-05-25 10:23:11', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_10_23_11_disp.sas'),
(98, '0101010000', 'disp.sas', '2016-05-25 11:27:01', 'dummy_user2', '', 'Validated', '2016_05_25_11_27_01_disp.sas'),
(99, '0101010000', 'disp.sas', '2016-05-25 11:37:03', 'dummy_user1', '', 'Validated', '2016_05_25_11_37_03_disp.sas'),
(100, '0101010000', 'disp.sas', '2016-05-25 11:37:11', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_11_37_11_disp.sas'),
(101, '0101010000', 'disp.sas', '2016-05-25 11:37:14', 'dummy_user1', '', 'Validated', '2016_05_25_11_37_14_disp.sas'),
(102, '0101010000', 'disp.sas', '2016-05-25 11:37:23', 'dummy_user1', '', 'To Be validated', '2016_05_25_11_37_23_disp.sas'),
(103, '0101010000', 'disp.sas', '2016-05-25 11:37:41', 'dummy_user1', '', 'Validated', '2016_05_25_11_37_41_disp.sas'),
(104, '0101010000', 'disp.sas', '2016-05-25 11:37:52', 'dummy_user1', '', 'Validated', '2016_05_25_11_37_52_disp.sas'),
(105, '0101010000', 'disp.sas', '2016-05-25 11:38:03', 'dummy_user1', '', 'In Development', '2016_05_25_11_38_03_disp.sas'),
(106, '0101010000', 'disp.sas', '2016-05-25 11:39:26', 'dummy_user1', '', 'In Development', '2016_05_25_11_39_26_disp.sas'),
(107, '0101010000', 'disp.sas', '2016-05-25 11:41:02', 'dummy_user2', '', 'Validated', '2016_05_25_11_41_02_disp.sas'),
(108, '0101010000', 'disp.sas', '2016-05-25 11:43:07', 'dummy_user2', '', 'Validated', '2016_05_25_11_43_07_disp.sas'),
(109, '0101010000', 'disp.sas', '2016-05-25 11:52:20', 'dummy_user1', '', 'In Development', '2016_05_25_11_52_20_disp.sas'),
(110, '0101010000', 'disp.sas', '2016-05-25 11:53:30', 'dummy_user1', '', 'In Development', '2016_05_25_11_53_30_disp.sas'),
(111, '0101010000', 'disp.sas', '2016-05-25 11:55:50', 'dummy_user1', '', 'In Development', '2016_05_25_11_55_50_disp.sas'),
(112, '0101010000', 'disp.sas', '2016-05-25 11:57:22', 'dummy_user2', '', 'Validated', '2016_05_25_11_57_22_disp.sas'),
(113, '0101010000', 'disp.sas', '2016-05-25 12:00:02', 'dummy_user2', '', 'Validated', '2016_05_25_12_00_02_disp.sas'),
(114, '0101010000', 'disp.sas', '2016-05-25 12:00:56', 'dummy_user2', '', 'Validated', '2016_05_25_12_00_56_disp.sas'),
(115, '0101010000', 'disp.sas', '2016-05-25 12:01:52', 'dummy_user2', '', 'Validated', '2016_05_25_12_01_53_disp.sas'),
(116, '0101010000', 'disp.sas', '2016-05-25 12:03:18', 'dummy_user2', '', 'Validated', '2016_05_25_12_03_18_disp.sas'),
(117, '0101010000', 'disp.sas', '2016-05-25 12:04:47', 'dummy_user2', '', 'Validated', '2016_05_25_12_04_47_disp.sas'),
(118, '0101010000', 'disp.sas', '2016-05-25 12:05:16', 'dummy_user2', '', 'Validated', '2016_05_25_12_05_16_disp.sas'),
(119, '0101010000', 'disp.sas', '2016-05-25 14:38:27', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_14_38_27_disp.sas'),
(120, '0101010000', 'disp.sas', '2016-05-25 14:39:01', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_14_39_01_disp.sas'),
(121, '0101010000', 'disp.sas', '2016-05-25 14:40:49', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_14_40_49_disp.sas'),
(122, '0102080000', 'ipvsummary.sas', '2016-05-25 15:00:44', 'dummy_user1', 'First Uploaded', 'In Development', '2016_05_25_15_00_44_ipvsummary.sas'),
(123, '0102080000', 'ipvsummary.sas', '2016-05-25 15:01:57', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_01_57_ipvsummary.sas'),
(124, '0102080000', 'ipvsummary.sas', '2016-05-25 15:03:35', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_03_35_ipvsummary.sas'),
(125, '0102080000', 'ipvsummary.sas', '2016-05-25 15:04:19', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_04_19_ipvsummary.sas'),
(126, '0102080000', 'ipvsummary.sas', '2016-05-25 15:10:23', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_10_23_ipvsummary.sas'),
(127, '0102080000', 'ipvsummary.sas', '2016-05-25 15:13:09', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_13_09_ipvsummary.sas'),
(128, '0102080000', 'ipvsummary.sas', '2016-05-25 15:18:50', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_18_50_ipvsummary.sas'),
(129, '0101040000', 'scrandtrt.sas', '2016-05-25 15:21:22', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_21_22_scrandtrt.sas'),
(130, '0102080000', 'ipvsummary.sas', '2016-05-25 15:30:16', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_30_16_ipvsummary.sas'),
(131, '0102080000', 'ipvsummary.sas', '2016-05-25 15:31:18', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_31_19_ipvsummary.sas'),
(132, '0102080000', 'ipvsummary.sas', '2016-05-25 15:33:58', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_33_58_ipvsummary.sas'),
(133, '0102080000', 'ipvsummary.sas', '2016-05-25 15:34:15', 'dummy_user1', '', 'Validated', '2016_05_25_15_34_15_ipvsummary.sas'),
(134, '0102080000', 'ipvsummary.sas', '2016-05-25 15:34:54', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_34_54_ipvsummary.sas'),
(135, '0102080000', 'ipvsummary.sas', '2016-05-25 15:35:19', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_35_19_ipvsummary.sas'),
(136, '0102080000', 'ipvsummary.sas', '2016-05-25 15:35:29', 'dummy_user1', '', 'Validated', '2016_05_25_15_35_29_ipvsummary.sas'),
(137, '0102080000', 'ipvsummary.sas', '2016-05-25 15:36:44', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_36_45_ipvsummary.sas'),
(138, '0102080000', 'ipvsummary.sas', '2016-05-25 15:36:55', 'dummy_user1', '', 'Validated', '2016_05_25_15_36_55_ipvsummary.sas'),
(139, '0102080000', 'ipvsummary.sas', '2016-05-25 15:38:21', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_38_22_ipvsummary.sas'),
(140, '0102080000', 'ipvsummary.sas', '2016-05-25 15:38:52', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_38_52_ipvsummary.sas'),
(141, '0102080000', 'ipvsummary.sas', '2016-05-25 15:39:45', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_39_45_ipvsummary.sas'),
(142, '0102080000', 'ipvsummary.sas', '2016-05-25 15:40:01', 'dummy_user1', '', 'Validated', '2016_05_25_15_40_01_ipvsummary.sas'),
(143, '0102080000', 'ipvsummary.sas', '2016-05-25 15:40:28', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_40_28_ipvsummary.sas'),
(144, '0102080000', 'ipvsummary.sas', '2016-05-25 15:41:36', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_41_37_ipvsummary.sas'),
(145, '0102080000', 'ipvsummary.sas', '2016-05-25 15:41:49', 'dummy_user1', '', 'Validated', '2016_05_25_15_41_50_ipvsummary.sas'),
(146, '0102080000', 'ipvsummary.sas', '2016-05-25 15:42:37', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_42_38_ipvsummary.sas'),
(147, '0102080000', 'ipvsummary.sas', '2016-05-25 15:43:17', 'dummy_user1', '', 'Validated', '2016_05_25_15_43_17_ipvsummary.sas'),
(148, '0102080000', 'ipvsummary.sas', '2016-05-25 15:43:46', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_43_46_ipvsummary.sas'),
(149, '0101010000', 'disp.sas', '2016-05-25 15:45:38', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_45_38_disp.sas'),
(150, '0101010000', 'disp.sas', '2016-05-25 15:46:15', 'dummy_user1', '', 'Validated', '2016_05_25_15_46_15_disp.sas'),
(151, '0101010000', 'disp.sas', '2016-05-25 15:46:23', 'dummy_user1', 'Run Program', 'Program running', '2016_05_25_15_46_23_disp.sas'),
(152, '0101010000', 'disp.sas', '2016-05-25 15:46:25', 'dummy_user1', '', 'Validated', '2016_05_25_15_46_25_disp.sas'),
(153, '0101010000', 'disp.sas', '2016-05-25 15:46:51', 'dummy_user1', 'Edited', 'In Development', '2016_05_25_15_46_51_disp.sas'),
(154, '0102080000', 'ipvsummary.sas', '2016-05-25 15:59:43', 'dummy_user1', '', 'Validated', '2016_05_25_15_59_43_ipvsummary.sas'),
(155, '0101010000', 'disp.sas', '2016-05-25 22:04:33', 'dummy_user2', 'Run Program', 'Program running', '2016_05_25_22_04_33_disp.sas'),
(156, '0101010000', 'disp.sas', '2016-05-25 22:04:47', 'dummy_user2', '', 'Validated', '2016_05_25_22_04_47_disp.sas'),
(157, '0102080000', 'ipvsummary.sas', '2016-05-25 22:06:55', 'dummy_user2', '', 'To Be validated', '2016_05_25_22_06_55_ipvsummary.sas'),
(158, '0101010000', 'disp.sas', '2016-05-25 22:45:20', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_45_20_disp.sas'),
(159, '0101010000', 'disp.sas', '2016-05-25 22:45:58', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_45_58_disp.sas'),
(160, '0101010000', 'disp.sas', '2016-05-25 22:47:13', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_47_13_disp.sas'),
(161, '0101010000', 'disp.sas', '2016-05-25 22:48:11', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_48_11_disp.sas'),
(162, '0101010000', 'disp.sas', '2016-05-25 22:49:12', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_49_12_disp.sas'),
(163, '0101010000', 'disp.sas', '2016-05-25 22:50:35', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_50_35_disp.sas'),
(164, '0101010000', 'disp.sas', '2016-05-25 22:52:07', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_52_07_disp.sas'),
(165, '0101010000', 'disp.sas', '2016-05-25 22:52:50', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_52_50_disp.sas'),
(166, '0101010000', 'disp.sas', '2016-05-25 22:53:58', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_53_58_disp.sas'),
(167, '0101010000', 'disp.sas', '2016-05-25 22:54:59', 'dummy_user2', 'Edited', 'In Development', '2016_05_25_22_54_59_disp.sas'),
(168, '0101030000', 'disp2.sas', '2016-05-26 18:06:57', 'dummy_user2', 'Run Program', 'Program running', '2016_05_26_18_06_57_disp2.sas'),
(169, '0102080000', 'ipvsummary.sas', '2016-05-26 18:09:58', 'dummy_user2', 'Run Program', 'Program running', '2016_05_26_18_09_58_ipvsummary.sas'),
(170, '0102080000', 'ipvsummary.sas', '2016-05-27 11:19:17', 'dummy_user1', 'Edited', 'In Development', '2016_05_27_11_19_17_ipvsummary.sas'),
(171, '0102080000', 'ipvsummary.sas', '2016-05-27 12:16:54', 'dummy_user2', 'Run Program', 'Program running', '2016_05_27_12_16_55_ipvsummary.sas'),
(172, '0102080000', 'ipvsummary.sas', '2016-05-27 13:57:08', 'dummy_user1', 'Run Program', 'Program running', '2016_05_27_13_57_08_ipvsummary.sas'),
(173, '0102080000', 'ipvsummary.sas', '2016-05-29 12:25:55', 'dummy_user1', 'Edited', 'In Development', '2016_05_29_12_25_55_ipvsummary.sas'),
(174, '0102080000', 'ipvsummary.sas', '2016-05-29 12:26:16', 'dummy_user1', 'Run Program', 'Program running', '2016_05_29_12_26_16_ipvsummary.sas'),
(175, '0102080000', 'ipvsummary.sas', '2016-05-29 15:06:02', 'dummy_user1', 'Run Program', 'Program running', '2016_05_29_15_06_02_ipvsummary.sas'),
(176, '0102080000', 'ipvsummary.sas', '2016-05-30 13:02:29', 'dummy_user1', '', 'Validated', '2016_05_30_13_02_29_ipvsummary.sas'),
(177, '0102080000', 'ipvsummary.sas', '2016-05-30 13:02:55', 'dummy_user1', 'Edited', 'In Development', '2016_05_30_13_02_55_ipvsummary.sas'),
(178, '0101050000', 'inv.sas', '2016-05-30 19:54:22', '', 'First Uploaded', 'In Development', '2016_05_30_23_54_22_inv.sas'),
(179, '0101050000', 'inv.sas', '2016-05-30 19:59:02', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_30_23_59_02_inv.sas'),
(180, '0101010000', 'disp.sas', '2016-05-31 10:20:40', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_20_40_disp.sas'),
(181, '0101030000', 'disp2.sas', '2016-05-31 10:21:58', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_21_58_disp2.sas'),
(182, '0101010000', 'disp.sas', '2016-05-31 10:38:38', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_38_38_disp.sas'),
(183, '0101030000', 'disp2.sas', '2016-05-31 10:38:54', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_38_54_disp2.sas'),
(184, '0101010000', 'disp.sas', '2016-05-31 10:39:47', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_39_47_disp.sas'),
(185, '0101030000', 'disp2.sas', '2016-05-31 10:48:21', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_48_21_disp2.sas'),
(186, '0101010000', 'disp.sas', '2016-05-31 10:54:19', 'dummy_user1', 'Edited', 'In Development', '2016_05_31_10_54_19_disp.sas'),
(187, '0101010000', 'disp.sas', '2016-05-31 10:54:52', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_54_52_disp.sas'),
(188, '0101010000', 'disp.sas', '2016-05-31 10:56:40', 'dummy_user1', 'Edited', 'In Development', '2016_05_31_10_56_41_disp.sas'),
(189, '0101010000', 'disp.sas', '2016-05-31 10:57:05', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_57_05_disp.sas'),
(190, '0101040000', 'scrandtrt.sas', '2016-05-31 10:58:12', 'dummy_user1', 'Edited', 'In Development', '2016_05_31_10_58_12_scrandtrt.sas'),
(191, '0101030000', 'disp2.sas', '2016-05-31 10:58:47', 'dummy_user1', 'Edited', 'In Development', '2016_05_31_10_58_47_disp2.sas'),
(192, '0102080000', 'ipvsummary.sas', '2016-05-31 10:59:22', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_10_59_22_ipvsummary.sas'),
(193, '0101030000', 'disp2.sas', '2016-05-31 11:01:05', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_11_01_05_disp2.sas'),
(194, '0101030000', 'disp2.sas', '2016-05-31 11:01:32', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_11_01_32_disp2.sas'),
(195, '0101010000', 'disp.sas', '2016-05-31 11:18:48', 'dummy_user1', 'passed', 'Validated', '2016_05_31_11_18_49_disp.sas'),
(196, '0101010000', 'disp.sas', '2016-05-31 07:19:37', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_31_11_19_37_disp.sas'),
(197, '0101010000', 'disp.sas', '2016-05-31 11:30:46', 'dummy_user1', 'passed', 'Validated', '2016_05_31_11_30_46_disp.sas'),
(198, '0101010000', 'disp.sas', '2016-05-31 11:44:09', 'dummy_user1', 'passed', 'Validated', '2016_05_31_11_44_09_disp.sas'),
(199, '0101010000', 'disp.sas', '2016-05-31 11:44:36', 'dummy_user1', 'passed', 'Validated', '2016_05_31_11_44_36_disp.sas'),
(200, '0101010000', 'disp.sas', '2016-05-31 11:47:11', 'dummy_user1', 'passed', 'Validated', '2016_05_31_11_47_11_disp.sas'),
(201, '0101010000', 'disp.sas', '2016-05-31 11:47:46', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_11_47_46_disp.sas'),
(202, '0101010000', 'disp.sas', '2016-05-31 11:50:26', 'dummy_user1', 'passed', 'Validated', '2016_05_31_11_50_26_disp.sas'),
(203, '0101010000', 'disp.sas', '2016-05-31 07:54:17', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_31_11_54_17_disp.sas'),
(204, '0101010000', 'disp.sas', '2016-05-31 12:24:41', 'dummy_user1', 'Run Program', 'Program running', '2016_05_31_12_24_42_disp.sas'),
(205, '0101010000', 'disp.sas', '0000-00-00 00:00:00', 'dummy_user1', 'Passed ', 'Validated', '2016_05_31_20_33_41_disp.sas'),
(206, '0102080000', 'ipvsummary.sas', '2016-05-31 20:43:43', 'dummy_user1', '', 'Validated', '2016_05_31_20_43_43_ipvsummary.sas'),
(207, '0102080000', 'ipvsummary.sas', '2016-06-01 08:19:29', 'dummy_user1', '', 'To Be validated', '2016_06_01_08_19_29_ipvsummary.sas'),
(208, '0102080000', 'ipvsummary.sas', '2016-06-01 09:49:49', 'dummy_user1', 'passed', 'Validated', '2016_06_01_09_49_49_ipvsummary.sas'),
(209, '0101010000', 'disp.sas', '2016-06-01 11:41:50', 'dummy_user1', 'Run Program', 'Program running', '2016_06_01_11_41_50_disp.sas'),
(210, '0101010000', 'disp.sas', '2016-06-01 11:42:09', 'dummy_user1', 'validated again ', 'Validated', '2016_06_01_11_42_09_disp.sas'),
(211, '0101010000', 'disp.sas', '2016-06-01 11:43:12', 'dummy_user1', 'Edited', 'In Development', '2016_06_01_11_43_12_disp.sas'),
(212, '0101010000', 'disp.sas', '2016-06-01 11:50:38', 'dummy_user1', 'Run Program', 'Program running', '2016_06_01_11_50_38_disp.sas'),
(213, '0101010000', 'disp.sas', '2016-06-01 07:51:00', 'dummy_user1', 'Uploaded', 'In Development', '2016_06_01_11_51_00_disp.sas'),
(214, '0101010000', 'disp.sas', '2016-06-01 11:51:24', 'dummy_user1', 'Run Program', 'Program running', '2016_06_01_11_51_24_disp.sas'),
(215, '0101010000', 'disp.sas', '2016-06-01 11:51:31', 'dummy_user1', '', 'Validated', '2016_06_01_11_51_32_disp.sas'),
(216, '0101010000', 'disp.sas', '2016-06-01 11:52:05', 'dummy_user1', 'Edited', 'In Development', '2016_06_01_11_52_05_disp.sas'),
(217, '0101010000', 'disp.sas', '2016-06-08 12:02:40', 'dummyuser3', 'Run Program', 'Program running', '2016_06_08_12_02_40_disp.sas'),
(218, '0101010000', 'disp.sas', '2016-06-08 12:03:37', 'dummyuser3', '', 'Validated', '2016_06_08_12_03_37_disp.sas'),
(219, '0101010000', 'disp.sas', '2016-06-09 16:26:18', 'dummy_user1', 'Run Program', 'Program running', '2016_06_09_16_26_18_disp.sas'),
(220, '0101020000', 'disp.sas', '2016-06-09 16:26:21', 'dummy_user1', 'Run Program', 'Program running', '2016_06_09_16_26_21_disp.sas'),
(221, '0101010000', 'disp.sas', '2016-06-09 21:55:24', 'dummy_user1', 'failed', 'In Development', '2016_06_09_21_55_24_disp.sas'),
(222, '0102030000', 'ipvsummary.sas', '2016-06-13 14:05:19', 'dummy_user1', '', 'In Development', '2016_06_13_14_05_19_ipvsummary.sas'),
(223, '0102030000', 'ipvsummary.sas', '2016-06-13 14:05:49', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_05_49_ipvsummary.sas'),
(224, '0102030000', 'ipvsummary.sas', '2016-06-13 10:10:36', 'dummy_user1', 'Uploaded', 'In Development', '2016_06_13_14_10_36_ipvsummary.sas'),
(225, '0102030000', 'ipvsummary.sas', '2016-06-13 14:10:47', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_10_47_ipvsummary.sas'),
(226, '0102030000', 'ipvsummary.sas', '2016-06-13 14:11:52', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_11_52_ipvsummary.sas'),
(227, '0102030000', 'ipvsummary.sas', '2016-06-13 14:12:03', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_12_03_ipvsummary.sas'),
(228, '0102030000', 'ipvsummary.sas', '2016-06-13 14:14:20', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_14_21_ipvsummary.sas'),
(229, '0102030000', 'ipvsummary.sas', '2016-06-13 14:15:34', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_15_34_ipvsummary.sas'),
(230, '0101010000', 'disp.sas', '2016-06-13 14:18:51', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_18_51_disp.sas'),
(231, '0102030000', 'ipvsummary.sas', '2016-06-13 14:27:17', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_27_17_ipvsummary.sas'),
(232, '0101010000', 'disp.sas', '2016-06-13 14:28:00', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_28_00_disp.sas'),
(233, '0102030000', 'ipvsummary.sas', '2016-06-13 14:30:13', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_30_14_ipvsummary.sas'),
(234, '0102030000', 'ipvsummary.sas', '2016-06-13 14:37:09', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_37_09_ipvsummary.sas'),
(235, '0101010000', 'disp.sas', '2016-06-13 14:37:56', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_37_56_disp.sas'),
(236, '0101020000', 'disp.sas', '2016-06-13 14:37:59', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_38_00_disp.sas'),
(237, '0101030000', 'disp2.sas', '2016-06-13 14:38:06', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_38_06_disp2.sas'),
(238, '0101010000', 'disp.sas', '2016-06-13 14:41:10', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_41_10_disp.sas'),
(239, '0101020000', 'disp.sas', '2016-06-13 14:41:14', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_41_14_disp.sas'),
(240, '0101030000', 'disp2.sas', '2016-06-13 14:41:18', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_41_19_disp2.sas'),
(241, '0101010000', 'disp.sas', '2016-06-13 14:42:44', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_42_44_disp.sas'),
(242, '0101020000', 'disp.sas', '2016-06-13 14:42:48', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_42_48_disp.sas'),
(243, '0101030000', 'disp2.sas', '2016-06-13 14:42:53', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_42_53_disp2.sas'),
(244, '0101010000', 'disp.sas', '2016-06-13 14:44:32', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_44_32_disp.sas'),
(245, '0101020000', 'disp.sas', '2016-06-13 14:44:35', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_44_35_disp.sas'),
(246, '0101030000', 'disp2.sas', '2016-06-13 14:44:40', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_44_40_disp2.sas'),
(247, '0102030000', 'ipvsummary.sas', '2016-06-13 10:45:50', 'dummy_user1', 'Uploaded', 'In Development', '2016_06_13_14_45_50_ipvsummary.sas'),
(248, '0102030000', 'ipvsummary.sas', '2016-06-13 14:46:08', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_46_08_ipvsummary.sas'),
(249, '0101010000', 'disp.sas', '2016-06-13 14:50:14', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_50_14_disp.sas'),
(250, '0101020000', 'disp.sas', '2016-06-13 14:50:17', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_50_17_disp.sas'),
(251, '0101030000', 'disp2.sas', '2016-06-13 14:50:21', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_50_22_disp2.sas'),
(252, '0101010000', 'disp.sas', '2016-06-13 14:59:10', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_59_10_disp.sas'),
(253, '0101020000', 'disp.sas', '2016-06-13 14:59:14', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_59_14_disp.sas'),
(254, '0101030000', 'disp2.sas', '2016-06-13 14:59:18', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_14_59_18_disp2.sas'),
(255, '0101010000', 'disp.sas', '2016-06-13 15:04:12', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_04_12_disp.sas'),
(256, '0101020000', 'disp.sas', '2016-06-13 15:04:16', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_04_16_disp.sas'),
(257, '0101030000', 'disp2.sas', '2016-06-13 15:04:21', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_04_21_disp2.sas'),
(258, '0101010000', 'disp.sas', '2016-06-13 15:05:31', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_05_31_disp.sas'),
(259, '0101020000', 'disp.sas', '2016-06-13 15:05:35', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_05_35_disp.sas'),
(260, '0101030000', 'disp2.sas', '2016-06-13 15:05:40', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_05_41_disp2.sas'),
(261, '0101010000', 'disp.sas', '2016-06-13 15:13:49', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_13_49_disp.sas'),
(262, '0101020000', 'disp.sas', '2016-06-13 15:13:52', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_13_52_disp.sas'),
(263, '0101010000', 'disp.sas', '2016-06-13 15:15:38', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_15_38_disp.sas'),
(264, '0101020000', 'disp.sas', '2016-06-13 15:15:41', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_15_41_disp.sas'),
(265, '0101010000', 'disp.sas', '2016-06-13 15:17:35', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_17_35_disp.sas'),
(266, '0101020000', 'disp.sas', '2016-06-13 15:17:39', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_17_39_disp.sas'),
(267, '0102030000', 'ipvsummary.sas', '2016-06-13 15:18:45', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_18_45_ipvsummary.sas'),
(268, '0101010000', 'disp.sas', '2016-06-13 15:23:17', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_23_17_disp.sas'),
(269, '0101020000', 'disp.sas', '2016-06-13 15:23:21', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_23_21_disp.sas'),
(270, '0102030000', 'ipvsummary.sas', '2016-06-13 15:28:47', 'dummy_user1', 'Run Program', 'Program running', '2016_06_13_15_28_47_ipvsummary.sas'),
(271, '0102030000', 'ipvsummary.sas', '2016-06-14 07:46:11', 'dummy_user1', 'Uploaded', 'In Development', '2016_06_14_11_46_11_ipvsummary.sas'),
(272, '0102030000', 'ipvsummary.sas', '2016-06-14 11:46:23', 'dummy_user1', 'Run Program', 'Program running', '2016_06_14_11_46_23_ipvsummary.sas'),
(273, '0102030000', 'ipvsummary.sas', '2016-06-23 17:13:09', 'dummy_user1', 'Run Program', 'Program running', '2016_06_23_17_13_09_ipvsummary.sas'),
(274, '0102030000', 'ipvsummary.sas', '2016-06-23 17:18:16', 'dummy_user1', 'Run Program', 'Program running', '2016_06_23_17_18_16_ipvsummary.sas'),
(275, '0102030000', 'ipvsummary.sas', '2016-06-23 17:20:04', 'dummy_user1', 'Run Program', 'Program running', '2016_06_23_17_20_04_ipvsummary.sas'),
(276, '0102030000', 'ipvsummary.sas', '2016-06-23 17:20:47', 'dummy_user1', 'Run Program', 'Program running', '2016_06_23_17_20_48_ipvsummary.sas'),
(277, '0102030000', 'ipvsummary.sas', '2016-06-23 17:25:13', 'dummy_user1', 'Run Program', 'Program running', '2016_06_23_17_25_13_ipvsummary.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_002`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_002` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `pgm_hist_study_002`
--

INSERT INTO `pgm_hist_study_002` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(1, '0101010000', 'tpopu.sas', '2016-05-18 09:07:17', 'dummy_user2', 'First Uploaded', 'In Development', '2016_05_18_09_07_17_tpopu.sas'),
(2, '0102010100', 'demo.sas', '2016-05-21 19:24:20', 'dummy_user2', 'First Uploaded', 'In Development', '2016_05_21_19_24_20_demo.sas'),
(3, '0101010000', 'tpopu.sas', '2016-08-02 18:56:45', 'dummy_user1', 'Edited', 'In Development', '2016_08_02_18_56_45_tpopu.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_lock_status`
--

CREATE TABLE IF NOT EXISTS `pgm_lock_status` (
  `sortorder` varchar(10) DEFAULT NULL,
  `id` varchar(200) NOT NULL,
  `study` varchar(20) NOT NULL,
  `pgmloc` varchar(200) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `macaddr` varchar(100) NOT NULL,
  `lock_date_time` datetime NOT NULL,
  `pgmloc_ori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pgm_lock_status`
--

INSERT INTO `pgm_lock_status` (`sortorder`, `id`, `study`, `pgmloc`, `pgmname`, `username`, `password`, `macaddr`, `lock_date_time`, `pgmloc_ori`) VALUES
('0101010000', '043d9e1e24dfe867585f422d3b4d2841f0616136', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:51:07', ''),
(NULL, '05d99b5607610eb8be7742dfbf48ae8718773b8f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:45:20', ''),
(NULL, '086e909a9b0ebd7f6dc13e37a57e0d56a9ede9f4', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 11:10:56', ''),
('0101010000', '0af7aef5b972ec95d73847c899e8a8bf409e4a8b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:44:03', 'pgm/ctr/disp.sas'),
('0101010000', '0f595261ec53c35c44ed160d75a644f3e180ec32', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 18:30:53', 'pgm/ctr/disp.sas'),
('0101010000', '0f899e633cf03ba3045283b2b9cc4f4d420f3243', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:44:57', 'pgm/ctr/disp.sas'),
('0102080000', '123a2dfa0b34fe7f424c9cea6725a45a1cd8f9c4', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:40:09', 'pgm/ctr/ipvsummary.sas'),
('0102080000', '1afb3b3cb843007a97daf0efce68b17f4c2778f5', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:38:32', 'pgm/ctr/ipvsummary.sas'),
(NULL, '1b4085cd5b9a5fc5720abf663fdf8d84df7f93ea', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:46:11', ''),
('0101030000', '1d97eac18ae5808ac77be93561fdce02c1a1d3d5', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-29 13:18:22', 'pgm/ctr/disp2.sas'),
('0101040000', '236ae425bb59271bc74465123330b7b4e5c6ef2b', 'study_001', 'temp/sas/scrandtrt.sas', 'scrandtrt.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:57:33', 'pgm/ctr/scrandtrt.sas'),
('0101010000', '2a86af8e7c50ed9a896104b2a3f0175fa8a9bf3b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-06-01 11:42:22', 'pgm/ctr/disp.sas'),
('0102080000', '2c079721a436d2d1d1b85dc341b14676867fa563', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:18:18', 'pgm/ctr/ipvsummary.sas'),
(NULL, '2ceb228aa3e0de4a4fd0ae915ac6b7a8130327ff', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:52:59', ''),
('0101010000', '30a4c5a791d7b8da8c8297a5cca6e0e88e83bc35', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:56:11', 'pgm/ctr/disp.sas'),
('0102080000', '3302fe5517beb01e99792afc74410dd4e577d2aa', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:30:25', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '34827d0468598094d80b1ce7f45d79aa18abdb2b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:44:24', 'pgm/ctr/disp.sas'),
('0101010000', '34b20ff3fc4f7f3c32e300fe4d0e166fc94a9c2f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:40:10', 'temp/sas/disp.sas'),
('0101010000', '350a40277a6de20e01abbf1731c927f7aa66f726', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:17:50', 'pgm/ctr/disp.sas'),
('0102080000', '362bde0e733e99226f0f9327ffbb265c1ca8d890', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:35:37', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '3834ab13e64602fb9f55a0ea944b35ff50c5015a', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:44:08', 'pgm/ctr/disp.sas'),
('0101010000', '3d7b1da240bef8c55e3d38d1ee2052e94e0c0457', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:44:03', 'pgm/ctr/disp.sas'),
('0101010000', '401f3bf22e521405940e9d7753c4f61769c80e3e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:44:43', 'pgm/ctr/disp.sas'),
('0101010000', '4137dd24a750296a7da74f1124bb474e145eb261', 'study_002', 'temp/sas/tpopu.sas', 'tpopu.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-08-02 18:56:17', 'pgm/ctr/tpopu.sas'),
('0102080000', '4240ae9755c17f3ec25dfb9903db11c3b3e52606', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:39:37', 'pgm/ctr/ipvsummary.sas'),
('0102080000', '47e75146926ea7baa6e4d31267a63e8bd911f2f9', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-27 11:16:28', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '4cb6f14e6b3ca0674ddbfcd329cc245ae6171547', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:48:39', 'pgm/ctr/disp.sas'),
('0101030000', '5024987f67f78ecae7aa7ae892f501f1e25cc4fc', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:58:30', 'pgm/ctr/disp2.sas'),
('0101010000', '51e33fa113edde82fda4e2df0439df2edd53ed8f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:47:16', 'pgm/ctr/disp.sas'),
('0102080000', '5299554cf5b0d55130e36b61b06f0f19b044e7b8', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:01:15', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '52f86d559e33b54afdef02a64d46552517dccb8b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:47:29', 'pgm/ctr/disp.sas'),
(NULL, '5a42c2375fb0d6116542870d30a02fdc7612b903', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:44:09', ''),
('0101010000', '5b45cb2c42dcffe0d72173aca0bb60377cd048f8', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:26:06', 'pgm/ctr/disp.sas'),
('0101010000', '5c809f2eb97a01126114da743db9edfe7c0444b9', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:23:56', 'disp.sas'),
('0101010000', '5d6b420446152e459a78220774f0cb211fd2f329', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:38:21', ''),
(NULL, '5db341979ebc01607cd3078f1ce89cf2024f5944', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:46:49', ''),
('0101010000', '5f4089705445c9341a0745a2b4ba178f26c2ea51', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 18:12:07', 'pgm/ctr/disp.sas'),
(NULL, '5f95ed2cc2a373c9cc2f47a93668b04441cc4b40', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:50:22', ''),
(NULL, '6811e6fdedb24e5dc29d7f5066473afc8435aa5b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:56:32', ''),
('0102080000', '6956f0590ba30ca225dc08610b6ed9b2ea60359a', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:34:20', 'pgm/ctr/ipvsummary.sas'),
(NULL, '6a2506153a7ca40b782c91a9cce4ae8015423ea2', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:54:26', ''),
('0101010000', '6c41e19bc5d2deaa79bb291007444fd9cb0f7913', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 18:12:27', 'pgm/ctr/disp.sas'),
('0101010000', '7004883455adee5e936b71e8b75ff0ea1a653905', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:53:52', 'pgm/ctr/disp.sas'),
('0101010000', '70375a555aecb0b2d33cb1fd00889af131219f3a', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:33:07', 'disp.sas'),
('0101010000', '71fbf18f6d9476fa7b18d59ef51f5fe65e212862', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:49:25', 'pgm/ctr/disp.sas'),
('0102080000', '73e4639cfb88b0e63d165aa2d3b46b3181873a18', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-30 13:01:58', 'pgm/ctr/ipvsummary.sas'),
(NULL, '79393029964ab57adfb4b55006144a16e75db232', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:58:27', ''),
('0102080000', '7f04a660f39ef18835985832b56d4b178d2e9f36', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:43:07', 'pgm/ctr/ipvsummary.sas'),
('0102080000', '8b4f96957cd9b98b72bfffb213fcb6b1e66c1767', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-30 13:02:36', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '8cd5c5e13d8b1849cd28e6b58db0814689540565', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:07:55', ''),
('0101010000', '8d2aae5aed70e616a46432b3c1306a2123735cf6', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 10:22:28', 'pgm/ctr/disp.sas'),
('0101010000', '8da4c5898cac2218b2934df787d73a99716b8ffb', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:58:35', ''),
(NULL, '8f7c151da5a9ee03751de1a189b205ee319752f1', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-22 08:56:21', ''),
('0102080000', '91243d60fd4dd3aada234f7c92204942435488c6', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:02:36', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '9a0b123f2174c5bda360698e5d0e333acc049618', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:54:14', 'pgm/ctr/disp.sas'),
('0101010000', '9fb790fbab94077bf5dc560b715686a6369bf5db', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:45:34', 'pgm/ctr/disp.sas'),
('0101010000', 'a4979931b76101cb3991ca2679a77b3986d64320', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 08:52:42', 'pgm/ctr/disp.sas'),
('0102080000', 'a526174a21f28cf62fadd22ed007fea02e284515', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:41:58', 'pgm/ctr/ipvsummary.sas'),
('0102080000', 'a6175cd96a035942a48bc2ed754c4ae9dc0235b0', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-27 11:18:29', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'a72f406c5adbd24d53da9c2632a959b2f83188dc', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:49:23', ''),
('0101010000', 'a76afb51b2e99c37385d0000bfcbd56288b03c91', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-06-01 11:51:38', 'pgm/ctr/disp.sas'),
('0101010000', 'aa08dcf1e121d65acee4b43f0b96f5619080dff2', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:39:49', 'pgm/ctr/disp.sas'),
('0101010000', 'af17df5afbeefac309f6e246cb06f0bd7e5850da', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 14:37:28', 'pgm/ctr/disp.sas'),
('0101010000', 'b07c90a394d902616cc1643ab1f20e9d86b5023c', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:46:22', 'pgm/ctr/disp.sas'),
('0102080000', 'b37a527039758b9a0e9751739de4f7211514204a', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-30 13:03:52', 'pgm/ctr/ipvsummary.sas'),
('0102080000', 'b8ac5750363c42cf914c01c0c547df1dccf46503', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-29 12:25:25', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'b8f786838c4f0122e35f99eaa701bfe904f22a13', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 14:39:49', 'pgm/ctr/disp.sas'),
('0101010000', 'ba576a7c5a90b168e71332958758368b60331ad5', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:50:54', 'pgm/ctr/disp.sas'),
(NULL, 'bb174bb5201c1f4b9c6a0477401d1c8866fe835d', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:50:16', ''),
('0101010000', 'bcee7b38f2a9208292a81105414f94f9520af90b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-08-10 10:47:43', 'pgm/ctr/disp.sas'),
('0101040000', 'c1006a8d017ef6195960d785f4e775ce3bf1d0d4', 'study_001', 'temp/sas/scrandtrt.sas', 'scrandtrt.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:21:08', 'pgm/ctr/scrandtrt.sas'),
(NULL, 'c43a68e5ae1ef10b6d6a80e8a1de767699c25c2e', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:43:41', ''),
(NULL, 'c81435218c70cf478e5785beac42cacd1933477f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:40:05', ''),
('0101010000', 'd38195922e77679e794142cb7440592cf8ba7307', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:03:07', 'pgm/ctr/disp.sas'),
('0101010000', 'd50a149ee898b0c24fe7581b159a3948ae4d5556', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:45:40', ''),
('0102080000', 'd5f499a081f43d976e893d04566c4164625fbb8d', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:09:41', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'd78652930fa4bd0603874504d81fa1c7a045da34', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:53:35', 'pgm/ctr/disp.sas'),
(NULL, 'd8c6a0ab80f4d8621c37efdce0d981e119ddc15e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 11:46:06', ''),
('0101010000', 'daffb24d252762d31fcaab1e94fdc76dc1a71ff6', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:20:53', 'disp.sas'),
('0101010000', 'dd6efca84638ae87b5f52071a18f6eb7d0ba3964', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:42:14', 'pgm/ctr/disp.sas'),
('0101010000', 'de5dde4ce338784309a0b754efec5c1c161f9d96', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:59:27', ''),
('0101010000', 'e0c47370e4ed13ea867edbdec8b0b654fc7f5bc3', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:41:29', 'pgm/ctr/disp.sas'),
('0101010000', 'e55a6b06327a0c4589968312872d223370c57637', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:09:55', 'pgm/ctr/disp.sas'),
('0101010000', 'ed4a8d43157a5e4326ccc650af331f12e38eb84b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:09:34', 'pgm/ctr/disp.sas'),
('0101010000', 'efb6f95c88dfe668404414c916a5641890a6bb9e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:52:29', 'pgm/ctr/disp.sas'),
(NULL, 'f0aa6b81c834c2c0bb0d9f1781d12867f84c7ab0', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:02:52', ''),
('0101010000', 'f364bc77ee2212eefb24eb51b223464c427fb150', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:49:34', 'pgm/ctr/disp.sas'),
(NULL, 'f60bfebc7f7b2f7905d82e34b3a5afe45ca72637', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:59:03', ''),
('0101010000', 'f805204fa70da4c58a0eeb36bd369d4c94e4214f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:46:33', 'pgm/ctr/disp.sas'),
('0102080000', 'fa52c86774a96424d250e3dd7132fc60d2c886be', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:43:25', 'pgm/ctr/ipvsummary.sas'),
(NULL, 'fe31505956782dddfa6fed2b99a4a08ea88c6cb9', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:56:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`, `user_type`) VALUES
(3, 12378, 2, '2016-05-18 09:05:17', 'ON', 1),
(4, 12377, 2, '2016-05-18 09:05:21', 'ON', 1),
(5, 12377, 1, '2016-05-18 09:05:48', 'ON', 1),
(6, 12378, 1, '2016-05-18 09:05:55', 'ON', 1),
(7, 12379, 2, '2016-06-08 12:01:32', 'ON', 1),
(8, 12379, 1, '2016-06-08 12:01:34', 'ON', 1);

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED','') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(1, 'study_001', 111, '2016-05-17 21:45:10', 'ON'),
(2, 'study_002', 111, '2016-05-18 09:04:55', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_001`
--

INSERT INTO `toc_status_study_001` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('study_001', '0100000000', 0, 'No Program', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24', 0, 'No Output', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24'),
('study_001', '0101000000', 0, 'No Program', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24', 0, 'No Output', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24'),
('study_001', '0101010000', 1, 'No Program', '2016-05-17 21:45:10', '2016-06-09 21:55:24', '2016-05-25 11:37:23', '2016-06-08 12:03:37', 2, 'No Output', '2016-06-13 15:23:21', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101020000', 1, 'No Program', '2016-05-17 21:45:10', '2016-06-09 21:55:24', '2016-05-25 11:37:23', '2016-06-08 12:03:37', 2, 'No Output', '2016-06-13 15:23:21', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101030000', 1, 'No Program', '2016-05-17 21:45:10', '2016-05-31 10:58:47', '2016-05-17 21:45:10', '2016-05-24 15:26:50', 2, 'No Output', '2016-06-13 15:05:40', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101040000', 1, 'No Program', '2016-05-18 08:47:50', '2016-05-31 10:58:12', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101050000', 1, 'No Program', '2016-05-18 08:47:50', '2016-05-30 19:59:02', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101060000', 1, 'No Program', '2016-05-18 08:47:50', '2016-05-30 19:59:02', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0102000000', 0, 'No Program', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24', 0, 'No Output', '2016-06-01 15:39:24', '2016-06-01 15:39:24', '2016-06-01 15:39:24'),
('study_001', '0102030000', 1, 'No Program', '2016-05-18 08:47:50', '2016-06-14 07:46:11', '2016-06-01 08:19:29', '2016-06-01 09:49:49', 2, 'No Output', '2016-06-23 17:25:13', '2016-05-18 08:47:50', '2016-05-18 08:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_002`
--

INSERT INTO `toc_status_study_002` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('1264_0003', '0100000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0101000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0101010000', 1, 'No Program', '2016-05-18 09:04:54', '2016-08-02 18:56:45', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010100', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010200', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010300', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010400', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010500', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010600', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010700', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010800', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010900', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020100', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020200', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020300', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0103000000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('study_002', '0103010000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('study_002', '0103020000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('STUDY_002', '0103030000', 0, 'No Program', '2016-05-20 11:43:19', '2016-05-20 11:43:19', '2016-05-20 11:43:19', '2016-05-20 11:43:19', 0, 'No Output', '2016-05-20 11:43:19', '2016-05-20 11:43:19', '2016-05-20 11:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_001`
--

INSERT INTO `toc_study_001` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102030000', '01', '02', '03', '00', '00', ' ', 'Table', '1.2.4', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_002`
--

INSERT INTO `toc_study_002` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('1264_0003', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', '', ''),
('study_002', '0101000000', '01', '01', '00', '00', '00', 'Definition of analysis sets', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('study_002', '0102000000', '01', '02', '00', '00', '00', 'Demographic data and baseline characteristics', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102010000', '01', '02', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102010100', '01', '02', '01', '01', '00', ' ', 'Table', '1.2.1.1', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('study_002', '0102010200', '01', '02', '01', '02', '00', ' ', 'Table', '1.2.1.2', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('study_002', '0102010300', '01', '02', '01', '03', '00', ' ', 'Table', '1.2.1.3', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('study_002', '0102010400', '01', '02', '01', '04', '00', ' ', 'Table', '1.2.1.4', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('study_002', '0102010500', '01', '02', '01', '05', '00', ' ', 'Table', '1.2.1.5', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('study_002', '0102010600', '01', '02', '01', '06', '00', ' ', 'Table', '1.2.1.6', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('study_002', '0102010700', '01', '02', '01', '07', '00', ' ', 'Table', '1.2.1.7', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('study_002', '0102010800', '01', '02', '01', '08', '00', ' ', 'Table', '1.2.1.8', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('study_002', '0102010900', '01', '02', '01', '09', '00', ' ', 'Table', '1.2.1.9', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('study_002', '0102020000', '01', '02', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102020100', '01', '02', '02', '01', '00', ' ', 'Table', '1.2.2.1', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('study_002', '0102020200', '01', '02', '02', '02', '00', ' ', 'Table', '1.2.2.2', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('study_002', '0102020300', '01', '02', '02', '03', '00', ' ', 'Table', '1.2.2.3', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('study_002', '0103000000', '01', '03', '00', '00', '00', 'Compliance data', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('study_002', '0103020000', '01', '03', '02', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log'),
('STUDY_002', '0103030000', '01', '03', '03', '00', '00', ' ', 'Table', '1.3.2', 'demo table', 'pgm/ctr/demotlf.sas', 'demotlf.sas', 'T1', 'pgm/ctr/demotlf.sas', 'demotlf_t1.lst', 'demotlf.log');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstlog` varchar(50) NOT NULL,
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF','DELETED','') NOT NULL DEFAULT 'ON',
  `fullname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12382 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstlog`, `user_id`, `user_type`, `password`, `username`, `email`, `creation_date`, `status`, `fullname`) VALUES
('Yes', 1, 0, '662af1cd1976f09a9f8cecc868ccc0a2', 'admin', 'zobair@shaficonsultancy.com', '2016-05-17 16:36:53', 'ON', 'administrator '),
('No', 12377, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user1', 'dummy_user1@gmail.com', '2016-05-17 22:28:51', 'ON', 'dummy user 1'),
('No', 12378, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user2', 'dummy_user2@gmail.com', '2016-05-18 08:50:18', 'ON', 'dummy user 2'),
('No', 12379, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'DummyUser3', 'dummyuser3@gmail.com', '2016-06-15 12:45:30', 'ON', 'Mr Dummy User 3'),
('No', 12380, 1, '126cfbcd4d16ae6d25c9bfcae76d8ee4', 'dummy_user4', 'abu@shaficonsultancy.com', '2016-06-16 11:46:43', 'ON', 'dummy user 4'),
('No', 12381, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'DummyUser1', 'zobair@shaficonsultancy.com', '2016-06-16 12:00:40', 'ON', 'Ahmed Faruk');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_info`
--

CREATE TABLE IF NOT EXISTS `user_type_info` (
  `user_type` int(11) NOT NULL,
  `user_type_dc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_info`
--

INSERT INTO `user_type_info` (`user_type`, `user_type_dc`) VALUES
(0, 'Super Admin'),
(1, 'Trail Admin'),
(2, 'Editor'),
(3, 'Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `cplist_study_001`
--
ALTER TABLE `cplist_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_study_002`
--
ALTER TABLE `cplist_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_lock_status`
--
ALTER TABLE `pgm_lock_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `toc_status_study_001`
--
ALTER TABLE `toc_status_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_study_002`
--
ALTER TABLE `toc_status_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_001`
--
ALTER TABLE `toc_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_002`
--
ALTER TABLE `toc_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=278;
--
-- AUTO_INCREMENT for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12382;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
