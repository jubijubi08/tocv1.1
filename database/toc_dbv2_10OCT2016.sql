-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 09, 2016 at 09:05 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toc_dbv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
  `client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `cpgm_hist_study_001`
--

CREATE TABLE IF NOT EXISTS `cpgm_hist_study_001` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `validation_system` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `cpgm_hist_study_001`
--

INSERT INTO `cpgm_hist_study_001` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`, `validation_system`) VALUES
(1, '444', 'studyini.sas', '2016-10-10 00:30:07', 'dummy_user1', 'Uploaded', 'In Development', '2016_10_10_00_30_08_studyini.sas', NULL),
(2, '444', 'titles.sas', '2016-10-10 00:30:18', 'dummy_user1', 'Uploaded', 'In Development', '2016_10_10_00_30_18_titles.sas', NULL),
(3, '1', 'studyini.sas', '2016-10-10 00:30:30', 'dummy_user1', '', 'Validated', '2016_10_10_00_30_30_studyini.sas', 'Manual Code check'),
(4, '2', 'titles.sas', '2016-10-10 00:30:40', 'dummy_user1', '', 'Validated', '2016_10_10_00_30_40_titles.sas', 'Manual Code check');

-- --------------------------------------------------------

--
-- Table structure for table `cpgm_hist_study_002`
--

CREATE TABLE IF NOT EXISTS `cpgm_hist_study_002` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `validation_system` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cpgm_hist_study_002`
--

INSERT INTO `cpgm_hist_study_002` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`, `validation_system`) VALUES
(1, '444', 'studyini.sas', '2016-10-10 00:37:32', 'dummy_user1', 'Uploaded', 'In Development', '2016_10_10_00_37_32_studyini.sas', NULL),
(2, '444', 'titles.sas', '2016-10-10 00:37:46', 'dummy_user1', 'Uploaded', 'In Development', '2016_10_10_00_37_46_titles.sas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_001`
--

CREATE TABLE IF NOT EXISTS `cplist_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
`sortorder` int(10) NOT NULL,
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `programmer` varchar(200) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL,
  `pgmstat` int(10) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT '',
  `validator` varchar(200) NOT NULL DEFAULT '',
  `status_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cplist_study_001`
--

INSERT INTO `cplist_study_001` (`study`, `sortorder`, `cploc`, `cpname`, `programmer`, `cpdate`, `pgmstat`, `status`, `validator`, `status_date`) VALUES
('study_001', 1, 'pgm/', 'studyini.sas', 'dummy_user1', '2016-10-10 00:30:07', 3, 'Validated', 'dummy_user1', '2016-10-10 00:30:30'),
('study_001', 2, 'pgm/', 'titles.sas', 'dummy_user1', '2016-10-10 00:30:18', 3, 'Validated', 'dummy_user1', '2016-10-10 00:30:40');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_002`
--

CREATE TABLE IF NOT EXISTS `cplist_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
`sortorder` int(10) NOT NULL,
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `programmer` varchar(200) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL,
  `pgmstat` int(10) DEFAULT NULL,
  `status` varchar(50) NOT NULL DEFAULT '',
  `validator` varchar(200) NOT NULL DEFAULT '',
  `status_date` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `cplist_study_002`
--

INSERT INTO `cplist_study_002` (`study`, `sortorder`, `cploc`, `cpname`, `programmer`, `cpdate`, `pgmstat`, `status`, `validator`, `status_date`) VALUES
('study_002', 1, 'pgm/', 'studyini.sas', 'dummy_user1', '2016-10-10 00:37:32', 1, 'In Development', ' ', '2016-10-10 00:37:32'),
('study_002', 2, 'pgm/', 'titles.sas', 'dummy_user1', '2016-10-10 00:37:46', 1, 'In Development', ' ', '2016-10-10 00:37:46');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_001`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_001` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `validation_system` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `pgm_hist_study_001`
--

INSERT INTO `pgm_hist_study_001` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`, `validation_system`) VALUES
(1, '0000000000', 'disp.sas', '2016-10-09 23:49:07', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_09_23_49_08_disp.sas', NULL),
(2, '0000000000', 'disp2.sas', '2016-10-09 23:49:07', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_09_23_49_08_disp2.sas', NULL),
(3, '0000000000', 'scrandtrt.sas', '2016-10-09 23:49:07', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_09_23_49_08_scrandtrt.sas', NULL),
(4, '0000000000', 'inv.sas', '2016-10-09 23:49:07', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_09_23_49_08_inv.sas', NULL),
(5, '0000000000', 'ipvsummary.sas', '2016-10-09 23:49:07', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_09_23_49_08_ipvsummary.sas', NULL),
(6, '0000000000', 'tpopu.sas', '2016-10-09 23:49:07', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_09_23_49_08_tpopu.sas', NULL),
(7, '0101010000', 'disp.sas', '2016-10-09 23:49:40', 'dummy_user1', 'Run Failed', 'Program running', '2016_10_09_23_49_40_disp.sas', NULL),
(8, '0101010000', 'disp.sas', '2016-10-09 23:53:39', 'dummy_user1', 'Run Failed', 'Program running', '2016_10_09_23_53_39_disp.sas', NULL),
(9, '0101010000', 'disp.sas', '2016-10-10 00:20:38', 'dummy_user1', 'Run Program', 'Program running', '2016_10_10_00_20_38_disp.sas', NULL),
(10, '0101040000', 'scrandtrt.sas', '2016-10-10 00:22:41', 'dummy_user1', 'Run Program', 'Program running', '2016_10_10_00_22_41_scrandtrt.sas', NULL),
(11, '0101010000', 'disp.sas', '2016-10-10 00:32:23', 'dummy_user1', '', 'Validated', '2016_10_10_00_32_23_disp.sas', 'Manual Code check'),
(12, '0102010000', 'ipvsummary.sas', '2016-10-10 00:35:22', 'dummy_user1', 'Run Program', 'Program running', '2016_10_10_00_35_22_ipvsummary.sas', NULL),
(13, '0101040000', 'scrandtrt.sas', '2016-10-09 20:51:07', 'dummy_user1', 'Uploaded', 'In Development', '2016_10_10_00_51_07_scrandtrt.sas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_002`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_002` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL,
  `validation_system` varchar(500) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pgm_hist_study_002`
--

INSERT INTO `pgm_hist_study_002` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`, `validation_system`) VALUES
(1, '0000000000', 'demo.sas', '2016-10-10 00:25:57', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_10_00_25_57_demo.sas', NULL),
(2, '0000000000', 'exp.sas', '2016-10-10 00:25:57', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_10_00_25_58_exp.sas', NULL),
(3, '0000000000', 'cttass.sas', '2016-10-10 00:25:57', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_10_00_25_58_cttass.sas', NULL),
(4, '0000000000', 'mhist.sas', '2016-10-10 00:25:57', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_10_00_25_58_mhist.sas', NULL),
(5, '0000000000', 'comp.sas', '2016-10-10 00:25:57', 'dummy_user1', 'Generated by System', 'In Development', '2016_10_10_00_25_58_comp.sas', NULL),
(6, '0101030000', 'exp.sas', '2016-10-10 00:28:30', 'dummy_user1', 'Run Program', 'Program running', '2016_10_10_00_28_30_exp.sas', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `pgm_lock_status`
--

CREATE TABLE IF NOT EXISTS `pgm_lock_status` (
  `sortorder` varchar(10) DEFAULT NULL,
  `id` varchar(200) NOT NULL,
  `study` varchar(20) NOT NULL,
  `pgmloc` varchar(200) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `macaddr` varchar(100) NOT NULL,
  `lock_date_time` datetime NOT NULL,
  `pgmloc_ori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pgm_lock_status`
--

INSERT INTO `pgm_lock_status` (`sortorder`, `id`, `study`, `pgmloc`, `pgmname`, `username`, `password`, `macaddr`, `lock_date_time`, `pgmloc_ori`) VALUES
('555', '00e621ef6149aa53be747d874d63a224ed5ed5ab', 'study_001', 'temp/sas/225_update_monitoring.sas', '225_update_monitoring.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 19:11:54', 'pgm/225_update_monitoring.sas'),
('555', '03d59a8d1d5d5aa28b28dfa4fe1a28c6b3bcb7f3', 'test', 'temp/sas/shell2.sas', 'shell2.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 15:59:12', 'pgm/shellshell2.sas'),
('555', '04358c87f10c3d7deda4b8b890b263ddc800a3db', 'test', 'temp/sas/studyini.sas', 'studyini.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 12:53:02', 'pgm/studyini.sas'),
('0101010000', '043d9e1e24dfe867585f422d3b4d2841f0616136', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:51:07', ''),
(NULL, '05d99b5607610eb8be7742dfbf48ae8718773b8f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:45:20', ''),
('555', '074bcad6551c5618466b1dafb81f4be002046db7', 'test', 'temp/sas/shell3.sas', 'shell3.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 14:48:30', 'pgm/shellshell3.sas'),
(NULL, '086e909a9b0ebd7f6dc13e37a57e0d56a9ede9f4', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 11:10:56', ''),
('0101010000', '0af7aef5b972ec95d73847c899e8a8bf409e4a8b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:44:03', 'pgm/ctr/disp.sas'),
('0101010000', '0f595261ec53c35c44ed160d75a644f3e180ec32', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 18:30:53', 'pgm/ctr/disp.sas'),
('0101010000', '0f899e633cf03ba3045283b2b9cc4f4d420f3243', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:44:57', 'pgm/ctr/disp.sas'),
('0102080000', '123a2dfa0b34fe7f424c9cea6725a45a1cd8f9c4', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:40:09', 'pgm/ctr/ipvsummary.sas'),
('555', '1786e4f17f6a70ecb4f8455fb90c66c211ff3a92', 'study_001', 'temp/sas/225_update_monitoring.sas', '225_update_monitoring.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-04 15:14:35', 'pgm/225_update_monitoring.sas'),
('0103010000', '1859449abf9a6c88695d6a24c196222f3f52931e', 'test', 'temp/sas/comp.sas', 'comp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 21:57:14', 'pgm/ctr/comp.sas'),
('555', '1ab30b0bfff69deba38e72036ba13e09d9d6c2f1', 'study_001', 'temp/sas/225_update_monitoring.sas', '225_update_monitoring.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 19:13:45', 'pgm/225_update_monitoring.sas'),
('0102080000', '1afb3b3cb843007a97daf0efce68b17f4c2778f5', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:38:32', 'pgm/ctr/ipvsummary.sas'),
(NULL, '1b4085cd5b9a5fc5720abf663fdf8d84df7f93ea', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:46:11', ''),
('0101030000', '1d97eac18ae5808ac77be93561fdce02c1a1d3d5', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-29 13:18:22', 'pgm/ctr/disp2.sas'),
('0101040000', '236ae425bb59271bc74465123330b7b4e5c6ef2b', 'study_001', 'temp/sas/scrandtrt.sas', 'scrandtrt.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:57:33', 'pgm/ctr/scrandtrt.sas'),
('0101010000', '2a86af8e7c50ed9a896104b2a3f0175fa8a9bf3b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-06-01 11:42:22', 'pgm/ctr/disp.sas'),
('0102080000', '2c079721a436d2d1d1b85dc341b14676867fa563', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:18:18', 'pgm/ctr/ipvsummary.sas'),
(NULL, '2ceb228aa3e0de4a4fd0ae915ac6b7a8130327ff', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:52:59', ''),
('0101010000', '30a4c5a791d7b8da8c8297a5cca6e0e88e83bc35', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:56:11', 'pgm/ctr/disp.sas'),
('0102080000', '3302fe5517beb01e99792afc74410dd4e577d2aa', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:30:25', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '34827d0468598094d80b1ce7f45d79aa18abdb2b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:44:24', 'pgm/ctr/disp.sas'),
('0101010000', '34b20ff3fc4f7f3c32e300fe4d0e166fc94a9c2f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:40:10', 'temp/sas/disp.sas'),
('0101010000', '350a40277a6de20e01abbf1731c927f7aa66f726', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:17:50', 'pgm/ctr/disp.sas'),
('0102080000', '362bde0e733e99226f0f9327ffbb265c1ca8d890', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:35:37', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '3834ab13e64602fb9f55a0ea944b35ff50c5015a', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:44:08', 'pgm/ctr/disp.sas'),
('555', '3a03a127bfd493ad51f2eca60add32c06fd0a615', 'test', 'temp/sas/studyini.sas', 'studyini.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 09:17:48', 'pgm/studyini.sas'),
('0103040000', '3aafd7265c32232842cb4a07ecc4cce274fc3969', 'test', 'temp/sas/demopgm.sas', 'demopgm.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-06 13:13:15', 'pgm/ctr/demopgm.sas'),
('0101010000', '3d7b1da240bef8c55e3d38d1ee2052e94e0c0457', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:44:03', 'pgm/ctr/disp.sas'),
('0102020300', '3f8537cdb877cab85e23225534c80e61190a401b', 'test', 'temp/sas/mhist.sas', 'mhist.sas', 'dummy_user1', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-27 15:54:51', 'pgm/ctr/mhist.sas'),
('0101010000', '401f3bf22e521405940e9d7753c4f61769c80e3e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:44:43', 'pgm/ctr/disp.sas'),
('0101010000', '4137dd24a750296a7da74f1124bb474e145eb261', 'study_002', 'temp/sas/tpopu.sas', 'tpopu.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-08-02 18:56:17', 'pgm/ctr/tpopu.sas'),
('0102080000', '4240ae9755c17f3ec25dfb9903db11c3b3e52606', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:39:37', 'pgm/ctr/ipvsummary.sas'),
('0102080000', '47e75146926ea7baa6e4d31267a63e8bd911f2f9', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-27 11:16:28', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '4cb6f14e6b3ca0674ddbfcd329cc245ae6171547', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:48:39', 'pgm/ctr/disp.sas'),
('0101030000', '5024987f67f78ecae7aa7ae892f501f1e25cc4fc', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:58:30', 'pgm/ctr/disp2.sas'),
('0101010000', '51e33fa113edde82fda4e2df0439df2edd53ed8f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:47:16', 'pgm/ctr/disp.sas'),
('0102080000', '5299554cf5b0d55130e36b61b06f0f19b044e7b8', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:01:15', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '52a22901dfad2e252b5ff3a508caea7d856baf3d', 'test', 'temp/sas/tpopu.sas', 'tpopu.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-06 13:09:56', 'pgm/ctr/tpopu.sas'),
('0101010000', '52f86d559e33b54afdef02a64d46552517dccb8b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:47:29', 'pgm/ctr/disp.sas'),
(NULL, '5a42c2375fb0d6116542870d30a02fdc7612b903', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:44:09', ''),
('0101010000', '5b45cb2c42dcffe0d72173aca0bb60377cd048f8', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:26:06', 'pgm/ctr/disp.sas'),
('0101010000', '5c809f2eb97a01126114da743db9edfe7c0444b9', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:23:56', 'disp.sas'),
('555', '5d33bb35fc0f426e3e67a4b70aeb06d8044e62d9', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 09:11:53', 'pgm/shellshell1.sas'),
('0101010000', '5d6b420446152e459a78220774f0cb211fd2f329', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:38:21', ''),
(NULL, '5db341979ebc01607cd3078f1ce89cf2024f5944', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:46:49', ''),
('0101010000', '5f4089705445c9341a0745a2b4ba178f26c2ea51', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 18:12:07', 'pgm/ctr/disp.sas'),
(NULL, '5f95ed2cc2a373c9cc2f47a93668b04441cc4b40', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:50:22', ''),
('0101010000', '60d029c9af1c3085de3e10804af00480376e8531', 'test', 'temp/sas/tpopu.sas', 'tpopu.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 12:59:42', 'pgm/ctr/tpopu.sas'),
(NULL, '6811e6fdedb24e5dc29d7f5066473afc8435aa5b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:56:32', ''),
('555', '688d2d47bb58327d2caf77c697fc8fa2656ff793', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 09:11:47', 'pgm/shellshell1.sas'),
('0102080000', '6956f0590ba30ca225dc08610b6ed9b2ea60359a', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:34:20', 'pgm/ctr/ipvsummary.sas'),
(NULL, '6a2506153a7ca40b782c91a9cce4ae8015423ea2', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:54:26', ''),
('555', '6bd76529631383593178b2a6219c7ba3d0286f98', 'study_001', 'temp/sas/225_update_monitoring.sas', '225_update_monitoring.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 16:52:28', 'pgm/225_update_monitoring.sas'),
('0101010000', '6c41e19bc5d2deaa79bb291007444fd9cb0f7913', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 18:12:27', 'pgm/ctr/disp.sas'),
('0101010000', '7004883455adee5e936b71e8b75ff0ea1a653905', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-31 10:53:52', 'pgm/ctr/disp.sas'),
('0101010000', '70375a555aecb0b2d33cb1fd00889af131219f3a', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:33:07', 'disp.sas'),
('0102010100', '718794045b8e42ef70f84a6ef6321c0ad3da7204', 'study_002', 'temp/sas/demo.sas', 'demo.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-02 16:49:38', 'pgm/ctr/demo.sas'),
('0101010000', '71fbf18f6d9476fa7b18d59ef51f5fe65e212862', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:49:25', 'pgm/ctr/disp.sas'),
('0102080000', '73e4639cfb88b0e63d165aa2d3b46b3181873a18', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-30 13:01:58', 'pgm/ctr/ipvsummary.sas'),
('0102010300', '7432368a957ed1d65479b0e8929366533d0d9bc8', 'test', 'temp/sas/condiag.sas', 'condiag.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 14:54:15', 'pgm/ctr/condiag.sas'),
(NULL, '79393029964ab57adfb4b55006144a16e75db232', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:58:27', ''),
('555', '7ae5a61e12ecbaf521fd8c6dcbde566f2936fe64', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 14:52:45', 'pgm/shellshell1.sas'),
('0102080000', '7f04a660f39ef18835985832b56d4b178d2e9f36', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:43:07', 'pgm/ctr/ipvsummary.sas'),
('0101010000', '7fc699110908d552646cb979e357fe577517a8ef', 'test', 'temp/sas/tpopu.sas', 'tpopu.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-03 18:29:10', 'pgm/ctr/tpopu.sas'),
('0102010400', '82e144a541a94eb582699da61810ca85796fee8d', 'study_002', 'temp/sas/conmed.sas', 'conmed.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-02 15:01:49', 'pgm/ctr/conmed.sas'),
('555', '8475209c67d039a9a20067fa9ccf6740f4a851d7', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 09:17:38', 'pgm/shellshell1.sas'),
('0102080000', '8b4f96957cd9b98b72bfffb213fcb6b1e66c1767', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-30 13:02:36', 'pgm/ctr/ipvsummary.sas'),
('555', '8c2a42cb874b39fca13abac7608395004cd5054f', 'study_001', 'temp/sas/225_update_monitoring.sas', '225_update_monitoring.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 19:15:42', 'pgm/225_update_monitoring.sas'),
('0101010000', '8cd5c5e13d8b1849cd28e6b58db0814689540565', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:07:55', ''),
('0101010000', '8d2aae5aed70e616a46432b3c1306a2123735cf6', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 10:22:28', 'pgm/ctr/disp.sas'),
('555', '8d5611b8327100f422c718180cd3ff4bf5404da3', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 14:53:15', 'pgm/shellshell1.sas'),
('0101010000', '8da4c5898cac2218b2934df787d73a99716b8ffb', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:58:35', ''),
('555', '8df96d253b963ad011b1f0c5be0998ff3d1e9fc9', 'test', 'temp/sas/shell2.sas', 'shell2.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 17:40:42', 'pgm/shellshell2.sas'),
(NULL, '8f7c151da5a9ee03751de1a189b205ee319752f1', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-22 08:56:21', ''),
('0102080000', '91243d60fd4dd3aada234f7c92204942435488c6', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:02:36', 'pgm/ctr/ipvsummary.sas'),
('0103040000', '934ce135a2ce917d6f288dcd5226f96182f54c5a', 'test', 'temp/sas/demopgm.sas', 'demopgm.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-06 13:11:02', 'pgm/ctr/demopgm.sas'),
('0101010000', '9a0b123f2174c5bda360698e5d0e333acc049618', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:54:14', 'pgm/ctr/disp.sas'),
('0101030000', '9d737ae76c637672c90b5a154d1cfbbd00d89d6f', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 19:17:01', 'pgm/ctr/disp2.sas'),
('0101010000', '9fb790fbab94077bf5dc560b715686a6369bf5db', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:45:34', 'pgm/ctr/disp.sas'),
('0101010000', 'a4979931b76101cb3991ca2679a77b3986d64320', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 08:52:42', 'pgm/ctr/disp.sas'),
('0102080000', 'a526174a21f28cf62fadd22ed007fea02e284515', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:41:58', 'pgm/ctr/ipvsummary.sas'),
('0102080000', 'a6175cd96a035942a48bc2ed754c4ae9dc0235b0', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-27 11:18:29', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'a72f406c5adbd24d53da9c2632a959b2f83188dc', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:49:23', ''),
('0101010000', 'a76afb51b2e99c37385d0000bfcbd56288b03c91', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-06-01 11:51:38', 'pgm/ctr/disp.sas'),
('0101010000', 'aa08dcf1e121d65acee4b43f0b96f5619080dff2', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:39:49', 'pgm/ctr/disp.sas'),
('0102030000', 'acb62ee2ac3a0d12ddac9e4eca79f8eae7f8ea31', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 15:11:24', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'af17df5afbeefac309f6e246cb06f0bd7e5850da', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 14:37:28', 'pgm/ctr/disp.sas'),
('0101010000', 'b07c90a394d902616cc1643ab1f20e9d86b5023c', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:46:22', 'pgm/ctr/disp.sas'),
('0101010000', 'b1de7fd33d590604c1b9d6ac945cab4132435511', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-04 12:39:49', 'pgm/ctr/disp.sas'),
('555', 'b35a9d27781136bf8859c27bdad1ffc70eb90579', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 14:50:22', 'pgm/shellshell1.sas'),
('0102080000', 'b37a527039758b9a0e9751739de4f7211514204a', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-30 13:03:52', 'pgm/ctr/ipvsummary.sas'),
('0101030000', 'b4d5f2e279974885dbd9e466e3b7e4b328017777', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 16:11:13', 'pgm/ctr/disp2.sas'),
('0102080000', 'b8ac5750363c42cf914c01c0c547df1dccf46503', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-29 12:25:25', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'b8f786838c4f0122e35f99eaa701bfe904f22a13', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 14:39:49', 'pgm/ctr/disp.sas'),
('0101010000', 'ba576a7c5a90b168e71332958758368b60331ad5', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:50:54', 'pgm/ctr/disp.sas'),
(NULL, 'bb174bb5201c1f4b9c6a0477401d1c8866fe835d', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:50:16', ''),
('0101010000', 'bcee7b38f2a9208292a81105414f94f9520af90b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-08-10 10:47:43', 'pgm/ctr/disp.sas'),
('0102010600', 'beaa33f07f91fce2439771f0f3e1de99c8a33965', 'test', 'temp/sas/conmed.sas', 'conmed.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 12:58:02', 'pgm/ctr/conmed.sas'),
('0102010100', 'bf90eede8defa9b11e0fd451c1e11fb16f5e9fcb', 'study_002', 'temp/sas/demo.sas', 'demo.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-06 15:23:58', 'pgm/ctr/demo.sas'),
('0101040000', 'c1006a8d017ef6195960d785f4e775ce3bf1d0d4', 'study_001', 'temp/sas/scrandtrt.sas', 'scrandtrt.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:21:08', 'pgm/ctr/scrandtrt.sas'),
(NULL, 'c43a68e5ae1ef10b6d6a80e8a1de767699c25c2e', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:43:41', ''),
(NULL, 'c81435218c70cf478e5785beac42cacd1933477f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:40:05', ''),
('555', 'd1180fdcfc48d0a67220fc7e924b2d49632291b5', 'test', 'temp/sas/shell3.sas', 'shell3.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 17:46:28', 'pgm/shellshell3.sas'),
('0101010000', 'd38195922e77679e794142cb7440592cf8ba7307', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:03:07', 'pgm/ctr/disp.sas'),
('0101010000', 'd50a149ee898b0c24fe7581b159a3948ae4d5556', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:45:40', ''),
('0102080000', 'd5f499a081f43d976e893d04566c4164625fbb8d', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:09:41', 'pgm/ctr/ipvsummary.sas'),
('0102030000', 'd672eb392402c2a9b8444752b6b6a8b722ec8b7e', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user2', '2c9341ca4cf3d87b9e4e', 'D2-53-49-14-9F-AB', '2016-09-06 12:09:20', 'pgm/ctr/ipvsummary.sas'),
('0101010000', 'd78652930fa4bd0603874504d81fa1c7a045da34', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:53:35', 'pgm/ctr/disp.sas'),
(NULL, 'd8c6a0ab80f4d8621c37efdce0d981e119ddc15e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 11:46:06', ''),
('0101010000', 'daffb24d252762d31fcaab1e94fdc76dc1a71ff6', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:20:53', 'disp.sas'),
('0101010000', 'dd6efca84638ae87b5f52071a18f6eb7d0ba3964', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:42:14', 'pgm/ctr/disp.sas'),
('0101010000', 'de5dde4ce338784309a0b754efec5c1c161f9d96', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:59:27', ''),
('0101010000', 'e0c47370e4ed13ea867edbdec8b0b654fc7f5bc3', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:41:29', 'pgm/ctr/disp.sas'),
('0101010000', 'e55a6b06327a0c4589968312872d223370c57637', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:09:55', 'pgm/ctr/disp.sas'),
('0103040000', 'e6ad42abb6457ef3170a5fcbb56f91dafc6b1da5', 'test', 'temp/sas/demopgm.sas', 'demopgm.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-06 13:09:20', 'pgm/ctr/demopgm.sas'),
('0101010000', 'ed4a8d43157a5e4326ccc650af331f12e38eb84b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:09:34', 'pgm/ctr/disp.sas'),
('555', 'edc8d6af07efbf4e04f0436420e96814b2014533', 'test', 'temp/sas/studyini.sas', 'studyini.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 12:34:52', 'pgm/studyini.sas'),
('0101010000', 'efb6f95c88dfe668404414c916a5641890a6bb9e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 22:52:29', 'pgm/ctr/disp.sas'),
(NULL, 'f0aa6b81c834c2c0bb0d9f1781d12867f84c7ab0', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:02:52', ''),
('0101010000', 'f364bc77ee2212eefb24eb51b223464c427fb150', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:49:34', 'pgm/ctr/disp.sas'),
(NULL, 'f60bfebc7f7b2f7905d82e34b3a5afe45ca72637', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:59:03', ''),
('0101010000', 'f805204fa70da4c58a0eeb36bd369d4c94e4214f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:46:33', 'pgm/ctr/disp.sas'),
('0102080000', 'fa52c86774a96424d250e3dd7132fc60d2c886be', 'study_001', 'temp/sas/ipvsummary.sas', 'ipvsummary.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-25 15:43:25', 'pgm/ctr/ipvsummary.sas'),
('555', 'fafa5899f3d1d5e6847d67c917f0a8e74f1a5406', 'test', 'temp/sas/shell1.sas', 'shell1.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-10-05 17:32:14', 'pgm/shellshell1.sas'),
(NULL, 'fe31505956782dddfa6fed2b99a4a08ea88c6cb9', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:56:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `program_run_time`
--

CREATE TABLE IF NOT EXISTS `program_run_time` (
`id` int(11) NOT NULL,
  `study_name` varchar(200) NOT NULL,
  `program_id` varchar(15) NOT NULL,
  `start_date` datetime NOT NULL,
  `run_type` varchar(20) NOT NULL,
  `last_run_time` varchar(20) DEFAULT NULL,
  `next_run_time` datetime NOT NULL,
  `status` enum('ON','OFF') NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `program_run_time`
--

INSERT INTO `program_run_time` (`id`, `study_name`, `program_id`, `start_date`, `run_type`, `last_run_time`, `next_run_time`, `status`) VALUES
(1, 'study_002', '0101010000', '2016-10-09 16:30:00', 'once', '2016-10-09 16:30:41', '2016-10-09 16:30:00', 'OFF');

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=27 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`, `user_type`) VALUES
(20, 12377, 16, '2016-10-09 19:15:18', 'ON', 1),
(21, 12377, 17, '2016-10-09 20:54:09', 'ON', 1),
(22, 12377, 19, '2016-10-09 22:04:25', 'ON', 1),
(23, 12377, 20, '2016-10-09 22:32:09', 'ON', 1),
(24, 12377, 21, '2016-10-09 22:32:11', 'ON', 1),
(25, 12378, 20, '2016-10-09 22:32:17', 'ON', 1),
(26, 12377, 22, '2016-10-09 23:33:39', 'ON', 1);

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED','') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(20, 'study_001', 111, '2016-10-09 22:31:04', 'ON'),
(22, 'study_002', 111, '2016-10-09 23:33:24', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `toc_dummy`
--

CREATE TABLE IF NOT EXISTS `toc_dummy` (
  `study` varchar(200) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(200) NOT NULL DEFAULT '',
  `l2` varchar(200) NOT NULL DEFAULT '',
  `l3` varchar(200) NOT NULL DEFAULT '',
  `l4` varchar(200) NOT NULL DEFAULT '',
  `l5` varchar(200) NOT NULL DEFAULT '',
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(200) NOT NULL DEFAULT '',
  `tlfnum` varchar(200) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(200) NOT NULL DEFAULT '',
  `outno` varchar(200) NOT NULL DEFAULT '',
  `outloc` varchar(200) NOT NULL DEFAULT '',
  `outname` varchar(200) NOT NULL DEFAULT '',
  `logname` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(200) NOT NULL DEFAULT '',
  `study_type` varchar(200) NOT NULL DEFAULT '',
  `drugs` varchar(200) NOT NULL DEFAULT '',
  `phase` varchar(200) NOT NULL DEFAULT '',
  `population` varchar(200) NOT NULL DEFAULT '',
  `shell_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_1_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_1_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_2_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_2_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_3_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_3_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_4_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_4_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_5_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_5_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_6_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_6_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_7_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_7_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_8_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_8_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_9_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_9_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_10_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_10_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_11_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_11_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_12_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_12_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_13_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_13_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_14_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_14_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_15_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_15_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_16_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_16_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_17_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_17_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_18_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_18_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_19_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_19_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_20_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_20_value` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_dummy`
--

INSERT INTO `toc_dummy` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`, `status`, `study_type`, `drugs`, `phase`, `population`, `shell_name`, `parameter_1_name`, `parameter_1_value`, `parameter_2_name`, `parameter_2_value`, `parameter_3_name`, `parameter_3_value`, `parameter_4_name`, `parameter_4_value`, `parameter_5_name`, `parameter_5_value`, `parameter_6_name`, `parameter_6_value`, `parameter_7_name`, `parameter_7_value`, `parameter_8_name`, `parameter_8_value`, `parameter_9_name`, `parameter_9_value`, `parameter_10_name`, `parameter_10_value`, `parameter_11_name`, `parameter_11_value`, `parameter_12_name`, `parameter_12_value`, `parameter_13_name`, `parameter_13_value`, `parameter_14_name`, `parameter_14_value`, `parameter_15_name`, `parameter_15_value`, `parameter_16_name`, `parameter_16_value`, `parameter_17_name`, `parameter_17_value`, `parameter_18_name`, `parameter_18_value`, `parameter_19_name`, `parameter_19_value`, `parameter_20_name`, `parameter_20_value`) VALUES
('study_002', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', '', '', '', '', '', '', 'EFF', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101000000', '01', '01', '00', '00', '00', 'Definition of analysis sets', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '', '', '', '', 'SAF', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log', '', '', '', '', 'SAF', 'shell', 'ft', 'test', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102000000', '01', '02', '00', '00', '00', 'Demographic data and baseline characteristics', '', '', ' ', '', '', ' ', ' ', ' ', ' ', '', '', '', '', 'PPROT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010000', '01', '02', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', '', ' ', '', ' ', ' ', ' ', ' ', ' ', '', '', '', '', 'ITT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010100', '01', '02', '01', '01', '00', ' ', 'Table', '1.2.1.1', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log', 'v', '', '', '', 'EFF', 'shell2', 'hallo', 'test2', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010200', '01', '02', '01', '02', '00', ' ', 'Table', '1.2.1.2', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log', 'v', '', '', '', 'ITT', 'shell3', 'adsl', 'adsl', 'pop', 'trtfl=''Y''', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010300', '01', '02', '01', '03', '00', ' ', 'Table', '1.2.1.3', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log', '', '', '', '', 'PPROT', 'shell9', 'proc9', '9', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010400', '01', '02', '01', '04', '00', ' ', 'Table', '1.2.1.4', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log', '', '', '', '', 'AT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010500', '01', '02', '01', '05', '00', ' ', 'Table', '1.2.1.5', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log', '', '', '', '', 'SAF', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010600', '01', '02', '01', '06', '00', ' ', 'Table', '1.2.1.6', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log', '', '', '', '', 'PPROT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010700', '01', '02', '01', '07', '00', ' ', 'Table', '1.2.1.7', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log', '', '', '', '', 'AT', 'shell15', 'pop', 'trtfl=''y''', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010800', '01', '02', '01', '08', '00', ' ', 'Table', '1.2.1.8', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log', '', '', '', '', 'EFF', 'shell15', 'pop', 'randfl=''y''', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010900', '01', '02', '01', '09', '00', ' ', 'Table', '1.2.1.9', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors2', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log', '', '', '', '', 'ITT', 'shell15', 'prog15', '15', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102020000', '01', '02', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '', '', '', '', 'PPROT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102020100', '01', '02', '02', '01', '00', ' ', 'Table', '1.2.2.1', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log', '', '', '', '', 'AT', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102020200', '01', '02', '02', '02', '00', ' ', 'Table', '1.2.2.2', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log', '', '', '', '', 'SAF', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102020300', '01', '02', '02', '03', '00', ' ', 'Table', '1.2.2.3', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log', '', '', '', '', 'EFF', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0103000000', '01', '03', '00', '00', '00', 'Compliance data', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log', '', '', '', '', 'EFF', 'shell21', 'prog21', '21', 'prog21b', '21b', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0103020000', '01', '03', '02', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log', '', '', '', '', 'ITT', 'shell22', 'prog22', '22', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_001`
--

INSERT INTO `toc_status_study_001` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('study_001', '0100000000', 0, 'No Program', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03', 0, 'No Output', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101000000', 0, 'No Program', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03', 0, 'No Output', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101010000', 3, 'No Program', '2016-10-09 22:31:03', '2016-10-09 23:49:07', '2016-10-09 22:31:03', '2016-10-10 00:32:23', 2, 'No Output', '2016-10-10 00:20:38', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101020000', 3, 'No Program', '2016-10-09 22:31:03', '2016-10-09 23:49:07', '2016-10-09 22:31:03', '2016-10-10 00:32:23', 2, 'No Output', '2016-10-10 00:20:38', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101030000', 1, 'No Program', '2016-10-09 22:31:03', '2016-10-09 23:49:07', '2016-10-09 22:31:03', '2016-10-09 22:31:03', 0, 'No Output', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101040000', 1, 'No Program', '2016-10-09 22:31:03', '2016-10-09 20:51:07', '2016-10-09 22:31:03', '2016-10-09 22:31:03', 1, 'No Output', '2016-10-09 20:51:07', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101050000', 1, 'No Program', '2016-10-09 22:31:03', '2016-10-09 23:49:07', '2016-10-09 22:31:03', '2016-10-09 22:31:03', 0, 'No Output', '2016-10-09 22:31:03', '2016-10-09 22:31:03', '2016-10-09 22:31:03'),
('study_001', '0101060000', 1, 'No Program', '2016-10-09 22:31:04', '2016-10-09 23:49:07', '2016-10-09 22:31:04', '2016-10-09 22:31:04', 0, 'No Output', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04'),
('study_001', '0102000000', 0, 'No Program', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04', 0, 'No Output', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04'),
('study_001', '0102010000', 1, 'No Program', '2016-10-09 22:31:04', '2016-10-09 23:49:07', '2016-10-09 22:31:04', '2016-10-09 22:31:04', 2, 'No Output', '2016-10-10 00:35:22', '2016-10-09 22:31:04', '2016-10-09 22:31:04'),
('study_001', '0103000000', 0, 'No Program', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04', 0, 'No Output', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04'),
('study_001', '0103010000', 1, 'No Program', '2016-10-09 22:31:04', '2016-10-09 23:49:07', '2016-10-09 22:31:04', '2016-10-09 22:31:04', 0, 'No Output', '2016-10-09 22:31:04', '2016-10-09 22:31:04', '2016-10-09 22:31:04');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_002`
--

INSERT INTO `toc_status_study_002` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('study_002', '0100000000', 0, 'No Program', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101000000', 0, 'No Program', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101010000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101020000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101030000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 2, 'No Output', '2016-10-10 00:28:30', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101040000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 2, 'No Output', '2016-10-10 00:28:30', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101050000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 2, 'No Output', '2016-10-10 00:28:30', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101070000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101080000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0101090000', 1, 'No Program', '2016-10-09 23:33:23', '2016-10-10 00:25:57', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0102000000', 0, 'No Program', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23', 0, 'No Output', '2016-10-09 23:33:23', '2016-10-09 23:33:23', '2016-10-09 23:33:23'),
('study_002', '0102010000', 1, 'No Program', '2016-10-09 23:33:24', '2016-10-10 00:25:57', '2016-10-09 23:33:24', '2016-10-09 23:33:24', 0, 'No Output', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24'),
('study_002', '0102020000', 1, 'No Program', '2016-10-09 23:33:24', '2016-10-10 00:25:57', '2016-10-09 23:33:24', '2016-10-09 23:33:24', 0, 'No Output', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24'),
('study_002', '0102030000', 1, 'No Program', '2016-10-09 23:33:24', '2016-10-10 00:25:57', '2016-10-09 23:33:24', '2016-10-09 23:33:24', 0, 'No Output', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24'),
('study_002', '0103000000', 0, 'No Program', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24', 0, 'No Output', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24'),
('study_002', '0103010000', 1, 'No Program', '2016-10-09 23:33:24', '2016-10-10 00:25:57', '2016-10-09 23:33:24', '2016-10-09 23:33:24', 0, 'No Output', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24'),
('study_002', '0103020000', 1, 'No Program', '2016-10-09 23:33:24', '2016-10-10 00:25:57', '2016-10-09 23:33:24', '2016-10-09 23:33:24', 0, 'No Output', '2016-10-09 23:33:24', '2016-10-09 23:33:24', '2016-10-09 23:33:24');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_study_001` (
  `study` varchar(200) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(200) NOT NULL DEFAULT '',
  `l2` varchar(200) NOT NULL DEFAULT '',
  `l3` varchar(200) NOT NULL DEFAULT '',
  `l4` varchar(200) NOT NULL DEFAULT '',
  `l5` varchar(200) NOT NULL DEFAULT '',
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(200) NOT NULL DEFAULT '',
  `tlfnum` varchar(200) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(200) NOT NULL DEFAULT '',
  `outno` varchar(200) NOT NULL DEFAULT '',
  `outloc` varchar(200) NOT NULL DEFAULT '',
  `outname` varchar(200) NOT NULL DEFAULT '',
  `logname` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(200) NOT NULL DEFAULT '',
  `study_type` varchar(200) NOT NULL DEFAULT '',
  `therapeutic_area` varchar(200) NOT NULL DEFAULT '',
  `drugs` varchar(200) NOT NULL DEFAULT '',
  `phase` varchar(200) NOT NULL DEFAULT '',
  `population` varchar(200) NOT NULL DEFAULT '',
  `shell_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_1_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_1_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_2_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_2_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_3_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_3_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_4_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_4_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_5_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_5_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_6_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_6_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_7_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_7_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_8_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_8_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_9_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_9_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_10_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_10_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_11_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_11_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_12_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_12_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_13_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_13_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_14_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_14_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_15_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_15_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_16_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_16_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_17_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_17_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_18_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_18_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_19_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_19_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_20_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_20_value` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_001`
--

INSERT INTO `toc_study_001` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`, `status`, `study_type`, `therapeutic_area`, `drugs`, `phase`, `population`, `shell_name`, `parameter_1_name`, `parameter_1_value`, `parameter_2_name`, `parameter_2_value`, `parameter_3_name`, `parameter_3_value`, `parameter_4_name`, `parameter_4_value`, `parameter_5_name`, `parameter_5_value`, `parameter_6_name`, `parameter_6_value`, `parameter_7_name`, `parameter_7_value`, `parameter_8_name`, `parameter_8_value`, `parameter_9_name`, `parameter_9_value`, `parameter_10_name`, `parameter_10_value`, `parameter_11_name`, `parameter_11_value`, `parameter_12_name`, `parameter_12_value`, `parameter_13_name`, `parameter_13_value`, `parameter_14_name`, `parameter_14_value`, `parameter_15_name`, `parameter_15_value`, `parameter_16_name`, `parameter_16_value`, `parameter_17_name`, `parameter_17_value`, `parameter_18_name`, `parameter_18_value`, `parameter_19_name`, `parameter_19_value`, `parameter_20_name`, `parameter_20_value`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'TRIAL STUDY001', '', '', '', '', '', '', '', '', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'SCR', 'sh_disp1', 'indata', 'popu', 'pop', '(''SCR'',''RS'',''TS'')', 'lbl_width', '62', 'space', '4', 'tlf', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'RS', 'sh_disp1', 'indata', 'popu', 'pop', '(''SCR'',''RS'',''TS'')', 'lbl_width', '62', 'space', '4', 'tlf', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'SCR', 'sh_disp2', 'indata', 'rand', 'pop', '(''SCR'')', 'inexc_criteria', '1', 'eligble', 'NO', 'tlf', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'SCR', 'sh_scrn1', 'indata', 'patd', 'pop', 'scr=1', 'regn', 'A', 'trt', 'Yes', 'duppop', 'Total', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'TS', 'sh_inv1', 'indata', 'e_tpatt', 'pop', 'TS', 'trtsort', '99', 'actmed', 'Y', 'duppop', 'Total', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'TS2', 'sh_inv1', 'indata', 'adsl', 'pop', 'fasfl=''Y''', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'TS', 'sh_ipv1', 'indata', 'ipv', 'pop', 'TS', 'popuny', '1', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0103000000', '01', '03', '00', '00', '00', 'Definition of analysis sets', '', '', ' ', ' ', ' ', ' ', ' ', '', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_001', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log', ' ', 'PG', 'diabetes ', 'Dummy GREEN', '1', 'RS', 'sh_tpop1', 'indata', 'gentrt', 'pop', 'RS', 'grp', '(1,2,3,4,5,6,7)', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_study_002` (
  `study` varchar(200) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(200) NOT NULL DEFAULT '',
  `l2` varchar(200) NOT NULL DEFAULT '',
  `l3` varchar(200) NOT NULL DEFAULT '',
  `l4` varchar(200) NOT NULL DEFAULT '',
  `l5` varchar(200) NOT NULL DEFAULT '',
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(200) NOT NULL DEFAULT '',
  `tlfnum` varchar(200) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(200) NOT NULL DEFAULT '',
  `outno` varchar(200) NOT NULL DEFAULT '',
  `outloc` varchar(200) NOT NULL DEFAULT '',
  `outname` varchar(200) NOT NULL DEFAULT '',
  `logname` varchar(200) NOT NULL DEFAULT '',
  `status` varchar(200) NOT NULL DEFAULT '',
  `study_type` varchar(200) NOT NULL DEFAULT '',
  `therapeutic_area` varchar(200) NOT NULL DEFAULT '',
  `drugs` varchar(200) NOT NULL DEFAULT '',
  `phase` varchar(200) NOT NULL DEFAULT '',
  `population` varchar(200) NOT NULL DEFAULT '',
  `shell_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_1_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_1_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_2_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_2_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_3_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_3_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_4_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_4_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_5_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_5_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_6_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_6_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_7_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_7_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_8_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_8_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_9_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_9_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_10_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_10_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_11_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_11_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_12_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_12_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_13_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_13_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_14_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_14_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_15_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_15_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_16_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_16_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_17_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_17_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_18_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_18_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_19_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_19_value` varchar(200) NOT NULL DEFAULT '',
  `parameter_20_name` varchar(200) NOT NULL DEFAULT '',
  `parameter_20_value` varchar(200) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_002`
--

INSERT INTO `toc_study_002` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`, `status`, `study_type`, `therapeutic_area`, `drugs`, `phase`, `population`, `shell_name`, `parameter_1_name`, `parameter_1_value`, `parameter_2_name`, `parameter_2_value`, `parameter_3_name`, `parameter_3_value`, `parameter_4_name`, `parameter_4_value`, `parameter_5_name`, `parameter_5_value`, `parameter_6_name`, `parameter_6_value`, `parameter_7_name`, `parameter_7_value`, `parameter_8_name`, `parameter_8_value`, `parameter_9_name`, `parameter_9_value`, `parameter_10_name`, `parameter_10_value`, `parameter_11_name`, `parameter_11_value`, `parameter_12_name`, `parameter_12_value`, `parameter_13_name`, `parameter_13_value`, `parameter_14_name`, `parameter_14_value`, `parameter_15_name`, `parameter_15_value`, `parameter_16_name`, `parameter_16_value`, `parameter_17_name`, `parameter_17_value`, `parameter_18_name`, `parameter_18_value`, `parameter_19_name`, `parameter_19_value`, `parameter_20_name`, `parameter_20_value`) VALUES
('study_002', '0100000000', '01', '00', '00', '00', '00', 'TRIAL STUDY002', '', '', '', '', '', '', '', '', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101000000', '01', '01', '00', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', '', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS', 'sh_demo1', 'indata', 'popu', 'pop', '(''TS'' ''FAS'')', 'byvar', 'study ptno', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'FAS', 'sh_demo1', 'indata', 'popu', 'pop', '(''TS'' ''FAS'')', 'byvar', 'study ptno', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Exposure to study drug during study part A - TS', 'pgm/ctr/exp.sas', 'exp.sas', 'T1', 'lst/exp_t1.lst', 'exp_t1.lst', 'exp.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS', 'sh_exp1', 'inads1', 'popu', 'inads2', 'gentrt', 'pop', 'TS''', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Exposure to study drug during study part B - TS2', 'pgm/ctr/exp.sas', 'exp.sas', 'T2', 'lst/conmed_t1.lst', 'exp_t2.lst', 'exp.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS2', 'sh_exp1', 'inads1', 'popu', 'inads2', 'gentrt', 'pop', 'TS2''', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Exposure to study drug during study part A and part B combined - TS', 'pgm/ctr/exp.sas', 'exp.sas', 'T4', 'lst/conmed_t2.lst', 'exp_t4.lst', 'exp.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS', 'sh_exp1', 'inads1', 'popu', 'inads2', 'gentrt', 'pop', '(''TS'',''TS2'')', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101070000', '01', '01', '07', '00', '00', ' ', 'Table', '1.1.6', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS', 'sh_ct1', 'indata', 'ct', 'pop', 'popu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101080000', '01', '01', '08', '00', '00', ' ', 'Table', '1.1.7', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS', 'sh_ct1', 'indata', 'ct', 'pop', 'popu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0101090000', '01', '01', '09', '00', '00', ' ', 'Table', '1.1.8', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'FAS', 'sh_ct1', 'indata', 'ct', 'pop', 'popu', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102000000', '01', '02', '00', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', '', ' ', ' ', ' ', ' ', ' ', '', '', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'FAS', 'sh_demo1', 'indata', 'popu', 'pop', '(''TS'' ''FAS'')', 'byvar', 'study ptno', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102020000', '01', '02', '02', '00', '00', ' ', 'Table', '1.2.2', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'FAS', 'sh_demo1', 'indata', 'popu', 'pop', '(''TS'' ''FAS'')', 'byvar', 'study ptno', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0102030000', '01', '02', '03', '00', '00', ' ', 'Table', '1.2.3', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS', 'sh_mhist1', 'indata', 'medh', 'pop', 'TS', 'analno', '0', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0103000000', '01', '03', '00', '00', '00', 'Compliance data', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'FAS', 'sh_comp1', 'indata', 'comp', 'pop', '(''TS'' ''FAS'')', 'trt1', 'Part A', 'trt2', 'Part B', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''),
('study_002', '0103020000', '01', '03', '02', '00', '00', ' ', 'Table', '1.3.2', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log', '', 'XO', 'diabetes ', 'Dummy Black', '2', 'TS2', 'sh_comp1', 'indata', 'comp', 'pop', '(''TS'' ''FAS'')', 'trt1', 'Part A', 'trt2', 'Part B', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstlog` varchar(50) NOT NULL,
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF','DELETED','') NOT NULL DEFAULT 'ON',
  `fullname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12382 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstlog`, `user_id`, `user_type`, `password`, `username`, `email`, `creation_date`, `status`, `fullname`) VALUES
('Yes', 1, 0, '474c909a68ce9b63e7f694d09a8f37f0', 'admin', 'zobair@shaficonsultancy.com', '2016-05-17 16:36:53', 'ON', 'administrator '),
('No', 12377, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user1', 'dummy_user1@gmail.com', '2016-09-27 16:06:03', 'ON', 'dummy user 1'),
('No', 12378, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user2', 'dummy_user2@gmail.com', '2016-09-27 19:34:55', 'ON', 'dummy user 2'),
('No', 12379, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'DummyUser3', 'dummyuser3@gmail.com', '2016-06-15 12:45:30', 'ON', 'Mr Dummy User 3'),
('No', 12380, 1, '126cfbcd4d16ae6d25c9bfcae76d8ee4', 'dummy_user4', 'abu@shaficonsultancy.com', '2016-06-16 11:46:43', 'ON', 'dummy user 4'),
('No', 12381, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'DummyUser1', 'zobair@shaficonsultancy.com', '2016-06-16 12:00:40', 'ON', 'Ahmed Faruk');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_info`
--

CREATE TABLE IF NOT EXISTS `user_type_info` (
  `user_type` int(11) NOT NULL,
  `user_type_dc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_info`
--

INSERT INTO `user_type_info` (`user_type`, `user_type_dc`) VALUES
(0, 'Super Admin'),
(1, 'Trail Admin'),
(2, 'Editor'),
(3, 'Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `cpgm_hist_study_001`
--
ALTER TABLE `cpgm_hist_study_001`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpgm_hist_study_002`
--
ALTER TABLE `cpgm_hist_study_002`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cplist_study_001`
--
ALTER TABLE `cplist_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_study_002`
--
ALTER TABLE `cplist_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_lock_status`
--
ALTER TABLE `pgm_lock_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `program_run_time`
--
ALTER TABLE `program_run_time`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `toc_status_study_001`
--
ALTER TABLE `toc_status_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_study_002`
--
ALTER TABLE `toc_status_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_001`
--
ALTER TABLE `toc_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_002`
--
ALTER TABLE `toc_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cpgm_hist_study_001`
--
ALTER TABLE `cpgm_hist_study_001`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cpgm_hist_study_002`
--
ALTER TABLE `cpgm_hist_study_002`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cplist_study_001`
--
ALTER TABLE `cplist_study_001`
MODIFY `sortorder` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cplist_study_002`
--
ALTER TABLE `cplist_study_002`
MODIFY `sortorder` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `program_run_time`
--
ALTER TABLE `program_run_time`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12382;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
