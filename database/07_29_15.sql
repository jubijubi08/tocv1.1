-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 29, 2015 at 05:43 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `test`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_dmc3_2015_05_27_18_00_12`
--

CREATE TABLE IF NOT EXISTS `bk_toc_dmc3_2015_05_27_18_00_12` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `l1` float DEFAULT NULL,
  `l2` int(10) DEFAULT NULL,
  `l3` int(10) DEFAULT NULL,
  `l4` int(10) DEFAULT NULL,
  `l5` int(10) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_dmc3_2015_05_27_18_00_12`
--

INSERT INTO `bk_toc_dmc3_2015_05_27_18_00_12` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('dmc3', 10000, 15.1, 0, 0, 0, 0, 'TRIAL SUBJECTS', '', '', '', '', '', '', '', ''),
('dmc3', 11000, 15.1, 1, 0, 0, 0, 'Disposition of patients', '', '', '', '', '', '', '', ''),
('dmc3', 11100, 15.1, 1, 1, 0, 0, ' ', 'Table', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('dmc3', 11200, 15.1, 1, 2, 0, 0, ' ', 'Table', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('dmc3', 11300, 15.1, 1, 3, 0, 0, ' ', 'Table', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('dmc3', 11400, 15.1, 1, 4, 0, 0, ' ', 'Table', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('dmc3', 11500, 15.1, 1, 5, 0, 0, ' ', 'Table', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('dmc3', 11600, 15.1, 1, 6, 0, 0, ' ', 'Table', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('dmc3', 12000, 15.1, 2, 0, 0, 0, 'Important protocol violations', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 12100, 15.1, 2, 1, 0, 0, ' ', 'Table', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('dmc3', 13000, 15.1, 3, 0, 0, 0, 'Definition of analysis sets', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 13100, 15.1, 3, 1, 0, 0, ' ', 'Table', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('dmc3', 14000, 15.1, 4, 0, 0, 0, 'Demographic data and baseline characteristics', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 14100, 15.1, 4, 1, 0, 0, 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 14110, 15.1, 4, 1, 1, 0, ' ', 'Table', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('dmc3', 14120, 15.1, 4, 1, 2, 0, ' ', 'Table', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('dmc3', 14130, 15.1, 4, 1, 3, 0, ' ', 'Table', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('dmc3', 14140, 15.1, 4, 1, 4, 0, ' ', 'Table', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('dmc3', 14150, 15.1, 4, 1, 5, 0, ' ', 'Table', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('dmc3', 14160, 15.1, 4, 1, 6, 0, ' ', 'Table', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('dmc3', 14170, 15.1, 4, 1, 7, 0, ' ', 'Table', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('dmc3', 14180, 15.1, 4, 1, 8, 0, ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('dmc3', 14190, 15.1, 4, 1, 9, 0, ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('dmc3', 14200, 15.1, 4, 2, 0, 0, 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 14210, 15.1, 4, 2, 1, 0, ' ', 'Table', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('dmc3', 14220, 15.1, 4, 2, 2, 0, ' ', 'Table', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('dmc3', 14230, 15.1, 4, 2, 3, 0, ' ', 'Table', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('dmc3', 15000, 15.1, 5, 0, 0, 0, 'Compliance data', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 15100, 15.1, 5, 1, 0, 0, ' ', 'Table', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('dmc3', 15200, 15.1, 5, 2, 0, 0, ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
`client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=112 ;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_1264_0003`
--

CREATE TABLE IF NOT EXISTS `cplist_1264_0003` (
  `study` varchar(20) NOT NULL DEFAULT '',
`sortorder` int(10) NOT NULL,
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `cplist_1264_0003`
--

INSERT INTO `cplist_1264_0003` (`study`, `sortorder`, `cploc`, `cpname`, `cpdate`) VALUES
('1264_0003', 1, 'pgm/', 'studyauto', '2015-05-11 15:18:04'),
('1264_0003', 2, 'pgm/', 'lst2rtf', '2015-05-11 15:18:18'),
('1264_0003', 3, 'pgm/', 'titles', '2015-05-11 15:18:34'),
('1264_0003', 4, 'pgm/', 'duplicate_biprint', '2015-05-13 18:09:23'),
('1264_0003', 5, 'pgm/', 'duplicate_biprint', '2015-05-13 18:10:13');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_1264_0003`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_1264_0003` (
`id` int(10) NOT NULL,
  `sortorder` int(10) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `event_date` datetime NOT NULL,
  `username` varchar(50) NOT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) NOT NULL,
  `link` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `pgm_hist_1264_0003`
--

INSERT INTO `pgm_hist_1264_0003` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(1, 11100, 'disp.sas', '2015-06-08 16:44:36', 'ahmed', '', 'To Be validate', '2015_06_08_16_44_36_disp.sas'),
(2, 11200, 'disp.sas', '2015-06-08 16:44:44', 'ahmed', '', 'To Be validate', '2015_06_08_16_44_44_disp.sas'),
(3, 11100, 'disp.sas', '2015-06-08 16:44:57', 'ahmed', 'yahhhhhhhhhhhhhhhh', 'Validated', '2015_06_08_16_44_57_disp.sas'),
(4, 11100, 'disp.sas', '2015-06-08 16:45:09', 'ahmed', 'opssss', 'In Development', '2015_06_08_16_45_10_disp.sas'),
(5, 11100, 'disp.sas', '2015-06-08 16:45:16', 'ahmed', '', 'To Be validate', '2015_06_08_16_45_16_disp.sas'),
(6, 11200, 'disp.sas', '2015-06-08 16:45:44', 'ahmed', 'ahhh', 'In Development', '2015_06_08_16_45_44_disp.sas'),
(7, 14110, 'demo.sas', '2015-06-17 10:22:56', 'ahmed', 'ok', 'Validated', '2015_06_17_10_22_56_demo.sas'),
(8, 14110, 'demo.sas', '2015-06-17 10:23:32', 'ahmed', 'ok', 'Validated', '2015_06_17_10_23_32_demo.sas'),
(9, 11300, 'disp2.sas', '2015-06-28 13:32:56', 'ahmed', 'done 100%', 'Validated', '2015_06_28_13_32_56_disp2.sas'),
(10, 11500, 'inv.sas', '2015-06-28 14:07:11', 'ahmed', '', 'To Be validate', '2015_06_28_14_07_11_inv.sas'),
(11, 11500, 'inv.sas', '2015-06-28 14:07:54', 'ahmed', 'text text', 'Validated', '2015_06_28_14_07_54_inv.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_dmc3`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_dmc3` (
`id` int(10) NOT NULL,
  `sortorder` int(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_lock_status`
--

CREATE TABLE IF NOT EXISTS `pgm_lock_status` (
  `id` varchar(200) NOT NULL,
  `study` varchar(20) NOT NULL,
  `pgmloc` varchar(200) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `macaddr` varchar(100) NOT NULL,
  `lock_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pgm_lock_status`
--

INSERT INTO `pgm_lock_status` (`id`, `study`, `pgmloc`, `pgmname`, `username`, `password`, `macaddr`, `lock_date_time`) VALUES
('03dd5dd67694542a8b628160bb06438ed8d8be71', '1264_0003', 'http://localhost/toc/temp/1264_0003/scrandtrt.sas', 'scrandtrt.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:33:55'),
('1306402668195e53e52c41a555ff74d136774bcd', '1264_0003', 'http://localhost/toc/temp/1264_0003/scrandtrt.sas', 'scrandtrt.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-23 14:17:36'),
('239bc7bcc6b6f9ad9817cfc20377a463bd592d45', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 12:09:03'),
('2abe256247accbb48c2d8a0bacabdae088f15d38', '1264_0003', 'toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:30:13'),
('3ef8334b0477fa5eafa52f6991ae1f1fe736279f', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:27:52'),
('42b16647cae96f92dc1603d02d2384ed28b9e053', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 12:09:14'),
('4b6574aef6382458938117bfccaf0119328a3e87', '1264_0003', 'temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:30:42'),
('4ec156e4ad9b3b032f8bd46897e8d048db28816a', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-23 14:48:47'),
('53a8c3cbe9cb34dbffde71f5fc4f6d61eaf55ad8', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:18:06'),
('5efb24fa3181a9467554deaea055930a6e6163fe', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:12:11'),
('6877a4fde9ee401768a9c2cd7784687678844591', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:17:53'),
('749ad62f1de0a1b9b9691f1ff0d3035cd371816e', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:10:06'),
('75ba745075cdef9875bd5d4c36cbc90511714187', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-28 15:18:17'),
('82f67577d3a27cc25b44cd384cbc903620a4715f', '1264_0003', 'http://localhost/toc/temp/1264_0003/inv.sas', 'inv.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:18:42'),
('8432154049ee2084390ca9963b651a60ee924b47', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:49:25'),
('97398ce7881da19255280f88d62fa6907465514e', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:10:22'),
('98903d5edb197130ce5d097016913a6e3f768913', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:46:41'),
('9cab10a4b615162cb772f3e4bde83c7a3e0d5fb0', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:24:48'),
('9ce3dd00be8e4bf5deaac8c9bcb5285903cd8aff', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:30:08'),
('9f6dcf4dbe5b81690ddc3dd4de1bb787e13b54d5', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 16:29:22'),
('a9a9005838673921e1ad106a72c659db322f9043', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:10:58'),
('ae679b231426964ed6c57da3614e739ef7d7cb1d', '1264_0003', 'http://localhost/toc/temp/1264_0003/ipvsummary.sas', 'ipvsummary.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-23 14:15:05'),
('c3771ccd31894014dabf3f64dafec16da23db72b', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 12:11:56'),
('d881173373b8f4c77c30ae5d8d016ba612299901', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:04:07'),
('e86b9e25fafe1a7e8f7762523b5af11ce5740fcc', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:54:12'),
('f5d9fc20af2368e6950f6e37b08f90ed2f6edbcb', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp.sas', 'disp.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 17:37:11'),
('fd4c6442f89c539f22845fd3bf19c59d0161a8bc', '1264_0003', 'http://localhost/toc/temp/1264_0003/disp2.sas', 'disp2.sas', 'ahmed', '101', 'B8-88-E3-05-B7-AE', '2015-06-18 18:07:06');

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=199 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`, `username`) VALUES
(1, 1, 1, '2015-03-12 00:00:00', 'OF', ''),
(2, 1, 2, '2015-03-12 00:00:00', 'ON', ''),
(123, 12345, 1260, '2015-03-30 00:00:00', 'ON', ''),
(150, 1, 4, '2015-04-22 00:00:00', 'ON', 'ahmed'),
(173, 4001, 2, '2015-05-25 17:49:57', 'ON', ''),
(174, 4001, 3, '2015-05-25 17:51:09', 'ON', ''),
(175, 4001, 4, '2015-05-25 17:51:41', 'OF', ''),
(176, 4001, 1260, '2015-05-25 17:52:14', 'OF', ''),
(177, 12346, 4, '2015-05-25 17:52:35', 'ON', ''),
(178, 12346, 3, '2015-05-25 17:52:37', 'ON', ''),
(179, 12346, 2, '2015-05-25 17:52:38', 'ON', ''),
(180, 1, 3, '2015-05-26 10:10:47', 'OF', ''),
(181, 4001, 1, '2015-05-26 14:56:05', 'ON', ''),
(182, 3, 1, '2015-05-26 15:02:48', 'ON', ''),
(183, 2, 2, '2015-05-26 15:25:31', 'OF', ''),
(184, 2, 3, '2015-05-26 15:25:33', 'OF', ''),
(185, 12347, 3, '2015-05-26 15:26:16', 'ON', ''),
(186, 12347, 2, '2015-05-26 15:26:18', 'ON', ''),
(187, 12347, 1260, '2015-05-26 15:26:20', 'ON', ''),
(188, 12348, 2, '2015-05-26 16:28:07', 'ON', ''),
(189, 12348, 4, '2015-05-26 16:28:09', 'ON', ''),
(190, 12349, 2, '2015-05-28 12:00:08', 'ON', ''),
(191, 12349, 3, '2015-05-28 12:00:10', 'ON', ''),
(192, 12349, 4, '2015-05-28 12:00:11', 'ON', ''),
(193, 3, 4, '2015-05-28 12:15:04', 'ON', ''),
(194, 2, 4, '2015-05-28 14:55:27', 'ON', ''),
(195, 12350, 4, '2015-06-02 15:14:12', 'ON', ''),
(196, 12350, 2, '2015-06-02 15:14:16', 'ON', ''),
(197, 12351, 2, '2015-07-28 14:47:34', 'ON', ''),
(198, 12351, 4, '2015-07-28 14:47:46', 'OF', '');

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1261 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(1, '1203_03', 111, '2015-03-11 00:00:00', 'ON'),
(2, 'DMC3', 111, '2015-03-11 00:00:00', 'ON'),
(3, 'R101', 111, '2015-03-04 00:00:00', 'ON'),
(4, '1264_0003', 111, '2015-04-18 00:00:00', 'ON'),
(1260, '1260_03', 111, '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `tab`
--

CREATE TABLE IF NOT EXISTS `tab` (
`id` int(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `fr` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `tab`
--

INSERT INTO `tab` (`id`, `name`, `fr`) VALUES
(1, 'baaa', ''),
(2, 'malek', ''),
(3, 'TOC SYSTEM', ''),
(4, '&#2470;&#2494;&#2527;&#2495;&#2468;&#2509;&#2476;', ''),
(5, '&#2474;&#2494;&#2482;&#2472;&#2503;&#2480;', ''),
(6, 'asd sad', ''),
(7, 'testishere', '');

-- --------------------------------------------------------

--
-- Table structure for table `toc_1264_0003`
--

CREATE TABLE IF NOT EXISTS `toc_1264_0003` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `l1` float DEFAULT NULL,
  `l2` int(10) DEFAULT NULL,
  `l3` int(10) DEFAULT NULL,
  `l4` int(10) DEFAULT NULL,
  `l5` int(10) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_1264_0003`
--

INSERT INTO `toc_1264_0003` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('1264_0003', 10000, 15.1, 0, 0, 0, 0, 'TRIAL SUBJECTS', '', '', '', '', '', '', '', ''),
('1264_0003', 11000, 15.1, 1, 0, 0, 0, 'Disposition of patients', '', '', '', '', '', '', '', ''),
('1264_0003', 11100, 15.1, 1, 1, 0, 0, ' ', 'Table', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('1264_0003', 11200, 15.1, 1, 2, 0, 0, ' ', 'Table', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('1264_0003', 11300, 15.1, 1, 3, 0, 0, ' ', 'Table', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('1264_0003', 11400, 15.1, 1, 4, 0, 0, ' ', 'Table', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('1264_0003', 11500, 15.1, 1, 5, 0, 0, ' ', 'Table', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('1264_0003', 11600, 15.1, 1, 6, 0, 0, ' ', 'Table', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('1264_0003', 12000, 15.1, 2, 0, 0, 0, 'Important protocol violations', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', 12100, 15.1, 2, 1, 0, 0, ' ', 'Table', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('1264_0003', 13000, 15.1, 3, 0, 0, 0, 'Definition of analysis sets', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', 13100, 15.1, 3, 1, 0, 0, ' ', 'Table', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('1264_0003', 14000, 15.1, 4, 0, 0, 0, 'Demographic data and baseline characteristics', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', 14100, 15.1, 4, 1, 0, 0, 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', 14110, 15.1, 4, 1, 1, 0, ' ', 'Table', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('1264_0003', 14120, 15.1, 4, 1, 2, 0, ' ', 'Table', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('1264_0003', 14130, 15.1, 4, 1, 3, 0, ' ', 'Table', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('1264_0003', 14140, 15.1, 4, 1, 4, 0, ' ', 'Table', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('1264_0003', 14150, 15.1, 4, 1, 5, 0, ' ', 'Table', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('1264_0003', 14160, 15.1, 4, 1, 6, 0, ' ', 'Table', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('1264_0003', 14170, 15.1, 4, 1, 7, 0, ' ', 'Table', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('1264_0003', 14180, 15.1, 4, 1, 8, 0, ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('1264_0003', 14190, 15.1, 4, 1, 9, 0, ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('1264_0003', 14200, 15.1, 4, 2, 0, 0, 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', 14210, 15.1, 4, 2, 1, 0, ' ', 'Table', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('1264_0003', 14220, 15.1, 4, 2, 2, 0, ' ', 'Table', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('1264_0003', 14230, 15.1, 4, 2, 3, 0, ' ', 'Table', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('1264_0003', 15000, 15.1, 5, 0, 0, 0, 'Compliance data', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('1264_0003', 15100, 15.1, 5, 1, 0, 0, ' ', 'Table', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('1264_0003', 15200, 15.1, 5, 2, 0, 0, ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_dmc3`
--

CREATE TABLE IF NOT EXISTS `toc_dmc3` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `l1` float DEFAULT NULL,
  `l2` int(10) DEFAULT NULL,
  `l3` int(10) DEFAULT NULL,
  `l4` int(10) DEFAULT NULL,
  `l5` int(10) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_dmc3`
--

INSERT INTO `toc_dmc3` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('dmc3', 10000, 15.1, 0, 0, 0, 0, 'TRIAL SUBJECTS', '', '', '', '', '', '', '', ''),
('dmc3', 11000, 15.1, 1, 0, 0, 0, 'Disposition of patients', '', '', '', '', '', '', '', ''),
('dmc3', 11100, 15.1, 1, 1, 0, 0, ' ', 'Table', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('dmc3', 11200, 15.1, 1, 2, 0, 0, ' ', 'Table', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('dmc3', 11300, 15.1, 1, 3, 0, 0, ' ', 'Table', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('dmc3', 11400, 15.1, 1, 4, 0, 0, ' ', 'Table', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('dmc3', 11500, 15.1, 1, 5, 0, 0, ' ', 'Table', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('dmc3', 11600, 15.1, 1, 6, 0, 0, ' ', 'Table', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('dmc3', 12000, 15.1, 2, 0, 0, 0, 'Important protocol violations', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 12100, 15.1, 2, 1, 0, 0, ' ', 'Table', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log'),
('dmc3', 13000, 15.1, 3, 0, 0, 0, 'Definition of analysis sets', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 13100, 15.1, 3, 1, 0, 0, ' ', 'Table', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('dmc3', 14000, 15.1, 4, 0, 0, 0, 'Demographic data and baseline characteristics', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 14100, 15.1, 4, 1, 0, 0, 'Demographic data, concomitant diagnoses and therapies', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 14110, 15.1, 4, 1, 1, 0, ' ', 'Table', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('dmc3', 14120, 15.1, 4, 1, 2, 0, ' ', 'Table', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('dmc3', 14130, 15.1, 4, 1, 3, 0, ' ', 'Table', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('dmc3', 14140, 15.1, 4, 1, 4, 0, ' ', 'Table', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('dmc3', 14150, 15.1, 4, 1, 5, 0, ' ', 'Table', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('dmc3', 14160, 15.1, 4, 1, 6, 0, ' ', 'Table', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('dmc3', 14170, 15.1, 4, 1, 7, 0, ' ', 'Table', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('dmc3', 14180, 15.1, 4, 1, 8, 0, ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('dmc3', 14190, 15.1, 4, 1, 9, 0, ' ', 'Table', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('dmc3', 14200, 15.1, 4, 2, 0, 0, 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 14210, 15.1, 4, 2, 1, 0, ' ', 'Table', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('dmc3', 14220, 15.1, 4, 2, 2, 0, ' ', 'Table', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('dmc3', 14230, 15.1, 4, 2, 3, 0, ' ', 'Table', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('dmc3', 15000, 15.1, 5, 0, 0, 0, 'Compliance data', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('dmc3', 15100, 15.1, 5, 1, 0, 0, ' ', 'Table', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('dmc3', 15200, 15.1, 5, 2, 0, 0, ' ', 'Table', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_1264_0003`
--

CREATE TABLE IF NOT EXISTS `toc_status_1264_0003` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_1264_0003`
--

INSERT INTO `toc_status_1264_0003` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('1264_0003', 10000, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11000, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11100, 2, 'No Program', '2015-05-19 09:59:54', '2015-06-08 16:45:09', '2015-06-08 16:45:16', '2015-06-08 16:44:57', 1, 'No Output', '2015-05-31 09:37:45', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11200, 1, 'No Program', '2015-05-19 09:59:54', '2015-06-08 16:45:44', '2015-06-08 16:44:44', '2015-06-08 16:18:58', 1, 'No Output', '2015-06-22 14:23:14', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11300, 3, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-06-08 16:41:46', '2015-06-28 13:32:56', 1, 'No Output', '2015-06-07 15:31:50', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11400, 1, 'No Program', '2015-05-19 09:59:54', '2015-06-08 16:04:46', '2015-05-19 09:59:54', '2015-06-08 16:03:49', 1, 'No Output', '2015-06-30 10:29:08', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11500, 3, 'No Program', '2015-05-19 09:59:54', '2015-06-08 16:00:07', '2015-06-28 14:07:11', '2015-06-28 14:07:54', 1, 'No Output', '2015-05-31 09:37:45', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 11600, 3, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-06-08 15:46:08', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 12000, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 12100, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 13000, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 13100, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 14000, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 14100, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 14110, 3, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-06-17 10:23:32', 1, 'No Output', '2015-06-22 14:23:14', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 14120, 3, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-27 14:59:35', 1, 'No Output', '2015-05-31 09:37:45', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 14130, 0, 'No Program', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54', 0, 'No Output', '2015-05-19 09:59:54', '2015-05-19 09:59:54', '2015-05-19 09:59:54'),
('1264_0003', 14140, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14150, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14160, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14170, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14180, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14190, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14200, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14210, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14220, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 14230, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 15000, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 15100, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55'),
('1264_0003', 15200, 0, 'No Program', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55', 0, 'No Output', '2015-05-19 09:59:55', '2015-05-19 09:59:55', '2015-05-19 09:59:55');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_dmc3`
--

CREATE TABLE IF NOT EXISTS `toc_status_dmc3` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_dmc3`
--

INSERT INTO `toc_status_dmc3` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('dmc3', 10000, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11000, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11100, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11200, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11300, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11400, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11500, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 11600, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 12000, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 12100, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 13000, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 13100, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14000, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14100, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14110, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14120, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14130, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14140, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14150, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14160, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14170, 0, 'No Program', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06', 0, 'No Output', '2015-07-28 13:04:06', '2015-07-28 13:04:06', '2015-07-28 13:04:06'),
('dmc3', 14180, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 14190, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 14200, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 14210, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 14220, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 14230, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 15000, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 15100, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07'),
('dmc3', 15200, 0, 'No Program', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07', 0, 'No Output', '2015-07-28 13:04:07', '2015-07-28 13:04:07', '2015-07-28 13:04:07');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstlog` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `fullname` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12353 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstlog`, `user_id`, `user_type`, `password`, `username`, `email`, `creation_date`, `status`, `fullname`) VALUES
('', 1, 2, '38b3eff8baf56627478ec76a704e9b52', 'ahmed', '', '2015-06-30 04:29:15', 'ON', ''),
('No', 2, 3, 'aa68c75c4a77c87f97fb686b2f068676', 'mark', '', '2015-03-11 00:00:00', 'ON', ''),
('', 3, 1, '6974ce5ac660610b44d9b9fed0ff9548', 'jack', '', '2015-03-11 00:00:00', 'ON', ''),
('', 4001, 2, 'ffc58105bf6f8a91aba0fa2d99e6f106', 'editor1', 'editor1@tocsystem.com', '2015-04-21 09:00:00', 'ON', 'mr editor1'),
('', 12345, 0, 'e10adc3949ba59abbe56e057f20f883e', 'admin', '', '2015-03-30 00:00:00', 'ON', ''),
('', 12346, 2, 'e10adc3949ba59abbe56e057f20f883e', 'mizan22', 'mm@tt.com', '0000-00-00 00:00:00', 'ON', 'mizan alam'),
('', 12347, 1, 'e10adc3949ba59abbe56e057f20f883e', 'Jabrul', 'jj@tt.com', '0000-00-00 00:00:00', 'ON', 'jabrul islam'),
('', 12348, 2, 'e10adc3949ba59abbe56e057f20f883e', 'editor2', 'editor2@gmail.com', '0000-00-00 00:00:00', 'ON', 'mr editor2'),
('No', 12349, 3, '38b3eff8baf56627478ec76a704e9b52', 'viewer1', 'editor2@gmail.com', '0000-00-00 00:00:00', 'ON', 'mr viewer1'),
('No', 12350, 1, 'e10adc3949ba59abbe56e057f20f883e', 'trailadmin', 'trailadmin@gmail.com', '0000-00-00 00:00:00', 'ON', 'mr trail admin'),
('No', 12351, 2, 'e10adc3949ba59abbe56e057f20f883e', 'editor3', 'editor2@gmail.com', '2015-07-24 00:00:00', 'ON', 'mr editor3');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_info`
--

CREATE TABLE IF NOT EXISTS `user_type_info` (
  `user_type` int(11) NOT NULL,
  `user_type_dc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_info`
--

INSERT INTO `user_type_info` (`user_type`, `user_type_dc`) VALUES
(0, 'Super Admin'),
(1, 'Study Admin'),
(2, 'Editor'),
(3, 'Viwer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `cplist_1264_0003`
--
ALTER TABLE `cplist_1264_0003`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `pgm_hist_1264_0003`
--
ALTER TABLE `pgm_hist_1264_0003`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_dmc3`
--
ALTER TABLE `pgm_hist_dmc3`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_lock_status`
--
ALTER TABLE `pgm_lock_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `tab`
--
ALTER TABLE `tab`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `toc_1264_0003`
--
ALTER TABLE `toc_1264_0003`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_dmc3`
--
ALTER TABLE `toc_dmc3`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_1264_0003`
--
ALTER TABLE `toc_status_1264_0003`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_dmc3`
--
ALTER TABLE `toc_status_dmc3`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `cplist_1264_0003`
--
ALTER TABLE `cplist_1264_0003`
MODIFY `sortorder` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `pgm_hist_1264_0003`
--
ALTER TABLE `pgm_hist_1264_0003`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `pgm_hist_dmc3`
--
ALTER TABLE `pgm_hist_dmc3`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=199;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1261;
--
-- AUTO_INCREMENT for table `tab`
--
ALTER TABLE `tab`
MODIFY `id` int(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12353;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `study_allocation`
--
ALTER TABLE `study_allocation`
ADD CONSTRAINT `study_allocation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`),
ADD CONSTRAINT `study_allocation_ibfk_2` FOREIGN KEY (`study_id`) REFERENCES `study_info` (`study_id`);

--
-- Constraints for table `study_info`
--
ALTER TABLE `study_info`
ADD CONSTRAINT `study_info_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client_info` (`client_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
