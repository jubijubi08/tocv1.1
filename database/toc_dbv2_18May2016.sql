-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 18, 2016 at 06:31 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toc_dbv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_study_001_2016_05_18_08_47_49`
--

CREATE TABLE IF NOT EXISTS `bk_toc_study_001_2016_05_18_08_47_49` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_study_001_2016_05_18_08_47_49`
--

INSERT INTO `bk_toc_study_001_2016_05_18_08_47_49` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
  `client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_001`
--

CREATE TABLE IF NOT EXISTS `cplist_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_002`
--

CREATE TABLE IF NOT EXISTS `cplist_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_001`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_001` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `pgm_hist_study_001`
--

INSERT INTO `pgm_hist_study_001` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(2, '0101010000', 'disp.sas', '2016-05-17 23:23:45', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_17_23_23_45_disp.sas'),
(3, '0101010000', 'disp.sas', '2016-05-17 23:36:06', 'dummy_user1', '', 'To Be validated', '2016_05_17_23_36_06_disp.sas'),
(4, '0101020000', 'disp.sas', '2016-05-17 23:37:00', 'dummy_user1', '', 'Validated', '2016_05_17_23_37_00_disp.sas'),
(5, '0101020000', 'disp.sas', '2016-05-17 23:37:30', 'dummy_user1', '', 'Validated', '2016_05_17_23_37_30_disp.sas'),
(6, '0101030000', 'disp2.sas', '2016-05-17 23:40:20', 'dummy_user1', 'First Uploaded', 'In Development', '2016_05_17_23_40_20_disp2.sas'),
(7, '0101010000', 'disp.sas', '2016-05-17 23:41:20', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_17_23_41_20_disp.sas'),
(8, '0101010000', 'disp.sas', '2016-05-17 23:43:03', 'dummy_user1', '', 'Validated', '2016_05_17_23_43_03_disp.sas'),
(9, '0101020000', 'disp.sas', '2016-05-18 09:42:40', 'dummy_user2', 'Wrong output', 'In Development', '2016_05_18_09_42_40_disp.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_002`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_002` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pgm_hist_study_002`
--

INSERT INTO `pgm_hist_study_002` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(1, '0101010000', 'tpopu.sas', '2016-05-18 09:07:17', 'dummy_user2', 'First Uploaded', 'In Development', '2016_05_18_09_07_17_tpopu.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_lock_status`
--

CREATE TABLE IF NOT EXISTS `pgm_lock_status` (
  `id` varchar(200) NOT NULL,
  `study` varchar(20) NOT NULL,
  `pgmloc` varchar(200) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `macaddr` varchar(100) NOT NULL,
  `lock_date_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pgm_lock_status`
--

INSERT INTO `pgm_lock_status` (`id`, `study`, `pgmloc`, `pgmname`, `username`, `password`, `macaddr`, `lock_date_time`) VALUES
('1b4085cd5b9a5fc5720abf663fdf8d84df7f93ea', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:46:11'),
('5a42c2375fb0d6116542870d30a02fdc7612b903', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:44:09'),
('5f95ed2cc2a373c9cc2f47a93668b04441cc4b40', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:50:22'),
('79393029964ab57adfb4b55006144a16e75db232', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:58:27'),
('c43a68e5ae1ef10b6d6a80e8a1de767699c25c2e', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:43:41'),
('f60bfebc7f7b2f7905d82e34b3a5afe45ca72637', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:59:03'),
('fe31505956782dddfa6fed2b99a4a08ea88c6cb9', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:56:38');

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`, `user_type`) VALUES
(3, 12378, 2, '2016-05-18 09:05:17', 'ON', 1),
(4, 12377, 2, '2016-05-18 09:05:21', 'ON', 3),
(5, 12377, 1, '2016-05-18 09:05:48', 'ON', 1),
(6, 12378, 1, '2016-05-18 09:05:55', 'ON', 1);

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED','') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(1, 'study_001', 111, '2016-05-17 21:45:10', 'ON'),
(2, 'study_002', 111, '2016-05-18 09:04:55', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_001`
--

INSERT INTO `toc_status_study_001` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('study_001', '0100000000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101000000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101010000', 3, 'No Program', '2016-05-17 21:45:10', '2016-05-17 23:41:20', '2016-05-17 23:36:06', '2016-05-17 23:43:03', 2, 'No Output', '2016-05-18 08:48:33', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101020000', 1, 'No Program', '2016-05-17 21:45:10', '2016-05-18 09:42:40', '2016-05-17 21:45:10', '2016-05-17 23:37:30', 2, 'No Output', '2016-05-18 08:48:33', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101030000', 1, 'No Program', '2016-05-17 21:45:10', '2016-05-17 23:40:20', '2016-05-17 21:45:10', '2016-05-17 21:45:10', 0, 'No Output', '2016-05-17 21:45:10', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101040000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101050000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101060000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0102000000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0102010000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_002`
--

INSERT INTO `toc_status_study_002` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('1264_0003', '0100000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0101000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0101010000', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:07:17', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010100', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010200', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010300', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010400', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010500', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010600', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010700', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010800', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010900', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020100', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020200', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020300', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0103000000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('study_002', '0103010000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('study_002', '0103020000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_001`
--

INSERT INTO `toc_study_001` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_002`
--

INSERT INTO `toc_study_002` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('1264_0003', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', '', ''),
('study_002', '0101000000', '01', '01', '00', '00', '00', 'Definition of analysis sets', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('study_002', '0102000000', '01', '02', '00', '00', '00', 'Demographic data and baseline characteristics', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102010000', '01', '02', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102010100', '01', '02', '01', '01', '00', ' ', 'Table', '1.2.1.1', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('study_002', '0102010200', '01', '02', '01', '02', '00', ' ', 'Table', '1.2.1.2', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('study_002', '0102010300', '01', '02', '01', '03', '00', ' ', 'Table', '1.2.1.3', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('study_002', '0102010400', '01', '02', '01', '04', '00', ' ', 'Table', '1.2.1.4', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('study_002', '0102010500', '01', '02', '01', '05', '00', ' ', 'Table', '1.2.1.5', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('study_002', '0102010600', '01', '02', '01', '06', '00', ' ', 'Table', '1.2.1.6', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('study_002', '0102010700', '01', '02', '01', '07', '00', ' ', 'Table', '1.2.1.7', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('study_002', '0102010800', '01', '02', '01', '08', '00', ' ', 'Table', '1.2.1.8', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('study_002', '0102010900', '01', '02', '01', '09', '00', ' ', 'Table', '1.2.1.9', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('study_002', '0102020000', '01', '02', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102020100', '01', '02', '02', '01', '00', ' ', 'Table', '1.2.2.1', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('study_002', '0102020200', '01', '02', '02', '02', '00', ' ', 'Table', '1.2.2.2', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('study_002', '0102020300', '01', '02', '02', '03', '00', ' ', 'Table', '1.2.2.3', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('study_002', '0103000000', '01', '03', '00', '00', '00', 'Compliance data', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('study_002', '0103020000', '01', '03', '02', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstlog` varchar(50) NOT NULL,
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF','DELETED','') NOT NULL DEFAULT 'ON',
  `fullname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12379 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstlog`, `user_id`, `user_type`, `password`, `username`, `email`, `creation_date`, `status`, `fullname`) VALUES
('No', 1, 0, '474c909a68ce9b63e7f694d09a8f37f0', 'admin', 'zobair@shaficonsultancy.com', '2016-05-17 16:36:53', 'ON', 'administrator '),
('No', 12377, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user1', 'dummy_user1@gmail.com', '2016-05-17 22:28:51', 'ON', 'dummy user 1'),
('No', 12378, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user2', 'dummy_user2@gmail.com', '2016-05-18 08:50:18', 'ON', 'dummy user 2');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_info`
--

CREATE TABLE IF NOT EXISTS `user_type_info` (
  `user_type` int(11) NOT NULL,
  `user_type_dc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_info`
--

INSERT INTO `user_type_info` (`user_type`, `user_type_dc`) VALUES
(0, 'Super Admin'),
(1, 'Trail Admin'),
(2, 'Editor'),
(3, 'Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `cplist_study_001`
--
ALTER TABLE `cplist_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_study_002`
--
ALTER TABLE `cplist_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_lock_status`
--
ALTER TABLE `pgm_lock_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `toc_status_study_001`
--
ALTER TABLE `toc_status_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_study_002`
--
ALTER TABLE `toc_status_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_001`
--
ALTER TABLE `toc_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_002`
--
ALTER TABLE `toc_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12379;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
