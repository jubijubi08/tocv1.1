-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 24, 2016 at 08:15 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toc_dbv2`
--

-- --------------------------------------------------------

--
-- Table structure for table `bk_toc_study_001_2016_05_18_08_47_49`
--

CREATE TABLE IF NOT EXISTS `bk_toc_study_001_2016_05_18_08_47_49` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bk_toc_study_001_2016_05_18_08_47_49`
--

INSERT INTO `bk_toc_study_001_2016_05_18_08_47_49` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102010000', '01', '02', '01', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
  `client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_001`
--

CREATE TABLE IF NOT EXISTS `cplist_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `cplist_study_002`
--

CREATE TABLE IF NOT EXISTS `cplist_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` int(10) NOT NULL DEFAULT '0',
  `cploc` varchar(200) NOT NULL DEFAULT '',
  `cpname` varchar(50) NOT NULL DEFAULT '',
  `cpdate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_001`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_001` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=89 ;

--
-- Dumping data for table `pgm_hist_study_001`
--

INSERT INTO `pgm_hist_study_001` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(44, '0101010000', 'disp.sas', '2016-05-23 12:02:52', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_12_02_52_disp.sas'),
(48, '', 'disp.sas', '2016-05-23 12:51:33', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_12_51_33_disp.sas'),
(49, '0101010000', 'disp.sas', '2016-05-23 12:55:30', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_23_12_55_30_disp.sas'),
(61, '0101010000', 'disp.sas', '2016-05-23 13:46:36', 'dummy_user1', '', 'Validated', '2016_05_23_13_46_36_disp.sas'),
(62, '0101010000', 'disp.sas', '2016-05-23 13:47:33', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_13_47_33_disp.sas'),
(66, '0101010000', 'disp.sas', '2016-05-23 14:26:14', 'dummy_user2', 'Uploaded', 'In Development', '2016_05_23_14_26_14_disp.sas'),
(69, '0101010000', 'disp.sas', '2016-05-23 14:27:19', 'dummy_user2', '', 'Validated', '2016_05_23_14_27_19_disp.sas'),
(70, '0101010000', 'disp.sas', '2016-05-23 14:27:28', 'dummy_user2', 'qa', 'In Development', '2016_05_23_14_27_28_disp.sas'),
(71, '0101010000', 'disp.sas', '2016-05-23 17:26:52', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_17_26_52_disp.sas'),
(72, '0101010000', 'disp.sas', '2016-05-23 18:30:42', 'dummy_user1', '', 'Validated', '2016_05_23_18_30_42_disp.sas'),
(73, '0101010000', 'disp.sas', '2016-05-23 18:31:14', 'dummy_user1', 'Edited', 'In Development', '2016_05_23_18_31_14_disp.sas'),
(74, '0101010000', 'disp.sas', '2016-05-24 08:42:51', 'dummy_user2', 'qa', 'In Development', '2016_05_24_08_42_51_disp.sas'),
(75, '0101010000', 'disp.sas', '2016-05-24 08:44:54', 'dummy_user2', '', 'Validated', '2016_05_24_08_44_54_disp.sas'),
(76, '0101010000', 'disp.sas', '2016-05-24 08:53:06', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_08_53_06_disp.sas'),
(77, '0101010000', 'disp.sas', '2016-05-24 09:19:38', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_09_19_38_disp.sas'),
(78, '0101040000', 'scrandtrt.sas', '2016-05-24 10:05:36', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_10_05_36_scrandtrt.sas'),
(79, '0101010000', 'disp.sas', '2016-05-24 11:07:57', 'dummy_user2', '', 'Validated', '2016_05_24_11_07_57_disp.sas'),
(80, '0101010000', 'disp.sas', '2016-05-24 11:10:50', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_11_10_50_disp.sas'),
(81, '0101010000', 'disp.sas', '2016-05-24 11:33:19', 'dummy_user1', '', 'Validated', '2016_05_24_11_33_19_disp.sas'),
(82, '0101010000', 'disp.sas', '2016-05-24 11:33:58', 'dummy_user1', 'ss', 'In Development', '2016_05_24_11_33_58_disp.sas'),
(83, '0101010000', 'disp.sas', '2016-05-24 11:42:00', 'dummy_user1', '', 'Validated', '2016_05_24_11_42_00_disp.sas'),
(84, '0101010000', 'disp.sas', '2016-05-24 11:42:36', 'dummy_user1', 'Edited', 'In Development', '2016_05_24_11_42_36_disp.sas'),
(85, '0101040000', 'scrandtrt.sas', '2016-05-24 11:49:55', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_11_49_55_scrandtrt.sas'),
(86, '0101040000', 'scrandtrt.sas', '2016-05-24 11:50:03', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_11_50_03_scrandtrt.sas'),
(87, '0101040000', 'scrandtrt.sas', '2016-05-24 11:50:31', 'dummy_user1', 'Uploaded', 'In Development', '2016_05_24_11_50_31_scrandtrt.sas'),
(88, '0101010000', 'disp.sas', '2016-05-24 12:04:35', 'dummy_user1', 'Run Program', 'Program running', '2016_05_24_12_04_35_disp.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_hist_study_002`
--

CREATE TABLE IF NOT EXISTS `pgm_hist_study_002` (
`id` int(10) NOT NULL,
  `sortorder` varchar(10) DEFAULT NULL,
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `event_date` datetime NOT NULL,
  `username` varchar(50) DEFAULT NULL,
  `comment` text NOT NULL,
  `status` varchar(100) DEFAULT NULL,
  `link` varchar(100) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pgm_hist_study_002`
--

INSERT INTO `pgm_hist_study_002` (`id`, `sortorder`, `pgmname`, `event_date`, `username`, `comment`, `status`, `link`) VALUES
(1, '0101010000', 'tpopu.sas', '2016-05-18 09:07:17', 'dummy_user2', 'First Uploaded', 'In Development', '2016_05_18_09_07_17_tpopu.sas'),
(2, '0102010100', 'demo.sas', '2016-05-21 19:24:20', 'dummy_user2', 'First Uploaded', 'In Development', '2016_05_21_19_24_20_demo.sas');

-- --------------------------------------------------------

--
-- Table structure for table `pgm_lock_status`
--

CREATE TABLE IF NOT EXISTS `pgm_lock_status` (
  `sortorder` varchar(10) DEFAULT NULL,
  `id` varchar(200) NOT NULL,
  `study` varchar(20) NOT NULL,
  `pgmloc` varchar(200) NOT NULL,
  `pgmname` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `password` varchar(20) NOT NULL,
  `macaddr` varchar(100) NOT NULL,
  `lock_date_time` datetime NOT NULL,
  `pgmloc_ori` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pgm_lock_status`
--

INSERT INTO `pgm_lock_status` (`sortorder`, `id`, `study`, `pgmloc`, `pgmname`, `username`, `password`, `macaddr`, `lock_date_time`, `pgmloc_ori`) VALUES
('0101010000', '043d9e1e24dfe867585f422d3b4d2841f0616136', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:51:07', ''),
(NULL, '05d99b5607610eb8be7742dfbf48ae8718773b8f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:45:20', ''),
(NULL, '086e909a9b0ebd7f6dc13e37a57e0d56a9ede9f4', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 11:10:56', ''),
('0101010000', '0af7aef5b972ec95d73847c899e8a8bf409e4a8b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:44:03', 'pgm/ctr/disp.sas'),
('0101010000', '0f595261ec53c35c44ed160d75a644f3e180ec32', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 18:30:53', 'pgm/ctr/disp.sas'),
(NULL, '1b4085cd5b9a5fc5720abf663fdf8d84df7f93ea', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:46:11', ''),
(NULL, '2ceb228aa3e0de4a4fd0ae915ac6b7a8130327ff', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:52:59', ''),
('0101010000', '34827d0468598094d80b1ce7f45d79aa18abdb2b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:44:24', 'pgm/ctr/disp.sas'),
('0101010000', '34b20ff3fc4f7f3c32e300fe4d0e166fc94a9c2f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:40:10', 'temp/sas/disp.sas'),
('0101010000', '350a40277a6de20e01abbf1731c927f7aa66f726', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:17:50', 'pgm/ctr/disp.sas'),
('0101010000', '51e33fa113edde82fda4e2df0439df2edd53ed8f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:47:16', 'pgm/ctr/disp.sas'),
(NULL, '5a42c2375fb0d6116542870d30a02fdc7612b903', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:44:09', ''),
('0101010000', '5c809f2eb97a01126114da743db9edfe7c0444b9', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:23:56', 'disp.sas'),
('0101010000', '5d6b420446152e459a78220774f0cb211fd2f329', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:38:21', ''),
(NULL, '5db341979ebc01607cd3078f1ce89cf2024f5944', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:46:49', ''),
('0101010000', '5f4089705445c9341a0745a2b4ba178f26c2ea51', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 18:12:07', 'pgm/ctr/disp.sas'),
(NULL, '5f95ed2cc2a373c9cc2f47a93668b04441cc4b40', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:50:22', ''),
(NULL, '6811e6fdedb24e5dc29d7f5066473afc8435aa5b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:56:32', ''),
(NULL, '6a2506153a7ca40b782c91a9cce4ae8015423ea2', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:54:26', ''),
('0101010000', '70375a555aecb0b2d33cb1fd00889af131219f3a', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:33:07', 'disp.sas'),
(NULL, '79393029964ab57adfb4b55006144a16e75db232', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:58:27', ''),
('0101010000', '8cd5c5e13d8b1849cd28e6b58db0814689540565', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:07:55', ''),
('0101010000', '8da4c5898cac2218b2934df787d73a99716b8ffb', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:58:35', ''),
(NULL, '8f7c151da5a9ee03751de1a189b205ee319752f1', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-22 08:56:21', ''),
('0101010000', 'a4979931b76101cb3991ca2679a77b3986d64320', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 08:52:42', 'pgm/ctr/disp.sas'),
('0101010000', 'a72f406c5adbd24d53da9c2632a959b2f83188dc', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:49:23', ''),
('0101010000', 'aa08dcf1e121d65acee4b43f0b96f5619080dff2', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:39:49', 'pgm/ctr/disp.sas'),
(NULL, 'bb174bb5201c1f4b9c6a0477401d1c8866fe835d', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-19 14:50:16', ''),
(NULL, 'c43a68e5ae1ef10b6d6a80e8a1de767699c25c2e', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:43:41', ''),
(NULL, 'c81435218c70cf478e5785beac42cacd1933477f', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 10:40:05', ''),
('0101010000', 'd50a149ee898b0c24fe7581b159a3948ae4d5556', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:45:40', ''),
(NULL, 'd8c6a0ab80f4d8621c37efdce0d981e119ddc15e', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 11:46:06', ''),
('0101010000', 'daffb24d252762d31fcaab1e94fdc76dc1a71ff6', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 13:20:53', 'disp.sas'),
('0101010000', 'dd6efca84638ae87b5f52071a18f6eb7d0ba3964', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:42:14', 'pgm/ctr/disp.sas'),
('0101010000', 'de5dde4ce338784309a0b754efec5c1c161f9d96', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:59:27', ''),
('0101010000', 'e55a6b06327a0c4589968312872d223370c57637', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:09:55', 'pgm/ctr/disp.sas'),
('0101010000', 'ed4a8d43157a5e4326ccc650af331f12e38eb84b', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user2', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-24 11:09:34', 'pgm/ctr/disp.sas'),
(NULL, 'f0aa6b81c834c2c0bb0d9f1781d12867f84c7ab0', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 12:02:52', ''),
('0101010000', 'f364bc77ee2212eefb24eb51b223464c427fb150', 'study_001', 'temp/sas/disp.sas', 'disp.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-23 17:49:34', 'pgm/ctr/disp.sas'),
(NULL, 'f60bfebc7f7b2f7905d82e34b3a5afe45ca72637', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:59:03', ''),
(NULL, 'fe31505956782dddfa6fed2b99a4a08ea88c6cb9', 'study_001', 'temp/sas/disp2.sas', 'disp2.sas', 'dummy_user1', '662af1cd1976f09a9f8c', 'D2-53-49-14-9F-AB', '2016-05-17 23:56:38', '');

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON',
  `user_type` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`, `user_type`) VALUES
(3, 12378, 2, '2016-05-18 09:05:17', 'ON', 1),
(4, 12377, 2, '2016-05-18 09:05:21', 'ON', 3),
(5, 12377, 1, '2016-05-18 09:05:48', 'ON', 1),
(6, 12378, 1, '2016-05-18 09:05:55', 'ON', 1);

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED','') CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(1, 'study_001', 111, '2016-05-17 21:45:10', 'ON'),
(2, 'study_002', 111, '2016-05-18 09:04:55', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_001`
--

INSERT INTO `toc_status_study_001` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('study_001', '0100000000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101000000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101010000', 1, 'No Program', '2016-05-17 21:45:10', '2016-05-24 11:42:36', '2016-05-23 14:26:53', '2016-05-24 11:42:00', 2, 'No Output', '2016-05-24 12:04:35', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101020000', 1, 'No Program', '2016-05-17 21:45:10', '2016-05-24 11:42:36', '2016-05-23 14:26:53', '2016-05-24 11:42:00', 2, 'No Output', '2016-05-24 12:04:35', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101030000', 1, 'No Program', '2016-05-17 21:45:10', '2016-05-21 17:44:03', '2016-05-17 21:45:10', '2016-05-17 21:45:10', 0, 'No Output', '2016-05-17 21:45:10', '2016-05-17 21:45:10', '2016-05-17 21:45:10'),
('study_001', '0101040000', 1, 'No Program', '2016-05-18 08:47:50', '2016-05-24 11:50:31', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101050000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0101060000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0102000000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50'),
('study_001', '0102030000', 0, 'No Program', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50', 0, 'No Output', '2016-05-18 08:47:50', '2016-05-18 08:47:50', '2016-05-18 08:47:50');

-- --------------------------------------------------------

--
-- Table structure for table `toc_status_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_status_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `pgmstat` int(10) DEFAULT NULL,
  `pgmstatdc` varchar(50) DEFAULT NULL,
  `pdate_0` datetime NOT NULL,
  `pdate_1` datetime NOT NULL,
  `pdate_2` datetime NOT NULL,
  `pdate_3` datetime NOT NULL,
  `outstat` int(10) DEFAULT NULL,
  `outstatdc` varchar(50) DEFAULT NULL,
  `odate_0` datetime NOT NULL,
  `odate_1` datetime NOT NULL,
  `odate_2` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_status_study_002`
--

INSERT INTO `toc_status_study_002` (`study`, `sortorder`, `pgmstat`, `pgmstatdc`, `pdate_0`, `pdate_1`, `pdate_2`, `pdate_3`, `outstat`, `outstatdc`, `odate_0`, `odate_1`, `odate_2`) VALUES
('1264_0003', '0100000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0101000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0101010000', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:07:17', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102000000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010100', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010200', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010300', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010400', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010500', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010600', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010700', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010800', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102010900', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020000', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020100', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020200', 1, 'No Program', '2016-05-18 09:04:54', '2016-05-21 19:24:20', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0102020300', 0, 'No Program', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54', 0, 'No Output', '2016-05-18 09:04:54', '2016-05-18 09:04:54', '2016-05-18 09:04:54'),
('study_002', '0103000000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('study_002', '0103010000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('study_002', '0103020000', 0, 'No Program', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55', 0, 'No Output', '2016-05-18 09:04:55', '2016-05-18 09:04:55', '2016-05-18 09:04:55'),
('STUDY_002', '0103030000', 0, 'No Program', '2016-05-20 11:43:19', '2016-05-20 11:43:19', '2016-05-20 11:43:19', '2016-05-20 11:43:19', 0, 'No Output', '2016-05-20 11:43:19', '2016-05-20 11:43:19', '2016-05-20 11:43:19');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_001`
--

CREATE TABLE IF NOT EXISTS `toc_study_001` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_001`
--

INSERT INTO `toc_study_001` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('study_001', '0100000000', '01', '00', '00', '00', '00', 'DUMMY TRIAL', '', '', '', '', '', '', '', '', ''),
('study_001', '0101000000', '01', '01', '00', '00', '00', 'Disposition of patients', '', '', '', '', '', '', '', '', ''),
('study_001', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Disposition of patients - SCR', 'pgm/ctr/disp.sas', 'disp.sas', 'T1', 'lst/disp_t1.lst', 'disp_t1.lst', 'disp.log'),
('study_001', '0101020000', '01', '01', '02', '00', '00', ' ', 'Table', '1.1.2', 'Disposition of patients in relation to the timing of the implementation', 'pgm/ctr/disp.sas', 'disp.sas', 'T2', 'lst/disp_t2.lst', 'disp_t2.lst', 'disp.log'),
('study_001', '0101030000', '01', '01', '03', '00', '00', ' ', 'Table', '1.1.3', 'Primary reason for not randomising screened patients - SCR', 'pgm/ctr/disp2.sas', 'disp2.sas', 'T1', 'lst/disp2_t1.lst', 'disp2_t1.lst', 'disp2.log'),
('study_001', '0101040000', '01', '01', '04', '00', '00', ' ', 'Table', '1.1.4', 'Number of screened, randomised and treated patients by region and country - SCR', 'pgm/ctr/scrandtrt.sas', 'scrandtrt.sas', 'T1', 'lst/scrandtrt_t1.lst', 'scrandtrt_t1.lst', 'scrandtrt.log'),
('study_001', '0101050000', '01', '01', '05', '00', '00', ' ', 'Table', '1.1.5', 'Number of treated patients in study part A by country and centre - TS', 'pgm/ctr/inv.sas', 'inv.sas', 'T1', 'lst/inv_t1.lst', 'inv_t1.lst', 'inv.log'),
('study_001', '0101060000', '01', '01', '06', '00', '00', ' ', 'Table', '1.1.6', 'Number of treated patients in study part B by country and centre - TS2', 'pgm/ctr/inv.sas', 'inv.sas', 'T2', 'lst/inv_t2.lst', 'inv_t2.lst', 'inv.log'),
('study_001', '0102000000', '01', '02', '00', '00', '00', 'Important protocol violations', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_001', '0102030000', '01', '02', '03', '00', '00', ' ', 'Table', '1.2.1', 'Number of patients with important protocol violations in study part A - TS', 'pgm/ctr/ipvsummary.sas', 'ipvsummary.sas', 'T1', 'lst/ipvsummary_t1.lst', 'ipvsummary_t1.lst', 'ipvsummary.log');

-- --------------------------------------------------------

--
-- Table structure for table `toc_study_002`
--

CREATE TABLE IF NOT EXISTS `toc_study_002` (
  `study` varchar(20) NOT NULL DEFAULT '',
  `sortorder` varchar(10) NOT NULL DEFAULT '',
  `l1` varchar(2) DEFAULT NULL,
  `l2` varchar(2) DEFAULT NULL,
  `l3` varchar(2) DEFAULT NULL,
  `l4` varchar(2) DEFAULT NULL,
  `l5` varchar(2) DEFAULT NULL,
  `section` varchar(200) NOT NULL DEFAULT '',
  `type` varchar(10) NOT NULL DEFAULT '',
  `tlfnum` varchar(10) NOT NULL DEFAULT '',
  `title` varchar(200) NOT NULL DEFAULT '',
  `pgmloc` varchar(200) NOT NULL DEFAULT '',
  `pgmname` varchar(50) NOT NULL DEFAULT '',
  `outno` varchar(50) NOT NULL DEFAULT '',
  `outloc` varchar(50) NOT NULL DEFAULT '',
  `outname` varchar(50) NOT NULL DEFAULT '',
  `logname` varchar(50) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `toc_study_002`
--

INSERT INTO `toc_study_002` (`study`, `sortorder`, `l1`, `l2`, `l3`, `l4`, `l5`, `section`, `type`, `tlfnum`, `title`, `pgmloc`, `pgmname`, `outno`, `outloc`, `outname`, `logname`) VALUES
('1264_0003', '0100000000', '01', '00', '00', '00', '00', 'TRIAL SUBJECTS', '', '', '', '', '', '', '', '', ''),
('study_002', '0101000000', '01', '01', '00', '00', '00', 'Definition of analysis sets', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0101010000', '01', '01', '01', '00', '00', ' ', 'Table', '1.1.1', 'Patient analysis sets and reasons for exclusion - RS', 'pgm/ctr/tpopu.sas', 'tpopu.sas', 'T1', 'lst/tpopu_t1.lst', 'tpopu_t1.lst', 'tpopu.log'),
('study_002', '0102000000', '01', '02', '00', '00', '00', 'Demographic data and baseline characteristics', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102010000', '01', '02', '01', '00', '00', 'Demographic data, concomitant diagnoses and therapies', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102010100', '01', '02', '01', '01', '00', ' ', 'Table', '1.2.1.1', 'Demographic data - TS', 'pgm/ctr/demo.sas', 'demo.sas', 'T1', 'lst/demo_t1.lst', 'demo_t1.lst', 'demo.log'),
('study_002', '0102010200', '01', '02', '01', '02', '00', ' ', 'Table', '1.2.1.2', 'Demographic data - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T2', 'lst/demo_t2.lst', 'demo_t2.lst', 'demo.log'),
('study_002', '0102010300', '01', '02', '01', '03', '00', ' ', 'Table', '1.2.1.3', 'Concomitant diagnoses by MedDRA SOC and preferred term - TS', 'pgm/ctr/condiag.sas', 'condiag.sas', 'T1', 'lst/condiag_t1.lst', 'condiag_t1.lst', 'condiag.log'),
('study_002', '0102010400', '01', '02', '01', '04', '00', ' ', 'Table', '1.2.1.4', 'Concomitant therapies during screening by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T1', 'lst/conmed_t1.lst', 'conmed_t1.lst', 'conmed.log'),
('study_002', '0102010500', '01', '02', '01', '05', '00', ' ', 'Table', '1.2.1.5', 'New concomitant therapies during study part A by WHO INN - TS', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T2', 'lst/conmed_t2.lst', 'conmed_t2.lst', 'conmed.log'),
('study_002', '0102010600', '01', '02', '01', '06', '00', ' ', 'Table', '1.2.1.6', 'New concomitant therapies during study part B by WHO INN - TS2', 'pgm/ctr/conmed.sas', 'conmed.sas', 'T3', 'lst/conmed_t3.lst', 'conmed_t3.lst', 'conmed.log'),
('study_002', '0102010700', '01', '02', '01', '07', '00', ' ', 'Table', '1.2.1.7', 'Use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors during screening - TS', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T1', 'lst/cttass_t1.lst', 'cttass_t1.lst', 'cttass.log'),
('study_002', '0102010800', '01', '02', '01', '08', '00', ' ', 'Table', '1.2.1.8', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T2', 'lst/cttass_t2.lst', 'cttass_t2.lst', 'cttass.log'),
('study_002', '0102010900', '01', '02', '01', '09', '00', ' ', 'Table', '1.2.1.9', 'New use of ASA, antihypertensives, lipid lowering drugs or P-gp and CYP 3A4 inhibitors', 'pgm/ctr/cttass.sas', 'cttass.sas', 'T3', 'lst/cttass_t3.lst', 'cttass_t3.lst', 'cttass.log'),
('study_002', '0102020000', '01', '02', '02', '00', '00', 'Diabetic baseline characteristics, antidiabetic therapies and diabetic medical history', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0102020100', '01', '02', '02', '01', '00', ' ', 'Table', '1.2.2.1', 'Baseline efficacy variables and duration of diabetes - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T3', 'lst/demo_t3.lst', 'demo_t3.lst', 'demo.log'),
('study_002', '0102020200', '01', '02', '02', '02', '00', ' ', 'Table', '1.2.2.2', 'Antidiabetic treatment at enrolment - FAS', 'pgm/ctr/demo.sas', 'demo.sas', 'T4', 'lst/demo_t4.lst', 'demo_t4.lst', 'demo.log'),
('study_002', '0102020300', '01', '02', '02', '03', '00', ' ', 'Table', '1.2.2.3', 'Relevant medical history - TS', 'pgm/ctr/mhist.sas', 'mhist.sas', 'T1', 'lst/mhist_t1.lst', 'mhist_t1.lst', 'mhist.log'),
('study_002', '0103000000', '01', '03', '00', '00', '00', 'Compliance data', '', '', ' ', ' ', ' ', ' ', ' ', ' ', ' '),
('study_002', '0103010000', '01', '03', '01', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part A - FAS', 'pgm/ctr/comp.sas', 'comp.sas', 'T1', 'lst/comp_t1.lst', 'comp_t1.lst', 'comp.log'),
('study_002', '0103020000', '01', '03', '02', '00', '00', ' ', 'Table', '1.3.1', 'Compliance data during study part B - TS2', 'pgm/ctr/comp.sas', 'comp.sas', 'T2', 'lst/comp_t2.lst', 'comp_t2.lst', 'comp.log'),
('STUDY_002', '0103030000', '01', '03', '03', '00', '00', ' ', 'Table', '1.3.2', 'demo table', 'pgm/ctr/demotlf.sas', 'demotlf.sas', 'T1', 'pgm/ctr/demotlf.sas', 'demotlf_t1.lst', 'demotlf.log');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
  `firstlog` varchar(50) NOT NULL,
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL DEFAULT '1',
  `password` varchar(50) NOT NULL,
  `username` varchar(40) NOT NULL,
  `email` varchar(100) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF','DELETED','') NOT NULL DEFAULT 'ON',
  `fullname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12379 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`firstlog`, `user_id`, `user_type`, `password`, `username`, `email`, `creation_date`, `status`, `fullname`) VALUES
('No', 1, 0, '474c909a68ce9b63e7f694d09a8f37f0', 'admin', 'zobair@shaficonsultancy.com', '2016-05-17 16:36:53', 'ON', 'administrator '),
('No', 12377, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user1', 'dummy_user1@gmail.com', '2016-05-17 22:28:51', 'ON', 'dummy user 1'),
('No', 12378, 1, '662af1cd1976f09a9f8cecc868ccc0a2', 'dummy_user2', 'dummy_user2@gmail.com', '2016-05-18 08:50:18', 'ON', 'dummy user 2');

-- --------------------------------------------------------

--
-- Table structure for table `user_type_info`
--

CREATE TABLE IF NOT EXISTS `user_type_info` (
  `user_type` int(11) NOT NULL,
  `user_type_dc` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_type_info`
--

INSERT INTO `user_type_info` (`user_type`, `user_type_dc`) VALUES
(0, 'Super Admin'),
(1, 'Trail Admin'),
(2, 'Editor'),
(3, 'Viewer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `cplist_study_001`
--
ALTER TABLE `cplist_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `cplist_study_002`
--
ALTER TABLE `cplist_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pgm_lock_status`
--
ALTER TABLE `pgm_lock_status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `toc_status_study_001`
--
ALTER TABLE `toc_status_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_status_study_002`
--
ALTER TABLE `toc_status_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_001`
--
ALTER TABLE `toc_study_001`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `toc_study_002`
--
ALTER TABLE `toc_study_002`
 ADD PRIMARY KEY (`sortorder`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pgm_hist_study_001`
--
ALTER TABLE `pgm_hist_study_001`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `pgm_hist_study_002`
--
ALTER TABLE `pgm_hist_study_002`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12379;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
