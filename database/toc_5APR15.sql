-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2015 at 06:55 AM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `toc`
--

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
`client_id` bigint(20) NOT NULL,
  `client_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OFF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=112 ;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`client_id`, `client_name`, `creation_date`, `status`) VALUES
(111, 'BI', '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `study_allocation`
--

CREATE TABLE IF NOT EXISTS `study_allocation` (
`allocation_id` bigint(20) NOT NULL,
  `user_id` bigint(20) NOT NULL,
  `study_id` bigint(20) NOT NULL,
  `allocation_date` datetime NOT NULL,
  `status` enum('ON','OF') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=124 ;

--
-- Dumping data for table `study_allocation`
--

INSERT INTO `study_allocation` (`allocation_id`, `user_id`, `study_id`, `allocation_date`, `status`) VALUES
(1, 1, 1, '2015-03-12 00:00:00', 'ON'),
(2, 1, 2, '2015-03-12 00:00:00', 'ON'),
(3, 2, 3, '2015-03-12 00:00:00', 'ON'),
(123, 12345, 1260, '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `study_info`
--

CREATE TABLE IF NOT EXISTS `study_info` (
`study_id` bigint(20) NOT NULL,
  `study_name` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` bigint(20) NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1261 ;

--
-- Dumping data for table `study_info`
--

INSERT INTO `study_info` (`study_id`, `study_name`, `client_id`, `creation_date`, `status`) VALUES
(1, '1203_03', 111, '2015-03-11 00:00:00', 'ON'),
(2, 'DMC3', 111, '2015-03-11 00:00:00', 'ON'),
(3, 'R101', 111, '2015-03-04 00:00:00', 'ON'),
(1260, '1260_03', 111, '2015-03-30 00:00:00', 'ON');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE IF NOT EXISTS `user_info` (
`user_id` bigint(20) NOT NULL,
  `user_type` int(10) NOT NULL,
  `user_type_dc` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `creation_date` datetime NOT NULL,
  `status` enum('ON','OF','DELETED') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ON'
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12346 ;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`user_id`, `user_type`, `user_type_dc`, `password`, `username`, `creation_date`, `status`) VALUES
(1, 2, 'Editor', '101', 'ahmed', '2015-03-30 00:00:00', 'ON'),
(2, 3, 'Viewer', '102', 'mark', '2015-03-11 00:00:00', 'ON'),
(3, 1, 'Study Admin', '103', 'jack', '2015-03-11 00:00:00', 'ON'),
(12345, 0, 'Super Admin', 'md5', 'Amanda ', '2015-03-30 00:00:00', 'ON');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
 ADD PRIMARY KEY (`client_id`);

--
-- Indexes for table `study_allocation`
--
ALTER TABLE `study_allocation`
 ADD PRIMARY KEY (`allocation_id`), ADD KEY `user_id` (`user_id`), ADD KEY `study_id` (`study_id`);

--
-- Indexes for table `study_info`
--
ALTER TABLE `study_info`
 ADD PRIMARY KEY (`study_id`), ADD KEY `client_id` (`client_id`);

--
-- Indexes for table `user_info`
--
ALTER TABLE `user_info`
 ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
MODIFY `client_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=112;
--
-- AUTO_INCREMENT for table `study_allocation`
--
ALTER TABLE `study_allocation`
MODIFY `allocation_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=124;
--
-- AUTO_INCREMENT for table `study_info`
--
ALTER TABLE `study_info`
MODIFY `study_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1261;
--
-- AUTO_INCREMENT for table `user_info`
--
ALTER TABLE `user_info`
MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12346;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `study_allocation`
--
ALTER TABLE `study_allocation`
ADD CONSTRAINT `study_allocation_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user_info` (`user_id`),
ADD CONSTRAINT `study_allocation_ibfk_2` FOREIGN KEY (`study_id`) REFERENCES `study_info` (`study_id`);

--
-- Constraints for table `study_info`
--
ALTER TABLE `study_info`
ADD CONSTRAINT `study_info_ibfk_1` FOREIGN KEY (`client_id`) REFERENCES `client_info` (`client_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
