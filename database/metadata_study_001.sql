-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 06, 2017 at 09:20 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toc-v1`
--

-- --------------------------------------------------------

--
-- Table structure for table `metadata_study_001`
--

CREATE TABLE `metadata_study_001` (
  `id` int(11) NOT NULL,
  `fieldname` varchar(100) NOT NULL,
  `searchtitle` varchar(250) DEFAULT NULL,
  `value` varchar(250) NOT NULL,
  `action` enum('New','Updated','Deleted') NOT NULL,
  `is_current` enum('YES','NO') NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(50) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(50) DEFAULT NULL,
  `new_value` varchar(250) DEFAULT NULL,
  `previous_value` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `metadata_study_001`
--

INSERT INTO `metadata_study_001` (`id`, `fieldname`, `searchtitle`, `value`, `action`, `is_current`, `created_at`, `created_by`, `updated_at`, `updated_by`, `new_value`, `previous_value`) VALUES
(2, 'phase', 'Phase', '1', 'New', 'NO', '2017-02-26 00:00:00', 'dummy_user1', NULL, NULL, 'test value', NULL),
(3, 'primary_endpoint', 'Primary Endpoint', 'quality of life', 'New', 'NO', '2017-02-27 00:00:00', 'dummy_user1', NULL, NULL, 'quality of life', NULL),
(4, 'phase', 'Phase', '2', 'Updated', 'NO', NULL, NULL, '2017-02-27 11:43:35', 'dummy_user1', '2', '1'),
(5, 'phase', 'Phase', '3', 'Updated', 'NO', NULL, NULL, '2017-02-27 11:50:43', 'dummy_user1', '3', '2'),
(6, 'phase', 'Phase', '4', 'Deleted', 'NO', NULL, NULL, '2017-02-27 11:51:12', 'dummy_user1', '4', '3'),
(7, 'metadataone', NULL, 'test', 'Deleted', 'NO', '2017-02-27 16:35:19', 'dummy_user1', NULL, NULL, 'test', NULL),
(8, 'metadataone', NULL, 'test', 'Deleted', 'NO', '2017-02-27 16:35:40', 'dummy_user1', NULL, NULL, 'test', NULL),
(9, 'NewMeta', NULL, '2', 'New', 'NO', '2017-02-27 16:39:20', 'dummy_user1', NULL, NULL, '2', NULL),
(10, 'NewMeta', NULL, '3', 'Updated', 'NO', NULL, NULL, '2017-02-27 16:40:01', 'dummy_user1', '3', '2'),
(11, 'NewMeta', '', '4', 'Deleted', 'NO', NULL, NULL, '2017-02-27 16:40:28', 'dummy_user1', '4', '3'),
(12, 'metanew', NULL, '0', 'New', 'NO', '2017-02-27 16:40:43', 'dummy_user1', NULL, NULL, '0', NULL),
(13, 'metanew', 'Test', '1', 'Deleted', 'NO', NULL, NULL, '2017-02-27 16:41:20', 'dummy_user1', '1', '0'),
(14, 'primary_endpoint', 'Test2', 'quality of life', 'Updated', 'YES', NULL, NULL, '2017-02-28 16:03:28', 'dummy_user1', 'quality of life', 'quality of life'),
(15, 'Test', NULL, '2', 'Deleted', 'NO', '2017-02-28 17:01:14', 'dummy_user1', NULL, NULL, '2', NULL),
(16, 'test2', NULL, 'Test', 'Deleted', 'NO', '2017-02-28 17:04:23', 'dummy_user1', NULL, NULL, 'Test', NULL),
(17, 'test', 'Search Test', 'Test', 'New', 'YES', '2017-02-28 17:09:26', 'dummy_user1', NULL, NULL, 'Test', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `metadata_study_001`
--
ALTER TABLE `metadata_study_001`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `metadata_study_001`
--
ALTER TABLE `metadata_study_001`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
