<?php
	include("include/header.php");
	include("include/connect.php");
	$study_name = $_SESSION["study"];
	$field_name = isset($_GET['data']) ? $_GET['data'] : "";
	if($field_name!= "")
		$sql = "SELECT * FROM metadata_$study_name WHERE fieldname = '$field_name'";
	else 
		$sql = "SELECT * FROM metadata_$study_name";
	//echo $sql;	
  $result = $conn->query($sql);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Manage Metadata</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-12">

        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?>

            </h3>
          </div><!-- /.box-header -->
          
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-12 col-sm-8">
                <div class="pad">
                  <div class="row">
                    <div class="col-md-12">
                      <p class="text-center" style="font-size:18px;">Metadata History</p>
                    </div>
                  </div>
                  
                  <div class="row">
                  	<div class="col-md-2"></div>
                  	<div class="col-md-8">
                  		<div id="data">
                  			<table class="table table-hover" id="metadata">
	                  			<thead>
	                  				<tr>
	                  					<th>Field Name</th>
	                  					<th>Value</th>
	                  					<th>Action</th>
	                  					<th>Created Datetime</th>
	                  					<th>Created By</th>
	                  					<th>Updated Datetime</th>
	                  					<th>Updated By</th>
	                  					<th>New Value</th>
	                  					<th>Previous Value</th>
	                  				</tr>
	                  			</thead>
	                  			<tbody>
	                  				<?php
	                  					while($row = $result->fetch_assoc()) {?>
	                  						<tr>
	                  							<td><?php echo $row["fieldname"]?></td>
	                  							<td><?php echo $row["value"]?></td>
	                  							<td><?php echo $row["action"]?></td>
	                  							<td><?php echo $row["created_at"]?></td>
	                  							<td><?php echo $row["created_by"]?></td>
	                  							<td><?php echo $row["updated_at"]?></td>
	                  							<td><?php echo $row["updated_by"]?></td>
	                  							<td><?php echo $row["new_value"]?></td>
	                  							<td><?php echo $row["previous_value"]?></td>
	                  						</tr>
	                  					<?php }?>
	                  			</tbody>
	                  		</table>
                  		</div>
                  	</div>
                  	<div class="col-md-2"></div>
                  </div>

                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->

      </div><!-- /.col -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script>
	$(document).ready(function(){
		$('#data').after('<ul class="pagination"></ul>');
	  var rowsShown = 20;
	  var rowsTotal = $('#metadata tbody tr').length;
	  if(rowsTotal > rowsShown)
		{
			$('.pagination').append('<li class="disabled"><a href="#" rel="'+0+'"><span aria-hidden="true">&laquo;</span></a></li>');
			var numPages = rowsTotal/rowsShown;
		  for(i = 0;i < numPages;i++) {
		    var pageNum = i + 1;
		    $('.pagination').append('<li><a href="#" rel="'+i+'">'+pageNum+'</a></li>');
		  }
		  $('.pagination').append('<li><a href="#" rel="'+(i-1)+'"><span aria-hidden="true">&raquo;</span></a></li>');
		  $('#data tbody tr').hide();
		  $('#data tbody tr').slice(0, rowsShown).show();
		  $('.pagination a:first').addClass('active');
		  
		  $('.pagination a').bind('click', function(){
		    $('.pagination a').removeClass('active');
		    $('.pagination li').removeClass('disabled');
		    $(this).addClass('active');
		    $(this).parent().addClass('disabled');
		    
		    var currPage = $(this).attr('rel');
		    var startItem = currPage * rowsShown;
		    var endItem = startItem + rowsShown;
		    if((numPages-1) == currPage){
					$(this).parent().next().addClass('disabled');
				}
				
				if(currPage==0){
					$(this).parent().prev().addClass('disabled');
				}

		    $('#data tbody tr').css('opacity','0.0').hide().slice(startItem, endItem).
		      css('display','table-row').animate({opacity:1}, 300);
		  });
		}
	});
</script>

<?php
include("include/footer.php");
$conn->close();
?>
      