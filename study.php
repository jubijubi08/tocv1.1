<?php

include("include/header.php");
include("include/connect.php");
include("include/custom_funtions.php");
toc_gen($conn);
$selected_section="all";
if(isset($_POST['section_selector'])){
    $selected_section=$_POST['section_selector'];

}


if(isset($_GET['study']))
	$study_name = $_GET['study'];
else 
	$study_name = $_SESSION["study"];

$_SESSION["study"] = $study_name;

if(!isset($_SESSION["dc_selector"])){
	$dc_id = 0;	
	$_SESSION["dc_selector"] =$dc_id;
}
else {
	$dc_id = $_SESSION["dc_selector"];
}
$dc_db_val='SP'.$dc_id;


if(isset($_SESSION["filter_dc_selector"])){
	$dc2_id = $_SESSION["filter_dc_selector"];	
}
else {
	$dc2_id = $dc_id;
    $_SESSION["filter_dc_selector"] = $dc2_id;
}

date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");

//delete temp pdf and rtf and sas
$files = glob('./temp/pdf/*');   // get all file names
foreach($files as $file){        // iterate files
	if(is_file($file)){
	  	unlink($file);           // delete file
	  	//print_r($file);
	}
}

$files = glob('./temp/rtf/*');   // get all file names
foreach($files as $file){        // iterate files
	if(is_file($file)){
	  	unlink($file);           // delete file
	  	//print_r($file);
	}
}

$files = glob('./temp/sas/*'); // get all file names
foreach($files as $file){ 	   // iterate files
	if(is_file($file)){
	  	unlink($file);        // delete file
	  	//print_r($file);
	}
}

//retrieve study location from database
$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
while($row = $result44->fetch_assoc()) {
	$file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
}

//retrieve available section
$section_list=$conn->query("SELECT DISTINCT section FROM toc_$study_name WHERE l2 != '00' and section != ''  ");


//update program status and output status if program physical file does not exist
$result45=$conn->query("SELECT DISTINCT pgmname,pgmloc FROM toc_$study_name WHERE pgmname != '' AND pgmloc != '' AND data_currency='SP0' ");
while($row = $result45->fetch_assoc()) {
	$pgmname = $row['pgmname'];
	$pgmloc = $row['pgmloc'];

	$actual_file_loc = $file_server.$study_name.'/'.$pgmloc.'/'.$pgmname;

	if(!file_exists($actual_file_loc)){
		//echo "this file not exist:".$actual_file_loc."<br>";
		$update_status_pgm_out = $conn->query("UPDATE toc_status_$study_name SET pgmstat = 0 , outstat = 0 where sortorder  IN (SELECT sortorder FROM toc_$study_name  WHERE pgmname = '$pgmname' AND data_currency = 'SP0') AND data_currency='SP0' ");
        $update_status_in_toc = $conn->query("UPDATE toc_$study_name SET status = ' ' where sortorder IN (SELECT sortorder FROM toc_status_$study_name  WHERE pgmstat = 0 AND outstat = 0 AND data_currency='SP0') AND data_currency='SP0'");
		// if($update_status_in_toc){ echo "status updated for: ". $pgmname;} else { echo "status var update failed";}
		 //echo "pgm name from database : ".$pgmname;
	}
}

//Check if study have the tree or not
$study_name=strtolower($study_name);
$table="toc_$study_name";
//$result = mysql_query("SHOW TABLES FROM test");
$result=$conn->query("SHOW TABLES");
while($row = mysqli_fetch_row($result)){
    $arr[] = $row[0];
}

if(in_array($table,$arr)){
  	$tree_exist=1;
}
else{
	$tree_exist=0;
}
$study_name=strtoupper($study_name);

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Study </li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<div class="col-md-12" >
				<!-- MAP & BOX PANE -->
				<div class="box box-success">

					<div class="box-header with-border">

						<div class="box-tools pull-left col-md-4" style="position: initial;">
							<div class="row">
								<div class="col-md-12">
									<h3 class="box-title">Study Name: <?php echo $_SESSION["study"]; ?>&nbsp; &nbsp; &nbsp; &nbsp;
										<a href="#" data-toggle="tooltip" title="Refresh Tree"><i class="fa fa-refresh" aria-hidden="true" onclick="reload_tree();"></i></a>
										<i id="lodspin" class="fa-li fa fa-spinner fa-spin" style="position: initial; visibility: hidden"></i>

									</h3>
								</div>
							</div>

								<br>
							
							<div class="row">
								<div class="col-md-12">
									<label><h3 class="box-title"> Select Data Currency: </h3> </label>

									<select id="dc_selector" name="dc_selector" onchange="reload_tree_dc(this)">
										<?php
										while($row = $snap_list->fetch_assoc()) {
											$dc_id_c = $row['id'];
											$sp_name = $row['snap_name'];

											if($dc_id_c==$dc_id){
												echo "<option value='".$dc_id_c."' selected='selected'>".$sp_name."</option>";
												$sp_name_active=$sp_name;
											}else{
												echo "<option value=".$dc_id_c.">".$sp_name."</option>";
											}

										}

										?>


									</select>
								</div>
								<!-- 	<div class="col-md-3">
									<button type="submit" id="ellBtn" class="btn btn-block btn-default btn-xs"  >View</button>
								</div> -->
							</div>
							
						</div>
                                                   

						<div class="box-tools pull-right" style="position: initial;">
						<?php
							if($tree_exist==1){
//								$sql_status=$conn->query("SELECT COUNT( DISTINCT pgmname) AS totalpgm FROM toc_$study_name Where section ='' AND data_currency='C' ");
//
//								while($row = $sql_status->fetch_assoc()) {
//									$totalpgm = $row['totalpgm'];
//								}
								$sql_status=$conn->query("SELECT COUNT( * ) AS totalout FROM toc_$study_name Where section ='' AND data_currency='$dc_db_val' ");

								while($row = $sql_status->fetch_assoc()) {
									$totalpgm = $row['totalout'];
									//$totalout = $row['totalout'];
								}
								$sql_status=$conn->query("SELECT   COUNT(*) AS nopgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 0 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $nopgm = $row['nopgm'];
							    }
							    $sql_status=$conn->query("SELECT   COUNT(*) AS indevpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x  WHERE pgmstat = 1 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $indevpgm = $row['indevpgm'];
							    }
							    $sql_status=$conn->query("SELECT   COUNT(*) AS tvalpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 2 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $tvalpgm = $row['tvalpgm'];
							    }
							    $sql_status=$conn->query("SELECT   COUNT(*) AS valpgm FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE pgmstat = 3 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $valpgm = $row['valpgm'];
							    }
							    $sql_status=$conn->query("SELECT   COUNT(*) AS nout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 0 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $nout = $row['nout'];
							    }
							    $sql_status=$conn->query("SELECT   COUNT(*) AS oout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 1 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $oout = $row['oout'];
							    }
							    $sql_status=$conn->query("SELECT   COUNT(*) AS cout FROM (SELECT a.section,b.* FROM toc_$study_name as a , toc_status_$study_name as b WHERE a.sortorder = b.sortorder  AND a.data_currency=b.data_currency AND a.section ='') as x WHERE outstat = 2 AND data_currency='$dc_db_val' ");

							    while($row = $sql_status->fetch_assoc()) {
		                            $cout = $row['cout'];
							    }
							    echo '
                                <form action="study.php" method="POST">
							    <label><h3 class="box-title">Select Section:</h3></label>
                                
								<select id="section_selector" name="section_selector" onchange="this.form.submit()">
									<option value="all" selected="selected">all</option>';

										while($row = $section_list->fetch_assoc()) {
										    $section = $row['section'];
										    if($section == $selected_section)
										        echo '<option  value="'.$section.'" selected>'.$section.'</option>';
                                            else
											    echo '<option  value="'.$section.'">'.$section.'</option>';
										}


                                echo '</select></form>
								
								<h3 class="box-title">Total Number of Outputs: <kbd id="totalpgm">'.$totalpgm.'</kbd></h3>

								 <table style="width:100%">
									  <tr>
										<td><span class="description-percentage text-black" >&nbsp;<img src="stree/metroStyle/img/p0.png"> No Program &nbsp;</span></td>
										<td><kbd id="nopgm">'.$nopgm.'</kbd></td>
										<td><span class="description-percentage text-yellow" >&nbsp;<img src="stree/metroStyle/img/p1.png"> &nbsp;In Development&nbsp;</span></td>
										<td><kbd id="indevpgm">'.$indevpgm.'</kbd></td>
										<td><span class="description-percentage text-red" >&nbsp;<img src="stree/metroStyle/img/p2.png">&nbsp; To Be Validated &nbsp;</span></td>
										<td><kbd id="tvalpgm">'.$tvalpgm.'</kbd></td>
										<td><span class="description-percentage text-green" >&nbsp;<img src="stree/metroStyle/img/p3.png"> &nbsp;Validated&nbsp; </span></td>
										<td><kbd id="valpgm">'.$valpgm.'</kbd></td>
									  </tr>
									  <tr>
										<td><span class="description-percentage text-black" >&nbsp;<img src="stree/metroStyle/img/o0.png"> &nbsp;No Output&nbsp; </span></td>
										<td><kbd id="nout">'.$nout.'</kbd></td>
										<td><span class="description-percentage text-red" >&nbsp;<img src="stree/metroStyle/img/o1.png"> &nbsp;Old Output&nbsp; </span></td>
										<td><kbd id="oout">'.$oout.'</kbd></td>
										<td><span class="description-percentage text-green" >&nbsp;<img src="stree/metroStyle/img/o2.png"> &nbsp;Current Output&nbsp; </span></td>
										<td><kbd id="cout">'.$cout.'</kbd></td>
									  </tr>
									</table>

								';
							}



						?>
					    </div>
					</div><!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="row">
							<div class="col-md-12 col-sm-12">
								<div id="clickevt" class="pad" style="padding: 10px; overflow-x: auto;">									
									<?php
									
										if($tree_exist==0){
											echo '<div class="alert alert-danger" role="alert">
											<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
											<span class="sr-only">Error:</span>
											No TOC Available  for this Study!
											</div>';
										}


									?>
										<div class="row">
											<div class="col-md-3 col-sm-3">
												<button id="expandAllBtn" class="btn btn-block btn-default btn-xs" onclick="return false;" >Expand All</button>
												<button id="collapseAllBtn" class="btn btn-block btn-default btn-xs" onclick="return false;">Collasped All</button>
											</div>
											<div class="col-md-3 col-sm-3">
												<button id="expandBtn" class="btn btn-block btn-default btn-xs" onclick="return false;">Expand Sections</button>
												<button id="collapseBtn" class="btn btn-block btn-default btn-xs" onclick="return false;">Collasped Sections</button>
											</div>
										
										<?php
									
										if($_SESSION["dc_selector"]==0){
											echo '<div class="col-md-2 col-sm-3">
													<button id="autogen" class="btn btn-block btn-default btn-xs" style="padding: 6px" onclick="run_autosas();">Auto Generate <br>Programs</button>
												</div>';
												}
										?>
										
										</div>
										<br>
								<div class="row">
									<div class="col-md-4">

										<span>Active Data Currecncy : <?php echo $sp_name_active;?> </span>
									</div>

                                <?php 

                                	if ($dc_id != 0) {
                                		
	                                    echo '<div class="col-md-6"><span>';
	                                    if ($_SESSION["dc_selector"]==$_SESSION["filter_dc_selector"]) {
	                                    	
										echo '<label class="radio-inline"><input type="radio" name="optradio" onchange="filter_dc(this.value);" value="0" > Show all  </label>';
										echo '<label class="radio-inline"><input type="radio"  checked="checked" name="optradio" selected onchange="filter_dc(this.value);" value='.$dc_id.'> Show only selected Data Currency </label>';
										
	                                    }else{

										echo '<label class="radio-inline"><input type="radio" checked="checked" name="optradio" onchange="filter_dc(this.value);" value="0" > Show all  </label>';
										echo '<label class="radio-inline"><input type="radio"   name="optradio" selected onchange="filter_dc(this.value);" value='.$dc_id.'> Show only selected Data Currency </label>';
										
	                                    }
										echo '</span></div>';	
									}


								?>		
								</div>				
										

										<ul id="treeDemo" class="ztree" style="-moz-user-select: none; user-select: none; width: auto; width: -moz-fit-content; height: auto; border: 1px solid #F0F9F8;
										background: #FFF none repeat scroll 0% 0%;"></ul>
                                    <?php if ($dc_id != $dc2_id) { echo '
                                    <label><p class="box-title">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;* Gray Colored entries came from Base data currency </p> </label>
                                    ';} ?>


										<div id="rMenu" style="background-color: #ffffff;z-index:1000;">
												<ul  style=" padding: 0px;">
														<?php
															if ($usertype==3) {
																	echo '
																		<li id="m_out" onclick="showout();">View output</li>
																		<li id="m_pdf" onclick="pdf_create();">Create PDF</li>
																		<li id="m_rtf" onclick="rtf_create();">Create RTF</li>

																	';
															}
															else if ($usertype==2) {

																if ($_SESSION["dc_selector"]==0) {
																	echo '
																		<li id="m_run_sec" onclick="runpgm(this,3);">Run Section</li>
																		<li id="m_run" onclick="runpgm(this,1);" >Run Program</li>
																		<li id="m_out" onclick="showout();">View output</li>
																		<li id="m_log" onclick="showlog();">View log</li>
																		<li id="m_pdf" onclick="pdf_create();">Create PDF</li>
																		<li id="m_rtf" onclick="rtf_create();">Create RTF</li>
																		<li id="m_val" onclick="validation();" >Validation Status</li>
																		<li id="m_phist" onclick="program_history();">Program History</li>
																		<li id="m_up" onclick="uploadp();" data-toggle="modal" data-target=".bs-example-up-modal-lg">Upload Program</li> 
																		<li id="m_dl" onclick="downloadp();">Download Program</li>
																		<li id="m_edit" onclick="editentr();" data-toggle="modal" data-target=".bs-example-edit-modal-lg">Edit Entry</li> 
																		<li id="m_editp" onclick="editpgm();">Edit Program</li>

																	';
																}else{
																	echo '
																		<li id="m_run_sec" onclick="runpgm(this,3);">Run Section</li>
																		<li id="m_run" onclick="runpgm(this,1);" >Run Program</li>
																		<li id="m_out" onclick="showout();">View output</li>
																		<li id="m_log" onclick="showlog();">View log</li>
																		<li id="m_pdf" onclick="pdf_create();">Create PDF</li>
																		<li id="m_rtf" onclick="rtf_create();">Create RTF</li>
																		<li id="m_phist" onclick="program_history();">Program History</li>
																		<li id="m_dl" onclick="downloadp();">Download Program</li>

																	';
																}

															}
															else {

																if ($_SESSION["dc_selector"]==0) {
																	echo'
																		<li id="m_run_sec" onclick="runpgm(this,3);">Run Section</li>
																		<li id="m_run" onclick="runpgm(this,1);" >Run Program</li>
																		<li id="m_out" onclick="showout();">View output</li>
																		<li id="m_log" onclick="showlog();">View log</li>
																		<li id="m_val" onclick="validation();">Validation Status</li>
																		<li id="m_phist" onclick="program_history();" >Program History</li>
																		<li id="m_up" onclick="uploadp();" data-toggle="modal" data-target=".bs-example-up-modal-lg">Upload Program</li> 
																		<li id="m_dl" onclick="downloadp();">Download Program</li>
																		<li id="m_edit" onclick="editentr();" data-toggle="modal" data-target=".bs-example-edit-modal-lg">Edit Entry</li> 
																		<li id="m_editp" onclick="editpgm();">Edit Program</li>
																		<li id="m_pdf" onclick="pdf_create();">Create PDF</li>
																		<li id="m_rtf" onclick="rtf_create();">Create RTF</li>
																		<li id="m_sec" onclick="new_sec();" data-toggle="modal" data-target=".bs-example-new-sec-modal-lg">Create new section</li>
									                                    <li id="m_sub_sec" onclick="new_sub_sec();" data-toggle="modal" data-target=".bs-example-new-sub-sec-modal-lg">Create new sub-section</li>
									                                    <li id="m_entry1" onclick="new_entry();" data-toggle="modal" data-target=".bs-example-new-entry-modal-lg">Create Entry Before</li>
									                                    <li id="m_entry2" onclick="new_entry();" data-toggle="modal" data-target=".bs-example-new-entry-modal-lg">Add a Last Entry In This Section</li>
									                                    <li id="m_move" onclick="move_sec();">Move Section</li>
									                                    <li id="m_update_sechead" onclick="update_sechead();" data-toggle="modal" data-target=".bs-example-update_sechead-modal-lg">Update Section Header</li>
									                                    <li id="m_del_sec" onclick="del_sec1();" data-toggle="modal" data-target=".bs-example-delcon-modal-sm">Delete Section</li>
									                                    <li id="m_del_ent" onclick="del_sec1();" data-toggle="modal" data-target=".bs-example-delcon-modal-sm">Delete Entry</li>
									                                                                           
																	';
																}else{
																	echo'
																		<li id="m_run_sec" onclick="runpgm(this,3);">Run Section</li>
																		<li id="m_run" onclick="runpgm(this,1);" >Run Program</li>
																		<li id="m_out" onclick="showout();">View output</li>
																		<li id="m_log" onclick="showlog();">View log</li>
																		<li id="m_phist" onclick="program_history();" >Program History</li>
																		<li id="m_dl" onclick="downloadp();">Download Program</li>
																		<li id="m_pdf" onclick="pdf_create();">Create PDF</li>
																		<li id="m_rtf" onclick="rtf_create();">Create RTF</li>
									                                                                           
																	';
																}


															}
														?>

												</ul>
										</div>



									<div class="row">

									</div>

								</div>
							</div><!-- /.col -->
						</div><!-- /.row -->
					</div><!-- /.box-body -->

				</div><!-- /.box -->
			</div><!-- /.col -->          
		</div><!-- /.row (main row) -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      