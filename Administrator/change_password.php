<?php
	include("include/header.php");
	include("include/connect.php");	
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <ol class="breadcrumb">
    <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
    <li class="active">Change password</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <!-- Main row -->
  	<div class="row">
    <!-- Left col -->
    	<div class="col-md-12">
      	<!-- MAP & BOX PANE -->
      		<div class="box box-success">
        		<div class="box-header with-border">
          			<h3 class="box-title">Change password</h3>
          			<div class="box-tools pull-right"></div>
        		</div><!-- /.box-header -->
        		<div class="box-body no-padding">
          			<div class="row">
                        <div class="col-md-3"></div>

                        <div class="col-md-6">
              				<div class="pad">
			              		<?php 

								 	if($_SERVER["REQUEST_METHOD"] == "POST"){
										$username = $_POST['user_name'];
										//echo $username;
								        $newpassword = md5($_POST['new_pass']);
								        $confirmnewpassword = md5($_POST['confirm_pass']);

										$sql="SELECT password FROM user_info WHERE username = '$username'";
										$result=$conn->query($sql);
										while($row = $result->fetch_assoc()) {
								            $userPass = $row['password']; 
								        }
										$password = $userPass;

										if($password == $userPass){
											if($newpassword == $confirmnewpassword){
												$sql_update="UPDATE user_info SET password = '$newpassword' where username = '$username'";
												$result_update=$conn->query($sql_update);
												if($result_update){
													$sucess="Password updated successfully";
													echo "<div class='alert alert-success'>
	                                                  <strong>Success!</strong> $sucess.
	                                                </div>";
												}
											}
											else{
												$error="Your confirmed password does not matched!!";
											    echo "<div class='alert alert-warning'>
                                                <strong>Warning!</strong> $error.
                                                </div>";				
											}
										}
										else{
											$error="Entered current password is wrong";
											echo "<div class='alert alert-warning'>
                                                <strong>Warning!</strong> $error.
                                              </div>";
										}
								        
							   		}    

								?>
				                        
				                <form action="change_password.php" method= "post">

									<div class="form-group has-feedback">
				                       <p>User Name :</p>
									   <input type="text" name="user_name" id="user_name" class="form-control" onkeyup="checkusername(); return false;"   required placeholder="User name" value="<?php echo $username; ?>"/>
									   <span class="glyphicon  form-control-feedback"></span>
									   <span id="username_confirmMessage" class="username_confirmMessage"></span>
								   </div>

									<div class="form-group has-feedback">
				                        <p>Current Password :</p>
										<input type="password" name="old_pass" id="old_pass" class="form-control" required placeholder="Current Password" value="" />
										<span class="glyphicon  form-control-feedback"></span>
										<span id="curr_pass_alrt" class="curr_pass_alrt"></span>
									</div>
								
									<div class="form-group has-feedback">
				                        <p>New Password :</p>
										<input type="password" name="new_pass" id="new_pass" class="form-control" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Password Should be contained (UpperCase, LowerCase, Number/SpecialChar and min 8 characters)" required  placeholder="New Password"/>
										<span class="glyphicon  form-control-feedback"></span>
									</div>
							
									<div class="form-group has-feedback">
				 					    <p>Confirm Password :</p>	
										<input type="password" name="confirm_pass" id="confirm_pass" onkeyup="checkPass(); return false;" class="form-control" required placeholder="Confirm Password"/>
										<span class="glyphicon  form-control-feedback"></span>
										<span id="confirmMessage" class="confirmMessage"></span>
									</div>
								
									<div class="row">
										<div class="col-xs-4">
										  	<button type="submit" class="btn btn-primary btn-block">Update password</button>
										</div>
									</div>
								
								</form>

			              	</div>
			            </div><!-- /.col -->
                        <div class="col-md-3"></div>

                    </div><!-- /.row -->
		        </div><!-- /.box-body -->
	      	</div><!-- /.box -->
	    </div><!-- /.col -->          
  	</div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<script type="text/javascript">
	function checkPass(){
		//Store the password field objects into variables ...
		var pass1 = document.getElementById('new_pass');
		var pass2 = document.getElementById('confirm_pass');
		//Store the Confimation Message Object ...
		var message = document.getElementById('confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#ffffff";
		var goodColorf = "#66cc66";
		var badColor = "#ff6666";
		//Compare the values in the password field
		//and the confirmation field
		if(pass1.value == pass2.value && pass2.value != ""){
			//The passwords match.
			//Set the color to the good color and inform
			//the user that they have entered the correct password
			pass2.style.backgroundColor = goodColor;
			message.style.color = goodColorf;
			message.innerHTML = "Passwords Match!";
		}else{
			//The passwords do not match.
			//Set the color to the bad color and
			//notify the user.
			pass2.style.backgroundColor = badColor;
			message.style.color = badColor;
			if(pass2.value == "")
                message.innerHTML = "Empty field!";
            else
                message.innerHTML = "Passwords Do Not Match!";
		}
	}

	function checkPass1(){
	    //Store the password field objects into variables ...
	    var pass0 = document.getElementById('old_pass');
	    //alert(pass0);
	    var pass1 = document.getElementById('new_pass');
	    //Store the Confimation Message Object ...
	    var new_pass_message = document.getElementById('new_pass_alrt');
	    //Set the colors we will be using ...
	    var goodColor = "#ffffff";
	    var goodColorf = "#66cc66";
	    var badColor = "#ff6666";
	    //Compare the values in the password field
	    //and the confirmation field
        if(pass0.value == pass1.value){
	        //The passwords match.
	        //Set the color to the good color and inform
	        //the user that they have entered the correct password
	        pass1.style.backgroundColor = badColor;
	        new_pass_message.style.color = badColor;
	        new_pass_message.innerHTML = "You can not use previous password!";
	        pass1.innerHTML = " ";
	    }
	    else{
	        pass1.style.backgroundColor = goodColor;
	        new_pass_message.innerHTML = " ";
	    }
	} 

	function checkusername(){
		//Store the password field objects into variables ...
		var name = document.getElementById('user_name').value;
		var name1 = document.getElementById('user_name');
		//Store the Confimation Message Object ...
		var message = document.getElementById('username_confirmMessage');
		//Set the colors we will be using ...
		var goodColor = "#ffffff";
		var goodColorf = "#66cc66";
		var badColor = "#ff6666";

		var data = {
         "name": name
	    };
	    $.ajax({
	        type: "POST",
	        dataType: "json",
	        url: "../ajax/username_chk.php", //Relative or absolute path to response.php file
	        data: data,
	        success: function(response) {
	            if(response.exist=="No"){
	            	name1.style.backgroundColor = badColor;
					message.style.color = badColor;
					message.innerHTML = "Please type correct username, you entered wrong user name!!";
					$("#old_pass").val("");
	            }
	            else{
					name1.style.backgroundColor = goodColor;
					message.style.color = goodColorf;
					message.innerHTML = "";
					$("#old_pass").val(response.pass);
	            }
	        }
	    });
	}    
</script>
<?php
include("include/footer.php");
?>
      