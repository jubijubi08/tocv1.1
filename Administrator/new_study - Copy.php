<?php
include("include/header.php");
error_reporting(E_ALL ^ E_NOTICE);

require 'include/Classes/PHPExcel.php';
include("include/Classes/PHPExcel/IOFactory.php");
include("include/connect.php");
date_default_timezone_set('Asia/Dhaka');
$today = date("Y-m-d H:i:s");

?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">New study</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

  <!-- Main row -->
  <div class="row">
  <!-- Left col -->
    <div class="col-md-8">
    <!-- MAP & BOX PANE -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">New study</h3>
          <div class="box-tools pull-right">
          </div>
        </div><!-- /.box-header -->
      <div class="box-body no-padding">
        <div class="row">
          <div class="col-md-9 col-sm-8">
            <div class="pad">
              <form action="new_study.php" method="post" enctype="multipart/form-data">

                <div class="form-group has-feedback">
                    <p>Enter Study Folder Name:</p>
                    <input type="text" required name="study_name" id="study_name" class="form-control" title="Please enter study name" onchange="chk_study()" value="<?php //echo set_value('fullname');  ?>" placeholder="Enter Study Folder Name"/>
                </div>

                  <div class="form-group has-feedback">
                      <p>Enter Study Folder Location:</p>
                      <input type="text" required name="study_loc" id="study_loc" class="form-control" title="Please enter study folder location"  value="<?php //echo set_value('fullname');  ?>" placeholder="Enter Study Folder Location here"/>
                  </div>

                  <div class="form-group has-feedback">
                  <p>Select TOC file:</p>
                  <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-primary btn-file"><span class="fileupload-new">Select file</span>
                    <input  type="file" name="fileToUpload" id="fileToUpload"></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-xs-8">
                      <div class="checkbox icheck">
                      </div>
                  </div><!-- /.col -->
                  <div class="col-xs-4">
                      <input type="submit" class="btn btn-primary btn-block btn-flat" value="Create Study" name="submit">
                  </div><!-- /.col -->
                </div>
              </form>
            </div>

            <?php
              if(isset($_POST["submit"])) {
                $uploadOk = 0;
                $study_name=$_POST["study_name"];
                $study_loc=$_POST["study_loc"];
                $target_dir = $file_server.$study_name."/";
                  if ($_FILES["fileToUpload"]["name"] != "") {
                    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                    $uploadOk = 1;
                    $FileType = pathinfo($target_file, PATHINFO_EXTENSION);
                    // Check if file already exists
                    if (file_exists($target_file)) {
                      echo '<div class="alert alert-info" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Info:</span>
                            One older meta file found and replaced with new meta file. !
                            </div>';
                      $uploadOk = 1;
                    }
                    // Check file size
                    if ($_FILES["fileToUpload"]["size"] > 500000) {
                      echo "Sorry, your file is too large.";
                      $uploadOk = 0;
                    }
                    // Allow certain file formats
                    if ($FileType != "xls" ) {
                      echo '<div class="alert alert-danger" role="alert">
                            <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                            <span class="sr-only">Error:</span>
                            Sorry, Only xls files are allowed !
                            </div>';
                      $uploadOk = 0;
                    }
                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {
                        echo '<div class="alert alert-danger" role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                          Sorry, your file was not uploaded !
                          </div>';
                        // if everything is ok, try to upload file
                    } 
                    else {
                        if(file_exists($target_dir)){

                          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                              echo "<div class='alert alert-success' role='alert'>The file ". basename($_FILES["fileToUpload"]["name"]) ." has been uploaded.</div>";
                              $uploadOk = 1; 
                          } 
                          else {
                              echo '<div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    Sorry, there was an error uploading your file !
                                    </div>';
                              $uploadOk = 0;     
                          }
                        }
                        else{
                          echo '<div class="alert alert-danger" role="alert">
                                    <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                    <span class="sr-only">Error:</span>
                                    Sorry, Your study folder does not exist in server, Please create study folder first.
                                    like  "study/'.$study_name.'"
                                    </div>';
                              $uploadOk = 0;   
                        }  
                    }
                  }
                  else {
                    echo '<div class="alert alert-danger" role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Error:</span>
                          No file selected !
                          </div>';
                  }

//                ////Create tables: toc status in database!!
                 if ($uploadOk == 1) {

                   $result = $conn->query("CREATE TABLE IF NOT EXISTS toc_status_".$study_name." (
                                        id int(11) NOT NULL AUTO_INCREMENT,
                                        data_currency varchar(200) ,
                                        study varchar(20) NOT NULL default '',
                                        sortorder varchar(10) ,
                                        pgmstat int(10) ,
                                        pgmstatdc varchar(50) ,
                                        pdate_0  datetime NOT NULL ,
                                        pdate_1  datetime NOT NULL ,
                                        pdate_2  datetime NOT NULL ,
                                        pdate_3  datetime NOT NULL ,
                                        outstat int(10) ,
                                        outstatdc varchar(50),
                                        odate_0  datetime NOT NULL ,
                                        odate_1  datetime NOT NULL ,
                                        odate_2  datetime NOT NULL ,

                                       PRIMARY KEY  (id)
                                   )");
                   if ($result) {
                     echo "<div class='alert alert-success' role='alert'>The table toc_status_$study_name has been created in database.</div>";
                   } else {
                     echo '<div class="alert alert-danger" role="alert">
                           <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                           <span class="sr-only">Error:</span>
                           Sorry, failed to create toc status table!
                           </div>';
                   }
                     ////Create tables: toc program history table  in database!!
                   $result = $conn->query("CREATE TABLE IF NOT EXISTS pgm_hist_".$study_name." (
                                        id int(10) NOT NULL AUTO_INCREMENT,
                                        sortorder varchar(10) ,
                                        pgmname varchar(50) NOT NULL default '' ,
                                        event_date  datetime NOT NULL ,
                                        username varchar(50),
                                        comment text NOT NULL,
                                        status varchar(100),
                                        link varchar(100),
                                        validation_system varchar(500),
                                        data_currency varchar(200),


                                       PRIMARY KEY  (id)
                                   )");
                   if ($result) {
                     echo "<div class='alert alert-success' role='alert'>The table pgm_hist_$study_name has been created in database.</div>";
                   } else {
                     echo '<div class="alert alert-danger" role="alert">
                           <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                           <span class="sr-only">Error:</span>
                           Sorry, failed to create pgm history table!
                           </div>';
                   }

                    ////Create tables: snapshot history table in database!!
                   $result = $conn->query("CREATE TABLE IF NOT EXISTS snap_".$study_name." (
                                        uid int(8) NOT NULL AUTO_INCREMENT,
                                        id int(10) NOT NULL ,
                                        snap_name varchar(200) ,
                                        lib_name varchar(200) ,
                                        Lib_loc varchar(200) ,
                                        is_lock enum('yes', 'no'),
                                        updated_at  datetime NOT NULL ,
                                        updated_by varchar(200) ,
                                        created_at  datetime NOT NULL ,
                                        created_by varchar(200) ,


                                       PRIMARY KEY  (uid)
                                   )");
                   if ($result) {
                     echo "<div class='alert alert-success' role='alert'>The table snap_$study_name has been created in database.</div>";
                   } else {
                     echo '<div class="alert alert-danger" role="alert">
                           <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                           <span class="sr-only">Error:</span>
                           Sorry, failed to create snap study table table!
                           </div>';
                   }

                   ////Create tables: toc program history table  in database!!
                   $result = $conn->query("CREATE TABLE IF NOT EXISTS cpgm_hist_".$study_name." (
                                        id int(10) NOT NULL AUTO_INCREMENT,
                                        sortorder varchar(10) ,
                                        pgmname varchar(50) NOT NULL default '' ,
                                        event_date  datetime NOT NULL ,
                                        username varchar(50),
                                        comment text NOT NULL,
                                        status varchar(100),
                                        link varchar(100),
                                        validation_system varchar(500),


                                       PRIMARY KEY  (id)
                                   )");
                   if ($result) {
                     echo "<div class='alert alert-success' role='alert'>The table cpgm_hist_$study_name has been created in database.</div>";
                   } else {
                     echo '<div class="alert alert-danger" role="alert">
                           <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                           <span class="sr-only">Error:</span>
                           Sorry, failed to create cpgm history table!
                           </div>';
                   }

                    ////Create tables: common program  table  in database!!
                   $result = $conn->query("CREATE TABLE IF NOT EXISTS cplist_".$study_name." (
                                        study varchar(20) NOT NULL default '',
                                        sortorder int(10) NOT NULL AUTO_INCREMENT,
                                        cploc varchar(200) NOT NULL default '' ,
                                        cpname varchar(50) NOT NULL default '' ,
                                        programmer varchar(200) NOT NULL default '' ,
                                        cpdate  datetime NOT NULL ,
                                        pgmstat  int(10) ,
                                        status  varchar(50) NOT NULL default '',
                                        validator   varchar(200) NOT NULL default '' ,
                                        status_date  datetime NOT NULL ,


                                       PRIMARY KEY  (sortorder)
                                   )");
                   if ($result) {
                       echo "<div class='alert alert-success' role='alert'>The table cplist_$study_name has been created in database.</div>";
                   } else {
                       echo '<div class="alert alert-danger" role="alert">
                             <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                             <span class="sr-only">Error:</span>
                             Sorry, failed to create common pgm table!
                             </div>';
                   }
                   ////create table for meta history
                     $result = $conn->query("CREATE TABLE IF NOT EXISTS metadata_".$study_name." (
                                              id int(11) NOT NULL,
                                              fieldname varchar(100) NOT NULL,
                                              searchtitle varchar(250) DEFAULT NULL,
                                              value varchar(250) NOT NULL,
                                              action enum('New','Updated','Deleted') NOT NULL,
                                              is_current enum('YES','NO') NOT NULL,
                                              created_at datetime DEFAULT NULL,
                                              created_by varchar(50) DEFAULT NULL,
                                              updated_at datetime DEFAULT NULL,
                                              updated_by varchar(50) DEFAULT NULL,
                                              new_value varchar(250) DEFAULT NULL,
                                              previous_value varchar(250) DEFAULT NULL
                                       
                                   )");
                     if ($result) {
                         echo "<div class='alert alert-success' role='alert'>The table metadata_$study_name has been created in database.</div>";
                     } else {
                         echo '<div class="alert alert-danger" role="alert">
                           <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                           <span class="sr-only">Error:</span>
                           Sorry, failed to create toc status table!
                           </div>';
                     }

                 }

                ////insert toc content into toc study table and toc status table!!
                if ($uploadOk == 1) {
                  $columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
  "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
  "BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
  "CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ");


                  $inputFileType = 'Excel5';
                  $inputFileName = $target_file;
                  $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                  $objPHPExcel = $objReader->load($inputFileName);

                  $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                  
                  $total_row = $objPHPExcel->setActiveSheetIndex(0)->getHighestRow();
                  $total_col = $objPHPExcel->setActiveSheetIndex(0)->getHighestColumn();
                  $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($total_col);
                  //echo "row: ".$total_row." col: ".$highestColumnIndex;


                    $db_variable_list_all0 = "id int(11) NOT NULL AUTO_INCREMENT, data_currency varchar(200) NOT NULL default '',";
                    $db_variable_list_all = "";
                    $db_variable_list_all2 = "";
                  for ($row = 1; $row < 2; $row++){

                    for($col = $highestColumnIndex-1; $col > -1; $col--){
                      $cell = $objPHPExcel->setActiveSheetIndex(0)->getCellByColumnAndRow($col, $row);
                      $variable_list= strtolower($val = $cell->getValue());
                 
                        if($col==0){
                            $db_variable_list_all = "sortorder"." varchar(10) NOT NULL default '', ".$db_variable_list_all;
                        }
                      $db_variable_list_all = $variable_list." varchar(200) NOT NULL default '', ".$db_variable_list_all;
                      $db_variable_list_all2 = $variable_list.",".$db_variable_list_all2;
                    }

                  }
                  $db_variable_list_all =$db_variable_list_all0.$db_variable_list_all;

                  //echo $db_variable_list_all;

                  //$study_name="test0187956";
                  //Create tables: toc  table  in database!!
                   $result = $conn->query("CREATE TABLE IF NOT EXISTS toc_$study_name (
                                          ".$db_variable_list_all."

                                         PRIMARY KEY  (id)
                                     )");
                   if ($result) {
                     echo "<div class='alert alert-success' role='alert'>The table toc_$study_name has been created in database.</div>";
                   }
                   else {
                     echo '<div class="alert alert-danger" role="alert">
                           <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                           <span class="sr-only">Error:</span>
                           Sorry, failed to create toc table!
                           </div>';
                   }

                  for ($row = 2; $row < $total_row + 1; $row++) {
                      $sql_values = " ";
                      $sort="";

                      for ($col = $highestColumnIndex-1; $col > -1; $col--) {
                        $cell = $objPHPExcel->setActiveSheetIndex(0)->getCellByColumnAndRow($col, $row);
                        $filter_value=$cell->getValue();
                        $filter_value1 = str_replace("'","''",$filter_value);
                        if($col>0 && $col<6){
                          if(iconv_strlen (trim($filter_value1))==1){
                            $filter_value1=trim("0".$filter_value1);
                          }
                            if(iconv_strlen (trim($filter_value1))==0){
                            $filter_value1=trim("00".$filter_value1);
                          }
                          $sort=$filter_value1.$sort;
                        }
                          // echo $sort."<BR>";

                        $sql_values = "'" . $filter_value1 . "'" . "," . $sql_values;
                      }
                      $sql_values2 = substr(trim($sql_values),0,-1);
                    
                     //echo "<BR>".$sql_values2."===>>".$sort;
                  
                   
                        /////insert values from metafile to database in toc table
                       $result = $conn->query("INSERT INTO toc_$study_name
                                             ($db_variable_list_all2 sortorder , data_currency) VALUES ($sql_values2,'$sort','SP0')");
//                        if ($result) {
//                            echo "INSERTED <BR>";
//                        }else{
//                            echo "FAILED <BR>";
//                        }
                    }

                     if ($result) {
                         echo "<div class='alert alert-success' role='alert'>TOC Table: Data has been inserted into database.</div>";
                     } else {
                         echo '<div class="alert alert-danger" role="alert">
                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                               <span class="sr-only">Error:</span>
                               TOC Table: Sorry, failed to insert !
                               </div>';
                     }


                  ////insert into toc status table
                    $sql1="SELECT * FROM toc_$study_name";
                    $result1 = $conn->query($sql1);
                    // print_r($result);
                     while($row = $result1->fetch_assoc()) {

                     $sortorder=$row['sortorder'];
                     $study=$row['study'];
                     //echo $sortorder."<BR>";

                     date_default_timezone_set('Asia/Dhaka');
                     $today = date("Y-m-d H:i:s");
                                 $var2 = "'SP0','".$study."','".$sortorder."','0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today'";
                                 // echo "<h3>".$var2."</h3>";

                                  $result5 = $conn->query("INSERT INTO toc_status_".$study_name."
                                   (data_currency,study,sortorder,pgmstat,pgmstatdc,pdate_0,pdate_1,pdate_2,pdate_3,outstat
                                    ,outstatdc,odate_0,odate_1,odate_2) VALUES (".$var2.")");


                     }
                     if ($result5) {
                         echo "<div class='alert alert-success' role='alert'>TOC Status Table: Data has been inserted in database.</div>";
                     }
                     else {
                         echo '<div class="alert alert-danger" role="alert">
                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                               <span class="sr-only">Error:</span>
                               TOC Status Table: Sorry, failed to insert !
                               </div>';
                     }
                    
                    //insert data in study information
                     $result=$conn->query("INSERT INTO study_info
                          (study_name,study_loc,client_id,creation_date,status) VALUES ('$study_name','$study_loc','111','$today','ON')");
                     if ($result) {
                         echo "<div class='alert alert-success' role='alert'>TOC study info: Data has been inserted in database.</div>";
                     }
                     else {
                         echo '<div class="alert alert-danger" role="alert">
                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                               <span class="sr-only">Error:</span>
                               TOC study info Table: Sorry, failed to insert !
                               </div>';
                     }

                     //insert data in snapshot table
                     $result=$conn->query("INSERT INTO snap_$study_name (id, snap_name) VALUES (0,'Base')");
                     if ($result) {
                         echo "<div class='alert alert-success' role='alert'>Data Currency: A base data currency has been created.</div>";
                     }
                     else {
                         echo '<div class="alert alert-danger" role="alert">
                               <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                               <span class="sr-only">Error:</span>
                               Data Currency info Table: Sorry, failed to insert !
                               </div>';
                     }

                    $conn->close();
                }

              }
            ?>

              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      