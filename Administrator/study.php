<?php
include("include/header.php");
include("include/connect.php");

//change output status dynamically after 1 day passed
$study_name=$_SESSION["study"];
date_default_timezone_set('Asia/Dhaka');
$today=date("Y-m-d H:i:s");
$date =date("Y-m-d H:i:s");
//echo 'date before day adding: ' . $date;
$date = date('Y-m-d H:i:s', strtotime($date . ' - 1 day'));
//echo 'date after adding 1 day: ' . $date;

$result=$conn->query("UPDATE toc_status_$study_name SET outstat = '1', odate_0 = '$today'  WHERE  (odate_0 < '$date' AND outstat = '2') ");


//Check if study have the tree or not
$study_name=strtolower($study_name);
$table="toc_$study_name";
//$result = mysql_query("SHOW TABLES FROM test");
$result=$conn->query("SHOW TABLES FROM toc_dbv2");
while($row = mysqli_fetch_row($result)){
    $arr[] = $row[0];
}

if(in_array($table,$arr)){
    $tree_exist=1;
}
else{
  $tree_exist=0;
}
$study_name=strtoupper($study_name);
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Study</li>
    </ol>

    
  </section>

  <!-- Main content -->
  <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
              <!-- MAP & BOX PANE -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Study Name : <span><?php echo $_SESSION["study"] ?> </span></h3>
                  <div class="box-tools pull-right" style="position: initial;">
                    <?php
                      if($tree_exist==1){
                          echo '
                          <span class="description-percentage text-black" ><img src="../stree/metroStyle/img/p0.png"> No Program</span>&nbsp;
                          <span class="description-percentage text-yellow" ><img src="../stree/metroStyle/img/p1.png"> In Development</span>&nbsp;
                          <span class="description-percentage text-red" ><img src="../stree/metroStyle/img/p2.png"> To Be  Validate</span>&nbsp;&nbsp;
                          <span class="description-percentage text-green" ><img src="../stree/metroStyle/img/p3.png"> Validated</span>&nbsp;&nbsp;

                          <br>
                          <span class="description-percentage text-black" ><img src="../stree/metroStyle/img/o0.png"> No Output</span>&nbsp;
                          <span class="description-percentage text-red" ><img src="../stree/metroStyle/img/o1.png"> Old Output</span>&nbsp;
                          <span class="description-percentage text-green" ><img src="../stree/metroStyle/img/o2.png"> Current Output</span>&nbsp;
                          ';
                        }
                      ?>
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-12 col-sm-12">
                      <div id="clickevt" class="pad" style="padding: 10px; overflow-x: auto;">
                          
<?php

//code for insert new section
if(isset($_POST["sec_id"]))  {

$section = $_POST['sec_name'];
$pgmid = $_POST["sec_id"];

$pgmid_len=iconv_strlen (trim($pgmid));
        if($pgmid_len>1){
          $masterid=substr($pgmid,0,$pgmid_len-1);
        }else {
          $masterid=$pgmid;
        }
        // echo "Selected id masterid : ".$masterid."<br>";
        // echo "Selected id pgmid : ".$pgmid."<br>"; 
        $pgmid_to_be_add = $pgmid+1;

 
        $pgmid_len=iconv_strlen (trim($pgmid_to_be_add));

        $pre_pgmid_len=iconv_strlen (trim($pgmid_to_be_add))-1;

        $tosearchid=$pgmid_to_be_add.str_repeat("0",5-$pgmid_len);


        // $study_name="dmc3";
        $result=$conn->query("select * from toc_$study_name where sortorder='$tosearchid' ");
        // print_r($result);
        if($result->num_rows > 0){
          while($row = $result->fetch_assoc()) {

                    $l1 = $row['l1'];
                    $l2 = $row['l2'];
                    $l3 = $row['l3'];
                    $l4 = $row['l4'];
                    $l5 = $row['l5'];
                                   

            $sql2="SELECT * FROM (SELECT * FROM toc_$study_name WHERE sortorder >=$tosearchid) AS a WHERE sortorder LIKE '$masterid%' ORDER BY sortorder DESC";
            $result2 = $conn->query($sql2);
            while($row = $result2->fetch_assoc()) {

                    $temp="l$pgmid_len";
                    $temp1=$row["$temp"]+1;
                    $sortorder=$row["sortorder"];
                    $newsort=substr_replace($sortorder,$temp1,$pgmid_len-1,1);
                    // echo "new sortorder :".$newsort." new l$pgmid_len : $temp1<br>";
            
             //update toc table
            $sql3="UPDATE toc_$study_name SET sortorder=$newsort, l$pgmid_len=$temp1 WHERE sortorder = $sortorder";
            $result3 = $conn->query($sql3);
             // if($result3){echo " toc table updated!!";}

             //update toc status table    
            $sql4="UPDATE toc_status_$study_name SET sortorder=$newsort WHERE sortorder = $sortorder";
            $result4 = $conn->query($sql4);
             // if($result4){echo "toc status table updated!!";}
            }
            // $section="demo section";
            //insert into toc table;
            $sql5="INSERT INTO toc_$study_name (study,sortorder,l1,l2,l3,l4,l5,section) VALUES ('$study_name',$tosearchid,'$l1',$l2,$l3,$l4,$l5,'$section') ";
            $result5 = $conn->query($sql5);
                // if($result5){echo "iNSERT into toc table --> SUCCESS<BR>";}else{echo "iNSERT FAILED";}
                //insert into toc status table
                date_default_timezone_set('Asia/Dhaka');
                        $today = date("Y-m-d H:i:s");
            $sql6="INSERT INTO toc_status_$study_name (study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('$study_name',$tosearchid,'0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
            $result6 = $conn->query($sql6);
                // if($result6){echo "iNSERT into toc status Table --> SUCCESS<BR>";}else{echo "iNSERT FAILED";}
            // echo "not last node <--";
                    
             }
            }else{
                  // echo "last node added <br>";
                  // $section="demo section";
                    $l2=substr($tosearchid,1,1);
                    $l3=substr($tosearchid,2,1);
                    $l4=substr($tosearchid,3,1);
                    $l5=substr($tosearchid,4,1);
                     //insert into toc table                       
                    $sql7="INSERT INTO toc_$study_name (study,sortorder,l1,l2,l3,l4,l5,section) VALUES ('$study_name',$tosearchid,'15.1',$l2,$l3,$l4,$l5,'$section') ";
                    $result7 = $conn->query($sql7);
                    print_r($result7);
                    // if($result7){echo "iNSERT into toc table --> SUCCESS<BR>";}else{echo "iNSERT FAILED";}
                     //insert into toc status table   
                    date_default_timezone_set('Asia/Dhaka');
                            $today = date("Y-m-d H:i:s");                    
                    $sql8="INSERT INTO toc_status_$study_name (study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('$study_name',$tosearchid,'0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
                    $result8 = $conn->query($sql8);
                    print_r($result8);
                    // if($result8){echo "iNSERT into toc status Table --> SUCCESS<BR>";}else{echo "iNSERT FAILED";}
                    
            }
}


//code for insert new sub section
if(isset($_POST["sub_sec_id"]))  {

        $section = $_POST['sub_sec_name'];
        $pgmid = $_POST["sub_sec_id"];

        $pgmid_len=iconv_strlen (trim($pgmid));
        if($pgmid_len>1){
          $masterid=substr($pgmid,0,$pgmid_len-1);
        }else {
          $masterid=$pgmid;
        }
 
        // $pgmid_to_be_add = $pgmid+1;
        $pgmid_len=iconv_strlen (trim($pgmid));

        // $pre_pgmid_len=iconv_strlen (trim($pgmid_to_be_add))-1;

          $tosearchid=$pgmid.str_repeat("0",5-$pgmid_len);
               
              $pgmid_len1=$pgmid_len+1;

        // echo "selected sortorder for search : ".$tosearchid."<br>"; 

        //select row information
        $result_rinfo=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder=$tosearchid");

          while($row_rinfo = $result_rinfo->fetch_assoc()) {
                    $l1 = $row_rinfo['l1'];
                    $l2 = $row_rinfo['l2'];
                    $l3 = $row_rinfo['l3'];
                    $l4 = $row_rinfo['l4'];
                    $l5 = $row_rinfo['l5'];
        }           
        
        $sql_max="SELECT MAX(l$pgmid_len1) as lvl FROM toc_dmc3 WHERE sortorder LIKE '$pgmid%' ORDER BY sortorder ASC";
        $result1_max = $conn->query($sql_max);
        while($row_rinfo = $result1_max->fetch_assoc()) {
          $maxvalue = $row_rinfo['lvl']+1;
        }
        // echo '<br>total child number : '.$maxvalue.'<br>';
        if($pgmid_len==1)
          $l2 = $maxvalue;
        if($pgmid_len==2)
          $l3 = $maxvalue;
        if($pgmid_len==3)
          $l4 = $maxvalue;
        if($pgmid_len==4)
          $l5 = $maxvalue;
        $newsortid=$pgmid.$maxvalue.str_repeat("0",5-$pgmid_len-1);
        // echo "New sort id : ".$newsortid;

                        // $section="demo section";
            //insert into toc table;
            $sql5="INSERT INTO toc_dmc3 (study,sortorder,l1,l2,l3,l4,l5,section) VALUES ('$study_name',$newsortid,'$l1',$l2,$l3,$l4,$l5,'$section') ";
            $result5 = $conn->query($sql5);
            // if($result5){echo "iNSERT into toc table --> SUCCESS<BR>";}else{echo "<BR>iNSERT FAILED<BR>";}
            //insert into toc status table
            date_default_timezone_set('Asia/Dhaka');
            $today = date("Y-m-d H:i:s");
            $sql6="INSERT INTO toc_status_dmc3 (study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('$study_name',$newsortid,'0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
            $result6 = $conn->query($sql6);

}

//code update  section header
if(isset($_POST['up_sec_id'])){
  
  $section= $_POST['up_sec_name'];
  $pgmid = $_POST['up_sec_id'];
  $pgmid_len=iconv_strlen (trim($pgmid));
  $tosearchid=$pgmid.str_repeat("0",5-$pgmid_len);

 // echo "$tosearchid";
  $update_sql=$conn->query("UPDATE toc_$study_name SET section='$section' WHERE sortorder = $tosearchid ");

}

//code insert new entry
if(isset($_POST['ne_row_id'])){

        $ne_title=$_POST["ne_title"];
        $ne_pgmnam=$_POST["ne_pgmnam"].".sas";
        $ne_pgtyp=$_POST["ne_pgtyp"];
        $ne_outnum=$_POST["ne_outnum"];
        $ne_outnam=$_POST["ne_outnam"].".lst";
        $ne_lognam=$_POST["ne_lognam"].".log";
        $ne_pgloc=$_POST["ne_pgloc"];
        $ne_outloc=$_POST["ne_outloc"];
        $pgmid = $_POST["ne_row_id"];

        // echo $ne_title." ".$ne_pgmnam." ".$ne_pgtyp." ".$ne_outnum." ".$ne_outnam." ".$ne_lognam." ".$ne_pgloc." ".$ne_outloc." ".$pgmid." ";
        $pgmid_len=iconv_strlen (trim($pgmid));
        $tosearchid=$pgmid.str_repeat("0",5-$pgmid_len);
        $ses_sql=$conn->query("SELECT section FROM toc_$study_name WHERE sortorder='$tosearchid' ");
        while($row = $ses_sql->fetch_assoc()) {
            $section = $row['section']; 
        }
        
        if($pgmid_len>1){
            $masterid=substr($pgmid,0,$pgmid_len-1);
        }else {
            $masterid=$pgmid;
        }
        // echo "Selected pgmid : ".$pgmid."<br>";
        // echo "masterid for Selected pgmid : ".$masterid."<br>";
        if($section==" "){
            $pgmid=$masterid;
        }

        // $pgmid_to_be_add = $pgmid+1;
        $pgmid_len=iconv_strlen (trim($pgmid));

        // $pre_pgmid_len=iconv_strlen (trim($pgmid_to_be_add))-1;
        $tosearchid=$pgmid.str_repeat("0",5-$pgmid_len);
               
        $pgmid_len1=$pgmid_len+1;

        // echo "selected sortorder for search : ".$tosearchid."<br>"; 
        //select row information
        $result_rinfo=$conn->query("SELECT * FROM toc_$study_name WHERE sortorder=$tosearchid");
            while($row_rinfo = $result_rinfo->fetch_assoc()) {
                                $l1 = $row_rinfo['l1'];
                                $l2 = $row_rinfo['l2'];
                                $l3 = $row_rinfo['l3'];
                                $l4 = $row_rinfo['l4'];
                                $l5 = $row_rinfo['l5'];
                                $section=$row_rinfo['section'];
        }
                  
        
        $sql_max="SELECT MAX(l$pgmid_len1) as lvl FROM toc_dmc3 WHERE sortorder LIKE '$pgmid%' ORDER BY sortorder ASC";
        $result1_max = $conn->query($sql_max);
        while($row_rinfo = $result1_max->fetch_assoc()) {
          $maxvalue = $row_rinfo['lvl']+1;
        }

        // $maxvalue=$result1_max->num_rows;
        // echo '<br>total child number : '.$maxvalue.'<br>';
        if($pgmid_len==1)
          $l2 = $maxvalue;
        if($pgmid_len==2)
          $l3 = $maxvalue;
        if($pgmid_len==3)
          $l4 = $maxvalue;
        if($pgmid_len==4)
          $l5 = $maxvalue;
        $newsortid=$pgmid.$maxvalue.str_repeat("0",5-$pgmid_len-1);
       
           // insert into toc table;
            $sql5="INSERT INTO toc_$study_name (study,sortorder,l1,l2,l3,l4,l5,section,type,title,pgmloc,pgmname,outno,outloc,outname,logname) VALUES ('$study_name',$newsortid,'$l1',$l2,$l3,$l4,$l5,' ','$ne_pgtyp','$ne_title','$ne_pgloc','$ne_pgmnam','$ne_outnum','$ne_pgloc','$ne_outnam','$ne_lognam') ";
            $result5 = $conn->query($sql5);
            if($result5){
                  //echo "iNSERT into toc table --> SUCCESS<BR>";
            }else{
                  echo "<div class='alert alert-warning'>
                          <strong>Warning!</strong> Failed to insert into main toc table.
                        </div>";
            }
                //insert into toc status table
            date_default_timezone_set('Asia/Dhaka');
            $today = date("Y-m-d H:i:s");
            $sql6="INSERT INTO toc_status_$study_name (study, sortorder, pgmstat, pgmstatdc, pdate_0, pdate_1, pdate_2, pdate_3, outstat, outstatdc, odate_0, odate_1, odate_2) VALUES ('$study_name',$newsortid,'0','No Program','$today','$today','$today','$today','0','No Output','$today','$today','$today') ";
            $result6 = $conn->query($sql6);
            if($result6){
                 // echo "iNSERT into toc status Table --> SUCCESS<BR>";
            }else{
                  echo "<div class='alert alert-warning'>
                          <strong>Warning!</strong> Failed to insert into toc status table.
                        </div>";
            }
          
}  
          //move section
          if(isset($_POST['mov_sec_id'])){
            $movid=$_POST['mov_sec_id'];
            $tarid=$_POST['tar_sec_id'];

            $m_pgmid=substr($movid,0,strpos($movid,"0"));
            $t_pgmid=substr($tarid,0,strpos($tarid,"0"));

            echo "move:".$movid."(".$m_pgmid.")"."</br> target:".$tarid."(".$t_pgmid.")";
            
            
            
            $sql2="SELECT * FROM  toc_$study_name  WHERE sortorder LIKE '$t_pgmid%' ORDER BY sortorder DESC";
            $result2 = $conn->query($sql2);
            // print_r($result);
            while($row = $result2->fetch_assoc()) {
               $sord=$row['sortorder'];
               $nsord="9".$sord;
               //$nsord=substr($sord,1,strrpos($sord,"0"));
               // Update each row ;
               $update_sql=$conn->query("UPDATE toc_$study_name SET sortorder='$nsord' WHERE sortorder = $sord ");
               if($update_sql)echo"<br>update success!!";
            }

            $sql1="SELECT * FROM  toc_$study_name  WHERE sortorder LIKE '$m_pgmid%' ORDER BY sortorder DESC";
            $result1 = $conn->query($sql1);



            // echo "Target section: rows";
            // echo '<table style="width:100%">';
            // while($row = $result2->fetch_assoc()) {
            // echo '<tr><td>'.$row['study'].'</td><td>'.$row['sortorder'].'</td>
            // <td>'.$row['l1'].'</td> <td>'.$row['l2'].'</td> <td>'.$row['l3'].'</td> 
            // <td>'.$row['l4'].'</td> <td>'.$row['l5'].'</td><td>'.$row['section'].'</td>
            // <td>'.$row['type'].'</td><td>'.$row['title'].'</td><td>'.$row['pgmloc'].'</td>
            // <td>'.$row['pgmname'].'</td><td>'.$row['outno'].'</td><td>'.$row['outloc'].'</td>
            // <td>'.$row['outname'].'</td><td>'.$row['logname'].'</td></tr>';
            // }
            // echo '</table>';
          }



              if($tree_exist==0){
                echo '<div class="alert alert-danger" role="alert">
                                              <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                              <span class="sr-only">Error:</span>
                                              No TOC Available  for this Study!
                                              </div>';
              } 
?> 

<?php
            // $sql2="SELECT * FROM  toc_$study_name ";
            // $result2 = $conn->query($sql2);
            // // print_r($result);
            // while($row = $result2->fetch_assoc()) {
            //    $section=$row['section'];
            //     echo $section."strlength:".strlen(trim($row['section']," "))."<br>";
            // }

// $text   = "\t\tThese are a few words :) ...  ";
// $binary = "\x09Example string\x0A";
// $hello  = "Hello World";
// var_dump($text, $binary, $hello);

// print "\n";

// $trimmed = trim($text);
// var_dump($trimmed);

// $trimmed = trim($text, " \t.");
// var_dump($trimmed);

// $trimmed = trim($hello, "Hdle");
// var_dump($trimmed);

// $trimmed = trim($hello, 'HdWr');
// var_dump($trimmed);
?>
              <ul id="treeDemo" class="ztree" style="-moz-user-select: none; width: -moz-fit-content; height: auto; border: 1px solid #F0F9F8;
              background: #FFF none repeat scroll 0% 0%;"></ul>

              <div id="rMenu" style="background-color: #ffffff;z-index:9999;">
                  <ul  style=" padding: 0px;">
                      <li id="m_sec" onclick="new_sec();" data-toggle="modal" data-target=".bs-example-new-sec-modal-lg">Copy</li>
                      <li id="m_sec" onclick="new_sec();" data-toggle="modal" data-target=".bs-example-new-sec-modal-lg">Paste</li>
<!--                      <li id="m_sec" onclick="new_sec();" data-toggle="modal" data-target=".bs-example-new-sec-modal-lg">Create new section</li>-->
<!--                        <li id="m_sub_sec" onclick="new_sub_sec();" data-toggle="modal" data-target=".bs-example-new-sub-sec-modal-lg">Create new sub-section</li>-->
<!--                        <li id="m_entry" onclick="new_entry();" data-toggle="modal" data-target=".bs-example-new-entry-modal-lg">Create New Entry</li>-->
<!--                        <li id="m_update_sechead" onclick="update_sechead();" data-toggle="modal" data-target=".bs-example-update_sechead-modal-lg">Update Section Header</li>-->
<!--                        <li id="m_del_sec" onclick="del_sec1();" data-toggle="modal" data-target=".bs-example-delcon-modal-sm">Delete Section</li>-->
<!--                        <li id="m_out" onclick="showout();">View Output</li>-->
<!--                        <li id="m_log" onclick="showlog();">View Log</li>-->
                  </ul>
              </div>
                        
              </div>
            </div><!-- /.col -->
          </div><!-- /.row -->
        </div><!-- /.box-body -->

      </div><!-- /.box -->
    </div><!-- /.col -->          
  </div><!-- /.row (main row) -->




  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      