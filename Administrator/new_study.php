<?php
include("include/header.php");
error_reporting(E_ALL ^ E_NOTICE);

require '../vendor/autoload.php';
include("include/connect.php");
date_default_timezone_set('Asia/Dhaka');
$today 			= date("Y-m-d H:i:s");
$username 	= $_SESSION['login_user'];

$studySql="SELECT * FROM study_info";
$result  =$conn->query($studySql);
$studyCount=$result->num_rows;

if(!empty($_POST)) {
    echo '<div class="loader"></div>';
}

?>
<style>
    .loader {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url('../images/loading.gif') 50% 50% no-repeat rgb(249,249,249);
        opacity: .8;
    }
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">New study</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

  <!-- Main row -->
  <div class="row">
  <!-- Left col -->
    <div class="col-md-12">
    <!-- MAP & BOX PANE -->
      <div class="box box-success">
        <div class="box-header with-border">
          <h3 class="box-title">New study</h3>
          <div class="box-tools pull-right">
              Total Study : <?php echo $studyCount;?>
              <br/>
              Note : Max 3 studies for test version
          </div>
        </div><!-- /.box-header -->
      <div class="box-body no-padding">
        <div class="row">
          <div class="col-md-3"></div>
          <div class="col-md-6">
            <div class="pad">

            <?php
                if($studyCount>=3)
                    echo '<div class="alert alert-danger" role="alert">
                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                          <span class="sr-only">Limit Exceeded:</span>
                          Study limit exceeded for test version.You cannot create new study! 
                          </div>';

            ?>
              <form id="new_study_form"  action="new_study.php" method="post" enctype="multipart/form-data">

                <div class="form-group has-feedback">
                    <label>Enter Study Folder Name:</label>
                    <input type="text" required name="study_name" id="study_name" class="form-control" title="Please enter study name" onchange="chk_study()" value="<?php //echo set_value('fullname');  ?>" placeholder="Enter Study Folder Name" <?php if($studyCount>=3) echo "readonly"; ?>/>
                </div>

                  <div class="form-group has-feedback">
                      <label>Enter Study Folder Location:</label>
                      <input type="text" required name="study_loc" id="study_loc" class="form-control" title="Please enter study folder location"  value="<?php //echo set_value('fullname');  ?>" placeholder="Enter Study Folder Location here" <?php if($studyCount>=3) echo "readonly";?>/>
                  </div>

                  <div class="form-group has-feedback">
                  <label>Select TOC file:</label>
                  <div class="fileupload fileupload-new" data-provides="fileupload">
                    <span class="btn btn-info btn-file"><span class="fileupload-new">Select file</span>
                    <input  type="file" name="fileToUpload" id="fileToUpload" <?php if($studyCount>=3) echo "disabled";?> /></span>
                    <span class="fileupload-preview"></span>
                    <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                      <input type="submit" <?php if($studyCount>=3) echo "disabled";?> class="btn btn-primary btn-block" value="Create Study" name="submit" id="submit_btn">
                  </div><!-- /.col -->
                </div>
                  <button type="hidden" id="submit_Hidden" style="display: none;"></button>

              </form>
            </div>

            <?php

              if(isset($_POST["submit"])) {
                if($studyCount >= 3) {
                    $msg="Sorry, You are using test version. Study Limit Exceeded.";
                    notificationMessage( $msg , 'danger' );
                    exit;
                }
                $uploadOk = 0;
                $study_name=trim($_POST["study_name"]);
                $study_loc=trim($_POST["study_loc"]);
                $target_dir = $study_loc."/";
                if(is_dir($target_dir)){
                  $new_tar=$target_dir.$study_name."/";
                  if(!is_dir($new_tar)) {
                      if(mkdir($new_tar, 0755))
                      {
                          $new_tar1=$new_tar."lst"."/";
                          $new_tar2=$new_tar."pgm"."/";
                          if(!is_dir($new_tar1)) {
                              mkdir($new_tar1, 0755);
                          }
                          if(!is_dir($new_tar2)) {
                              mkdir($new_tar2, 0755);
                          }
                      }
                  }

                }else{
                    $msg="Study location is not exists !";
                    notificationMessage( $msg , 'danger' );
                    $uploadOk = 0;
                }
                
                if ($_FILES["fileToUpload"]["name"] != "") {

                    $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
                    $uploadOk = 1;
                    $FileType = pathinfo($target_file, PATHINFO_EXTENSION);
                    // Check if file already exists
                    if (file_exists($target_file)) {
                      $msg="One older meta file found and replaced with new meta file!";
                      notificationMessage( $msg , 'info' );
                      $uploadOk = 1;
                    }
                    // Check file size
                    $fileSize=$_FILES["fileToUpload"]["size"]/1024;
                    if ( $fileSize > 5000) {
                      $msg="Sorry, your file is too large!";
                      notificationMessage( $msg , 'danger' );
                      $uploadOk = 0;
                    }
                    // Allow certain file formats
                    if ($FileType != "xls" ) {
                        $msg="Sorry, Only xls files are allowed !";
                        notificationMessage( $msg , 'danger' );
                        $uploadOk = 0;
                    }
                    // Check if $uploadOk is set to 0 by an error
                    if ($uploadOk == 0) {

                        $msg="Sorry, your file was not uploaded !";
                        notificationMessage( $msg , 'danger' );
                        // if everything is ok, try to upload file
                    } 
                    else {
                        if(file_exists($target_dir)){

                          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
                              /*$msg="The file ". basename($_FILES["fileToUpload"]["name"]) ." has been uploaded.";
                              notificationMessage( $msg , 'success' );*/
                              $uploadOk = 1;
                          }
                          else {
                              $msg="Sorry, there was an error uploading your file !";
                              notificationMessage( $msg , 'danger' );
                              $uploadOk = 0;     
                          }
                        }
                        else{

                            $msg="Sorry, Your study folder does not exist in server, Please create study folder first.";
                            notificationMessage( $msg , 'danger' );
                            $uploadOk = 0;
                        }  
                    }
                }
                else {
                    $uploadOk = 1;
                      /*$msg="No file selected !";
                      notificationMessage( $msg , 'danger' );*/
                }

                ////Create tables: toc status in database (ALL)!!
                if ($uploadOk == 1) {
                    createTables($conn,$study_name);
                }

                ////insert toc content into toc study table and toc status table!!
                if ($uploadOk == 1 && $_FILES["fileToUpload"]["name"] != "") {

                    $inputFileType = 'Xls';
                    $inputFileName = $target_file;

                    /**  Create a new Reader of the type defined in $inputFileType  **/
                    $reader = \PhpOffice\PhpSpreadsheet\IOFactory::createReader($inputFileType);
                    /**  Advise the Reader that we only want to load cell data  **/
                    $reader->setReadDataOnly(true);
                    /**  Load $inputFileName to a Spreadsheet Object  **/
                    $spreadsheet = $reader->load($inputFileName);
                    $sheetData = $spreadsheet->getActiveSheet()->toArray(null,true,true,true);

                    $dbCollumns=array();
                    $dbValues  =array();
                    foreach($sheetData as $key=>$val)
                    {
                        if($key==1){
                            foreach ($val as $k=>$v)
                            {
                                array_push($dbCollumns,$v);
                            }
                        }else{
                            $dbValues[$key-1]=$val;
                        }
                        //if($key==4) break;
                    }
                    //deleting all values from toc table
                    $sql="truncate table toc_$study_name";
                    $conn->query($sql);

                    createCollumnIfNotExists($conn,$study_name,$dbCollumns);
                    $dbValue = generate_array_usingDBKey($dbCollumns,$dbValues,$study_name);
                    $sql=build_sql_insert('toc_'.$study_name,$dbValue);


                    if($conn->query($sql))
                    {
                        $query="INSERT INTO study_info(study_name, study_loc, creation_date, status) VALUES ('$study_name','$study_loc','$today','ON')";
                        $conn->query($query);
                        //fetching data from toc table and inserting values to toc_status table
                        $query="SELECT DISTINCT sortorder FROM toc_$study_name";
                        $result=$conn->query($query);
                        while($row = $result->fetch_assoc()){
                            $sortID=$row['sortorder'];
                            $query="INSERT INTO toc_status_$study_name(data_currency,study,sortorder)
                            VALUES ('SP0','$study_name','$sortID')";
                            $conn->query($query);
                        }
                        $msg="Study ( $study_name ) created successfully.";
                        notificationMessage( $msg , 'success' );
                    }else{

                        $msg="Sorry!Something went wrong.Study is not created";
                        notificationMessage( $msg , 'danger' );
                    }
                }else if($uploadOk==1){
                    $query="INSERT INTO study_info(study_name, study_loc, creation_date, status) VALUES ('$study_name','$study_loc','$today','ON')";
                    $conn->query($query);
                    $query="SELECT DISTINCT sortorder FROM toc_$study_name";
                    $result=$conn->query($query);
                    while($row = $result->fetch_assoc()){
                        $sortID=$row['sortorder'];
                        $query="INSERT INTO toc_status_$study_name(data_currency,study,sortorder)
                            VALUES ('SP0','$study_name','$sortID')";
                        $conn->query($query);
                    }
                    $msg="Study ( $study_name ) created successfully.";
                    notificationMessage( $msg , 'success' );
                }else{

                    $msg="Sorry!Something went wrong.Study is not created";
                    notificationMessage( $msg , 'danger' );
                }

              }
            ?>

              </div><!-- /.col -->
          <div class="col-md-3"></div>

        </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php

function createTables($conn,$study_name){

    createTocTable($conn,$study_name);

    $result = $conn->query("CREATE TABLE IF NOT EXISTS toc_status_".$study_name." (
                                        id int(11) NOT NULL AUTO_INCREMENT,
                                        data_currency varchar(200) ,
                                        study varchar(20) NOT NULL default '',
                                        sortorder varchar(10) ,
                                        pgmstat int(10) ,
                                        pgmstatdc varchar(50) ,
                                        pdate_0  datetime NOT NULL ,
                                        pdate_1  datetime NOT NULL ,
                                        pdate_2  datetime NOT NULL ,
                                        pdate_3  datetime NOT NULL ,
                                        outstat int(10) ,
                                        outstatdc varchar(50),
                                        odate_0  datetime NOT NULL ,
                                        odate_1  datetime NOT NULL ,
                                        odate_2  datetime NOT NULL ,

                                       PRIMARY KEY  (id)
                                   )");
    /*if ($result) {
        $msg="The table toc_status_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );

    } else {
        $msg="Sorry, failed to create toc status table!";
        notificationMessage( $msg , 'danger' );
    }*/

    ////Create tables: toc program history table  in database!!
    $result = $conn->query("CREATE TABLE IF NOT EXISTS pgm_hist_".$study_name." (
                                        id int(10) NOT NULL AUTO_INCREMENT,
                                        sortorder varchar(10) ,
                                        pgmname varchar(50) NOT NULL default '' ,
                                        event_date  datetime NOT NULL ,
                                        username varchar(50),
                                        comment text NOT NULL,
                                        status varchar(100),
                                        link varchar(100),
                                        validation_system varchar(500),
                                        data_currency varchar(200),


                                       PRIMARY KEY  (id)
                                   )");
    /*if ($result) {
        $msg="The table pgm_hist_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );

    } else {
        $msg="Sorry, failed to create pgm history table!";
        notificationMessage( $msg , 'danger' );
    }*/

    ////Create tables: snapshot history table in database!!
    $result = $conn->query("CREATE TABLE IF NOT EXISTS snap_".$study_name." (
                                        uid int(11) NOT NULL AUTO_INCREMENT,
                                        id int(10) NOT NULL ,
                                        snap_name varchar(200) ,
                                        lib_name varchar(200) ,
                                        Lib_loc varchar(200) ,
                                        is_lock enum('yes', 'no'),
                                        updated_at  datetime NOT NULL ,
                                        updated_by varchar(200) ,
                                        created_at  datetime NOT NULL ,
                                        created_by varchar(200) ,


                                       PRIMARY KEY  (uid)
                                   )");
    /*if ($result) {
        $msg="The table snap_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );

    } else {
        $msg="Sorry, failed to create snap study table table!";
        notificationMessage( $msg , 'danger' );
    }*/
    //insert primary data to snap_studyname table
    $query="INSERT INTO snap_$study_name( snap_name) VALUES ('Base')";
    $conn->query($query);

    ////Create tables: toc program history table  in database!!
    $result = $conn->query("CREATE TABLE IF NOT EXISTS cpgm_hist_".$study_name." (
                                        id int(10) NOT NULL AUTO_INCREMENT,
                                        sortorder varchar(10) ,
                                        pgmname varchar(50) NOT NULL default '' ,
                                        event_date  datetime NOT NULL ,
                                        username varchar(50),
                                        comment text NOT NULL,
                                        status varchar(100),
                                        link varchar(100),
                                        validation_system varchar(500),


                                       PRIMARY KEY  (id)
                                   )");
    /*if ($result) {
        $msg="The table cpgm_hist_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );
    } else {
        $msg="Sorry, failed to create cpgm history table!";
        notificationMessage( $msg , 'danger' );
    }*/

    ////Create tables: common program  table  in database!!
    $result = $conn->query("CREATE TABLE IF NOT EXISTS cplist_".$study_name." (
                                        study varchar(20) NOT NULL default '',
                                        sortorder int(10) NOT NULL AUTO_INCREMENT,
                                        cploc varchar(200) NOT NULL default '' ,
                                        cpname varchar(50) NOT NULL default '' ,
                                        programmer varchar(200) NOT NULL default '' ,
                                        cpdate  datetime NOT NULL ,
                                        pgmstat  int(10) ,
                                        status  varchar(50) NOT NULL default '',
                                        validator   varchar(200) NOT NULL default '' ,
                                        status_date  datetime NOT NULL ,


                                       PRIMARY KEY  (sortorder)
                                   )");
    /*if ($result) {
        $msg="The table cplist_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );

    } else {
        $msg="Sorry, failed to create common pgm table!";
        notificationMessage( $msg , 'danger' );
    }*/
    ////create table for meta history
    $result = $conn->query("CREATE TABLE IF NOT EXISTS metadata_".$study_name." (
                                              id int(11) NOT NULL AUTO_INCREMENT,
                                              fieldname varchar(100) NOT NULL,
                                              searchtitle varchar(250) DEFAULT NULL,
                                              value varchar(250) NOT NULL,
                                              action enum('New','Updated','Deleted') NOT NULL,
                                              is_current enum('YES','NO') NOT NULL,
                                              created_at datetime DEFAULT NULL,
                                              created_by varchar(50) DEFAULT NULL,
                                              updated_at datetime DEFAULT NULL,
                                              updated_by varchar(50) DEFAULT NULL,
                                              new_value varchar(250) DEFAULT NULL,
                                              previous_value varchar(250) DEFAULT NULL,

                                              PRIMARY KEY  (id)
                                       
                                   )");
    /*if ($result) {
        $msg="The table metadata_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );
    } else {
        $msg="Sorry, failed to create metadata_'.$study_name.' table!";
        notificationMessage( $msg , 'danger' );
    }*/
}
function build_sql_insert($table, $data) {

    $key = array_keys($data[1]);
    $val = array_values($data);
    $values="";
    $count=count($val)-1;
    foreach($val as $index=>$v)
    {
        if($count!=$index)
            $values=$values."('".implode("', '", $v)."'),";
        else
            $values=$values."('".implode("', '", $v)."')";
    }
    $sql = "INSERT INTO $table (" . implode(', ', $key) . ") "
        . "VALUES ".$values;

    return($sql);
}
function generate_array_usingDBKey($dbCollumns,$dbValues,$study_name){
    $newArray=array();
    foreach($dbValues as $index=>$val)
    {
        $count=0;
        foreach($val as $key=>$v)
        {
            $name=strtolower($dbCollumns[$count]);
            $newArray[$index][$name]=addslashes($v);
            $count++;
        }
        $newArray[$index]['l1']=fixLenght($newArray[$index]['l1']);
        $newArray[$index]['l2']=fixLenght($newArray[$index]['l2']);
        $newArray[$index]['l3']=fixLenght($newArray[$index]['l3']);
        $newArray[$index]['l4']=fixLenght($newArray[$index]['l4']);
        $newArray[$index]['l5']=fixLenght($newArray[$index]['l5']);
        $sortorder=$newArray[$index]['l1'].$newArray[$index]['l2'].$newArray[$index]['l3'].$newArray[$index]['l4'].$newArray[$index]['l5'];
        $newArray[$index]["data_currency"]='SP0';
        $newArray[$index]["sortorder"]=$sortorder;
        $newArray[$index]["study"]=$study_name;
    }
    return $newArray;
}

function createTocTable($conn,$study_name){
    $result = $conn->query("CREATE TABLE IF NOT EXISTS toc_".$study_name."(
                  `id` int(11) NOT NULL AUTO_INCREMENT,
                  `data_currency` varchar(200) NOT NULL DEFAULT 'SP0',
                  `study` varchar(200) NOT NULL DEFAULT '',
                  `sortorder` varchar(10) NOT NULL DEFAULT '',
                  `l1` varchar(200) NOT NULL DEFAULT '',
                  `l2` varchar(200) NOT NULL DEFAULT '',
                  `l3` varchar(200) NOT NULL DEFAULT '',
                  `l4` varchar(200) NOT NULL DEFAULT '',
                  `l5` varchar(200) NOT NULL DEFAULT '',
                  `section` varchar(200) NOT NULL DEFAULT '',
                  `type` varchar(200) NOT NULL DEFAULT '',
                  `tlfnum` varchar(200) NOT NULL DEFAULT '',
                  `title` varchar(1024) NOT NULL DEFAULT '',
                  `footnote` varchar(200) DEFAULT NULL,
                  `pgmloc` varchar(200) NOT NULL DEFAULT '',
                  `pgmname` varchar(200) NOT NULL DEFAULT '',
                  `outno` varchar(200) NOT NULL DEFAULT '',
                  `outloc` varchar(200) NOT NULL DEFAULT '',
                  `outname` varchar(200) NOT NULL DEFAULT '',
                  `logname` varchar(200) NOT NULL DEFAULT '',
                  `status` varchar(200) NOT NULL DEFAULT '',
                  `secondary_endpoint` varchar(250) DEFAULT NULL,
                  `study_medication` varchar(200) NOT NULL DEFAULT '',
                  `phase` varchar(200) NOT NULL DEFAULT '',
                  `population` varchar(200) NOT NULL DEFAULT '',
                  `shell_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_1_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_1_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_2_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_2_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_3_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_3_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_4_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_4_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_5_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_5_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_6_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_6_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_7_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_7_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_8_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_8_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_9_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_9_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_10_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_10_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_11_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_11_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_12_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_12_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_13_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_13_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_14_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_14_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_15_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_15_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_16_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_16_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_17_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_17_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_18_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_18_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_19_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_19_value` varchar(200) NOT NULL DEFAULT '',
                  `parameter_20_name` varchar(200) NOT NULL DEFAULT '',
                  `parameter_20_value` varchar(200) NOT NULL DEFAULT '',
                   PRIMARY KEY  (id)
                )");

    $sql="truncate table toc_$study_name";
    $conn->query($sql);

    $sql="INSERT INTO toc_$study_name(data_currency,study,sortorder,l1,l2,l3,l4,l5,section) values('SP0','$study_name','0100000000','01','00','00','00','00','Trail Section')";
    $conn->query($sql);
    /*if ($result) {
        $msg="The table toc_$study_name has been created in database.";
        notificationMessage( $msg , 'success' );
    } else {
        $msg="Sorry, failed to create toc_'.$study_name.' table!";
        notificationMessage( $msg , 'success' );
    }*/
}

function createCollumnIfNotExists($conn,$study_name,$dbCollumns){

    $tableName="toc_".$study_name;
    foreach($dbCollumns as $collumn)
    {
        $query="SHOW COLUMNS FROM `$tableName` LIKE '$collumn'";
        $result = $conn->query($query);
        if($result->num_rows>0) {
            //nothing to do
        }else{
            $conn->query("ALTER TABLE `$tableName` ADD `$collumn` VARCHAR(200) NOT NULL DEFAULT ''");
        }
    }
}

function fixLenght($val){
    $filter_value=$val;
    if(iconv_strlen (trim($val))==1){
        $filter_value=trim("0".$val);
    }
    if(iconv_strlen (trim($val))==0){
        $filter_value=trim("00");
    }
    return $filter_value;
}

function notificationMessage( $msg , $msgType ){
    $notification='<div class="alert alert-'.$msgType.'" role="alert">';
    if($msgType!='success'){
        $notification=$notification.'<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>';
    }
    $notification=$notification.$msg.'</div>';
    echo $notification;
}
include("include/footer.php");
?>

<script type="text/javascript">
    $(window).load(function() {
        $(".loader").fadeOut("slow");
    });
    $(function() {

        $("#submit_btn").on('click', function(ev){
            var $this=$(this).closest('form');
            var msg="";
            var study = $('#study_name').val();
            var studyLoc = $('#study_loc').val();


            if(study == '' || studyLoc== ''){
                $('#submit_Hidden').click();
            }else{
                if( document.getElementById("fileToUpload").files.length == 0 ){
                    msg="You did not select any excel file.Do you want to continue?";
                }else
                    msg="Do you want to submit?";
                if (confirm(msg)){

                }else{
                    ev.preventDefault();
                }
            }

        });
    });
</script>

      