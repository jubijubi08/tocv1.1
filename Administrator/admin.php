<?php
include("include/header.php");
include("include/connect.php");
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Study</li>
          </ol>

          
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-8">
              <!-- MAP & BOX PANE -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Study Tree</h3>
                  <div class="box-tools pull-right"></div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-9 col-sm-8">
                      <div class="pad">
                        
                       <h4>Select the Study</h4>
                     <form action="study.php" method="post">
                     <div class="form-group has-feedback">

                        <select name="study_name" class="form-control">
                        <?php

                              //$sql = "SELECT z.study_name as sname FROM user_info as x , study_allocation as y , study_info as z WHERE x.username = '".$username."' AND x.user_id = y.user_id AND y.study_id = z.study_id";
                              $sql = "SELECT z.study_name as sname FROM study_info as z"; 
                              $result = $conn->query($sql);

                              if ($result->num_rows > 0) {
                                  while($row = $result->fetch_assoc()) {
                                      echo "<option value=".$row["sname"].">" . $row["sname"].  "</option>" ;
                                         }
                              } else {
                                  echo "0 results";
                              }
                              $conn->close();
                        ?>
                       </select>

                     </div>
                         <div class="row">
                                 <div class="col-xs-8">
                                   <div class="checkbox icheck">
                                   </div>
                                 </div><!-- /.col -->
                                 <div class="col-xs-4">
                                   <button type="submit" class="btn btn-primary btn-block btn-flat">Go</button>
                                 </div><!-- /.col -->
                         </div>
                      </form>



                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->          
          </div><!-- /.row (main row) -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      