<?php
    include("include/header.php");
    include("include/connect.php");
    require '../vendor/autoload.php';
    include("../lib/gen_func.php");

    $userSql="SELECT * FROM user_info";
    $result  =$conn->query($userSql);
    $userCount=$result->num_rows;
?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">New User</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
              <!-- MAP & BOX PANE -->
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Add New User</h3>
                    <div class="box-tools pull-right">
                        Total User : <?php echo $userCount;?>
                        <br/>
                        Note : Max 50 users for test version
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                      <div class="col-md-3"></div>
                      <div class="col-md-6">
                      <div class="pad">

                      <?php
                            if($_SERVER["REQUEST_METHOD"] == "POST")
                            {

                                $username = $_POST['username1'];
                                $pass= $_POST['password'];
                                $password = md5($pass);
                                $email = $_POST['email'];
                                $fullname = $_POST['fullname'];
                                //$type = $_POST['type'];
                                $countSql="SELECT user_id FROM user_info";
                                $countUser=$conn->query($countSql);
                                if($countUser->num_rows>=50){
                                    echo '<div class="alert alert-danger" role="alert">
                                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                          <span class="sr-only">Error:</span>
                                                          Users limit exceeded for test version!!! 
                                                          </div>';
                                }else{
                                    $checkusername = "SELECT * FROM user_info WHERE username = '$username'";

                                    $check=$conn->query($checkusername);

                                    if($check->fetch_assoc())
                                    {
                                        echo '<div class="alert alert-danger" role="alert">
                                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                          <span class="sr-only">Error:</span>
                                                          This username already taken, Try with another name!!
                                                          </div>';
                                    }
                                    else
                                    {
                                        date_default_timezone_set('Asia/Dhaka');
                                        $today = date("Y-m-d H:i:s");
                                        $registerquery = $conn->query("INSERT INTO user_info (fullname, email, username, password, status, firstlog ,creation_date) VALUES('$fullname', '$email', '$username', '$password', 'ON', 'Yes','$today')");

                                        if($registerquery)
                                        {
                                            $body = "Hi $fullname,<br><br> Your account has been created by admin.<br><br>
                                                Your username : $username <br><br>
                                                Password      : $pass";

                                            $res = smtpmailer($email, 'admin_medoms@shaficonsultancy.com', 'Med-OMS', 'MeD-OMS Account Created!', $body);
                                            if ($res == true)
                                            {
                                                echo "<div class='alert alert-success' role='alert'>Email Successfully sent to $email</div>";
                                            }
                                            else
                                            {
                                                echo "<div class='alert alert-error' role='alert'>Email is not send.</div>";
                                            }
                                            echo "<div class='alert alert-success' role='alert'>Successfully created new user!!</div>";
                                        }
                                        else
                                        {
                                            echo '<div class="alert alert-danger" role="alert">
                                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                                          <span class="sr-only">Error:</span>
                                                          Sorry something wrong,Try agin Please!!  
                                                          </div>';
                                        }
                                    }

                                }

                            }
                        ?>
                                 <form action="new_user.php" method="post" name="userform" id="userform">
                                   
                                   <div class="form-group has-feedback">
                                     <p>Full Name :</p>
                                     <input type="text" autofocus required name="fullname" id="fullname" class="form-control" title="Please enter user full name" value="<?php //echo set_value('fullname');  ?>" placeholder="Enter Full Name"/>
                                   </div>

                                   <div class="form-group has-feedback">
                                     <p>User Name:</p>
                                     <input type="text" name="username1" id="username1" class="form-control" pattern="[a-zA-Z0-9_-]{4,12}"  onchange="chk_exist_user()" required title="user name must be at least 4 characters and cannot exceed 12 characters" value="<?php //echo set_value('username');  ?>" placeholder="User Name"/>
                                   </div>

                                   <div class="form-group has-feedback">
                                     <p>User E-mail address : </p>
                                     <input type="email" required name="email" id="email" class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" title="Please enter a valid email address like, example@gamil.com " value="<?php //echo set_value('email');  ?>" placeholder="User E-mail address"/>
                                   </div>

                                   <div class="form-group has-feedback">
                                      <p>Password :</p>
                                     <input type="password" required name="password" id="password" class="form-control" pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" title="Password Should be contain(UpperCase, LowerCase, Number/SpecialChar and min 8 characters)" value="<?php //echo set_value('password');  ?>" placeholder="Password"/>
                                                                   
                                   </div>

                                    <div class="form-group has-feedback">
                                      <p>Confirm Password :</p>
                                     <input type="password" required name="cpassword" id="cpassword" onkeyup="checkPass(); return false;" class="form-control" placeholder="Confirm Password"/>
									                   <span id="confirmMessage" class="confirmMessage"></span>
                                   </div>

                                   <div class="row">
                                       <div class="col-xs-8">    
                                           <div class="checkbox icheck">
                                           </div>                        
                                       </div><!-- /.col -->
                                       <div class="col-xs-4">
                                           <button type="submit" class="btn btn-primary btn-block">Create New User</button>
                                       </div><!-- /.col -->
                                   </div>
                                 </form>

                      </div>
                    </div><!-- /.col -->
                      <div class="col-md-3"></div>

                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->          
          </div><!-- /.row (main row) -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


<script type="text/javascript">
function checkPass()
{
    //Store the password field objects into variables ...
    var pass1 = document.getElementById('password');
    var pass2 = document.getElementById('cpassword');
    //Store the Confimation Message Object ...
    var message = document.getElementById('confirmMessage');
    //Set the colors we will be using ...
    var goodColor = "#ffffff";
    var goodColorf = "#66cc66";
    var badColor = "#ff6666";
    //Compare the values in the password field
    //and the confirmation field
    if(pass1.value == pass2.value){
        //The passwords match.
        //Set the color to the good color and inform
        //the user that they have entered the correct password
        pass2.style.backgroundColor = goodColor;
        message.style.color = goodColorf;
        message.innerHTML = "Passwords Match!"
    }else{
        //The passwords do not match.
        //Set the color to the bad color and
        //notify the user.
        pass2.style.backgroundColor = badColor;
        message.style.color = badColor;
        message.innerHTML = "Passwords Do Not Match!"
    }
}  
</script>
<?php
include("include/footer.php");
?>      