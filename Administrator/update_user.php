<?php
include("include/header.php");
include("include/connect.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<ol class="breadcrumb">
			<li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
			<li class="active">Update User</li>
		</ol>
          
	</section>

	<!-- Main content -->
	<section class="content">

		<!-- Main row -->
		<div class="row">
			<!-- Left col -->
			<div class="col-md-12">
				<!-- MAP & BOX PANE -->
				<div class="box box-success">
					<div class="box-header with-border">
						<h3 class="box-title">Update User</h3>
						<div class="box-tools pull-right"></div>
					</div><!-- /.box-header -->
					<div class="box-body no-padding">
						<div class="row">
                            <div class="col-md-3"></div>

                            <div class="col-md-6">
								<div class="pad">
								<?php
								if($_SERVER["REQUEST_METHOD"] == "POST"){	
									$username= $_POST['username'];
									$fullname = $_POST['fullname'];
									$email = $_POST['email'];
									//$ctype = $_POST['user_ctype'];
									$type = $_POST['status'];
										
									$sql = "SELECT * FROM user_info";
									$quer2=$conn->query($sql);
									//echo $type;
									//echo "UPDATE user_info SET status ='$type' where user_id = '$username'";

									$sql_update="UPDATE user_info SET status ='$type',email='$email',fullname='$fullname' where user_id = '$username'";
									$result_update=$conn->query($sql_update);
									if($result_update){
										echo '<div class="alert alert-success">
												  <strong>Success!</strong> User updated successfully.
											  </div>';
									}
									else{
										echo '<div class="alert alert-danger">
  												<strong>Error!</strong> Sorry, Something going wrong, Try again. !
  											  </div>';
									}			
								}
								?>
									<form action="update_user.php" method="post">
										<div class="form-group has-feedback">
										 <p>User Name:</p>        
								           	<select required id="username"  name="username"  class="form-control">
										           <?php                                  
										           $opt = '<option selected value="">Select one</option>';

										           $sql="SELECT * FROM user_info";
										           $result=$conn->query($sql);

										           while($row = $result->fetch_assoc()){
										            $opt.="<option value='$row[user_id]'>$row[username]</option>";                       
										           } 
										           echo $opt;
										           ?>
								            </select>
								            <span class="glyphicon  form-control-feedback"></span>
								        </div>

										<div class="form-group has-feedback">
											<p>Enter Full Name</p>
											<input type="text" name="fullname" id="fullname" class="form-control"required placeholder="Enter Full Name"/>
											<span class="glyphicon  form-control-feedback"></span>
										</div>

										<div class="form-group has-feedback">
											<p>User E-mail address :</p>
											<input type="email" name="email" id="email" class="form-control"required placeholder="User E-mail address"/>
											<span class="glyphicon  form-control-feedback"></span>

										</div>

									<!-- 	<div class="form-group has-feedback">
											<p>Current Status :</p>
											<input type="text" name="user_ctype" id="user_ctype" class="form-control"required placeholder="Current User Type"/>
											<span class="glyphicon  form-control-feedback"></span>
										</div> -->

										<div class="form-group has-feedback"> 
											<p>Change Status :</p>
											<select required id="type"  name="status" class="form-control">
												<!-- <option selected value="">Select Status</option> -->
												<option value="ON">ON</option>
												<option value="OFF">OFF</option>
											</select>
										</div>

										<div class="row">
											<div class="col-xs-8">    
												<div class="checkbox icheck">
												</div>                        
											</div><!-- /.col -->
											<div class="col-xs-4">
												<button type="submit" class="btn btn-primary btn-block">Update User</button>
											</div><!-- /.col -->
										</div>
									</form>
								</div>
							</div><!-- /.col -->
                            <div class="col-md-3"></div>

                        </div><!-- /.row -->
					</div><!-- /.box-body -->
				</div><!-- /.box -->
			</div><!-- /.col -->
		</div><!-- /.row (main row) -->
	</section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
