<?php
include("include/header.php");
include("include/connect.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<!-- Content Header (Page header) -->
<section class="content-header">
  <ol class="breadcrumb">
	<li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
	<li class="active">Study Allocation</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">

  <!-- Main row -->
  <div class="row">
	<!-- Left col -->
	<div class="col-md-8">
	  <!-- MAP & BOX PANE -->
	  <div class="box box-success">
		<div class="box-header with-border">
		  <h3 class="box-title">Allocate/De-allocate Study</h3>
		  <div id="opstat" class="box-tools pull-right"></div>
		</div><!-- /.box-header -->
		<div class="box-body no-padding">
		  	<div class="row">
				  <div class="pad">
						<div id="col_user" class="col-md-12">
						<p>Select User Name :</p>
					            <!-- <label class="radio-inline"><input class="radio" type="radio" name="user_type" value="1">Study Admin</label><br /> -->
								<!-- <label class="radio-inline"><input class="radio" type="radio" name="user_type" value="2">Editor</label><br /> -->
								<!-- <label class="radio-inline"><input class="radio" type="radio" name="user_type" value="3">Viewer</label><br /> -->
						 <?php

                              //$sql = "SELECT z.study_name as sname FROM user_info as x , study_allocation as y , study_info as z WHERE x.username = '".$username."' AND x.user_id = y.user_id AND y.study_id = z.study_id";
                              $sql = "SELECT DISTINCT user_id,username  FROM user_info WHERE username !='admin' AND status !='OFF'"; 
                              $result = $conn->query($sql);

                              if ($result->num_rows > 0) {
                                  while($row = $result->fetch_assoc()) {
                                      echo "<label class='radio-inline' id='radio-inline-".$row['user_id']."'><input class='radio'  type='radio' name='user_id' value='".$row['user_id']."'>".$row['username']."</label><br />" ;
                                      //echo "<input value='".$row['user_id']."' name='user_id' id='radio-inline".$row['user_id']."' class='radio-inline' type='radio'>";
									  //echo "<label style='padding-left:10px;font-weight:500;' for='radio-inline".$row['user_id']."' id='radio-inline".$row['user_id']."' class='radio-inline'>".$row['username']."</label><br />";
                                         }
                              } else {
                                  echo "0 results";
                              }
                              $conn->close();
                        ?>


						</div><!-- /.col -->

						<div id="col_study" class="col-md-0">	   
			              <!-- <p>Select User Name</p>			   
					            <label class="radio-inline"><input type="radio" name="user_name" value="1">user1</label><br />
								<label class="radio-inline"><input type="radio" name="user_name" value="2">user2</label><br />
								<label class="radio-inline"><input type="radio" name="user_name" value="3">user3</label><br /> -->
						</div><!-- /.col -->

						<div id="col_type" class="col-md-0 ">		   
			              <!-- <p>Allocate/De-allocate</p>			   
					          <label><input type="checkbox" class="ckbox" name="study_name" value="1"> study 1 </label><br />
			                  <label><input type="checkbox" class="ckbox" name="study_name" value="2"> study 2 </label><br />
			                  <label><input type="checkbox" class="ckbox" name="study_name" value="3"> study 3 </label><br /> -->
						</div><!-- /.col -->

<script src="../plugins/jQuery/jQuery-2.1.3.min.js"></script>
<script type="text/javascript">

var user_id;
var study_id;
var ds_id;
var user_type;
var user_info;
var study_info;
var user_type_info;

$(document).ready(function () {
	// $("#col_user").hide();
    $('.radio').click(function () {
    	 //alert($('input:radio[name=user_type]:checked').val());
        if ($('input:radio[name=user_id]:checked')) {
            // $("#col_user").show();
            $('#col_type').empty();
            $('#col_study').empty();
            $('#opstat').empty();

            var user_id=$('input:radio[name=user_id]:checked').val();
            user_info=$('#radio-inline-'+user_id).text();

             //alert("username: "+user_info);
		    $.ajax({
	            type: "POST",
	            dataType: "json",
	         
	            url: "../ajax/sql_query_study.php",
	            data: {user_id:user_id},
	            success: function(response) { 
	            	// alert(response.length);

	            	            $('#col_study')
					         	.append($("<p></p>")
					         	.attr("class","dam")
					         	.text("Select Study Name:"));

					          //   $('#opstat')
					         	// .append($("<p></p>")
					         	// .attr("class","dam1")
					         	// .text("Selected username "));

					         	var astdcount=0;
					         	for(i = 0; i < response.length; i++) {

				         	            $('#col_study')
							         	.append($("<input></input>")
							         	.attr("class","radio-inline")
							         	.attr("id","radio_option"+i)
							         	.attr("type","checkbox")
							         	.attr("name","study_name")
							         	.attr("onclick", "optionchk(this,value);")
							         	.attr("value",response[i].study_id));
                                         
                                        if(response[i].status == "ON"){	
                                        astdcount++;
                                        //$( "#radio_option"+i).prop( "checked", true );	
				         	         	$('#col_study')
							         	.append($("<label></label>")
							         	.attr("class","radio-inline")
							         	.attr("id","radio-inline"+i)
							         	.attr("for","radio_option"+i)
							         	.attr("style","padding-left:10px;font-weight:700;")
							         	.text(response[i].study_name));
                                        //$('#radio-inline'+i).attr('checked', true);
							            }
							            else{
							            $('#col_study')
							         	.append($("<label></label>")
							         	.attr("class","radio-inline")
							         	.attr("id","radio-inline"+i)
							         	.attr("for","radio_option"+i)
							         	.attr("style","padding-left:10px;font-weight:500;")
							         	.text(response[i].study_name));
							            }


				         	         	$('#col_study')
		         						.append($("<br>"));
					         	}

					            $('#opstat')
					         	.append($("<p></p>")
					         	.attr("class","dam1")
					         	.text(user_info+" assigned to "+astdcount+" Study" ));

	         	$("#col_user").removeClass("col-md-12").addClass("col-md-6");
	         	$("#col_study").removeClass("col-md-0").addClass("col-md-6"); 


	            }
	        });
        } 
    });

	// $('.radio-inline1').click();
});



function optionchk(checkbox,value) {
    	
    	var id = $(checkbox).attr("id");
    	ds_id=id;
		study_id=value;
		user_id=$('input:radio[name=user_id]:checked').val();
        //console.log("ds_id: "+ds_id);
        var dm=id.substring(13,12);;            
        study_info=$('#radio-inline'+dm).text();

        //alert("study name: "+study_info);

    	if (checkbox.checked) {
    		// $("#col_user").show();
            $('#col_type').empty();
            $('#opstat').empty();
            //$("label[for='"+id+"']").css("font-weight",700);

            study_id=value;
            user_id=$('input:radio[name=user_id]:checked').val();;
             //alert(study_id+" : "+user_id);
		    $.ajax({
	            type: "POST",
	            dataType: "json",
	         
	            url: "../ajax/sql_query_type.php",
	            data: {study_id:study_id,
	                   user_id:user_id},
	            success: function(response) { 
	            	// alert(response.length);

	            	            $('#col_type')
					         	.append($("<p></p>")
					         	.attr("class","dam")
					         	.text("Select Role :"));


					         	var astdcount=0;
					         	for(i = 0; i < response.length; i++) {
					         			//console.log(ds_id);	
				         	         	$('#col_type')
							         	.append($("<input></input>")
							         	.attr("class","chk")
							         	.attr("id","chk_option"+i)
							         	.attr("type","checkbox")
							         	.attr("onclick", "studychkchange(this,value);")							         	
							         	//.attr("onchange", "bold("+"'chk_option"+i+"');")
							         	.attr("name","type_name")
							         	.attr("value",response[i].user_type));
                                        //response[i].status = "ON";
							         	if(response[i].status == "ON"){
							         		    $( "#chk_option"+i).prop( "checked", true );
							         			astdcount++;
						         	         	$('#col_type')
									         	.append($("<label></label>")
									         	.attr("id","chk-inline"+i)
									         	.attr("for","chk_option"+i)
									         	.attr("style","padding-left:10px;font-weight:700;")
									         	.text(response[i].user_type_dc));

							         	}else{
						         	         	$('#col_type')
									         	.append($("<label></label>")
									         	.attr("id","chk-inline"+i)
									         	.attr("for","chk_option"+i)
									         	.attr("style","padding-left:10px;font-weight:500;")
									         	.text(response[i].user_type_dc));	
							         	}

				         	         	$('#col_type')
		         						.append($("<br>"));
					         	}

					            $('#opstat')
					         	.append($("<p></p>")
					         	.attr("class","dam1")
					         	.text("Selected Study : "+study_info));

	         	$("#col_type").removeClass("col-md-6").addClass("col-md-4");
	         	$("#col_user").removeClass("col-md-6").addClass("col-md-4");
	         	$("#col_study").removeClass("col-md-0").addClass("col-md-4"); 
 
	            }
	        });

    	}
    	else
    	{

            user_type=3;
         	$.ajax({
	            type: "POST",
	            dataType: "json",
	         
	            url: "../ajax/sql_query_study_allocation.php",
	            data: {study_id:study_id,
	            	   user_id:user_id,
	            	   user_type:user_type,
	            	   status:"OF"},

	            success: function(response) { 
	            	if (response.success=="Y"){
	            		//alert ("User name : "+user_id+"       Study : "+value+" is De-allocated");
    		    		$('#col_type').empty();
			        	$('#opstat').empty();
			        	$("label[for='"+id+"']").css("font-weight",500);
			        	$('#opstat')
			         	.append($("<p></p>")
			         	.attr("class","dam1")
			         	.text(study_info+" De-allocated for "+user_info));
	            	}
	            	else{
	            		alert("failed !!");
	            	}

	            }
	        });
    	}

    }

function studychkchange(checkbox,value) {

    	//var std_ds_id=ds_id;
    	var id = $(checkbox).attr("id");
		var user_type=value;
		var dm1=id.substring(11,10);;            
        user_type_info=$('#chk-inline'+dm1).text();
        
		//console.log("ds_id: "+user_type_info)

    	if (checkbox.checked) {
            $("label[for='"+ds_id+"']").css("font-weight",700);

            for (var i = 0; i < 3 ; i++) {
            	comid="chk_option"+i;
              	//console.log("comid: "+comid)
              	if(comid==id){
					$('#'+comid).attr('checked', true);
					$("label[for='"+comid+"']").css("font-weight",700);

              	}else{
					$('#'+comid).attr('checked', false);
					$("label[for='"+comid+"']").css("font-weight",500);
              	}

              };

            //$("label[for='"+id+"']").css("font-weight",700);                // alert(study_id);
		    $.ajax({
	            type: "POST",
	            dataType: "json",
	         
	            url: "../ajax/sql_query_study_allocation.php",
	            data: {study_id:study_id,
	            	   user_id:user_id,
	            	   user_type:user_type,
	            	   status:"ON"},

	            success: function(response) { 
	            	// alert(response.length);
	            	if (response.success=="Y"){
	            		//alert ("User name : "+user_id+"       Study : "+value+" is Allocated");
	            		$("label[for='"+id+"']").css("font-weight",700);
	            		$('#opstat').empty();
		        	    $('#opstat')
			         	.append($("<p></p>")
			         	.attr("class","dam1")
			         	.text(study_info+" allocated to "+user_info+" 	as "+user_type_info));

	            	}
	            	else{
	            		alert("failed !!");
	            	}

	            }
	        });
            }
            else {

                // alert ("User name : "+user_id+"       Study : "+value+" is De-allocated");
                            // alert(user_id);
		    $.ajax({
	            type: "POST",
	            dataType: "json",
	         
	            url: "../ajax/sql_query_study_allocation.php",
	            data: {study_id:study_id,
	            	   user_id:user_id,
	            	   user_type:user_type,
	            	   status:"OF"},

	            success: function(response) { 
	            	if (response.success=="Y"){
	            		//alert ("User name : "+user_id+"       Study : "+value+" is De-allocated");
	            		$("label[for='"+id+"']").css("font-weight",500);
		            	$("label[for='"+id+"']").css("font-weight",500);
		            	$("label[for='"+ds_id+"']").css("font-weight",500);
		            	$('#'+ds_id).attr('checked', false);
		            	$('#col_type').empty();
		            	$('#opstat').empty();
		        	    $('#opstat')
			         	.append($("<p></p>")
			         	.attr("class","dam1")
			         	.text(study_info+" De-allocated to "+user_info));
	            	}
	            	else{
	            		alert("failed !!");
	            	}

	            }
	        });
            }
    }    

        
            // alert(user_id);
		    // $.ajax({
	     //        type: "POST",
	     //        dataType: "json",
	         
	     //        url: "ajax/sql_query_study_allocation.php",
	     //        data: {study_id:study_id,
	     //        	   user_id:user_id},

	     //        success: function(response) { 
	     //        	// alert(response.length);

	     //        }
	     //    });


</script>

				  </div><!-- /.row -->
			</div><!-- /.box-body -->
	  </div><!-- /.box -->
	</div><!-- /.col -->          
  </div><!-- /.row (main row) -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->

	
<?php
include("include/footer.php");
?>
      