<?php

error_reporting(E_ALL ^ E_NOTICE);
include("include/connect.php");
$study_name=$_SESSION["study"];
?>
    <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.01
        </div>
        <strong>Copyright &copy; <?php echo date("Y") ?> <a href="http://shaficonsultancy.com">MeD-OMS</a>.</strong> All rights reserved.
    </footer>
</div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <!-- // <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script> -->
    <!-- // <script src="../cssmenu/menuscript.js"></script> -->
    <script src="../plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="../dist/js/app.min.js" type="text/javascript"></script>
   <!-- // <script src="dist/js/demo.js" type="text/javascript"></script>  -->
    <script type="text/javascript" src="../stree/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript" src="../stree/jquery.ztree.excheck-3.5.js"></script>
    <script type="text/javascript" src="../stree/jquery.ztree.exedit-3.5.js"></script>
    
    <!-- notify   -->
    <script src="../plugins/notify/bootstrap-notify.min.js" type="text/javascript"></script>

    <script>
        !function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
    </script>

    <SCRIPT type="text/javascript">

        var setting = {
            view: {
                // addHoverDom: addHoverDom,
                // removeHoverDom: removeHoverDom,

                selectedMulti: true
            },
            check: {
                enable: false
            },
            callback: {
                onRightClick: OnRightClick
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            edit: {
                enable: false
            }
        };



        var zNodes =[

                 <?php
//$study_name="R101";
$sql="SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.data_currency= y.data_currency AND x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency='SP0' ORDER BY x.sortorder";
$result = $conn->query($sql);
// print_r($result);
//echo "<br>";
$count=1;
if ($result->num_rows > 0) {

    while($row = $result->fetch_assoc()) {
        $count++;
        $pgmstat=$row["pgmstat"];
        $outstat=$row["outstat"];


          //code for name argument
           if(strlen(trim($row['section']," ")) == 0){
              $name=$row["type"]." ".$row["tlfnum"]." - ".$row["title"];
                // if($row["l5"]=="00")
                //   $name=$row["type"]." ".(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"].".". (int)$row["l4"]." ".$row["title"];
                // if($row["l4"]=="00")
                //   $name=$row["type"]." ".(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"]." ".$row["title"];
                // if($row["l3"]=="00")
                //   $name=$row["type"]." ".(int)$row["l1"].".". (int)$row["l2"]." ".$row["title"];
                // if($row["l2"]=="00")
                //   $name=$row["type"]." ".(int)$row["l1"]." ".$row["title"];
            }
            else
            {
                    $name=(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"].".". (int)$row["l4"].".". (int)$row["l5"]." ".$row["section"];
                if($row["l5"]=="00")
                    $name=(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"].".". (int)$row["l4"]." ".$row["section"];
                if($row["l4"]=="00")
                    $name=(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"]." ".$row["section"];
                if($row["l3"]=="00")
                    $name=(int)$row["l1"].".". (int)$row["l2"]." ".$row["section"];
                if($row["l2"]=="00")
                    $name=(int)$row["l1"]." ".$row["section"];
            }

          //code for pid argument          
          if ($row["l2"]=="00")
            $row["l2"]=" ";
          if ($row["l3"]=="00")
            $row["l3"]=" ";
          if ($row["l4"]=="00")
            $row["l4"]=" ";
          if ($row["l5"]=="00")
            $row["l5"]=" ";
          $allnum=$row["l1"]. $row["l2"]. $row["l3"]. $row["l4"]. $row["l5"];
          //echo "$allnum";
          $len=iconv_strlen (trim($allnum));
          if ($len==2)
            $p="00";
          else
            $p=substr(trim($allnum),0,$len-2);
          

          //detect pid have child or not
          $child_number=0;
          if( strlen(trim($row['section']," ")) != 0){
              $idtoseach=str_replace("00","",$row['sortorder']);
              // echo "id to search for child count : ".$idtoseach."<br>";
              $sql9="SELECT * FROM toc_".$study_name." WHERE sortorder LIKE '$idtoseach%' ";
              $result9 = $conn->query($sql9);
              $child_number=$result9->num_rows;
              // echo "number of child : ".$child_number."<br><br><br>";
          }

          $lst= "{id: '".trim($row["l1"].$row["l2"].$row["l3"].$row["l4"].$row["l5"])."' ,pId:'".$p."', name: '".$name."'}," ;
          if( strlen(trim($row['section']," ")) != 0 AND $child_number==1)
            $lst= "{id: '".trim($row["l1"].$row["l2"].$row["l3"].$row["l4"].$row["l5"])."' ,pId:'".$p."', name: '".$name."', isParent:true}," ;
          if( strlen(trim($row['section']," ")) == 0)
            $lst= "{id: '".trim($row["l1"].$row["l2"].$row["l3"].$row["l4"].$row["l5"])."' ,pId:'".$p."', name: '".$name."',icon:'../stree/metroStyle/img/state_".$pgmstat."_".$outstat.".png'}," ;
          if( strlen(trim($row['section']," ")) != 0 AND $child_number==1 AND $count-1 == $result->num_rows)
            $lst= "{id: '".trim($row["l1"].$row["l2"].$row["l3"].$row["l4"].$row["l5"])."' ,pId:'".$p."', name: '".$name."', isParent:true}" ;
          elseif( $count-1 == $result->num_rows)
            $lst= "{id: '".trim($row["l1"].$row["l2"].$row["l3"].$row["l4"].$row["l5"])."' ,pId:'".$p."', name: '".$name."',icon:'../stree/metroStyle/img/state_".$pgmstat."_".$outstat.".png'}" ;
        
          echo $lst;
    }
    } else {
          // $lst= "{id: 1 ,pId:0, name: 'No TOC available for this study',icon:'./stree/metroStyle/img/metro2.png'}" ;
          // echo "$lst";
}
?>

                       ];
        
        var pgmid;
        var pgmname;
        var pgmerr;
        var pgmwarn;
        function OnRightClick(event, treeId, treeNode) {
            pgmid=treeNode.id;
            pgmname=treeNode.name;

            if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
                zTree.cancelSelectedNode();
                showRMenu("root", event.clientX - $('#clickevt').offset().left, event.clientY - $('#clickevt').offset().top);
            } else if (treeNode && !treeNode.noR) {
                zTree.selectNode(treeNode);
                showRMenu("node", event.clientX - $('#clickevt').offset().left, event.clientY - $('#clickevt').offset().top);
            }

        }

        function showRMenu(type, x, y) {
             $("#rMenu ul").show();
             if (type=="root") {

                 $("#m_run").hide();
                 $("#m_out").hide();
                 $("#m_log").hide();
                 $("#m_val").hide();
             } else {
                 $("#m_run").show();
                 $("#m_out").show();
                 $("#m_log").show();
                 $("#m_val").show();
            }
            $("#m_sec").hide();
            $("#m_sub_sec").hide();
            $("#m_entry").hide();
            $("#m_move").hide();
            $("#m_update_sechead").hide();
            $("#m_del_sec").hide();
            $("#m_out").hide();
            $("#m_log").hide();
           

            var data = {
             "pgmid": pgmid,
             "pgmname": pgmname
               };
                 $.ajax({
                 type: "POST",
                 dataType: "json",
                 url: ".././ajax/chk_sec_node.php",
                 data: data,
                 success: function(response) {
                    if(response.section!=" "){
                            $("#m_sec").show();
                            $("#m_sub_sec").show();
                            $("#m_entry").show();
                            $("#m_move").show();
                            $("#m_update_sechead").show();
                            $("#m_del_sec").show();
                            $("#m_out").hide();
                            $("#m_log").hide();
                    }
                    else
                    {
                            $("#m_sec").hide();
                            $("#m_sub_sec").hide();
                            $("#m_entry").show();
                            $("#m_move").hide();
                            $("#m_update_sechead").hide();
                            $("#m_del_sec").hide();
                            $("#m_out").show();
                            $("#m_log").show();
                    }
                   }
                });
            rMenu.css({"top":y+"px", "left":x+"px", "visibility":"visible"});

            $("body").bind("mousedown", onBodyMouseDown);
        }
        function hideRMenu() {
            if (rMenu) rMenu.css({"visibility": "hidden"});
            $("body").unbind("mousedown", onBodyMouseDown);
        }
        function onBodyMouseDown(event){
            if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
                rMenu.css({"visibility" : "hidden"});
            }
        }
        var addCount = 1;
        function addTreeNode() {
            hideRMenu();
            var newNode = { name:"newNode " + (addCount++)};
            if (zTree.getSelectedNodes()[0]) {
                newNode.checked = zTree.getSelectedNodes()[0].checked;
                zTree.addNodes(zTree.getSelectedNodes()[0], newNode);
            } else {
                zTree.addNodes(null, newNode);
            }
        }
        function removeTreeNode() {
            hideRMenu();
            var nodes = zTree.getSelectedNodes();
            if (nodes && nodes.length>0) {
                if (nodes[0].children && nodes[0].children.length > 0) {
                    var msg = "If you delete this node will be deleted along with sub-nodes. \n\nPlease confirm!";
                    if (confirm(msg)==true){
                        zTree.removeNode(nodes[0]);
                    }
                } else {
                    zTree.removeNode(nodes[0]);
                }
            }
        }
        function checkTreeNode(checked) {
            var nodes = zTree.getSelectedNodes();
            if (nodes && nodes.length>0) {
                zTree.checkNode(nodes[0], checked, true);
            }
            hideRMenu();
        }
        function resetTree() {
            hideRMenu();
            $.fn.zTree.init($("#treeDemo"), setting, zNodes);
        }
        function alr(treeId, treeNode, clickFlag){
            alert("table id "+pgmid);
        }
        
        function new_sec() {
            var x = document.getElementById("sec_id");
            x.value=pgmid;
            // alert("table id "+pgmid);
            hideRMenu();
        }
        function move_sec() {
            var pg=" "+pgmid;
            var id=pg.trim();
            var n=id.length;
            var sid='0'.repeat(5-n);
            var tosearchid =(pgmid+sid).trim();
            //console.log(tosearchid);
            var x = document.getElementById("mov_sec_id");
            x.value=tosearchid;
            // $('#m_sec_frm #tar_sec_id').clear();
             var data = {
                 "pgmid": pgmid,
                 "pgmname": pgmname
                                     };
                                         $.ajax({
                                         type: "POST",
                                         dataType: "json",
                                         url: "../ajax/mov_sec.php",
                                         data: data,
                                         success: function(response) {
                                         
                                            if(response.length > 0){
                                                for (i=0;i<response.length;i++){
                                                    $('#m_sec_frm #tar_sec_id').append('<option value='+response[i].sortorder+'>'+response[i].sortorder+'</option>');
                                                }
                                            }
                                            //.append('<option value="foo">foo</option>')
                                            //.append('<option value="bar">bar</option>')
                                         //document.getElementById("up_sec_name").value=response.section;
                                             }
                                        });
            hideRMenu();
        }

        function new_sub_sec() {
            var x = document.getElementById("sub_sec_id");
            x.value=pgmid;
            // alert("table id "+pgmid);
            hideRMenu();
        }

        function new_entry() {
            var x = document.getElementById("ne_row_id");
            x.value=pgmid;
            // alert("table id "+pgmid);
            hideRMenu();
        }

        function showout() {
            var data = {
                 "pgmid": pgmid,
                 "pgmname": pgmname
                                     };
                                         $.ajax({
                                         type: "POST",
                                         dataType: "json",
                                         url: "../ajax/pgm_chk.php", //Relative or absolute path to response.php file
                                         data: data,
                                         success: function(response) {
                                          if (response.outstat == 0) {
                                                        alert('Warning:There is no output for this entry. Please run the program and create output.');
                                                }
                                          else if   (response.outstat > 0) {                
                                                            window.open("../ajax/view_out.php?q="+pgmid,"", "scrollbars=yes, resizable=yes, width=1200, height=900");             
                                                }
                                             }
                                        });
                                    hideRMenu();
        }
                
        function showlog() {
            var data = {
                 "pgmid": pgmid,
                 "pgmname": pgmname
                                     };
                                         $.ajax({
                                         type: "POST",
                                         dataType: "json",
                                         url: "../ajax/pgm_chk.php", 
                                         data: data,
                                         success: function(response) {
                                          if (response.outstat == 0) {
                                                        alert('Warning:There is no output for this entry. Please run the program and create output.');
                                                }
                                          else if   (response.outstat > 0) {                
                                                            window.open("../ajax/view_log.php?q="+pgmid,"", "scrollbars=yes, resizable=yes, width=1200, height=900");             
                                                }
                                             }
                                        });
                                    
            // window.open("./ajax/view_log.php?q="+pgmid,"", "scrollbars=yes, resizable=yes, width=1200, height=900");
            hideRMenu();
        }
                

                
        function validation() {
            document.getElementById("pgnam").innerHTML=pgmname;

            var x = document.getElementById("sid");
            x.value=pgmid;
            hideRMenu();
        }
                
        var etitle,epgmname,eoutname,elogname;
        function update_sechead() {
            document.getElementById("secpgnam1").innerHTML=pgmname;

            var x = document.getElementById("up_sec_id");
            x.value=pgmid;
                        
                        var data = {
                 "pgmid": pgmid,
                 "pgmname": pgmname
                                     };
                                         $.ajax({
                                         type: "POST",
                                         dataType: "json",
                                         url: "../ajax/edit_entry.php",
                                         data: data,
                                         success: function(response) {
                                        //   etitle=response.title;
                                        //   epgmname=response.pgmname;
                                        //   eoutname=response.outname;
                                        //   elogname=response.logname;
                                        //     console.log(etitle);
                                        // document.getElementById("title").value=etitle;
                                        // document.getElementById("pgmname").value=epgmname;
                                        // document.getElementById("outname").value=eoutname;
                                        // document.getElementById("logname").value=elogname;
                                        document.getElementById("up_sec_name").value=response.section;
                                             }
                                        });
                                     hideRMenu();
        }
                
        function uploadp() {
            document.getElementById("pgnam_up").innerHTML=pgmname;

            var x = document.getElementById("dl_sid");
            x.value=pgmid;
            hideRMenu();
        }
                
        function downloadp() {
                    var data = {
                                         "pgmid": pgmid,
                                         "pgmname": pgmname
                                     };
                                         $.ajax({
                                         type: "POST",
                                         dataType: "json",
                                         url: "../ajax/program_download.php",
                                         data: data,
                                         success: function(response) {
                                                // Download(response.path);
                                                console.log(response.path);
                                                 open(response.path);
                                             }
                                        });
                                             
                                     hideRMenu();       
        }

        function del_sec1() {
            var x = document.getElementById("delseccon");
            x.value=pgmname;
            hideRMenu();       
        }
        function del_sec2() {
            var data = {
                         "pgmid": pgmid
                        };

                                 $.ajax({
                                 type: "POST",
                                 dataType: "json",
                                 url: "./ajax/delete_section.php", 
                                 data: data,
                                 success: function(response) {
                                    if(response.success=="1"){
                                        // alert("Section deleted !!");
                                        location.reload();
                                    }else{
                                        alert("Failed to delete the Section!!");
                                    }
                                      
                                     }
                                });
        }
                
        
                var zTree, rMenu;
                $(document).ready(function(){
                    $.fn.zTree.init($("#treeDemo"), setting, zNodes);
                    zTree = $.fn.zTree.getZTreeObj("treeDemo");
                    rMenu = $("#rMenu");
                });

                //update user request
                $(document).ready(function(){
                   $("#username").on('change', function()  {
                    var data = $("#username").val();
                    //alert(data);
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                     
                        url: "../ajax/load_user_info.php",
                        data: {user_id:data},
                        success: function(data) { 
                          $("#fullname").val(data.fullname);
                          $("#email").val(data.email);
                          $("#type").val(data.status);
                        }
                      });
                      return false;
                    
                  });
                  });


                //new entry add functionality 
                function ne_pgmnamf(val) {
                    var ne_outnum = document.getElementById("ne_outnum");
                    var ne_pgtyp = document.getElementById("ne_pgtyp").value;
                    ne_outnum.value=ne_pgtyp.substr(0, 1);

                    var ne_pid=document.getElementById("ne_row_id");
                    var ne_lognam = document.getElementById("ne_lognam");
                    ne_lognam.value=val;

                    var ne_pgloc = document.getElementById("ne_pgloc");
                    ne_pgloc.value='pgm/ctr/'+val+'.sas';

                    var ne_outnam = document.getElementById("ne_outnam");
                    var ne_outnum = document.getElementById("ne_outnum").value;
                    ne_outnam.value=val+'_'+ne_outnum.toLowerCase();

                    var ne_outloc = document.getElementById("ne_outloc");
                    ne_outloc.value='lst/'+val+'_'+ne_outnum.toLowerCase()+'.lst';
                }

                function ne_pgtypf(val) {
                    var x = document.getElementById("ne_outnum");
                    x.value=val.substr(0, 1);

                    var ne_outnam = document.getElementById("ne_outnam");
                    var ne_outnum = document.getElementById("ne_outnum").value;
                    var ne_pgmnam = document.getElementById("ne_pgmnam").value;
                    ne_outnam.value=ne_pgmnam+'_'+ne_outnum.toLowerCase();

                    var ne_outloc = document.getElementById("ne_outloc");
                    ne_outloc.value='lst/'+ne_pgmnam+'_'+ne_outnum.toLowerCase()+'.lst';
                }

                function ne_outnumf(val) {

                    var ne_outnam = document.getElementById("ne_outnam");
                    var ne_pgmnam = document.getElementById("ne_pgmnam").value;
                    ne_outnam.value=ne_pgmnam+'_'+val.toLowerCase();

                    var ne_outloc = document.getElementById("ne_outloc");
                    ne_outloc.value='lst/'+ne_pgmnam+'_'+val.toLowerCase()+'.lst';
                }


                function chk_study() {

                  var study_name = document.getElementById("study_name").value.trim();
                  var invalid_cha_count = study_name.search("/");
                  //var invalid_cha_count = study_name.search('\\');
                  var invalid_cha_count = study_name.search(":");
                  //var invalid_cha_count = study_name.search("*");
                  //var invalid_cha_count = study_name.search("?");
                  var invalid_cha_count = study_name.search("<");
                  var invalid_cha_count = study_name.search(">");
                  var invalid_cha_count = study_name.search("|");
                  var invalid_cha_count = study_name.search("'");
                  var invalid_cha_count = study_name.search('"');

                  console.log(invalid_cha_count);
                  if (invalid_cha_count > 0) {
                             $.notify({
                              icon: 'glyphicon glyphicon-star',
                              message: "undefined character found in the study folder name !!"
                            });  
                             
                            var ne_outloc = document.getElementById("study_name");
                            ne_outloc.value=' ';                 
                  } 

                    //alert(data);
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                     
                        url: "../ajax/chk_dup_study.php",
                        data: {study_name:study_name},
                        success: function(data) { 
                          if (data.count == 0) {

                          } else{
                            $.notify({
                              icon: 'glyphicon glyphicon-star',
                              message: "This Study ["+data.study_name+"] Already exists !!"
                            });

                            var ne_outloc = document.getElementById("study_name");
                            ne_outloc.value=' ';
                          };

                        }
                      });

                }

                  function chk_exist_user() {

                  var username1 = document.getElementById("username1").value.trim();

                    //alert(data);
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                     
                        url: "../ajax/chk_exist_user.php",
                        data: {username1:username1},
                        success: function(data) { 
                          if (data.count == 0) {

                          } else{
                            $.notify({
                              icon: 'glyphicon glyphicon-star',
                              message: "This user ["+data.username+"] Already exists !!"
                            });

                            var ne_outloc = document.getElementById("username1");
                            ne_outloc.value=' ';
                          };

                        }
                      });

                }


                
</SCRIPT>

    <style type="text/css">
    div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
    div#rMenu ul li{
    margin: 1px 0;
    padding: 0 5px;
    cursor: pointer;
    list-style: none outside none;
    background-color: #DFDFDF;
    }
    </style>

<!--  validation   -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                            <div class="box box-success">
                                     <div class="box-header with-border">
                                                <h3 class="box-title">Validation Program </h3>
                                                <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                                                </div>
                                     </div><!-- /.box-header -->

                                    <div class="box-body no-padding">
                                                <div class="row">
                                                        <div class="col-md-12 col-sm-8">
                                                                <div class="pad">
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span id="pgnam"></span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span>Change Program Status:</span>
                                                                                        <form action="study.php" method="post">
                                                                                                <div class="form-group has-feedback">
                                                                                                        <input id="sid" type="hidden" name="sid" value=>

                                                                                                        <select name="study_status" class="form-control">
                                                                                                                
                                                                                                                <option value="1">To be validate</option>;
                                                                                                                <option value="2">To be modify</option>;
                                                                                                                <option value="3">Validated</option>;
                                                                                                        </select>

                                                                                                </div>
                                                                                                <div class="row">
                                                                                                        <div class="col-xs-8">
                                                                                                                <div class="checkbox icheck">

                                                                                                                </div>
                                                                                                        </div><!-- /.col -->
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                                                                                        </div>
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                                                                                                        </div><!-- /.col -->
                                                                                                </div>
                                                                                        </form>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div><!-- /.col -->
                                                </div><!-- /.row -->
                                    </div><!-- /.box-body -->
                            </div>
                 </div>
         </div>
</div>



<!--  upload program    -->
<div class="modal fade bs-example-up-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">upload program</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">

                                <div class="row">
                                        <div class="col-md-12">
                                            <span id="pgnam_up"></span>
                                        </div>
                                </div>

                               <form action="study.php" method="post" enctype="multipart/form-data">                                          

                                          <div class="form-group has-feedback">
                                              <input id="dl_sid" type="hidden" name="dl_sid" value=>
                                              <p>Select Program file:</p>
                                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <span class="btn btn-primary btn-file"><span class="fileupload-new">Select file</span>
                                                     <input  type="file" name="fileToUpload" id="fileToUpload"></span>
                                                   <span class="fileupload-preview"></span>
                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                                              </div>

                                          </div>

                                          <div class="row">
                                              <div class="col-xs-8">
                                                  <div class="checkbox icheck">
                                                  </div>
                                              </div><!-- /.col -->
                                              <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                              </div>
                                              <div class="col-xs-2">
                                                  <input type="submit" class="btn btn-primary btn-block btn-flat" value="Upload" name="submit">
                                              </div><!-- /.col -->
                                          </div>
                               </form>

                            </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
</div>

<!--  New section   -->
<div class="modal fade bs-example-new-sec-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                            <div class="box box-success">
                                     <div class="box-header with-border">
                                                <h3 class="box-title">Add New Section</h3>
                                                <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                                                </div>
                                     </div><!-- /.box-header -->

                                    <div class="box-body no-padding">
                                                <div class="row">
                                                        <div class="col-md-12 col-sm-8">
                                                                <div class="pad">
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span id="pgnam1"></span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        
                                                                                        
                                                                                        <form action="study.php" method="post">
                                                                                                <div class="form-group has-feedback">
                                                                                                        <input id="sec_id" type="hidden" name="sec_id" value=>
                                                                                                         <div class="form-group has-feedback">
                                                                                                             <p><b> Enter Section Name :</b></p>
                                                                                                             <input type="text" name="sec_name" id="sec_name" class="form-control" value="" required />
                                                                                                         </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                        <div class="col-xs-8">
                                                                                                                <div class="checkbox icheck">
                                                                                                                </div>
                                                                                                        </div><!-- /.col -->
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                                                                                        </div>
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Insert Section</button>
                                                                                                        </div><!-- /.col -->
                                                                                                </div>
                                                                                        </form>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div><!-- /.col -->
                                                </div><!-- /.row -->
                                    </div><!-- /.box-body -->
                            </div>
                 </div>
         </div>
</div>



<!--   move section   -->
<div class="modal fade bs-example-mov-sec-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                            <div class="box box-success">
                                     <div class="box-header with-border">
                                                <h3 class="box-title">Move Section</h3>
                                                <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                                                </div>
                                     </div><!-- /.box-header -->

                                    <div class="box-body no-padding">
                                                <div class="row">
                                                        <div class="col-md-12 col-sm-8">
                                                                <div class="pad">
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span id="pgnam1"></span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        
                                                                                        
                                                                                        <form id="m_sec_frm" action="study.php" method="post">
                                                                                                <div class="form-group has-feedback">
                                                                                                        <!-- <input id="mov_sec_id" type="hidden" name="mov_sec_id" value=> -->
                                                                                                         <div class="form-group has-feedback">
                                                                                                             <p><b> Selected Section :</b></p>
                                                                                                             <input type="text" name="mov_sec_id" id="mov_sec_id" class="form-control" value="" required />
                                                                                                         </div>
                                                                                                         <div class="form-group has-feedback">
                                                                                                             <p><b> Move To Section :</b></p>
                                                                                                             <select id="tar_sec_id" name="tar_sec_id" class="form-control">
                                                                                                             <!-- <option value=""></option> -->

                                                                                                             </select>
                                                                                                             <!-- <input type="text" name="sec_name" id="sec_name" class="form-control" value="" required /> -->
                                                                                                         </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                        <div class="col-xs-8">
                                                                                                                <div class="checkbox icheck">
                                                                                                                </div>
                                                                                                        </div><!-- /.col -->
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                                                                                        </div>
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Move</button>
                                                                                                        </div><!-- /.col -->
                                                                                                </div>
                                                                                        </form>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div><!-- /.col -->
                                                </div><!-- /.row -->
                                    </div><!-- /.box-body -->
                            </div>
                 </div>
         </div>
</div>

<!--  Update section header   -->
<div class="modal fade bs-example-update_sechead-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                            <div class="box box-success">
                                     <div class="box-header with-border">
                                                <h3 class="box-title">Update Section Header</h3>
                                                <div class="box-tools pull-right">
                                                        <span>Study Name :<?php echo $_SESSION["study"] ?></span>

                                                </div>
                                     </div><!-- /.box-header -->

                                    <div class="box-body no-padding">
                                                <div class="row">
                                                        <div class="col-md-12 col-sm-8">
                                                                <div class="pad">
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span id="secpgnam1"></span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        
                                                                                        
                                                                                        <form action="study.php" method="post">
                                                                                                <div class="form-group has-feedback">
                                                                                                        <input id="up_sec_id" type="hidden" name="up_sec_id" value=>
                                                                                                         <div class="form-group has-feedback">
                                                                                                             <p><b>Section Name :</b></p>
                                                                                                             <input type="text" name="up_sec_name" id="up_sec_name" class="form-control" value="" required/>
                                                                                                         </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                        <div class="col-xs-8">
                                                                                                                <div class="checkbox icheck">
                                                                                                                </div>
                                                                                                        </div><!-- /.col -->
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                                                                                        </div>
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Update</button>
                                                                                                        </div><!-- /.col -->
                                                                                                </div>
                                                                                        </form>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div><!-- /.col -->
                                                </div><!-- /.row -->
                                    </div><!-- /.box-body -->
                            </div>
                 </div>
         </div>
</div>



<!--  New sub section   -->
<div class="modal fade bs-example-new-sub-sec-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                            <div class="box box-success">
                                     <div class="box-header with-border">
                                                <h3 class="box-title">Add New Sub Section</h3>
                                                <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                                                </div>
                                     </div><!-- /.box-header -->

                                    <div class="box-body no-padding">
                                                <div class="row">
                                                        <div class="col-md-12 col-sm-8">
                                                                <div class="pad">
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span id="pgnam1"></span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        
                                                                                        
                                                                                        <form action="study.php" method="post">
                                                                                                <div class="form-group has-feedback">
                                                                                                        <input id="sub_sec_id" type="hidden" name="sub_sec_id" value=>
                                                                                                         <div class="form-group has-feedback">
                                                                                                             <p><b> Enter Sub Section Name :</b></p>
                                                                                                             <input type="text" name="sub_sec_name" id="sub_sec_name" class="form-control" value="" required/>
                                                                                                         </div>
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                        <div class="col-xs-8">
                                                                                                                <div class="checkbox icheck">
                                                                                                                </div>
                                                                                                        </div><!-- /.col -->
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                                                                                        </div>
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Insert Sub Section</button>
                                                                                                        </div><!-- /.col -->
                                                                                                </div>
                                                                                        </form>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div><!-- /.col -->
                                                </div><!-- /.row -->
                                    </div><!-- /.box-body -->
                            </div>
                 </div>
         </div>
</div>



<!--  New entry   -->
<div class="modal fade bs-example-new-entry-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
         <div class="modal-dialog modal-lg">
                 <div class="modal-content">
                            <div class="box box-success">
                                     <div class="box-header with-border">
                                                <h3 class="box-title">Add New Entry</h3>
                                                <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                                                </div>
                                     </div><!-- /.box-header -->

                                    <div class="box-body no-padding">
                                                <div class="row">
                                                        <div class="col-md-12 col-sm-8">
                                                                <div class="pad">
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        <span id="pgnam1"></span>
                                                                                </div>
                                                                        </div>
                                                                        <div class="row">
                                                                                <div class="col-md-12">
                                                                                        
                                                                                        
                                                                                        <form action="study.php" method="post">
                                                                                                <div class="form-group has-feedback">
                                                                                                        <input id="ne_row_id" type="hidden" name="ne_row_id" value=>
                                                                                                        
                                                                                                            <div class="form-group has-feedback">
                                                                                                                 <p><b> Enter Output Title :</b></p>
                                                                                                                 <input required type="text" name="ne_title" id="ne_title" class="form-control" value=""/>
                                                                                                            </div>

                                                                                                        <div class="row">
                                                                                                            <div class="col-md-3 form-group has-feedback">
                                                                                                                <p><b> Enter Program Name :</b></p>
                                                                                                                 <input required type="text" name="ne_pgmnam" id="ne_pgmnam" class="form-control" value="" onchange="ne_pgmnamf(this.value)"/>
                                                                                                            </div>
                                                                                                            <div class="col-md-3 form-group has-feedback">
                                                                                                            <!-- <p><b> Enter Program type:</b></p> -->
                                                                                                                   <label for="ne_pgtyp">Select Program Type :</label>
                                                                                                                      <select class="form-control" id="ne_pgtyp" name="ne_pgtyp" onchange="ne_pgtypf(this.value)">
                                                                                                                        <option>Table</option>
                                                                                                                        <option>Listing</option>
                                                                                                                        <option>Graph</option>
                                                                                                                      </select>
                                                                                                            </div>
                                                                                                                <div class="col-md-2 form-group has-feedback">
                                                                                                                <p><b> Output Number :</b></p>
                                                                                                                 <input required type="text" name="ne_outnum" id="ne_outnum" class="form-control" value="" onchange="ne_outnumf(this.value)"/>
                                                                                                            </div>
                                                                                                            <div class="col-md-2 form-group has-feedback">
                                                                                                                    <p><b> Output Name :</b></p>
                                                                                                                     <input required type="text" name="ne_outnam" id="ne_outnam" class="form-control" value="" />
                                                                                                            </div>
                                                                                                            <div class="col-md-2 form-group has-feedback">
                                                                                                                    <p><b> log Name :</b></p>
                                                                                                                     <input required type="text" name="ne_lognam" id="ne_lognam" class="form-control" value="" />
                                                                                                            </div>

                                                                                                        </div>
                                                                                                        <div class="row">    
                                                                                                            <div class="col-md-6 form-group has-feedback">
                                                                                                                    <p><b> Program location :</b></p>
                                                                                                                     <input required type="text" name="ne_pgloc" id="ne_pgloc" class="form-control" value="" />
                                                                                                            </div>
                                                                                                             <div class="col-md-6 form-group has-feedback">
                                                                                                                    <p><b> Output location :</b></p>
                                                                                                                     <input required type="text" name="ne_outloc" id="ne_outloc" class="form-control" value="" />
                                                                                                            </div>
                                                                                                        </div>    
                                                                                                </div>
                                                                                                <div class="row">
                                                                                                        <div class="col-xs-8">
                                                                                                                <div class="checkbox icheck">
                                                                                                                </div>
                                                                                                        </div><!-- /.col -->
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Cancel</button>
                                                                                                        </div>
                                                                                                        <div class="col-xs-2">
                                                                                                                <button type="submit" class="btn btn-primary btn-block btn-flat">Create</button>
                                                                                                        </div><!-- /.col -->
                                                                                                </div>
                                                                                        </form>
                                                                                </div>
                                                                        </div>
                                                                </div>
                                                        </div><!-- /.col -->
                                                </div><!-- /.row -->
                                    </div><!-- /.box-body -->
                            </div>
                 </div>
         </div>
</div>


<!--  section delete confirmation   -->

<div class="modal fade bs-example-delcon-modal-sm" id="ld" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="box box-success">
                     <div class="box-header with-border">
                                <h3 class="box-title" style="color:red;"> Do you want to delete this section ??</h3>   
                     </div><!-- /.box-header -->

                      <div class="box-body no-padding">
                                <div class="row">
                                        <div class="col-md-12 col-sm-8">
                                                <div class="pad">
                                                        <div class="row">
                                                                <div class="col-md-12">
                                                                        <span id="delseccon"></span>
                                                                </div>
                                                        </div>
                                                        <div class="row">
                                                                <div class="col-md-12">      
                                                                        <div class="row">
                                                                                
                                                                                <div class="col-xs-6">
                                                                                        <button onclick="del_sec2();" type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">Yes</button>
                                                                                </div>
                                                                                <div class="col-xs-6">
                                                                                        <button type="button" class="btn btn-primary btn-block btn-flat" data-dismiss="modal">No</button>
                                                                                </div><!-- /.col -->
                                                                        </div> 
                                                                </div>
                                                        </div>
                                                </div>
                                        </div><!-- /.col -->
                                </div><!-- /.row -->
                    </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

  </body>
</html>
