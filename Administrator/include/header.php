<?php

include("include/connect.php");
session_start();

$user_check=$_SESSION['login_user'];

$ses_sql=$conn->query("select * from user_info where username='$user_check' ");
//print_r($ses_sql);

  while($row = $ses_sql->fetch_assoc()) {
                        $login_session = $row['username'];
						$usertype = $row['user_type'];						
                         }
 if(!isset($login_session))
 {
 header("Location: ../index.php");
 }
 if($usertype != 0)
 {
    header("Location: ../index.php");
 }
 
 $username = $_SESSION['login_user'];
 $sql="SELECT count(z.study_name) as snum FROM study_info as z"; 
 $countstudy = $conn->query($sql); 

  while($row = $countstudy->fetch_assoc()) {
                        $snum = $row['snum'];                     
                         }

if( ! empty($_POST['study_name'])){
$stdname=$_POST['study_name'];
$_SESSION["study"] = $stdname;

}
if( ! empty($_GET['study_name'])){
$stdname=$_GET['study_name'];
$_SESSION["study"] = $stdname;

}                       
?>    

<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>MeD-OMS | SYSTEM</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />    
    <!-- FontAwesome 4.3.0 -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Ionicons 2.0.0 -->
    <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
    <!-- Theme style -->
    <link href="../dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins 
         folder instead of downloading all of them to reduce the load. -->
    <link href="../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- css top menu -->
    <link rel="stylesheet" href="../cssmenu/menustyles.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    
    <link rel="stylesheet" href="../stree/metroStyle/metroStyle.css" type="text/css">
    
    <link rel="shortcut icon" href="../dist/img/scbd.ico">

    <style>
    .scbd {
    height:80px;
    background:#ffffff url('../dist/img/logo.png') no-repeat;
    text-align: center;
    position: relative;
    background-position:center;
    z-index:1000;
    }
    .sidebar-menu {
        list-style: none;
        margin: 0;
        padding-top: 40px;
    }
     li, span {font-size:17px;}
    </style>
  </head>


  <body class="skin-blue layout-wide">
    <div class="scbd"></div>
    <div class="wrapper">
      <header class="main-header">
        <!-- Logo -->
        <a href="admin.php" class="logo"><b>MeD-OMS</b></a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation" >
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>


          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav"> 

              <!-- drop down menu for create_pdf.php-->            
              <!-- <li class="dropdown user user-menu">
              <a href="new_study.php">
                <i class="fa fa-upload"></i>       
                  <span class="hidden-xs">New Study</span>
                </a>
              </li> -->
              <!-- drop down menu for create_pdf.php--> 

              <!-- drop down menu for Crate RTF-->            
         <!--      <li class="dropdown user user-menu">
                <a href="new_user.php" >
                  <i class="fa fa-user-plus"></i>
                  <span class="hidden-xs">New User</span>
                </a>
              </li> -->
              <!-- drop down menu for Crate RTF--> 

              <!-- drop down menu for Execute-->            
      <!--         <li class="dropdown user user-menu">
                <a href="update_user.php" >
                 <i class="fa fa-users"></i>
                  <span class="hidden-xs">Update Users</span>
                </a>
              </li> -->
              <!-- drop down menu for Execute--> 

              <!-- drop down menu for Manage-->            
   <!--            <li class="dropdown user user-menu">
                <a href="study_allocation.php" >
                  <i class="fa fa-check-square-o"></i>
                  <span class="hidden-xs">Study Allocation</span>
                </a>
              </li> -->
              <!-- drop down menu for Manage--> 

              <!-- drop down menu for Maintain-->            
<!--               <li class="dropdown user user-menu">
                <a href="manage_study.php" >
                  <i class="fa fa-cogs"></i>
                  <span class="hidden-xs">Manage</span>
                </a>
              </li> -->
              <!-- drop down menu for Maintain--> 


              <!-- drop down menu for study-->            
              <li class="dropdown user user-menu">
                <a href="study.php" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-folder-o"></i>
                  <span class="hidden-xs">Study</span>
                </a>
                <ul class="dropdown-menu">
                <?php

                // $sql = "SELECT z.study_name as sname FROM user_info as x , study_allocation as y , study_info as z WHERE x.username = '".$username."' AND x.user_id = y.user_id AND y.study_id = z.study_id";
                $sql = "SELECT z.study_name as sname FROM study_info as z"; 
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                  // output data of each row
                  while($row = $result->fetch_assoc()) {
                      echo "<li><a href='study.php?study_name=". $row["sname"]."'><i class='fa fa-book'></i> ". $row["sname"]."</a></li>" ;
                         }
                } else {
                      echo "0 results";
                }
                $conn->close();
                ?> 

                </ul>
              </li>
              <!-- drop down menu for study--> 



              <!-- drop down menu for user-->            
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-user"></i>
                  <span class="hidden-xs"><?php  echo "$username";  ?> </span>
                </a>
                <ul class="dropdown-menu">

                  <li class="user-footer">
                    <div class="pull-left">
                     <p> <small>New Study:<?php  echo " $snum";  ?> Total Study:<?php  echo " $snum";  ?></small></p>              
                    </div>
                  </li>
                  <li class="user-footer">
                    <div class="pull-right">
                      <a href="../logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                    <div class="pull-left">
                      <a href="change_password.php" class="btn btn-default btn-flat">Change Password</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- drop down menu for user--> 
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">

          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
              

            <li>
              <a href="admin.php">
                <i class="fa fa-home"></i> <span>Admin Home</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>

            
            <li>
              <a href="new_study.php">
                <i class="fa fa-upload"></i> <span>New Study</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
            <li>
              <a href="new_user.php">
                <i class="fa fa-user-plus"></i> <span>New User</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
            <li>
              <a href="update_user.php">
                <i class="fa fa-users"></i> <span>Update User</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
            <li>
              <a href="study_allocation2.php">
                <i class="fa fa-check-square-o"></i> <span>Study Allocation</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>

<!--             <li>
              <a href="manage_study.php">
                <i class="fa fa-cogs"></i> <span>Manage</span> <small class="label pull-right bg-green"></small>
              </a>
            </li> -->
     
            <li>
              <a href="change_password.php">
                <i class="fa fa-exchange"></i> <span>Change Password</span> <small class="label pull-right bg-green"></small>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>