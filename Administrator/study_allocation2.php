<?php
include("include/header.php");
include("include/connect.php");
?>

<link href="../plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-home"></i> Home</a></li>
            <li class="active">Study Allocation</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-12">
                <!-- MAP & BOX PANE -->
                <div class="box box-success">
                    <div class="box-header with-border">
                        <h3 class="box-title">Allocate/De-allocate Study</h3>
                        <div id="opstat" class="box-tools pull-right">
                            <button class="btn btn-primary pull-right" id="" data-toggle="modal" data-target="#allocate_study_modal"><span class="fa fa-plus"></span> Allocate Study </button>
                        </div>
                    </div><!-- /.box-header -->
                    <div class="box-body no-padding">
                        <div class="row">
                            <div class="pad">
                                <div class="col-md-12">
                                    <table class="table table-hover" id="allocateTable">
                                        <thead>
                                        <tr>
                                            <th>Username</th>
                                            <th>Study + Role</th>
                                            <th>Status</th>
                                            <th>Start Date</th>
                                            <th>Stop Date</th>
                                            <th>Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $sql="SELECT DISTINCT username,user_info.user_id FROM study_allocation LEFT JOIN user_info ON study_allocation.user_id=user_info.user_id";
                                        $result = $conn->query($sql);
                                        while($row = $result->fetch_assoc()) {
                                            $tableTD1="";
                                            $tableTD2="";
                                            $tableTD3="";
                                            $tableTD4="";
                                            $sql="SELECT study_allocation.allocation_date,study_allocation.stop_date ,study_allocation.status as a_status ,study_info.study_name, user_type_info.user_type_dc FROM study_allocation 
                                                    LEFT JOIN study_info ON study_allocation.study_id=study_info.study_id 
                                                    LEFT JOIN user_type_info ON study_allocation.user_type=user_type_info.user_type 
                                                    WHERE study_allocation.user_id='".$row['user_id']."'";
                                            $result2 = $conn->query($sql);
                                            $i=0;
                                            while($row2 = $result2->fetch_assoc()){
                                                $i++;
                                                $startDate=date("d-M-Y", strtotime($row2['allocation_date']));
                                                if($row2['stop_date']=="0000-00-00 00:00:00")
                                                    $endDate="";
                                                else $endDate=date("d-M-Y", strtotime($row2['stop_date']));

                                                $tableTD1=$tableTD1.$row2['study_name']." <b>[".$row2['user_type_dc']."]</b>";
                                                $tableTD2=$tableTD2.$row2['a_status'];
                                                $tableTD3=$tableTD3.$startDate;
                                                $tableTD4=$tableTD4.$endDate;

                                                if($result2->num_rows!=$i){
                                                    $tableTD1=$tableTD1."<br><br>";
                                                    $tableTD2=$tableTD2."<br><br>";
                                                    $tableTD3=$tableTD3."<br><br>";
                                                    $tableTD4=$tableTD4."<br><br>";
                                                }

                                            }
                                            //echo "<br><br>";
                                            $tableRow="<tr>".
                                                "<td>".$row['username']."</td>".
                                                "<td>".$tableTD1."</td>".
                                                "<td>".$tableTD2."</td>".
                                                "<td>".$tableTD3."</td>".
                                                "<td>".$tableTD4."</td>".
                                                "<td>"."<a data-user-id='".$row['user_id']."' style='cursor: pointer;' title='Edit' class='editBtn'><i class='fa fa-edit'></i></a>"."</td>".
                                                "</tr>";

                                            echo $tableRow;
                                        }
                                        ?>
                                        </tbody>
                                    </table>

                                </div>
                            </div><!-- /.row -->
                        </div><!-- /.box-body -->
                    </div><!-- /.box -->
                </div><!-- /.col -->
            </div><!-- /.row (main row) -->
    </section><!-- /.content -->



</div><!-- /.content-wrapper -->


<!-- Modal for Allocate new study -->
<div id="allocate_study_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Allocate New Study</h4>
            </div>
            <div class="modal-body">
                <form action="" method="post" id="allocate_user_form">
                    <div class="row">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">

                            <div class="form-group">
                                <label for="username">User Name :</label>
                                <select class="form-control" id="username" required="">
                                    <option value="">Select User</option>
                                    <?php
                                    $sql="SELECT user_info.user_id,user_info.username FROM user_info WHERE  user_type!='0' AND user_info.user_id NOT IN (SELECT study_allocation.user_id FROM study_allocation WHERE status = 'ON')";
                                    $result = $conn->query($sql);
                                    while($row = $result->fetch_assoc()){
                                        echo "<option value='".$row['user_id']."'>".$row['username']."</option>";

                                    }
                                    ?>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="studyname">Study Name :</label>
                                <select class="form-control" id="studyname" required="">
                                    <option value="">Select Study</option>
                                    <?php
                                    $sql="SELECT * FROM study_info WHERE status='ON'";
                                    $result = $conn->query($sql);
                                    while($row = $result->fetch_assoc()){
                                        echo "<option value='".$row['study_id']."'>".$row['study_name']."</option>";

                                    }
                                    ?>

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="role">Role :</label>
                                <select class="form-control" id="role" required="">
                                    <option value="">Select Role</option>
                                    <?php
                                    $sql="SELECT * FROM user_type_info WHERE user_type_dc!='Super Admin'";
                                    $result = $conn->query($sql);
                                    while($row = $result->fetch_assoc()){
                                        echo "<option value='".$row['user_type']."'>".$row['user_type_dc']."</option>";

                                    }
                                    ?>
                                </select>
                            </div>


                            <button type="hidden" id="submit_hide" style="display: none;">

                            </button>



                        </div>
                        <div class="col-md-2">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="saveAllocation" class="btn btn-primary">Save</button>
                    </div>

                </form>
            </div>

        </div>

    </div>
</div>


<!-- Modal for Update Allocation -->
<div id="update_allocation_modal" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <div class="pull-right">
                    <button class="btn btn-primary" id="addStudyBtn">Add Study</button>
                </div>
                <h4 class="modal-title">Update Allocation For User : <span id="userName"> </span> </h4>

            </div>
            <div class="modal-body">
                <form action="" method="post" id="update_allocate_user_form">
                    <div class="row">

                        <div class="col-md-2">
                        </div>
                        <div class="col-md-8">

                            <table class="table table-hover" id="updateTable">
                                <thead>
                                <tr>
                                    <th>Study Name</th>
                                    <th>Role</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                <tr>
                                    <td> <input type="hidden" name="preStudy" value="study_001">study_001</td>
                                    <td><div> <select class="form-control preRole"> <option value="trail admin" selected=""> trail admin</option><option value="editor"> editor</option><option value="viewer"> viewer</option></select></div></td>
                                    <td> <input type="hidden" name="roleID" value="27">
                                        <a style="cursor: pointer; color:red;" title="Delete" class="delBtn">
                                            <i class="fa fa-trash"></i>
                                        </a>
                                    </td>

                                </tr>

                                </tbody>
                            </table>

                            <div class="row" style="display: none;" id="updateRoleHidden">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="studynameUpdate">Study Name :</label>
                                        <select class="form-control studynameUpdate" id="studynameUpdate" name="studynameUpdate" required="">
                                            <option value="">Select Study</option>

                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="role">Role :</label>
                                        <select class="form-control roleUpdate" id="roleUpdate" name="roleUpdate" required="">
                                            <option value="">Select Role</option>
                                            <option value="trail admin">Trail Admin</option>
                                            <option value="editor">Editor </option>
                                            <option value="viewer">Viewer </option>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <button type="hidden" id="submit_hide" style="display: none;">

                            </button>

                        </div>
                        <div class="col-md-2">
                        </div>


                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="updateAllocation" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>

        </div>

    </div>
</div>

<?php
include("include/footer.php");
?>

<script src="../plugins/datatables/jquery.dataTables.js"></script>
<script src="../plugins/datatables/dataTables.bootstrap.js"></script>

<script type="text/javascript">
    var user_id;
    $(document).ready(function() {
        $('#allocateTable').dataTable();

        $( "#saveAllocation" ).click(function(e) {
            e.preventDefault();
            if(!$('#allocate_user_form')[0].checkValidity()) {
                $('#submit_hide').click();
            }else{
                var study_id  = $( "#studyname option:selected" ).val();
                var user_id   = $( "#username option:selected" ).val();
                var user_type = $( "#role option:selected" ).val();
                $.ajax({
                    type: "POST",
                    dataType: "json",

                    url: "../ajax/sql_query_study_allocation.php",
                    data: {study_id:study_id,
                        user_id:user_id,
                        user_type:user_type,
                        status:"ON"},

                    success: function(response) {
                        $('#allocate_study_modal').modal('hide');
                        if (response.success=="Y"){
                            $.notify("Successfully Added.","success");
                        }
                        else{
                            $.notify("Something went wrong.Try again later.","error");
                        }
                        setTimeout(function(){
                            location.reload();
                        },2000);
                    }
                });
            }


        });

        $(document).on('click', '.editBtn', function (e) {
           e.preventDefault();
            user_id = $(this).data('user-id');
            $.ajax({
                type: "POST",
                dataType: "json",

                url: "../ajax/get_study_allocation_info.php",
                data: {
                    user_id:user_id
                },

                success: function(response) {

                    $("#updateTable tbody tr").remove();
                    $("#userName").text(response.username);
                    var studyInfo =response.allocation_info;
                    var roleInfo =response.role_info;
                    var remainStudy =response.remain_study;
                    var tr="";
                    $.each( studyInfo, function( key, value ) {
                        tr=tr+"<tr>";
                        tr=tr+"<td><input type='hidden' name='preStudy' value='"+value['study_id']+"'>"+value['study_name']+"</td>";
                        tr=tr+"<td><div> <select class=\"form-control preRole\">";
                        $.each( roleInfo, function( rKey, rValue ) {
                            if(rValue['role_id']==value['user_type'])
                                tr=tr+"<option value='"+rValue['role_id']+"' selected>"+rValue['role_name']+"</option>";
                            else
                                tr=tr+"<option value='"+rValue['role_id']+"'>"+rValue['role_name']+"</option>";

                        });
                        tr=tr+"</select></div></td>";
                        tr=tr+"<td><div> <select class=\"form-control preStatus\">";
                        if(value['status']=="ON")
                            tr=tr+"<option value='ON' selected>ON</option>";
                        else
                            tr=tr+"<option value='ON'>ON</option>";
                        if(value['status']=="OFF")
                            tr=tr+"<option value='OFF' selected>OFF</option>";
                        else
                            tr=tr+"<option value='OFF'>OFF</option>";

                        tr=tr+"</select></div></td>";
                        tr=tr+"<input type='hidden' name='preAllocationID' value='"+value['allocation_id']+"'>";
                        tr=tr+"</tr>";

                    });

                    $('#updateTable').append(tr);
                    $('#studynameUpdate').find('option:not(:first)').remove();
                    $.each(remainStudy, function (i, item) {
                        $('#studynameUpdate').append($('<option>', {
                            value: item['study_id'],
                            text : item['study_name']
                        }));
                    });

                    $('#roleUpdate').find('option:not(:first)').remove();
                    $.each(roleInfo, function (i, item) {
                        $('#roleUpdate').append($('<option>', {
                            value: item['role_id'],
                            text : item['role_name']
                        }));
                    });

                    //console.log(studyInfo);

                }
            });

            $('#update_allocation_modal').modal('show');

        });


    } );
    $(document).on('click', '.delBtn', function (ev) {
        ev.preventDefault();
        var that = $(this);
        that.closest('tr').remove();

    });
    $(document).on('click', '#addStudyBtn', function (ev) {
        ev.preventDefault();
        $('#updateRoleHidden').show();

    });

    $(document).on('click', '#updateAllocation', function (ev) {
        ev.preventDefault();
        var allocationID = [];
        var preRoleID=[];
        var preStatus=[];
        //console.log(user_id);
        var newStudy,newRole;
        newStudy =$('#studynameUpdate').val();
        newRole  =$('#roleUpdate').val();
        console.log(newStudy);
        var i = 0;
        $('input[name=preAllocationID]').each(function () {
            allocationID[i] = $(this).val();
            i++;
        });

        i=0;
        $.each($(".preRole option:selected"), function(){
            preRoleID[i]=$(this).val();
            i++;
        });

        i=0;
        $.each($(".preStatus option:selected"), function(){
            preStatus[i]=$(this).val();
            i++;
        });

        $.ajax({
            type: "POST",
            dataType: "json",

            url: "../ajax/update_allocation.php",
            data: {
                allocationID:allocationID,
                preRoleID:preRoleID,
                preStatus:preStatus,
                user_id :user_id ,
                newStudy :newStudy ,
                newRole :newRole ,
            },

            success: function(response) {
                $('#update_allocation_modal').modal('hide');
                if(response=="OK"){
                    $.notify("Successfully Updated.","success");
                }else{
                    $.notify("Something went wrong.Please try again later!", "error");
                }
                setTimeout(function(){
                    location.reload();
                },2000);
            }
        });


    });

</script>