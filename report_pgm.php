<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];

if(isset($_POST['dc_selector']))
    $dc_name = $_POST['dc_selector'];
else $dc_name = 0;
$_SESSION["dc_selector"] = $dc_name;
$dc_db_val='SP'.$dc_name;

$snapshot = get_snapshot_name($dc_name, $study_name, $conn);

$_SESSION['snapshot'] = $snapshot;
//Check if any common programs exist or not
$study_name=strtolower($study_name);
$table="cplist_$study_name";
$result=$conn->query("SHOW TABLES FROM toc_dbv2");
while($row = mysqli_fetch_row($result)){
    $arr[] = $row[0];
}

if(in_array($table,$arr))
{
    $cp_exist=1;
}else{
    $cp_exist=0;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Manage Programs</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
      <!-- Main row -->
      <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                   <h3 class="box-title">Study Name: <?php echo $_SESSION["study"] ?></h3>
				   <br />
				   <h4 class="box-title">Snapshot Name: <?php echo $snapshot;?></h4>
                    <div class="box-tools pull-right" style="position: initial;">

                        <form action='report_pgm.php' method='post'>
                            <div class="row" style="margin-right: 0px;">

                                <label><h3 class="box-title">Data Currency: </h3> </label>

                                <select id="dc_selector" name="dc_selector">
                                    <?php
                                    while($row = $snap_list->fetch_assoc()) {
                                        $dc_id = $row['id'];
                                        $sp_name = $row['snap_name'];

                                        if($dc_id==$dc_name){
                                            echo "<option value='".$dc_id."' selected='selected'>".$sp_name."</option>";
                                        }else{
                                            echo "<option value=".$dc_id.">".$sp_name."</option>";
                                        }

                                    }
                                    ?>


                                </select>


                                <button type="submit" id="ellBtn" class="btn btn-block btn-default btn-xs"  >View</button>

                            </div>
                        </form>

                    </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-12 col-sm-8">
                      <div class="pad">
                          <div class="row">
                            <div class="col-md-12">
                                <p style="font-size:18px;">Current Status Of Programs</p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                                
                              <?php
								$sql = "SELECT q.pgmloc, p.pgmname, p.username, p.data_currency, p.event_date,  p.comment, p.status
										FROM   pgm_hist_$study_name AS p
											   LEFT JOIN toc_$study_name AS q
													  ON p.pgmname = q.pgmname
										WHERE  p.status = 'Program running'
											   AND q.data_currency = '$dc_db_val'
											   AND p.data_currency = '$dc_db_val'
											   AND q.title != ' '
										GROUP  BY p.pgmname
										ORDER  BY p.pgmname ASC, p.event_date DESC";

                                /* $sql = "SELECT *
									FROM   (SELECT q.pgmname,
												   p.pgmuser,
												   p.event_date,
												   p.username,
												   p.status,
												   p.pgm_date,
												   p.out_date,
												   q.type,
												   q.tlfnum,
												   q.outno,
												   q.title,
												   q.pgmloc,
												   q.outname,
												   q.outloc
											FROM   (SELECT abc.pgmname,
														   abc.pgmuser,
														   abc.event_date,
														   abc.username,
														   abc.status,
														   abc.pgm_date,
														   d.out_date
													FROM   (SELECT *
															FROM   (SELECT a.pgmname,
																		   a.pgmuser,
																		   b.event_date,
																		   b.validator AS username,
																		   b.status,
																		   c.pgm_date
																	FROM   (SELECT pgmname,
																				   username AS pgmuser
																			FROM   pgm_hist_$study_name
																			WHERE  event_date IN
																				   (SELECT
																				   Max(event_date) AS event_date
																								  FROM
																				   pgm_hist_$study_name
																								  WHERE
																				   comment LIKE '%Uploaded%'
																					OR comment LIKE
																				   '%Generated by System%'
																								  GROUP  BY pgmname)
																			ORDER  BY event_date DESC) AS a,
																		   (SELECT pgmname,
																				   username AS validator,
																				   status,
																				   event_date
																			FROM   pgm_hist_$study_name
																			WHERE  event_date IN (SELECT
																				   Max(event_date) AS event_date
																								  FROM
																				   pgm_hist_$study_name
																								  WHERE
																				   comment NOT LIKE '%Run%'
																								  GROUP  BY pgmname)
																			ORDER  BY event_date DESC) AS b,
																		   (SELECT pgmname,
																				   Max(event_date) AS pgm_date
																			FROM   pgm_hist_$study_name
																			WHERE  comment LIKE '%Edited%'
																					OR comment LIKE '%Uploaded%'
																					OR comment LIKE
																					   '%Generated by System%'
																			GROUP  BY pgmname
																			ORDER  BY pgm_date DESC) AS c
																	WHERE  a.pgmname = b.pgmname
																		   AND b.pgmname = c.pgmname
																	ORDER  BY pgmname,
																			  event_date DESC) AS b_abc
															GROUP  BY pgmname) AS abc
														   LEFT JOIN (SELECT pgmname,
																			 Max(event_date) AS out_date
																	  FROM   pgm_hist_$study_name
																	  WHERE  comment LIKE '%Run%'
																	  GROUP  BY pgmname) AS d
																  ON abc.pgmname = d.pgmname) AS p
												   RIGHT JOIN (SELECT pgmname,
																	  type,
																	  tlfnum,
																	  outno,
																	  title,
																	  pgmloc,
																	  outname,
																	  outloc,
																	  sortorder
															   FROM   toc_$study_name
															   WHERE  data_currency = '$dc_db_val'
																	  AND title != ' ') AS q
														   ON p.pgmname = q.pgmname
											ORDER  BY q.sortorder ASC) AS x
									GROUP  BY pgmname"; */
								
                                $result = $conn->query($sql);
                                

                                // if($result){
                                if ($result->num_rows > 0) {
                                  $distinct_pgm = array();
                                  $distinct_out = array();
                                  $distinct_status = array();
                                  $distinct_status_date = array();
                                  $pgm_run_status = array();
								  $current_status = '';
								  $validator_name = '';
                                  echo "<table id='myTable3' class='table table-hover'>";
                                  echo "<thead>";
                                  echo "<tr bgcolor='#e8e8e8'>";
								  echo "<th>Program <br/> Name</th>";
                                  echo "<th>Program <br/> Location</th>";
                                  
                                  /* echo "<th>Snapshot</th>"; */
                                  echo "<th>Programmer <br/> Name</th>";								  
                                  echo "<th>Snapshot <br/> Name</th>";								  
                                  echo "<th>Date/Time of Last <br> Program Update</th>";
                                    echo "<th>Date/Time of Last <br> Program Submit</th>";
                                    echo "<th>Date/Time of Last <br> Status Update </th>";
								  /* echo "<th>Program<br />Runtime"; */
                                  echo "<th>Validator <br> Name</th>";
                                  echo "<th>Current <br /> Status</th>";
                                  echo "<th>Final Status<br/> at Program Run</th>";                    
                                  /* echo "<th>Date/Time of Last <br>Status Update</th>"; */
                                  echo "</tr>";
                                  echo "</thead>";
                                  echo "<tbody>";

                                  while($row = $result->fetch_assoc()) {

                                 /*  $date = $row["event_date"];  

                                  if ($date != "") {
                                    $my_date = date('d M Y H:i:s', strtotime($date));
                                  }
                                  else{
                                    $my_date = $date;
                                  } */
								  
                                  /* $pdate = $row["pgm_date"];
                                  if ($pdate != "") {
                                  $p_date = date('d M Y H:i:s', strtotime($pdate));
                                  }
                                  else{
                                  $p_date = $pdate;
                                  }

                                  $out_date = $row["out_date"];
                                  if ($out_date != "") {
                                  $out_date = date('d M Y H:i:s', strtotime($out_date));
                                  }
                                  else{
                                  $out_date = $out_date;
                                  } */
									$pgm_date = '';   
									  // echo "<tr bgcolor='#FF0000'>";
									  echo "<tr>";
									  $pgmname = $row["pgmname"];
                                      if (!in_array($pgmname, $pgm_run_status)) {
                                          $sql_get_max_date_run = "SELECT comment
															FROM   pgm_hist_$study_name
															WHERE  pgmname = '$pgmname'
																   AND data_currency = '$dc_db_val'
																   AND  status = 'Program running' ORDER BY event_date DESC LIMIT 1";
                                          $result2=$conn->query($sql_get_max_date_run);
                                          while($row2 = $result2->fetch_assoc()) {
                                              $pgm_run_stat = $row2['comment'];
                                          }
                                          array_push($pgm_run_status,$pgmname);
                                          $pgm_run_status[$pgmname] = $pgm_run_stat;
                                      }

                                      if (!in_array($pgmname, $distinct_pgm)) {
                                          $sql_get_max_date = "SELECT Max(event_date) AS pgm_date
															FROM   pgm_hist_$study_name
															WHERE  pgmname = '$pgmname'
																   AND data_currency = 'SP0'
																   AND ( comment LIKE '%Edited%'
																		  OR comment LIKE '%Uploaded%'
																		  OR comment LIKE '%Generated by System%' ) LIMIT 1";
                                          $result2=$conn->query($sql_get_max_date);
                                          while($row2 = $result2->fetch_assoc()) {
                                              $pgm_date = $row2['pgm_date'];
                                          }
                                          array_push($distinct_pgm,$pgmname);
                                          $distinct_pgm[$pgmname] = $pgm_date;
                                      }
									  if (!in_array($pgmname, $distinct_out)) {
											$sql2_get_max_date = "SELECT 
																Max(event_date) AS out_date
																FROM   pgm_hist_$study_name
																WHERE pgmname = '$pgmname'
																AND data_currency = '$dc_db_val'
																AND  comment LIKE '%Run%' LIMIT 1"; 
										   $result3=$conn->query($sql2_get_max_date);								  
										   while($row3 = $result3->fetch_assoc()) {
											 $out_date = $row3['out_date'];  
										   }
										   array_push($distinct_out,$pgmname);
										   $distinct_out[$pgmname] = $out_date;
									  }

                                      if (!in_array($pgmname, $distinct_status_date)) {
                                          $sql2_get_max_date = "SELECT
																MAX(event_date) AS event_date
                                                                FROM
																pgm_hist_$study_name
																WHERE
																pgmname = '$pgmname' 
																AND data_currency = 'SP0'
																AND (status LIKE '%In Development%'
																OR status LIKE '%Validated%'
																OR status LIKE '%To Be validated%')";

                                          $result3 = $conn->query($sql2_get_max_date);
                                          while($row3 = $result3->fetch_assoc()) {
                                              $event_date = $row3['event_date'];
                                          }
                                          array_push($distinct_status_date,$pgmname);
                                          $distinct_status_date[$pgmname] = $event_date;
                                      }

									  if (!in_array($pgmname, $distinct_status)) {
											$sql3_get_max_date = "SELECT pgmname,
																			   username AS validator,
																			   status,
																			   event_date
																		FROM   pgm_hist_$study_name
																		WHERE  pgmname = '$pgmname'
																			   AND event_date IN (SELECT Max(event_date) AS event_date
																								  FROM   pgm_hist_$study_name
																								  WHERE  pgmname = '$pgmname'
																										 AND data_currency = 'SP0'
																										 AND comment NOT LIKE '%Run%')
																		ORDER  BY event_date DESC"; 
																  
										   $result4=$conn->query($sql3_get_max_date);								  
										   while($row4 = $result4->fetch_assoc()) {
											 $current_status = $row4['status'];
											 if($current_status == 'Validated')
											 {
												$validator_name = $row4['validator'];
											 }else{
												 $validator_name = '';
											 }												 
										   }
										   array_push($distinct_status,$pgmname);
										   $distinct_status[$pgmname] = $current_status;
									  }
									  
									  
									  echo "<td>" . $pgmname.  "</td>";
                                      echo "<td>" . substr_replace($row["pgmloc"],"",strripos($row["pgmloc"],"/")+1).  "</td>";
                                     
									 /*  $snapshot = '';
									  if ($row["data_currency"] == "SP0") {
										$snapshot = 'Current';
									  }
									  if ($row["data_currency"] == "SP1") {
										$snapshot = 'Snapshot 1';
									  }
									  if ($row["data_currency"] == "SP2") {
										$snapshot = 'Snapshot 2';
									  }
									  
									  echo "<td>" . $snapshot.  "</td>"; */
                                      echo "<td>" . $row["username"].  "</td>";
									  $snaps = str_split($row["data_currency"], 2);
									  $snapshot = get_snapshot_name($snaps[1], $study_name, $conn);
									 
                                      echo "<td>" . $snapshot.  "</td>"; 
									  
									  $pdate = $distinct_pgm[$pgmname];
									  if ($pdate != "") {

									  $p_date = date('d M Y H:i:s', strtotime($pdate));

									  }
									  else{

									  $p_date = $pdate;
									  }

									  $out_date = $distinct_out[$pgmname];
									  if ($out_date != "") {
									  $out_date = date('d M Y H:i:s', strtotime($out_date));
									  }
									  else{
									  $out_date = $out_date;
									  }

                                      $date = $distinct_status_date[$pgmname];
                                      if ($date != "") {
                                          $my_date = date('d M Y H:i:s', strtotime($date));
                                      }
                                      else{
                                          $my_date = $date;
                                      }

                                      echo "<td>" . $p_date.  "</td>";
                                      echo "<td>" . $out_date.  "</td>";
                                      echo "<td>" . $my_date.  "</td>";
                                     
                                     /*  echo "<td>" . date('d M Y H:i:s', strtotime($row["event_date"])).  "</td>"; */
                                      
									  echo "<td>" . $validator_name.  "</td>";  
									  
			  
									  
                                      echo "<td>" . $distinct_status[$pgmname].  "</td>";
                                     /*  echo "<td>" . $p_date.  "</td>"; */
                                      /* echo "<td>" . $out_date.  "</td>"; */
									  
                                      //echo "<td>" . $row["validator"].  "</td>";
//                                      echo "<td>" . $pgm_run_status[$pgmname].  "</td>";
                                      if ($pgm_run_status[$pgmname]=='Run Failed'){
                                          echo "<td style='background-color: #ed9a9a;'>" . $pgm_run_status[$pgmname].  "</td>";
                                      }else{
                                          echo "<td style='background-color: #9fed9a;'>" . $pgm_run_status[$pgmname].  "</td>";
                                      }
                                      /* echo "<td>" . $my_date.  "</td>"; */
                                      echo "</tr>";
                                  }
                                  echo "</tbody>";
                                  echo "</table>";        
                                } else {
                                      echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                }
                                // }else {
                                  // echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                // }

                                ?>
															
                            </div> 
                          </div>

                          <div class="row">
                            <div class="col-md-9">
                                <p style="font-size:18px;"> Download Report in PDF format</p>
                            </div>
                            <div class="col-md-3">
                                <a href='study_report_pgm.php' target='_blank' class='demo'>
                                <button  class="btn btn-primary btn-block btn-flat" >Download</button></a>
                            </div> 
                          </div>
                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->          
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php
include("include/footer.php");
$conn->close();
?>
      