<?php
include("include/connect.php");
include("lib/gen_func.php");
require 'vendor/autoload.php';
session_start();

if($_SERVER["REQUEST_METHOD"] == "POST"){
    $email = addslashes($_POST['email']);

    $query="SELECT * FROM user_info WHERE email = '".$email."'";
    $result   = $conn->query($query);

    if($result->num_rows==1){
        while($row = $result->fetch_assoc()) {
            $to = $row['email'];
        }
        function random_password( $length = 8 ) {
            $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&?";
            $passwd = substr( str_shuffle( $chars ), 0, $length );
            return $passwd;
        }
        $passwd1 = random_password(8);
        $passwd = md5($passwd1);
        //echo "passwd:"$passwd1;
        //echo "  passwd MD5:"$passwd;
        $sql_update="UPDATE user_info SET password = '$passwd' , firstlog = 'Yes' where email = '$to'";
        $result_update=$conn->query($sql_update);

        $body = "Hi,\n\n Your new password is  $passwd1";

        $res = smtpmailer($to, 'admin_medoms@shaficonsultancy.com', 'Med-OMS', 'Med-OMS Password Recovered', $body);
        if ($res == true)
        {
            echo "<div class='alert alert-success' role='alert'>Password has been sent to your email. Please check your email.</div>";
        }
        else
        {
            echo "<div class='alert alert-error' role='alert'>Email is not send.</div>";
        }
    }
    else {
        if ($_POST ['email'] != "")
        {
            echo '<div class="alert alert-danger">
						<strong>SORRY!</strong> Your Email Has Not Found in Our Database.
					</div>';
        }
    }
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>MeD-OMS</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
    <link rel="shortcut icon" href="dist/img/scbd.ico">
    <style>
        .scbd {
            height:65px;
            background:#ffffff url('dist/img/logo.png') no-repeat;
            text-align: center;
            position: relative;
            background-position:center;
            z-index:1000;
            margin-top: 50px;
        }
    </style>
</head>
<body class="login-page" style="background: #ffffff none repeat scroll 0% 0%;">
<div class="scbd"></div>
<div class="login-box" >
    <div class="login-logo">
        <a href="index.php"><b>MeD-OMS</b> </a>
    </div><!-- /.login-logo -->
    <div class="login-box-body" style="background: #D2D6DE none repeat scroll 0% 0%;">
        <p class="login-box-msg">Enter email address to reset your password</p>
        <form action="" method="post">
            <input type="hidden" name="forgot_form" value="true">
            <div class="form-group has-feedback">
                <input required type="email" name="email" id="email" class="form-control" placeholder="Email"/>
                <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <a href="index.php" class="btn btn-primary btn-block btn-flat">Go Back</a>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
                </div><!-- /.col -->
            </div>
        </form>
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<!-- jQuery 2.1.3 -->
<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js" type="text/javascript"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });
</script>
</body>
</html>
