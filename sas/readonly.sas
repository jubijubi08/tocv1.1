%macro readonly;

  %local pgmname;
  
   data _null_;
      set sashelp.vextfl;
      if (substr(fileref,1,3)='_LN' or substr (fileref,1,3)='#LN' or substr(fileref,1,3)='SYS') and index(upcase(xpath),'.SAS')>0 then do;
         call symput("pgmname",trim(xpath));
         stop;
      end;
   run;

   %PUT File: &pgmname.;

	DATA null ;
       x "attrib -r %BQUOTE("&pgmname.")";
        dm wedit 'winclose' continue;
   RUN ;

       dm 'wedit "&pgmname."';
%mend readonly;

%readonly;
