%MACRO upload;

	%local pgmname fnames2;

	proc sql noprint;
		select xpath, input(compress(fileref,,'kd'),?best.) as inx, max(calculated inx) as minx  
		into: fnames2 SEPARATED by ' # ', :f2 , :f3
		from sashelp.vextfl
		where directory = 'no'
		and xpath ne 'C:\xampp\htdocs\toc\sas\upload.sas'
		having inx eq minx;
	quit; 
	;

   %PUT Latest File: &fnames2.;

	filename in "C:\xampp\htdocs\toc\sas\in";
	filename out "C:\xampp\htdocs\toc\sas\out.text";

	DATA _NULL_;
	   file in;
	   put "filedic=&fnames2.";
	   var="&fnames2.";
	   
	PROC HTTP in=in out=out url="http://localhost/toc/upload.php"
	     method="post"
	      ct="application/x-www-form-urlencoded";
	RUN;
/*filename myxls "c:\data\data.xls"; */
/*filename results "F:\draft code\out.text"; */
/**/
/*proc http */
/* url="http://mysite.company.com/add-excel.php"   */
/* in=myxls*/
/* out=results*/
/* ct="application/vnd.ms-excel"*/
/* method='POST'*/
/* ;*/
/*run;*/

	data _null_;
	  /* call execute('dm "FLSVLAST"');*/
	  call execute('dm editor "winclose" editor;');
      call execute('dm "wedit '||"'"||"%superq(pgmname)"||"'"||'" ');
    run;

%MEND upload;

%upload;
