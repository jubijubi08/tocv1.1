/*======================================================================
* SHAFI CONSULTANCY LIMITED
*
* STUDY                : ToC auto-generate programs        CREATION DATE: 18SEP2016
*                                                          
* PROGRAM              : dynamic.sas                           
*                                                          
* CURRENT VERSION      : 1.5                                            
*                                                          
* PROGRAMMER           : Marco Ackermann
*                                                           
* DESCRIPTION          : Creates output programs according to ToC Spreadsheet:
*                               
* USER REQUIREMENTS    : 
*                                                          
* SAS VERSION                                              
*   in which validated : SASV9.4                              
*                                                          
* COMMENTS             : 
*                                                          
*                                                          
*-----------------------------------------------------------------------
*                                                          
* REVISION HISTORY (COPY FOR EACH REVISION):               
*                                                            
* DATE                 : 
* PROGRAMMER           : 
* REASON FOR CHANGE    : 
*                                                          
*=======================================================================*/

OPTIONS SYMBOLGEN MPRINT MPRINTNEST MLOGIC MLOGICNEST spool;
*************************************************************************;
***                                                                   ***;
***  Macro to check if Directory exists                               ***;
***                                                                   ***;
*************************************************************************;



%MACRO chk_dir(dir=) ; 
  OPTIONS NOXWAIT
  %LOCAL rc fileref ; 
  %LET rc = %SYSFUNC(FILENAME(fileref,&dir)) ; 
  %IF %SYSFUNC(FEXIST(&fileref))  %THEN 
    %PUT NOTE: The directory "&dir" exists ; 
  %ELSE %DO ; 
    %SYSEXEC md   &dir ; 
    %PUT %SYSFUNC(sysmsg()) The directory has been created. ; 
  %END ; 
  %LET rc=%SYSFUNC(filename(fileref)) ; 
%MEND chk_dir ; 

*************************************************************************;
***                                                                   ***;
***  convert to character format                                    ***;
***                                                                   ***;
*************************************************************************;
 %macro
 ConvertType(DsetIn =,  
DsetOut=&DsetIn,       
ConvertVars=,       
Original_Char_VLength=Keep);       
  OPTIONS MISSING = 
""; 
  DATA _null_; 
%* Count number of variables in the ConvertVars parameter ;
CALL SYMPUTX(
"novars"
, count(
"&ConvertVars"
, "") + 
1
, "G"); 
  RUN; 
  %PUT Number of variables count is &novars; 
  %GLOBAL
 vars newvar; 
  %LET vars = 1; 
  %* Find out datatypes and labels ;
  %DO %WHILE
(&vars lt (&novars + 
1
)); 
%LET newvar = 
%SCAN
(&ConvertVars, &vars); 
%PUT Processing variable is &newvar; 
%LET dsid=
%SYSFUNC
(open(&DsetIn,i)); 
%GLOBAL
 &newvar.v &newvar.l;     
%DO i = 
1
 %TO %SYSFUNC
(ATTRN(&dsid,nvars)); 
%IF %UPCASE
(&newvar) = 
%UPCASE
(%SYSFUNC
(VARNAME(&dsid,&i))) 
 %THEN
 %DO; 
%LET &newvar.v = 
%SYSFUNC
(VARTYPE(&dsid,&i)); 
 %LET &newvar.l = 
%SYSFUNC
(VARLABEL(&dsid,&i)); 
%GOTO
 _over_; 
%END; 
%END; 
%_over_: 
%LET rc=%SYSFUNC
(CLOSE(&dsid)); 
%PUT NOTE: &newvar datatype is &&&newvar.v and label is &&&newvar.l; 
%LET vars = 
%EVAL
(&vars +1); 
%PUT Updated vars loop count &vars; 
  %END; 
  %* Derive variables for further purpose ;
  DATA &DsetOut; 
SET &DsetIn; 
%LET vars = 1; 
%DO %WHILE
(&vars lt (&novars + 
1
)); 
%LET newvar = 
%SCAN
(&ConvertVars, &vars); 
%* If Numeric then derive a character variable ;
%* If Character then keep data as is ;
%IF "&&&newvar.v"
 eq "N" %THEN
 %DO; 
IF &newvar ne 
.
      THEN &newvar._d = STRIP(PUTN(&newvar, VFORMAT(&newvar))); 
%END; 
%ELSE
 %IF "&&&newvar.v"
 eq "C" %THEN
 %DO; 
%IF %UPCASE
(%SUBSTR
(&Original_Char_VLength, 
1
, 
1
)) = K 
%THEN
 %DO; 
          IF &newvar ne 
"" THEN  
&newvar._d = STRIP(PUTC(&newvar, VFORMAT(&newvar))); 
%END; 

%ELSE
 %DO; 
LENGTH &newvar._d $
200
; 
IF &newvar ne 
"" THEN DO; 
&newvar._d       
=       STRIP(PUTC(&newvar,       
VFORMAT(&newvar)));       
       IF &newvar._d ne &newvar tHEN DO; 
PUT "WARNING: Derived variable is not matching with raw, suggest to keep original 
attributes"
; 
PUT "Raw variable "
 &newvar=; 
PUT "Derived variable "
 &newvar._d=; 
 END; 
END; 
%END;      
%END;  
%* Assign label only if available in raw ;
%IF "&&&newvar.l"
 ne "" %THEN
 %DO; 
LABEL &newvar._d = 
"&&&newvar.l"
;  
%END; 
%LET vars = 
%EVAL
(&vars +1); 
%END; 
  RUN; 
%mend ConvertType; 



*************************************************************************;
***                                                                   ***;
***  rename variable list                                             ***;
***                                                                   ***;
*************************************************************************;


  %macro RenameList(vars= ,vars2=);

	  RENAME 
	   %do i = 1 %to %sysfunc(countw(&vars));
	      %let var=%scan(&vars,&i);
	      %let var2=%scan(&vars2,&i);
	      &var = &var2
	   %end;
	   ;
   %mend;

*************************************************************************;
***                                                                   ***;
***  Macro to create the header of outfile                            ***;
***                                                                   ***;
*************************************************************************;

%MACRO head;
  IF _n_=1 THEN DO;
    PUT ' ';
    PUT '/*======================================================================';
    PUT '* SHAFI CONSULTANCY LIMITED';
    PUT '*';
    PUT '* STUDY                : '"&_study"'        CREATION DATE: '"&_datenow";
    PUT '*';
    PUT '* PROGRAM              : ' pgmname;
    PUT '*';
    PUT '* PROGRAMMER           : '"&_author";
    PUT '*';
    PUT '* DESCRIPTION          : Create following outputs:';
    %DO k = 1 %TO &allnrN;
      PUT '*                        - '"%SCAN(&allno,&k,%str(#)) " "&&alltit&k";      
    %END;

    PUT '*';
    PUT '* USER REQUIREMENTS    : SAP';
    PUT '*';
    PUT '* SAS VERSION          : SASV9.4';
    PUT '*';
    PUT '* COMMENTS             : ';
    PUT '*';
    PUT '*-----------------------------------------------------------------------';
    PUT '*';
    PUT '* REVISION HISTORY';
    PUT '*';
    PUT '* DATE                 :';
    PUT '* PROGRAMMER           :';
    PUT '* REASON FOR CHANGE    :';
    PUT '*';
    PUT '*=======================================================================*/';
    PUT ' ';
	PUT "%INCLUDE %sysfunc(tranwrd(&stdinidir,/,\));";
	PUT "FILENAME L %sysfunc(tranwrd(&logdir,/,\));";
	PUT " ";
/*	PUT "PROC PRINTTO LOG=L;";*/
/*	PUT "RUN;";*/
  END;
%MEND head;


*************************************************************************;
***                                                                   ***;
***  Macro to create the header of section in the outfile             ***;
***                                                                   ***;
*************************************************************************;

%MACRO mhead;
  RETAIN count 1;
  IF _n_=1 THEN count=1;
  ELSE count=count + 1 ;
  PUT ' ';
  PUT ' ';
  PUT ' ******************************************************************;';
  PUT ' ***';
  PUT ' *** Output No. ' outno; *count;
  PUT ' *** Title: ' tlfnum Title;
  PUT ' ***';
  PUT ' ******************************************************************;';
  PUT ' ';
%MEND mhead;


*************************************************************************;
***                                                                   ***;
***  Macro to create the footer of the Section in the outfile         ***;
***                                                                   ***;
*************************************************************************;

%MACRO foot;
  PUT "PROC PRINTTO LOG=LOG;";
  PUT "RUN;";
  PUT ' ';
  PUT ' ';
  PUT '******************************************************************;';
  PUT '***                                                            ***;';
  PUT '***                  End of Program                            ***;';
  PUT '***                                                            ***;';
  PUT '******************************************************************;';
%MEND foot;


*************************************************************************;
***                                                                   ***;
***  Macro to create the outfile itself                               ***;
***                                                                   ***;
***  global variables for the outfile, can be altered by macro call   ***;
***                                                                   ***;
*************************************************************************;

%MACRO dynamic (
                _author      = Med-OMS  /* Programmers Name */
                ,_timenow    = %SYSFUNC(time(), time.)
                ,_datenow    = %SYSFUNC(date(), date9.)
                ,_customer   = Internal
                ,_study      = Testproject
                ,_desc       = Macro for xxx
                ,_inxlspath  = 
                ,_outpgmpath = c:\Shaficonsultancy\&_customer\&_study\
                ,_overwrite  = N
                ); 


  *************************************************************************;
  ***                                                                   ***;
  ***  INPUT: Import ToC Spreadsheet into Program                       ***;
  ***                                                                   ***;
  *************************************************************************;

  PROC IMPORT DATAFILE="&_inxlspath"
     OUT=inToC
     DBMS=xlsx replace ;
  RUN;

  *************************************************************************;
  ***                                                                   ***;
  *** PROCESS Section starts here                                       ***;
  ***                                                                   ***;
  *** a. add line numbers to dataset inTOC                              ***;
  *** b. get parameter names from spreadsheet                           ***;
  *** c. get parameters values from spreadsheet                         ***;
  *** d. combine parameter values into makro values                     ***;
  *** e. Transpose DATASET IN by PARAMETER NAME                         ***;
  *** f. Transpose DATASET IN by PARAMETER VALUE                        ***;
  *** g. join parameter names and values dataset into one dataset       ***;
  *** h. only write parameter with no missing values into in3 dataset   ***;
  *** i. delete all observation with empy first parameter               ***;
  *** j. remove trailing white spaces from parameters in dataset in4    ***;
  *************************************************************************;

 *a*; 
  DATA in;
    SET inToC;
    ID=_N_;
  RUN;

*b*;  
  PROC CONTENTS DATA=in OUT=pars (WHERE=(prxmatch('/^PARAMETER\_\d+\_NAME/',UPCASE(NAME)))) NOPRINT;
  RUN;

*c*;
  PROC CONTENTS DATA=in OUT=vals (WHERE=(prxmatch('/^PARAMETER\_\d+\_VALUE/',UPCASE(NAME)))) NOPRINT;
  RUN;

*d*;
  PROC SQL NOPRINT;
    SELECT name INTO :pars SEPARATED BY '  '  FROM pars ;
    SELECT name INTO :vals SEPARATED BY '  '  FROM vals ;
  QUIT;

  %put &pars;
  %put &vals;

  %LET vals2con=;
  %LET vals2con_d=;
  *e*;
  PROC SQL NOPRINT;
    SELECT name INTO :vals2con SEPARATED BY '  '  FROM vals WHERE type = 1;
  QUIT;
  *e*;
  PROC SQL NOPRINT;
    SELECT DISTINCT STRIP(name)||STRIP('_d') INTO :vals2con_d SEPARATED BY '  '  FROM vals WHERE type = 1;
  QUIT;

  %put &vals2con ;
  %put &vals2con_d ;

  %IF %LENGTH(&vals2con) NE 0 %THEN %DO;
	  %ConvertType(DsetIn  =  in,      
	             DsetOut = in1,  
		         ConvertVars =  &vals2con,  
	             Original_Char_VLength = drop); 

	DATA in;
		SET in1(DROP= &vals2con);
	%RenameList(vars=&vals2con_d ,vars2=&vals2con);;	
	RUN;
%END;

*e*;
   DATA  in3a;
    ARRAY ar(*) $255 &pars;
    ATTRIB varname LABEL="" LENGTH=$25 FORMAT=$25. ;
    ATTRIB  value LABEL="" LENGTH=$1024 format=$1024.  ;
    SET  in;
      DO _i=1 TO DIM(ar);
        varname=VNAME(ar[_i]);
        value=ar[_i];
        OUTPUT;
      END;
     DROP _i &pars ;
  RUN;

*f*;
  DATA  in3b;
    ARRAY ar(*) $255 &vals;
    ATTRIB varname LABEL="" LENGTH=$25 FORMAT=$25. ;
    ATTRIB  value LABEL="" LENGTH=$1024 format=$1024.  ;
    SET  in;
      DO _i=1 TO DIM(ar);
        varname=VNAME(ar[_i]);
        value=ar[_i];
        OUTPUT;
      END;
     DROP _i &vals ;
  RUN;

*g*;
  PROC SQL;
    CREATE TABLE in3c AS
    SELECT a.*, b.value AS value2,length(a.value) as LengthOfValue, max(CALCULATED LengthOfValue) as MaxLength            
    FROM  in3a AS a, in3b (KEEP=varname value ID) AS b
    WHERE a.ID=b.ID AND SCAN(a.varname, 2,'_')= SCAN(b.varname, 2,'_')
		GROUP BY a.pgmname;
  QUIT;

*h*;
  DATA in3;
    SET in3c;
		lenaftername=MaxLength-LengthOfValue;
		IF (lenaftername LT 0) THEN lenaftername=0;
    IF NOT MISSING ( value ) AND NOT MISSING (value2) THEN value=STRIP(value)||repeat(' ',lenaftername)||'= '||STRIP(value2);
	RUN;

 *i*;
  DATA in4;
    SET in3;
    IF varname NE "PAR1" AND STRIP(value) ="" THEN DELETE;
  RUN;
   
  PROC SORT DATA=in4;
    BY pgmname outno;
  RUN;

*j*;
  DATA _NULL_;
    SET in4;
    CALL SYMPUT("fname"||STRIP(PUT(_N_,8.)), STRIP(pgmname) );
    CALL SYMPUT("fnamen",STRIP(PUT(_N_,8.)));

  RUN;

 %PUT &fname1;
 %PUT &fnamen;

  DATA _null_;
    LENGTH LN $255;
    LN="'"||STRIP("!baseloc\")||"pgm\studyini.sas"||"'";
    CALL SYMPUT('stdinidir', STRIP(LN));
  RUN;

	%PUT &stdinidir;

    *************************************************************************;
    ***                                                                   ***;
    ***  OUTPUT Section starts here                                       ***;
    ***                                                                   ***;
    *** a. loop through each file an get unique numbering and titles      ***;
    *** b.  strip pgmname from pgmloc                                     ***;
    *** c.  check if program is validated, then donnot overwrite unless macro variable is set to _overwrite=Y ***;
    *** d.  strip pgmname from pgmloc                                     ***;
    *************************************************************************;

 *a*;
/*  %LET i=1;*/
    %DO i=1 %TO  &fnamen; 
      PROC SQL NOPRINT;
        SELECT COUNT(DISTINCT outno) INTO :allnrN TRIMMED              
        FROM in4
        WHERE pgmname="&&fname&i";

        SELECT DISTINCT outno INTO :allno SEPARATED BY '#'               
        FROM in4
        WHERE pgmname="&&fname&i";

        SELECT DISTINCT pgmloc INTO :allpgmloc SEPARATED BY '#'               
        FROM in4
        WHERE pgmname="&&fname&i" and pgmloc ne '';

        SELECT DISTINCT logname INTO :logname               
        FROM in4
        WHERE pgmname="&&fname&i";

        SELECT DISTINCT outloc INTO :outname               
        FROM in4
        WHERE pgmname="&&fname&i";
     QUIT;

   %PUT &allnrN;
	 %PUT &allno;
	 %PUT &allpgmloc;
	 %PUT &logname;
	 %PUT &outname;

     PROC SQL NOPRINT;
        SELECT DISTINCT Title INTO :alltit1 - :alltit&fnameN                
        FROM in4
        WHERE pgmname="&&fname&i";
      QUIT; 

	%PUT &alltit1;


 *b*;

      DATA _null_; 
      *** call SYMPUT ('pgmloc2', REVERSE((REVERSE(SYMGET('allpgmloc')))));***;
        a = SYMGET ('allpgmloc');
        b = REVERSE (a);
        c = INDEX (b,'/');
        d = SUBSTR(b,c,LENGTH(b)-c+1);
        e = REVERSE (d);
        f = TRANSLATE(e,'\','/');
        CALL SYMPUT('pgmloc2', STRIP(f));
      RUN;

	  %PUT &pgmloc2;
        
      %chk_dir(dir=&_outpgmpath.&pgmloc2); 
      
 *c*;
	  %LET _outpgmpath2 = !baseloc\;

      %LET canwrite=N;

       DATA _NULL_;
         SET IN4 ;
         IF  pgmname="&&fname&i" THEN CALL SYMPUT("canwrite","Y") ;
         WHERE pgmname="&&fname&i";
         %IF %UPCASE(&_overwrite) EQ N %then %do; 
           WHERE ALSO UPCASE(STATUS) NE 'V';
         %END;
       RUN;

      DATA _null_;
        LENGTH LN $255;
        LN="'"||STRIP("&_outpgmpath2.&pgmloc2.&logname")||"'";
        CALL SYMPUT('logdir', STRIP(LN));
        PUT "";
      RUN;
	  %PUT &logdir;

/*      DATA _null_;*/
/*        LENGTH LN $255;*/
/*        LN="'"||STRIP("&_outpgmpath.")||outloc||"'";*/
/*        CALL SYMPUT('outdir', STRIP(LN));*/
/*      RUN;*/
     

      %IF &canwrite = Y %THEN %DO;
      DATA test;
        SET in4 END=EOF;
        WHERE pgmname="&&fname&i";
        BY  pgmname outno;
		    outloc1 = STRIP(tranwrd(outloc,'/','\')) || STRIP(outname);
/*		outloc = tranwrd(outloc,/,\);*/
 *d*;
		    CALL SYMPUT('pgmname', STRIP(pgmname));
				%LET pgmname1=&&fname&i;
				%PUT &_outpgmpath.&allpgmloc.&pgmname;
        FILE "&_outpgmpath.&allpgmloc.&pgmname1";
        %head;
		
		 
        gap=LENGTH(shell_name)+3;
        IF FIRST.outno THEN DO;
          %mhead;
          
          %PUT &&_outpgmpath;
          PUT "FILENAME O '%sysfunc(tranwrd(&&_outpgmpath2,/,\))"outloc1"';";
	  
          PUT ' ';
          PUT 'PROC PRINTTO PRINT=O NEW;';
          PUT 'RUN;';
          PUT ' ';
          PUT '%' %TRIM(shell_name) '(  ' value;
        END;

				IF NOT FIRST.outno THEN DO;
	        DO __i=1 TO gap;
	          PUT ' ' @@;
	        END;
	        IF value NE '' THEN PUT ", " value;
				END;

        IF LAST.outno THEN DO;
          DO __i=1 TO gap;
            PUT ' ' @@;
          END;
          PUT ');';
          PUT ' ';
          PUT 'PROC PRINTTO PRINT=PRINT;';
          PUT 'RUN;';	  
          PUT ' ';
        END;
        IF eof THEN DO;
          %foot;
        END;
      RUN;
      %end;
	  %PUT AUTOGEN &&fname&i;
    %END;
%MEND dynamic;

/*%dynamic(_author=someone else, */
/*         _customer=Customer,*/
/*		 _study=Study,*/
/*		 _desc=Describtion,*/
/*		 _inxlspath=C:\Users\Marco\Dropbox\SAS\ToC\toc_study_002.xls,*/
/*		 _outpgmpath = ,*/
/*		 _overwrite=N);*/

  ******************************************************************;
  ***                                                            ***;
  ***                  End of Program                            ***;
  ***                                                            ***;
  ******************************************************************;
