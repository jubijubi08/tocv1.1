/*======================================================================
* SHAFI CONSULTANCY LIMITED
*
* STUDY                : 1264.0003                CREATION DATE: 18DEC2012 
*                                                           
* PROGRAM              : studyauto.sas                           
*                                                           
* CURRENT VERSION      : 1                                  
*                                                           
* PROGRAM LOCATION     : !clinrep\1264_0003\pgm               
*                                                           
* PROGRAMMER           : Rafi Rahi                   
*                                                           
* DESCRIPTION          : Set up study specific libnames and formats        
*                                                           
* USER REQUIREMENTS    : TSAP                              
*                                                           
* SAS VERSION                                               
*   in which validated : SASV9.2                               
*                                                           
* COMMENTS             :
*                      
*-----------------------------------------------------------------------
*                                                           
* REVISION HISTORY (COPY FOR EACH REVISION):                
*                                                            
* DATE                 : 19DEC2012 
* PROGRAMMER           : Md.Aminul Islam 
* REASON FOR CHANGE    : CH1: The macro variable inlib should be point to newd
*                        which contains client data.
*-----------------------------------------------------------------------
* REVISION HISTORY (COPY FOR EACH REVISION):                
*                                                            
* DATE                 : 30DEC2012 
* PROGRAMMER           : Md.Aminul Islam 
* REASON FOR CHANGE    : CH2:Two libries del_qc1 and newd_qc1 pointing to
*                            !clinrep\1264_0003\ocview\qc_deleted_qc and 
*                            !clinrep\1264_0003\ocview\qc_newdata_qc respectively 
*                            to store DELETED and NEWDATA by qc extraction pgm.
*
*                      : Added two new formats analf and $popuf
*-----------------------------------------------------------------------
* REVISION HISTORY (COPY FOR EACH REVISION):                
*                                                            
* DATE                 : 17JAN2013 
* PROGRAMMER           : Md. Nurahamid Siddiki 
* REASON FOR CHANGE    : Add new formats
*
*=======================================================================*/

OPTIONS NOFMTERR MSGLEVEL=I NOTES SOURCE SOURCE2 MPRINT NOMLOGIC NOSYMBOLGEN 
        COMPRESS=YES NODATE NONUMBER MISSING='.' PS=54 LS=152 NOCENTER
        VALIDVARNAME=UPCASE; 

OPTIONS SET = clinrep 'D:\study';

  ******************************************************************;
  ******************************************************************;
  ******************************************************************;
  ****************                                  ****************;
  ****************             LIBNAMES             ****************;
  ****************                                  ****************;
  ******************************************************************;
  ******************************************************************;
  ******************************************************************;

LIBNAME ocview     '!clinrep\study_001\ocview' ACCESS=READONLY;
/*LIBNAME del        '!clinrep\1264_0003\ocview\deleted';*/
/*LIBNAME del_qc     '!clinrep\1264_0003\ocview\deleted_qc';*/
/*LIBNAME del_qc1    '!clinrep\1264_0003\ocview\qc\deleted_qc'; *CH2:*;*/
LIBNAME newd       '!clinrep\study_001\ocview\newdata';
/*LIBNAME newd_qc    '!clinrep\1264_0003\ocview\newdata_qc';*/
/*LIBNAME newd_qc1   '!clinrep\1264_0003\ocview\qc\newdata_qc'; *CH2:*;*/
LIBNAME pgm        '!clinrep\study_001\pgm';
LIBNAME ads        '!clinrep\study_001\ads';
LIBNAME lst        '!clinrep\study_001\lst';
/*LIBNAME lst_qc     '!clinrep\1264_0003\lst\qc';*/
LIBNAME tlfds      '!clinrep\study_001\tlfds';  *Keep final output dataset for main*;
/*LIBNAME tlfds_qc   '!clinrep\1264_0003\tlfds_qc'; *Keep final output dataset for qc*;*/
/*LIBNAME jona       '!clinrep\1264_0003\dict';*/


  ******************************************************************;
  ******************************************************************;
  ******************************************************************;
  ****************                                  ****************;
  ****************         MACRO VARIABLES          ****************;
  ****************                                  ****************;
  ******************************************************************;
  ******************************************************************;
  ******************************************************************;

*%LET inlib = ocview;
*CH1:*;
%LET inlib = newd;
%LET DBlockdt = '21MAR2013'D;
*%LET DBlockdt = %SYSFUNC(TODAY());
%LET cutdt = '19AUG2011'D;
%LET cutdtc = '20110819';

%LET qcoutfile=\\SERVER\SC_Projects\TOC\study\1264_0003\lst\ctr_qc;

  ******************************************************************;
  *** To identify the study number as used in the CLINREP folder.***;
  ***                                                            ***;
  *** This will be used as password for Analysis datasets.       ***;
  ******************************************************************;

%LET x_study = %LOWCASE(%SCAN(%SYSFUNC(PATHNAME(pgm)),-2,\));      * e.g. sc00032 for 1200.32 *;

%PUT ==========>>>> CLINREP STUDY NUMBER = &x_study <<<<===========;


  ******************************************************************;
  ******************************************************************;
  ******************************************************************;
  ****************                                  ****************;
  ****************             FORMATS              ****************;
  ****************                                  ****************;
  ******************************************************************;
  ******************************************************************;
  ******************************************************************;

PROC FORMAT CNTLIN=ocview._formats;
RUN; 


PROC FORMAT;

  %***********************************************;
  %***        Treatment related Formats        ***;
  %***********************************************;


  %***********************************************;
  %*** All other formats - alphabetical order  ***;
  %***********************************************;

  VALUE $country
    'A       '='Austria'
    'AND     '='Andorra'
    'AUS     '='Australia'
    'B       '='Belgium'
    'BA      '='Bosnia and Herzegovina'
    'BDS     '='Barbados'
    'BG      '='Bulgaria'
    'BR      '='Brazil'
    'BS      '='Bahamas'
    'BY      '='Belarus'
    'CH      '='Switzerland'
    'CN      '='Canada'
    'CO      '='Colombia'
    'CR      '='Costa Rica'
    'CZ      '='Czech Republic'
    'DE      ' ='Germany'
    'DK      '='Denmark'
    'E       '='Spain'
    'EC      '='Ecuador'
    'EE      '='Estonia'
    'F       '='France'
    'GE      '='Georgia EU'
    'GR      '='Greece'
    'H       '='Hungary'
    'HK      '='Hongkong'
    'I       '='Italy'
    'IL      '='Israel'
    'IND     '='India'
    'IRL     '='Ireland'
    'J       '='Japan'
    'KO      '='Croatia'
    'L       '='Luxembourg'
    'LT      '='Lithuania'
    'LV      '='Latvia'
    'MAL     '='Malaysia'
    'MD      '='Moldova'
    'MEX     '='Mexico'
    'MK      '='Macedonia'
    'N       '='Norway'
    'NL      '='Netherlands'
    'NZ      '='New Zealand'
    'P       '='Portugal'
    'PA      '='Panama'
    'PE      '='Peru'
    'PL      '='Poland'
    'PRC     '='China, Peoples Republic of'
    'RA      '='Argentina'
    'RB      '='Botswana'
    'RC      '='Taiwan'
    'RCH     '='Chile'
    'RF      '='Russia'
    'RO      '='Romania'
    'ROK     '='Korea'
    'RP      '='Philippines'
    'S       '='Sweden'
    'SF      '='Finland'
    'SGP     '='Singapore'
    'SK      '='Slovakia'
    'SLO     '='Slovenia'
    'T       '='Thailand'
    'TN      '='Tunisia'
    'TR      '='Turkey'
    'TT      '='Trinidad and Tobago'
    'UA      '='Ukraine'
    'UAE     '='United Arab. Emirates'
    'UK      '='United Kingdom'
    'USA     '='United States'
    'YU      '='Serbia / Yugoslavia'
    'YV      '='Venezuela'
    'ZA      '='South Africa'
    ;

*CH2:*;
  VALUE     $region
    'B        '='Europe'
    'DE       '='Europe'
    'E        '='Europe'
    'LV       '='Europe'
    'UK       '='Europe'
    'USA      '='North America'
    'EE       '='Europe'
    ;
  VALUE $trtn2clr
    '01' = 'Red\Blue'
    '02' = 'Blue\Blue'
    '03' = 'Green\Green'
    '04' = 'Brown\Brown'
    '05' = 'Black\White'
    '06' = 'White\White'
    '07' = 'Gray\Gray'
    ;

  VALUE $atr2clr
    'DummyRed' = 'Red\Blue'
    'DummyBlue' = 'Blue\Blue'
    'DummyGreen' = 'Green\Green'
    'DummyBrown' = 'Brown\Brown'
    'DummyBlack' = 'Black\White'
    'DummyWhite' = 'White\White'
    'DummyGray' = 'Gray\Gray'
    ;

  VALUE racea1f
    1     ="White"
    2     ="Asian"
    3     ="Black/African American"
    4     ="Other"
    ;

  VALUE agec1v
    1     ="<65"
    2     ="65-74"
    3     =">=75"
    ;

  VALUE wgtc1v
    1     ="<=70 kg"
    2     =">70-80 kg"
    3     =">80-90 kg"
    4     =">90 kg"
    ;
  
  VALUE bmic1v
    1     ="<30 kg/m^2"
    2     =">=30 kg/m^2"
    ;

  VALUE bmic2v
    1     ="<25"
    2     ="25-<30"
    3     =">=30"
    ;

  VALUE admc1v
    1     ="0"
    2     ="1"
    3     =">=2"
    ;

  VALUE hba1c1v
    1     ="<7.0% "
    2     ="7.0% to <8.0%"
    3     ="8.0% to <9.0%"
    4     =">=9.0%"
    ;

  VALUE hba1c2v
    1     ="<8.5%"
    2     =">=8.5%"
    ;

  VALUE hba1c3v
    1     ="<7.5%"
    2     ="7.5 - <8.0%"
    3     ="8.0 - <9.0%"
    4     =">=9.0%"
    ;

  VALUE hba1c4v
    1     ="<8.0%"
    2     =">=8.0%"
    ;
  
  VALUE hba1c5v 
    1     ="< 7.0%"
    0     =">=  7.0%"
    ;

  VALUE hbac3v
    1     ="<9.0%"
    2     =">=9.0%"
    ;

  VALUE hba1c7v
    1     ="<-2.0"
    2     =">=-2.0 to <-1.5"
    3     =">=-1.5 to <-1.0"
    4     =">=-1.0 to <-0.5"
    5     =">=-0.5 to <0.0"
    6     =">=0.0"
    ;

  VALUE gluc1v 
    1     ="<126" 
    2     ="126  to < 140"
    3     ="140 to <200"
    4     =">=200"
    ;
/**/
/*  VALUE eccrc1v*/
/*    1     =">=80" */
/*    2     ="50-<80"*/
/*    3     ="30-<50"*/
/*    4     ="15-<30"*/
/*    5     ="<15"*/
/*    ;*/

  VALUE eccrc1v
    1     =">=80" 
    2     ="50-<80"
    3     ="30-<50"
    4     ="<30"
    ;

/*  VALUE eccrc6v*/
/*    1     =">=80 (none)"*/
/*    2     ="50 to <80 (mild)"*/
/*    3     ="30 to <50 (moderate)"*/
/*    4     ="15 to <30 (severe)"*/
/*    5     ="<15 (endstage)"*/
/*    ;*/


  VALUE eccrc6v
    1     =">=80 (normal renal function)"
    2     ="50 to <80 (mild renal impairment)"
    3     ="30 to <50 (moderate renal impairment)"
    4     ="<30 (severe or endstage renal impairment)"
    ;

  VALUE egfrc1v
    1     =">=90"
    2     ="60-<90"
    3     ="30-<60" 
    4     ="15-<30"
    5     ="<15"
    ;

/*This is needed for table*/
/*  VALUE egfrc1v*/
/*    1     =">=90"*/
/*    2     ="60-<90"*/
/*    3     ="30-<60" */
/*    4     ="<30"*/
/*    ;*/

/*  VALUE egfrc6v*/
/*    1     =">=90 (none)"*/
/*    2     ="60 to <90 (mild)"*/
/*    3     ="30 to <60 (moderate)"*/
/*    4     ="<30 (severe or endstage)"*/
/*    5     ="<30 (severe or endstage)"*/
/*    ;*/

  VALUE egfrc6v
    1     =">=90 (normal renal function)"
    2     ="60 to <90 (mild renal impairment)"
    3     ="30 to <60 (moderate renal impairment)"
    4     ="<30 (severe or endstage renal impairment)"
    ;
 
  VALUE egfrc7v
    1     ="eGFR >=90"
    2     ="eGFR <90"
    ;

  VALUE hepinc1v
    1     ="Yes"
    0     ="No"
    ;
  VALUE analf 
    0 = "E_TPATT"
    1 = "E_TRTEXP"
    3 = "Drug stop + 7 days (rand trt only)"
    31= "Drug stop + 7 days for treatment (PART A  only)"
    32= "Drug stop + 7 days for treatment (PART B only)"
    33= "Drug stop + 7 days (rand trt only)"
    4 = "Drug stop + 7 days (rand trt, total)"
    41= "Drug stop + 7 days for treatment (PART A, total)"
    42= "Drug stop + 7 days for treatment (PART B, total)"
    5 = "Drug stop + 1 day (rand trt only)"
    51= "Drug stop + 1 days for treatment (PART A only)"
    52= "Drug stop + 1 days for treatment (PART B only)"
    6 = "Overall interval (rand trt grps only)"
    7 = "Drug stop + 7 days (Pre-trt, rand trt, post-trt by trt)"
    71= "Drug stop + 7 days (Pre-trt, PART A, post-trt by trt)"
    72= "Drug stop + 7 days (Pre-trt, PART B, post-trt by trt)"
    8 = "Drug stop + 7 days (Pre-trt, titr, stable-dose, post-trt by trt)"
    9 = "Drug stop + 7 days (actual study trt only)"
    91= "Drug stop + 7 days (actual study PART A only)"
    92= "Drug stop + 7 days (actual study PART B only)"
    10= "Drug stop + 7 days (rescue, rand trt only)"
    ;

  VALUE $popuf
   'SCR' = 'Screened set'
   'RS'  = 'Randomised set'
   'TS'  = 'Treated set'
   'TS2' = 'Treated set in part B'
   'FAS' = 'Full analysis set'
   'PPS' = 'Per protocol set'
   'FASC'= 'Full analysis set completers'
   'MTT' = 'Meal tolerance test set'
    ;

  VALUE tptno1vf
    3  = 'Baseline'
    4  = 'Week 6'
    5  = 'Week 12'
    6  = 'Week 18'
    7  = 'Week 24'
    8  = 'Week 30'
    9  = 'Week 36'
    10 = 'Week 48'
    11 = 'Week 60'
    12 = 'Week 72'
    13 = 'Week 84'
    ;

  VALUE anatypc1f
    0 = 'OR'
    1 = 'OC'
    2 = 'OC-ROC'
    3 = 'LOCF'
    4 = 'LOCF-ROC'
    5 = 'NCF'
    ;

  *KM: it should be changed by anatypc values;
  VALUE anatypdc1f
    0 = 'Original results'
    1 = 'Observed case'
    2 = 'Observed case (val. after resc. med. incl.)'
    3 = 'LOCF'
    4 = 'LOCF (val. after resc. med. incl.)'
    5 = 'NCF'
    ;
  
  VALUE $eptnmdc1f
    "HBA1C"  = "HbA1c"
    "HBA1C2" = "HbA2c" /*KM: replaced HBA1C by HBA1C2*/
    "FPG"    = "FPG"
    "PPG1"   = "PPG 1Hr"
    "PPG2"   = "PPG 2Hr"
    "WTSTD"  = "Body weight"
    "WAIST"  = "Waist circumference"
    "RESCUE" = "Rescue medication part A"
    "EGFR"   = "Glomerular filtration rate (MDRD)"
    "ECCR"   = "Creatinine clearance (Cockcroft-Gault)"
    "UACR"   = "UACR"  
    "SYSBP"  = "SYSBP"
    "DIABP"  = "DIABP"
    "PULSE"  = "PULSE"
    "HR"     = "HR"    
    "QRS"    = "QRS"  
    "PR"     = "PR"    
    "QT"     = "QT"    
    "QTCB"   = "QTCB"  
    "QTCF"   = "QTCF"  

    ;

  VALUE epttrfdc1f
    0 = "No transformation"
    1 = "Change from baseline"
    10= "No transformation, categorical"
    11= "Change from baseline, categorical"
    21= "HbA1c Response 1: <7.0%, >=7%"
    22= "HbA1c Response 2: <6.5%, >=6.5%"
    23= "HbA1c Reduction 1: <0.5%, >=0.5%"
    ;
  
  VALUE epttrsdc1f
    0 = "No transformation"
    ;

  VALUE eptfdc1f
    0 = "Observed case"
    1 = "Imputed as indicated with ANATYP"
    2 = "Imputed by linear interpolation of adjacent data"
    ;

  VALUE $epttypdc1f
    "HBA1C" = "HbA1c"
    "BIOM"  = "Biomarker ie: FPG, PPG 1h and PPG 2h ?"
    "VS"    = "Vital signs"
    "RESCUE"= "Rescue medication"
    "LAB"   = "Laboratory parameter"
    "COMP"  = "Composite endpoint"
    ;

  /*KM: Newly added*/
  VALUE $eptudc1f
    "HBA1C"  = '%'
    "HBA1C2" = 'mmol/mol'
    "FPG"    = 'mg/dL'
    "PPG1"   = 'mg/dL'
    "PPG2"   = 'mg/dL'
    "WTSTD"  = 'kg'
    "WAIST"  = 'cm'
    "RESCUE" = '-'
    "EGFR"   = 'mL/min'
    "ECCR"   = 'mL/min'
    "UACR"   = "Ug/mg"
    "SYSBP"  = "mmHg"
    "DIABP"  = "mmHg"
    "PULSE"  = "bpm"
    "HR"     = "bpm"
    "QRS"    = "msec"
/*    "PR"     = "msec"*/
    "PR"     = "bmp"
    "QT"     = "msec"
    "QTCB"   = "msec"
    "QTCF"   = "msec"
    ;

  VALUE $interv1f
    "PART A"="Treatment part A"
    "PART B"="Treatment part B"
    "Post-study"="Post-study"
    "Post-treat"="Post-treatment"
    "Run-in"="Run-in"
    "Screening"="Screening"
    ;

    VALUE $intervdc1f
    "PART A"="Treatment part A"
    "PART B"="Treatment part B"
    "Post-study"="Post-study"
    "Post-treat"="Post-treatment"
    "Run-in"="Run-in"
    "Screening"="Screening"
    ;

    VALUE ept21f
    1="HBA1C<7.0%"
    0="HBA1C>=7.0%"
    .=""
    ;

    VALUE ept22f
    1="HBA1C<6.5%"
    0="HBA1C>=6.5%"
    .=""
    ;

    VALUE ept23f
    1="Change from baseline of HBA1C=<-0.5%"
    0="Change from baseline of HBA1C>-0.5%"
    .=""
    ;

    VALUE tagf
    1= 'Number of patients [N(%)]'
    2= 'Patients with TIA [N(%)]'
    3= 'Patients with non-fatal stroke [N(%)]'
    4= 'Patient with non-fatal MI [N(%)]'
    5= 'Patients with other myocardial ischemia [N(%)]'
    6= 'Patients with cardio-vascular death (including fatal stroke) [N(%)]'
    7= 'Patients with cardio-vascular death, MI,stroke or unstable angina [N(%)]'
    ;

  VALUE stag3f
    1= '  Patients with ischemic stroke [N(%)]'
    2= '  Patients with hemorrhagic stroke [N(%)]'
    3= '  Not assessable [N(%)]'
    ;

  VALUE stag4f
    1= '  Patients with STEMI [N(%)]'
    2= '  Patients with NSTEMI [N(%)]'
    3= '  Not assessable [N(%)]'
    ;

  VALUE stag5f
    1= '  Patients with stable angina [N(%)]'
    2= '  Patients with unstable angina [N(%)]'
    ;

  VALUE stag6f
    1= '  Patients with acute MI [N(%)]'
    2= '  Patients with sudden death [N(%)]'
    3= '  Patients with worsening of heart failure [N(%)]'
    4= '  Patients with cardio-genic shock [N(%)]'
    5= '  Patients with fatal stroke [N(%)]'
    6= '    Patients with fatal ischemic stroke [N(%)]'
    7= '    Patients with fatal hemorrhagic stroke [N(%)]'
    8= '    Not assessable [N(%)]'
    ;

    VALUE $trtf
    '01'= 'Red\Blue'
    '02'= 'Blue\Blue'
    '03'= 'Blue\Blue'
    '04'= 'Brown\Brown'
    '05'= 'Black\White'
    '06'= 'White\White'
    '07'= 'Gray\Gray'
    ;

    VALUE epttrf /* RAGe has same named format???*/
    0="Value"
    1="Change"
    ;

    VALUE visit
      10  = "VISIT 1"
      20  = "VISIT 2"
      30  = "Baseline"
      40  = "Week 6"
      50  = "Week 12"
      60  = "Week 18"
      70  = "Week 24"
      80  = "Week 30"
      90  = "Week 36"
      100 = "Week 48"
      110 = "Week 60"
      120 = "Week 72"
      980 = "Week 84"
      990 = "Follow-up"
      ;

    VALUE tmic2f
      1 = "<= 1 year"
      2 = ">1 to 5 years"
      3 = ">5 to 10 years"
      4 = ">10 years"
     ;

/*  VALUE $criterionf  */
/*    'crit_01' ='ALT>3ULN or AST>3ULN and TBili>2ULN'                     */
/*    'crit_02' ='ALT>3ULN or AST>3ULN with concurrent* TBili>2ULN'        */
/*    'crit_03' ='ALT>3ULN or AST>3ULN with conc.* TBili>2ULN and ALKP<2ULN'*/
/*    'crit_04' ='ALT>3ULN with concurrent* TBili>2ULN'                    */
/*    'crit_05' ='ALT>3ULN and TBili>2ULN'                                */
/*    'crit_06' ='ALT>3ULN'                                                */
/*    'crit_07' ='ALT>5ULN'                                                */
/*    'crit_08' ='ALT>10ULN'                                               */
/*    'crit_09' ='ALT>20ULN'                                              */
/*    'crit_10' ='TBili>2ULN'                                              */
/*    'crit_11' ='ALT>3ULN or AST>3ULN'                                    */
/*    'crit_12' ='ALKP>1.5ULN'   */
/*   ;*/

   VALUE $criterionf 
    'crit_01' ='ALT>3ULC or AST>3ULC and TBili>2ULC'                    
    'crit_02' ='ALT>3ULC or AST>3ULC with concurrent* TBili>2ULC'       
    'crit_03' ='ALT>3ULC or AST>3ULC with conc.* TBili>2ULC and ALKP<2ULC'
    'crit_04' ='ALT>3ULC with concurrent* TBili>2ULC'                   
    'crit_05' ='ALT>3ULC and TBili>2ULC'                               
    'crit_06' ='ALT>3ULC'                                               
    'crit_07' ='ALT>5ULC'                                               
    'crit_08' ='ALT>10ULC'                                              
    'crit_09' ='ALT>20ULC'                                             
    'crit_10' ='TBili>2ULC'                                             
    'crit_11' ='ALT>3ULC or AST>3ULC'                                   
    'crit_12' ='ALKP>1.5ULC'  
   ;
  VALUE compyn
  1= "N"
  2= "No"
  3= "Yes"
  4= "Missing"
  ;

  VALUE tweek1f
  3 = "Run-in"
  4 = "Week 6"
  5 = "Week 12"
  6 = "Week 18"
  7 = "Week 24"
  8 = "Week 30"
  9 = "Week 36"
  10= "Week 48"
  11= "Week 60"
  12= "Week 72"
  13= "Week 84/EOT"
  ;

*Add by hamid 11MAR2013;
  VALUE _nofepif
    1="1"
    2="2 - 3"
    3=">= 4"
  ;

  VALUE _tmevf
    1="<= 7 days"
    2="> 7 days to <= 28 days"
    3="> 28 days"
  ;

  VALUE _glucf
    1="<54 mg/dL"                  
    2=">= 54 mg/dL and <= 70 mg/dL" 
    3="> 70 mg/dL"                 
    4="Not measured"
  ; 

  VALUE _asymf
    1="Any asymptomatic hypoglycaemia"             
    2="Any symptomatic, PG>= 54 and <= 70 mg/dL"    
    3="Any symptomatic, PG <54 mg/dL"               
    4="Severe hypoglycaemic episode**"             
  ;
  VALUE _demof
    11="Number of patients"
    21="Number of patients with investigator defined hypoglycaemia @ [N (%)]"
    22="  No"
    23="  Yes"                                               
    31="Number of episodes per patient # [N (%)]"
    32="  1"                                                   
    33="  2 - 3"                                              
    34="  >= 4"                                               
    41="Time to onset of first episode # [N (%)]"
    42="  <= 7 days"                                           
    43="  > 7 days to <= 28 days"                              
    44="  > 28 days"                                           
    51="Minimum glucose level (worst episode) # [N (%)]"
    52="  <54 mg/dL"                                                     
    53="  >= 54 mg/dL and <= 70 mg/dL"                         
    54="  > 70 mg/dL"                                                   
    55="  Not measured"                                
    61="Symptoms of hypoglycaemia (worst episode) # [N (%)]"
    62="  No"                                                  
    63="  Yes"                                                 
    71="Assistance required (worst episode) # [N (%)]"
    72="  No"                                                  
    73="  Yes"
    81="For patients with hypoglycaemia, # * [N (%)]"
    82="  Any asymptomatic hypoglycaemia"             
    83="  Any symptomatic, PG>= 54 and <= 70 mg/dL"    
    84="  Any symptomatic, PG <54 mg/dL"               
    85="  Severe hypoglycaemic episode**"             
    other=" "
  ; 

  VALUE _eptf
    0="No"
    1="Yes"
    2="Total"
  ;
  VALUE _hbac6f
    0="<6.5%"
    1=">=6.5%"
  ;

  VALUE _hbac2f
    1="<7.0%"
    2="7.0% to <8.0%"
    3="8.0% to <9.0%"
    4=">=9.0%"
  ;

  VALUE _hbac3f
    1="<7%"
    2=">=7%"
  ;


RUN;


  ******************************************************************;
  ******************************************************************;
  ******************************************************************;
  ****************                                  ****************;
  ****************              MACROS              ****************;
  ****************                                  ****************;
  ****************                                  ****************;
  ****************    Start macro names with X_     ****************;
  ****************                                  ****************;
  ******************************************************************;
  ******************************************************************;
  ******************************************************************;

/*%INCLUDE "!clinrep\1264_0003\qc\qc_tlf\x_count.sas";*/


  %******************************************************************;
  %*** YMD2DMY: Convert variables with character values of the    ***;
  %***            form YYYYMMDD to new variables with character   ***;
  %***            values of the form DDMMMYYYY.                   ***;
  %***                                                            ***;
  %*** If you replace the YMD variable with the new result, then  ***;
  %***   make sure YMD has at least LENGTH 9. Otherwise the last  ***;
  %***   character will not fit, as DMY has a DATE9. format.      ***;
  %***                                                            ***;
  %*** E.g. 20040302 => 02MAR2004                                 ***;
  %***      200405   =>   MAY2004                                 ***;
  %***      2004     =>      2004                                 ***;
  %***                                                            ***;
  %******************************************************************;

%MACRO ymd2dmy(ymdvar=, dmyvar=);

  IF (&ymdvar. NE '') THEN DO;
    IF LENGTH(&ymdvar.)=8 THEN &dmyvar.=PUT(INPUT(&ymdvar.,YYMMDD8.),DATE9.);
    ELSE IF LENGTH(&ymdvar.)=6 THEN &dmyvar.=SUBSTR(PUT(INPUT(TRIM(&ymdvar.)||'01',YYMMDD8.),DATE9.),3);
    ELSE IF LENGTH(&ymdvar.)=4 THEN &dmyvar.=PUT(&ymdvar.,$9.);
    &dmyvar.=RIGHT(&dmyvar.);
  END;

%MEND ymd2dmy;


  %******************************************************************;
  %*** YMD2DT: Convert variables with character values of the     ***;
  %***           form YYYYMMDD to new numeric variables with      ***;
  %***           complete dates of the for DDMMMYYYY.             ***;
  %***         Partial dates will be completed to form whole dates***;
  %***           based on values specified to replace missing day ***;
  %***           and month values.                                ***;
  %***                                                            ***;
  %*** Only create the DT variable if it is currently missing.    ***;
  %***                                                            ***;
  %*** If REPL_DAY=LASTDAY then the last day of the month will be ***;
  %***   used.                                                    ***;
  %***   Last day is derived as 1 day before the first day of the ***;
  %***   next month.                                              ***;
  %***                                                            ***;
  %*** E.g. If  repl_day=15        20040302 => 02MAR2004          ***;
  %***      and repl_mon=6         200405   => 15MAY2004          ***;
  %***                             2004     => 15JUN2004          ***;
  %***                                                            ***;
  %***      If  repl_day=LASTDAY   20040302 => 02MAR2004          ***;
  %***      and repl_mon=6         200405   => 31MAY2004          ***;
  %***                             200402   => 29FEB2004          ***;
  %***                             200602   => 28FEB2006          ***;
  %***                             2004     => 30JUN2004          ***;
  %***                                                            ***;
  %******************************************************************;

%MACRO ymd2dt(ymdvar=, dtvar=, repl_day=15, repl_mon=6);

  %IF &repl_day. = %STR() %THEN %LET repl_day=15;
  %IF &repl_mon. = %STR() %THEN %LET repl_mon=6;

  %IF %UPCASE(&repl_day.)=LASTDAY %THEN %LET _repl_dy=1;
  %ELSE %LET _repl_dy=&repl_day.;

  IF (&ymdvar. NE '') AND (&dtvar. EQ .) THEN DO;
    IF LENGTH(&ymdvar.)=8 THEN &dtvar.=INPUT(&ymdvar.,YYMMDD8.);
    ELSE DO;
      IF LENGTH(&ymdvar.)=6 THEN &dtvar.=INPUT(TRIM(&ymdvar.)||PUT(&_repl_dy.,Z2.),YYMMDD8.);
      ELSE IF LENGTH(&ymdvar.)=4 THEN &dtvar.=MDY(&repl_mon.,&_repl_dy.,INPUT(&ymdvar.,BEST.));

      %IF %UPCASE(&repl_day.)=LASTDAY %THEN %DO;
        IF MONTH(&dtvar.)=12 THEN &dtvar.=MDY(12,31,YEAR(&dtvar.));
        ELSE &dtvar.=MDY(MONTH(&dtvar.)+1,1,YEAR(&dtvar.))-1;
      %END;
    END;
  END;

  FORMAT &dtvar. DATE9.;

%MEND ymd2dt;


  %******************************************************************;
  %*** X_COUNTX:As we have another x_count with advanced features ***;
  %***          for this study,so it will be used as x_countx     ***;
  %***          just to count patients being used condition.      ***;
  %***                                                            ***;
  %*** Macro to count number of patients or number of obs.        ***;
  %*** It can count within BY groups if they are specified.       ***;
  %***                                                            ***;
  %*** E.g. %x_count(indata=ads.basco);                           ***;
  %***      %x_count(indata=ads.gentrt, byvars=analno atrslbl);   ***;
  %***                                                            ***;
  %******************************************************************;

%MACRO x_countx(text=, indata=, restrict=, outdata=x_count, byvars=, count=ptno);

  %IF %LENGTH(&byvars.) 
    %THEN %LET _byvars=%SYSFUNC(TRANWRD(%TRIM(%CMPRES(&byvars.)),%STR( ),%STR(,)));

  PROC SQL;
    CREATE TABLE &outdata. AS
    SELECT DISTINCT %IF %LENGTH(&text.) %THEN "&text." AS text LENGTH=100,;
/*                    LOWCASE("&indata.") AS dataset LABEL='Dataset name',*/
                    %IF %LENGTH(&byvars.) %THEN &_byvars.,;
                    %IF %UPCASE(&count.)=PTNO %THEN 
                      COUNT(DISTINCT ptno) AS noofptno LABEL='Number of patients';
                    %ELSE COUNT(*) AS noofobs LABEL='Number of observations';
    FROM &indata.
    %IF %LENGTH(&restrict.) %THEN WHERE &&restrict;
    %IF %LENGTH(&byvars.) %THEN GROUP BY &_byvars.;;
  QUIT;

  %IF &SQLOBS. GT 0 %THEN %DO;
    PROC PRINT DATA=&outdata. WIDTH=MIN LABEL;
    RUN;
  %END;
  %ELSE %DO;
    DATA _NULL_;
      FILE PRINT FOOTNOTE;
      PUT #5 @3 "&text. - None";
    RUN; 
  %END;

%MEND x_countx;


  %******************************************************************;
  %*** SEND2LOG:                                                  ***;
  %***                                                            ***;
  %*** TITLE   = Title for the listing e.g. wrong sex entered     ***;
  %*** INDATA  = Name of dataset to print    e.g. indata= patd,   ***;
  %*** VARS    = List of variables to print  e.g. vars= ptno sex, ***;
  %*** WIDTH   = Width of variables, by default each has 10 spaces***;
  %***             e.g. width= ptno 5 sex 6,                      ***;
  %*** LINELEN = Length of the dashed and * filled lines, 95 by   ***;
  %***             default.                                       ***;
  %***                                                            ***;
  %*** E.g. %send2log(title   = DI: SEX and RACE of all patients, ***;
  %***                indata  = patd,                             ***;
  %***                vars    = ptno sex racea,                   ***;
  %***                width   = ptno 5 sex 6);                    ***;
  %***                                                            ***;
  %******************************************************************;
 
%MACRO send2log(title=, indata=, vars=, width=, linelen=95);

  %LET _colhead = ;

  %******************************************************************;
  %*** Go through each variable and first identify the position   ***;
  %***   where it should be printed, then use this to create part ***;
  %***   of the PUT statement for the column header.              ***;
  %***   e.g. @5 "ptno" @15 "actevent"      (_COLHEAD)            ***;
  %*** Variables will be 10 spaces apart unless width is specified***;
  %***   for one or more variables.                               ***;
  %***                                                            ***;
  %*** Remove quotes from around the variables for using this     ***;
  %***   in the PUT statement to print the values of each variable***;
  %***   e.g. @5 ptno @15 actevent.         (_COLBODY)            ***;
  %******************************************************************;

  %LET _noofvar = 1;
  %DO %WHILE(%LENGTH(%SCAN(&vars.,&_noofvar.)));
    %IF &_noofvar. = 1 %THEN %LET _pos = 5;
    %ELSE %DO;
      %LET _pos = %EVAL(&_pos.+10);
      %IF %LENGTH(&width.) %THEN %DO;
        %LET _widthx = 1;
        %DO %WHILE(%LENGTH(%SCAN(&width.,&_widthx)) AND (&_widthx. LT 20));
          %IF %SCAN(&vars.,%EVAL(&_noofvar.-1)) = %SCAN(&width.,&_widthx.) 
            %THEN %LET _pos= %EVAL((&_pos.-10)+(%SCAN(&width.,%EVAL(&_widthx.+1)))+2);
          %LET _widthx = %EVAL(&_widthx.+2);
        %END; 
      %END;
    %END;
    %LET _colhead=%TRIM(&_colhead.) @&_pos. "%SCAN(&vars.,&_noofvar.)";
    %LET _noofvar = %EVAL(&_noofvar.+1);
  %END;

  %LET _colbody = %SYSFUNC(COMPRESS(&_colhead.,'"'));
  %******************************************************************;
  %*** Print the specified variables in the LOG window.           ***;
  %******************************************************************;

  DATA _NULL_;
    SET &indata. END=eof;
    LENGTH _star_ _dash_ $200 _title $150;
     
    _star_=REPEAT('*',&linelen);
    _dash_=REPEAT('-',&linelen-4);

    IF _N_=1 THEN DO;

      IF SUBSTR("&title.",1,3)="DI:" THEN _title = 'DATA'||' ISSUE:'||SUBSTR("&title.",4);
      ELSE IF SUBSTR("&title.",1,2)="W:" THEN _title = 'WAR'||'NING:'||SUBSTR("&title.",3);
      ELSE IF SUBSTR("&title.",1,2)="E:" THEN _title = 'ERR'||'OR:'||SUBSTR("&title.",3);
      ELSE _title = "&title.";

      PUT / _star_
          /'***'
          /"*** " _title
          /'***'
          /'***' %UPCASE(&_colhead.)
          /'*** ' _dash_;
    END;

    PUT '***' &_colbody.;

    IF eof THEN DO;
      PUT  '***'
          / _star_;
    END;
  RUN;

%MEND send2log;


  %******************************************************************;
  %*** PRINT_CHK:                                                 ***;
  %***                                                            ***;
  %*** OUTNO = Outputno for RAGe to send to a specified file.     ***;
  %***                                                            ***;
  %*** This macro prints all the datasets in the WORK library     ***;
  %***   which has a name starting with CHK_.                     ***;
  %***                                                            ***;
  %*** It expects the dataset to have a LABEL, which is used as   ***;
  %***   the title. For example:                                  ***;
  %***   DATA chk_trt(LABEL='Treatment order is not correct');    ***;
  %***                                                            ***;
  %*** If you wish to print specific variables in a specific      ***;
  %***   order, then this can be done using KEEP=var1 var2..      ***;
  %***   WITHIN the LABEL statement when creating the CHK_ dataset***;
  %***   For example:                                             ***;
  %***   DATA chk_trt(LABEL='Treatment order is not correct'      ***;
  %***                      'KEEP=ptno atrstdt atrspdt atrslbl'); ***;        
  %***                                                            ***;
  %*** Otherwise it will print all variables in the dataset in the***;
  %***   order they appear within the dataset.                    ***;
  %***                                                            ***;
  %*** You can restrict the variables in the dataset by using a   ***;
  %***   KEEP= statement when creating the dataset. For example:  ***;
  %***   DATA chk_trt(LABEL='Treatment order is not correct'      ***;
  %***                KEEP=ptno atrstdt atrspdt atrslbl);         ***;        
  %***                                                            ***;
  %******************************************************************;

%MACRO print_chk(outno=, cleanup=y);

  %IF %LENGTH(&outno.) %THEN %DO;
    %LET outputno=&outno.;
    %INC titles;
  %END;

  PROC SQL;
    CREATE TABLE _vtable AS
    SELECT DISTINCT libname, memname, memlabel
    FROM sashelp.vtable
    WHERE libname='WORK' AND memtype='DATA' AND SUBSTR(memname,1,4)='CHK_';
  QUIT;

  DATA _NULL_;
    SET _vtable;
    varpos=INDEX(UPCASE(memlabel),'KEEP=');
    IF varpos GT 0 THEN DO;
      lbl=SUBSTR(memlabel,1,varpos-1);
      tokeep=SUBSTR(memlabel,varpos+5);
    END;
    CALL EXECUTE ('TITLE3 "'||TRIM(lbl)||'";');
    CALL EXECUTE ('PROC PRINT DATA='||TRIM(memname)||' WIDTH=MIN;');
    IF varpos GT 0 THEN CALL EXECUTE ('VAR '||TRIM(tokeep)||';');
    CALL EXECUTE ('RUN;');
    CALL EXECUTE ('TITLE3;');
  RUN;

  %IF %UPCASE(&cleanup.) NE N AND %UPCASE(&cleanup.) NE NO %THEN %DO;
    PROC DATASETS LIB=work MEMTYPE=DATA NOLIST;
      DELETE _vtable;
    QUIT;
  %END;

%MEND print_chk;


  ******************************************************************;
  ***                                                            ***;
  *** CHKORD: Check the order of a variable when sorted by       ***;
  ***           another set of variables.                        ***;
  ***                                                            ***;
  *** NOTE: Using DESCENDING in the sort also helps to identify  ***;
  ***         cases where variable specified with "OF" has more  ***;
  ***         than one value when sorted by the "BY" variables.  ***;
  ***                                                            ***;
  ******************************************************************;

%MACRO chkord(indata=, ref=, of=, by=, alsoprnt=, desc=, print=n);

  %IF "&desc"="" %THEN %LET desc=%UPCASE(&indata.: &of) is not in order when sorted by %UPCASE(&by.);

  PROC SORT DATA=&indata OUT=&ref.1;
    BY &by. DESCENDING &of.;
  RUN;

  DATA &ref.2;
    ATTRIB _x FORMAT=$1. LABEL="Out of sequence flag";
    RETAIN _prev_;
    SET &ref.1;
    BY &by. DESCENDING &of.;
      
    IF NOT(FIRST.ptno) AND (&of. LT _prev_) THEN _x='*';
    OUTPUT;
    _prev_=&of.;
  RUN;

  PROC SQL;
    CREATE TABLE &ref.3 AS
    SELECT a.*
    FROM &ref.2 a, (SELECT DISTINCT ptno
                    FROM &ref.2
                    WHERE _x='*') b
    WHERE a.ptno=b.ptno;
  QUIT;

  %IF %UPCASE(%SUBSTR(&print.,1,1))=Y %THEN %DO;;
    TITLE3 "&desc.";

    PROC PRINT DATA=&ref.3 N;
      BY study ptno;
      VAR &by. &of. _x &alsoprnt.;
    RUN;
  %END;

  DATA _NULL_;
    IF 0 THEN SET &ref.3 NOBS=count;
    FILE PRINT;
    IF count=0 THEN PUT /'There are no observations';
  RUN;

  TITLE3;

%MEND chkord;

%MACRO cleanup(lib=work, type=data, dsets=);
  PROC DATASETS LIB=&lib. MEMTYPE=&type NOLIST %IF %LENGTH(&dsets)=0 %THEN KILL;;
    %IF %LENGTH(&dsets) %THEN DELETE &dsets;;
  QUIT;
%MEND cleanup;


  %******************************************************************;
  %*** TITLES: Program to read in SAS dataset containing the      ***;
  %***           Table of contents and generate titles and        ***; 
  %***           footnotes for the specified program name and     ***;
  %***           output number Specific for BI.                   ***;
  %***                                                            ***;
  %*** Define options to remove the printing of date and page no. ***;
  %*** Select the entry based on program name and output no. then ***;
  %***   derive title2 (as title1 is already in TEXT), the        ***;
  %***   footnote containing the program name and the date when   ***;
  %***   the program is submitted, and the name of the output file***;
  %*** These are then sent to macro variables.                    ***;
  %*** The macro variables are used to :                          ***;
  %***   - define the Pagesize and Linesize                       ***;
  %***   - generate title1, title2 and footnote1                  ***;
  %***   - re-direct output to the output file specified          ***;
  %******************************************************************;

/*%MACRO titles;*/
/**/
/*  OPTIONS NONUMBER NODATE;*/
/**/
/*  %GLOBAL _outdset _maindset _qcdset;*/
/**/
/*  DATA _NULL_;*/
/*    LENGTH _maindset _qcdset $32.;*/
/*    SET lst.biprint;*/
/*    WHERE progname="&progname." AND outputno="&outputno.";*/
/**/
/*    _t2='"'||REPEAT('',INDEX(text,descx)-2)||TRIM(descx2)||'"';*/
/**/
/*    _ft_txt=TRIM(progname)||'   '||PUT(TODAY(),DATE9.);*/
/*    _fnote ='"'||REPEAT('',linesize-LENGTH(_ft_txt)-1)||TRIM(_ft_txt)||'"';*/
/**/
/*    _outfile="%SYSFUNC(PATHNAME(lst))"||'\'||listref;*/
/**/
/*    _outdset=SCAN(listref,1,'.')||'(LABEL="'||TRIM(text)||'*/
/*'||TRIM(LEFT(descx2))||'")';*/
/**/
/*    IF progname NE ''  AND INDEX(listref,'qc_') THEN DO;*/
/*      _qcdset=SCAN(listref,1,'.');*/
/*      _maindset=SUBSTR(_qcdset,4);*/
/*    END;*/
/*    ELSE DO;*/
/*      _maindset=SCAN(listref,1,'.');*/
/*      _qcdset='qc_'||TRIM(_maindset);*/
/*    END;*/
/**/
/*    CALL SYMPUT ('_title1','"'||TRIM(text)||'"');*/
/*    CALL SYMPUT ('_title2',_t2);*/
/*    CALL SYMPUT ('footnote',_fnote);*/
/*    CALL SYMPUT ('_ps_ls','OPTIONS  PS='||COMPRESS(PUT(pagesize,BEST8.))||' LS='||COMPRESS(PUT(linesize,BEST8.)));*/
/*    CALL SYMPUT ('_outfile',_outfile);*/
/*    CALL SYMPUT ('_outdset',_outdset);*/
/*    CALL SYMPUT ('_maindset',_maindset);*/
/*    CALL SYMPUT ('_qcdset'  ,_qcdset);*/
/*  RUN;*/
/**/
/*  %PUT &_ps_ls.;*/
/*  OPTIONS &_ps_ls.;*/
/**/
/*  TITLE1 &_title1.;*/
/*  TITLE2 &_title2.;*/
/*  FOOTNOTE1 &footnote.;*/
/**/
/*  PROC PRINTTO PRINT="&_outfile." NEW;*/
/*  RUN;*/
/**/
/*%MEND titles;*/

%MACRO titles;
  OPTIONS NONUMBER NODATE;

  %GLOBAL _outdset _maindset _qcdset;

  DATA _NULL_;
    LENGTH _maindset _qcdset $32.;
    SET lst.biprint;
    WHERE progname="&progname." AND outputno="&outputno.";

    _t2='"'||REPEAT('',INDEX(text,descx)-2)||TRIM(descx2)||'"';

    _ft_txt=TRIM(progname)||'   '||PUT(TODAY(),DATE9.);
    _fnote ='"'||REPEAT('',linesize-LENGTH(_ft_txt)-1)||TRIM(_ft_txt)||'"';

    _outfile="%SYSFUNC(PATHNAME(lst))"||'\'||listref;

    _outdset=TRANWRD(SCAN(SCAN(listref,-1,'\'),1,'.'),"-","_")||'(LABEL="'||TRIM(text)||'
'||TRIM(LEFT(descx2))||'")';

    IF SUBSTR(LOWCASE(progname),1,4)='ctr\' THEN DO;
      _maindset=TRANWRD(SCAN(SCAN(listref,-1,'\'),1,'.'),"-","_");
      _qcdset='qc_'||TRIM(_maindset);
    END;
    ELSE DO;
      _qcdset=TRANWRD(SCAN(SCAN(listref,-1,'\'),1,'.'),"-","_");
      _maindset=SUBSTR(_qcdset,4);
    END;

    CALL SYMPUT ('_title1','"'||TRIM(text)||'"');
    CALL SYMPUT ('_title2',_t2);
    CALL SYMPUT ('footnote',_fnote);
    CALL SYMPUT ('_ps_ls','OPTIONS  PS='||COMPRESS(PUT(pagesize,BEST8.))||' LS='||COMPRESS(PUT(linesize,BEST8.)));
    CALL SYMPUT ('_outfile',_outfile);
    CALL SYMPUT ('_outdset',_outdset);
    CALL SYMPUT ('_maindset',_maindset);
    CALL SYMPUT ('_qcdset'  ,_qcdset);
  RUN;

  %PUT &_ps_ls.;
  OPTIONS &_ps_ls.;

  TITLE1 &_title1.;
  TITLE2 &_title2.;
  FOOTNOTE1 &footnote.;

  PROC PRINTTO PRINT="&_outfile." NEW;
  RUN;
%MEND titles;


  ******************************************************************;
  *** A macro to read title for qc compared outputs to validate  ***;
  *** main output data sets.                                     ***;
  ******************************************************************;

%MACRO qctitles;
  OPTIONS NONUMBER NODATE;
  %GLOBAL prognamex;
  %LET prognamex=%SYSFUNC(TRANWRD(&progname.,_qc\qc_,\));

  DATA _NULL_;
    SET lst.biprint;
    WHERE progname="&prognamex." AND outputno="&outputno.";

    _t2='"'||REPEAT('',INDEX(text,descx)-2)||TRIM(descx2)||'"';

    _ft_txt=TRIM(progname)||'   '||PUT(TODAY(),DATE9.);

    CALL SYMPUT ('_title1x','"'||TRIM(text)||'"');
    CALL SYMPUT ('_title2x',_t2);
  RUN;

  TITLE1 &_title1x.;
  TITLE2 &_title2x.;

%MEND qctitles;

*Null macros to be used in each program to keep similar structure with BI;

%MACRO gmlstart;
%MEND gmlstart;

%MACRO gmlend;
%MEND gmlend;



  ******************************************************************;
  ***                                                            ***;
  *** End of program                                             ***;
  ***                                                            ***;
  ******************************************************************;
  
