%macro editprog;

  %local pgmname;
  
   data _null_;
      set sashelp.vextfl;
      if (substr(fileref,1,3)='_LN' or substr (fileref,1,3)='#LN' or substr(fileref,1,3)='SYS') and index(upcase(xpath),'.SAS')>0 then do;
         call symput("pgmname",trim(xpath));
         stop;
      end;
   run;

   %PUT File: &pgmname.;

    DATA _null_ ;
        x "attrib -r %BQUOTE("&pgmname.")";

    RUN ;

    data _null_;
      /*call execute('dm "wedit; next;";');*/
      /*  call execute("dm 'winclose';");*/
      call execute('dm "whostedit '||"'"||"%superq(pgmname)"||"'"||'" ');
	 
	 /*call execute ("dm 'color text cyan; wpgm ;"||'"winclose" wpgm;'|| "   ';");*/
    run;
  %put Finished;

   DATA null ;
       x "attrib -r %BQUOTE("&pgmname.")";
                               dm wedit 'winclose' continue;
   RUN ;

       dm 'wedit "&pgmname."';
%mend;

%editprog;
