<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];

if(!isset($_SESSION["dc_selector"])){
    $dc_id = 0;
    $_SESSION["dc_selector"] =$dc_id;
}
else {
    $dc_id = $_SESSION["dc_selector"];
}
//$_SESSION["dc_selector"] = $dc_id;
$dc_db_val='SP'.$dc_id;


if(isset($_SESSION["filter_dc_selector"])){
    $dc2_id = $_SESSION["filter_dc_selector"];
}
else {
    $dc2_id = $dc_id;
    $_SESSION["filter_dc_selector"] = $dc2_id;
}


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Execute Program</li>
    </ol>
    
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <!-- Left col -->
      <div class="col-md-8">
        <!-- MAP & BOX PANE -->
        <div class="box box-success">
          <div class="box-header with-border">
            <h3 class="box-title">Study Name : <?php echo $_SESSION["study"] ?></h3>
            <div class="box-tools pull-right">

                <div class="col-md-12">
                    <label><h3 class="box-title"> Select Data Currency: </h3> </label>

                    <select id="dc_selector" name="dc_selector" onchange="reload_tree_dc(this)">
                        <?php
                        while($row = $snap_list->fetch_assoc()) {
                            $dc_id_c = $row['id'];
                            $sp_name = $row['snap_name'];

                            if($dc_id_c==$dc_id){
                                echo "<option value='".$dc_id_c."' selected='selected'>".$sp_name."</option>";
                                $sp_name_active=$sp_name;
                            }else{
                                echo "<option value=".$dc_id_c.">".$sp_name."</option>";
                            }

                        }

                        ?>


                    </select>
                </div>
            </div>
          </div><!-- /.box-header -->
          <div class="box-body no-padding">
            <div class="row">
              <div class="col-md-9 col-sm-8">
                <div class="pad">
                    <div class="row">
                      <div class="col-md-12">
                        <p style="font-size:18px;"> Run Existing Programs</p>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingTwo">
                              <h4 class="panel-title">
                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                  Select Individual Programs
                                </a>
                              </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                              <div class="panel-body">
                                <p>List of all available programs</p>
                                  <form onsubmit="runpgm(this,2); return false;" >
                                    <div class="col-xs-8">
                                      <div class="checkbox icheck">
                                        <?php                                        
                                          $sql="SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND y.pgmstat!=0 AND x.data_currency='$dc_db_val' ORDER BY x.sortorder  ";
                                          $result = $conn->query($sql);
                                          if ($result->num_rows > 0) {
                                            while($row = $result->fetch_assoc()) {
                                              if($row['pgmstat']==1){
                                                echo "<label style='color:#F6AA12;'><input type='checkbox' class='ckbox' name='pgnam' value='".$row['sortorder']."'>".$row['title']."</label><br />";
                                              }
                                              if($row['pgmstat']==3){
                                                 echo "<label style='color:#0FAA0F;'><input type='checkbox' class='ckbox' name='pgnam' value='".$row['sortorder']."'>".$row['title']."</label><br />";
                                              }
                                            }
                                              // echo "$stringData2";
                                          } 
                                          else{

                                          }
                                        ?>
                                      </div>
                                    </div>
                                  <div class="col-xs-4">
                                    <button type="submit" disabled class="btn btn-primary btn-block btn-flat sendRun">Run</button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>


                        <div class="panel panel-default">
                          <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                              <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Select Sections
                              </a>
                            </h4>
                          </div>
                          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                              <div class="panel-body">
                                        <p>List of all Sections</p>
                                        <form onsubmit="runpgm(this,4); return false;" >
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                        <?php                                        
                                                        $sql="SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder  AND x.data_currency=y.data_currency AND x.data_currency='$dc_db_val' AND x.section!='' ORDER BY x.sortorder  ";
                                                        $result = $conn->query($sql);
                                                        if ($result->num_rows > 0) {
                                                            while($row = $result->fetch_assoc()) {
                                                                   if($row['l1']!=0){
                                                                      if ($row['l2']!=0) {
                                                                          if ($row['l3']!=0) {
                                                                              if ($row['l4']!=0) {
                                                                                  if ($row['l5']!=0) {
                                                                                      echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' class='ckbox' name='outsec' value='".$row['sortorder']."'>l5: ".$row['section']."</label><br />";
                                                                                  }else{
                                                                                      echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' class='ckbox' name='outsec' value='".$row['sortorder']."'>l4: ".$row['section']."</label><br />";
                                                                                  }
                                                                              }else{
                                                                                  echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' class='ckbox' name='outsec' value='".$row['sortorder']."'>l3: ".$row['section']."</label><br />";
                                                                              }
                                                                          }else{
                                                                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label><input type='checkbox' class='ckbox' name='outsec' value='".$row['sortorder']."'>l2: ".$row['section']."</label><br />";
                                                                          }
                                                                      }else{
                                                                          echo "<label><input type='checkbox' class='ckbox' name='outsec' value='".$row['sortorder']."'>l1: ".$row['section']."</label><br />";
                                                                      }
                                                                  }
                                                            }
                                                            // echo "$stringData2";
                                                        } else {

                                                        }
                                                        ?>
                                                  </div>
                                              </div>
                                              <div class="col-xs-4">
                                                  <button type="submit"  disabled class="btn btn-primary btn-block btn-flat sendRun">Run</button>
                                              </div>
                                        </form>

                              </div>
                          </div>

                      </div>
                    </div>
                   </div>

                </div>
                </div>
              </div><!-- /.col -->
            </div><!-- /.row -->
          </div><!-- /.box-body -->
        </div><!-- /.box -->
      </div><!-- /.col -->          
    </div><!-- /.row (main row) -->
  </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
      