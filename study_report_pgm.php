<?php 
require('fpdf.php');
session_start();
include("include/connect.php");
$study_name=$_SESSION["study"];

$user_check=$_SESSION['login_user'];
$user = $_SESSION['login_user'];
$snapshot = $_SESSION['snapshot'];
$dc_name1 = $_SESSION["dc_selector"];
$dc_db_val = 'SP'.$dc_name1;

class PDF_MC_Table extends FPDF
{
var $widths;
var $aligns;

function SetWidths($w)
{
    //Set the array of column widths
    $this->widths=$w;
}

function SetAligns($a)
{
    //Set the array of column alignments
    $this->aligns=$a;
}

function Row($data)
{
    //Calculate the height of the row
    $nb=0;
    for($i=0;$i<count($data);$i++)
        $nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
    $h=5*$nb;
    //Issue a page break first if needed
    $this->CheckPageBreak($h);
    //Draw the cells of the row
    for($i=0;$i<count($data);$i++)
    {
        $w=$this->widths[$i];
        $a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
        //Save the current position
        $x=$this->GetX();
        $y=$this->GetY();
        //Draw the border
        $this->Rect($x,$y,$w,$h);
        //Print the text
        $this->MultiCell($w,5,$data[$i],0,$a);
        //Put the position to the right of the cell
        $this->SetXY($x+$w,$y);
    }
    //Go to the next line
    $this->Ln($h);
}

function CheckPageBreak($h)
{
    //If the height h would cause an overflow, add a new page immediately
    if($this->GetY()+$h>$this->PageBreakTrigger)
        $this->AddPage($this->CurOrientation);
}

function NbLines($w,$txt)
{
    //Computes the number of lines a MultiCell of width w will take
    $cw=&$this->CurrentFont['cw'];
    if($w==0)
        $w=$this->w-$this->rMargin-$this->x;
    $wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
    $s=str_replace("\r",'',$txt);
    $nb=strlen($s);
    if($nb>0 and $s[$nb-1]=="\n")
        $nb--;
    $sep=-1;
    $i=0;
    $j=0;
    $l=0;
    $nl=1;
    while($i<$nb)
    {
        $c=$s[$i];
        if($c=="\n")
        {
            $i++;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
            continue;
        }
        if($c==' ')
            $sep=$i;
        $l+=$cw[$c];
        if($l>$wmax)
        {
            if($sep==-1)
            {
                if($i==$j)
                    $i++;
            }
            else
                $i=$sep+1;
            $sep=-1;
            $j=$i;
            $l=0;
            $nl++;
        }
        else
            $i++;
    }
    return $nl;
}

// Page header
function Header()
{
    include("include/connect.php");
    $study_name=$_SESSION["study"];

    $user_check=$_SESSION['login_user'];
    $user = $_SESSION['login_user'];
	
	date_default_timezone_set('Asia/Dhaka');
	$date_time=date("Y-m-d, H:i:s");
	
    // Logo
    $this->Image('dist/img/logo.png',100,5,100);
    // Arial bold 15
    $this->SetFont('Arial','B',10);
    // Move to the right
    $this->Cell(80);
    // Title
    $this->Cell(30,10,'','C');
    // Line break
    $this->Ln(20);

    $this->SetY(22);
    $this->SetX(10);
    $this->Cell(40,6,"Report Title: Current Status Of Programs",0,"C",1);

    $this->SetY(30);
    $this->SetX(10);
    $this->Cell(40,6,"Study Name: ".$study_name.',',0,"C",1);
	
	$this->SetY(30);
    $this->SetX(55);
    $this->Cell(40,6,"Snapshot Name: ".$_SESSION['snapshot'],0,"C",1);
	
    $this->SetY(38);
    $this->SetX(10);
    $this->Cell(35,6,"Date: ".$date_time,0,"C",1);
    $this->SetFillColor(232,232,232);
    //Bold Font for Field Name
    $this->SetFont('Arial','B',10);

    $this->SetY(49);
    $this->SetX(10);
    $this->MultiCell(30,8,"\nProgram \n Name",1,'C',1);
    $this->SetY(49);
    $this->SetX(40);
    $this->MultiCell(20,8,"\nProgram \n Location",1,'C',1);
    $this->SetY(49);
    $this->SetX(60);
    $this->MultiCell(30,8,"\nProgrammer \n Name",1,'C',1);
	$this->SetY(49);
    $this->SetX(90);
    $this->MultiCell(25,8,"\nSnapshot Name",1,'C',1);
    $this->SetY(49);
    $this->SetX(115);
    $this->MultiCell(30,6,"Date/Time of Last \n Program Update",1,'C',1);
    $this->SetY(49);
    $this->SetX(145);
    $this->MultiCell(30,6,"Date/Time of Last \n Program Submit",1,'C',1);
    $this->SetY(49);
    $this->SetX(175);
    $this->MultiCell(30,8,"Date/Time of Last \n Status Update",1,'C',1);
    $this->SetY(49);
    $this->SetX(205);
    $this->MultiCell(30,8,"\nValidator \n Name",1,'C',1);
    $this->SetY(49);
    $this->SetX(235);
    $this->MultiCell(25,8,"\nCurrent \n Status",1,'C',1);
    $this->SetY(49);
    $this->SetX(260);
    $this->MultiCell(30,8,"\nFinal Status \n at Program Run",1,'C',1);
    
    $this->SetY(65);
    $this->Ln();
}

// Page footer
function Footer()
{
    $user_check=$_SESSION['login_user'];
    $user = $_SESSION['login_user'];
    // Position at 1.5 cm from bottom
    $this->SetY(-15);
    // Arial italic 8
    $this->SetFont('Arial','I',8);
    // Page number
    $this->Cell(0,10,'Page '.$this->PageNo().'/{nb}',0,0,'C'); 
    // Page Print by
    //$this->Cell(0,10,'Print by : ',0,0,'C'); 
    // Print Date
    //$this->Cell(120,10,'Print Date : ');
    $this->SetY(-13);
    $this->SetFont('Arial','',10);
    $this->Cell(30,6,'Printed By : '.$user);

    $this->SetY(-13);
    $this->SetX(-45);
    $this->SetFont('Arial','',10);
    $this->Cell(30,6,'Print Date : '.date("d M Y"));
  // $pdf->SetFont('Arial','',10);
  // $pdf->SetY(270);
  // $pdf->SetX(160);
  // $pdf->Cell(30,6,'Print Date : '.date("Y.m.d"));
}
}




$pdf=new PDF_MC_Table();
$pdf->AddPage('L','A4');
$pdf->AliasNbPages();
$pdf->SetFont('Arial','',10);

//Fields Name position
$Y_Fields_Name_position = 20;
//Table position, under Fields Name
$Y_Table_Position = 26;

//First create each Field Name
//Gray color filling each Field Name box




//Table with 20 rows and 4 columns
$pdf->SetWidths(array(30,20,30,25,30,30,30,30,25,30));
  include("include/connect.php");
  $study_name=$_SESSION["study"];

  $user_check=$_SESSION['login_user'];
  $user = $_SESSION['login_user'];
  
	$sql = "SELECT q.pgmloc, p.pgmname, p.username, p.data_currency, p.event_date,  p.comment, p.status
					FROM   pgm_hist_$study_name AS p
						   LEFT JOIN toc_$study_name AS q
								  ON p.pgmname = q.pgmname
					WHERE  p.status = 'Program running'
						   AND q.data_currency = '$dc_db_val'
						   AND p.data_currency = '$dc_db_val'
						   AND q.title != ' '
					GROUP  BY p.pgmname
					ORDER  BY p.pgmname ASC, p.event_date DESC";
	$result = $conn->query($sql);


    $temp="";
	$distinct_pgm = array();
	$distinct_out = array();
    $distinct_status = array();
    $pgm_run_status = array();
	$current_status = '';
	$validator_name = '';
    while($row = $result->fetch_assoc()){
    $pgmname = $row['pgmname'];
    if (!in_array($pgmname, $pgm_run_status)) {
        $sql_get_max_date_run = "SELECT comment
                                                        FROM   pgm_hist_$study_name
                                                        WHERE  pgmname = '$pgmname'
                                                               AND data_currency = '$dc_db_val'
                                                               AND  status = 'Program running' ORDER BY event_date DESC LIMIT 1";
        $result2=$conn->query($sql_get_max_date_run);
        while($row2 = $result2->fetch_assoc()) {
            $pgm_run_stat = $row2['comment'];
        }
        array_push($pgm_run_status,$pgmname);
        $pgm_run_status[$pgmname] = $pgm_run_stat;
    }
	if (!in_array($pgmname, $distinct_pgm)) {
		$sql_get_max_date = "SELECT Max(event_date) AS pgm_date
						FROM   pgm_hist_$study_name
						WHERE  pgmname = '$pgmname'
							   AND data_currency = 'SP0'
							   AND ( comment LIKE '%Edited%'
									  OR comment LIKE '%Uploaded%'
									  OR comment LIKE '%Generated by System%' ) LIMIT 1"; 
	   $result2=$conn->query($sql_get_max_date);								  
	   while($row2 = $result2->fetch_assoc()) {
		 $pgm_date = $row2['pgm_date'];  
	   }
	   array_push($distinct_pgm,$pgmname);
	   $distinct_pgm[$pgmname] = $pgm_date;
    }
	if (!in_array($pgmname, $distinct_out)) {
		$sql2_get_max_date = "SELECT 
							Max(event_date) AS out_date
							FROM   pgm_hist_$study_name
							WHERE pgmname = '$pgmname'
							AND data_currency = '$dc_db_val'
							AND  comment LIKE '%Run%' LIMIT 1"; 
	   $result3=$conn->query($sql2_get_max_date);								  
	   while($row3 = $result3->fetch_assoc()) {
		 $out_date = $row3['out_date'];  
	   }
	   array_push($distinct_out,$pgmname);
	   $distinct_out[$pgmname] = $out_date;
   }
	if (!in_array($pgmname, $distinct_status)) {
		$sql3_get_max_date = "SELECT pgmname,
										   username AS validator,
										   status,
										   event_date
									FROM   pgm_hist_$study_name
									WHERE  pgmname = '$pgmname'
										   AND event_date IN (SELECT Max(event_date) AS event_date
															  FROM   pgm_hist_$study_name
															  WHERE  pgmname = '$pgmname'
																	 AND data_currency = 'SP0'
																	 AND comment NOT LIKE '%Run%')
									ORDER  BY event_date DESC"; 
							  
	   $result4=$conn->query($sql3_get_max_date);								  
	   while($row4 = $result4->fetch_assoc()) {
		 $current_status = $row4['status'];
		 if($current_status == 'Validated')
		 {
			$validator_name = $row4['validator'];
		 }else{
			 $validator_name = '';
		 }												 
	   }
	   array_push($distinct_status,$pgmname);
	   $distinct_status[$pgmname] = $current_status;
   }
	
	
    $pgmloc = substr_replace($row["pgmloc"],"",strripos($row["pgmloc"],"/")+1);
    $pgmuser = $row["username"];
    //$study_id = $study_id;
    $user = $user;
	if($row["status"] == 'Validated')
	{
		$username = $row['username'];
	}
	else 
	{
		$username = "";
	}
    $status = $row['status'];
    $event_date = $row['event_date'];
      $date = $row["event_date"];
      if ($date != "") {
        $my_date = date('d M Y H:i:s', strtotime($date));
      }
      else{
        $my_date = $date;
      }
	  
      $pdate = $distinct_pgm[$pgmname];
      if ($pdate != "") {
        $p_date = date('d M Y H:i:s', strtotime($pdate));
      }
      else{
        $p_date = $pdate;
      }

      $out_date = $distinct_out[$pgmname];
      if ($out_date != "") {
      $out_date = date('d M Y H:i:s', strtotime($out_date));
      }
      else{
      $out_date = $out_date;
      }

	  
									 
	  $pdf->Row(array($pgmname,$pgmloc,$pgmuser,$snapshot,$p_date,$out_date,$out_date,$validator_name,$distinct_status[$pgmname],$pgm_run_status[$pgmname]));
  } 

//  $pdf->Output();

$file_name = 'study_report_pgm_'.date('Y-m-d h:i:s a').'.pdf';
$pdf->Output('D',$file_name);
?>