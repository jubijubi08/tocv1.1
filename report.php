<?php
include("include/header.php");
include("include/connect.php");
$study_name=$_SESSION["study"];

//Check if any common programs exist or not
$study_name=strtolower($study_name);
$table="cplist_$study_name";
$result=$conn->query("SHOW TABLES FROM toc_db");
while($row = mysqli_fetch_row($result)){
    $arr[] = $row[0];
}

if(in_array($table,$arr))
{
    $cp_exist=1;
}else{
    $cp_exist=0;
}
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

  <!-- Content Header (Page header) -->
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      <li class="active">Manage Programs</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
      <!-- Main row -->
      <div class="row">
          <!-- Left col -->
          <div class="col-md-12">

            <!-- MAP & BOX PANE -->
            <div class="box box-success">
                <div class="box-header with-border">
                   <h3 class="box-title">Study Name:<?php echo $_SESSION["study"] ?></h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                  <div class="row">
                    <div class="col-md-12 col-sm-8">
                      <div class="pad">
                          <div class="row">
                            <div class="col-md-12">
                                <p style="font-size:20px;">List of all Outputs</p>
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-12">
                                
                              <?php 
                                $sql = "SELECT x.sortorder,x.title,y.username,y.status,y.event_date 
                                        FROM (SELECT * FROM toc_$study_name WHERE title !=' ') as x 
                                        LEFT OUTER JOIN (SELECT a.sortorder as sortorder ,a.username as username ,a.status as status,a.event_date as event_date 
                                        FROM pgm_hist_$study_name AS a 
                                        JOIN toc_$study_name AS b 
                                        WHERE a.sortorder=b.sortorder AND a.event_date IN (SELECT MAX(event_date) 
                                                                                              FROM pgm_hist_$study_name 
                                                                                              GROUP BY sortorder) 
                                            GROUP BY a.sortorder) as y
                                        ON x.sortorder=y.sortorder
                                        ORDER BY x.sortorder ASC"; 
                                $result = $conn->query($sql);
                                // print_r($result);

                                // if($result){
                                if ($result->num_rows > 0) {
                                  echo "<table id='myTable' class='table table-hover'>";
                                  echo "<thead>";
                                  echo "<tr>";
                                  echo "<th>Title</th>";
                                  echo "<th>Programer</th>";
                                  echo "<th>Status</th>";                    
                                  echo "<th>Modify Date</th>";
                                  echo "</tr>";
                                  echo "</thead>";
                                  echo "<tbody>";

                                  while($row = $result->fetch_assoc()) {

                                    
                                      // echo "<tr bgcolor='#FF0000'>";
                                      echo "<tr>";
                                      echo "<td>" . $row["title"].  "</td>";
                                      echo "<td>" . $row["username"].  "</td>";
                                      echo "<td>" . $row["status"].  "</td>";
                                      echo "<td>" . $row["event_date"].  "</td>";
                                      echo "</tr>";
                                  }
                                  echo "</tbody>";
                                  echo "</table>";       
                                } else {
                                      echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                }
                                // }else {
                                  // echo "<span class='glyphicon glyphicon-exclamation-sign' aria-hidden='true'>  No Programs uploaded yet</span>";
                                // }

                                ?>
															
                            </div> 
                          </div>

                          <div class="row">
                            <div class="col-md-9">
                                <p style="font-size:20px;"> Download Report in PDF format</p>
                            </div>
                            <div class="col-md-3">
                                
                                <a href='fpdfmysql.php' target='_blank' class='demo'>
                                <button  class="btn btn-primary btn-block btn-flat" >Download</button></a>
                            </div> 
                          </div>
                      </div>
                    </div><!-- /.col -->
                  </div><!-- /.row -->
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->          
        </div><!-- /.row (main row) -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->



<?php
include("include/footer.php");
$conn->close();
?>
      