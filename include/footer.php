<?php
error_reporting(E_ALL ^ E_NOTICE);
require_once 'include/excel_reader2.php';
include("include/connect.php");
?>
<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1.01
    </div>
    <strong>Copyright &copy; 2016 <a href="http://shaficonsultancy.com">MeD-OMS</a>.</strong> All rights reserved.
</footer>
</div><!-- ./wrapper -->

<!-- jQuery 2.1.3 -->
<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="dist/js/app.min.js" type="text/javascript"></script>
<!-- study titles tree  -->
<script type="text/javascript" src="stree/jquery.ztree.core-3.5.js"></script>
<script type="text/javascript" src="stree/jquery.ztree.excheck-3.5.js"></script>
<script type="text/javascript" src="stree/jquery.ztree.exedit-3.5.js"></script>
<!-- crypto js titles tree  -->
<script type="text/javascript" src="js/aes.js"></script>
<!-- study titles tree  -->
<script>
    !function (e) {
        var t = function (t, n) {
            this.$element = e(t), this.type = this.$element.data("uploadtype") || (this.$element.find(".thumbnail").length > 0 ? "image" : "file"), this.$input = this.$element.find(":file");
            if (this.$input.length === 0)return;
            this.name = this.$input.attr("name") || n.name, this.$hidden = this.$element.find('input[type=hidden][name="' + this.name + '"]'), this.$hidden.length === 0 && (this.$hidden = e('<input type="hidden" />'), this.$element.prepend(this.$hidden)), this.$preview = this.$element.find(".fileupload-preview");
            var r = this.$preview.css("height");
            this.$preview.css("display") != "inline" && r != "0px" && r != "none" && this.$preview.css("line-height", r), this.original = {
                exists: this.$element.hasClass("fileupload-exists"),
                preview: this.$preview.html(),
                hiddenVal: this.$hidden.val()
            }, this.$remove = this.$element.find('[data-dismiss="fileupload"]'), this.$element.find('[data-trigger="fileupload"]').on("click.fileupload", e.proxy(this.trigger, this)), this.listen()
        };
        t.prototype = {
            listen: function () {
                this.$input.on("change.fileupload", e.proxy(this.change, this)), e(this.$input[0].form).on("reset.fileupload", e.proxy(this.reset, this)), this.$remove && this.$remove.on("click.fileupload", e.proxy(this.clear, this))
            }, change: function (e, t) {
                if (t === "clear")return;
                var n = e.target.files !== undefined ? e.target.files[0] : e.target.value ? {name: e.target.value.replace(/^.+\\/, "")} : null;
                if (!n) {
                    this.clear();
                    return
                }
                this.$hidden.val(""), this.$hidden.attr("name", ""), this.$input.attr("name", this.name);
                if (this.type === "image" && this.$preview.length > 0 && (typeof n.type != "undefined" ? n.type.match("image.*") : n.name.match(/\.(gif|png|jpe?g)$/i)) && typeof FileReader != "undefined") {
                    var r = new FileReader, i = this.$preview, s = this.$element;
                    r.onload = function (e) {
                        i.html('<img src="' + e.target.result + '" ' + (i.css("max-height") != "none" ? 'style="max-height: ' + i.css("max-height") + ';"' : "") + " />"), s.addClass("fileupload-exists").removeClass("fileupload-new")
                    }, r.readAsDataURL(n)
                } else this.$preview.text(n.name), this.$element.addClass("fileupload-exists").removeClass("fileupload-new")
            }, clear: function (e) {
                this.$hidden.val(""), this.$hidden.attr("name", this.name), this.$input.attr("name", "");
                if (navigator.userAgent.match(/msie/i)) {
                    var t = this.$input.clone(!0);
                    this.$input.after(t), this.$input.remove(), this.$input = t
                } else this.$input.val("");
                this.$preview.html(""), this.$element.addClass("fileupload-new").removeClass("fileupload-exists"), e && (this.$input.trigger("change", ["clear"]), e.preventDefault())
            }, reset: function (e) {
                this.clear(), this.$hidden.val(this.original.hiddenVal), this.$preview.html(this.original.preview), this.original.exists ? this.$element.addClass("fileupload-exists").removeClass("fileupload-new") : this.$element.addClass("fileupload-new").removeClass("fileupload-exists")
            }, trigger: function (e) {
                this.$input.trigger("click"), e.preventDefault()
            }
        }, e.fn.fileupload = function (n) {
            return this.each(function () {
                var r = e(this), i = r.data("fileupload");
                i || r.data("fileupload", i = new t(this, n)), typeof n == "string" && i[n]()
            })
        }, e.fn.fileupload.Constructor = t, e(document).on("click.fileupload.data-api", '[data-provides="fileupload"]', function (t) {
            var n = e(this);
            if (n.data("fileupload"))return;
            n.fileupload(n.data());
            var r = e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');
            r.length > 0 && (r.trigger("click.fileupload"), t.preventDefault())
        })
    }(window.jQuery)
</script>
<!-- Program Editor  -->
<link rel="stylesheet" type="text/css" href="dist/prism/prism.css"/>
<script src="dist/prism/prism.js" type="text/javascript"></script>
<script src="dist/prism/components.js" type="text/javascript"></script>
<script src="dist/prism/prism-sas.js" type="text/javascript"></script>
<script src="dist/prism/prism-sas.min.js" type="text/javascript"></script>

<!-- datatable   -->
<script src="plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js" type="text/javascript"></script>


<!-- notify   -->
<script src="plugins/notify/bootstrap-notify.min.js" type="text/javascript"></script>
<!-- upload script   -->


<SCRIPT type="text/javascript">

    $.extend(true, $.fn.dataTable.defaults, {
        "searching": false,
        "info": false

    });


    $(document).ready(function () {
        $('#example1').DataTable();
    });


    var setting = {
        view: {
            selectedMulti: true,
            showLine: false,
            fontCss: getFont,
            nameIsHTML: true
        },
        check: {
            enable: false
        },
        callback: {
            onRightClick: OnRightClick,
            beforeClick: beforeClick,
            beforeCollapse: beforeCollapse,
            beforeExpand: beforeExpand,
            onCollapse: onCollapse,
            onExpand: onExpand
        },
        data: {
            simpleData: {
                enable: true
            }
        },
        edit: {
            enable: false
        }
    };

    var zNodes = [
   
        //$study_name="R101";
<?php
        $dc_db_val="'SP".$dc_id."', 'SP".$dc2_id."'";

        if ($dc_id!=$dc2_id) {
            $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val) AND x.id NOT IN (SELECT id FROM toc_$study_name WHERE section !='' AND data_currency = 'SP$dc_id')  ORDER BY x.sortorder";
        }else{
            $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val)  ORDER BY x.sortorder";
        }// echo $sql;
        if($selected_section != "all"){
            $sql="SELECT * FROM toc_$study_name WHERE Section='$selected_section'";
            $section_sort_order = $conn->query($sql);
            while($row = $section_sort_order->fetch_assoc()){
                $l1= $row['l1'];
                $l2= $row['l2'];
            }
            $like = $l1.$l2;
            $sql = "SELECT * FROM toc_$study_name AS x ,toc_status_$study_name AS y WHERE x.study=y.study AND x.sortorder=y.sortorder AND x.data_currency=y.data_currency AND x.data_currency IN ($dc_db_val) AND x.sortorder LIKE '$like%' ORDER BY x.sortorder";
        }
        $result = $conn->query($sql);
        // print_r($result);
        //echo "<br>";
        $count = 1;
        if ($result->num_rows > 0) {

            while ($row = $result->fetch_assoc()) {
                $count++;
                $pgmstat = $row["pgmstat"];
                $outstat = $row["outstat"];
                $dc_name = $row["data_currency"];


                //code for name argument
                if (strlen(trim($row['section'], " ")) == 0) {
                    $name = $row["type"] . " " . $row["tlfnum"] . " - " . $row["title"];
                    // $name=$row["type"]." ".(int)$row["l1"].".".(int)$row["l2"].".".(int)$row["l3"].".".(int)$row["l4"].".".(int)$row["l5"]." ".$row["title"];
                    //   if($row["l5"]=="00")
                    //     $name=$row["type"]." ".(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"].".". (int)$row["l4"]." ".$row["title"];
                    //   if($row["l4"]=="00")
                    //     $name=$row["type"]." ".(int)$row["l1"].".". (int)$row["l2"].".". (int)$row["l3"]." ".$row["title"];
                    //   if($row["l3"]=="00")
                    //     $name=$row["type"]." ".(int)$row["l1"].".". (int)$row["l2"]." ".$row["title"];
                    //   if($row["l2"]=="00")
                    //     $name=$row["type"]." ".(int)$row["l1"]." ".$row["title"];
                } else {
                    $name = (int)$row["l1"] . "." . (int)$row["l2"] . "." . (int)$row["l3"] . "." . (int)$row["l4"] . "." . (int)$row["l5"] . " " . $row["section"];
                    if ($row["l5"] == "00")
                        $name = (int)$row["l1"] . "." . (int)$row["l2"] . "." . (int)$row["l3"] . "." . (int)$row["l4"] . " " . $row["section"];
                    if ($row["l4"] == "00")
                        $name = (int)$row["l1"] . "." . (int)$row["l2"] . "." . (int)$row["l3"] . " " . $row["section"];
                    if ($row["l3"] == "00")
                        $name = (int)$row["l1"] . "." . (int)$row["l2"] . " " . $row["section"];
                    if ($row["l2"] == "00")
                        $name = (int)$row["l1"] . " " . $row["section"];
                }

                //code for pid argument
                if ($row["l2"] == "00")
                    $row["l2"] = " ";
                if ($row["l3"] == "00")
                    $row["l3"] = " ";
                if ($row["l4"] == "00")
                    $row["l4"] = " ";
                if ($row["l5"] == "00")
                    $row["l5"] = " ";
                $allnum = $row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"];
                //echo "$allnum";
                $len = iconv_strlen(trim($allnum));
                if ($len == 2)
                    $p = "00";
                else
                    $p = substr(trim($allnum), 0, $len - 2);


                //detect pid have child or not
                $child_number = 0;
                if (strlen(trim($row['section'], " ")) != 0) {
                    $idtoseach = str_replace("00", "", $row['sortorder']);
                    // echo "id to search for child count : ".$idtoseach."<br>";
                    $sql9 = "SELECT * FROM toc_" . $study_name . " WHERE sortorder LIKE '$idtoseach%' ";
                    $result9 = $conn->query($sql9);
                    $child_number = $result9->num_rows;
                    // echo "number of child : ".$child_number."<br><br><br>";
                }

                $lst = "{id: '" . trim($row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"]) . "' ,pId:'" . $p . "', name: '" . $name . "',},";
                if (strlen(trim($row['section'], " ")) == 0)
                {
                    if (strlen(trim($row['section'], " ")) == 0 AND $dc_id!=$dc2_id AND $dc_name== 'SP0')
                        $lst = "{id: '" . trim($row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"]) . "' ,pId:'" . $p . "', name: '" . $name . "',icon:'./stree/metroStyle/img/state_9_9.png'},";

                    else if ($count - 1 == $result->num_rows AND $dc_id!=$dc2_id AND $dc_name== 'SP0')
                        $lst = "{id: '" . trim($row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"]) . "' ,pId:'" . $p . "', name: '" . $name . "',icon:'./stree/metroStyle/img/state_9_9.png'}";

                    else if($count - 1 == $result->num_rows)
                        $lst = "{id: '" . trim($row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"]) . "' ,pId:'" . $p . "', name: '" . $name . "',icon:'./stree/metroStyle/img/state_" . $pgmstat . "_" . $outstat . ".png'}";

                    else $lst = "{id: '" . trim($row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"]) . "' ,pId:'" . $p . "', name: '" . $name . "',icon:'./stree/metroStyle/img/state_" . $pgmstat . "_" . $outstat . ".png'},";
                }else{
                    $lst = "{id: '" . trim($row["l1"] . $row["l2"] . $row["l3"] . $row["l4"] . $row["l5"]) . "' ,pId:'" . $p . "', name: '" . $name . "', isParent:true},";
                }


                echo $lst;
            }
        } else {
            // $lst= "{id: 1 ,pId:0, name: 'No TOC available for this study',icon:'./stree/metroStyle/img/metro2.png'}" ;
            // echo "$lst";
        }
?>
    ];

    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////tree functions///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////
    var pgmid;
    var pgmname;
    var nodeuid;
    var pgmerr;
    var pgmwarn;

    function OnRightClick(event, treeId, treeNode) {

       // console.log(event);
        pgmid = treeNode.id;
        pgmname = treeNode.name;
        nodeuid = treeNode.tId;

        if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
            zTree.cancelSelectedNode();
            //showRMenu("root", event.clientX - $('#clickevt').offset().left, event.clientY - $('#clickevt').offset().top);
			showRMenu("root", event.pageX, event.pageY);
        } else if (treeNode && !treeNode.noR) {
            zTree.selectNode(treeNode);
            //showRMenu("node", event.clientX - $('#clickevt').offset().left, event.clientY - $('#clickevt').offset().top);
			showRMenu("node", event.pageX, event.pageY);
        }

    }
    function getFont(treeId, node) {
            return node.font ? node.font : {};
    }

    function showRMenu(type, x, y) {

        $("#m_run").hide();
        $("#m_out").hide();
        $("#m_log").hide();
        $("#m_val").hide();
        $("#m_up").hide();
        $("#m_dl").hide();
        $("#m_edit").hide();
        $("#m_editp").hide();
        $("#m_run_sec").hide();
        $("#m_sec").hide();
        $("#m_sub_sec").hide();
        $("#m_entry1").hide();
        $("#m_entry2").hide();
        $("#m_move").hide();
        $("#m_update_sechead").hide();
        $("#m_del_sec").hide();
        $("#m_out").hide();
        $("#m_log").hide();
        $("#m_pdf").hide();
        $("#m_rtf").hide();
        $("#m_phist").hide();
        $("#m_del_ent").hide();

        var data = {
            "pgmid": pgmid,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/chk_sec_node.php",
            data: data,
            success: function (response) {
                if (response.seclen != 0) {
                    $("#m_run").hide();
                    $("#m_out").hide();
                    $("#m_log").hide();
                    $("#m_val").hide();
                    $("#m_up").hide();
                    $("#m_dl").hide();
                    $("#m_edit").hide();
                    $("#m_editp").hide();
                    $("#m_run_sec").show();
                    $("#m_sec").show();
                    $("#m_sub_sec").show();
                    $("#m_entry2").show();
                    $("#m_move").hide();
                    $("#m_update_sechead").show();
                    $("#m_del_sec").show();
                    $("#m_del_ent").hide();
                    $("#m_pdf").show();
                    $("#m_rtf").show();
                    $("#m_phist").hide();
                }
                else {
                    $("#m_run").show();
                    $("#m_out").show();
                    $("#m_log").show();
                    $("#m_val").show();
                    $("#m_up").show();
                    $("#m_dl").show();
                    $("#m_edit").show();
                    $("#m_editp").show();
                    $("#m_run_sec").hide();
                    $("#m_sec").hide();
                    $("#m_sub_sec").hide();
                    $("#m_entry1").show();
                    $("#m_move").hide();
                    $("#m_update_sechead").hide();
                    $("#m_del_sec").hide();
                    $("#m_pdf").show();
                    $("#m_rtf").show();
                    $("#m_phist").show();
                    $("#m_del_ent").show();
                }
            }
        });
        if (type == 'root') {
            alert("sss");
        }
        //rMenu.css({"top": y + "px", "left": x + "px", "visibility": "visible"});
		rMenu.css({"top":y-250+"px", "left":x-240+"px", "visibility":"visible"});
        $("body").bind("mousedown", onBodyMouseDown);
    }

    function hideRMenu() {
        if (rMenu) rMenu.css({"visibility": "hidden"});
        $("body").unbind("mousedown", onBodyMouseDown);
    }

    function onBodyMouseDown(event) {
        if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length > 0)) {
            rMenu.css({"visibility": "hidden"});
        }
    }

    var addCount = 1;
    function addTreeNode() {
        hideRMenu();
        var newNode = {name: "newNode " + (addCount++)};
        if (zTree.getSelectedNodes()[0]) {
            newNode.checked = zTree.getSelectedNodes()[0].checked;
            zTree.addNodes(zTree.getSelectedNodes()[0], newNode);
        } else {
            zTree.addNodes(null, newNode);
        }
    }

    function removeTreeNode() {
        hideRMenu();
        var nodes = zTree.getSelectedNodes();
        if (nodes && nodes.length > 0) {
            if (nodes[0].children && nodes[0].children.length > 0) {
                var msg = "If you delete this node will be deleted along with sub-nodes. \n\nPlease confirm!";
                if (confirm(msg) == true) {
                    zTree.removeNode(nodes[0]);
                }
            } else {
                zTree.removeNode(nodes[0]);
            }
        }
    }

    function checkTreeNode(checked) {
        var nodes = zTree.getSelectedNodes();
        if (nodes && nodes.length > 0) {
            zTree.checkNode(nodes[0], checked, true);
        }
        hideRMenu();
    }

    function resetTree() {
        hideRMenu();
        $.fn.zTree.init($("#treeDemo"), setting, zNodes);
    }

    function alr(treeId, treeNode, clickFlag) {
        alert("table id " + pgmid);
    }

    var log, className = "dark";
    function beforeClick(treeId, treeNode) {

        //console.log(treeNode); // Logs output to dev tools console.
        if (treeNode.isParent) {
            return true;
        } else {
            //alert("This Demo is used to test the parent node expand / collapse...\n\nGo to click parent node... ");
            return true;
        }
    }
    function beforeCollapse(treeId, treeNode) {
        className = (className === "dark" ? "" : "dark");
        showLog("[ " + getTime() + " beforeCollapse ]  " + treeNode.name);
        return (treeNode.collapse !== false);
    }
    function onCollapse(event, treeId, treeNode) {
        showLog("[ " + getTime() + " onCollapse ]  " + treeNode.name);
    }
    function beforeExpand(treeId, treeNode) {
        className = (className === "dark" ? "" : "dark");
        showLog("[ " + getTime() + " beforeExpand ]  " + treeNode.name);
        return (treeNode.expand !== false);
    }
    function onExpand(event, treeId, treeNode) {
        showLog("[ " + getTime() + " onExpand ]  " + treeNode.name);
    }
    function showLog(str) {
        //console.log(str);
    }

    function getTime() {
        var now = new Date(),
            h = now.getHours(),
            m = now.getMinutes(),
            s = now.getSeconds(),
            ms = now.getMilliseconds();
        return (h + ":" + m + ":" + s + " " + ms);
    }

    function expandNode(e) {
        var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
            type = e.data.type,
            nodes = zTree.getSelectedNodes();
        allnodes = zTree.getNodes();
        secnodes = allnodes[0].children;
        //console.log(allnodes);
        // console.log(allnodes[0].children);
        // console.log(nodes);
        // if (type.indexOf("All")<0 && nodes.length == 0) {
        //     alert("Please select one parent node at first...");
        // }

        if (type == "expandAll") {
            zTree.expandAll(true);
        } else if (type == "collapseAll") {
            zTree.expandAll(false);
        } else {
            var callbackFlag = true;
            for (var i = 0, l = secnodes.length; i < l; i++) {
                console.log(nodes.length);
                zTree.setting.view.fontCss = {};
                if (type == "expand") {
                    zTree.expandNode(secnodes[i], true, null, null, callbackFlag);
                } else if (type == "collapse") {
                    zTree.expandNode(secnodes[i], false, null, null, callbackFlag);
                } else if (type == "toggle") {
                    zTree.expandNode(nodes[i], null, null, null, callbackFlag);
                } else if (type == "expandSon") {
                    zTree.expandNode(nodes[i], true, true, null, callbackFlag);
                } else if (type == "collapseSon") {
                    zTree.expandNode(nodes[i], false, true, null, callbackFlag);
                }
            }
        }
    }


    var zTree, rMenu;
    $(document).ready(function () {
        $.fn.zTree.init($("#treeDemo"), setting, zNodes);
        zTree = $.fn.zTree.getZTreeObj("treeDemo");
        rMenu = $("#rMenu");
        $("#expandAllBtn").bind("click", {type: "expandAll"}, expandNode);
        $("#collapseAllBtn").bind("click", {type: "collapseAll"}, expandNode);
        $("#expandBtn").bind("click", {type: "expand"}, expandNode);
        $("#collapseBtn").bind("click", {type: "collapse"}, expandNode);

    });

    var someObject = {
        'part1': {
            'name': 'Part 1',
            'size': '20',
            'qty': '50'
        },
        'part2': {
            'name': 'Part 2',
            'size': '15',
            'qty': '60'
        },
        'part3': [
            {
                'name': 'Part 3A',
                'size': '10',
                'qty': '20'
            }, {
                'name': 'Part 3B',
                'size': '5',
                'qty': '20'
            }, {
                'name': 'Part 3C',
                'size': '7.5',
                'qty': '20'
            }
        ]
    };

    Object.byString = function (o, s) {
        s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
        s = s.replace(/^\./, '');           // strip a leading dot
        var a = s.split('.');
        for (var i = 0, n = a.length; i < n; ++i) {
            var k = a[i];
            if (k in o) {
                o = o[k];
            } else {
                return;
            }
        }
        return o;
    }

    // console.log(someObject);
    // console.log(Object.byString(someObject, 'part1.name'));
    // console.log(Object.byString(someObject, 'part2.qty'));
    // console.log(Object.byString(someObject, 'part3[0].name'));
    ////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////Custom functions////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////

    //program run//
    var epgmstat;
    $("#loading").hide();
    function runpgm(form, opx) {
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        //alert(strUser);
        if (opx == 3) {
            var pid = [];

            if (form.outsec == undefined) {
                var pglist = pgmid;
            }
            else {
                var outsec = form.outsec;

                for (var i = 0, iLen = outsec.length; i < iLen; i++) {
                    if (outsec[i].checked) {
                        pid.push(outsec[i].value);
                    }
                }
                var pglist = pid.join([separator = ',']);
            }
            console.log(pglist);
            var data = {
                "pgmid": pglist,
                "dc_val": dc_val,
                "dc_text": dc_text,
                "op": opx
            };
            $('#pgmld').modal('show');
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ajax/program_run.php",
                data: data,
                success: function (response) {
                    var txt = "";
                    for (i = 0; i < response.length; i++) {
                        pgmerr = response[i].err;
                        pgmwarn = response[i].warn;
                        pgmname = response[i].title;
                        txt = txt + pgmname + "\nERROR: " + pgmerr + "\nWARNING: " + pgmwarn + "\n\n";


                        document.getElementById("nopgm").innerHTML = response[i].nopgm;
                        document.getElementById("indevpgm").innerHTML = response[i].indevpgm;
                        document.getElementById("tvalpgm").innerHTML = response[i].tvalpgm;
                        document.getElementById("valpgm").innerHTML = response[i].valpgm;
                        document.getElementById("nout").innerHTML = response[i].nout;
                        document.getElementById("oout").innerHTML = response[i].oout;
                        document.getElementById("cout").innerHTML = response[i].cout;
                        var pgmstat = response[i].pgmstat;
                        var outstat = response[i].outstat;

                        var nofo = (response[i].dtreeId.split("#").length - 1);
                        var res = response[i].dtreeId.split("#");
                        console.log(res);
                        res.forEach(function (e, k) {
                            // if (k != 0){
                            console.log('treeDemo_' + e + '_ico');
                            e2 = $('#treeDemo_' + e + '_ico');
                            // var element = document.getElementById('treeDemo_'+e+'_ico');

                            // console.log(element);
                            // console.log(e2);
                            if (e !== '0' && e2 != null) {
                                console.log(e, e2);
                                e2.css('background', 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat');
                                // e2.style.background = 'url("./stree/metroStyle/img/state_'+pgmstat+'_'+outstat+'.png") 0px 0 no-repeat';
                            }

                        });
                        // for (var j = 1; j <= nofo; j++) {
                        //     console.log('treeDemo_'+res[j]+'_ico');};
                    }
                    $('#pgmld').modal('hide');
                    alert(txt);
                }

            });
        }
        else if (opx == 4) {
            var pid = [];

            if (form.outsec == undefined) {
                var pglist = pgmid;
            }
            else {
                var outsec = form.outsec;

                for (var i = 0, iLen = outsec.length; i < iLen; i++) {
                    if (outsec[i].checked) {
                        pid.push(outsec[i].value);
                    }
                }
                var pglist = pid.join([separator = ',']);
            }
            console.log(pglist);
            var data = {
                "pgmid": pglist,
                "dc_val": dc_val,
                "dc_text": dc_text,
                "op": opx - 1
            };
            $('#pgmld').modal('show');
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ajax/program_run.php",
                data: data,
                success: function (response) {
                    var txt = "";
                    for (i = 0; i < response.length; i++) {
                        pgmerr = response[i].err;
                        pgmwarn = response[i].warn;
                        pgmname = response[i].title;
                        txt = txt + pgmname + "\nERROR: " + pgmerr + "\nWARNING: " + pgmwarn + "\n\n";


                        // document.getElementById("nopgm").innerHTML = response[i].nopgm;
                        // document.getElementById("indevpgm").innerHTML = response[i].indevpgm;
                        // document.getElementById("tvalpgm").innerHTML = response[i].tvalpgm;
                        // document.getElementById("valpgm").innerHTML = response[i].valpgm;
                        // document.getElementById("nout").innerHTML = response[i].nout;
                        // document.getElementById("oout").innerHTML = response[i].oout;
                        // document.getElementById("cout").innerHTML = response[i].cout;
                        var pgmstat = response[i].pgmstat;
                        var outstat = response[i].outstat;

                        var nofo = (response[i].dtreeId.split("#").length - 1);
                        var res = response[i].dtreeId.split("#");
                        console.log(res);
                        // res.forEach( function(e,k){
                        //      // if (k != 0){
                        //     console.log('treeDemo_'+e+'_ico');
                        //     e2= $('#treeDemo_'+e+'_ico');

                        //     if(e !== '0' && e2 != null){
                        //         console.log(e,e2);
                        //         e2.css('background','url("./stree/metroStyle/img/state_'+pgmstat+'_'+outstat+'.png") 0px 0 no-repeat');

                        //     }

                        // });
                    }
                    $('#pgmld').modal('hide');
                    alert(txt);
                }

            });
        }
        else if (opx == 2) {
            // alert(opx);
            var pid = [];
            var pgnam = form.pgnam;

            for (var i = 0, iLen = pgnam.length; i < iLen; i++) {
                if (pgnam[i].checked) {
                    pid.push(pgnam[i].value);
                }
            }
            var pglist = pid.join([separator = ',']);
            //alert(pglist);
            var data = {
                "pgmid": pglist,
                "dc_val": dc_val,
                "dc_text": dc_text,
                "op": opx
            };
            $('#pgmld').modal('show');
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ajax/program_run.php",
                data: data,
                success: function (response) {
                    var txt = "";
                    for (i = 0; i < response.length; i++) {
                        pgmerr = response[i].err;
                        pgmwarn = response[i].warn;
                        pgmname = response[i].title;
                        txt = txt + pgmname + "\nERROR: " + pgmerr + "\nWARNING: " + pgmwarn + "\n\n";
                    }
                    $('#pgmld').modal('hide');
                    alert(txt);
                }
            });
        }
        else {
            // alert(opx);
            var data = {
                "pgmid": pgmid,
                "dc_val": 0,
                "dc_text": "Current",
                "pgmname": pgmname
            };
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ajax/pgm_chk.php",
                data: data,
                success: function (response) {
                    if (response.pgmstat == 0) {
                        alert('Warning:There is no program for this entry. Please upload the program.');
                    }
                    else if (response.pgmstat > 0) {

                        var data = {
                            "pgmid": pgmid,
                            "pgmname": pgmname,
                            "dc_val": dc_val,
                            "dc_text": dc_text,
                            "op": opx
                        };
                        $('#pgmld').modal('show');
                        //$("#loading").show();
                        $.ajax({
                            type: "POST",
                            dataType: "json",
                            url: "ajax/program_run.php",
                            data: data,
                            success: function (response) {
                                pgmerr = response.err;
                                pgmwarn = response.warn;

                                //$("#loading").hide();
                                $('#pgmld').modal('hide');
                                alert(pgmname + "\nERROR: " + pgmerr + "\nWARNING: " + pgmwarn);

                                document.getElementById("nopgm").innerHTML = response.nopgm;
                                document.getElementById("indevpgm").innerHTML = response.indevpgm;
                                document.getElementById("tvalpgm").innerHTML = response.tvalpgm;
                                document.getElementById("valpgm").innerHTML = response.valpgm;
                                document.getElementById("nout").innerHTML = response.nout;
                                document.getElementById("oout").innerHTML = response.oout;
                                document.getElementById("cout").innerHTML = response.cout;
                                var pgmstat = response.pgmstat;
                                var outstat = response.outstat;

                                var nofo = (response.dtreeId.split("#").length - 1);
                                var res = response.dtreeId.split("#");
                                console.log(res);

                                for (var i = 1; i <= nofo; i++) {
                                    var element = document.getElementById('treeDemo_' + res[i] + '_ico');
                                    //console.log(element);
                                    element.style.background = 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat';
                                }
                                ;


                                //console.log(response.dtreeId, nofo, res, res[0], res[1]);
                                //resetTree();
                                //location.reload();
                            }
                        });
                    }
                    else {
                        alert('Sorry there was an error');
                    }
                    epgmstat = response.pgmstat;
                    console.log(epgmstat);
                }
            });
        }
        hideRMenu();
    }

    function run_autosas() {
        //console.log("inside javascript now ");
        //$('#pgmld').modal('show');
        //$("#loading").show();
        var table = document.getElementById("ext_pgm_tab");
        var delCount = table.rows.length;
        //console.log("total row:"+delCount);
        if (delCount > 1) {
            for (var i = delCount - 1; i > 0; i--) {
                //console.log("i="+i);
                table.deleteRow(i);
            }
        }
        //$('#existing_pgm_modal').modal('show');

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/check_existing_pgm_in_pgmFolder.php",
            success: function (response) {


                var total_lenght = response.length;
                var status = "";
                if (total_lenght > 0) {

                    $('#existing_pgm_modal').modal('show');
                    for (var i = 0; i < total_lenght; i++) {
                        var table = document.getElementById("ext_pgm_tab");
                        var rowCount = table.rows.length;
                        var row = table.insertRow(rowCount);
                        var pgm_name = response[i].pgmname;

                        // var cell0 = row.insertCell(0);
                        // cell0.innerHTML = response[i].pgmname;

                        //document.getElementById("phname").innerHTML="Program History: "+response[i].pgmname;


                        var cell0 = row.insertCell(0);
                        cell0.innerHTML = response[i].pgmname;

                        var col = "";
                        if (response[i].pgmstat == "0") {
                            col = "default";
                            status = "No program";
                        }
                        if (response[i].pgmstat == "1") {
                            col = "warning";
                            status = "In Development";
                        }
                        if (response[i].pgmstat == "2") {
                            col = "danger";
                            status = "To Be validated";
                        }
                        if (response[i].pgmstat == "3") {
                            col = "success";
                            status = "Validated";
                        }

//                    if(response[i].pgmstat=="To Be validated")  col="danger";
//                    if(response[i].pgmstat=="Validated") col="success";
//                    if(response[i].pgmstat=="In Development") col="warning";

                        var cell1 = row.insertCell(1);
                        cell1.innerHTML = "<span class='label label-" + col + "'>" + status + "</span>";

//                    var cell2 = row.insertCell(2);
//                    cell2.innerHTML = response[i].comment;


//                    var cell3 = row.insertCell(3);
//                    cell3.innerHTML = "<span class='label label-"+col+"'>"+response[i].status+"</span>";

                        // var cell5 = row.insertCell(5);
                        // cell5.innerHTML = "<button type='button' class='btn btn-primary btn-block btn-flat' onclick='sas_backup_dl(11,11);'>use</button>";

                        var cell2 = row.insertCell(2);
                        cell2.innerHTML = "<input type='checkbox' id='ext_pgm' name='ext_pgm' value=" + pgm_name + " onchange='update_pgm_gen(this);'>";

                    }
                }
                else {
                    $('#noexisting_pgm_modal').modal('show');
                }
            }
        });

    }

    //reload the tree and legend//
    function reload_tree() {
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        //console.log("inside javascript");
        document.getElementById('lodspin').style.visibility = 'visible';

        var data = {
            "x_checked": "null",
            "dc_val": dc_val,
            "dc_text": dc_text
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/tree_reload.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                for (i = 0; i < response.length; i++) {

                    document.getElementById("nopgm").innerHTML = response[i].nopgm;
                    document.getElementById("indevpgm").innerHTML = response[i].indevpgm;
                    document.getElementById("tvalpgm").innerHTML = response[i].tvalpgm;
                    document.getElementById("valpgm").innerHTML = response[i].valpgm;
                    document.getElementById("nout").innerHTML = response[i].nout;
                    document.getElementById("oout").innerHTML = response[i].oout;
                    document.getElementById("cout").innerHTML = response[i].cout;
                    var pgmstat = response[i].pgmstat;
                    var outstat = response[i].outstat;

                    var nofo = (response[i].dtreeId.split("#").length - 1);
                    var res = response[i].dtreeId.split("#");
                    console.log(res);
                    res.forEach(function (e, k) {
                        // if (k != 0){
                        console.log('treeDemo_' + e + '_ico');
                        e2 = $('#treeDemo_' + e + '_ico');
                        // var element = document.getElementById('treeDemo_'+e+'_ico');

                        // console.log(element);
                        // console.log(e2);
                        if (e !== '0' && e2 != null) {
                            console.log(e, e2);
                            e2.css('background', 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat');
                            // e2.style.background = 'url("./stree/metroStyle/img/state_'+pgmstat+'_'+outstat+'.png") 0px 0 no-repeat';
                        }

                    });
                    // for (var j = 1; j <= nofo; j++) {
                    //     console.log('treeDemo_'+res[j]+'_ico');};
                }
                document.getElementById('lodspin').style.visibility = 'hidden';
                //alert(txt);
            }
        });


    }

    //update pgm gengen//
    function update_pgm_gen(x) {

        var pgm_sas = x.value;
        var x_checked = x.checked;
        //console.log(x.checked);

        var data = {
            "x_checked": x_checked,
            "pgmname": pgm_sas
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk_v_status.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {

            }
        });


    }

    //update pgm gengen//
    function autogen_pgm_gen() {
        $('#existing_pgm_modal').modal('hide');
        $('#noexisting_pgm_modal').modal('hide');
        $('#pgmld').modal('show');
        //console.log(x.checked);

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/autogen_program_run.php",
            success: function (response) {
//           //$("#loading").hide();
                $('#pgmld').modal('hide');

                if (response[0].success == 1) {
                    for (i = 0; i < response.length; i++) {

                        document.getElementById("nopgm").innerHTML = response[i].nopgm;
                        document.getElementById("indevpgm").innerHTML = response[i].indevpgm;
                        document.getElementById("tvalpgm").innerHTML = response[i].tvalpgm;
                        document.getElementById("valpgm").innerHTML = response[i].valpgm;
                        document.getElementById("nout").innerHTML = response[i].nout;
                        document.getElementById("oout").innerHTML = response[i].oout;
                        document.getElementById("cout").innerHTML = response[i].cout;
                        var pgmstat = response[i].pgmstat;
                        var outstat = response[i].outstat;

                        var nofo = (response[i].dtreeId.split("#").length - 1);
                        var res = response[i].dtreeId.split("#");
                        console.log(res);
                        res.forEach(function (e, k) {
                            // if (k != 0){
                            console.log('treeDemo_' + e + '_ico');
                            e2 = $('#treeDemo_' + e + '_ico');
                            // var element = document.getElementById('treeDemo_'+e+'_ico');

                            // console.log(element);
                            // console.log(e2);
                            if (e !== '0' && e2 != null) {
                                console.log(e, e2);
                                e2.css('background', 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat');
                                // e2.style.background = 'url("./stree/metroStyle/img/state_'+pgmstat+'_'+outstat+'.png") 0px 0 no-repeat';
                            }

                        });
                        // for (var j = 1; j <= nofo; j++) {
                        //     console.log('treeDemo_'+res[j]+'_ico');};
                        $.notify(response[i].pgmname + ' created. ', {
                            allow_dismiss: false,
                            type: 'success',
                            placement: {
                                from: 'top',
                                align: 'right'
                            }
                        });
                    }

                    console.log(response.dtreeId, nofo, res, res[0], res[1]);
                } else {
                    // alert("All programs already exist!!");
                    $.notify('All programs already exist,No new program created ', {
                        allow_dismiss: false,
                        type: 'danger',
                        placement: {
                            from: 'top',
                            align: 'right'
                        }
                    });
                }
                //alert(pgmname+"\nERROR: "+pgmerr+"\nWARNING: "+pgmwarn);


            }
        });


    }


    //show log//
    function showout() {

        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk.php",
            data: data,
            success: function (response) {
                if (response.outstat == 0 && response.snap == 1) {

                    alert('Warning:There is no output for this entry. Please run the program and create output.');

                }
                if (response.outstat == 0 && response.snap == 0) {

                    alert('Warning:There is no output for this entry under this Snapshot. Please run the program and create output for this snapshot.');

                }
                else if (response.outstat > 0) {
                    var data = {
                        "q": pgmid,
                        "d": dc_val
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/view_out.php",
                        data: data,
                        success: function (response) {
                            //console.log(response.OutputLocation);
                            window.open('read_file.php?file='+response.OutputLocation, 'newwindow', 'width=1200px, height=800px'); return false;
                        }
                    });
                   //window.open("ajax/view_out.php?q=" + pgmid + "&d="+dc_val, "", "scrollbars=yes, resizable=yes, width=1200, height=900");
                    //window.open('read_file.php?file=D:\study\\study_001/lst//disp_t1.lst', 'newwindow', 'width=1200px, height=800px'); return false;
                }
            }
        });
        hideRMenu();
    }


    //show log//
    function showlog() {

        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;


        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                if (response.outstat == 0 && response.snap == 1) {
                    alert('Warning:There is no log file for this entry. Please run the program and create log first.');
                }
                if (response.outstat == 0 && response.snap == 0) {
                    alert('Warning:There is no log file for this entry under this snapshot. Please run the program under this snapshot and create log.');
                }
                else if ((response.outstat > 0) || (response.pgmstat > 0)) {
                    window.open("ajax/view_log.php?q=" + pgmid + "&d="+dc_val, "", "scrollbars=yes, resizable=yes, width=1200, height=900");
                }
            }
        });

        // window.open("http://localhost/toc/ajax/view_log.php?q="+pgmid,"", "scrollbars=yes, resizable=yes, width=1200, height=900");
        hideRMenu();
    }


 
    //converter base64 to utf8 charecter set
    function b64_to_utf8(str) {
        return decodeURIComponent(escape(window.atob(str)));
    }

    //program validation
    function validation() {
        document.getElementById("pgnam").innerHTML = pgmname;

        var x = document.getElementById("sid");
        x.value = pgmid;

        $('#pfbox1').hide();
        $('#pfbox2').hide();
        $('#pfbox3').hide();

        var data = {
            "pgmid": pgmid,
            "dc_val": 0,
            "dc_text": "Current",
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                if (response.pgmstat > 0 && response.outstat == 0) {
                    alert('Warning:Program is not yet run. Please run the program first.');

                }
                else if (response.pgmstat == 0 && response.outstat >= 0) {
                    alert('Warning:There is no program for this entry. Please Upload the program first.');
                }
                else {
                    $('#lod').modal('show');
                }
            }
        });

        hideRMenu();
    }


    //keeping program validation history
    function program_history() {

        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        //document.getElementById("pid_hist").innerHTML=pgmid;
        $('#pid_hist').val(pgmid);

        var table = document.getElementById("hist_tab");
        var delCount = table.rows.length;
        //console.log("total row:"+delCount);
        if (delCount > 1) {
            for (var i = delCount - 1; i > 0; i--) {
                //console.log("i="+i);
                table.deleteRow(i);
            }
        }

        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                // if (response.pgmstat > 0 && response.outstat == 0) {
                //     alert('Warning:Program is not yet run. Please run the program first.');
                // }
                if (response.pgmstat == 0 && response.outstat >= 0) {
                    alert('Warning:There is no program for this entry. Please Upload the program first.');
                }
                else {
                    $('#phist_modal').modal('show');
                    var data = {
                        "pgmid": pgmid,
                        "pgmname": pgmname
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/program_history.php",
                        data: data,
                        success: function (response) {
                            var total_lenght = response.length;
                            if (total_lenght > 0) {
                                for (var i = 0; i < total_lenght; i++) {
                                    var table = document.getElementById("hist_tab");
                                    var rowCount = table.rows.length;
                                    var row = table.insertRow(rowCount);

                                    // var cell0 = row.insertCell(0);
                                    // cell0.innerHTML = response[i].pgmname;

                                    document.getElementById("phname").innerHTML = "Program History: " + response[i].pgmname;


                                    var cell0 = row.insertCell(0);
                                    cell0.innerHTML = response[i].event_date;

                                    var cell1 = row.insertCell(1);
                                    cell1.innerHTML = response[i].username;

                                    var cell2 = row.insertCell(2);
                                    cell2.innerHTML = response[i].comment;

                                    var col = "";
                                    if (response[i].status == "To Be validated")  col = "danger";
                                    if (response[i].status == "Validated") col = "success";
                                    if (response[i].status == "In Development") col = "warning";

                                    var cell3 = row.insertCell(3);
                                    cell3.innerHTML = "<span class='label label-" + col + "'>" + response[i].status + "</span>";

                                    // var cell5 = row.insertCell(5);
                                    // cell5.innerHTML = "<button type='button' class='btn btn-primary btn-block btn-flat' onclick='sas_backup_dl(11,11);'>use</button>";

                                    var cell4 = row.insertCell(4);
                                    cell4.innerHTML = "<button type='button' class='btn btn-primary btn-block btn-flat' onclick='sas_backup_dl(" + response[i].study_id + "," + response[i].id + ");'>download</button>";

                                }
                            }
                        }
                    });
                }
            }
        });

        hideRMenu();
    }


    //keeping common  program validation history
    function cprogram_history(x) {
        cp_name = x;
        //document.getElementById("pid_hist").innerHTML=pgmid;
        //$('#pid_hist').val(pgmid);

        var table = document.getElementById("cp_hist_tab");
        var delCount = table.rows.length;
        //console.log("total row:"+delCount);
        if (delCount > 1) {
            for (var i = delCount - 1; i > 0; i--) {
                //console.log("i="+i);
                table.deleteRow(i);
            }
        }

        $('#cphist_modal').modal('show');
        var data = {
            "pgmname": cp_name
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/cprogram_history.php",
            data: data,
            success: function (response) {
                var total_lenght = response.length;
                if (total_lenght > 0) {
                    for (var i = 0; i < total_lenght; i++) {
                        var table = document.getElementById("cp_hist_tab");
                        var rowCount = table.rows.length;
                        var row = table.insertRow(rowCount);

                        // var cell0 = row.insertCell(0);
                        // cell0.innerHTML = response[i].pgmname;

                        document.getElementById("cphname").innerHTML = "Program History: " + response[i].pgmname;


                        var cell0 = row.insertCell(0);
                        cell0.innerHTML = response[i].event_date;

                        var cell1 = row.insertCell(1);
                        cell1.innerHTML = response[i].username;

                        var cell2 = row.insertCell(2);
                        cell2.innerHTML = response[i].comment;

                        var col = "";
                        if (response[i].status == "To Be validated")  col = "danger";
                        if (response[i].status == "Validated") col = "success";
                        if (response[i].status == "In Development") col = "warning";

                        var cell3 = row.insertCell(3);
                        cell3.innerHTML = "<span class='label label-" + col + "'>" + response[i].status + "</span>";

                        // var cell5 = row.insertCell(5);
                        // cell5.innerHTML = "<button type='button' class='btn btn-primary btn-block btn-flat' onclick='sas_backup_dl(11,11);'>use</button>";

                        var cell4 = row.insertCell(4);
                        cell4.innerHTML = "<button type='button' class='btn btn-primary btn-block btn-flat' onclick='cp_sas_backup_dl(" + response[i].study_id + "," + response[i].id + ");'>download</button>";

                    }
                }
            }
        });


    }


    function uploadp() {
        document.getElementById("pgnam_up").innerHTML = pgmname;

        var x = document.getElementById("dl_sid");
        x.value = pgmid;

        hideRMenu();
    }


    // function uploadcp() {
    //     document.getElementById("pgnam_up").innerHTML=pgmname;

    //     var x = document.getElementById("dl_sid");
    //     x.value=pgmid;

    //     hideRMenu();
    // }


    function downloadp() {
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                if (response.pgmstat == 0) {
                    alert('Warning:There is no program for this entry. Please Upload the program first.');
                }
                else {
                    var data = {
                        "pgmid": pgmid,
                        "dc_val": dc_val,
                        "dc_text": dc_text,
                        "pgmname": pgmname
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/program_download.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (response) {
                            // Download(response.path);
                            console.log(response.path);
                            open(response.path);

                        }
                    });
                }
            }
        });
        hideRMenu();
    }


   //export toc
    function export_toc() {
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/export_toc.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                // Download(response.path);
                //console.log(response.path);
                open(response.path);

            }
        });


    }
    //edit programs//
    function editpgm() {
        
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;
        var x = document.getElementById("editpgm_sid");
        x.value = pgmid;

        //window.open("http://localhost/toc/ajax/editpgm.php?q="+pgmid,"", "scrollbars=yes, resizable=yes, width=1200, height=900");
        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                if (response.pgmstat == 0) {
                    alert('Warning:There is no program for this entry. Please Upload the program first.');
                }
                else {
                    var data = {
                        "pgmid": pgmid,
                        "pgmname": pgmname
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/editpgm.php",
                        data: data,
                        success: function (response) {
                            //console.log(response.path
                            open(response.path);
                        }
                    });
                }
            }
        });
        hideRMenu();
    }

    //create pdf on context menu
    function pdf_create() {
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/chk_sec_node.php",
            data: data,
            success: function (response) {
                if (response.seclen != 0) {
                    var data = {
                        "grp": pgmid,
                        "dc_val": dc_val,
                        "dc_text": dc_text,
                        "op": "5"
                    };

                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/create_pdf.php",
                        data: data,
                        success: function (response) {
                            open(response.dlink);
                        }
                    });
                }
                else {
                    var data = {
                        "pgmid": pgmid,
                        "dc_val": dc_val,
                        "dc_text": dc_text,
                        "pgmname": pgmname
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (response) {
                            if (response.outstat == 0) {
                                alert('Warning:No output available. Please run the program first.');
                            }
                            else {
                                var data = {
                                    "grp": pgmid,
                                    "dc_val": dc_val,
                                    "dc_text": dc_text,
                                    "op": "4"
                                };
                                $.ajax({
                                    type: "POST",
                                    dataType: "json",
                                    url: "ajax/create_pdf.php",
                                    data: data,
                                    success: function (response) {
                                        open(response.dlink);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        hideRMenu();
    }


    //create rtf on context menu
    function rtf_create() {
        var e = document.getElementById("dc_selector");
        var dc_val = e.options[e.selectedIndex].value;
        var e = document.getElementById("dc_selector");
        var dc_text = e.options[e.selectedIndex].text;

        var data = {
            "pgmid": pgmid,
            "dc_val": dc_val,
            "dc_text": dc_text,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/chk_sec_node.php",
            data: data,
            success: function (response) {
                if (response.seclen != 0) {
                    var data = {
                        "grp": pgmid,
                        "op": "5"
                    };
                    $('#pgmld').modal('show');
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/create_rtf.php",
                        data: data,
                        success: function (response) {
                            $('#pgmld').modal('hide');
                            open(response.dlink);
                        }
                    });
                }
                else {
                    var data = {
                        "pgmid": pgmid,
                        "dc_val": dc_val,
                        "dc_text": dc_text,
                        "pgmname": pgmname
                    };
                    $.ajax({
                        type: "POST",
                        dataType: "json",
                        url: "ajax/pgm_chk.php", //Relative or absolute path to response.php file
                        data: data,
                        success: function (response) {
                            if (response.outstat == 0) {
                                alert('Warning:No output available. Please run the program first.');
                            }
                            else {
                                var data = {
                                    "grp": pgmid,
                                    "op": "4"
                                };
                                $('#pgmld').modal('show');
                                $.ajax({
                                    type: "POST",
                                    dataType: "json",
                                    url: "ajax/create_rtf.php",
                                    data: data,
                                    success: function (response) {
                                        $('#pgmld').modal('hide');
                                        open(response.dlink);
                                    }
                                });
                            }
                        }
                    });
                }
            }
        });
        hideRMenu();
    }

    // context menu for manage common programs
    (function ($, window) {


        $.fn.contextMenu = function (settings) {

            return this.each(function () {

                // Open context menu
                $(this).on("contextmenu", function (e) {
                    // return native menu if pressing control
                    if (e.ctrlKey) return;

                    //open menu
                    $(settings.menuSelector)
                        .data("invokedOn", $(e.target))
                        .show()
                        .css({
                            position: "absolute",
                            left: getMenuPosition(e.clientX - 522, 'width', 'scrollLeft'),
                            top: getMenuPosition(e.clientY - 240, 'height', 'scrollTop')
                        })
                        .off('click')
                        .on('click', function (e) {
                            $(this).hide();

                            var $invokedOn = $(this).data("invokedOn");
                            var $selectedMenu = $(e.target);

                            settings.menuSelected.call(this, $invokedOn, $selectedMenu);
                        });

                    return false;
                });

                //make sure menu closes on any click
                $(document).click(function () {
                    $(settings.menuSelector).hide();
                });
            });

            function getMenuPosition(mouse, direction, scrollDir) {
                var win = $(window)[direction](),
                    scroll = $(window)[scrollDir](),
                    menu = $(settings.menuSelector)[direction](),
                    position = mouse + scroll;

                // opening menu would pass the side of the page
                if (mouse + menu > win && menu < mouse)
                    position -= menu;

                return position;
            }

        };
    })(jQuery, window);

    //manage common program edit and download run
    $("#myTable td.pg").contextMenu({
        menuSelector: "#contextMenu",
        menuSelected: function (invokedOn, selectedMenu) {
            var msg = "You selected the menu item '" + selectedMenu.text() +
                "' on the value '" + invokedOn.text() + "'";

            if (selectedMenu.text() == "Edit Program") {
                var x = document.getElementById("editcpgm_sid");
                x.value = invokedOn.text();

                var data = {
                    "pgmname": invokedOn.text()
                };


                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "ajax/editcomonpgm.php",
                    data: data,
                    success: function (response) {
                        open(response.path);
                        // $('#prgm').text(b64_to_utf8(response.pgmtext));
                        // Prism.highlightElement($('#prgm')[0]);
                        // $('#prgm1').text(b64_to_utf8(response.pgmtext));
                    }
                });

            }
            if (selectedMenu.text() == "Download") {

                var data = {
                    "pgmname": invokedOn.text()
                };
                $.ajax({
                    type: "POST",
                    dataType: "json",
                    url: "ajax/dlcomonpgm.php",
                    data: data,
                    success: function (response) {
                        open(response.dlink);
                    }
                });
            }

            if (selectedMenu.text() == "Validation Status") {

                var data = {
                    "pgmname": invokedOn.text()
                };
                //console.log(data.pgmname);
                //Common program validation

                document.getElementById("cpgnam").innerHTML = "Program Name : " + data.pgmname;
                var x = document.getElementById("cp_name");
                x.value = data.pgmname;

                $('#cpfbox1').hide();
                $('#cpfbox2').hide();
                $('#cpfbox3').hide();

                $('#cp_val').modal('show');


            }

            if (selectedMenu.text() == "Program History") {

                var data = {
                    "pgmname": invokedOn.text()
                };
                console.log(data.pgmname);
                //Common program validation
                x = data.pgmname;
                cprogram_history(x);

            }
            // alert(msg);
        }
    });

    //create backup when download
    function sas_backup_dl(sn, df) {
        //alert(sn);
        var data = {
            "sid": sn,
            "rid": df
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/back_pgm_dl.php",
            data: data,
            success: function (response) {
                sname = response[0].sname;
                pname = response[1].pname;
                //console.log(response[1].pname);
                //open("file:////server/SC_Projects/TOC/study/"+sname+"/backup/"+pname+"");
                //open("study/"+sname+"/backup/"+pname+"");download_toc
                open("temp/sas/" + pname);
            }
        });
    }

    //create backup when download
    function cp_sas_backup_dl(sn, df) {
        //alert(sn);
        var data = {
            "sid": sn,
            "rid": df
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/back_cpgm_dl.php",
            data: data,
            success: function (response) {
                sname = response[0].sname;
                pname = response[1].pname;
                //console.log(response[1].pname);
                //open("file:////server/SC_Projects/TOC/study/"+sname+"/backup/"+pname+"");
                //open("study/"+sname+"/backup/"+pname+"");download_toc
                open("temp/sas/" + pname);
            }
        });
    }
    //Create pdf on submit grp and create pdf
    function getCheckboxValues(form) {
        // var values = [];
        var grp = form.grp;


        for (var i = 0, iLen = grp.length; i < iLen; i++) {
            if (grp[i].checked) {
                // values.push(grp[i].value);
                val = grp[i].value;
            }
        }
        var data = {
            "grp": val,
            "op": "1"
        };
        $('#pgmld').modal('show');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_pdf.php",
            data: data,
            success: function (response) {
                $('#pgmld').modal('hide');
                open(response.dlink);
            }
        });
    }

    function getCheckboxValues1(form) {
        var outid = [];
        var outname = form.outname;

        for (var i = 0, iLen = outname.length; i < iLen; i++) {
            if (outname[i].checked) {
                outid.push(outname[i].value);
            }
        }
        var outlist = outid.join([separator = ',']);
        // return values;
        // alert(outlist);
        var data = {
            "grp": outlist,
            "op": "2"
        };
        $('#pgmld').modal('show');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_pdf.php",
            data: data,
            success: function (response) {
                $('#pgmld').modal('hide');
                open(response.dlink);
            }
        });
    }

    function getCheckboxValues2(form) {
        var outsecid = [];
        var outsec = form.outsec;

        for (var i = 0, iLen = outsec.length; i < iLen; i++) {
            if (outsec[i].checked) {
                outsecid.push(outsec[i].value);
            }
        }
        var outseclist = outsecid.join([separator = ',']);
        // return values;
        // alert(outseclist);
        var data = {
            "grp": outseclist,
            "op": "3"
        };
        $('#pgmld').modal('show');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_pdf.php",
            data: data,
            success: function (response) {
                $('#pgmld').modal('hide');
                open(response.dlink);
            }
        });
    }

    //Create rtf on submit grp and create rtff
    function getCheckboxValuesrtf(form) {
        // var values = [];
        var grp = form.grp;


        for (var i = 0, iLen = grp.length; i < iLen; i++) {
            if (grp[i].checked) {
                // values.push(grp[i].value);
                val = grp[i].value;
            }
        }
        var data = {
            "grp": val,
            "op": "1"
        };
        $('#pgmld').modal('show');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_rtf.php",
            data: data,
            success: function (response) {
                $('#pgmld').modal('hide');
                open(response.dlink);
            }
        });
    }

    function getCheckboxValuesrtf1(form) {
        var outid = [];
        var outname = form.outname;

        for (var i = 0, iLen = outname.length; i < iLen; i++) {
            if (outname[i].checked) {
                outid.push(outname[i].value);
            }
        }
        var outlist = outid.join([separator = ',']);
        // return values;
        var data = {
            "grp": outlist,
            "op": "2"
        };
        $('#pgmld').modal('show');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_rtf.php",
            data: data,
            success: function (response) {
                $('#pgmld').modal('hide');
                open(response.dlink);
            }
        });
    }

    function getCheckboxValuesrtf2(form) {
        var outsecid = [];
        var outsec = form.outsec;

        for (var i = 0, iLen = outsec.length; i < iLen; i++) {
            if (outsec[i].checked) {
                outsecid.push(outsec[i].value);
            }
        }
        var outseclist = outsecid.join([separator = ',']);
        // return values;
        // alert(outseclist);
        var data = {
            "grp": outseclist,
            "op": "3"
        };
        $('#pgmld').modal('show');
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_rtf.php",
            data: data,
            success: function (response) {
                $('#pgmld').modal('hide');
                open(response.dlink);
            }
        });
    }

    //validation form show/hide
    function ready_pass(val) {

        //alert(val.value);
        if (val.value == 2) {
            $('#pfbox1').hide();
            $('#pfbox2').hide();
            $('#pfbox3').hide();
        } else if (val.value == 3) {
            $('#pfbox1').show();
            $('#pfbox2').show();
            $('#pfbox3').show();
        }
    }


    //common program validation form show/hide
    function ready_pass2(val) {

        //alert(val.value);
        if (val.value == 2) {
            $('#cpfbox1').hide();
            $('#cpfbox2').hide();
            $('#cpfbox3').hide();
        } else if (val.value == 3) {
            $('#cpfbox1').show();
            $('#cpfbox2').show();
            $('#cpfbox3').show();
        }
    }



    //new entry add functionality
    function ne_pgmnamf(val) {
        var ne_outnum = document.getElementById("ne_outnum");
        var ne_pgtyp = document.getElementById("ne_pgtyp").value;
        ne_outnum.value = ne_pgtyp.substr(0, 1);

        var ne_pid = document.getElementById("ne_row_id");
        var ne_lognam = document.getElementById("ne_lognam");
        ne_lognam.value = val;

        var ne_pgloc = document.getElementById("ne_pgloc");
        ne_pgloc.value = 'pgm/ctr/' + val + '.sas';

        var ne_outnam = document.getElementById("ne_outnam");
        var ne_outnum = document.getElementById("ne_outnum").value;
        ne_outnam.value = val + '_' + ne_outnum.toLowerCase();

        var ne_outloc = document.getElementById("ne_outloc");
        ne_outloc.value = 'lst/' + val + '_' + ne_outnum.toLowerCase() + '.lst';
    }

    function ne_pgtypf(val) {
        var x = document.getElementById("ne_outnum");
        x.value = val.substr(0, 1);

        var ne_outnam = document.getElementById("ne_outnam");
        var ne_outnum = document.getElementById("ne_outnum").value;
        var ne_pgmnam = document.getElementById("ne_pgmnam").value;
        ne_outnam.value = ne_pgmnam + '_' + ne_outnum.toLowerCase();

        var ne_outloc = document.getElementById("ne_outloc");
        ne_outloc.value = 'lst/' + ne_pgmnam + '_' + ne_outnum.toLowerCase() + '.lst';
    }

    function ne_outnumf(val) {

        var ne_outnam = document.getElementById("ne_outnam");
        var ne_pgmnam = document.getElementById("ne_pgmnam").value;
        ne_outnam.value = ne_pgmnam + '_' + val.toLowerCase();

        var ne_outloc = document.getElementById("ne_outloc");
        ne_outloc.value = 'lst/' + ne_pgmnam + '_' + val.toLowerCase() + '.lst';
    }

    function new_sec() {
        var x = document.getElementById("sec_id");
        x.value = pgmid;
        // alert("table id "+pgmid);
        hideRMenu();
    }

    function new_sub_sec() {
        var x = document.getElementById("sub_sec_id");
        x.value = pgmid;
        // alert("table id "+pgmid);
        hideRMenu();
    }

    var etitle, epgmname, eoutname, elogname;
    function update_sechead() {
        document.getElementById("secpgnam1").innerHTML = pgmname;

        var x = document.getElementById("up_sec_id");
        x.value = pgmid;

        var data = {
            "pgmid": pgmid,
            "pgmname": pgmname
        };
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "./ajax/edit_entry.php",
            data: data,
            success: function (response) {
                //   etitle=response.title;
                //   epgmname=response.pgmname;
                //   eoutname=response.outname;
                //   elogname=response.logname;
                //     console.log(etitle);
                // document.getElementById("title").value=etitle;
                // document.getElementById("pgmname").value=epgmname;
                // document.getElementById("outname").value=eoutname;
                // document.getElementById("logname").value=elogname;
                document.getElementById("up_sec_name").value = response.section;
            }
        });
        hideRMenu();
    }

    //delete section and entry
    function del_sec1() {
        var x = document.getElementById("delseccon");
        x.value = pgmname;
        hideRMenu();
    }

    function del_sec2() {

        if (pgmid.length < 3) {
            $('.bs-example-delcon-modal-sm').modal('hide');
            $.notify({
                title: '<strong>Failed to Update!</strong>',
                message: 'You Are not allow to delete the root section'
            }, {
                type: 'danger'
            });
        } else {
            var data = {"pgmid": pgmid};

            $.ajax({
                type: "POST",
                dataType: "json",
                url: "./ajax/delete_section.php",
                data: data,
                success: function (response) {
                    if (response.success == "1") {
                        // alert("Section deleted !!");
                        location.reload();
                        $.notify({
                            title: '<strong>SUCCESS!</strong>',
                            message: 'Delete successful !'
                        }, {
                            type: 'success'
                        });
                    } else {
                        //alert("Failed to delete the Section!!");
                        $('.bs-example-delcon-modal-sm').modal('hide');
                        $.notify({
                            title: '<strong>Failed!</strong>',
                            message: 'Fail to delete!'
                        }, {
                            type: 'danger'
                        });

                    }

                }
            });
        }

    }

    //Download pdf version - all TLF last status
    function report_pdf_all() {

        var data = {
            "pgmid": pgmid
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "./ajax/report_pdf_all.php",
            data: data,
            success: function (response) {

            }
        });

    }

    var files;
    function up_pgm_btn_active() {
        $('#uppgmbtn').removeClass('disabled');
        // Add events
        $('input[type=file]').on('change', prepareUpload);
        // Grab the files and set them to our variable
        function prepareUpload(event) {
            files = event.target.files;
            //console.log('original -- ', files);
        }
    }

    function up_pgm_btn_dactive() {
        $('#uppgmbtn').addClass('disabled');
    }


    function up_cpgm_btn_active() {
        $('#upcpgmbtn').removeClass('disabled');
        console.log("enabled!!");
        // Add events
        $('input[type=file]').on('change', prepareUpload);
        // Grab the files and set them to our variable
        function prepareUpload(event) {
            files = event.target.files;
            console.log('original -- ', files);
        }
    }

    function up_cpgm_btn_dactive() {
        $('#upcpgmbtn').addClass('disabled');
        console.log("disabled!!");
    }
    // Check for the various File API support.
    if (window.File && window.FileReader && window.FileList && window.Blob) {
        // Great success! All the File APIs are supported.
    } else {
        alert('The File APIs are not fully supported in this browser.');
    }

    $(document).ready(function () {
        //upload btn trigger
        $(document).on('click', '#uppgmbtn', function (event) {

            event.preventDefault;
            up_pgm();
        });

        //common program upload btn trigger
        $(document).on('click', '#upcpgmbtn', function (event) {

            event.preventDefault;
            up_cpgm();
        });

        //pgm validation save btn trigger
        $(document).on('click', '#val_sav_btn', function (event) {

            event.preventDefault;

            //var chkvel=$("#pgm_status").val();
            var e = document.getElementById("pgm_status");
            var chkval = e.options[e.selectedIndex].value;
            //var text = e.options[e.selectedIndex].text;

            $('#lod').modal('hide');
            if (chkval == "1") {
                $.notify('Nothing Saved, Because You did not update Validation status !', {
                    allow_dismiss: false,
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
            } else {
                change_pgm_status();
            }
            //console.log(chkval);


        });

        //common pgm validation save btn trigger
        $(document).on('click', '#cp_val_sav_btn', function (event) {

            event.preventDefault;

            //var chkvel=$("#pgm_status").val();
            var e = document.getElementById("cpgm_status");
            var chkval = e.options[e.selectedIndex].value;
            //var text = e.options[e.selectedIndex].text;

            $('#cp_val').modal('hide');
            if (chkval == "1") {
                $.notify('Nothing Saved, Because You did not update Validation status !', {
                    allow_dismiss: false,
                    type: 'danger',
                    placement: {
                        from: 'top',
                        align: 'right'
                    }
                });
            } else {
                change_cpgm_status();
                //console.log(chkval);
            }
            //console.log(chkval);


        });
        //edit entry save btn trigger
        $(document).on('click', '#edit_entry_btn', function (event) {

            event.preventDefault;
            edit_entry_fun();
        });

        //edit meta data save btn trigger
        $(document).on('click', '#edit_meta_btn', function (event) {

            event.preventDefault;
            edit_meta_fun();
        });

        //add snap data save btn trigger
        $(document).on('click', '#add_snap_btn', function (event) {

            event.preventDefault;

            // var sp_name = document.getElementById("snap_name").value;
            // var inads = document.getElementById("ads_lib_name").value;
            // var inlib = document.getElementById("oc_lib_name").value;
            // if(sp_name == "" || inads == "" || inlib == "" ){
            //     console.log('fill out the fields');
            //     $('.bs-example-snap-new-modal-lg').modal('hide');
            //     $.notify({
            //         title: '<strong>Failed to Insert!</strong>',
            //         message: 'fill out all the fields and try again'
            //     }, {
            //         type: 'danger'
            //     });
            // }else{
                add_snap_fun();
            // }

        });

        //edit snap data save btn trigger
        $(document).on('click', '#edit_snap_btn', function (event) {

            event.preventDefault;
            edit_snap_fun();
        });

        //update sec header save btn trigger
        $(document).on('click', '#update_sec_head_btn', function (event) {

            event.preventDefault;
            update_sec_head_fun();
        });

        //new entry save btn trigger
        $(document).on('click', '#new_entry_btn', function (event) {

            event.preventDefault;
            new_entry_fun();
        });

        //new section save btn trigger
        $(document).on('click', '#new_sec_btn', function (event) {
            $(this).attr('disabled','disabled');
            event.preventDefault;

            new_sec_fun();
        });

        //new sub section save btn trigger
        $(document).on('click', '#new_sub_sec_btn', function (event) {
            $(this).attr('disabled','disabled');
            event.preventDefault;
            new_sub_sec_fun();
        });


        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
            var x_checked = $(this).prop("checked");
            console.log(x_checked);

            var data = {
                "x_checked": x_checked
            };
            $.ajax({
                type: "POST",
                dataType: "json",
                url: "ajax/pgm_chk_v_status_all.php", //Relative or absolute path to response.php file
                data: data,
                success: function (response) {

                }
            });
        });

    });

    function up_pgm() {

        uploadFiles();

        // Catch the form submit and upload the files
        function uploadFiles() {

            // Create a formdata object and add the files
            var fd = new FormData(document.getElementById("fileHolder"));

            console.log('form data check -- ', fd);

            $.ajax({
                url: 'ajax/pgm_upload.php',
                type: 'POST',
                data: fd,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data) {
                    //console.log(data.issue);

                    if (data.success == "1") {
                        // Success so call function to process the form
                        $('.bs-example-up-modal-lg').modal('hide');
                        console.log('success: ' + data.success);
                        $.notify({
                            title: '<strong>SUCCESS!</strong>',
                            message: 'The file ' + data.pgmname + ' has been uploaded.'
                        }, {
                            type: 'success'
                        });

                        document.getElementById("nopgm").innerHTML = data.nopgm;
                        document.getElementById("indevpgm").innerHTML = data.indevpgm;
                        document.getElementById("tvalpgm").innerHTML = data.tvalpgm;
                        document.getElementById("valpgm").innerHTML = data.valpgm;
                        document.getElementById("nout").innerHTML = data.nout;
                        document.getElementById("oout").innerHTML = data.oout;
                        document.getElementById("cout").innerHTML = data.cout;
                        var pgmstat = data.pgmstat;
                        var outstat = data.outstat;

                        var nofo = (data.dtreeId.split("#").length - 1);
                        var res = data.dtreeId.split("#");

                        for (var i = 1; i <= nofo; i++) {
                            var element = document.getElementById('treeDemo_' + res[i] + '_ico');
                            //console.log(element);
                            element.style.background = 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat';
                        }
                        ;


                        console.log(data.dtreeId, nofo, res, res[1]);
                    }
                    else {
                        // Handle errors here
                        $('.bs-example-up-modal-lg').modal('hide');
                        console.log('ERRORS: ' + data.issue);

                        listofissue = data.issue;
                        issuenumber = listofissue.length;

                        for (var i = 0; i < 1; i++) {

                            if (data.issue.substr(i, 1) == '1') {
                                $.notify({
                                    title: '<strong>Mismatch Found in file name!</strong>',
                                    message: 'Provided file name: ' + data.given_file + ' But in TOC file name: ' + data.pgmname + 'Sorry, file name does not match. ! Please upload correct file'
                                }, {
                                    type: 'danger',
                                    autoHideDelay: 9000
                                });
                            }

                            else if (data.issue.substr(i, 1) == '2') {
                                $.notify({
                                    title: '<strong>File Size exceeds!</strong>',
                                    message: 'Sorry, your file is too large.'
                                }, {
                                    type: 'danger'
                                });
                            }

                            else if (data.issue.substr(i, 1) == '3') {
                                $.notify({
                                    title: '<strong>Wrong file type provided!</strong>',
                                    message: 'Sorry, Only sas files are allowed !'
                                }, {
                                    type: 'danger'
                                });
                            }

                            else if (data.issue.substr(i, 1) == '5') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'Sorry, Your study folder does not exist in server, Please create study folder first.'
                                }, {
                                    type: 'danger'
                                });
                            }

                            else if (data.issue.substr(i, 1) == '6') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'Sorry, there was an error uploading your file !'
                                }, {
                                    type: 'danger'
                                });
                            }

                            else if (data.issue.substr(i, 1) == '7') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'No file selected, Please file first'
                                }, {
                                    type: 'danger'
                                });
                            }
                            else {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'Sorry, your file was not uploaded !'
                                }, {
                                    type: 'danger'
                                });
                            }
                        }
                        ;
                    }
                }
            });
        }
    }

    // upload common program
    function up_cpgm() {

        uploadFiles();
        console.log("");
        // Catch the form submit and upload the files
        function uploadFiles() {

            // Create a formdata object and add the files
            var fd = new FormData(document.getElementById('Holder'));
            /// var form = $('form').get(0);
            //var fd = new FormData(form);
            //var fd = new FormData();
            //fd.append('file', $('input[type=file]')[0].files[1]);
//        var fd = new FormData();
//        var inputs = document.getElementById('pgm_loc');
//        for(i=0;i<inputs.length;i++) {
//            fd.append(inputs[i].name, inputs[i].value);
//        }
            console.log('form data check -- ', fd);

            $.ajax({
                url: 'ajax/cpgm_upload.php',
                type: 'POST',
                data: fd,
                cache: false,
                dataType: 'json',
                processData: false, // Don't process the files
                contentType: false, // Set content type to false as jQuery will tell the server its a query string request
                success: function (data) {
                    //console.log(data.issue);

                    if (data.success == "1") {
                        $('.bs-example-common-up-modal-lg').modal('hide');

                        console.log('success: ' + data.success);
                        $.notify({
                            title: '<strong>SUCCESS!</strong>',
                            message: 'The file ' + data.pgmname + ' has been uploaded.'
                        }, {
                            type: 'success'
                        });


                        var pgmstat = data.pgmstat;

                    }
                    else {
                        //Handle errors here
                        $('.bs-example-common-up-modal-lg').modal('hide');
                        console.log('ERRORS: ' + data.issue);

                        listofissue = data.issue;
                        issuenumber = listofissue.length;

                        for (var i = 0; i <= issuenumber; i++) {

                            if (data.issue.substr(i, 1) == '1') {
                                $.notify({
                                    title: '<strong>Mismatch Found in file name!</strong>',
                                    message: 'Provided file name: ' + data.given_file + ' But in TOC file name: ' + data.pgmname + 'Sorry, file name does not match. ! Please upload correct file'
                                }, {
                                    type: 'danger'
                                });
                            }

                            if (data.issue.substr(i, 1) == '2') {
                                $.notify({
                                    title: '<strong>File Size exceeds!</strong>',
                                    message: 'Sorry, your file is too large.'
                                }, {
                                    type: 'danger'
                                });
                            }

                            if (data.issue.substr(i, 1) == '3') {
                                $.notify({
                                    title: '<strong>Wrong file type provided!</strong>',
                                    message: 'Sorry, Only sas files are allowed !'
                                }, {
                                    type: 'danger'
                                });
                            }

                            if (data.issue.substr(i, 1) == '4') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'Sorry, your file was not uploaded !'
                                }, {
                                    type: 'danger'
                                });
                            }

                            if (data.issue.substr(i, 1) == '5') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'Sorry, Your study folder does not exist in server, Please create study folder first.'
                                }, {
                                    type: 'danger'
                                });
                            }

                            if (data.issue.substr(i, 1) == '6') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'Sorry, there was an error uploading your file !'
                                }, {
                                    type: 'danger'
                                });
                            }

                            if (data.issue.substr(i, 1) == '7') {
                                $.notify({
                                    title: '<strong>Failed to upload!</strong>',
                                    message: 'No file selected, Please file first'
                                }, {
                                    type: 'danger'
                                });
                            }
                        }
                        ;
                    }
                    location.reload();
                }
            });
        }
    }

    function change_pgm_status() {

// Create a formdata object and add the files
        var fd = new FormData(document.getElementById("pgm_status_form"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/val_sta_pgm.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(data.issue);

                if (data.success == "1") {

                    //$('.bs-example-up-modal-lg').modal('hide');
                    console.log('success: ' + data.success);
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'Program Status has been updated for ' + data.pgmname + ' '
                    }, {
                        type: 'success'
                    });

                    document.getElementById("nopgm").innerHTML = data.nopgm;
                    document.getElementById("indevpgm").innerHTML = data.indevpgm;
                    document.getElementById("tvalpgm").innerHTML = data.tvalpgm;
                    document.getElementById("valpgm").innerHTML = data.valpgm;
                    document.getElementById("nout").innerHTML = data.nout;
                    document.getElementById("oout").innerHTML = data.oout;
                    document.getElementById("cout").innerHTML = data.cout;
                    var pgmstat = data.pgmstat;
                    var outstat = data.outstat;

                    var nofo = (data.dtreeId.split("#").length - 1);
                    var res = data.dtreeId.split("#");

                    for (var i = 1; i <= nofo; i++) {
                        var element = document.getElementById('treeDemo_' + res[i] + '_ico');
                        //console.log(element);
                        element.style.background = 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat';
                    }
                    ;


                    console.log(data.dtreeId, nofo, res, res[1]);

                }
                else {

                    //$('.bs-example-up-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update the Status!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }

    //common program update validation status
    function change_cpgm_status() {

        // var table = document.getElementById("myTable");
        // var delCount = table.rows.length;
        // //console.log("total row:"+delCount);
        // if(delCount > 1){
        //     for (var i = delCount-1; i > 0; i--) {
        //         //console.log("i="+i);
        //         table.deleteRow(i);
        //     }
        // }
// Create a formdata object and add the files
        var fd = new FormData(document.getElementById("cpgm_status_form"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/cp_val_sta_pgm.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (response) {
                //console.log(response[0].success);
                // var total_lenght=response.length;
                // var status = "";
                // if(total_lenght > 0 ) {

                //     //var table = document.getElementById("myTable");
                //     var myTable = "";
                //     for (var i = 0; i < total_lenght; i++) {

                //         // var rowCount = table.rows.length;
                //         // var row = table.insertRow(rowCount);


                //         var cploc = response[i].cploc;
                //         var cpname = response[i].cpname;
                //         var programmer = response[i].programmer;
                //         var cpdate = response[i].cpdate;
                //         var status = response[i].status;
                //         //var pgmstat = response[i].pgmstat;
                //         var validator = response[i].validator;
                //         var status_date = response[i].status_date;

                //          var col="";
                //         if(response[i].pgmstat=="0"){
                //             col="default";
                //             status="No program";
                //         }
                //         if(response[i].pgmstat=="1"){
                //             col="warning";
                //             status="In Development";
                //         }
                //         if(response[i].pgmstat=="2"){
                //             col="danger";
                //             status="To Be validated";
                //         }
                //         if(response[i].pgmstat=="3"){
                //             col="success";
                //             status="Validated";
                //         }


                //         myTable = myTable+"<tr><td>"+cploc+"</td><td class='pg'>"+cpname+"</td><td>"+programmer+"</td><td>"+cpdate+"</td><td>"+validator+"</td><td>"+status+"</td><td>"+status_date+"</td></tr>";


                //         $('#myTable tbody').children().remove();
                //         $('#myTable tbody').append(myTable);
                // var cell0 = row.insertCell(0);
                // cell0.innerHTML = cploc;


                // var cell1 = row.insertCell(1);
                // cell1.innerHTML = cpname;
                // cell1.className = "pg";


                // var cell2 = row.insertCell(2);
                // cell2.innerHTML = programmer;

                // var cell3 = row.insertCell(3);
                // cell3.innerHTML = cpdate;

                // var cell4 = row.insertCell(4);
                // cell4.innerHTML = validator;


                // var cell5 = row.insertCell(5);
                // cell5.innerHTML = "<span class='label label-"+col+"'>"+status+"</span>";

                // var cell6 = row.insertCell(6);
                // cell6.innerHTML = status_date;


                //     }
                //     console.log(myTable);
                // }else{

                // }


                if (response[0].success == "1") {
                    $('.bs-example-modal-lg-cp-val').modal('hide');
                    //console.log('success: ' + response.success);
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'Program Status has been updated'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    $('.bs-example-modal-lg-cp-val').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update the Status!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
                location.reload();
            }
        });
    }


 

    function edit_meta_fun() {
        $('#pgmld').modal('show');
     // Create a formdata object and add the files
        var fd = new FormData(document.getElementById("edit_meta_fd"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/edit_meta_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(nodeuid+'_span', data.title_text);

                if (data.success == "1") {

//                document.getElementById(nodeuid+'_span').innerHTML = data.title_text;
                    //console.log('success: ' + data.success);
                    $('#pgmld').modal('hide');
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'Meta information has been updated'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    $('.bs-example-edit-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }




    function add_snap_fun() {
        $('#pgmld').modal('show');
         var table = document.getElementById("mysnapTable");
         var delCount = table.rows.length;
         //console.log("total row:"+delCount);
         if(delCount > 1){
             for (var i = delCount-1; i > 0; i--) {
                 //console.log("i="+i);
                 table.deleteRow(i);
             }
         }
      // Create a formdata object and add the files
        var fd = new FormData(document.getElementById("add_snap_fd"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/add_snap_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (response) {
                //console.log(response[0]);

                if (response[0].success == "1") {

                     var total_lenght=response.length;
                     if(total_lenght > 0 ) {
                         for (var i = 0; i < total_lenght; i++) {
                             var table = document.getElementById("mysnapTable");
                             var rowCount = table.rows.length;
                             var row = table.insertRow(rowCount);

                             var cell0 = row.insertCell(0);
                             cell0.innerHTML = response[i].snap_name;

                             var cell1 = row.insertCell(1);
                             cell1.innerHTML = response[i].is_lock;


                             var cell3 = row.insertCell(2);
                             cell3.innerHTML = response[i].updated_at;

                             var cell4 = row.insertCell(3);
                             cell4.innerHTML = response[i].updated_by;

                             var cell5 = row.insertCell(4);
                             cell5.innerHTML = response[i].created_at;

                             var cell6 = row.insertCell(5);
                             cell6.innerHTML = response[i].updated_by;

                             var cell7 = row.insertCell(6);
                             cell7.innerHTML = "<a href='update_snap.php?snap="+response[i].id+"'><button  type='button' class='btn btn-primary btn-block btn-flat'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Update</i></button></a>";
                         }
                     }

                    $('#pgmld').modal('hide');
                    $('.bs-example-snap-new-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'New Data Currency has been Created'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    var total_lenght=response.length;
                    if(total_lenght > 0 ) {
                        for (var i = 0; i < total_lenght; i++) {
                            var table = document.getElementById("mysnapTable");
                            var rowCount = table.rows.length;
                            var row = table.insertRow(rowCount);

                            var cell0 = row.insertCell(0);
                            cell0.innerHTML = response[i].snap_name;

                            var cell1 = row.insertCell(1);
                            cell1.innerHTML = response[i].ads_lib_name;

                            var cell2 = row.insertCell(2);
                            cell2.innerHTML = response[i].oc_lib_name;

                            var cell3 = row.insertCell(3);
                            cell3.innerHTML = response[i].updated_at;

                            var cell4 = row.insertCell(4);
                            cell4.innerHTML = response[i].updated_by;

                            var cell5 = row.insertCell(5);
                            cell5.innerHTML = response[i].created_at;

                            var cell6 = row.insertCell(6);
                            cell6.innerHTML = response[i].updated_by;

                            var cell7 = row.insertCell(7);
                            cell7.innerHTML = "<a href='update_snap.php?snap="+response[i].id+"'><button  type='button' class='btn btn-primary btn-block btn-flat'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Update</i></button></a>";
                        }
                    }
                    $('#pgmld').modal('hide');
                    $('.bs-example-snap-new-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }


    function edit_snap_fun() {
        $('#pgmld').modal('show');
// Create a formdata object and add the files
        var fd = new FormData(document.getElementById("edit_snap_fd"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/edit_snap_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(nodeuid+'_span', data.title_text);
                if (data.success == "1") {
//                document.getElementById(nodeuid+'_span').innerHTML = data.title_text;
                    //console.log('success: ' + data.success);
                    $('#pgmld').modal('hide');
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'Data Currency information has been updated'
                    }, {
                        type: 'success'
                    });
                }
                else {
                    $('.bs-example-edit-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });
                }
            }
        });
    }

    function update_sec_head_fun() {

// Create a formdata object and add the files
        var fd = new FormData(document.getElementById("update_sec_head_fd"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/update_sec_head_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(nodeuid+'_span', data.title_text);

                if (data.success == "1") {

                    document.getElementById(nodeuid + '_span').innerHTML = data.title_text;
                    $('.bs-example-update_sechead-modal-lg').modal('hide');
                    //console.log('success: ' + data.success);
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'Section information has been updated'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    $('.bs-example-update_sechead-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }


  

    function new_sec_fun() {

        // Create a formdata object and add the files
        var fd = new FormData(document.getElementById("new_sec_fd"));

        //console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/new_sec_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                if (data.success == "1") {
                    $('.bs-example-new-sec-modal-lg').modal('hide');
                    //location.reload();
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'New Section inserted'
                    }, {
                        type: 'success'
                    });
                }
                else {

                    $('.bs-example-new-sec-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'You can not create section here!'
                    }, {
                        type: 'danger'
                    });
                }
            }
        });
    }

    function new_sub_sec_fun() {

        // Create a formdata object and add the files
        var fd = new FormData(document.getElementById("new_sub_sec_fd"));

        console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/new_sub_sec_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(nodeuid+'_span', data.title_text);

                if (data.success == "1") {

                    //document.getElementById(nodeuid+'_span').innerHTML = data.title_text;
                    $('.bs-example-new-sub-sec-modal-lg').modal('hide');
                    location.reload();
                    //console.log('success: ' + data.success);
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'New Sub Section inserted'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    $('.bs-example-new-sub-sec-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }

    $('.ckbox').click(function () {
        //check if checkbox is checked
        if ($(this).is(':checked')) {

            $('.sendRun').removeAttr('disabled'); //enable input

        } else {
            $('.sendRun').attr('disabled', true); //disable input
        }
    });

   function edit_entry_fun() {

// Create a formdata object and add the files
        var fd = new FormData(document.getElementById("edit_entry_fd"));
       //console.log(fd);
       console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/edit_entry_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(nodeuid+'_span', data.title_text);

                if (data.success == "1") {

                    document.getElementById(nodeuid + '_span').innerHTML = data.title_text;
                    $('.bs-example-edit-modal-lg').modal('hide');
                    //console.log('success: ' + data.success);

                    document.getElementById("nopgm").innerHTML = data.nopgm;
                    document.getElementById("indevpgm").innerHTML = data.indevpgm;
                    document.getElementById("tvalpgm").innerHTML = data.tvalpgm;
                    document.getElementById("valpgm").innerHTML = data.valpgm;
                    document.getElementById("nout").innerHTML = data.nout;
                    document.getElementById("oout").innerHTML = data.oout;
                    document.getElementById("cout").innerHTML = data.cout;
                    var pgmstat = data.pgmstat;
                    var outstat = data.outstat;

                    var nofo = (data.dtreeId.split("#").length - 1);
                    var res = data.dtreeId.split("#");

                    for (var i = 1; i <= nofo; i++) {
                        var element = document.getElementById('treeDemo_' + res[i] + '_ico');
                        //console.log(element);
                        element.style.background = 'url("./stree/metroStyle/img/state_' + pgmstat + '_' + outstat + '.png") 0px 0 no-repeat';
                    }



                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'Entry information has been updated'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    $('.bs-example-edit-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }

    //Create new entry
    function new_entry() {
        var x = document.getElementById("ne_row_id");
        x.value = pgmid;
        // alert("table id "+pgmid);
        hideRMenu();
        console.log("now here ");
    }

  function new_entry_fun() {

        // Create a formdata object and add the files
        var fd = new FormData(document.getElementById("new_entry_fd"));

       // console.log('form data check -- ', fd);

        $.ajax({
            url: 'ajax/new_entry_edit.php',
            type: 'POST',
            data: fd,
            cache: false,
            dataType: 'json',
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
            success: function (data) {
                //console.log(nodeuid+'_span', data.title_text);

                if (data.success == "1") {

                    //document.getElementById(nodeuid+'_span').innerHTML = data.title_text;
                    $('.bs-example-new-entry-modal-lg').modal('hide');
                    location.reload();
                    //console.log('success: ' + data.success);
                    $.notify({
                        title: '<strong>SUCCESS!</strong>',
                        message: 'New Entry inserted'
                    }, {
                        type: 'success'
                    });

                }
                else {

                    //$('.bs-example-update_sechead-modal-lg').modal('hide');
                    $.notify({
                        title: '<strong>Failed to Update!</strong>',
                        message: 'Something went wrong, Please try again'
                    }, {
                        type: 'danger'
                    });

                }
            }
        });
    }

    /**
     * AES JSON formatter for CryptoJS
     *
     * @author BrainFooLong (bfldev.com)
     * @link https://github.com/brainfoolong/cryptojs-aes-php
     */

    var CryptoJSAesJson = {
        stringify: function (cipherParams) {
            var j = {ct: cipherParams.ciphertext.toString(CryptoJS.enc.Base64)};
            if (cipherParams.iv) j.iv = cipherParams.iv.toString();
            if (cipherParams.salt) j.s = cipherParams.salt.toString();
            return JSON.stringify(j);
        },
        parse: function (jsonStr) {
            var j = JSON.parse(jsonStr);
            var cipherParams = CryptoJS.lib.CipherParams.create({ciphertext: CryptoJS.enc.Base64.parse(j.ct)});
            if (j.iv) cipherParams.iv = CryptoJS.enc.Hex.parse(j.iv);
            if (j.s) cipherParams.salt = CryptoJS.enc.Hex.parse(j.s);
            return cipherParams;
        }
    }

    // the highest value is calculating for how much parameter in the create entry
    highest=1;
   //grab all info for entry and show it in modal - edit entry//
    var etitle, epgmname, eoutname, elogname, etype, epgmloc, eoutloc, eoutno, etlfnum, eshell_name,epopulation,efootnote;
    function editentr() {
        document.getElementById("pgnam1").innerHTML = pgmname;

        var x = document.getElementById("edit_sid");
        x.value = pgmid;

        var data = {
            "pgmid": pgmid,
            "pgmname": pgmname
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/edit_entry.php",
            data: data,
            success: function (response) {

                $("#input_frm_grp_edit_entry_fd").children().remove();

                var mydivz1 = '';
                var mydivz2 = '';
                var mydivz3 = '';
                var mydivz4 = '';
                var mydivz5 = '';
                var mydivz6 = '';
                var mydivz7 = '';
                var mydivz8 = '';
                var mydivz9 = '';

                var mydiv1 = '';
                var mydiv2 = '';
                var mydiv3 = '';
                var mydiv3_1='';
                var mydiv4 = '';
                var mydiv5 = '';
                var mydiv6 = '';
                var mydiv7 = '';
                var mydiv8 = '';
                var mydiv8_1 = '';
                var mydiv9 = '';
                var mydiv10 = '';
                var mydiv11 = '';
                var myinpute = '';
                var myinput1 = '';

//                var encrypted = response.title;
//                var decrypted = JSON.parse(CryptoJS.AES.decrypt( "mz5", encrypted, {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));

                etitle = response.title;
                efootnote = response.footnote;
                epgmname = response.pgmname;
                eoutname = response.outname;
                elogname = response.logname;
                etype = response.type;
                etlfnum = response.tlfnum;
                epgmloc = response.pgmloc;
                eoutloc = response.outloc;
                eoutno = response.outno;
                epopulation=response.population;
                eshell_name = response.shell_name;

               // console.log ("i am here "+etitle);

                myinpute = '<input id="param_list_count" type="hidden" name="param_list_count" value=>';
                myinput1 = '<input id="param_list_count_previous" type="hidden" name="param_list_count_previous" value='+response.param_list_count+'>';

                mydiv1 = mydiv1 + '<div class="row"> <div class="col-md-2"> <div class="form-group has-feedback">';
                mydiv1 = mydiv1 + '<p><b>Type :</b></p>';
                mydiv1 = mydiv1 + '<input type="text" name="type" id="type" class="form-control" value="' + etype + '"/> </div></div>';

                mydiv2 = mydiv2 + '<div class="col-md-2"> <div class="form-group has-feedback">';
                mydiv2 = mydiv2 + '<p><b>TLF Number :</b></p>';
                mydiv2 = mydiv2 + '<input type="text" name="tlfnum" id="tlfnum" class="form-control" value="' + etlfnum + '"/> </div></div>';

                mydiv3 = mydiv3 + '<div class="col-md-8"> <div class="form-group has-feedback">';
                mydiv3 = mydiv3 + '<p><b>Program Title :</b></p>';
                mydiv3 = mydiv3 + '<input type="text" name="title" id="title" class="form-control" value="' + etitle + '"/> </div></div> </div>';

                mydiv3_1 = mydiv3_1 + '<div class="row"> <div class="col-md-12"> <div class="form-group has-feedback">';
                mydiv3_1 = mydiv3_1 + '<p><b>Footnote :</b></p>';
                mydiv3_1 = mydiv3_1 + '<input type="text" name="footnote" id="footnote" class="form-control" value="' + efootnote + '"/> </div></div> </div>';



                mydiv4 = mydiv4 + '<div class="row"> <div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv4 = mydiv4 + '<p><b>Program Name :</b></p>';
                mydiv4 = mydiv4 + '<input type="text" name="pgmname" id="pgmname" class="form-control" value="' + epgmname + '"/> </div> </div>';

                mydiv5 = mydiv5 + '<div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv5 = mydiv5 + '<p><b>Program Location :</b></p>';
                mydiv5 = mydiv5 + '<input type="text" name="pgmloc" id="pgmloc" class="form-control" value="' + epgmloc + '"/> </div> </div>';

                mydiv6 = mydiv6 + '<div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv6 = mydiv6 + '<p><b>Log Name :</b></p>';
                mydiv6 = mydiv6 + '<input type="text" name="logname" id="logname" class="form-control" value="' + elogname + '"/> </div> </div>';

                mydiv7 = mydiv7 + '<div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv7 = mydiv7 + '<p><b>Output Number :</b></p>';
                mydiv7 = mydiv7 + '<input type="text" name="outno" id="outno" class="form-control" value="' + eoutno + '"/> </div> </div> </div>';


                mydiv8_1 = mydiv8_1 + '<div class="row"> <div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv8_1 = mydiv8_1 + '<p><b>Population :</b></p>';
                mydiv8_1 = mydiv8_1 + '<input type="text" name="population" id="population" class="form-control" value="' + epopulation + '"/> </div> </div>';

                mydiv8 = mydiv8 + '<div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv8 = mydiv8 + '<p><b>Output Name :</b></p>';
                mydiv8 = mydiv8 + '<input type="text" name="outname" id="outname" class="form-control" value="' + eoutname + '"/> </div> </div>';

                mydiv9 = mydiv9 + '<div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv9 = mydiv9 + '<p><b>Output Location :</b></p>';
                mydiv9 = mydiv9 + '<input type="text" name="outloc" id="outloc" class="form-control" value="' + eoutloc + '"/> </div> </div>';

                mydiv10 = mydiv10 + '<div class="col-md-3"> <div class="form-group has-feedback">';
                mydiv10 = mydiv10 + '<p><b>Shell Name :</b></p>';
                mydiv10 = mydiv10 + '<input type="text" name="shell_name" id="shell_name" class="form-control" value="' + eshell_name + '"/> </div> </div> </div>';

                mydiv11 = mydiv11 + '<div class="row" id="create_entry_param_list2">  </div>';


                $("#input_frm_grp_edit_entry_fd").append(myinpute + myinput1 + mydiv1 + mydiv2 + mydiv3 + mydiv3_1 + mydiv4 + mydiv5 + mydiv6 + mydiv7 + mydiv8_1 + mydiv8 + mydiv9 + mydiv10+ mydiv11);
//
//                document.getElementById("title").value = etitle;
//                document.getElementById("pgmname").value = epgmname;
//                document.getElementById("outname").value = eoutname;
//                document.getElementById("logname").value = elogname;
//                document.getElementById("type").value = etype;
//                document.getElementById("tlfnum").value = etlfnum;
//                document.getElementById("pgmloc").value = epgmloc;
//                document.getElementById("outloc").value = eoutloc;
//                document.getElementById("outno").value = eoutno;


                document.getElementById("param_list_count").value = response.param_list_count;
                highest= parseInt(response.param_list_count);
                //console.log(highest);
                //checking. has parameter value or not
                if(highest==0)
                    highest=1;
                for (var i = 1; i <= highest; i++) {

                    var _suf_name = 'parameter_' + i + '_name';
                    pramname = response[_suf_name];
                    var _suf_value = 'parameter_' + i + '_value';
                    paramvalue = response[_suf_value];
                    if(pramname==null)
                        pramname="";
                    if(paramvalue==null)
                        paramvalue="";
//                    var pramname= response.;
//                    var paramvalue = 'parameter_'+i+'_value';
                    mydivz1 = '<div id="edit_entry_param_list_'+i+'"> <div class="col-md-5"> <div class="form-group has-feedback">';
                    mydivz2 = '<p><b id="name_level_edit_'+i+'">parameter_' + i + '_name:</b></p>';
                    mydivz3 = '<input type="text" name="parameter_' + i + '_name"  id="parameter_' + i + '_name" class="form-control" value="' + pramname + '"/> </div></div>';
                    //$("#create_entry_param_list2").append();

                    mydivz4 = '<div class="col-md-5"> <div class="form-group has-feedback">';
                    mydivz5 = '<p><b id="value_level_edit_'+i+'">parameter_' + i + '_value:</b></p>';
                    mydivz6 = '<input type="text" name="parameter_' + i + '_value" id="parameter_' + i + '_value" class="form-control" value="' + paramvalue + '"/> </div></div>';
                    // $("#create_entry_param_list2").append(mydivz4 + mydivz5 + mydivz6);

                    mydivz7 = '<div class="col-md-2 form-group has-feedback"><div id="add_button_edit_'+i+'"></div>';
                    mydivz8 = '<p></p>';

                    mydivz9 = '<a class="btn" href="#" id="add_btn_edit_' + i + '" onclick="paramadd_edit(' + (i) + ');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>';

                    if(i!=1)
                        mydivz9 = mydivz9+'<a class="btn" href="#" id="del_btn_edit_' + i + '" onclick="paramdel_edit(' + (i) + ');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a></div></div>';


                    $("#create_entry_param_list2").append(mydivz1 + mydivz2 + mydivz3+mydivz4 + mydivz5 + mydivz6+mydivz7 + mydivz8 + mydivz9);
                }



                //console.log(epgmloc);console.log(etype);
            }
        });
        hideRMenu();
    }

    // these functions are adding and deleting parameter in edit enty
    function paramadd_edit(x){

        var div='';
        var div_update_add_btn='';
        var div_update_del_btn='';

        var y=x+1;

        if(x<highest)
        {

            for(var i=highest+1;i>x;i--)
            { //alert(i);
                var i2=i+1;
                $("#edit_entry_param_list_"+i).attr("id","edit_entry_param_list_"+i2);
                $("#name_level_edit_"+i).text("parameter_"+i2+"_name");
                $("#value_level_edit_"+i).text("parameter_"+i2+"_value");
                $("#name_level_edit_"+i).attr("id","name_level_edit_"+i2);
                $("#value_level_edit_"+i).attr("id","value_level_edit_"+i2);
                $("#parameter_"+i+"_name").attr("name","parameter_"+i2+"_name");
                $("#parameter_"+i+"_name").attr("id","parameter_"+i2+"_name");
                $("#parameter_"+i+"_value").attr("name","parameter_"+i2+"_value");
                $("#parameter_"+i+"_value").attr("id","parameter_"+i2+"_value");

                $("#add_button_edit_"+i).attr("id","add_button_edit_"+i2);
                $("#add_btn_edit_"+i).remove();
                $("#del_btn_edit_"+i).remove();
                div_update_add_btn='<a class="btn" href="#" id="add_btn_edit_'+i2+'" onclick="paramadd_edit('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>'
                div_update_del_btn='<a class="btn" href="#" id="del_btn_edit_'+i2+'" onclick="paramdel_edit('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a>'
                $("#add_button_edit_"+i2).after(div_update_add_btn);
                $("#add_btn_edit_"+i2).after(div_update_del_btn);

            }
        }

        div='<div id="edit_entry_param_list_'+y+'">';
        div=div+'<div class="col-md-5">';
        div=div+'<div class="form-group has-feedback">';
        div=div+'<p><b id="name_level_edit_'+y+'">parameter_'+y+'_name:</b></p>';
        div=div+'<input type="text" name="parameter_'+y+'_name" id="parameter_'+y+'_name" class="form-control">';
        div=div+'</div></div>';
        div=div+'<div class="col-md-5"><div class="form-group has-feedback">';
        div=div+'<p><b id="value_level_edit_'+y+'">parameter_'+y+'_value:</b></p>';
        div=div+'<input type="text" name="parameter_' + y + '_value" id="parameter_'+y+'_value" class="form-control" >';
        div=div+'</div></div>';
        div=div+'<div class="col-md-2 form-group has-feedback"><p></p>';
        div=div+'<div id="add_button_edit_'+y+'"></div>';
        div=div+'<a class="btn" href="#" id="add_btn_edit_'+y+'" onclick="paramadd_edit('+y+');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>';
        div=div+'<a class="btn" href="#" id="del_btn_edit_'+y+'" onclick="paramdel_edit('+y+');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a></div>';

        div=div+'</div></div></div>';


        $("#edit_entry_param_list_"+x).after(div);
        highest=highest+1;
        var zz = document.getElementById("param_list_count");
        zz.value = highest;
        console.log(highest);

    }
    function paramdel_edit(x) {


        $("#edit_entry_param_list_"+x).remove();
        for(var i=x+1;i<=highest;i++)
        {
            var i2=i-1;
            $("#edit_entry_param_list_"+i).attr("id","edit_entry_param_list_"+i2);
            $("#name_level_edit_"+i).text("parameter_"+i2+"_name");
            $("#value_level_edit_"+i).text("parameter_"+i2+"_value");
            $("#name_level_edit_"+i).attr("id","name_level_edit_"+i2);
            $("#value_level_edit_"+i).attr("id","value_level_edit_"+i2);
            $("#parameter_"+i+"_name").attr("name","parameter_"+i2+"_name");
            $("#parameter_"+i+"_name").attr("id","parameter_"+i2+"_name");
            $("#parameter_"+i+"_value").attr("name","parameter_"+i2+"_value");
            $("#parameter_"+i+"_value").attr("id","parameter_"+i2+"_value");

            $("#add_button_edit_"+i).attr("id","add_button_edit_"+i2);
            $("#add_btn_edit_"+i).remove();
            $("#del_btn_edit_"+i).remove();
            div_update_add_btn='<a class="btn" href="#" id="add_btn_edit_'+i2+'" onclick="paramadd_edit('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>'
            div_update_del_btn='<a class="btn" href="#" id="del_btn_edit_'+i2+'" onclick="paramdel_edit('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a>'
            $("#add_button_edit_"+i2).after(div_update_add_btn);
            $("#add_btn_edit_"+i2).after(div_update_del_btn);

        }
        highest=highest-1;
        var zz = document.getElementById("param_list_count");
        zz.value = highest;
    }


    // these function are adding and deleting parameter in create entry
    function paramadd(x){

        var div='';
        var div_update_add_btn='';
        var div_update_del_btn='';
        //alert("clicked "+x+" H : "+highest);
        var y=x+1;

        if(x<highest)
        {

            for(var i=highest+1;i>x;i--)
            {   console.log(i);
                var i2=i+1;
                $("#create_entry_param_list_"+i).attr("id","create_entry_param_list_"+i2);
                $("#name_level_"+i).text("Parameter_"+i2+"_name:");
                $("#value_level_"+i).text("Parameter_"+i2+"_value:");
                $("#name_level_"+i).attr("id","name_level_"+i2);
                $("#value_level_"+i).attr("id","value_level_"+i2);
                $("#parameter_"+i+"_name").attr("name","parameter_"+i2+"_name");
                $("#parameter_"+i+"_name").attr("id","parameter_"+i2+"_name");
                $("#parameter_"+i+"_value").attr("name","parameter_"+i2+"_value");
                $("#parameter_"+i+"_value").attr("id","parameter_"+i2+"_value");

                $("#add_button_"+i).attr("id","add_button_"+i2);
                $("#add_btn_"+i).remove();
                $("#del_btn_"+i).remove();
                div_update_add_btn='<a class="btn" href="#" id="add_btn_'+i2+'" onclick="paramadd('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>'
                div_update_del_btn='<a class="btn" href="#" id="del_btn_'+i2+'" onclick="paramdel('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a>'
                $("#add_button_"+i2).after(div_update_add_btn);
                $("#add_btn_"+i2).after(div_update_del_btn);



            }
        }

        div='<div class="row" id="create_entry_param_list_'+y+'">';
        div=div+'<div class="col-md-5">';
        div=div+'<div class="form-group has-feedback">';
        div=div+'<p><b id="name_level_'+y+'">Parameter_'+y+'_name:</b></p>';
        div=div+'<input type="text" name="parameter_'+y+'_name" id="parameter_'+y+'_name" class="form-control">';
        div=div+'</div></div>';
        div=div+'<div class="col-md-5"><div class="form-group has-feedback">';
        div=div+'<p><b id="value_level_'+y+'">Parameter_'+y+'_value:</b></p>';
        div=div+'<input type="text" name="parameter_'+y+'_value" id="parameter_'+y+'_value" class="form-control" >';
        div=div+'</div></div>';
        div=div+'<div class="col-md-2 form-group has-feedback"><p></p>';
        div=div+'<div id="add_button_'+y+'"></div>';
        div=div+'<a class="btn" href="#" id="add_btn_'+y+'" onclick="paramadd('+y+');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>';
        div=div+'<a class="btn" href="#" id="del_btn_'+y+'" onclick="paramdel('+y+');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a></div>';

        div=div+'</div></div></div>';


        $("#create_entry_param_list_"+x).after(div);
        highest=highest+1;
    }


    function paramdel(x) {


        $("#create_entry_param_list_"+x).remove();
        for(var i=x+1;i<=highest;i++)
        {
            var i2=i-1;
            $("#create_entry_param_list_"+i).attr("id","create_entry_param_list_"+i2);
            $("#name_level_"+i).text("Parameter_"+i2+"_name");
            $("#value_level_"+i).text("Parameter_"+i2+"_value");
            $("#name_level_"+i).attr("id","name_level_"+i2);
            $("#value_level_"+i).attr("id","value_level_"+i2);
            $("#parameter_"+i+"_name").attr("name","parameter_"+i2+"_name");
            $("#parameter_"+i+"_name").attr("id","parameter_"+i2+"_name");
            $("#parameter_"+i+"_value").attr("name","parameter_"+i2+"_value");
            $("#parameter_"+i+"_value").attr("id","parameter_"+i2+"_value");

            $("#add_button_"+i).attr("id","add_button_"+i2);
            $("#add_btn_"+i).remove();
            $("#del_btn_"+i).remove();
            div_update_add_btn='<a class="btn" href="#" id="add_btn_'+i2+'" onclick="paramadd('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-plus"></span></a>'
            div_update_del_btn='<a class="btn" href="#" id="del_btn_'+i2+'" onclick="paramdel('+i2+');"><span class="text-center glyphicon glyphicon glyphicon-minus"></span></a>'
            $("#add_button_"+i2).after(div_update_add_btn);
            $("#add_btn_"+i2).after(div_update_del_btn);

        }
        highest=highest-1;
    }




    function update_legend(x){
        console.log(x.value);

        var data = {
            "x_sec": x.value
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/sec_chk_legend_status_update.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                document.getElementById("totalpgm").innerHTML = response.totalpgm;
                document.getElementById("nopgm").innerHTML = response.nopgm;
                document.getElementById("indevpgm").innerHTML = response.indevpgm;
                document.getElementById("tvalpgm").innerHTML = response.tvalpgm;
                document.getElementById("valpgm").innerHTML = response.valpgm;
                document.getElementById("nout").innerHTML = response.nout;
                document.getElementById("oout").innerHTML = response.oout;
                document.getElementById("cout").innerHTML = response.cout;
            }
        });
    }

    function lib_folder_create (i,x,y) {     
    var libname = x;   
    var dc_name = y;
        console.log(x);

        var data = {
            "x_lib": libname,
            "x_dc": dc_name
        };

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/create_lib_folder.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
                $('#lib-loc-'+i).remove();

                var msg1="<div class='row' id='lib-loc-msg-"+i+"'> <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>";
                var msg2="<span style='color: green;'><i class='fa fa-check'></i>  Physical location created for "+response[0].dc_name+" Library </span>";
                var msg3="</div> </div>";

                $("#add_snap_fd").append(msg1+msg2+msg3);
            }
        });
    }

    function reload_tree_dc(x){
        
         var data = {
            "x_id_n": x.value
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/update_dc_session.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
               location.reload(true);
            }
        });
    }

    function filter_dc (x) {
        console.log(x);
        

        var data = {
            "x_id_n": x
        }

        $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/update_filter_dc_session.php", //Relative or absolute path to response.php file
            data: data,
            success: function (response) {
               location.reload(true);
            }
        });
    }

    function chk_DC_loc(x){
        var dc_name = "'"+x+"'";

        $('#help-block').remove();
        $('#rowtoshowinput').remove();
        $('#dcinputbutton').remove();
        //console.log(x);

        for (var i = 0; i < 99; i++) {
            $('#lib-loc-'+i).remove();
            }
        for (var i = 0; i < 99; i++) {
            $('#lib-loc-msg-'+i).remove();
            }



        if (x=="") {
                    var msg_line = '<span class="help-block" id="help-block" style="color: red;"><i class="fa fa-times-circle-o"></i> Please provide data currency name first</span>';
                    $("#snap_name_field").append(msg_line);
        }
        else{
            var data = {
            "x_dc": x
            };
            $.ajax({
            type: "POST",
            dataType: "json",
            url: "ajax/chk_dc_folder_exist.php", 
            data: data,
            success: function (response) {

                if (response[0].status=="yes") {

                    var maxid_line = '<input id="max_row_id" type="hidden" name="max_row_id" value='+response[0].maxid+'>';
                    $("#snap_name_field").append(maxid_line);

                    var totrow_line = '<input id="tot_row" type="hidden" name="tot_row" value='+response.length+'>';
                    $("#snap_name_field").append(totrow_line);

                    var msg_line = '<span class="help-block" id="help-block" style="color: green;"><i class="fa fa-check"></i> Physical location exist for this Data Currency </span>';
                    $("#snap_name_field").append(msg_line);



                    for (var i = 0; i < response.length; i++) {
                        if ((response[i].libnm == "ocview") && (response[i].lib_folder_ext == "no")) {

                            var msg1="<div class='row ' id='lib-loc-"+i+"'> <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>";
                            var msg2="<span style='color: red;'><i class='fa fa-times-circle-o'></i> Physical location does not exist for "+response[i].libnm+" Library </span>";
                            var msg3="</div> </div>";

                            $("#add_snap_fd").append(msg1+msg2+msg3);

                        }
                        if ((response[i].libnm == "newd") && (response[i].lib_folder_ext == "no")) {

                            var msg1="<div class='row ' id='lib-loc-"+i+"'> <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>";
                            var msg2="<span style='color: red;'><i class='fa fa-times-circle-o'></i> Physical location does not exist for "+response[i].libnm+" Library </span>";
                            var msg3="</div> </div>";

                            $("#add_snap_fd").append(msg1+msg2+msg3);

                        }
                        if ((response[i].libnm == "ads") && (response[i].lib_folder_ext == "no")) {

                            var msg1="<div class='row' id='lib-loc-"+i+"'> <div class='col-xs-12 col-sm-6 col-md-6 col-lg-6'>";
                            var msg2="<span style='color: red;'><i class='fa fa-times-circle-o'></i> Physical location does not exist for "+response[i].libnm+" Library </span>";
                            var msg3="</div> </div>";

                            $("#add_snap_fd").append(msg1+msg2+msg3);

                        }
                        if ((response[i].libnm != "ocview") && (response[i].libnm != "newd") && (response[i].libnm != "ads") && (response[i].lib_folder_ext == "no")) {

                            var msg1="<div class='row' id='lib-loc-"+i+"'> <div class='col-xs-12 col-sm-12 col-md-12 col-lg-12'>";
                            var libnamjs = "'"+response[i].libnm+"'";
                            var msg2='<span style="color: red;"><i class="fa fa-times-circle-o"></i> Physical location does not exist for '+response[i].libnm+' Library <a id="click_fld_create_'+response[i].libnm+'" onclick="lib_folder_create('+i+','+dc_name+','+libnamjs+')">Click here to create folder.</a> </span> ';
                            var msg3="</div> </div>";

                            $("#add_snap_fd").append(msg1+msg2+msg3);

                        }
                    }

                    var inputrow = '<div class="row" id="rowtoshowinput"><div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"><div class="row"><div class="col-md-4"><div class="form-group has-feedback" id="libnamlist"><p><b>Library Name:</b></p></div></div><div class="col-md-8"><div class="form-group has-feedback" id="libloclist"><p><b>Library Location:</b></p></div></div></div></div></div>';

                    $("#add_snap_fd").append(inputrow);

                    for (var i = 0; i < response.length; i++) {
                        $('#lib_name_'+response[i].libnm).remove();     
                        $('#lib_location_'+response[i].libnm).remove();                     

                    }
                    for (var i = 0; i < response.length; i++) {
                        var libnminput = '<input required  type="text" name="lib_name_'+i+'"  id="lib_name_'+i+'" class="form-control" value="'+response[i].libnm+'"/>';
                        $("#libnamlist").append(libnminput);
                        var liblocinput = '<input required type="text" name="lib_location_'+i+'"  id="lib_location_'+i+'" class="form-control" value="'+response[i].libloc+'"/>';
                        $("#libloclist").append(liblocinput);

                    }

                    var inputsavebutton = '    <div class="row" id="dcinputbutton"><div class="col-md-8"></div><div class="col-md-2"></div><div class="col-md-2"><button type="button" class="btn btn-primary btn-block btn-flat" id="add_snap_btn">Add</button></div></div>';
                    $("#add_snap_fd").append(inputsavebutton);


                }else{

                var msg_line = '<span class="help-block" id="help-block" style="color: red;"><i class="fa fa-times-circle-o"></i> Physical location does not exist for this Data Currency </span>';
                    $("#snap_name_field").append(msg_line);

                 
                    }
     
                }
            });
        }  
    }


</SCRIPT>

<style type="text/css">
    div#rMenu {
        position: absolute;
        visibility: hidden;
        top: 0;
        background-color: #555;
        text-align: left;
        padding: 2px;
    }

    div#rMenu ul li {
        margin: 1px 0;
        padding: 0 5px;
        cursor: pointer;
        list-style: none outside none;
        background-color: #DFDFDF;
    }
</style>


<script>
    $(function () {

        $('#myTable1').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });

    $(function () {
        $('#myTable2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });

    $(function () {
        $('#myTable3').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    });
</script>

<!--  validation modal  -->
<div class="modal fade bs-example-modal-lg" id="lod" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Validation Status</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <br><span>Change Validation Status:</span>
                                        <form action="#" method="post" id="pgm_status_form">
                                            <div class="form-group has-feedback">
                                                <input id="sid" type="hidden" name="sid" value=>

                                                <select onchange="ready_pass(this);" name="pgm_status" id="pgm_status"
                                                        class="form-control">
                                                    <option selected value="1">Select one</option>
                                                    <option value="2">Ready for validation</option>
                                                    <option value="3">Validated</option>
                                                </select>

                                            </div>
                                            <div class="row" id="pfbox1">
                                                <div class="col-md-6">
                                                    <label class="radio-inline"><input type="radio" name="valstat"
                                                                                       id="valstat" value="1" checked>PASS</label>
                                                    <label class="radio-inline"><input type="radio" name="valstat"
                                                                                       id="valstat"
                                                                                       value="0">FAIL</label>
                                                </div>
                                            </div>
                                            <div class="row" id="pfbox2">
                                                <div class="col-md-12">
                                                    <br><span>Comments :</span>
                                                    <div class="form-group">
                                                        <textarea id="pfcmt" name="pfcmt" class="form-control"
                                                                  rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="pfbox3">
                                                <div class="col-md-12">
                                                    <label class="checkbox-inline"><input type="checkbox" name="v_sys[]"
                                                                                          value="0">Independent
                                                        Programming</label>
                                                    <label class="checkbox-inline"><input checked type="checkbox"
                                                                                          name="v_sys[]" value="1">Manual
                                                        Code check</label>
                                                    <label class="checkbox-inline"><input type="checkbox" name="v_sys[]"
                                                                                          value="2">Manual Output Check</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" id="val_sav_btn"
                                                            class="btn btn-primary btn-block btn-flat">Save
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  validation modal  for common programs-->
<div class="modal fade bs-example-modal-lg-cp-val" id="cp_val" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Validation Status</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="cpgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <br><span>Change Validation Status:</span>
                                        <form action="#" method="post" id="cpgm_status_form">
                                            <input id="cp_name" type="hidden" name="cp_name" value=>
                                            <div class="form-group has-feedback">
                                                <select onchange="ready_pass2(this);" name="cpgm_status"
                                                        id="cpgm_status" class="form-control">
                                                    <option selected value="1">Select one</option>
                                                    <option value="2">Ready for validation</option>
                                                    <option value="3">Validated</option>
                                                </select>
                                            </div>

                                            <div class="row" id="cpfbox1">
                                                <div class="col-md-6">
                                                    <label class="radio-inline"><input type="radio" name="valstat"
                                                                                       id="valstat" value="1" checked>PASS</label>
                                                    <label class="radio-inline"><input type="radio" name="valstat"
                                                                                       id="valstat"
                                                                                       value="0">FAIL</label>
                                                </div>
                                            </div>

                                            <div class="row" id="cpfbox2">
                                                <div class="col-md-12">
                                                    <br><span>Comments :</span>
                                                    <div class="form-group">
                                                        <textarea id="pfcmt" name="pfcmt" class="form-control"
                                                                  rows="3"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" id="cpfbox3">
                                                <div class="col-md-12">
                                                    <label class="checkbox-inline"><input type="checkbox" name="v_sys[]"
                                                                                          value="0">Independent
                                                        Programming</label>
                                                    <label class="checkbox-inline"><input checked type="checkbox"
                                                                                          name="v_sys[]" value="1">Manual
                                                        Code check</label>
                                                    <label class="checkbox-inline"><input type="checkbox" name="v_sys[]"
                                                                                          value="2">Manual Output Check</label>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" id="cp_val_sav_btn"
                                                            class="btn btn-primary btn-block btn-flat">Save
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>

                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  program history  -->
<div class="modal fade bs-phist-modal-lg" id="phist_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <!-- <h3 class="box-title">Program History :</h3><br> -->
                    <h3 id="phname" class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="hist_tab" class="table table-hover  table-bordered ">
                                            <thead>
                                            <tr>
                                                <!-- <th>Program</th> -->
                                                <th>Date/time</th>
                                                <th>User Name</th>
                                                <th>Comment</th>
                                                <th>Final Status</th>
                                                <!--                                               <th colspan="2" >Action</th>
                                                 -->
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">


                                    <div class="col-md-3">
                                        <form action="pgmhist_report.php" method="post" target="_blank">

                                            <input type="hidden" id="pid_hist" name="pid_hist" value="1010"><br>

                                            <input class="btn btn-primary btn-block btn-flat" type="submit"
                                                   value="Download as PDF">

                                            <!-- <button type="button" class="btn btn-primary btn-block btn-flat pull-right" data-dismiss="modal">Close</button> -->
                                        </form>

                                        <!-- <a href='tuto1.php' target='_blank' class='demo'>
                                        <button  class="btn btn-primary btn-block btn-flat" >Download PDF</button></a> -->
                                    </div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br>
                                        <button type="button" class="btn btn-primary btn-block btn-flat"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!-- common program history  -->
<div class="modal fade bs-cphist-modal-lg" id="cphist_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <!-- <h3 class="box-title">Program History :</h3><br> -->
                    <h3 id="cphname" class="box-title"></h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="cpgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="cp_hist_tab" class="table table-hover  table-bordered ">
                                            <thead>
                                            <tr>
                                                <!-- <th>Program</th> -->
                                                <th>Date/time</th>
                                                <th>User Name</th>
                                                <th>Comment</th>
                                                <th>Final Status</th>
                                                <!--                                               <th colspan="2" >Action</th>
                                                 -->
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">


                                    <div class="col-md-3">
                                        <form action="pgmhist_report.php" method="post" target="_blank">

                                            <input type="hidden" id="pid_hist" name="pid_hist" value="1010"><br>

                                            <!--                                            <input class="btn btn-primary btn-block btn-flat" type="submit" value="Download as PDF">-->

                                            <!-- <button type="button" class="btn btn-primary btn-block btn-flat pull-right" data-dismiss="modal">Close</button> -->
                                        </form>

                                        <!-- <a href='tuto1.php' target='_blank' class='demo'>
                                        <button  class="btn btn-primary btn-block btn-flat" >Download PDF</button></a> -->
                                    </div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br>
                                        <button type="button" class="btn btn-primary btn-block btn-flat"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!-- existing program list  -->
<div class="modal fade bs-extpgm-modal-lg" id="existing_pgm_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <!-- <h3 class="box-title">Program History :</h3><br> -->
                    <h3 id="phname" class="box-title">Existing Programs</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table id="ext_pgm_tab" class="table table-hover  table-bordered ">
                                            <thead>
                                            <tr>
                                                <!-- <th>Program</th> -->
                                                <th>Program Name</th>
                                                <th>Final Status</th>
                                                <th>Replace Program [<label>Select all <input type="checkbox"
                                                                                              id="checkAll"/> </label>]
                                                </th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">


                                    <div class="col-md-3">
                                        <!--                                        <form action="pgmhist_report.php" method="post" target="_blank">-->

                                        <input type="hidden" id="pid_hist" name="pid_hist" value="1010"><br>

                                        <input class="btn btn-primary btn-block btn-flat" type="submit"
                                               onclick="autogen_pgm_gen();" value="Generate Now">

                                        <!-- <button type="button" class="btn btn-primary btn-block btn-flat pull-right" data-dismiss="modal">Close</button> -->
                                        <!--                                        </form>-->

                                        <!-- <a href='tuto1.php' target='_blank' class='demo'>
                                        <button  class="btn btn-primary btn-block btn-flat" >Download PDF</button></a> -->
                                    </div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br>
                                        <button type="button" class="btn btn-primary btn-block btn-flat"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

<!-- existing program list if no programs found in the list -->
<div class="modal fade bs-noextpgm-modal-lg" id="noexisting_pgm_modal" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <!-- <h3 class="box-title">Program History :</h3><br> -->
                    <h3 id="phname" class="box-title">Existing Programs</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding" style="max-height: calc(100vh - 210px); overflow-y: auto;">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        No Validated programs found, It will create all programs from the tree.
                                    </div>
                                </div>
                                <div class="row">


                                    <div class="col-md-3">
                                        <!--                                        <form action="pgmhist_report.php" method="post" target="_blank">-->

                                        <input type="hidden" id="pid_hist" name="pid_hist" value="1010"><br>

                                        <input class="btn btn-primary btn-block btn-flat" type="submit"
                                               onclick="autogen_pgm_gen();" value="Generate Now">

                                        <!-- <button type="button" class="btn btn-primary btn-block btn-flat pull-right" data-dismiss="modal">Close</button> -->
                                        <!--                                        </form>-->

                                        <!-- <a href='tuto1.php' target='_blank' class='demo'>
                                        <button  class="btn btn-primary btn-block btn-flat" >Download PDF</button></a> -->
                                    </div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br></div>
                                    <div class="col-md-3"><br>
                                        <button type="button" class="btn btn-primary btn-block btn-flat"
                                                data-dismiss="modal">Close
                                        </button>
                                    </div>

                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  upload program  -->
<div class="modal fade bs-example-up-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">upload program</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam_up"></span>
                                    </div>
                                </div>

                                <form action="#" enctype="multipart/form-data" method="post" id="fileHolder">
                                    <div class="form-group has-feedback">
                                        <input id="dl_sid" type="hidden" name="dl_sid" value=>
                                        <p>Select Program file:</p>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                            <span class="btn btn-primary btn-file">
                                                <span class="fileupload-new">Select file</span>
                                                <input required type="file" name="file_upload" id="file_upload"
                                                       onclick="up_pgm_btn_active()">
                                            </span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                               style="float: none" onclick="up_pgm_btn_dactive()">×</a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="checkbox icheck">
                                            </div>
                                        </div><!-- /.col -->
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-primary btn-block btn-flat"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                        <div class="col-xs-2">

                                            <!-- <input  type="submit" id="uppgmbtn" class="btn btn-primary btn-block btn-flat disabled" value="Upload" onclick="up_pgm()" name="submit"> -->
                                            <button type="button" id="uppgmbtn"
                                                    class="btn btn-primary btn-block btn-flat disabled ">Upload
                                            </button>
                                        </div><!-- /.col -->
                                    </div>
                                </form>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>

<!--  Edit Entry model  -->
<div class="modal fade bs-example-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Entry</h3>
                    <div class="box-tools pull-right">
							<span>Study Name : <span><?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">


                                        <form action="#" method="post" id="edit_entry_fd">

                                            <input id="edit_sid" type="hidden" name="edit_sid" value=>

                                            <div id="input_frm_grp_edit_entry_fd" class="form-group has-feedback">



                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">

                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            id="edit_entry_btn">Update
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  upload new common program   -->
<div class="modal fade bs-example-common-up-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Upload Common Programs</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">

                                <form action="#" method="post" enctype="multipart/form-data" id="Holder">
                                    <!-- <div class="form-group has-feedback">
                                              <p>Enter Program Name:</p>
                                              <input type="text" required name="cp_name" id="cp_name" class="form-control" title="Please enter program name" value="<?php //echo set_value('fullname');  ?>" placeholder="Enter Program Name"/>
                                          </div> -->

                                    <div class="form-group has-feedback">
                                        <p><b>Select Program file:</b></p>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                                <span class="btn btn-primary btn-file">
                                                    <span class="fileupload-new">Select file</span>
                                                     <input required type="file" name="fileToUpload" id="fileToUpload"
                                                            onclick="up_cpgm_btn_active()"></span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                               style="float: none" onclick="up_cpgm_btn_dactive()">X</a>
                                        </div>
                                    </div>

                                    <div class="form-group has-feedback">
                                        <p><b>File Location:</b></p>
                                        <input type="text" name="pgm_loc" id="pgm_loc" class="form-control"
                                               value="pgm/"/>
                                    </div>


                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="checkbox icheck">
                                            </div>
                                        </div><!-- /.col -->
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-primary btn-block btn-flat"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-primary btn-block btn-flat disabled"
                                                    id="upcpgmbtn">Upload
                                            </button>
                                        </div><!-- /.col -->
                                    </div>

                                </form>

                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>


<style>

    pre {
        word-wrap: normal;
        height: 500px;
        width: 850px;
        border: 1px solid black;
        padding: 5px;
        overflow: auto;
        display: block;
        margin-left: 100px;
    }

</style>

<!--  Edit program  -->
<div class="modal fade bs-example-editp-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Edit Programs</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="editpgm"></span>
                                    </div>
                                </div>
                                <div class="row">

                                    <!-- HTML form -->
                                    <form id="edtfrm" action="" method="post">
                                        <input id="editpgm_sid" type="hidden" name="editpgm_sid" value=>
                                        <pre><code contenteditable id="prgm" class="language-sas"></code></pre>
                                        <textarea name="prgm" id="prgm1" style="display:none;"></textarea>
                                        <input type="submit" style="margin:10px 20px 10px 30px;" Value="Save"
                                               onclick="var x = $('#prgm').text(); $('#prgm1').text(x); $('#edtfrm').submit();"/>
                                        <input type="reset"/>
                                    </form>

                                </div><!-- /.col -->
                            </div>
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>


<!--  import toc - maintain toc   -->
<div class="modal fade bs-example-import-toc-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Import TOC</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>
                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">

                                <form action="maintain_toc.php" method="post" enctype="multipart/form-data">


                                    <div class="form-group has-feedback">
                                        <p>Select TOC File:</p>
                                        <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <span class="btn btn-primary btn-file"><span class="fileupload-new">Select File</span>
                                                     <input type="file" name="fileToUpload" id="fileToUpload"></span>
                                            <span class="fileupload-preview"></span>
                                            <a href="#" class="close fileupload-exists" data-dismiss="fileupload"
                                               style="float: none">X</a>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xs-8">
                                            <div class="checkbox icheck">
                                            </div>
                                        </div><!-- /.col -->
                                        <div class="col-xs-2">
                                            <button type="button" class="btn btn-primary btn-block btn-flat"
                                                    data-dismiss="modal">Cancel
                                            </button>
                                        </div>
                                        <div class="col-xs-2">
                                            <input type="submit" class="btn btn-primary btn-block btn-flat"
                                                   value="Import" name="submit">
                                        </div><!-- /.col -->
                                    </div>

                                </form>

                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.box-body -->
                </div>
            </div>
        </div>
    </div>
</div>

<!--  loading modal   -->
<div class="modal fade bs-example-loading-modal-sm" id="pgmld" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

            <a href="#" class="thumbnail">
                <img src="stree/metroStyle/img/38.gif" alt="loading...">
            </a>


        </div>
    </div>
</div>


<!--  New entry   -->
<div class="modal fade bs-example-new-entry-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Add New Entry</h3>
                    <div class="box-tools pull-right">
                       <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">


                                        <form action="#" method="post" id="new_entry_fd">
                                            <div class="form-group has-feedback">
                                                <input id="ne_row_id" type="hidden" name="ne_row_id" value=>

                                                <div class="row">
                                                    <div class="col-md-2 form-group has-feedback">
                                                        <p><b> TLF Number :</b></p>
                                                        <input required type="text" name="ne_tlfnum" id="ne_tlfnum"
                                                               class="form-control" value=""/>
                                                    </div>
                                                    <div class="col-md-10 form-group has-feedback">
                                                        <p><b> Enter Output Title :</b></p>
                                                        <input required type="text" name="ne_title" id="ne_title"
                                                               class="form-control" value=""/>
                                                    </div>
                                                </div>

                                                <div class="row">
                                                    <div class="col-md-3 form-group has-feedback">
                                                        <p><b> Enter Program Name :</b></p>
                                                        <input required type="text" name="ne_pgmnam" id="ne_pgmnam"
                                                               class="form-control" value=""
                                                               onchange="ne_pgmnamf(this.value)"/>
                                                    </div>
                                                    <div class="col-md-3 form-group has-feedback">
                                                        <!-- <p><b> Enter Program type:</b></p> -->
                                                        <label for="ne_pgtyp">Select Program Type :</label>
                                                        <select class="form-control" id="ne_pgtyp" name="ne_pgtyp"
                                                                onchange="ne_pgtypf(this.value)">
                                                            <option>Table</option>
                                                            <option>Listing</option>
                                                            <option>Figure</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-2 form-group has-feedback">
                                                        <p><b> Output Number :</b></p>
                                                        <input required type="text" name="ne_outnum" id="ne_outnum"
                                                               class="form-control" value=""
                                                               onchange="ne_outnumf(this.value)"/>
                                                    </div>
                                                    <div class="col-md-2 form-group has-feedback">
                                                        <p><b> Output Name :</b></p>
                                                        <input required type="text" name="ne_outnam" id="ne_outnam"
                                                               class="form-control" value=""/>
                                                    </div>
                                                    <div class="col-md-2 form-group has-feedback">
                                                        <p><b> log Name :</b></p>
                                                        <input required type="text" name="ne_lognam" id="ne_lognam"
                                                               class="form-control" value=""/>
                                                    </div>

                                                </div>
                                                <div class="row">
                                                    <div class="col-md-3 form-group has-feedback">
                                                        <p><b> Population :</b></p>
                                                        <input required type="text" name="population" id="population"
                                                               class="form-control" value=""/>
                                                    </div>
                                                    <div class="col-md-3 form-group has-feedback">
                                                        <p><b> Program location :</b></p>
                                                        <input required type="text" name="ne_pgloc" id="ne_pgloc"
                                                               class="form-control" value=""/>
                                                    </div>
                                                    <div class="col-md-3 form-group has-feedback">
                                                        <p><b> Output location :</b></p>
                                                        <input required type="text" name="ne_outloc" id="ne_outloc"
                                                               class="form-control" value=""/>
                                                    </div>
                                                    <div class="col-md-3 form-group has-feedback">
                                                        <p><b> Shell Name :</b></p>
                                                        <input required type="text" name="shell_name" id="shell_name"
                                                               class="form-control" value=""/>
                                                    </div>
                                                </div>
                                                <div class="row" id="create_entry_param_list_1">
                                                    <div class="col-md-5 form-group has-feedback">
                                                        <p><b> Parameter_1_name :</b></p>
                                                        <input required type="text" name="Parameter_1_name"
                                                               id="Parameter_1_name" class="form-control" value=""/>
                                                    </div>
                                                    <div class="col-md-5 form-group has-feedback">
                                                        <p><b> Parameter_1_value :</b></p>
                                                        <input required type="text" name="Parameter_1_value"
                                                               id="Parameter_1_value" class="form-control" value=""/>
                                                    </div>
                                                    <div id="param_1_btn_list" class="col-md-2 form-group has-feedback">
                                                        <p><b> Add / Remove :</b></p>
                                                        <a class="btn" href="#" id="add_btn_1"
                                                           onclick="paramadd(1);"><span
                                                                class='text-center glyphicon glyphicon glyphicon-plus'></span></a>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            id="new_entry_btn">Create
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

<!--  New section   -->
<div class="modal fade bs-example-new-sec-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Add New Section</h3>
                    <div class="box-tools pull-right">
                            <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">


                                        <form action="#" method="post" id="new_sec_fd">
                                            <div class="form-group has-feedback">
                                                <input id="sec_id" type="hidden" name="sec_id" value=>
                                                <div class="form-group has-feedback">
                                                    <p><b> Enter Section Name :</b></p>
                                                    <input type="text" name="sec_name" id="sec_name"
                                                           class="form-control" value="" required/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            id="new_sec_btn">Insert Section
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--   move section   -->
<div class="modal fade bs-example-mov-sec-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Move Section</h3>
                    <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">


                                        <form id="m_sec_frm" action="study.php" method="post">
                                            <div class="form-group has-feedback">
                                                <!-- <input id="mov_sec_id" type="hidden" name="mov_sec_id" value=> -->
                                                <div class="form-group has-feedback">
                                                    <p><b> Selected Section :</b></p>
                                                    <input type="text" name="mov_sec_id" id="mov_sec_id"
                                                           class="form-control" value="" required/>
                                                </div>
                                                <div class="form-group has-feedback">
                                                    <p><b> Move To Section :</b></p>
                                                    <select id="tar_sec_id" name="tar_sec_id" class="form-control">
                                                        <!-- <option value=""></option> -->

                                                    </select>
                                                    <!-- <input type="text" name="sec_name" id="sec_name" class="form-control" value="" required /> -->
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="submit" class="btn btn-primary btn-block btn-flat">
                                                        Move
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

<!--  Update section header   -->
<div class="modal fade bs-example-update_sechead-modal-lg" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Update Section Header</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name :<?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="secpgnam1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">


                                        <form action="#" method="post" id="update_sec_head_fd">
                                            <div class="form-group has-feedback">
                                                <input id="up_sec_id" type="hidden" name="up_sec_id" value=>
                                                <div class="form-group has-feedback">
                                                    <p><b>Section Name :</b></p>
                                                    <input type="text" name="up_sec_name" id="up_sec_name"
                                                           class="form-control" value="" required/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            id="update_sec_head_btn">Update
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  New sub section   -->
<div class="modal fade bs-example-new-sub-sec-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Add New Sub Section</h3>
                    <div class="box-tools pull-right">
                                                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam1"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">

                                        <form action="#" method="post" id="new_sub_sec_fd">
                                            <div class="form-group has-feedback">
                                                <input id="sub_sec_id" type="hidden" name="sub_sec_id" value=>
                                                <div class="form-group has-feedback">
                                                    <p><b> Enter Sub Section Name :</b></p>
                                                    <input type="text" name="sub_sec_name" id="sub_sec_name"
                                                           class="form-control" value="" required/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-xs-8">
                                                    <div class="checkbox icheck">
                                                    </div>
                                                </div><!-- /.col -->
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            data-dismiss="modal">Cancel
                                                    </button>
                                                </div>
                                                <div class="col-xs-2">
                                                    <button type="button" class="btn btn-primary btn-block btn-flat"
                                                            id="new_sub_sec_btn">Insert Sub Section
                                                    </button>
                                                </div><!-- /.col -->
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  section delete confirmation   -->

<div class="modal fade bs-example-delcon-modal-sm" id="ld" tabindex="-1" role="dialog"
     aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title" style="color:red;"> Do you want to delete ?</h3>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-8">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="delseccon"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">

                                            <div class="col-xs-6">
                                                <button onclick="del_sec2();" type="button"
                                                        class="btn btn-primary btn-block btn-flat" data-dismiss="modal">
                                                    Yes
                                                </button>
                                            </div>
                                            <div class="col-xs-6">
                                                <button type="button" class="btn btn-primary btn-block btn-flat"
                                                        data-dismiss="modal">No
                                                </button>
                                            </div><!-- /.col -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


</body>
</html>
