<?php 

	include("connect.php");

	//Copying all log file to the pgm directory;
	function copyToDir($pattern, $dir){
	  foreach (glob($pattern) as $file){
	      if(!is_dir($file) && is_readable($file))
	      {
	          $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
	          copy($file, $dest);
	          unlink($file);
	      }
	  }
	}
	function run_cron($study_name, $tosearchid){
		$username = 'By MeD-OMS';

		$ses_sql=$conn->query("SELECT study, pgmname, pgmloc,logname from toc_$study_name where sortorder='$tosearchid' AND data_currency='C' ");

	  while($row = $ses_sql->fetch_assoc()) {
	    $study = $row['study'];
	    $pgmname = $row['pgmname'];  
	    $pgmloc = $row['pgmloc']; 
	    $logname = $row['logname'];                      
	  }
	  //Create log location 
	  $logloc=substr_replace($pgmloc," ",strripos($pgmloc,"/"));
	  // echo "$logloc";

	  $pgmfileloc = $file_server.$study.'/'.$pgmloc;
	  $loglocfile = $file_server.$study.'/'.substr_replace($pgmloc,$logname,strripos($pgmloc,"/")+1); 
	   //echo "$loglocfile";

	  //check snap and update studyauto pgm

	  $inlib='newd';
	  $inads='ads';
	  $dc_db_val='C';
  
	  $reading = fopen($file_server.$study. DIRECTORY_SEPARATOR .'pgm/studyini.sas', 'r');
	  $writing = fopen($file_server.$study. DIRECTORY_SEPARATOR .'pgm/studyini.sas.tmp', 'w');

	  $replaced = false;

	  while (!feof($reading)) {
	    $line = fgets($reading);
	    if (stristr($line,'%LET ads')) {
	      $line = "%LET ads = ".$inads.";\n";
	      $replaced = true;
	    }
	    if (stristr($line,'%LET inlib')) {
	      $line = "%LET inlib = ".$inlib.";\n";
	      $replaced = true;
	    }
	    fputs($writing, $line);
	  }


	  fclose($reading); fclose($writing);
	  // might as well not overwrite the file if we didn't replace anything
	  if ($replaced) 
	  {
	    rename($file_server.$study. DIRECTORY_SEPARATOR .'pgm/studyini.sas.tmp', $file_server.$study. DIRECTORY_SEPARATOR .'pgm/studyini.sas');
	  } else {
	    unlink($file_server.$study. DIRECTORY_SEPARATOR .'pgm/studyini.sas.tmp');
	  }


	  //Execute SAS program
	  $command= '"'.$sas_soft.'" -batch -uprint -uprintmenuswitch -nosplash -icon -SYSIN "'.$pgmfileloc.'"';
	  // echo "<script> console.log(".$command."); </script>"; 
	  exec($command, $output); 
		copyToDir('../ajax/*.log', $file_server.$study.'/'.$logloc);
	  //Check log file for error and warning and send it to the user 
	  $myFile = $loglocfile;

	  $file_handler = fopen($myFile, "r")
	  or die ("Can't open the File.");

	  $err=0;
	  $war=0;
	  while (!feof($file_handler)) {
	    $dataline = fgets($file_handler);
	    
	    if (strpos($dataline,'ERROR') !== false) {
	        $err++;
	    }
	    if (strpos($dataline,'WARNING') !== false) {
	        $war++;
	    }
	  }


		////Updating output date;
		date_default_timezone_set('Asia/Dhaka');
		$today = date("Y-m-d H:i:s");

	  if($err==0){
	  	$out_date = "UPDATE toc_status_$study SET odate_0='$today',outstat='2' where sortorder IN (SELECT sortorder FROM toc_$study_name WHERE pgmloc='$pgmloc') AND data_currency='C'";
	  	$conn->query($out_date);

	  date_default_timezone_set('Asia/Dhaka');
	  $datetime=date("Y_m_d_H_i_s");
	  $filelink=$datetime.'_'.$pgmname;
	  //echo $stat;
	  $status="Program running";
	  $result=$conn->query("INSERT INTO pgm_hist_$study_name  (sortorder,pgmname,event_date,username,comment,status,link)
	    VALUES ('$tosearchid','$pgmname','$today','$username','Run Program','$status','$filelink')");
		}
	}
?>