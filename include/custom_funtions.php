<?php

function toc_gen($conn) {

	// include("include/connect.php");
	/** Error reporting */
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);
	ini_set('display_startup_errors', TRUE);
	include("Classes/PHPExcel.php");
	date_default_timezone_set('Asia/Dhaka');
	$datetime=date("Y_m_d_H_i_s");

	$study_name=$_SESSION["study"];

	$result44=$conn->query("SELECT * FROM study_info WHERE study_name = '".$study_name."' ");
	while($row = $result44->fetch_assoc()) {
	    $file_server = $row['study_loc'].DIRECTORY_SEPARATOR;
	}
	$username = $_SESSION['login_user'];


	//Copying all log file to the pgm directory;
	function copyToDir($pattern, $dir,$file_server,$study_name,$datetime){
	    foreach (glob($pattern) as $file){
	        if(!is_dir($file) && is_readable($file))
	        {
	            $dest = realpath($dir) . DIRECTORY_SEPARATOR . basename($file);
	            copy($file, $dest);
				rename($file_server.$study_name.'/lst/'.$datetime.$study_name.'_toc.xlsx',$file_server.$study_name.'/lst/toc.xlsx');
	            unlink($file);
	        }
	    }
	}

	//echo "[inside ajax:]";

	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();

	// Set document properties
	$objPHPExcel->getProperties()->setCreator($username)
	    ->setLastModifiedBy($username)
	    ->setTitle("Office 2007 XLSX Test Document")
	    ->setSubject("Office 2007 XLSX Test Document")
	    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
	    ->setKeywords("office 2007 openxml php")
	    ->setCategory("Test result file");

	//need to check and update status column when first time inserted in to toc tree
	//select existing validated program
	//update toc tree according to the current status
	//then select all information from toc table


	$result=$conn->query("select * from toc_$study_name");

	$columnArray = array("A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
		"AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI", "AJ", "AK", "AL", "AM", "AN", "AO", "AP", "AQ", "AR", "AS", "AT", "AU", "AV", "AW", "AX", "AY", "AZ",
		"BA", "BB", "BC", "BD", "BE", "BF", "BG", "BH", "BI", "BJ", "BK", "BL", "BM", "BN", "BO", "BP", "BQ", "BR", "BS", "BT", "BU", "BV", "BW", "BX", "BY", "BZ",
		"CA", "CB", "CC", "CD", "CE", "CF", "CG", "CH", "CI", "CJ", "CK", "CL", "CM", "CN", "CO", "CP", "CQ", "CR", "CS", "CT", "CU", "CV", "CW", "CX", "CY", "CZ",
        "DA", "DB", "DC", "DD", "DE", "DF", "DG", "DH", "DI", "DJ", "DK", "DL", "DM", "DN", "DO", "DP", "DQ", "DR", "DS", "DT", "DU", "DV", "DW", "DX", "DY", "DZ");

	$rowCount = 1;
	// fetch result set column information
	$finfo = mysqli_fetch_fields($result);

	$columnlenght = 0;
	foreach ($finfo as $val) {
	// set column header values
	    $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$columnlenght++] . $rowCount, $val->name);
	}
	$objPHPExcel->getActiveSheet()->getStyle($columnArray[0]."1:".$columnArray[$columnlenght]."1")->getFont()->setBold(true);

	$rowCount++;
	// Iterate through each result from the SQL query in turn
	// We fetch each database result row into $row in turn

	while ($row = mysqli_fetch_array($result)) {
	    for ($i = 0; $i < mysqli_num_fields($result); $i++) {
	        $objPHPExcel->getActiveSheet()->SetCellValue($columnArray[$i] . $rowCount, $row[$i]);
	    }
	    $rowCount++;
	}

	// Rename worksheet
	$objPHPExcel->getActiveSheet()->setTitle('toc');

	// Set active sheet index to the first sheet, so Excel opens this as the first sheet
	$objPHPExcel->setActiveSheetIndex(0);
	$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	$objWriter->save($datetime.$study_name.'_toc.xlsx');
	copyToDir($datetime.$study_name.'_toc.xlsx', $file_server.$study_name.'/lst',$file_server,$study_name,$datetime);
}

