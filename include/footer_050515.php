<?php

error_reporting(E_ALL ^ E_NOTICE);
require_once 'include/excel_reader2.php';
include("include/connect.php");
?>
<footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2015 <a href="http://toc.com">SCBD</a>.</strong> All rights reserved.
</footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->

    <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js" type="text/javascript"></script>

    <!-- study titles tree  -->
    <script type="text/javascript" src="stree/jquery.ztree.core-3.5.js"></script>
    <script type="text/javascript" src="stree/jquery.ztree.excheck-3.5.js"></script>
    <script type="text/javascript" src="stree/jquery.ztree.exedit-3.5.js"></script>
    <!-- study titles tree  -->

<script>
    !function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)
</script>

<SCRIPT type="text/javascript">

        var setting = {
            view: {
                // addHoverDom: addHoverDom,
                // removeHoverDom: removeHoverDom,
                selectedMulti: true
            },
            check: {
                enable: false
            },
            callback: {
                onRightClick: OnRightClick
            },
            data: {
                simpleData: {
                    enable: true
                }
            },
            edit: {
                enable: false
            }
        };



        var zNodes =[

                        <?php
                        $study_name=$_SESSION["study"];
                        $sql="SELECT * FROM toc_" . $study_name . " AS x ,toc_status_" . $study_name . " AS y WHERE x.study=y.study AND x.sortorder=y.sortorder ORDER BY x.sortorder";
                        $result = $conn->query($sql);

                        $count=1;
                        if ($result->num_rows > 0) {

                          while($row = $result->fetch_assoc()) {
                              $count++;
                              $pgmstat=$row["pgmstat"];
                              $outstat=$row["outstat"];
//                              $outstat=2;
//                              $pgmstat=3;

                                  //code for name argument
                                  if($row["type"]=="Table"){
                                      $name=$row["type"].$row["l1"].".".$row["l2"].".".$row["l3"].".".$row["l4"]." ".$row["title"];
                                      if($row["l4"]==0)
                                          $name=$row["type"].$row["l1"].".". $row["l2"].".". $row["l3"]." ".$row["title"];
                                      if($row["l3"]==0)
                                          $name=$row["type"].$row["l1"].".". $row["l2"]." ".$row["title"];
                                      if($row["l2"]==0)
                                          $name=$row["type"].$row["l1"]." ".$row["title"];
                                  }

                                  else
                                  {
                                      $name=$row["l1"].".". $row["l2"].".". $row["l3"].".". $row["l4"]." ".$row["section"];
                                      if($row["l4"]==0)
                                          $name=$row["l1"].".". $row["l2"].".". $row["l3"]." ".$row["section"];
                                      if($row["l3"]==0)
                                          $name=$row["l1"].".". $row["l2"]." ".$row["section"];
                                      if($row["l2"]==0)
                                          $name=$row["l1"]." ".$row["section"];
                                  }

                                  //code for pid argument
                                  if ($row["l1"]==15.1)
                                    $row["l1"]="1";
                                  if ($row["l1"]==15.2)
                                    $row["l1"]="2";
                                  if ($row["l1"]==15.3)
                                    $row["l1"]="3";
                                  if ($row["l2"]==0)
                                    $row["l2"]=" ";
                                  if ($row["l3"]==0)
                                    $row["l3"]=" ";
                                  if ($row["l4"]==0)
                                    $row["l4"]=" ";
                                  $allnum=$row["l1"]. $row["l2"]. $row["l3"]. $row["l4"];
                                  //echo "$allnum";
                                  $len=iconv_strlen (trim($allnum));
                                  if ($len==1)
                                    $p="0";
                                  else
                                    $p=substr(trim($allnum),0,$len-1);

                                  $lst= "{id: ".$row["l1"]."". $row["l2"]."". $row["l3"]."". $row["l4"]." ,pId:".$p.", name: '".$name."'}," ;
                                  if( $row["type"]=="Table")
                                  $lst= "{id: ".$row["l1"]."". $row["l2"]."". $row["l3"]."". $row["l4"]." ,pId:".$p.", name: '".$name."',icon:'./stree/metroStyle/img/state_".$pgmstat."_".$outstat.".png'}," ;
                                  if( $count-1 == $result->num_rows)
                                  $lst= "{id: ".$row["l1"]."". $row["l2"]."". $row["l3"]."". $row["l4"]." ,pId:".$p.", name: '".$name."',icon:'./stree/metroStyle/img/state_".$pgmstat."_".$outstat.".png'}" ;
                                  echo "$lst";
                                     }
                                  } else {
                                      echo "0 results";
                                  }
                            ?>

                       ];
        
        var pgmid;
        var pgmname;
        function OnRightClick(event, treeId, treeNode) {
            pgmid=treeNode.id;
            pgmname=treeNode.name;

            if (!treeNode && event.target.tagName.toLowerCase() != "button" && $(event.target).parents("a").length == 0) {
                zTree.cancelSelectedNode();
                showRMenu("root", event.clientX, event.clientY);
            } else if (treeNode && !treeNode.noR) {
                zTree.selectNode(treeNode);
                showRMenu("node", event.clientX, event.clientY);
            }

        }

        function showRMenu(type, x, y) {
            $("#rMenu ul").show();
            if (type=="root") {
                $("#m_al").show();
                $("#m_run").hide();
                $("#m_out").hide();
                $("#m_log").hide();
                $("#m_val").hide();
            } else {
                $("#m_run").show();
                $("#m_out").show();
                $("#m_log").show();
                $("#m_val").show();
            }
            rMenu.css({"top":y-130+"px", "left":x-280+"px", "visibility":"visible"});

            $("body").bind("mousedown", onBodyMouseDown);
        }

        function hideRMenu() {
            if (rMenu) rMenu.css({"visibility": "hidden"});
            $("body").unbind("mousedown", onBodyMouseDown);
        }

        function onBodyMouseDown(event){
            if (!(event.target.id == "rMenu" || $(event.target).parents("#rMenu").length>0)) {
                rMenu.css({"visibility" : "hidden"});
            }
        }

        var addCount = 1;
        function addTreeNode() {
            hideRMenu();
            var newNode = { name:"newNode " + (addCount++)};
            if (zTree.getSelectedNodes()[0]) {
                newNode.checked = zTree.getSelectedNodes()[0].checked;
                zTree.addNodes(zTree.getSelectedNodes()[0], newNode);
            } else {
                zTree.addNodes(null, newNode);
            }
        }

        function removeTreeNode() {
            hideRMenu();
            var nodes = zTree.getSelectedNodes();
            if (nodes && nodes.length>0) {
                if (nodes[0].children && nodes[0].children.length > 0) {
                    var msg = "If you delete this node will be deleted along with sub-nodes. \n\nPlease confirm!";
                    if (confirm(msg)==true){
                        zTree.removeNode(nodes[0]);
                    }
                } else {
                    zTree.removeNode(nodes[0]);
                }
            }
        }

        function checkTreeNode(checked) {
            var nodes = zTree.getSelectedNodes();
            if (nodes && nodes.length>0) {
                zTree.checkNode(nodes[0], checked, true);
            }
            hideRMenu();
        }

        function resetTree() {
            hideRMenu();
            $.fn.zTree.init($("#treeDemo"), setting, zNodes);
        }

        function alr(treeId, treeNode, clickFlag){
            alert("table id "+pgmid);
        }

        function runpgm() {
            if (pgmid=="") {
                document.getElementById("txtHint").innerHTML="";
                return;
            }
            if (window.XMLHttpRequest) {
                // code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp=new XMLHttpRequest();
            } else { // code for IE6, IE5
                xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange=function() {
                if (xmlhttp.readyState==4 && xmlhttp.status==200) {
                    //document.getElementById("txtHint").innerHTML=xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET","http://localhost/toc/ajax/program_run.php?q="+pgmid,true);
            xmlhttp.send();
            hideRMenu();

            alert("NOTE: One SAS program executed");
            location.reload();

        }

        function showout() {


            hideRMenu();
            open("http://localhost/toc/study/1264_0003/lst/disp_t1.lst");
            //alert("NOTE: One SAS program executed");
        }

         function showlog() {


            hideRMenu();
            open("http://localhost/toc/study/1264_0003/lst/disp_t1.lst");
            //alert("NOTE: One SAS program executed");
        }

        function validation() {
            document.getElementById("pgnam").innerHTML=pgmname;

            var x = document.getElementById("sid");
            x.value=pgmid;
            //x.style.color = "red";pgnam
            hideRMenu();
            //open("http://localhost/toc/study/1264_0003/lst/disp_t1.lst");
            //alert("NOTE: One SAS program executed");
        }

        function uploadp() {
            document.getElementById("pgnam").innerHTML=pgmname;

            var x = document.getElementById("dl_sid");
            x.value=pgmid;
            //x.style.color = "red";pgnam
            hideRMenu();
            //open("http://localhost/toc/study/1264_0003/lst/disp_t1.lst");
            //alert("NOTE: One SAS program executed");
        }

        // function Download(url) {
            // document.getElementById('my_iframe').src = url;
        // };
        function downloadp() {
            
            var data = {
                  "pgmid": pgmid,
                  "pgmname": pgmname
          };
            $.ajax({
            type: "POST",
            dataType: "json",
            url: "http://localhost/toc/ajax/program_download.php", //Relative or absolute path to response.php file
            data: data,
            success: function(response) {
               // Download(response.path);
               console.log(response.path);
                open(response.path);
              }
           });
              
          hideRMenu();
        }

       var etitle,epgmname,eoutname,elogname;
        function editentr() {
            document.getElementById("pgnam1").innerHTML=pgmname;

            var x = document.getElementById("edit_sid");
            x.value=pgmid;
            
            var data = {
                 "pgmid": pgmid,
                 "pgmname": pgmname
                   };
                     $.ajax({
                     type: "POST",
                     dataType: "json",
                     url: "http://localhost/toc/ajax/edit_entry.php", //Relative or absolute path to response.php file
                     data: data,
                     success: function(response) {
                      etitle=response.title;
                      epgmname=response.pgmname;
                      eoutname=response.outname;
                      elogname=response.logname;
                      console.log(etitle);
                    document.getElementById("title").value=etitle;
                    document.getElementById("pgmname").value=epgmname;
                    document.getElementById("outname").value=eoutname;
                    document.getElementById("logname").value=elogname;
                       }
                    });
                   hideRMenu();
            }

            var zTree, rMenu;
		        $(document).ready(function(){
		            $.fn.zTree.init($("#treeDemo"), setting, zNodes);
		            zTree = $.fn.zTree.getZTreeObj("treeDemo");
		            rMenu = $("#rMenu");
		        });
		        //-->
		    </SCRIPT>
		    <style type="text/css">
		    div#rMenu {position:absolute; visibility:hidden; top:0; background-color: #555;text-align: left;padding: 2px;}
		    div#rMenu ul li{
		    margin: 1px 0;
		    padding: 0 5px;
		    cursor: pointer;
		    list-style: none outside none;
		    background-color: #DFDFDF;
		}
    </style>

    <!-- <iframe id=my_iframe style='display:none;'> -->

    <!--  validation model  -->
                <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                     <div class="modal-dialog modal-lg">
                         <div class="modal-content">
                              <div class="box box-success">
                                   <div class="box-header with-border">
                                        <h3 class="box-title">Validation Program </h3>
                                        <div class="box-tools pull-right">
                                            <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                                        </div>
                                   </div><!-- /.box-header -->

                                  <div class="box-body no-padding">
                                        <div class="row">
                                            <div class="col-md-12 col-sm-8">
                                                <div class="pad">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <span id="pgnam"></span>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <span>Change Program Status:</span>
                                                            <form action="study.php" method="post">
                                                                <div class="form-group has-feedback">
                                                                    <input id="sid" type="hidden" name="sid" value=>

                                                                    <select name="study_status" class="form-control">
                                                                        
                                                                        <option value="1">To be validate</option>;
                                                                        <option value="2">To be modify</option>;
                                                                        <option value="3">Validated</option>;
                                                                    </select>

                                                                </div>
                                                                <div class="row">
                                                                    <div class="col-xs-8">
                                                                        <div class="checkbox icheck">

                                                                        </div>
                                                                    </div><!-- /.col -->
                                                                    <div class="col-xs-4">
                                                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                                                                    </div><!-- /.col -->
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- /.col -->
                                        </div><!-- /.row -->
                                  </div><!-- /.box-body -->
                              </div>
                         </div>
                     </div>
                </div>

<!--  view output model   -->
<div class="modal fade bs-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Program Output</h3>
                    <div class="box-tools pull-right">
                                            <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <?php
                                    $myFile = "disp_t1.lst";

                                    $file_handler = fopen($myFile, "r")
                                    or die ("Can't open SEG File.");

                                    while (!feof($file_handler)) {
                                        $dataline = fgets($file_handler);

                                        echo str_replace(' ', '&nbsp;&nbsp;', $dataline) . "<br>";
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

<!--  upload program model   -->
<div class="modal fade bs-example-up-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">upload program</h3>
                    <div class="box-tools pull-right">
                                            <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                               <form action="study.php" method="post" enctype="multipart/form-data">
                                          

                                          <div class="form-group has-feedback">
                                              <input id="dl_sid" type="hidden" name="dl_sid" value=>
                                              <p>Select Program file:</p>
                                              <div class="fileupload fileupload-new" data-provides="fileupload">
                                             <span class="btn btn-primary btn-file"><span class="fileupload-new">Select file</span>
                                                     <input  type="file" name="fileToUpload" id="fileToUpload"></span>
                                                   <span class="fileupload-preview"></span>
                                                  <a href="#" class="close fileupload-exists" data-dismiss="fileupload" style="float: none">×</a>
                                              </div>

                                          </div>

                                          <div class="row">
                                              <div class="col-xs-8">
                                                  <div class="checkbox icheck">
                                                  </div>
                                              </div><!-- /.col -->
                                              <div class="col-xs-4">
                                                  <input type="submit" class="btn btn-primary btn-block btn-flat" value="Upload" name="submit">
                                              </div><!-- /.col -->
                                          </div>
                               </form>

                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>
</div>

<!--  view output model   -->
<div class="modal fade bs-example1-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:90%;font-family:monospace;">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Program Output</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <?php
                                    $myFile = "disp_t1.lst";

                                    $file_handler = fopen($myFile, "r")
                                    or die ("Can't open the File.");

                                    while (!feof($file_handler)) {
                                        $dataline = fgets($file_handler);

                                        echo str_replace(' ', "&nbsp;", $dataline) . "<br>";
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>


<!--  view Log model   -->
<div class="modal fade bs-example2-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="width:80%;font-family:monospace;">
        <div class="modal-content">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Program Log</h3>
                    <div class="box-tools pull-right">
                        <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                    </div>
                </div><!-- /.box-header -->

                <div class="box-body no-padding">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="pad">
                                <div class="row">
                                    <div class="col-md-12">
                                        <span id="pgnam"></span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                    <?php
                                    $myFile = "disp.log";

                                    $file_handler = fopen($myFile, "r")
                                    or die ("Can't open the File.");

                                    while (!feof($file_handler)) {
                                        $dataline = fgets($file_handler);

                                        echo str_replace(' ', "&nbsp;", $dataline) . "<br>";
                                    }
                                    ?>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.col -->
                    </div><!-- /.row -->
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>
</div>

<!--  Edit Entry model  -->
<div class="modal fade bs-example-edit-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
     <div class="modal-dialog modal-lg">
         <div class="modal-content">
              <div class="box box-success">
                   <div class="box-header with-border">
                        <h3 class="box-title">Edit Entry</h3>
                        <div class="box-tools pull-right">
                            <span>Study Name : <span><?php echo $_SESSION["study"] ?></span>

                        </div>
                   </div><!-- /.box-header -->

                  <div class="box-body no-padding">
                        <div class="row">
                            <div class="col-md-12 col-sm-8">
                                <div class="pad">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span id="pgnam1"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <span>Edit Program Title:</span><br/>
                                            
                                            <form action="study.php" method="post">
                                                <div class="form-group has-feedback">
                                                    <input id="edit_sid" type="hidden" name="edit_sid" value=>
                                                    <?php 
                                                    
                                                    /*$ses_sql=$conn->query("select title, pgmname, outname, logname from toc_1264_0003 where sortorder='11300' ");

                                                    while($row = $ses_sql->fetch_assoc()) 
                                                    {
                                                      $title = $row['title'];
                                                      $pgmname = $row['pgmname'];  
                                                      $outname = $row['outname']; 
                                                      $logname = $row['logname'];                                                     
                                                       }*/
                                                    ?>                       
                                                     <div class="form-group has-feedback">
                                                       <p><b>Program Title:</b></p>
                                                       <input type="text" name="title" id="title" class="form-control" value=""/>
                                                     </div>

                                                     <div class="form-group has-feedback">
                                                       <p><b>Program Name:</b></p>
                                                       <input type="text" name="pgmname" id="pgmname" class="form-control" value=""/>
                                                     </div>

                                                     <div class="form-group has-feedback">
                                                       <p><b>Output Name:</b></p>
                                                       <input type="text" name="outname" id="outname" class="form-control" value=""/>
                                                     </div>
                                                     <div class="form-group has-feedback">
                                                       <p><b>Log Name:</b></p>
                                                       <input type="text" name="logname" id="logname" class="form-control" value=""/>
                                                     </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-xs-8">
                                                        <div class="checkbox icheck">

                                                        </div>
                                                    </div><!-- /.col -->
                                                    <div class="col-xs-4">
                                                        <button type="submit" class="btn btn-primary btn-block btn-flat">Save</button>
                                                    </div><!-- /.col -->
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                  </div><!-- /.box-body -->
              </div>
         </div>
     </div>
</div>

  </body>
</html>
