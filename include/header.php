<?php

include("include/connect.php");
session_start();

$user_check=$_SESSION['login_user'];

$ses_sql=$conn->query("SELECT * FROM user_info WHERE username='$user_check' ");
//print_r($ses_sql);
while($row = $ses_sql->fetch_assoc()) {
    $login_session = $row['username'];
    $user_id=$row['user_id'];
	$usertype = $row['user_type'];
    $firstlog=$row['firstlog'];
    $prev_date=$row['creation_date']; 
}

if(!isset($login_session))
{
    header("Location: index.php");
}


date_default_timezone_set('Asia/Dhaka');
$today = date("Y-m-d H:i:s");
//$prev_date="2015-06-10 12:44:34";
$diff = abs(strtotime($today) - strtotime($prev_date));
$interval = floor($diff / (60*60*24));
$_SESSION['login_interval']=$interval;

if($interval>=90 OR trim($firstlog)=='Yes'){
  header("location:change_pass.php");
}
 
$username = $_SESSION['login_user'];
$sql="SELECT count(y.allocation_id) as snum FROM user_info as x , study_allocation as y WHERE x.username ='$username' AND x.user_id = y.user_id AND y.status='ON'";
$countstudy = $conn->query($sql);

while($row = $countstudy->fetch_assoc()) {
    $snum = $row['snum'];
}

if(isset($_POST['study_name'])){
    $stdname=$_POST['study_name'];
    $_SESSION["study"] = $stdname;
}

if(isset($_SESSION["study"])){
    $study_name=$_SESSION["study"];
}else{
  header("Location: home.php");
}


$sql_sid="SELECT * FROM study_info WHERE study_name='$study_name'";
$result_sid=$conn->query($sql_sid);

while($row = $result_sid->fetch_assoc()) {
    $study_id = $row['study_id'];
}

$sql="SELECT * FROM study_allocation WHERE user_id='$user_id' AND study_id='$study_id'";
$result=$conn->query($sql);

while($row = $result->fetch_assoc()) {
    $usertype = $row['user_type'];
}


$sql45="SELECT DISTINCT snap_name,id,is_lock FROM snap_$study_name";
$snap_list=$conn->query($sql45);


// Function to get the snapshot name
function get_snapshot_name($snap_id, $study_name, $conn)
{
	$sql_get_snap="SELECT * FROM snap_$study_name WHERE id =$snap_id LIMIT 1";
	$snap_name=$conn->query($sql_get_snap);

	while($row = $snap_name->fetch_assoc()) {
		$snapshot = $row['snap_name'];
	}
	return $snapshot;
}


if($usertype == 0)
{
  //echo $usertype;
  header("Location: index.php");
}

?>    

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>MeD-OMS  </title>
<link href="js/jquery.simple-dtpicker.css" rel="stylesheet">

<link rel="stylesheet" href="stree/demo.css" type="text/css">

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- Bootstrap 3.3.2 -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- FontAwesome 4.3.0 -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons 2.0.0 -->
<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
<!-- css top menu -->
<link rel="stylesheet" href="cssmenu/menustyles.css">
<!-- bootstrap wysihtml5 - text editor -->
<link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
<!-- bootstrap datatable -->
<link href="plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="dist/img/scbd.ico">

<link rel="stylesheet" href="stree/metroStyle/metroStyle.css" type="text/css">

<script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>

<!-- <link type="text/css" rel="stylesheet" href="fpdf.css"> -->

<style>
  .scbd {
    height:80px;
    background:#ffffff url('dist/img/logo.png') no-repeat;
    text-align: center;
    position: relative;
    background-position:center;
    z-index:1000;
  }

  li, span{
    font-size:17px;
  }

<?php
 
 $sql_sd="SELECT * FROM toc_$study_name ";
 $result_sd=$conn->query($sql_sd);
 $count=0;
 while($row = $result_sd->fetch_assoc()) {
    $count=$count+1;

    echo "li#treeDemo_".$count.".level1{
            background-color: #f4f4f4;
            border: 1px solid #ddd;
            margin-bottom: 5px;
          }";
}
?>

</style>

<script>
(function worker() {
  $.ajax({
    url: 'ajax/cron_program_run.php', 
    success: function(data) {
	      
    },
    complete: function() {
      setTimeout(worker, 50000);
    }
  });
})();

</script>
</head>

<body class="skin-blue layout-wide">

<div class="scbd"></div>

<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="home.php" class="logo" ><b>MeD-OMS</b></a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top" role="navigation" >
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>


      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

        <?php
            if ($usertype==3) {
                echo '
                    <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs">'.$username.'</span>
            </a>
            <ul class="dropdown-menu">

              <li class="user-footer">
                <div class="pull-left">
                 <p> <small>New Study: '.$snum.' Total Study: '.$snum.' </small></p>
                </div>               

                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>

            ';
            }
            else{
            echo '

          <!-- drop down menu for user-->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-user"></i>
              <span class="hidden-xs"> '.$username.' </span>
            </a>
            <ul class="dropdown-menu">

              <li class="user-footer">
                <div class="pull-left">
                 <p> <small>New Study: '.$snum.' Total Study: '.$snum.'  </small></p>
                </div>

                  <div class="pull-left">
                <a href="change_pass.php" class="btn btn-default btn-flat">Change Password</a>
                </div>

                <div class="pull-right">
                  <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
            '; }
            ?>
          <!-- drop down menu for user-->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="padding-top: 80px;">

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
		<?php
		//Report menu
		$reports_menu = 
		'<ul class="treeview-menu" style="display: none;">
            <li><a href="report_out.php"><i class="fa fa-circle-o"></i> Output Status By <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Table Number</a></li>
            <li><a href="report_out_pgm.php"><i class="fa fa-circle-o"></i> Output Status By <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Program Name </a></li>
            <li><a href="report_pgm.php"><i class="fa fa-circle-o"></i> Program Status</a></li>
			<li><a href="program_run_history.php"><i class="fa fa-circle-o"></i>Program Run History</a></li>
        </ul>';
        if ($usertype==3) {
            
        echo '

        <li>
          <a href="search.php">
            <i class="fa fa-search"></i> <span>Search</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li>
          <a href="home.php">
            <i class="fa fa-home"></i> <span>Home</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
          </a>'.
         $reports_menu.'
        </li>

        <li>
          <a href="create_pdf.php">
            <i class="fa fa-file-pdf-o"></i> <span>Create PDF</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
        
        <li>
          <a href="create_rtf.php">
            <i class="fa fa-file-word-o"></i> <span>Create RTF</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

            ';
        }
        else if ($usertype==2) {
                                  echo '
        

        <li>
          <a href="search.php">
            <i class="fa fa-search"></i> <span>Search</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li>
          <a href="home.php">
            <i class="fa fa-home"></i> <span>Home</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


        <li>
          <a href="study.php">
            <i class="fa fa-book"></i> <span>Study Tree</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Reports</span> <i class="fa fa-angle-left pull-right"></i>
          </a>'.
          $reports_menu.
		'</li>

        <li>
          <a href="create_pdf.php">
            <i class="fa fa-file-pdf-o"></i> <span>Create PDF</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
        <li>
          <a href="create_rtf.php">
            <i class="fa fa-file-word-o"></i> <span>Create RTF</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
        <li>
          <a href="execute_program.php">
            <i class="fa fa-caret-square-o-right"></i> <span>Execute All Programs</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
        <li>
          <a href="manage_program.php">
            <i class="fa fa-list"></i> <span>Manage Common Programs</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

         <li>
          <a href="change_pass.php">
            <i class="fa fa-th"></i> <span>Change Password</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
            ';
        }
        else{
         echo'


        <li>
          <a href="search.php">
            <i class="fa fa-search"></i> <span>Search</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
        
        <li>
          <a href="home.php">
            <i class="fa fa-home"></i> <span>Home</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


        <li>
          <a href="study.php">
            <i class="fa fa-book"></i> <span>Study Tree</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li class="treeview">
          <a href="#">
            <i class="fa fa-list-alt"></i> <span>Reports</span> <i class="fa fa-angle-down pull-right"></i>
          </a>'.
          $reports_menu.'
        </li>

        <li>
          <a href="create_pdf.php">
            <i class="fa fa-file-pdf-o"></i> <span>Create PDF</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li>
          <a href="create_rtf.php">
            <i class="fa fa-file-word-o"></i> <span>Create RTF</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li>
          <a href="execute_program.php">
            <i class="fa fa-caret-square-o-right"></i> <span>Execute All Programs</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li>
          <a href="manage_program.php">
            <i class="fa fa-list"></i> <span>Manage Common Programs</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>
        
        <li>
          <a href="set_time.php">
            <i class="fa fa-clock-o"></i> <span>Set Run Time</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

        <li>
          <a href="manage_meta.php">
            <i class="fa fa-suitcase"></i> <span>Manage Metadata</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


        <li>
          <a href="manage_snap.php">
            <i class="fa fa-suitcase"></i> <span>Data Currency</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


        <li>
          <a href="maintain_toc.php">
            <i class="fa fa-suitcase"></i> <span>Maintain TOC</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>

         <li>
          <a href="change_pass.php">
            <i class="fa fa-th"></i> <span>Change Password</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>';}?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
