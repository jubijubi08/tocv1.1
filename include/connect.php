<?php
  //Database Configuration 
  $servername = "localhost";
  $dbusername = "root";
  $dbpassword = "";
  $dbname = "med-oms_v1_dev";

  // Create connection
  $conn = new mysqli($servername, $dbusername, $dbpassword, $dbname);
  
  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  }

  // file server location
  //$file_server="D:\\study/";
  //$file_server="\\\\server\\SC_Projects\\TOC\study/";
  // SAS software location
  $sas_soft="C:\\Program Files\\SASHome\\SASFoundation\\9.4\\SAS.exe";
              
?>
