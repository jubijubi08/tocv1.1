<?php

include("include/connect.php");
session_start();

$user_check=$_SESSION['login_user'];

$ses_sql=$conn->query("select username,firstlog from user_info where username='$user_check' ");
//print_r($ses_sql);

while($row = $ses_sql->fetch_assoc()) {
    $login_session = $row['username'];
    $firstlog = $row['firstlog'];
}

if(!isset($login_session))
{
    header("Location: index.php");
}

$username = $_SESSION['login_user'];
$sql="SELECT count(y.allocation_id) as snum FROM user_info as x , study_allocation as y WHERE x.username = '$username' AND x.user_id = y.user_id AND y.status='ON'";
$countstudy = $conn->query($sql);

while($row = $countstudy->fetch_assoc()) {
  $snum = $row['snum'];
}


?>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>TOC | SYSTEM</title>
<link rel="stylesheet" href="stree/demo.css" type="text/css">

<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- Bootstrap 3.3.2 -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- FontAwesome 4.3.0 -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons 2.0.0 -->
<link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
     folder instead of downloading all of them to reduce the load. -->
<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
<!-- css top menu -->
<link rel="stylesheet" href="cssmenu/menustyles.css">
<!-- bootstrap wysihtml5 - text editor -->
<link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="dist/img/scbd.ico">

<link rel="stylesheet" href="stree/metroStyle/metroStyle.css" type="text/css">


<style>
  .scbd {
    height:65px;
    background:#ffffff url('dist/img/logo.png') no-repeat;
    text-align: center;
    position: relative;
    background-position:center;
    z-index:1000;
  }
  
  li, span{
    font-size:17px;
  }
</style>

</head>


<body class="skin-blue layout-wide">
<div class="scbd"></div>
<div class="wrapper">
<header class="main-header">
<!-- Logo -->
<a href="home.php" class="logo" ><b>TOC</b>SYSTEM</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation" > 
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
    </a>


    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">

            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <span class="hidden-xs"><?php  echo "$username";  ?> </span>
                </a>
                <ul class="dropdown-menu">

                    <li class="user-footer">
                        <div class="pull-left">
                            <p> <small>New Study:<?php  echo " $snum";  ?> Total Study:<?php  echo " $snum";  ?></small></p>
                        </div>

                        <div class="pull-right">
                            <a href="logout.php" class="btn btn-default btn-flat">Sign out</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- drop down menu for user-->
        </ul>
    </div>
</nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar" style="padding-top: 80px;">

    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>



      <?php
           if ($firstlog == 'No'){

            echo "

        <li>
            <a href='home.php'>
                <i class='fa fa-th'></i> <span>Home</span> <small class='label pull-right bg-green'></small>
            </a>
        </li>

        <li>
          <a href='search.php'>
            <i class='fa fa-search'></i> <span>Search</span> <small class='label pull-right bg-green'></small>
          </a>
        </li>

                 ";
           }

      ?>
        <li>
          <a href="change_pass.php">
            <i class="fa fa-th"></i> <span>Change Password</span> <small class="label pull-right bg-green"></small>
          </a>
        </li>


    </ul>
</section>
<!-- /.sidebar -->
</aside>



<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="home.php"><i class="fa fa-home"></i> Home</a></li>
      </ol>
<!--   <h1>    </h1> -->

    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-8">
          <!-- MAP & BOX PANE -->
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Study Across Advanced Search</h3>
              <div class="box-tools pull-right">      </div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
              <div class="row">
                <div class="col-md-9 col-sm-8">
                  <div class="pad">
                    <div class="row">

                    <div class="col-md-12">
                                                                                        
                                                                                        
                        
                                  <div class="form-group has-feedback">
                                          <input id="ne_row_id" type="hidden" name="ne_row_id" value=>
                                          
                                              <div class="form-group has-feedback">
                                                   <p><b>Enter Search Terms Here:</b></p>                                                   
                                              </div>

                                          <div class="form-group row">
                                            <label for="example-text-input" class="col-xs-2 col-form-label">Search Terms:</label>
                                            <div class="col-xs-10">
                                              <input class="form-control" type="text" value=" " id="example-text-input">
                                            </div>
                                          </div>
                                          <hr>
                                            <div class="form-group has-feedback">
                                                   <p><b> Additional Criteria:</b></p>                                                   
                                              </div>

                                          <div class="form-group row">
                                            <label for="example-search-input" class="col-xs-2 col-form-label">Study Type:</label>
                                            <div class="col-xs-10">
                                              <select class="form-control">
                                                <option>All Studies</option>
                                                <option>Interventional Studies</option>
                                                <option>Observational Studies</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-group row">
                                            <label for="example-email-input" class="col-xs-2 col-form-label">Drugs:</label>
                                            <div class="col-xs-10">
                                              <input class="form-control" type="email" value="" id="example-email-input">
                                            </div>
                                          </div>
                                          
                                          <div class="form-group row">
                                            <label for="example-search-input" class="col-xs-2 col-form-label">Population Type:</label>
                                            <div class="col-xs-10">
                                              <select class="form-control">
                                                <option>screened</option>
                                                <option>randomised</option>
                                                <option>treated</option>
                                              </select>
                                            </div>
                                          </div>
                                          
                                          <div class="form-group row">
                                            <label for="example-tel-input" class="col-xs-2 col-form-label">Phase:</label>
                                            <div class="col-xs-10">
                                              <label class="custom-control custom-radio">
                                                <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Phase 0&nbsp;&nbsp;</span>
                                              </label>
                                              <label class="custom-control custom-radio">
                                                <input id="radio2" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Phase 1&nbsp;&nbsp;</span>
                                              </label>
                                              <label class="custom-control custom-radio">
                                                <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Phase 2&nbsp;&nbsp;</span>
                                              </label>
                                              <label class="custom-control custom-radio">
                                                <input id="radio2" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Phase 3&nbsp;&nbsp;</span>
                                              </label>
                                            </div>
                                          </div>
                                          
                                          <hr>
                                            <div class="form-group has-feedback">
                                                   <p><b> Eligibility Criteria:</b></p>                                                   
                                              </div>

                                          
                                          <div class="form-group row">
                                            <label for="example-email-input" class="col-xs-2 col-form-label">Age:</label>
                                            <div class="col-xs-10">
                                              <input class="form-control" type="email" value="" id="example-email-input">
                                            </div>
                                          </div>

                                          <div class="form-group row">
                                            <label for="example-tel-input" class="col-xs-2 col-form-label">Age Group:</label>
                                            <div class="col-xs-10">
                                              <label class="custom-control custom-radio">
                                                <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Child (birth–17)&nbsp;&nbsp;</span>
                                              </label>
                                              <label class="custom-control custom-radio">
                                                <input id="radio2" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Adult (18–65)&nbsp;&nbsp;</span>
                                              </label>
                                              <label class="custom-control custom-radio">
                                                <input id="radio1" name="radio" type="radio" class="custom-control-input">
                                                <span class="custom-control-indicator"></span>
                                                <span class="custom-control-description">Senior (66+)&nbsp;&nbsp;</span>
                                              </label>
                                            </div>
                                          </div>
                                  </div>
                                   
                            </div>
                    </div>
                    <form action="search_result.php" method="post">
                    <div class="row">
                    <div class="col-md-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat" id="new_entry_btn">Search Now</button>
                    </div>
                    </div>
                    </form>
                  </div>
                </div><!-- /.col -->
              </div><!-- /.row -->
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div><!-- /.col -->
      </div><!-- /.row (main row) -->
    </section><!-- /.content -->
  </div><!-- /.content-wrapper -->

<?php
include("include/footer.php");
?>
